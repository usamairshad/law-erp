
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Rules & Policies Sheet</h3>
    </div>

    <div class="panel panel-default myDivToPrint" id="myDivToPrint">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>

        <div class="panel-body table-responsive">
{{--            <button id="print" class="no-print btn btn-primary" onclick="printContent('myDivToPrint');" >Print</button>--}}

            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Category Policy</th>
                    <th>Code of Conduct</th>
                    <th>Policy</th>
                </tr>
                </thead>
                @php
                $i = 1;
                @endphp
                <tbody>
                @foreach($policy_data as $key => $data)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data->categorypolicy_name}}</td>
                    <td>{{$data['codeofconduct'][0]->codeofconduct_name ?? ""}}</td>
                    <td><a href="{{url('related-code-of-conduct',$data->id)}}">{{$data['policy'][0]->policy_name ?? ""}}</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop


@section('css')
    @media print {
        .myDivToPrint {
            background-color: white;
            height: 100%;
            width: 100%;
            position: fixed;
            top: 0;
            left: 0;
            margin: 0;
            padding: 15px;
            font-size: 14px;
            line-height: 18px;
        }
        .no-print, .no-print *
        {
            display: none !important;
        }
    }
@endsection



@section('javascript')
<script>
    function printContent()
    {
        window.print();
    }
</script>
@endsection