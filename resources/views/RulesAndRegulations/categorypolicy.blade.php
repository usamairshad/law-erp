@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Category Policy</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Policy</h3>

            <a href="" class="btn btn-success pull-right" data-toggle="modal" data-target=".exampleModal" onclick="resetform()">Add New Policy</a>
        </div>



        <div class="panel-body table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
                @if ($message = Session::get('delete'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Category Policy Name</th>
                    <th>Category Policy Description</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @php
                $i = 1;
                @endphp
                @foreach($records as $record)

                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$record->categorypolicy_name}}</td>
                    <td>{{$record->categorypolicy_description}}</td>
                    <form method="post" action="{{ url('/category-policy', ['id' => $record->id]) }}">
                        {{ method_field('delete') }}
                        {!! csrf_field() !!}
                    <td>
                        <input type="button"  data-categorypolicy-description="{{$record->categorypolicy_description}}" data-categorypolicy-name="{{$record->categorypolicy_name}}" data-categorypolicy-id="{{$record->id}}" class="btn btn-primary update" data-toggle="modal" data-target="#exampleModal" value="Update">

                            <button type="submit" class="btn btn-danger">Delete</button>


                    </td>
                    </form>
                </tr>

                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    {{--add new modal popup--}}
    <div class="modal fade exampleModal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('category-policy')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="hidden_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Category Policy Name:</label>
                                    <input type="text" class="form-control" value="" name="categorypolicy_name" id="categorypolicy_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Category Policy Description:</label>
                                    <input type="text" class="form-control" value="" name="categorypolicy_description" id="categorypolicy_description" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new modal popup end--}}

@stop

@section('javascript')
<script>
    $(".update").on("click",function() {

        var id = $(this).data('categorypolicy-id');
        var name = $(this).data('categorypolicy-name');
        var description = $(this).data('categorypolicy-description');
        $('#categorypolicy_name').val(name);
        $('#categorypolicy_description').val(description);
        $('#hidden_id').val(id);

    });
    function resetform(){

        $("#add_new").trigger("reset");
    }
</script>
@endsection