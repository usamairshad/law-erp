
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Report</h3>
    </div>

    <div class="panel panel-default myDivToPrint" id="myDivToPrint">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>

        <div class="panel-body table-responsive">
                        <button id="print" class="no-print btn btn-primary" onclick="printContent('myDivToPrint');" >Print</button>

            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">

                <tr>
                    <th>Policy Category</th>
                    <th>Policy Name</th>
                    <th>Code Of Conduct Name</th>

                </tr>

                <tr>
                    <td>{{$policy_category->categorypolicy_name}}</td>
                    <td>{{$policy->policy_name}}</td>
                    <td>{{$conducts->codeofconduct_name}}</td>

                </tr>
                <tr>

                    <th>Policy Category Description</th>
                    <th>Policy Description</th>
                    <th>Code Of Conduct Description</th>

                </tr>
                <tr>
                    <td>{{$policy_category->categorypolicy_description}}</td>
                    <td>{{$policy->policy_description}}</td>
                    <td>{{$conducts->codeofconduct_description}}</td>

                </tr>
            </table>
        </div>
    </div>
@stop


@section('css')
    @media print {
    .myDivToPrint {
    background-color: white;
    height: 100%;
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    margin: 0;
    padding: 15px;
    font-size: 14px;
    line-height: 18px;
    }
    .no-print, .no-print *
    {
    display: none !important;
    }
    }
@endsection



@section('javascript')
    <script>
        function printContent()
        {
            window.print();
        }
    </script>
@endsection