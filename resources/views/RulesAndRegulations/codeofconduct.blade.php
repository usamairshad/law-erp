@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Code of Conduct</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Policy</h3>

            <a href="" class="btn btn-success pull-right" data-toggle="modal" data-target=".exampleModal" onclick="resetform()">Add New Conduct</a>
        </div>



        <div class="panel-body table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('delete'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Code Of Conduct Name</th>
                    <th>Code Of Conduct Description</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @php
                $i = 1;
                @endphp
                @foreach($records as $record)


                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$record->codeofconduct_name}}</td>
                        <td>{{$record->codeofconduct_description}}</td>
                        <form method="post" action="{{url('code-of-conduct',$record->id)}}">
                            {{ method_field('delete') }}
                            {!! csrf_field() !!}
                            <td>
                                <input type="button"  data-codeofconduct-description="{{$record->codeofconduct_description}}" data-codeofconduct-name="{{$record->codeofconduct_name}}" data-codeofconduct-id="{{$record->id}}" class="btn btn-primary update" data-toggle="modal" data-target="#exampleModal" value="Update">

                                <button type="submit" class="btn btn-danger">Delete</button>


                            </td>
                        </form>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    {{--add new modal popup--}}
    <div class="modal fade exampleModal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('code-of-conduct')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="hidden_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Code Of Conduct Name:</label>
                                    <input type="text"  class="form-control" value="" name="code_of_conduct_name" id="code_of_conduct_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Code Of Conduct Description:</label>
                                    <input type="text" class="form-control" value="" name="code_of_conduct_description" id="code_of_conduct_description" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Select Category Policy:</label>
                                    <select class="form-control" name="category_policy_id" required id="hide">
                                        <option value="" disabled selected>--Select--</option>
                                        {!! App\Models\RulesAndRegulations\CategoryPolicy::categorypolicy() !!}

                                    </select>                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new modal popup end--}}

@stop

@section('javascript')
    <script>
        $(".update").on("click",function() {

            var id = $(this).data('codeofconduct-id');
            var name = $(this).data('codeofconduct-name');
            var description = $(this).data('codeofconduct-description');
            $('#code_of_conduct_name').val(name);
            $('#code_of_conduct_description').val(description);
            $('#hidden_id').val(id);

        });
        function resetform(){

            $("#add_new").trigger("reset");
        }
    </script>
@endsection