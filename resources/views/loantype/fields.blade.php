<div class="form-group col-md-3 @if($errors->has('loan_type')) has-error @endif" style = "float:left;">
    {!! Form::label('Loan Type', 'Loan Type*', ['class' => 'control-label']) !!}
    <input type="text" name="loan_type" class="form-control" required >
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
    <input type="text" name="description"  class="form-control" required >
</div>