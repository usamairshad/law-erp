<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
    {!! Form::label('Loan Type', 'Loan Type*', ['class' => 'control-label']) !!}
    <input type="text" name="loan_type" value="{{$asset_types->loan_type}}" class="form-control" required maxlength="30">
    <input type="hidden" name="id" value="{{$asset_types->loan_id}}" class="form-control" required maxlength="30">
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
    <input type="text" name="description" value="{{$asset_types->description}}"  class="form-control" required >
</div>