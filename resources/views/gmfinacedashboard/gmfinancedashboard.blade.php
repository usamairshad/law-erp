
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
         GM Finance Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <div class="container-fluid" style="margin-top: 40px">
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class="table">
                    <tr>
                        <th class="bg-color2">Roots Ivy International Schools</th>
                        <th class="bg-color2">IVY Collage of Sciences</th>
                        <th class="bg-color2">IVY United Schools</th>
                        <th class="bg-color2">Overall</th>
                    </tr>
                    <tr>
                        <td  class="bg-color2"></td>
                        <td  class="bg-color2"></td>
                        <td  class="bg-color2"></td>
                        <td  class="bg-color2"></td>

                    </tr>
                </table>
            </div>

            </div>



        <h2>
         Roots IVY INT'L SCHOOLS (PVT)LTD
        </h2>
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <table class="table">
                        <tr>
                            <th class="bg-color">S.No</th>
                            <th class="bg-color">Campus</th>
                            <th class="bg-color">Avg Monthly Fee</th>
                            <th class="bg-color">Physical Strength</th>
                            <th class="bg-color">Physical Strength</th>
                            <th colspan="7" class="text-center bg-color1">Monthly Revenue</th>

                        </tr>
                        <tr>
                            <td  class="bg-color"></td>
                            <td  class="bg-color"></td>
                            <td  class="bg-color"></td>
                            <td  class="bg-color"></td>
                            <td class="bg-color"></td>

                            <th style="color: white" class="bg-color1">Salaries</th>
                            <th style="color: white" class="bg-color1">Lower Staff</th>
                            <th style="color: white" class="bg-color1">Security Staff</th>
                            <th style="color: white" class="bg-color1">EOBI
                                ESSI
                                and
                                EPF</th>
                            <th style="color: white" class="bg-color1">Markup</th>
                            <th style="color: white" class="bg-color1">Rents</th>
                            <th style="color: white" class="bg-color1">Total FIXED</th>
                        </tr>
                    </table>
                </div>

            </div>




        <!-- Main content -->
        <section class="content" style="height: auto !important; min-height: 0px !important;">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3> {{$companies ?? 0}} </h3>

                            <p>COMPANIES</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="{{url('total-companies')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
                    </div>
                    </a>
                </div>

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3> {{$branches ?? 0}} </h3>
                            <p>BRANCHES</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="{{url('total-branches')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>65</h3>

                            <p>BOARDS</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="{{url('total-boards')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{{$programs ?? 0}}</h3>

                            <p>PROGRAMS</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="{{url('total-programs')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>


                <!-- ./col -->
            </div>

            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{$staff ?? 0}} </h3>

                            <p>TOTAL STAFF</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="{{url('total-staffs')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
                    </div>
                    </a>
                </div>

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>  {{$students ?? 0}}</h3>
                            <p>TOTAL STUDENTS</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="{{url('total-students')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3> {{$courses ?? 0}} </h3>
                            <p>COURSES</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="{{url('total-courses')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>


                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>3 </h3>

                            <p>FEE DEFAULTERS</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
                    </div>
                    </a>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row (main row) -->
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Income Vs Expense</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <div id="bar_chart_for_income_expense" style="width: 100%%; height: 70%;"></div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col (LEFT) -->
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Payable</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <div id="barchart_payable" style="width: 1100px; height: 500px;"></div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>

                <!-- /.col (RIGHT) -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Receivable</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <div id="barchart_receiable" style="width: 1100px; height: 500px;"></div>
                            </div>
                        </div>

                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">SOS</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <div id="SOSChart" style="width: 1200px; height: 500px"></div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">FEE DEFAULTERS</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <div id="barchart_defaulter" style="width: 1100px; height: 500px"></div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Walking Registration</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <div id="piechart" style="width: 900px; height: 500px;"></div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- LINE CHART -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Student Attendance</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <div id="barchart_student_attendance" style="width: 1100px; height: 500px;"></div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

        </div>




    <style>

        th {
            background-color: #6666ff;
            color: white;
        }
        td {
            background-color: #6666ff;

        }
        table, th, td {
            border: 1px solid white;
        }

        .border-right {
            border-right: 1px solid black;
        }
        .new-bg {
            background-color: #262626;
        }

        .new-bg1 {
            background-color: #8080ff;
        }
        .new-bg2 {
            background-color:  #006600;
        }

        .card-box {
            position: relative;
            color: #fff;
            padding: 20px 10px 40px;
            margin: 20px 0px;
        }
        .card-box:hover {
            text-decoration: none;
            color: #f1f1f1;
        }
        .card-box:hover .icon i {
            font-size: 100px;
            transition: 0s;
            -webkit-transition: 1s;
        }
        .card-box .inner {
            padding: 5px 10px 0 10px;
        }
        .card-box h3 {
            font-size: 27px;
            font-weight: bold;
            margin: 0 0 8px 0;
            white-space: nowrap;
            padding: 0;
            text-align: left;
        }
        .card-box p {
            font-size: 15px;
        }
        .card-box .icon {
            position: absolute;
            top: auto;
            bottom: 5px;
            right: 5px;
            z-index: 0;
            font-size: 72px;
            color: rgba(0, 0, 0, 0.15);
        }
        .card-box .card-box-footer {
            position: absolute;
            left: 0px;
            bottom: 0px;
            text-align: center;
            padding: 3px 0;
            color: rgba(255, 255, 255, 0.8);
            background: rgba(0, 0, 0, 0.1);
            width: 100%;
            text-decoration: none;
        }
        .card-box:hover .card-box-footer {
            background: rgba(0, 0, 0, 0.3);
        }
        .bg-blue {
            background-color: #00c0ef !important;
        }
        .bg-green {
            background-color: #00a65a !important;
        }
        .bg-orange {
            background-color: #f39c12 !important;
        }
        .bg-red {
            background-color: #d9534f !important;
        }

    </style>
        @stop
@section('javascript')
<script>

    $('.bg-color').addClass('new-bg');
    $('.bg-color1').addClass('new-bg1');
    $('.bg-color2').addClass('new-bg2');


</script>
    @endsection
<script>
    function branchID(id){
        var getid = id[id.selectedIndex].value;
        $.ajax({
            url: 'branch-id',
            type: 'GET',
            data: {
                "branch_id": getid
            },
            success: function(data){
                for(i in data){

                    $("#board").append("<option value='"+data[i]['board'].id+"'>"+data[i]['board'].name+"</option>");


                }
            }
        });

    }
    function boardID(id){
        var getid = id[id.selectedIndex].value;
        $.ajax({
            url: 'board-id',
            type: 'GET',
            data: {
                "board_id": getid
            },
            success: function(data){
                for(i in data){

                    // console.log(data);
                    $("#program").append("<option value='"+data[i]['program'].id+"'>"+data[i]['program'].name+"</option>");


                }
            }
        });

    }
    function programID(id){
        var getid = id[id.selectedIndex].value;
        $.ajax({
            url: 'program-id',
            type: 'GET',
            data: {
                "program_id": getid
            },
            success: function(data){
                for(i in data){

                    // console.log(data);
                    $("#class").append("<option value='"+data[i]['classes'].id+"'>"+data[i]['classes'].name+"</option>");


                }
            }
        });

    }
</script>
<!-- Area chart for bhul gya hun -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>



<!-- Area chart for bhul gya hun end -->

<script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:route('admin.account_reports.income'),
            success:function (data) {
                var chart_data ="";
                var htmlData="";
                for(i in data.data){
                    var exp= parseInt(data.data[i].expenses);
                    var inc= parseInt(data.data[i].income);
                    chart_data =  [data.data[i].branch_name, inc, exp];
                }

                var data = google.visualization.arrayToDataTable([
                    ['Month', 'Income', 'Expensive'],
                    chart_data

                    //  ['Roots IVY International School - Head Office', 1000, 400],



                ]);
                var options = {
                    chart: {
                        title: 'Income Vs Expense',
                    },

                    bars: 'vertical' // Required for Material Bar Charts.
                };

                var chart = new google.charts.Bar(document.getElementById('bar_chart_for_income_expense'));
                chart.draw(data, google.charts.Bar.convertOptions(options));

            }
        });



    }
</script>



<!-- Line chart for Payable -->
<script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:route('admin.account_reports.payable'),
            success:function (data) {
                xaxis = JSON.parse(data.data)
                yaxis = JSON.parse(data.amount)
                console.log(xaxis);

                var data = google.visualization.arrayToDataTable([
                    xaxis,
                    yaxis

                ]);

                var options = {
                    chart: {
                        title: 'Payable',
                        subtitle: 'Sales, Expenses, and Profit',
                    },
                    bars: 'vertical' // Required for Material Bar Charts.
                };

                var chart = new google.charts.Bar(document.getElementById('barchart_payable'));
                chart.draw(data, google.charts.Bar.convertOptions(options));

            }
        });
    }


</script>

<!-- Line chart for Receivable -->
<script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:route('admin.account_reports.receivable'),
            success:function (data) {
                xaxis = JSON.parse(data.data)
                yaxis = JSON.parse(data.amount)

                var data = google.visualization.arrayToDataTable([
                    xaxis,
                    yaxis

                ]);

                var options = {
                    chart: {
                        title: 'Receivable',
                        subtitle: 'Receivable',
                    },
                    bars: 'vertical' // Required for Material Bar Charts.
                };

                var chart = new google.charts.Bar(document.getElementById('barchart_receiable'));
                chart.draw(data, google.charts.Bar.convertOptions(options));
            }
        });
    }
</script>
<!-- Line chart for Receivable -->
<!-- Bar chart for SOS -->
<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawMultSeries);
    var chart_data=<?php echo json_encode($branch_wise_sos); ?>;

    function drawMultSeries() {
        var data = new google.visualization.DataTable();
        data.addColumn('string','Branches');
        data.addColumn('number','Students');
        data.addColumn('number','Teachers');
        data.addRows(
            chart_data
        );


        var options = {
            title: 'SOS',
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Value',
                minValue: 0
            },
            vAxis: {
                title: 'City'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('SOSChart'));
        chart.draw(data, options);
    }
</script>
<!-- Bar chart for SOS -->


<!-- Bar chart for SOS -->

<!-- Bar chart for Defaulter -->

<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawMultSeries);

    function drawMultSeries() {
        var data = google.visualization.arrayToDataTable([
            ['City', 'Students'],
            ['Roots IVY International School - Head Office', 8008000 ],
            ['Roots IVY International School - PWD Campus', 3694000],
            ['Roots IVY International School - Westridge Campus', 2896000],
            ['Roots IVY International School - Riverview Campus', 1953000],
            ['Roots IVY International School - F-8 Campus A Level', 1517000 ],
            ['Roots IVY International School - Educational Complex Faisalabad', 1517000 ],

            ['Roots IVY International School - DHA Phase-5 Lahore', 1517000 ],
            ['Roots IVY International School - Cantt Campus Bahawalpur', 1517000 ],
            ['Roots IVY International School - Signature School - G6/4 Islamabad', 1517000 ],
            ['Roots IVY International School - DHA Bahawalpur', 1517000 ],
        ]);

        var options = {
            title: 'FEE DEFAULTERS',
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Values',
                minValue: 0
            },
            vAxis: {
                title: 'City'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('barchart_defaulter'));
        chart.draw(data, options);
    }
</script>

<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    var chart_data_for_branch=<?php echo json_encode($branch_wise_walkin); ?>;
    //debugger
    function drawChart() {

        var data = new google.visualization.DataTable();
        data.addColumn('string','Branches');
        data.addColumn('number','Walkin');

        for (var i = 0; i < chart_data_for_branch.length; i++) {

            data.addRow(
                [chart_data_for_branch[i][0],chart_data_for_branch[i][1]]
            );
        }


        var options = {
            title: 'Walking Registration'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>

<!-- Student attendance -->
<script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Weekly', 'Roots IVY International School - Head Office', 'Roots IVY International School - PWD Campus', 'Roots IVY International School - Westridge Campus' , 'Roots IVY International School - Riverview Campus' , 'Roots IVY International School - F-8 Campus A Level','Roots IVY International School - Educational Complex Faisalabad','Roots IVY International School - DHA Phase-5 Lahore','Roots IVY International School - Cantt Campus Bahawalpur','Roots IVY International School - Signature School - G6/4 Islamabad','Roots IVY International School - DHA Bahawalpur'],
            ['Monday', 1000, 400, 200 ,250 ,300,400 , 500 ,800 ,900,950],
            ['Tuesday', 1170, 460, 250 ,250 ,300,400 , 500 ,800 ,700,950],
            ['Wednesday', 1170, 460, 250 ,250 ,450,400 , 500 ,800 ,900,950],
            ['Thursday', 1170, 460, 250 ,250 ,300,400 , 500 ,500 ,900,950],
            ['Friday', 1170, 460, 250 ,250 ,300,400 , 400 ,800 ,900,950],
            ['Saturday', 1170, 460, 250 ,300 ,300,400 , 500 ,800 ,500,950],

        ]);

        var options = {
            chart: {
                title: 'Student Attendance',
            },
            bars: 'vertical' // Required for Material Bar Charts.
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_student_attendance'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
</script>
<!-- Bar chart for Defaulter -->
<script src="{{ url('public/adminlte') }}/bower_components/chart.js/Chart.js"></script>
<script src="{{ url('public/adminlte') }}/bower_components/fastclick/lib/fastclick.js"></script>
s<script src="{{ url('public/adminlte') }}/dist/js/demo.js"></script>
