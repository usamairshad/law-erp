@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Utilities Bills</h3>
<a href="" class="btn btn-success pull-right" style = "float:right;" data-toggle="modal" id="reset" data-target="#newModal" data-target="#MymodalPreventHTML" data-toggle="modal" data-backdrop="static" data-keyboard="false">Add Bill</a>


        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>S.No</th>
                    <th>Branch</th>
                    <th>Month</th>
                    <th>Meter No</th>
                    <th>Nature</th>
                    <th>Bill Amount</th>
                    <th>With Holding Tax</th>
                    <th>Total Bill</th>
                    <th>Last Month Bill</th>
                    <th>Increase/Decrease Bill</th>
                    <th>Reason</th>
                    <th>Status</th>

                </tr>
                </thead>
                <tbody>

                    @foreach($utility_bills as $bills)
                    <tr data-entry-id="">
                    <td>{{$bills->id}}</td>
                        <td>{{$bills->branch->name}}</td>
                        <td>{{$bills->month}}</td>
                        <td>{{$bills->meter_no}}</td>
                        <td>{{$bills->nature}}</td>
                        <td>{{$bills->bill_amount}}</td>
                        <td>{{$bills->with_holding_tax}}</td>
                        <td>{{$bills->total_bill}}</td>
                        <td>{{$bills->last_month_bill}}</td>
                        <td>{{$bills->increase_decease_bill}}</td>
                        <td>{{$bills->status}}</td>
                        <td>
                            <a href="#" class="btn btn-xs btn-info edit" data-bills="{{$bills}}" data-branch="{{$bills->branch->name}}" data-toggle="modal"  data-target="#newModal" data-target="#MymodalPreventHTML" data-toggle="modal" data-backdrop="static" data-keyboard="false">Edit</a>
                            <form method="post" action="{{url('utility-bills',$bills->id)}}">
                                {{ method_field('delete') }}
                                {!! csrf_field() !!}
                                <button type="submit" href="#"
                                        class="btn btn-xs btn-danger">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--add new modal popup--}}
    <div class="modal fade exampleModal" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style = "max-width:750px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form" method="post" action="{{url('utility-bills')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="something">
                                        Branch:</label>
                                    <select name="branch" class="form-control" id="branch" required>
                                        <option selected disabled >--Select--</option>
                                        {!! \App\Models\Admin\Branches::branchList() !!}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">

                                    <label for="formGroupExampleInput">Month:</label>
                                    <input type="date" name="month" class="form-control" id="month">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Meter No:</label>
                                    <input type="text" class="form-control" value="" name="meter_no" id="meter_no" required>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Nature:</label>
                                    <input type="text" class="form-control" value="" name="nature" id="nature" required>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="something">
                                      Bill Amount:</label>
                                    <input type="text" name="bill_amount" class="form-control" id="bill_amount">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="something">
                                       With Holding Tax:</label>
                                  <input type="text" name="with_holding_tax" class="form-control" id="with_holding_tax">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Total Bill:</label>
                                    <input type="text" class="form-control" value="" name="total_bill" id="total_bill" required>
                                    <input type="hidden" id="hide" name="hide_id" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="something">
                                        Last Month Bill:</label>
                                    <input type="text" name="last_month_bill" class="form-control" id="last_month_bill">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="something">
                                        Increase or Decrease Bill:</label>
                                <input type="text" name="increase_decrease_bill" id="increase_decrease_bill" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Reason:</label>
                                    <input type="text" class="form-control" value="" name="reason" id="reason" required>

                                </div>
                            </div>

                        </div>
                    </div>



                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new modal popup end--}}


@stop

@section('javascript')

    <script>

        $(".edit").on("click",function() {
            var branch_id = $(this).data('bills').branch_id;
            var month = $(this).data('bills').month;
            var meter_no = $(this).data('bills').meter_no;
            var nature = $(this).data('bills').nature;
            var bill_amount = $(this).data('bills').bill_amount;
            var with_holding_tax = $(this).data('bills').with_holding_tax;
            var total_bill = $(this).data('bills').total_bill;
            var last_month_bill = $(this).data('bills').last_month_bill;
            var increase_decease_bill = $(this).data('bills').increase_decease_bill;
            var reason = $(this).data('bills').reason;
            var hidden_id = $(this).data('bills').id;




            $('#branch').val(branch_id);
            $('#meter_no').val(meter_no);
            $('#month').val(month);
            $('#nature').val(nature);
            $('#bill_amount').val(bill_amount);
            $('#with_holding_tax').val(with_holding_tax);
            $('#total_bill').val(total_bill);
            $('#last_month_bill').val(last_month_bill);
            $('#increase_decrease_bill').val(increase_decease_bill);
            $('#reason').val(reason);
            $('#hide').val(hidden_id);

        });
        $("#reset").on('click', function(){
            $("#form").trigger("reset");
        });
    </script>
@endsection
