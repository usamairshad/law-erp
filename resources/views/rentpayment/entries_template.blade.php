<table style="display: none;">
    <tbody id="entry_item-container" >
    <tr id="entry_item-######">
        <td colspan="2">
            <div class="form-group" style="margin-bottom: 0px !important;">
               <select name = "ledger[]" id = 'entry_item-ledger_id-######' class ="form-control description-data-ajax######" style = "width:150px;">
                                
                                
            </div>
          
        </td>
        <td style = "width:10%;">
                                <div class="form-group" style="margin-bottom: 0px !important;">
                                  <select name = "rent_id[]"  id="rent_id-2" class ="form-control" style = "width:100px;" required>
                                    <option> Select Property</option>
                                    @foreach($property as $pay )
                                    <option value = "{{$pay->id}}">{{$pay->name}}</option>
                                    @endforeach

                                  </select>
                                </div>
                             
                            </td>
        <td style="width:10%;">
            <div class="form-group" style="margin-bottom: 0px !important;">
            <select name = "pay[]" class ="form-control" style = "width:100px;">
                                    <option> Select Payee</option>
                                    @foreach($asset_types as $pay )
                                    <option value = "{{$pay->vendor_id}}">{{$pay->vendor_name}}</option>
                                    @endforeach

                                  </select> </div>
         
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
            <input type = "number" name ="invoice_total[]" placeholder = 'Invoice Total' class = "form-control"  />

            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
            <input type = "number" name ="advance[]" placeholder = 'Advance' class = "form-control invoice_total"  />

            </div>
        </td>
             <td style = "width:9%">
                                <div class="form-group" style="width:60px; margin-bottom: 0px !important;">
                                <input type = "number"  id="adjust-2" name ="adjust[]" style = "width:100px;" placeholder = 'adjust' value ='0' class = "form-control tax_rate"  required/>
                              
                                </div>
                            </td>
                            <td style = "width:9%">
                                <div class="form-group" style="width:60px; margin-bottom: 0px !important;">
                                <input type = "number"  id="amount-before-2" style = "width:100px;" name ="amount-before[]" placeholder = 'PRA' value ='0' class = "form-control tax_rate"  required/>
                              
                                </div>
                            </td>
                            <td style = "width:9%">
                                <div class="form-group" style="width:60px; margin-bottom: 0px !important;">
                                <input type = "number"  id="amount-after-2" style = "width:100px;" name ="amount-after[]" placeholder = 'PRA' value ='0' class = "form-control tax_rate"  required/>
                              
                                </div>
                            </td>
                            <td style = "width:9%">
                                <div class="form-group" style="width:60px; margin-bottom: 0px !important;">
                                <input type = "number"  id="pra-2" name ="pra[]" placeholder = 'PRA' value ='0' class = "form-control tax_rate"  required/>
                              
                                </div>
                            </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
            <select name = "law[]" class ="form-control" onchange="EachRow(######)" id = "law-######" style = "width:80px;">
                                    <option> Select Law</option>
                                    @foreach($laws as $pay )
                                    <option value = "{{$pay->law_id}}">{{$pay->law_section}}</option>
                                    @endforeach

                                  </select>
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
            <input type = "number" name ="law_rate[]"  id = "law_rate-######" placeholder = 'Tax Rate' class = "form-control"  />
                              
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
            <input type = "number"   id = 'entry_item-dr_amount-######' name ="amount[]" onblur = "FormControls.CalculateTotal()" onkeyup = "FormControls.CalculateTotal()"  onkeydown = "FormControls.CalculateTotal()" placeholder = 'Amount' class = "form-control entry_items-dr_amount########"  />

          
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
            <input type = "text"   name ="narration[]"  placeholder = 'Narration' class = "form-control"  />

            </div>
        </td>
        <td><button id="entry_item-del_btn-######" onclick="FormControls.destroyEntryItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>
    </tbody>
</table>