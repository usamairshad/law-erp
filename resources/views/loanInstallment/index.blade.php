@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Loan Installment Plan</h3>

                
                <a href="{{ route('loan-installment-create') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Add Loan Installments</a>
               

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($asset_types) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Loan Installment</th>
                        <th>Duration</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($asset_types) > 0)
                        @foreach ($asset_types as $asset_type)
                            <tr data-entry-id="{{ $asset_type->loan_plan_id }}">
                            <td>{{ $asset_type->loan_plan_id }}</td>
                                <td>{{ $asset_type->loan_installment }}</td>
                                <td>{{ $asset_type->tenure }}</td>
                                <td style = "float:left;">
                                      
                                        <a href="{{ route('loan-installment-edit', [$asset_type->loan_plan_id]) }}"
                                            class="btn btn-xs btn-info">Edit</a>
                                   
                                            <a  class="btn btn-xs btn-danger view" href="{{ route('loan-installment-delete', [$asset_type->loan_plan_id]) }}">Delete</a>
                             


                                  
                                </td> 
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
