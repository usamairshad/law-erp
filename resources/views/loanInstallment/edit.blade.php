@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Edit Loan Installment Plan</h3>

                
            <a href="{{ route('loan-installment-list') }}" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::open(['method' => 'POST','route' => ['loan-installment-update']]) !!}

        {!! Form::token(); !!}
        <div class="box-body" style = "margin-top:40px;">
            @include('loanInstallment.edit_fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer" style = "padding-top:26px; float: right;">
              <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
  
@endsection