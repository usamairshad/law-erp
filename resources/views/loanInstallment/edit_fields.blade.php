<div class="form-group col-md-3 @if($errors->has('loan_plan')) has-error @endif" style = "float:left;">
    {!! Form::label('Loan Type', 'Loan Plan*', ['class' => 'control-label']) !!}
    <input type="text" name="loan_plan" value="{{$asset_types->loan_installment}}" class="form-control" required maxlength="30">
    <input type="hidden" name="id" value="{{$asset_types->loan_plan_id}}" class="form-control" required maxlength="30">
</div>
<div class="form-group col-md-3 @if($errors->has('tenure')) has-error @endif" style = "float:left;">
    {!! Form::label('Tenure', 'Tenure*', ['class' => 'control-label']) !!}
    <input type="text" name="tenure" value="{{$asset_types->tenure}}"  class="form-control" required >
</div>