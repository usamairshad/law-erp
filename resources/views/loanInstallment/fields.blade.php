<div class="form-group col-md-3 @if($errors->has('loan_pan')) has-error @endif" style = "float:left;">
    {!! Form::label('Loan Plan', 'Loan Plan*', ['class' => 'control-label']) !!}
    <input type="text" name="loan_plan" class="form-control" required >
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('Duration', 'Duration*', ['class' => 'control-label']) !!}
    <input type="text" name="Duration"  class="form-control" required >
</div>