@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Increment Providend Fund</h1>
    </section>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ url('incrementPf') }}/create" class="btn btn-success pull-right">Add New Increment Provident Fund</a>
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Loans) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Role</th>
                    <th>Staff</th>
                    <th>Basic pay</th>
                    <th>Previuos Providend Fund</th>
                    <th>New Providend Fund</th>
                    <th>Amount</th>
                    <th>New Salry After Increment</th>
                    <th>Date</th>
                    
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($Loans) > 0)
                    @foreach ($Loans as $Loan)

                        <tr data-entry-id="{{ $Loan->id }}">
                            <td> {{$r}} </td>
                            <td>{{$Loan->role_id}}</td>
                            <td>{{$Loan->staff->first_name}} {{$Loan->staff->last_name}}</td>
                            <td>{{$Loan->basic}}</td>
                            <td>{{$Loan->pf_previous}}</td>
                            <td>{{$Loan->pf_new}}</td>
                            <td>{{$Loan->Amount}}</td>
                            <td>{{$Loan->New_salary_increment}}</td>
                            <td>{{$Loan->increment_date}}</td>
                            


                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection