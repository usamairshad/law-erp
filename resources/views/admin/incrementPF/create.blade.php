@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Increment Providend Fund</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Increment Providend Fund</h3>
            <a href="{{ url('incrementPf') }}" class="btn btn-success pull-right">Back</a>
        </div>


        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'url' => ['incrementPf'], 'id' => 'validation-form']) !!}
        <div class="box-body">


            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="row">
                 <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
               <div class="form-group">
                                  
                                       <label for="formGroupExampleInput">Roles:</label>
                              
                              <select name="role_id" onchange="rolesID(this)" class="form-control" id="asset" required>
                                  <option selected disabled >--Select--</option>
                                  @foreach($roles as $key=>$role)
                                  <option value="{{$role}}">{{$role}}</option>
                                  @endforeach
                              </select>
                                </div>
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
                 <div class="form-group">
                                      <label for="something">
                                        Assign TO Staff:</label>
                                <select name="user_id" onchange="staffID(this)" class="form-control" id="user_id">
                                  <option selected disabled >--Select--</option>
                             
                              </select>
                                </div>
                            </div>
                <div class="form-group col-md-3 @if($errors->has('increment_date')) has-error @endif">
                    {!! Form::label('increment_date', 'Date of Increment*', ['class' => 'control-label']) !!}
                    <input type="date" name="increment_date" class="form-control" required maxlength="10">
                </div>
                <div class="form-group col-md-3 @if($errors->has('')) has-error @endif">
                    {!! Form::label('Amount', 'Amount*', ['class' => 'control-label']) !!}
                        <input type="text" name="Amount" class="form-control contri" required maxlength="10">
                </div>
                </div>
               

            </div>

            </div>
            <div class="row">
                
                
                <div class="form-group col-md-3 @if($errors->has('basic')) has-error @endif">
                    {!! Form::label('basic', 'Basic', ['class' => 'control-label']) !!}
                    {!! Form::text('basic', old('basic'), ['class' => 'form-control basic', 'placeholder' => '','required' , 'readonly']) !!}
                    @if($errors->has('basic'))
                        <span class="help-block">
                        {{ $errors->first('basic') }}
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-3 @if($errors->has('')) has-error @endif">
                    {!! Form::label('New_salary_increment', 'New salary after increment*', ['class' => 'control-label']) !!}
                        <input type="text" name="New_salary_increment" class="form-control deduc" required maxlength="10" readonly="readonly">
                </div>

                <div class="form-group col-md-3 @if($errors->has('pf_previous')) has-error @endif">
                    {!! Form::label('pf_previous', 'Previous PF', ['class' => 'control-label pf_previous']) !!}
                    {!! Form::text('pf_previous', old('pf_previous'), ['class' => 'form-control pf_previous balance', 'placeholder' => '','required', 'readonly']) !!}
                    @if($errors->has('pf_previous'))
                        <span class="help-block">
                        {{ $errors->first('pf_previous') }}
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-3 @if($errors->has('pf_previous')) has-error @endif">
                    {!! Form::label('pf_new', 'New PF', ['class' => 'control-label']) !!}
                    {!! Form::text('pf_new', old('pf_new'), ['class' => 'form-control balance', 'placeholder' => '','required']) !!}
                    @if($errors->has('pf_new'))
                        <span class="help-block">
                        {{ $errors->first('pf_new') }}
                    </span>
                    @endif
                </div>
                
                <div class="form-group col-md-3 @if($errors->has('reason')) has-error @endif">
                    {!! Form::label('reason', 'Reason', ['class' => 'control-label']) !!}
                    {!! Form::text('reason', old('reason'), ['class' => 'form-control balance', 'placeholder' => '','required']) !!}
                    @if($errors->has('reason'))
                        <span class="help-block">
                        {{ $errors->first('reason') }}
                    </span>
                    @endif
                </div>
                </div>
{{--            <div class="form-group col-md-4 @if($errors->has('country_id')) has-error @endif">--}}
{{--                {!! Form::label('country_id', 'Country Name*', ['class' => 'control-label']) !!}--}}
{{--                {!! Form::select('country_id', $country->prepend('Select Country', ''),old('country_id'), ['class' => 'form-control','required']) !!}--}}

{{--                @if($errors->has('country_id'))--}}
{{--                    <span class="help-block">--}}
{{--                        {{ $errors->first('country_id') }}--}}
{{--                    </span>--}}
{{--                @endif--}}
{{--            </div>--}}
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')


    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    function rolesID(id){
        var getid = id[id.selectedIndex].value;
        //console.log(getid);
        $.ajax({
            url: '{{url('pf-role-related-user')}}',
            type: 'GET',
            data: {
                "role_id": getid
            },
            success: function(data){
                $('#user_id').empty();
            //console.log(data)
                for(i in data){

                  
                    $("#user_id").append("<option selected disabled >--Select--</option>")
                    $("#user_id").append("<option value='"+data[i].id+"'>"+data[i].first_name+"</option>");


                }
            }
        });

    }
    function staffID(id){
       
         var staffid = id[id.selectedIndex].value;
        //console.log(getid);
        $.ajax({
            url: '{{url('pf-user')}}',
            type:"get",
            data: {
                "staff_id":staffid
            },
            success:function(data){
                console.log(data);
                $(".basic").val(data[0].salary);
                $(".pf_previous").val(data[0].salary);

                $(".contri").change(function(event) {
                    var a = parseInt($(this).val());
                    var b = parseInt($(".pf_previous").val());

                    function add(a,b){
                        var c = a + b;
                        return c;
                    }
                    $(".deduc").val(add(a,b));
                });
            }
        })
    }
    $(document).ready(function(){
            
            $('.cnic').mask('00000-0000000-0');
            
            
        })
        
    
    </script>
    <script type="text/javascript">
                
        </script>
   
@endsection

