@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Loan</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <div class="box-header with-border">
            <h3 class="box-title">Update Loan</h3>
            <a href="{{ url('/loan') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
            {!! Form::model($Loan, ['method' => 'PUT', 'id' => 'validation-form', 'url' => ['loan', $Loan->id]]) !!}
            <div class="box-body">


            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="row">
                <div class="form-group col-md-3 @if($errors->has('amount')) has-error @endif">
                    {!! Form::label('amount', 'Amount', ['class' => 'control-label']) !!}
                    {!! Form::text('amount', old('amount'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    @if($errors->has('amount'))
                        <span class="help-block">
                        {{ $errors->first('amount') }}
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Role', ['class' => 'control-label']) !!}
                {!! Form::select('role_id',$role->prepend('Select Role', ''), old('role_id'), array('onchange' => 'rolesID(this)'), ['class' => 'form-control', 'placeholder' => '','required',]) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('role_id') }}
                    </span>
                @endif
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
                 <div class="form-group">
                                      <label for="something">
                                        Assign TO User:</label>
                                <select name="user_id" class="form-control" id="user_id" required>
                                  <option selected disabled >--Select--</option>
                             
                              </select>
                                </div>
                            </div>
                
                
                <div class="orm-group col-md-3 @if($errors->has('paid_status')) has-error @endif">
                    {!! Form::label('paid_status', 'Paid Status', ['class' => 'control-label']) !!}
                    <select name="paid_status" id="discount" class="form-control" placeholder="required">
                         <option selected disabled="">--Select</option>
                         @if($Loan->paid_status == 'not paid')
                        <option value="1" selected="not paid">Not Paid</option>
                        <option value="2">Partial Paid</option>
                        <option value="3">Paid</option>
                        @elseif($Loan->paid_status == 'partial paid')
                        <option value="1">Not Paid</option>
                        <option value="2" selected="partial paid">Partial Paid</option>
                        <option value="3">Paid</option>
                        @elseif($Loan->paid_status == 'paid')
                        <option value="1">Not Paid</option>
                        <option value="2">Partial Paid</option>
                        <option value="3" selected="paid">Paid</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="orm-group col-md-3 @if($errors->has('salary_deduct')) has-error @endif">
                    {!! Form::label('salary_deduct', 'Salary Deduct', ['class' => 'control-label']) !!}
                    <select name="salary_deduct" id="discount" class="form-control" placeholder="required">
                         <option selected disabled="">--Select</option>
                         @if($Loan->salary_deduct == 'yes')
                        <option value="1" selected="yes">Yes</option>
                        <option value="2">No</option>
                        @elseif($Loan->salary_deduct == 'no')
                        <option value="1">Yes</option>
                        <option value="2" selected="no">No</option>
                        @endif
                    </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('amount')) has-error @endif">
                    {!! Form::label('return_date', 'Return Date*', ['class' => 'control-label']) !!}
                        <input type="date" value="{{$Loan->return_date}}" name="return_date" class="form-control" required maxlength="10">
                </div>

                <div class="form-group col-md-3 @if($errors->has('amount')) has-error @endif">
                    {!! Form::label('remaining', 'Remaining', ['class' => 'control-label']) !!}
                    {!! Form::text('remaining', old('remaining'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    @if($errors->has('remaining'))
                        <span class="help-block">
                        {{ $errors->first('remaining') }}
                    </span>
                    @endif
                </div>
                <div class="orm-group col-md-3 @if($errors->has('type')) has-error @endif">
                    {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
                    <select name="type" id="discount" class="form-control" placeholder="required">
                         <option selected disabled="">--Select</option>
                        @if($Loan->type == 'advance')
                        <option value="1" selected="advance">Advance</option>
                        <option value="2">Loan</option>
                        @elseif($Loan->type == 'loan')
                        <option value="1">Advance</option>
                        <option value="2" selected="loan">Loan</option>
                        @endif
                    </select>
                </div>
                
            </div>

{{--            <div class="form-group col-md-4 @if($errors->has('country_id')) has-error @endif">--}}
{{--                {!! Form::label('country_id', 'Country Name*', ['class' => 'control-label']) !!}--}}
{{--                {!! Form::select('country_id', $country->prepend('Select Country', ''),old('country_id'), ['class' => 'form-control','required']) !!}--}}

{{--                @if($errors->has('country_id'))--}}
{{--                    <span class="help-block">--}}
{{--                        {{ $errors->first('country_id') }}--}}
{{--                    </span>--}}
{{--                @endif--}}
{{--            </div>--}}
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
            {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    function rolesID(id){
        var getid = id[id.selectedIndex].value;
        //console.log(getid);
        $.ajax({
            url: '{{url('loan-role-related-user')}}',
            type: 'GET',
            data: {
                "role_id": getid
            },
            success: function(data){
                $('#user_id').empty();
            console.log(data)
                for(i in data){

                  

                    $("#user_id").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");


                }
            }
        });

    }


    </script>
@endsection