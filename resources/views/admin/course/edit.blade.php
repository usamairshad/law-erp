@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Course</h3>
    <div class="box box-primary">
        <a href="{{ route('admin.courses.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'PUT', 'enctype' => 'multipart/form-data', 'route' => ['admin.courses.update',$courses->id], 'id' => 'validation-form']) !!}
        <div id="show">
            <div id="box" class="box-body" style = "padding-top:40px;">
             
                <input type="hidden" name="course_id" value="{{$courses->id}}">
                <div class="form-group col-md-5 @if($errors->has('name')) has-error @endif" style ="float:left;">
                    {!! Form::label('name', 'Course Name*', ['class' => 'control-label']) !!}
                   <input type="text" name="name" value="{{$courses->name}}"  required="required" placeholder="Enter Course Name" class="form-control" />
                </div>

                    <div class="form-group col-md-3 @if($errors->has('subject_code')) has-error @endif">
                    {!! Form::label('subject_code', 'Subject Code*', ['class' => 'control-label']) !!}
                   <input type="text" name="subject_code"  required="required"  value="{{$courses->subject_code}}" placeholder="Enter Subject Code" class="form-control" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('credit_hours')) has-error @endif">
                    {!! Form::label('credit_hours', 'Credit hours*', ['class' => 'control-label']) !!}
                   <input type="text" name="credit_hours"  required="required" value="{{$courses->credit_hours}}" placeholder="Enter Credit Hours" class="form-control" />
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <select name="type" value="{{$courses->type}}" class="form-control" required >
                            <option>Select Type</option>
                            @if($type = 'cie')
                            <option value="cie"  selected="cie">Cie</option>
                            <option value="edexcle">Edexcle</option>
                            @elseif($type = 'edexcle')
                            <option value="male">Cie</option>
                            <option value="edexcle"selected="edexcle">Edexcle</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-3 @if($errors->has('actual_fee')) has-error @endif">
                    {!! Form::label('actual_fee', 'Actual Fee*', ['class' => 'control-label']) !!}
                   <input type="text" name="actual_fee"  required="required"  value="{{$courses->actual_fee}}" placeholder="Enter Actual Fee" class="form-control" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('subject_fee')) has-error @endif">
                    {!! Form::label('subject_fee', 'Subject Fee*', ['class' => 'control-label']) !!}
                   <input type="text" name="subject_fee"  required="required"  value="{{$courses->subject_fee}}"  placeholder="Enter Actual Fee" class="form-control" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('subject_optional_code')) has-error @endif">
                    {!! Form::label('subject_optional_code', 'Subject Optional Code*', ['class' => 'control-label']) !!}
                   <input type="text" name="subject_optional_code"  value="{{$courses->subject_optional_code}}" placeholder="Enter Subject Optional Code" class="form-control" />
                </div>
                <input type="hidden" name="section_id" value="{{$courses->id}}">
                <div class="form-group col-md-6 @if($errors->has('description')) has-error @endif" style ="float:left;">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                     <input type="text" name="description" value="{{$courses->description}}" required="required" placeholder="Enter Description" class="form-control" />
                </div>
            </div>  
            <button type="submit"  class="btn btn-success col-md-12" > Update</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
    
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

