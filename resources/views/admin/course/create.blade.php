@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')
<style>
    .form-control
    {
        float:left;
    }
    </style>
@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
	<div class="card">
		<div class="card-body">
			<div class="main-content-label mg-b-5">
            <h3 style = "float:left;">Create Course</h3>
    <div class="box box-primary">
         <a  href="{{route('admin.courses.index')}}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.courses.store'], 'id' => 'validation-form']) !!}
        <div id="show">
            <div id="box" class="box-body" style = "padding-top:40px;">
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left">
                    {!! Form::label('name', 'Course Name*', ['class' => 'control-label']) !!}
                   <input type="text" name="name[0]"  required="required" placeholder="Enter Course Name" class="form-control" />
                </div>
                 
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left">
                    {!! Form::label('name', 'Program Name*', ['class' => 'control-label']) !!}
                        <select style="width: 100%" name="program_id" class="form-control" required="required">

                          @foreach($programs as $key => $program)
                            <option value="{{$program->id}}">{{$program->name}}</option>
                          @endforeach
                          
                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('subject_code')) has-error @endif"  style = "float:left">
                    {!! Form::label('subject_code', 'Subject Code*', ['class' => 'control-label']) !!}
                   <input type="text" name="subject_code[0]"  required="required" placeholder="Enter Subject Code" class="form-control" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('credit_hours')) has-error @endif" style = "float:left">
                    {!! Form::label('credit_hours', 'Credit hours*', ['class' => 'control-label']) !!}
                   <input type="number" name="credit_hours[0]" value = "0"  placeholder="Enter Credit Hours" class="form-control" min="0" />
                </div>

                <div class="col-md-3" style = "float:left">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Type:</label>
                        <select  class="form-control" name="type[0]">
                            <option value = "none">--Select--</option>
                            <option value="cie">Cie</option>
                            <option value="edexcle">Edexcle</option> 
                            <option value="Others">Others</option>                            
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-3 @if($errors->has('actual_fee')) has-error @endif" style = "float:left">
                    {!! Form::label('actual_fee', 'Actual Fee*', ['class' => 'control-label']) !!}
                   <input type="number" name="actual_fee[0]"  value = "0" placeholder="Enter Actual Fee" class="form-control" min="0" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('subject_fee')) has-error @endif" style = "float:left">
                    {!! Form::label('subject_fee', 'Subject Fee*', ['class' => 'control-label']) !!}
                   <input type="number" name="subject_fee[0]" value = "0"   placeholder="Enter Subject Fee" class="form-control" min="0" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('subject_optional_code')) has-error @endif" style = "float:left">
                    {!! Form::label('subject_optional_code', 'Subject Optional Code', ['class' => 'control-label']) !!}
                   <input type="text" name="subject_optional_code[0]" placeholder="Enter Subject Optional Code" class="form-control" />
                </div>

                <div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                     <input type="text" name="description[0]"  placeholder="Enter Description" class="form-control" />
                </div>
           
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
    
    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" style="height:120px"><hr style="border-top:1px solid #222d32"><div class="form-group col-md-3"><input required="required" type="text" name="name[]" placeholder="Enter Course Name" class="form-control" /></div><div class="form-group col-md-3"><input required="required" type="text" name="subject_code[]" placeholder="Enter Subject Code" class="form-control" /></div><div class="form-group col-md-3"><input required="required" type="number" name="credit_hours[]" placeholder="Enter Credit Hours" class="form-control" min="0" /></div><div class="form-group col-md-3"><input required="required" type="text" name="actual_fee[]" placeholder="Enter Actual Fee" class="form-control"/></div><div class="form-group col-md-3"><input required="required" type="text" name="subject_fee[]" placeholder="Enter Subject Fee" class="form-control"/></div><div class="form-group col-md-3"><input required="required" type="text" name="subject_optional_code[]" placeholder="Enter Subject Fee" class="form-control"/></div><div class="form-group col-md-6"><input type="text" required="required" name="description[]" placeholder="Enter Description" class="form-control" /></div><br> <div class="form-group col-md-2"><button style="margin-top: -27px;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

