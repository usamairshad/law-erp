@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Territories</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('territory_create'))
                <a href="{{ route('admin.territory.create') }}" class="btn btn-success pull-right">Add New Territory</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Territory) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>City Name</th>
                    <th>Region Name</th>
                    <th>Country Name</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                <?php $Branches = \App\Models\Admin\Branches::get()->getDictionary();?>
                @if (count($Territory) > 0)
                    @foreach ($Territory as $territory)
                        <tr data-entry-id="{{ $territory->id }}">
                            <td>{{ $territory->name }}</td>
                            <td>{{  $territory->city->city_name }}</td>
                            <td>{{  $territory->region->name }}</td>
                            <td>{{  $territory->country->country_name }}</td>
                            <td>
                                @if(Gate::check('territory_edit'))
                                    <a href="{{ route('admin.territory.edit',[$territory->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                {{-- @if(Gate::check('territory_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.territory.destroy', $territory->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif --}}
                            </td>
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection