@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Territories</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Territory</h3>
            <a href="{{ route('admin.territory.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.territory.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="form-group col-md-4 @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>30,'required','minlength'=>2]) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-4 @if($errors->has('city_id')) has-error @endif">
                {!! Form::label('city_id', 'City', ['class' => 'control-label']) !!}
                {!! Form::select('city_id', $city->prepend('City', ''),old('city_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('city_id'))
                    <span class="help-block">
                        {{ $errors->first('city_id') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-4 @if($errors->has('region_id')) has-error @endif">
                {!! Form::label('region_id', 'Region*', ['class' => 'control-label']) !!}
                {!! Form::select('region_id', $region->prepend('Select Region', ''),old('region_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('region_id'))
                    <span class="help-block">
                        {{ $errors->first('region_id') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-4 @if($errors->has('country_id')) has-error @endif">
                {!! Form::label('country_id', 'Country*', ['class' => 'control-label']) !!}
                {!! Form::select('country_id', $country->prepend('Select Country', ''),old('country_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('country_id'))
                    <span class="help-block">
                        {{ $errors->first('country_id') }}
                    </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/territory/create_modify.js') }}" type="text/javascript"></script>
@endsection

