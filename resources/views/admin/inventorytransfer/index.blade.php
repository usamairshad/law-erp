@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assets Transfer</h3>
            
                <a href="{{ route('admin.inventorytransfer.create') }}" style = "float:right;" class="btn btn-success pull-right">Assets Transfer</a>
       
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($InventoryTransfer) > 0 ? 'datatable' : '' }}" id="issue-table">
                <thead>
                <tr>
                    <th width="">S No:</th>
                    <th>Branch From</th>
                    <th>Branch To</th>
                    <th>Date</th>
                 
                </tr>
                </thead>
                <tbody>
                @if (count($InventoryTransfer) > 0)
                    @foreach ($InventoryTransfer as $InventoryTransfers)
                        <tr data-entry-id="{{ $InventoryTransfers->id }}">
                            <td>{{ $InventoryTransfers->id }}</td>

                            <td>{{ \App\Helpers\Helper::branchIdToName($InventoryTransfers->branch_from) }}</td>

                            <td>{{ \App\Helpers\Helper::branchIdToName($InventoryTransfers->branch_to) }}</td>

                            <td>{{ $InventoryTransfers->created_at }}</td>
                           
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop



@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.stockitems.mass_destroy') }}';
    </script>
@endsection

