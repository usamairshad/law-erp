@extends('layouts.app')
@section('stylesheet')
    <style>
        .example-template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop


@section('content')
    <h3 class="page-title">@lang('Update Inventory Transfer Stock Items')</h3>
    
    {!! Form::model($InventoryTransferModel, ['method' => 'PUT', 'id' => 'validation-form','route' => ['admin.inventorytransfer.update', $InventoryTransferModel->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-4 form-group">
                        {!! Form::label('branch_id', 'Branch From', ['class' => 'control-label']) !!}
                        <select id="branch_id" name="branch_id" class="form-control select2">
                            @if ($Branches->count())
                                <option value="">Select a Branch</option>
                                @foreach($Branches as $key =>$value)
                                    <option value="{{$key}}" {{$InventoryTransferModel->branch_from == $key ? 'selected="selected"' : ''}}>{{ $value }}</option>
                                @endforeach
                            @endif
                        </select>

                        <span id="branch_id_handler"></span>
                        @if($errors->has('branch_id'))
                            <span class="help-block">
                            {{ $errors->first('branch_id') }}
                            </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group">
                        {!! Form::label('branch_to', 'Branch To', ['class' => 'control-label']) !!}
                        @if ($Branches->count())
                            <select id="branch_to" name="branch_to" class="form-control select2 branch_to">
                                @if ($Branches->count())
                                    <option value="">Select a Branch</option>
                                    @foreach($Branches as $key =>$value)
                                        <option value="{{$key}}" {{$InventoryTransferModel->branch_to == $key ? 'selected="selected"' : ''}}>{{ $value }}</option>
                                    @endforeach
                                @endif
                            </select>
                        @endif
                        <span id="branch_id_handler"></span>
                        @if($errors->has('branch_to'))
                            <span class="help-block">
                            {{ $errors->first('branch_to') }}
                            </span>
                        @endif
                    </div>

                    <div class="col-xs-4 form-group @if($errors->has('sup_id')) has-error @endif">
                        {!! Form::label('sup_id', 'Brands *', ['class' => 'control-label']) !!}
                        <select id="sup_id" name="sup_id" class="form-control select2" disabled>
                            @if ($Suppliers->count())
                                <option value="">Select Brands</option>
                                @foreach($Suppliers as $Supplier)
                                    <option value="{{ $Supplier->id }}"{{$Transfer_detail[0]->sup_id == $Supplier->id ? 'selected="selected"' : ''}}>{{ $Supplier->sup_name }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('sup_id'))
                            <span class="help-block">
                                    {{ $errors->first('sup_id') }}
                                 </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="edit-area col-xs-12">

                <div class="controls">
                    <div class="box-header with-border">
                        <h3 class="box-title">Update Stock Detail</h3>
                    </div>

                    <ul class="heading_ul list-style-none text-center" style="float: left; width: 100%; margin-bottom:8px; padding: 0px;">

                        <li class="col-xs-4">
                            {!! Form::label('product_id', 'Item Description', ['id'=>'product_id','class' => 'control-label']) !!}
                        </li>
                        <li class="col-xs-3">
                            {!! Form::label('transfer_qty', 'Transfer Quantity', ['id'=>'transfer_qty','class' => 'control-label']) !!}
                        </li>
                        <li class="col-xs-4">
                            {!! Form::label('sr_no', 'Sr#', ['id'=>'sr_no','class' => 'control-label']) !!}
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-xs-12">
                <div>
                    <?php
                    $a = 0;
                    ?>
                    @foreach ($Transfer_detail as $Details)
                        <?php
                        $a++;
                        ?>
                            {!! Form::hidden('detail_id[]', $Details->id, ['id' => 'detail_id', 'class' => 'form-control', 'placeholder' => '']) !!}

                            <ul class="example-template" style="float: left; width: 100%; margin-bottom:8px; padding: 0px;">
                            <li class="col-xs-4 my-models" id="model">
                                @if ($StockItems->count())
                                    <select id="product_id" name="product_id[]" class="form-control select2 product">
                                        <option value="">Select Product</option>
                                        @foreach($StockItems as $Product)
                                            <option value="{{ $Product->id }}"{{$Details->product_id == $Product->id ? 'selected="selected"' : ''}}>{{ $Product->item_code  }} - {{$Product->products->products_name}} ( {{$Product->stockCategoryModel->cat_name}} )</option>
                                        @endforeach
                                    </select>
                                @endif
                                @if($errors->has('stock_item_id'))
                                    <span class="help-block">
                                    {{ $errors->first('stock_item_id') }}
                                    </span>
                                @endif
                            </li>
                            <li class="col-xs-3">
                                {!! Form::text('transfer_qty[]', $Details->transfer_qty, ['id' => 'transfer_qty', 'class' => 'form-control test','data-row-id'=>'', 'placeholder' => '']) !!}
                            </li>
                            <li class="col-xs-4">
                                {!! Form::text('sr_no[]', $Details->sr_no, ['id' => 'sr_no', 'class' => 'form-control test','data-row-id'=>'', 'placeholder' => '']) !!}

                            </li>
                        </ul>
                    @endforeach
                </div>

            </div>

            <div class="row">
                <div id="stockitems_submit" class="col-xs-12">
                    <div class="col-xs-6">
                        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    {!! Form::close() !!}
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/inventorytransfer/inventorytransfer_update_modify.js') }}" type="text/javascript"></script>

@endsection