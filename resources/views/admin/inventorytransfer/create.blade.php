@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Assets To Branch</h3>
            <a href="{{ route('admin.inventorytransfer.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.inventorytransfer.store'], 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body" style = "margin-top:40px;">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="col-md-3 form-group" style = "float:left;">
                            {!! Form::label('tracking_no', 'Tracking No', ['class' => 'control-label']) !!}
                            <input type="text" id="tracking_no" name="tracking_no" class="form-control" readonly placeholder="????????">
                            <span id="tracking_no_handler"></span>
                            @if($errors->has('tracking_no'))
                                <span class="help-block">
                            {{ $errors->first('tracking_no') }}
                            </span>
                            @endif
                        </div>
                        <div class="col-md-3 form-group" style = "float:left;">
                                {!! Form::label('transfer_mode', 'Transfer Mode *', ['class' => 'control-label']) !!}
                                <select id="transfer_mode" name="transfer_mode" class="form-control select2" required>
                                    <option value="">Select Mode</option>
                                    <option value="1">By Air</option>
                                    <option value="2">By Road</option>
                                </select>
    
                                <span id="branch_id_handler"></span>
                                @if($errors->has('transfer_mode'))
                                    <span class="help-block">
                                {{ $errors->first('transfer_mode') }}
                                </span>
                                @endif
                            </div>
                        <div class="col-md-3 form-group" style = "float:left;">
                                {!! Form::label('branch_id', 'Branch From*', ['class' => 'control-label']) !!}
                                <select id="branch_id" onchange="myFunction()" name="branch_id" class="form-control select2 branches" required>
                                    @if ($Warehouse->count())
                                        <option value="">Select a Branch</option>
                                        @foreach($Warehouse as $key =>$value)
                                            <option value="{{$value->id}}">{{ $value->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
    
                                <span id="branch_id_handler"></span>
                                @if($errors->has('branch_id'))
                                    <span class="help-block">
                                {{ $errors->first('branch_id') }}
                                </span>
                                @endif
                            </div>
                            <div class="col-md-3 form-group"  style = "float:left;">
                            {!! Form::label('branch_to', 'Branch To *', ['class' => 'control-label']) !!}
                            @if ($Warehouse->count())
                                <select id="branch_to" name="branch_to" class="form-control select2 SelectBranch" required>
                                    @if ($Warehouse->count())
                                        <option value="">Select a Branch</option>
                                        @foreach($Warehouse as $key =>$value)
                                            <option value="{{$value->id}}">{{ $value->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            @endif
                            <span id="branch_to_handler"></span>
                            @if($errors->has('branch_to'))
                                <span class="help-block">
                            {{ $errors->first('branch_to') }}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group" >
               

                       {{--<div class="col-xs-4 form-group @if($errors->has('sup_id')) has-error @endif">
                            {!! Form::label('sup_id', 'Brands *', ['class' => 'control-label']) !!}
                            <select id="sup_id"  name="sup_id" class="form-control select2" required>
                                @if ($Suppliers->count())
                                    <option value="">Select Brands</option>
                                    @foreach($Suppliers as $Supplier)
                                        <option value="{{ $Supplier->id }}">{{ $Supplier->sup_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span id="sup_id_handler"></span>
                            @if($errors->has('sup_id'))
                                <span class="help-block">
                                    {{ $errors->first('sup_id') }}
                                 </span>
                            @endif
                        </div>--}} 

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="about_featured">
                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default wow fadInLeft">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#11">
                                                <span class="fa fa-check-square-o"></span><b>Product List</b>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="11" class="panel-collapse">


                                        <div class="panel-body pad table-responsive">
                                            <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>R</u>ow</button>
                                            <table class="table table-bordered table-striped" id="users-table">
                                                <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Quantity</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php  $counter=0 ?>
                                                <tr id="line_item-1">
                                                    <td style="width: 50%;"><div class="form-group  @if($errors->has('product_id')) has-error @endif">


                                                            {!! Form::select('InventoryItems[product_id][1]',array(), old('product_id'), ['id' => 'InventoryItems-product_id-1','class' => 'form-control description-data-ajax select2 dublicate_product products', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                                                            @if($errors->has('product_id'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('product_id') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td> <div class="form-group  @if($errors->has('qunity')) has-error @endif">
                                                           
                                                            {!! Form::number('InventoryItems[qunity][1]', old('qunity'), ['id' => 'InventoryItems-qunity_id-1','class' => 'form-control maxQty' ,'data-row-id'=>'1', 'required' => 'true','min'=>'1']) !!}
                                                            @if($errors->has('line_qunity'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('line_qunity') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td style="width: 5%;"><button id="InventoryItems-del_btn-1" onclick="FormControls.destroyLineItem('0');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>

                                                </tr>
                                                <input type="hidden" id="InventoryItems-global_counter" value="<?php  echo ++$counter ?>"   />

                                                </tbody>

                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    @include('admin.inventorytransfer.items_template')
                </div>

                <div class="box-footer">
                    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
    </div>
@stop
<script type = "text/javascript">
     function myFunction() {
       
        $(".about_featured").show();
        $(".box-footer").show();

    }
    </script>
@section('javascript')

    <script src="{{ url('public/js/admin/inventorytransfer/inventorytransfer_create_modify.js') }}" type="text/javascript"></script>


<script type="text/javascript">
        $(document).on('change','.branches',function () 
        {
              var branch_id=$(this).val();
            
           
            
            console.log(branch_id);
            var op="";
            $.ajax({
                type:'get',
                url:'{!!URL::to('find_asset')!!}',

                data:{'branch_id':branch_id},
                dataType:'json',//return data will be json
                success:function(data){
               
                   
                   console.log(data);
                    // here price is coloumn name in products table data.coln name
                    
                     $html = '<option value="null">Select Product</option>';
                     data.forEach((dat) => {


                        $html += `<option value=${dat.id}>${dat.product_name}</option>`;
                     });

                   $(".products").html($html);

                },
                error:function(){
                }
            });


        }); 
</script>


@endsection


