@if ($StockItems->count())
<select id="product_id" name="product_id[]" class="form-control select2">
    <option value="">Select Product</option>
    @foreach($StockItems as $Product)
        <option value="{{ $Product->id }}">{{ $Product->item_code  }} - {{$Product->products->products_name}} ( {{$Product->stockCategoryModel->cat_name}} )</option>
    @endforeach
</select>
@endif