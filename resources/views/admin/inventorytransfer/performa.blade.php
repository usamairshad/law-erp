{{--$StockItems[0]->branches->name--}}
<div class="brands_row">
    {!! Form::hidden('stid', $StockItems->id, ['id'=>'stid','class' => 'form-control', 'placeholder' => '']) !!}
    {!! Form::hidden('group_id', $StockItems->group_id , ['id'=>'unit_price','class' => 'form-control', 'placeholder' => '']) !!}
    {!! Form::hidden('sup_id', $StockItems->sup_id, ['id'=>'sup_id','class' => 'form-control', 'placeholder' => '']) !!}
    {!! Form::hidden('cat_id', $StockItems->cat_id , ['id'=>'cat_id','class' => 'form-control', 'placeholder' => '']) !!}
    {!! Form::hidden('branch_id', $StockItems->branch_id , ['id'=>'branch_id','class' => 'form-control', 'placeholder' => '']) !!}
    {!! Form::hidden('itemcodes', $StockItems->item_code , ['id'=>'itemcodes','class' => 'form-control', 'placeholder' => '']) !!}
    {!! Form::hidden('st_name', $StockItems->st_name , ['id'=>'st_name','class' => 'form-control', 'placeholder' => '']) !!}

    {{--<div class="col-xs-6 form-group @if($errors->has('branch_id')) has-error @endif">--}}
        {{--{!! Form::label('branch_id', 'Branch *', ['class' => 'control-label']) !!}--}}
        {{--{!! Form::text('branch_id', '', ['id'=>'branch_id','class' => 'form-control', 'placeholder' => '']) !!}--}}
        {{--@if($errors->has('branch_id'))--}}
            {{--<span class="help-block">--}}
                {{--{{ $errors->first('branch_id') }}--}}
                {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}
    <div class="col-xs-4 form-group @if($errors->has('item_description')) has-error @endif">
        {!! Form::label('item_description', 'Item Description', ['class' => 'control-label']) !!}
        {!! Form::text('item_description', $StockItems->products->products_name." - ".$StockItems->stockCategoryModel->cat_name." -   (".$StockItems->suppliersModel->sup_name.")", ['id'=>'item_description','class' => 'form-control', 'placeholder' => '','readonly']) !!}
        @if($errors->has('item_description'))
            <span class="help-block">
                {{ $errors->first('item_description') }}
                </span>
        @endif
    </div>
    <div class="col-xs-2 form-group @if($errors->has('unit_price')) has-error @endif">
        {!! Form::label('unit_price', 'Unit Price', ['class' => 'control-label']) !!}
        {!! Form::text('unit_price', $StockItems->st_unit_price, ['id'=>'unit_price','class' => 'form-control', 'placeholder' => '','readonly']) !!}
        @if($errors->has('unit_price'))
            <span class="help-block">
                {{ $errors->first('unit_price') }}
                </span>
        @endif
    </div>
    <div class="col-xs-2 form-group @if($errors->has('total_qty')) has-error @endif">
        {!! Form::label('total_qty', 'Total Quantity', ['class' => 'control-label']) !!}
        {!! Form::text('total_qty', $StockItems->quantity, ['id'=>'total_qty','class' => 'form-control', 'placeholder' => '','readonly']) !!}
        @if($errors->has('total_qty'))
            <span class="help-block">
                {{ $errors->first('total_qty') }}
                </span>
        @endif
    </div>
    <div class="col-xs-2 form-group @if($errors->has('issue_qty')) has-error @endif">
        {!! Form::label('issue_qty', 'Issue Quantity', ['class' => 'control-label']) !!}
        {!! Form::text('issue_qty', old('model'), ['id'=>'issue_qty','class' => 'form-control', 'placeholder' => '']) !!}
        @if($errors->has('issue_qty'))
            <span class="help-block">
                {{ $errors->first('issue_qty') }}
                </span>
        @endif
    </div>
    <div class="col-xs-2 form-group @if($errors->has('stock_value')) has-error @endif">
        {!! Form::label('stock_value', 'Stock value', ['class' => 'control-label']) !!}
        {!! Form::text('stock_value', old('model'), ['id'=>'stock_value','class' => 'form-control', 'placeholder' => '','readonly']) !!}
        @if($errors->has('stock_value'))
            <span class="help-block">
                {{ $errors->first('stock_value') }}
                </span>
        @endif
    </div>

</div>
<div class="brands_row">
    <div class="col-xs-6 form-group @if($errors->has('remarks')) has-error @endif">
        {!! Form::label('remarks', 'Remarks', ['class' => 'control-label']) !!}
        {!! Form::textarea('remarks', '', ['id'=>'remarks','class' => 'form-control', 'placeholder' => '']) !!}
        @if($errors->has('remarks'))
            <span class="help-block">
                {{ $errors->first('remarks') }}
                </span>
        @endif
    </div>
</div>