@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Eobi</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">EOBI</h3>
            <a href="{{ url('eobi') }}" class="btn btn-success pull-right">Back</a>
        </div>


        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'url' => ['eobi'], 'id' => 'validation-form']) !!}
        <div class="box-body">


            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="row">
                <div class="form-group col-md-3 @if($errors->has('cnic')) has-error @endif">
                    {!! Form::label('cnic', 'CNIC*', ['class' => 'control-label']) !!}
                    <input type="text" name="cnic" placeholder="XXXXX-XXXXXXX-X" data-mask="00000-0000000-0" required="required" class="form-control cnic" />
                </div>
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
               <div class="form-group">
                                  
                                       <label for="formGroupExampleInput">Roles:</label>
                              
                              <select name="role_id" onchange="rolesID(this)" class="form-control" id="asset" required>
                                  <option selected disabled >--Select--</option>
                                  @foreach($roles as $key=>$role)
                                  <option value="{{$key}}">{{$role}}</option>
                                  @endforeach
                              </select>
                                </div>
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
                 <div class="form-group">
                                      <label for="something">
                                        Assign TO User:</label>
                                <select name="user_id" class="form-control" id="user_id">
                                  <option selected disabled >--Select--</option>
                             
                              </select>
                                </div>
                            </div>
                <div class="form-group col-md-3 @if($errors->has('date_of_reg')) has-error @endif">
                    {!! Form::label('date_of_reg', 'Date of Registration*', ['class' => 'control-label']) !!}
                    <input type="date" name="date_of_reg" class="form-control" required maxlength="10">
                </div>
                

            </div>

            </div>
            <div class="row">
                <div class="form-group col-md-3 @if($errors->has('year')) has-error @endif">
                    {!! Form::label('year', 'EOBI for year*', ['class' => 'control-label']) !!}
                    <input type="date" name="year" class="form-control" required maxlength="10">
                </div>
                
                <div class="form-group col-md-3 @if($errors->has('')) has-error @endif">
                    {!! Form::label('contribution', 'Contribution*', ['class' => 'control-label']) !!}
                        <input type="text" name="contribution" class="form-control contri" required maxlength="10">
                </div>
                <div class="form-group col-md-3 @if($errors->has('')) has-error @endif">
                    {!! Form::label('deduction', 'Deduction*', ['class' => 'control-label']) !!}
                        <input type="text" name="deduction" class="form-control deduc" required maxlength="10">
                </div>
                <div class="form-group col-md-3 @if($errors->has('balance')) has-error @endif">
                    {!! Form::label('balance', 'Balance', ['class' => 'control-label']) !!}
                    {!! Form::text('balance', old('balance'), ['class' => 'form-control balance', 'placeholder' => '','required']) !!}
                    @if($errors->has('balance'))
                        <span class="help-block">
                        {{ $errors->first('balance') }}
                    </span>
                    @endif
                </div>
                </div>
{{--            <div class="form-group col-md-4 @if($errors->has('country_id')) has-error @endif">--}}
{{--                {!! Form::label('country_id', 'Country Name*', ['class' => 'control-label']) !!}--}}
{{--                {!! Form::select('country_id', $country->prepend('Select Country', ''),old('country_id'), ['class' => 'form-control','required']) !!}--}}

{{--                @if($errors->has('country_id'))--}}
{{--                    <span class="help-block">--}}
{{--                        {{ $errors->first('country_id') }}--}}
{{--                    </span>--}}
{{--                @endif--}}
{{--            </div>--}}
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')


    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    function rolesID(id){
        var getid = id[id.selectedIndex].value;
        //console.log(getid);
        $.ajax({
            url: '{{url('eobi-role-related-user')}}',
            type: 'GET',
            data: {
                "role_id": getid
            },
            success: function(data){
                $('#user_id').empty();
            console.log(data)
                for(i in data){

                  

                    $("#user_id").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");


                }
            }
        });

    }
    $(document).ready(function(){
            
            $('.cnic').mask('00000-0000000-0');
            
            
        })
    
    </script>
    <script type="text/javascript">
        $(".deduc").keyup(function(event) {
            var a = parseInt($(this).val());
            var b = parseInt($(".contri").val());

            function add(a,b){
                var c = a + b;
                return c;
            }
            $(".balance").val(add(a,b));
        });
    </script>
@endsection

