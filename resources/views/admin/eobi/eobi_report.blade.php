@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Eobi Report</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        
        <div class="box-body">
            <div class="row">
                
                    <div class="form-group col-md-3 @if($errors->has('group_id')) has-error @endif">
                        <div class="form-group">
                        
                            <label for="formGroupExampleInput">Company:</label>
                            <select name="group_id" onchange="companyID(this)" id="selUser1" class="form-control select2" style="width: 100%;">
                                <option value=""> Select a Company </option>
                                @if (count($staff) > 0)
                                    @foreach ($companies as $key => $company)
                                        
                                        <option value='{{$key}}'>{{$company}}</option> 
                                    @endforeach
                                @endif
                            </select>
                            


                        </div>
                    
                    </div>
                    
                
                   <div class="form-group col-md-3 @if($errors->has('group_id')) has-error @endif">
                        <div class="form-group">  
                            <label for="formGroupExampleInput">Branch:</label>
                            <select name="group_id" onchange="branchID(this)" id="selUser2" class="form-control branch select2" style="width: 100%;">
                                <option value=""> Select a Branch </option>
                                {{-- @if (count($staff) > 0)
                                    @foreach ($branches as $key => $branch)
                                        
                                        <option value='{{$key}}'>{{$branch}}</option> 
                                    @endforeach
                                @endif --}}
                            </select>
                            

                        </div>
                        
                    
                    </div>
                    <div class="form-group col-md-3 @if($errors->has('group_id')) has-error @endif">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Staff:</label>
                            <select name="group_id" id="selUser3" class="form-control select3" style="width: 100%;">
                                <option value=""> Select a Staff </option>
                                {{-- @if (count($staff) > 0)
                                    @foreach ($staff as $id => $staf)
                                        
                                        <option value='{{$id}}'>{{$staf}}</option> 
                                    @endforeach
                                @endif --}}
                            </select>
                            


                        </div>
                    
                    </div>
                    <div class="form-group col-md-3 @if($errors->has('group_id')) has-error @endif">
                        <div class="form-group">

                            <button type="button" class="btn btn-info search" onclick="search()" style="margin-top: 20px">search</button>
                            <button type="button" class="btn btn-success print"  style="margin-top: 20px">print</button>
                            


                        </div>
                    
                    </div>
                
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>CNIC No</th>
                            <th>Campus</th>
                            <th>Name</th>
                            <th>EOBI Paid for the period</th>
                            <th>Contribution</th>
                            <th>Deduction</th>
                            <th>Balance</th>
                            <th>Date of Registration</th>
                          </tr>
                        </thead>
                        <tbody id="get_data"></tbody>
                      </table>
                </div>
            </div>
        <!-- /.box-header -->
        <!-- form start -->
        
    </div>
@stop

@section('javascript')


    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
 
  // Initialize select2
  $("#selUser1").select2();
  $("#selUser2").select2();
  $("#selUser3").select2();

  // Read selected option
  
});
    </script>
    <script type="text/javascript">
        function companyID(id){
            var getid = id[id.selectedIndex].value;
            //console.log(getid);
            $.ajax({
                url: '{{url('eobi-company')}}',
            type: 'GET',
            data: {
                "comp_id": getid
            },
            success: function(data){
                $(".branch").empty();
                $("#selUser2").append("<option selected disabled >--Select--</option>")
                for(i in data){
                    
                    $("#selUser2").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
                }
            }
            });
        }
        function branchID(id){
            var getid = id[id.selectedIndex].value;
            $.ajax({
                url: '{{url('eobi-branch')}}',
                type: "get",
                data: {
                "branch_id": getid
            },
            success: function(data){
                $("#selUser3").empty();
                $("#selUser3").append("<option selected disabled >--Select--</option>");

                for(i in data){
                    $("#selUser3").append("<option value='"+data[i].id+"'>"+data[i].first_name +" "+data[i].middle_name+"</option>")
                }
            }
            })
        }
        function search(){
            var staffid = $("#selUser3").val();
            $.ajax({
                url: '{{url('eobi-staff')}}',
                type: "get",
                data: {
                    "staff_id": staffid
                },
                success: function(data){
                    var htmlData = '';
                    console.log(data);
                    for(i in data){
                        
                            htmlData +="<tr>";
                                htmlData +="<td>"+data[i].cnic+"</td>";
                                htmlData+="<td>"+data[i].name+"</td>";
                                htmlData+="<td>"+data[i].first_name+"</td>";
                                htmlData +="<td>"+data[i].year+"</td>";
                                htmlData+="<td>"+data[i].contribution+"</td>";
                                htmlData+="<td>"+data[i].deduction+"</td>";
                                htmlData +="<td>"+data[i].balance+"</td>";
                                htmlData+="<td>"+data[i].date_of_reg+"</td>";
                                
                            htmlData+="</tr>";
                    }
                    $("#get_data").html(htmlData);
                    
                        

                }
            })

        }
        $(".print").click(function(event) {
            window.print();
        });
        
    </script>
    
@endsection

