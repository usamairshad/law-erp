@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>EOBI</h1>
    </section>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ url('eobi') }}/create" class="btn btn-success pull-right">Add New Eobi</a>
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Loans) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Role</th>
                    <th>User</th>
                    <th>Cnic</th>
                    <th>Year</th>
                    <th>Contribution</th>
                    <th>Deduction</th>
                    <th>Balance</th>
                    <th>date_of_reg</th>
                    
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($Loans) > 0)
                    @foreach ($Loans as $Loan)

                        <tr data-entry-id="{{ $Loan->id }}">
                            <td> {{$r}} </td>
                            <td>{{$Loan->role->name}}</td>
                            <td>{{$Loan->user->name}}</td>
                            <td>{{$Loan->cnic}}</td>
                            <td>{{$Loan->year}}</td>
                            <td>{{$Loan->contribution}}</td>
                            <td>{{$Loan->deduction}}</td>
                            <td>{{$Loan->balance}}</td>
                            <td>{{$Loan->date_of_reg}}</td>
                            


                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection