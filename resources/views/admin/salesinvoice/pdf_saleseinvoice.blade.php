<!DOCTYPE>
<!DOCTYPE html>
<html>
<head>
    <title>ERP Invoice Table</title>
</head>
<body>
    <!-------------------
    heading  sufi wheat
   -------------------->
<div style="background: #000; color: #ffffff; margin-bottom: 0;">
    <h2 style="  margin-top: 5px; margin-bottom: 0; text-align: center"><strong>SUFI WHEAT PRODUCTS (PVT) LTD.</strong></h2>
    <h3  style=" margin-top: 5px; margin-bottom: 0;  text-align: center">37-Ferozepur Road, Lahore.</h3>
    <h4  style=" margin: 5px 0 0 0;  text-align: center">CREDIT BOOK</h4>
</div>

    <!-------------------
     Gate-Pass Details
    -------------------->


    <table style="border:1px solid #000000;  margin:0 0 20px 0; " width="100%" >
        <tbody style="padding: 100px;">
        <tr style="">
            <th style="padding: 10px;">Date</th>
            <td style="border-right:1px solid black;">{{$SalesInvoiceMode->invoice_date}}</td>
            <th>Customer Name</th>
            <td style="border-right:1px solid black;">{{$SalesInvoiceMode->CustomersModel->name}}</td>
            <th>Invoice No</th>
            <td>{{$SalesInvoiceMode->invoice_no}}</td>
        </tr>
        </tbody>
    </table>

        <!-------------------
        Invoice Products Details
        -------------------->

        <table style="border-collapse:collapse; border: 1px solid black; text-align: left; margin:20px 0; " width="100%" cellpadding="5">
            <tbody>
            <thead style="color:#FFFFFF; background-color:#000000;">
            <tr>
                <th>Sr</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                <th>Weight</th>
                <th>Total Weight</th>
                <th>Total Price</th>
            </tr>
            </thead>
            <!-- table head............................................... -->
           <?php  $counter = 1; ?>
            @foreach($Products as $product)
            <tr>
                <td style="border: 1px solid black">{{$counter}}</td>
                <td style="border: 1px solid black">{{ $product->productsModel->products_name }}</td>
                <td style="border: 1px solid black;">{{ $product->quantity }}</td>
                <td style="border: 1px solid black;">{{ $product->unit_price }}</td>
                <td style="border: 1px solid black;">{{ $product->weight }}{{ $product->UnitModel->shortcode }}</td>
                <td style="border: 1px solid black;">{{ $product->total_weight }}{{ $product->UnitModel->shortcode }}</td>
                <td style="border: 1px solid black;">{{ $product->total_price }}</td>
            </tr>
                $counter++;
            @endforeach
            </tbody>

        </table>

            <!-------------------
             Invoice fotter
             -------------------->

    <table width="30%" style="float: right" cellspacing="10">
        <tbody>
        <tr>
            <td  style="width: 20%; float: left;">
                    <strong>Tax</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left;">
                    {{$SalesInvoiceMode->tax}}
                </span>
            </td>

        </tr>
        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Freight</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left; border-bottom: 1px solid #000000;">
                    {{$SalesInvoiceMode->freight}}
                </span>
            </td>
        </tr>
        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Discount</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left; border-bottom: 1px solid #000000;">
                    {{$SalesInvoiceMode->discount}}
                </span>
            </td>
        </tr>
        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Total</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left; border-bottom: 1px solid #000000;">
                    {{$SalesInvoiceMode->grand_total}}
                </span>
            </td>
        </tr>
        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Signature</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000; ">
            </td>
        </tr>


        </tbody>
    </table>


</body>
</html>