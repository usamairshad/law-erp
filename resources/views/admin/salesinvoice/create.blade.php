
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale Order</h1>
    </section>
@stop

<style>
    .select2{
        width: 100% !important;
    }
    .amount_section{
        margin-top: 30px;
    }

    .form-group{
        width: 100%;
        float: left;
    }

</style>

@section('content')
    <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title">Sale Order</h3>
            <a href="{{ route('admin.salesinvoice.index') }}" class="btn btn-success pull-right">Back</a>
        </div>


        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.saleinvoice.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('admin.saleseinvoice.fields')

        <!-- /.box-body -->


                {{--
                --}}{{--product list of Purchase request--}}{{--
                @include('admin.purchaseorder.product_list_of_request')--}}
                <div id="prodcut"></div>

                @include('admin.saleseorder.caluculate_total_form')

                <div class="box-footer">

                    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                </div>
                @include('admin.saleseorder.items_template')

                {!! Form::close() !!}
        </div>
    </div>


@stop

@section('javascript')
    <script src="{{ url('public/js/admin/saleinvoice/sale_invoice_create_modify.js') }}" type="text/javascript"></script>
@endsection

