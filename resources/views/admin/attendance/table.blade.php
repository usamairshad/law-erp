@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Student Attendance Detail</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
                
               
            
        
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($student_attendance) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Student First Name</th>
                    <th>Student Last Name</th>
                    <th>Attendance Type</th>
                    <th>Attendance</th>
                    <th>Branch Name</th>
                    <th>Board Name</th>
                    <th>Program Name</th>
                    <th>Class Name</th>
                    <th>Class Date</th>
                    <th>Class Time</th>


                </tr>
                </thead>

                <tbody>
                @if (count($student_attendance) > 0)
                    @foreach ($student_attendance as $student_attendance1)
                        
                            <td>{{ \App\Helpers\Helper::studentIdToFirstName($student_attendance1->student_id) }}</td>
                            <td>{{ \App\Helpers\Helper::studentIdToLastName($student_attendance1->student_id) }}</td>
                            <td>{{ $student_attendance1->attendance_type }}</td> 
                            <td>{{ $student_attendance1->attendance }}</td> 
                            <td>{{ \App\Helpers\Helper::studentIdToBranchName($student_attendance1->student_id) }}</td>
                            <td>{{ \App\Helpers\Helper::studentIdToBoardName($student_attendance1->student_id) }}</td>
                            <td>{{ \App\Helpers\Helper::studentIdToProgramName($student_attendance1->student_id) }}</td>
                            <td>{{ \App\Helpers\Helper::studentIdToClassName($student_attendance1->student_id) }}</td>
                            <td>{{ $student_attendance1->class_date }}</td>                    
                            <td>{{ $student_attendance1->class_time }}</td> 

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection