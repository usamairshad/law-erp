

<div class="form-group col-md-3 @if($errors->has('branch_name')) has-error @endif">
    {!! Form::label('branch_name', 'Branch Name', ['class' => 'control-label']) !!}

     <select name="branch_name" id="branch_name" class="form-control SelectBranch">
            <option>Select</option>
              @foreach($branch_data as $key => $branch)
                <option value="{{$key}}">{{$branch}}</option>                                      
              @endforeach    
      </select>
</div>


<div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
    {!! Form::label('Board', 'Board', ['class' => 'control-label']) !!}

     <select name="SelectBoard" id="SelectBoard" class="form-control SelectBoard">
            
      </select>
</div>



<div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif">
   {!! Form::label('Programs', 'Programs', ['class' => 'control-label']) !!}
    <select id="programs" name="programs" class="form-control SelectProgram">      
    </select>
</div>


<div class="form-group col-md-3 @if($errors->has('classes')) has-error @endif">
    {!! Form::label('Class_id', 'Class ', ['class' => 'control-label']) !!}
    <select id="classes" name="classes" class="form-control SelectClass">      
    </select>
    
</div>
<div class="form-group col-md-3 @if($errors->has('sections')) has-error @endif">
    {!! Form::label('Sections_id', 'Section', ['class' => 'control-label']) !!}
    <select id="sections" name="sections" class="form-control SelectSection">      
    </select>
</div>
<div class="form-group col-md-4 @if($errors->has('start_date')) has-error @endif">
    {!! Form::label('Start_date', 'Start Date*', ['class' => 'control-label']) !!}
    <!--<input type="date" id="start_date" name="start_date" class="form-control"  required >
    --><input type="date" id="dateInput1" name="dateInput1" class="form-control" > 

</div>

<div class="form-group col-md-4 @if($errors->has('end_date')) has-error @endif">
    {!! Form::label('End_date', 'End Date*', ['class' => 'control-label']) !!}
  <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
    --><input type="date" id="dateInput2" name="dateInput2" class="form-control"    > 
</div>

<!-- <div class="form-group col-md-3 @if($errors->has('courses')) has-error @endif">
    {!! Form::label('Subject_id', 'Course', ['class' => 'control-label']) !!}
    <select id="courses" name="courses" class="form-control SelectCourse1 SelectSection">      
    </select>
</div>
 -->



<script type="text/javascript">


//Board Selection
        $(document).on('change','.SelectBranch',function () 
        {
          
            
            var branch_id=$('#branch_name').val();
           
            
            console.log(branch_id);


            var op="";
            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board')!!}',
                data:{'branch_id':branch_id},
                dataType:'json',//return data will be json
                success:function(data){

                  console.log(data);
                    // here price is coloumn name in products table data.coln name
                    // console.log(data);
                     $html = '<option value="null">select board</option>';
                     data.forEach((dat) => {


                         $html += `<option value=${dat.board.id}>${dat.board.name}</option>`;
                     });

                   $("#SelectBoard").html($html);

                },
                error:function(){
                }
            });


        });  


//Program Selection
        $(document).on('change','.SelectBoard',function () 
        {
          
            var board_id=$(this).val();
            var branch_id=$('#branch_name').val();
            var a=$('#program').parent();
            console.log(board_id);
            console.log(branch_id);
            var op="";
            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program')!!}',
                data:{'board_id':board_id,'branch_id':branch_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    $html = '<option value="null">select program</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.program.id}>${dat.program.name}</option>`;
                    });

                    $("#programs").html($html);

                },
                error:function(){
                }
            });


        });

  function selectRefresh() {
      $("#courses").select2({

      });
  }

  $(document).ready(function() {
    selectRefresh();
  });
//ClassSelection

 $(document).on('change','.SelectProgram',function () 
        {
          
            var program_id=$(this).val();
            console.log(program_id);
            var branch_id=$('#branch_name').val();
            var board_id=$('#SelectBoard').val();
            
            console.log(board_id);
            console.log(branch_id);
            
            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program_class')!!}',
                data:{'board_id':board_id,'branch_id':branch_id,'program_id':program_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    $html = '<option value="null">select class</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.get_class.id}>${dat.get_class.name}</option>`;
                    });

                    $("#classes").html($html);

                },
                error:function(){
                }
            });


        });
//Classes Selection



//Sections

 $(document).on('change','.SelectClass',function () 
        {
          
            var class_id=$(this).val();
            
            var branch_id=$('#branch_name').val();
            var board_id=$('#SelectBoard').val();
            var program_id=$('.SelectProgram').val();

            console.log(class_id);
            console.log(board_id);
            console.log(branch_id);
            console.log(program_id);
            


            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program_class_section')!!}',
                data:{'board_id':board_id,'branch_id':branch_id,'program_id':program_id,'class_id':class_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    $html = '<option value="null">select class</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.section.id}>${dat.section.name}</option>`;
                    });

                    $("#sections").html($html);

                },
                error:function(){
                }
            });

        });
//Sections



//Courses

 $(document).on('change','.SelectSection',function () 
{



          
            //var teacher_id=$('#teacher_id').val();

            
            
            var branch_id=$('#branch_name').val();
            
            var board_id=$('#SelectBoard').val();
            var program_id=$('.SelectProgram').val();
            var class_id=$('.SelectClass').val();  



            var section_id=$(this).val();

            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_program_courses_timetable')!!}',
                data:{'program_id':program_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    
                    $html = '<option value="null">select course</option>';
                     data.forEach((dat) => {
                       $html += `<option value=${dat.id}>${dat.name}</option>`;
                     });

                     $("#courses").html($html);
                    

                },
                error:function(){
                }
            });




        });



  $(document).on('change','#courses',function () 
{
           
            var teacher_id=$('#teacher_id').val();            
            var branch_id=$('#branch_id').val();
            var board_id=$('#SelectBoard').val();
            var program_id=$('.SelectProgram').val();
            var class_id=$('.SelectClass').val();   
            var section_id=$(this).val();

            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program_class_section_students')!!}',
                data:{'board_id':board_id,'branch_id':branch_id,'program_id':program_id,'class_id':class_id,'section_id':section_id},
                dataType:'json',//return data will be json
                success:function(studentlistdat){
                    // here price is coloumn name in products table data.coln name
                    
                    



                    //$html = '<option value="null">select course</option>';
                    $list='';
                  var i=0;
                    studentlistdat.forEach((dataa) => {
                        $list += `
                        
                        <tr>                            
                            <input type="hidden" id="student.${dataa.id}" name="student[]" value="${dataa.id}">
                            <td>${dataa.reg_no}</td>
                            <td>${dataa.first_name}</td>
                            <td>${dataa.last_name}</td>
                            <td>${dataa.mobile_1}</td>
                            <td>
                            <input type="radio" id="present" name="attendance[`+ i +`]" value="present" required>
                            </td>
                            <td><input type="radio" id="absent" name="attendance[`+ i +`]" value="absent" required></td>
                            <td><input type="radio" id="leave" name="attendance[`+ i +`]" value="leave" required></td>
                            <td><input type="radio" id="shortleave" name="attendance[`+ i +`]" value="shortleave" required></td>
                           
                            <br>

                        </tr>

                        `;

                        i++;
                    });

                    $("#studnetlist").html($list);
                    

                   
                    

                },
                error:function(){
                }
            });

});
//Courses




$(document).ready(function() {
    $('.datatable').DataTable()
});




$("#seeAnotherField").change(function() {
  if ($(this).val() != "Teacher") { 
    $('#otherFieldDiv').show();
    $('#otherField').attr('required', '');
    $('#otherField').attr('data-error', 'This field is required.');
  } else {
    $('#otherFieldDiv').hide();
    $('#otherField').removeAttr('required');
    $('#otherField').removeAttr('data-error');
  }
});
$("#seeAnotherField").trigger("change");

</script>





