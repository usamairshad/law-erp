@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sections</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <a href="{{ route('admin.section.index') }}" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'PUT', 'enctype' => 'multipart/form-data', 'route' => ['admin.section.update',$section->id], 'id' => 'validation-form']) !!}
        <div id="show">
            <div id="box" class="box-body">
                <h2>Update Section</h2>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
                    {!! Form::label('name', 'Section Name*', ['class' => 'control-label']) !!}
                   <input type="text" name="name" value="{{$section->name}}"  required="required" placeholder="Enter Section Name" class="form-control" />
                </div>
                <input type="hidden" name="section_id" value="{{$section->id}}">
                <div class="form-group col-md-6 @if($errors->has('description')) has-error @endif">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                     <input type="text" name="description" value="{{$section->description}}" required="required" placeholder="Enter Description" class="form-control" />
                </div>
            </div>  
            <button type="submit"  class="btn btn-success col-md-12" > Update</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
    
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

