@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Section</h3>
            <a href="{{ route('admin.section.create') }}" style = "float:right;" class="btn btn-success pull-right">Create Section</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($section) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($section) > 0)
                    @foreach ($section as $s)
                        <tr data-entry-id="{{ $s->id }}">
                            <td>{{ $s->name }}</td>
                            <td>{{ $s->description }}</td>
                            <td>
                                <a href="{{route('admin.section.edit',[$s->id])}}"><button style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-primary">Edit</button></a>
                                {{--{{ Form::open([ 'method'  => 'delete', 'route' => [ 'admin.section.destroy', $s->id ] ]) }}
                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }} --}}                  
                            </td>

                        </tr>
                        
                    @endforeach
                    
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection