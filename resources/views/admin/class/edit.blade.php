@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Classes</h3>
    <div class="box box-primary">
        <a href="{{ route('admin.classes.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'PUT', 'enctype' => 'multipart/form-data', 'route' => ['admin.classes.update',$classes->id], 'id' => 'validation-form']) !!}
        <div id="show">
            <div id="box" class="box-body" style = "padding-top:40px;">
         
                <input type="hidden" name="class_id" value="{{$classes->id}}">
                <div class="form-group col-md-6 @if($errors->has('name')) has-error @endif" style ="float:left">
                    {!! Form::label('name', 'Class Name*', ['class' => 'control-label']) !!}
                   <input type="text" name="name" value="{{$classes->name}}"  required="required" placeholder="Enter Class Name" class="form-control" />
                </div>
                <input type="hidden" name="section_id" value="{{$classes->id}}">
                <div class="form-group col-md-6 @if($errors->has('description')) has-error @endif" style ="float:left">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                     <input type="text" name="description" value="{{$classes->description}}" required="required" placeholder="Enter Description" class="form-control" />
                </div>
            </div>  
            <button type="submit"  class="btn btn-success col-md-12" > Update</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
    
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

