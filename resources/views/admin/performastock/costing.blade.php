@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Lc Costing</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Costing</h3>
            <a href="{{ route('admin.performastock.index') }}" class="btn btn-success pull-right">Back</a>
        </div>

        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'id' => 'validation-form', 'route' => ['admin.performastock.costing', $performaStockModel->id]]) !!}
            <div class="box-body">
                {!! Form::hidden('pf_stock_id',  $performaStockModel->id, ['class' => 'form-control', 'placeholder' => '']) !!}

                <div class="lcbank_col">
                    <div class="col-xs-6 form-group @if($errors->has('insurance')) has-error @endif">
                        {!! Form::label('insurance', 'Insurance*', ['class' => 'control-label']) !!}
                        {!! Form::text('insurance', old('insurance'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('insurance'))
                            <span class="help-block">
                            {{ $errors->first('insurance') }}
                        </span>
                        @endif
                    </div>
                <div class="col-xs-6 form-group @if($errors->has('freight')) has-error @endif">
                    {!! Form::label('freight', 'Freight*', ['class' => 'control-label']) !!}
                    {!! Form::text('freight', old('freight'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    @if($errors->has('freight'))
                        <span class="help-block">
                            {{ $errors->first('freight') }}
                        </span>
                    @endif
                </div>
                </div>
                <div class="lcbank_col">
                <div class="col-xs-6 form-group @if($errors->has('profit_investment')) has-error @endif">
                    {!! Form::label('profit_investment', 'Profit Investment*', ['class' => 'control-label']) !!}
                    {!! Form::text('profit_investment', old('profit_investment'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    @if($errors->has('profit_investment'))
                        <span class="help-block">
                            {{ $errors->first('profit_investment') }}
                        </span>
                    @endif
                </div>
                <div class="col-xs-6 form-group @if($errors->has('murahba_profit')) has-error @endif">
                    {!! Form::label('murahba_profit', 'Murahba Profit*', ['class' => 'control-label']) !!}
                    {!! Form::text('murahba_profit', old('murahba_profit'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    @if($errors->has('murahba_profit'))
                        <span class="help-block">
                            {{ $errors->first('murahba_profit') }}
                        </span>
                    @endif
                </div>
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title">Calculate Duties</h3>
                </div>
                <div class="lcbank_col">
                    <div class="col-xs-6 form-group @if($errors->has('cf_value')) has-error @endif">
                        {!! Form::label('cf_value', 'C&F Value*', ['class' => 'control-label']) !!}
                        {!! Form::text('cf_value', old('cf_value'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('cf_value'))
                            <span class="help-block">
                            {{ $errors->first('cf_value') }}
                        </span>
                        @endif
                    </div>
                    <div class="col-xs-6 form-group @if($errors->has('c_insurance')) has-error @endif">
                        {!! Form::label('c_insurance', 'Insurance*', ['class' => 'control-label']) !!}
                        {!! Form::text('c_insurance', old('c_insurance'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('c_insurance'))
                            <span class="help-block">
                            {{ $errors->first('c_insurance') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="lcbank_col">
                    <div class="col-xs-6 form-group @if($errors->has('cs_duty')) has-error @endif">
                        {!! Form::label('cs_duty', 'Custom Duty*', ['class' => 'control-label']) !!}
                        {!! Form::text('cs_duty', old('cs_duty'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('cs_duty'))
                            <span class="help-block">
                            {{ $errors->first('cs_duty') }}
                        </span>
                        @endif
                    </div>
                    <div class="col-xs-6 form-group @if($errors->has('ad_duty')) has-error @endif">
                        {!! Form::label('ad_duty', 'Additional Duty*', ['class' => 'control-label']) !!}
                        {!! Form::text('ad_duty', old('ad_duty'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('ad_duty'))
                            <span class="help-block">
                            {{ $errors->first('ad_duty') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="lcbank_col">
                    <div class="col-xs-6 form-group @if($errors->has('sal_tax')) has-error @endif">
                        {!! Form::label('sal_tax', 'Sales Tax*', ['class' => 'control-label']) !!}
                        {!! Form::text('sal_tax', old('sal_tax'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('sal_tax'))
                            <span class="help-block">
                            {{ $errors->first('sal_tax') }}
                        </span>
                        @endif
                    </div>
                    <div class="col-xs-6 form-group @if($errors->has('ad_sale')) has-error @endif">
                        {!! Form::label('ad_sale', 'Additional Sales*', ['class' => 'control-label']) !!}
                        {!! Form::text('ad_sale', old('ad_sale'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('ad_sale'))
                            <span class="help-block">
                            {{ $errors->first('ad_sale') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="lcbank_col">
                    <div class="col-xs-6 form-group @if($errors->has('incom_tax')) has-error @endif">
                        {!! Form::label('incom_tax', 'Income Tax*', ['class' => 'control-label']) !!}
                        {!! Form::text('incom_tax', old('incom_tax'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('incom_tax'))
                            <span class="help-block">
                            {{ $errors->first('incom_tax') }}
                        </span>
                        @endif
                    </div>
                    <div class="col-xs-6 form-group @if($errors->has('se_duty')) has-error @endif">
                        {!! Form::label('se_duty', 'Sindh Excise Duty*', ['class' => 'control-label']) !!}
                        {!! Form::text('se_duty', old('se_duty'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('se_duty'))
                            <span class="help-block">
                            {{ $errors->first('se_duty') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="lcbank_col">
                    <div class="col-xs-6 form-group @if($errors->has('cc_cif')) has-error @endif">
                        {!! Form::label('cc_cif', 'Clearing Charges on CIF*', ['class' => 'control-label']) !!}
                        {!! Form::text('cc_cif', old('cc_cif'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('cc_cif'))
                            <span class="help-block">
                            {{ $errors->first('cc_cif') }}
                        </span>
                        @endif
                    </div>
                    <div class="col-xs-6 form-group @if($errors->has('mv_kh_lh')) has-error @endif">
                        {!! Form::label('mv_kh_lh', 'Movement From KHI to LHR on CIF Value*', ['class' => 'control-label']) !!}
                        {!! Form::text('mv_kh_lh', old('mv_kh_lh'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('mv_kh_lh'))
                            <span class="help-block">
                            {{ $errors->first('mv_kh_lh') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="lcbank_col">
                    <div class="col-xs-6 form-group @if($errors->has('fr_fob')) has-error @endif">
                        {!! Form::label('fr_fob', 'Freight on FOB Value*', ['class' => 'control-label']) !!}
                        {!! Form::text('fr_fob', old('fr_fob'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('fr_fob'))
                            <span class="help-block">
                            {{ $errors->first('fr_fob') }}
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
        {!! Form::close() !!}

    </div>
@stop

@section('javascript')
    <script src="{{ url('js/admin/lcproceed/lcproceed_create_modify.js') }}" type="text/javascript"></script>
@endsection

<style>
    .lcbank_col {
        float: left;
        width: 100%;
    }
</style>

