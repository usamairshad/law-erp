

<div class="row">
    <div class="col-xs-12 form-group">
        <div class="sales_ro row">
            <div class="col-xs-4 form-group @if($errors->has('sal_no')) has-error @endif">
                {!! Form::label('sal_no', 'Voucher Code *', ['class' => 'control-label']) !!}
                <input id="sal_no" class="form-control" type="text" name="sal_no" value="????" readonly="readonly" />
            </div>
            <div class="col-xs-4 form-group @if($errors->has('pay_date')) has-error @endif">
                {!! Form::label('pay_date', 'Payment Date', ['class' => 'control-label']) !!}
                {!! Form::text('pay_date', old('pay_date'), ['class' => 'form-control', 'placeholder' => '' ]) !!}
                @if($errors->has('pay_date'))
                    <span class="help-block">
				{{ $errors->first('pay_date') }}
				</span>
                @endif
            </div>
            <div class="col-xs-4 form-group @if($errors->has('valid_upto')) has-error @endif">
                {!! Form::label('valid_upto', 'Due Date', ['class' => 'control-label']) !!}
                {!! Form::text('valid_upto', old('valid_upto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                @if($errors->has('valid_upto'))
                    <span class="help-block">
				{{ $errors->first('valid_upto') }}
				</span>
                @endif
            </d iv>
        </div>
        <div class="sales_ro row">
            <div class="col-xs-4 form-group @if($errors->has('sup_id')) has-error @endif">
                {!! Form::label('sup_id', 'Brands *', ['class' => 'control-label']) !!}
                <select id="sup_id" name="sup_id" class="form-control select2">
                    @if ($SuppliersModel->count())
                    <option value="">Select Brands</option>
                    @foreach($SuppliersModel as $Supplier)
                    <option value="{{ $Supplier->id }}">{{ $Supplier->sup_name }}</option>
                    @endforeach
                    @endif
                </select>
                @if($errors->has('sup_id'))
                    <span class="help-block">
				{{ $errors->first('sup_id') }}
				</span>
                @endif
            </div>
            <div class="col-xs-4 form-group @if($errors->has('branch_id')) has-error @endif">
                {!! Form::label('branch_id', 'Branche', ['class' => 'control-label']) !!}
                <select id="branch_id" name="branch_id" class="form-control select2">
                    @if ($Branches->count())
                        <option value="">Select Branches</option>
                        @foreach($Branches as $Branche)
                            <option value="{{ $Branche->id }}">{{ $Branche->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if($errors->has('branch_id'))
                    <span class="help-block">
				{{ $errors->first('branch_id') }}
				</span>
                @endif
            </div>

        </div>
        <div id="productmodel_col"></div>
        <div class="sales_ro row">
            <div class="hidden">
                <ul class="example-template" style="float: left; width: 100%; margin-bottom:8px; padding: 0px;">
                    <li class="col-xs-6">
                        {!!  Form::select('lc_status', [
                            '' => 'select status',
                            '1' => 'Open',
                            '2' => 'In Process',
                            '3' => 'In Bank',
                            '4' => 'Complete',
                            '0' => 'Pending',
                            '5' => 'Cancel'],
                            null,
                            ['class' => 'form-control'])
                        !!}
                        @if($errors->has('products_model'))
                            <span class="help-block">
						{{ $errors->first('products_model') }}
						</span>
                        @endif
                    </li>
                    <li class="col-xs-4">
                        {!! Form::text('amount', old('amount'), ['id' => 'amount','class' => 'form-control', 'placeholder' => '']) !!}
                    </li>
                    <li class="pull-right" style="margin-right: 0px;">
                        <button class="del btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                    </li>
                </ul>
            </div>
            <div class="edit-area col-xs-12">


                <div class="controls">
                    <div class="controls_inn" style="float: left; width: 100%; border: 1px solid rgb(221, 221, 221); padding: 4px; margin-bottom: 10px;">
                    <label class="control-label" style="float: left; margin-top: 4px;">Sales Detail</label>
                    <span class="add btn pull-right btn-sm btn-flat btn-primary" style="margin-bottom: 0px;">
					+ Add Row
					</span>
                </div>


                    <ul class="heading_ul list-style-none text-center" style="float: left; width: 100%; margin-bottom:8px; padding: 0px; display: none;">
                        <li class="col-xs-6">
                            {!! Form::label('lc_status', 'Status*', ['class' => 'control-label']) !!}
                        </li>
                        <li class="col-xs-4">
                            {!! Form::label('shipping_charges', 'Amount', ['class' => 'control-label']) !!}
                        </li>
                        <li class="pull-right" style="margin-right: 0px;">
                            {!! Form::label('shipping_charges', 'Action', ['class' => 'control-label']) !!}
                        </li>
                    </ul>
                    {{--<span class="rem"><i class="fa fa-trash"></i></span>--}}
                </div>
            </div>
        </div>
    </div>
</div>

