@extends('layouts.app')

@section('stylesheet')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/select2/dist/css/select2.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">


    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">


    <style>
        .example-template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Lc Payment Voucher</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            {{--<h3 class="box-title">Create Sale</h3>--}}
            {{--<a href="{{ route('admin.sales.index') }}" class="btn btn-success pull-right">Back</a>--}}
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.performastock.sale_voucher.create'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('admin.performastock.sale_voucher.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
        {{--@include('admin.entries.voucher.sale_voucher.sales_template')--}}
    </div>
@stop

@section('javascript')

    <script>

        $( document ).ready(function() {
            $('.select2').select2();
        });



        (function($) {
            "use strict";

            var itemTemplate = $('.example-template').detach(),
                editArea = $('.edit-area'),
                itemNumber = 1;

            $(document).on('click', '.edit-area .add', function(event) {

                $(".heading_ul").show();

                var item = itemTemplate.clone();
                item.find('[name]').attr('name', function() {
                    return $(this).attr('name') + '_' + itemNumber;
                });
                item.find('[id]').attr('id', function() {
                    return $(this).attr('id') + '_' + itemNumber;
                });
                ++itemNumber;
                item.appendTo(editArea);
            });

            $(document).on('click', '.edit-area .rem', function(event) {
                editArea.children('.example-template').last().remove();
            });

            $(document).on('click', '.edit-area .del', function(event) {
                var target = $(event.target),
                    row = target.closest('.example-template');
                row.remove();
            });
        }(jQuery));
    </script>




    <!-- date-range-picker -->
    <script src="{{ url('adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- bootstrap datepicker -->
    {{--<script src="{{ url('adminlte') }}/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>--}}

    <script src="{{ url('adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Select2 -->
    <script src="{{ url('adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('js/admin/performastock/vouche_create_modify.js') }}" type="text/javascript"></script>
@endsection

