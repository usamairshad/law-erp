@extends('layouts.app')
@section('content')
    <h3 class="page-title">Performa Stock</h3>
    {!! Form::model($Performastock,['method'=>'PUT','id'=>'validation-form','route'=>['admin.performastock.update',$Performastock->id]]) !!}
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_edit')
        </div>
        <div class="panel-body">
            <div class="row perofarmastock_col">
                <div class="col-xs-6 form-group @if($errors->has('lc_no')) has-error @endif">
                    {!! Form::label('lc_no', 'LC Number*', ['class' => 'control-label']) !!}

                    {{--{!! Form::select('lc_no', array('' => 'Select LC Number') + Config::get('constants.lc_no'), old('lc_no'), ['class' => 'form-control']) !!}--}}

                    <select name="lc_no" class="form-control">
                        @if ($LcBankModel->count())
                            <option value="">Select LC Number</option>
                            @foreach($LcBankModel as $role)
                                <option value="{{ $role->id }}" {{ $role->id == $Performastock->lc_no ? 'selected':''}}>{{ $role->lc_no }}</option>
                            @endforeach
                        @endif
                    </select>
                    @if($errors->has('lc_no'))
                        <span class="help-block">
				{{ $errors->first('lc_no') }}
				</span>
                    @endif
                </div>
                <div class="col-xs-6 form-group @if($errors->has('product_name')) has-error @endif">
                    {!! Form::label('product_name', 'Product Name / Part Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('product_name', old('product_name'), ['class' => 'form-control']) !!}
                    @if($errors->has('product_name'))
                        <span class="help-block">
				{{ $errors->first('product_name') }}
				</span>
                    @endif
                </div>
            </div>
            <div class="row perofarmastock_col">
                <div class="col-xs-6 form-group @if($errors->has('product_model')) has-error @endif">
                    {!! Form::label('product_model', 'Product Model*', ['class' => 'control-label']) !!}
                    {!! Form::text('product_model', old('product_model'), ['class' => 'form-control']) !!}
                    @if($errors->has('product_model'))
                        <span class="help-block">
				{{ $errors->first('product_model') }}
				</span>
                    @endif
                </div>
                <div class="col-xs-6 form-group @if($errors->has('total_qty')) has-error @endif">
                    {!! Form::label('total_qty', 'Total Quantity*', ['class' => 'control-label']) !!}
                    {!! Form::text('total_qty', old('total_qty'), ['min' => 1, 'class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('total_qty'))
                        <p class="help-block">
                            {{ $errors->first('total_qty') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row perofarmastock_col">
                <div class="col-xs-6 form-group @if($errors->has('amount')) has-error @endif">
                    {!! Form::label('amount', 'Amount*', ['class' => 'control-label']) !!}
                    {!! Form::text('amount', old('amount'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('amount'))
                        <p class="help-block">
                            {{ $errors->first('amount') }}
                        </p>
                    @endif
                </div>
            </div>
            {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('javascript')
    <script src="{{ url('js/admin/performastock/performastock_update_modify.js') }}" type="text/javascript"></script>
@endsection