@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Performa Stock</h3>

    <p>
        <a href="{{ route('admin.performastock.create') }}" class="btn btn-success">@lang('global.app_add_new')</a>
    </p>


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($Performastock) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        {{--<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>--}}
                        {{--<th>Lc No</th>--}}
                        <th>Product Name</th>
                        {{--<th>Product Model</th>--}}
                        <th>Total Qty</th>
                        <th>Bal. Qty</th>
                        <th>Amount($)</th>
                        <th>Total Amount</th>
                        <th>Rate($)</th>
                        <th>Amount Rs.</th>
                        <th>Less(30%) Margin</th>
                        <th>Total Morabaha</th>
                        <th>Morabaha Profit</th>
                        <th>Net Morabaha</th>
                        <th>Cost Per Unit</th>
                        <th>Balance Murabaha</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($Performastock) > 0)
                        @foreach ($Performastock as $Performastocks)
                            <tr data-entry-id="{{ $Performastocks->perf_id }}">
                                {{--<td></td>--}}
                                {{--<td>{{ $Performastocks->lc_no}}</td>--}}
                                <td>{{ $Performastocks->product_name }}  {{ $Performastocks->product_model}}</td>
                                {{--<td>{{ $Performastocks->product_model}}</td>--}}
                                <td>{{ $Performastocks->total_qty }}</td>
                                <td>{{ $Performastocks->balance_qty}}</td>
                                <td>{{ $Performastocks->amount }}</td>
                                <td>{{ $Performastocks->total_amount}}</td>
                                <td>{{ $Performastocks->dollar_rate}}</td>
                                <td>{{ $Performastocks->amount_rs}}</td>
                                <td>{{ $Performastocks->margin}}</td>
                                <td>{{ $Performastocks->total_murahba}}</td>
                                <td>{{ $Performastocks->murahba_profit}}</td>
                                <td>{{ $Performastocks->net_murahba}}</td>
                                <td>{{ $Performastocks->cost_per_unit}}</td>
                                <td>{{ $Performastocks->balance_murahba}}</td>

                                <td>
                                    <a href="{{ route('admin.performastock.costing',[$Performastocks->perf_id]) }}" class="btn btn-xs btn-success">LcCosting</a>

                                    <a href="{{ route('admin.performastock.edit',[$Performastocks->perf_id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>

                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.performastock.destroy', $Performastocks->perf_id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}

                                </td>
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.performastock.mass_destroy') }}';
    </script>
@endsection