@extends('layouts.app')
@section('content')
    <h3 class="page-title">Performa Stock</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.performastock.store'], 'id' => 'validation-form']) !!}
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_create')
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    <div class="perofarmastock_col">
                        <div class="@if($errors->has('lc_no')) has-error @endif">
                            {!! Form::label('lc_no', 'LC Number*', ['class' => 'control-label']) !!}
                            <select name="lc_no" class="form-control">
                                @if ($LcBankModel->count())
                                    <option value="">Select LC Number</option>
                                    @foreach($LcBankModel as $role)
                                        <option value="{{ $role->id }}">{{ $role->lc_no }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has('lc_no'))
                                <span class="help-block">
						{{ $errors->first('lc_no') }}
						</span>
                            @endif
                        </div>
                    </div>
                    <div class="perofarmastock_col">
                        <div class="@if($errors->has('product_name')) has-error @endif">
                            {!! Form::label('product_name', 'Product Name / Part Name*', ['class' => 'control-label']) !!}
                            {!! Form::text('product_name', old('product_name'), ['class' => 'form-control']) !!}
                            @if($errors->has('product_name'))
                                <span class="help-block">
						{{ $errors->first('product_name') }}
						</span>
                            @endif
                        </div>
                    </div>
                    <div class="perofarmastock_col">
                        <div class="@if($errors->has('product_model')) has-error @endif">
                            {!! Form::label('product_model', 'Product Model*', ['class' => 'control-label']) !!}
                            {!! Form::text('product_model', old('product_model'), ['class' => 'form-control']) !!}
                            @if($errors->has('product_model'))
                                <span class="help-block">
						{{ $errors->first('product_model') }}
						</span>
                            @endif
                        </div>
                    </div>
                    <div class="perofarmastock_col">
                        <div class="@if($errors->has('quantity')) has-error @endif">
                            {!! Form::label('quantity', 'Quantity*', ['class' => 'control-label']) !!}
                            {{--{!! Form::text('quantity', old('quantity'), ['class' => 'form-control']) !!}--}}
                            {!! Form::text('quantity', old('quantity'), ['min' => 1, 'class' => 'form-control']) !!}

                            @if($errors->has('quantity'))
                                <p class="help-block">
                                    {{ $errors->first('quantity') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="perofarmastock_col @if($errors->has('amount')) has-error @endif">
                            {!! Form::label('amount', 'Amount*', ['class' => 'control-label']) !!}
                            {!! Form::text('amount', old('amount'), ['class' => 'form-control']) !!}

                            @if($errors->has('amount'))
                                <p class="help-block">
                                    {{ $errors->first('amount') }}
                                </p>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
    {!! Form::close() !!}
@stop
@section('javascript')
    <script src="{{ url('js/admin/performastock/performastock_create_modify.js') }}" type="text/javascript"></script>
@endsection