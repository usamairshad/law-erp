@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Add Group</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <a href="{{  route('admin.class-group.index') }}"  class="btn btn-success pull-right">Back
                        </a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.class-group.store'], 'id' => 'validation-form']) !!}
            <div class="box-body">

              <div class="row">
                <div class="col-md-12">
                  <label>Select Group</label>
                  <select name="group" class="form-control" id="group"> 
                    <option selected="selected" disabled="disabled">--Select---</option>
                    <option value="program">Program Group</option>
                    <option value="class">Class Group</option>
                    <option value="course">Course Group</option>
                  </select/>
                </div>
              </div>
              <!--*************START*********** For Program Group *******************************-->
              <div class="row" >
                <div class="col-md-12">
                    <div id="program" >
                      <h2>Add Program Course Group</h2>
                      <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                          {!! Form::label('board_id', 'Boards*', ['class' => 'control-label']) !!}
                              <select id="board" class="form-control select2 boards" name="board_id" required="required" onselect="loadboardPrograms()">
                                  <option selected="selected" disabled="disabled">---Select----</option>
                                  @foreach($boards as $key => $board)
                                  <option value="{{$board['id']}}">{{$board['name']}}</option>
                                  @endforeach
                              </select>
                      </div>
                      <div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif">
                          {!! Form::label('programs', 'Programs*', ['class' => 'control-label']) !!}

                          <select class="form-control select2 programs" name="program_id" id="programs" required="required"></select>

                      </div>

                      <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                          {!! Form::label('class_id', 'Program Group*', ['class' => 'control-label']) !!}
                              <select class="form-control select2 classes" name="program_group_id" id="classes" required="required"></select>
                      </div>
                      <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                          {!! Form::label('course_id', 'Select Course*', ['class' => 'control-label']) !!}
                            <select  class="form-control select2 courses" multiple="multiple" name="course_id" required="required">
                                <option selected="selected" disabled="disabled">---Select----</option>
                                @foreach($courses as $key => $course)
                                <option value="{{$course['id']}}">{{$course['name']}}</option>
                                @endforeach
                            </select>
                      </div>
                  </div>   
                  <div id="class">
                    <h2>Add Class Course Group</h2>
                    <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                        {!! Form::label('board_id', 'Boards*', ['class' => 'control-label']) !!}
                            <select id="board" class="form-control select2 boards" name="board_id" required="required">
                                <option selected="selected" disabled="disabled">---Select----</option>
                                @foreach($boards as $key => $board)
                                <option value="{{$board['id']}}">{{$board['name']}}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif">
                        {!! Form::label('programs', 'Programs*', ['class' => 'control-label']) !!}

                        <select class="form-control select2 programs" name="program_id" id="programs" required="required"></select>

                    </div>


                    <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                        {!! Form::label('class_id', 'Program Group*', ['class' => 'control-label']) !!}
                            <select class="form-control select2 classes" name="program_group_id" id="classes" required="required"></select>
                    </div>
                    <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('course_group_id', 'Course Group*', ['class' => 'control-label']) !!}

                        <select class="form-control select2 courses" name="course_group_id" id="courses" required="required"></select>
                    </div>
                    <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                        {!! Form::label('course_id', 'Select Course*', ['class' => 'control-label']) !!}
                            <select  class="form-control" name="course_id" required="required">
                                <option selected="selected" disabled="disabled">---Select----</option>
                                @foreach($courses as $key => $course)
                                <option value="{{$course['id']}}">{{$course['name']}}</option>
                                @endforeach
                            </select>
                    </div>
                  </div>
                </div>              
              </div>
               <!--*************END*********** For Program Group *******************************-->

              <!--*************START*********** For Class Group *******************************-->
             
               <!--*************END*********** For Class Group *******************************-->


            </div>   
            <button id="btn"  class="btn btn-success col-md-12" > Save</button>

        <!-- /.box-body -->
       
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script>
        function loadboardPrograms(){
          var board_id =$("select[name*='board_id']").val();
          console.log(board_id)
        }

        $(function(){
            $('#programs').change(function(){
               $("#classes option").remove();
               var id = $(this).val();
               var board_id =$("select[name*='board_id']").val();

               $.ajax({
                  url : '{{ route( 'admin.load-board-program-program-group' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "program_id": id,
                    "board_id":board_id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {     
              
                        $('#classes').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        
                        $.each( result, function(k, v) {
                           $('#classes').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
        $(function(){
            $('#classes').change(function(){
               $("#courses option").remove();;
               // var id = $(this).val();
               var program_group_id =$("select[name*='program_group_id']").val();
         
               $.ajax({
                  url : '{{ route( 'admin.load-program-group-courses-group' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",

                    "program_group_id":program_group_id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {     
                 
                        $('#courses').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        
                        $.each( result, function(k, v) {
                           $('#courses').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
    <script type="text/javascript">
       $( document ).ready(function() {
         $('#program').hide();
         $('#class').hide();

        $("#group").change(function(){
          var group = $( this ).val();
          if(group == 'program')
          {
            $('#program').show();
            $('#class').hide();
            $('#programs').empty();

          }
          else if(group == 'class')
          {
            $('#class').show();
            $('#program').hide();
            $('#programs').empty();
          }
        });
      });
    </script>
    <script type="text/javascript">
   
    
         $(".boards").select2({

            });
    </script>
    <script type="text/javascript">

        function selectRefresh() {
            $(".programs").select2({

            });
        }
        function selectRefresh() {
            $(".classes").select2({

            });
        }
        function selectRefresh() {
            $(".courses").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
    <script type="text/javascript">

    // function showBranch() {
    //     var data= $('input[name="board_name"]').val();

    //     if(data)
    //     {
    //          $('#show').show();
    //          $('#btn').hide(); 
    //     }
    // }
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>

@endsection

