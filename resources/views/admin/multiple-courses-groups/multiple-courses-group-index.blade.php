@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Boards</h1>
    </section>
@stop

@section('content')
    <example></example>
@stop

@section('javascript') 

@endsection