@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Classes</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <a  href="{{route('admin.multiple-courses-groups.create')}}" class="btn btn-success pull-right">Create Program Group Courses</a>
            <a  href="{{route('admin.class-group-course-create')}}" class="btn btn-success pull-right">Create Class Group Courses</a>
            <a  href="{{route('admin.course-group-course-create')}}" class="btn btn-success pull-right">Create Course Group Course</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection