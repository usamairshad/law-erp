@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
.form-group{
    float:left;
}
        </style>
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Board Program Class</h3>
    <div class="box box-primary">

        <!-- /.box-header -->
        <!-- form start -->
         {!! Form::open(['route' => 'admin.branch-board-program-store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
        <div id="show">
            <div id="box" class="box-body" style = "padding-top:40px;">
            <input type="hidden" name="branch_id" value="{{$branch_id}}">
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif" style = "float:left;">
                    {!! Form::label('board_id', 'Boards*', ['class' => 'control-label']) !!}
                        <select required="required" class="form-control" name="board_id" id="board_select">
                            <option value="0" selected>Select Board</option>

                            @foreach($boards as $board)
                            <option value="{{$board['id']}}">{{$board['name']}}</option>
                            @endforeach

                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif" style = "float:left;">
                    {!! Form::label('programs', 'Programs*', ['class' => 'control-label']) !!}
                    <select class="form-control select2 programs" name="programs[]" multiple="multiple" required="required"  id="program_select"></select>
                </div>              
            </div> 
          
            <button  class="btn btn-success col-md-12" > Save</button>
            
        </div>

        <!-- /.box-body -->
       
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById('btn').setAttribute("disabled",null);
            $( document ).change(function(){
                var e = document.getElementById("classes");
                var strUser = e.value;
                if (strUser != "") {
                document.getElementById('btn').removeAttribute("disabled");
                } else {
                    document.getElementById('btn').setAttribute("disabled", null);
                }
            })
            
        });
         $(".boards").select2({

            });
    </script>
    <script type="text/javascript">
        
        function selectRefresh() {
            $(".programs").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
    <script type="text/javascript">


    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" style="height:120px"><hr style="border-top:1px solid #222d32"><div class="form-group col-md-3"><select id="board" class="form-control select2 boards" name="board_id[]">@foreach($boards as $key => $board)<option value="{{$board['id']}}">{{$board['name']}}</option>@endforeach</select></div><div class="form-group col-md-3"><select class="form-control select2 programs" name="programs" id="programs"></select></div><br> <div class="form-group col-md-2"><button style="margin-top: -27px;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');
            $('.select2').select2(); 

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    // function showBranch() {
    //     var data= $('input[name="board_name"]').val();

    //     if(data)
    //     {
    //          $('#show').show();
    //          $('#btn').hide(); 
    //     }
    // }
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
    <script>
        $(function(){
            $('#board').change(function(){
               $("#programs option").remove();;
               var id = $(this).val();

               $.ajax({
                  url : '{{ route( 'admin.board-loadPrograms' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {     
                    
                        $.each( result, function(k, v) {
                            $('#programs').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });

    $("#board_select").on("change", function () {
    $board_id = $(this).find(":selected").val();
    console.log($board_id)
    getProgramByBoard($board_id);

    });    
        function getProgramByBoard($board_id) {

            $.ajax({
                 url: "{{url('admin/board/loadPrograms')}}/" + $board_id,
                success: function (res) {
                    console.log(res)
                    $html = "";
                    $('#program_select').html("");

                    res.forEach(data => {

                        $html += `<option value="${data[0].id}">${data[0].name}</option>`;
                    });

                    $('#program_select').append($html);

                }
            });

        }
    </script>
@endsection

