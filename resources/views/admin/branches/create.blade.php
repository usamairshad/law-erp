@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')
<style>
.form-control
{
    float:left;
}
    </style>
@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Branches</h3>
            <a href="{{ url()->previous() }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.branches.store'], 'id' => 'validation-form']) !!}
        <div class="box-body" style = "margin-top:40px;">
            <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'100', 'required']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>

            <div class="form-group col-md-3 @if($errors->has('city_id')) has-error @endif" style = "float:left;">
                {!! Form::label('city_id', 'City*', ['class' => 'control-label']) !!}
                {!! Form::select('city_id', $city->prepend('Select City', ''),old('city_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('city_id'))
                    <span class="help-block">
                        {{ $errors->first('city_id') }}
                    </span>
                @endif
            </div>

            <div class="form-group col-md-3 @if($errors->has('company_id')) has-error @endif"style = "float:left;" >
                {!! Form::label('company_id', 'Company', ['class' => 'control-label']) !!}
                <input class="form-control" type="text" readonly value="{{$company_name}}">
                <input type="hidden" name="company_id" value="{{$id}}">
            </div>
            <div class="form-group col-md-3 @if($errors->has('branch_code')) has-error @endif" style = "float:left;">
                {!! Form::label('branch_code', 'Branch Code*', ['class' => 'control-label']) !!}
                {!! Form::number('branch_code', old('branch_code'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'10','min'=>1, 'required']) !!}
                @if($errors->has('branch_code'))
                    <span class="help-block">
                        {{ $errors->first('branch_code') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('phone')) has-error @endif" style = "float:left;">
                {!! Form::label('phone', 'Phone*', ['class' => 'control-label']) !!}
                <input type="tel" data-mask="000-00000000" placeholder="0XX-XXXXXXX" name="phone" required="required" class="form-control home_phone" />
            </div>
            <div class="form-group col-md-3 @if($errors->has('fax')) has-error @endif" style = "float:left;">
                {!! Form::label('fax', 'Fax', ['class' => 'control-label']) !!}
                {!! Form::number('fax', old('fax'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'10','min'=>1,]) !!}
                @if($errors->has('fax'))
                    <span class="help-block">
                        {{ $errors->first('fax') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('cell')) has-error @endif" style = "float:left;">
                {!! Form::label('cell', 'Mobile Number*', ['class' => 'control-label']) !!}
                <input type="tel" data-mask="0000-0000000"  placeholder="03XX-XXXXXXX" name="cell" required="required" class="form-control phone" />
            </div>
            <div class="form-group col-md-3 @if($errors->has('address')) has-error @endif" style = "float:left;">
                {!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
                {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'255', 'required']) !!}
                @if($errors->has('address'))
                    <span class="help-block">
                        {{ $errors->first('address') }}
                    </span>
                @endif
            </div>
            <!--div class="col-xs-12 form-group">
        {!! Form::label('parent_id', 'Parent Group', ['class' => 'control-label']) !!}
        <select name="group_id" id="group_id" class="form-control select2" style="width: 100%;">
            <option value=""> Select a Parent Group </option>
                {!! $Groups !!}
        </select>
        <span id="parent_id_handler"></span>
        @if($errors->has('parent_id'))
                <span class="help-block">
                        {{ $errors->first('parent_id') }}
                </span>
        @endif
</div -->

            <!-- Common Ledger selector -->
  
        </div>
        {{-- <h3>Default ledgers for branch</h3>
        @foreach($common_ledgers_list as $common_ledger)
            <div class="col-md-4">
                <div class="form-group">
                    <label for="common_ledger">{{$common_ledger['name']}}</label>
                    <input type="checkbox" name="common_ledgers[]" value="{{$common_ledger['id']}}"  checked>
                </div>
            </div>
        @endforeach   --}}
        {{-- <h3 class="text-center">Default Group for branch</h3>
        @foreach($groups as $group)
            <div class="col-md-4">
                <div class="form-group">
                    <label for="group">{{$group['name']}}</label>
                    <input type="checkbox" name="groups[]" value="{{$group['id']}}"  checked>
                </div>
            </div>
        @endforeach  --}}
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.phone').mask('0000-0000000');
            $('.cnic').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');   
        })
    </script>
    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
@endsection

