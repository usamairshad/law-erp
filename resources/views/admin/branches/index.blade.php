@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">branches List</h3>
            @if(Gate::check('erp_branches_create'))
                <a href="{{ route('admin.branch-create',[$company_id]) }}" style = "float:right;" class="btn btn-success pull-right">Add New Branch</a>
                <a href="{{ route('admin.companies.index') }}" style = "float:right;margin-right:20px;" class="btn btn-success pull-right">Back</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Branches) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Branch Code</th>
                    <th>Assign Board Program</th>
                    <th>View Board Program</th>
                    <th>Address</th>
                    
                </tr>
                </thead>

                <tbody>

                @if (count($Branches) > 0)
                    @foreach ($Branches as $key => $Branche)
                        <tr data-entry-id="{{ $Branche->id }}">
                            <td>{{ $Branche->name }}</td>
                            <td>{{ $Branche->branch_code }}</td>
                            <td> 
                               <a href="{{ route('admin.assign_board_programs', [$Branche->id]) }}"
                                            class="btn btn-xs btn-info">Assign Board Program</a>                          
                            </td>
                                                        
                            <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-body">
                                      {!! Form::open(['route' => 'admin.branch-board-program-store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      <!-- <input type="hidden" name="branch_id" value="{{$Branche->id}}"> -->
                                       <input type="hidden" name="branch_id" id="training_id_input">
                                        <div class="form-group">
                                            {!! Form::label('board_id', 'Board*', ['class' => 'control-label']) !!}

                                            <select required="required" class="form-control" name="board_id" id="board_select">
                                                <option value="0" selected>Select Board</option>

                                                @foreach($boards as $board)
                                                <option value="{{$board['id']}}">{{$board['name']}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('programs', 'Programs*', ['class' => 'control-label']) !!}
                                            <select style="width: 100%" class="js-example-basic-multiple"
                                            name="program_id[]" id="program_select">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>           
                                </div>
                            </div>   
                            <td>

                                <a  data-id="{{$Branche->id}}" class="btn btn-xs btn-primary branch-board-program" data-toggle="modal" data-target="#exampleModal2{{$Branche->id}}"><span>View</span></a>
                                <a  data-id="" class="btn btn-xs btn-primary branch_detail" data-branch-detail="{{$Branche}}" data-city-name="{{$Branche->cities()->get()}}" data-toggle="modal" data-target="#branch_detail"><span>Branch Detail</span></a>

                                <div class="modal fade" id="exampleModal2{{$Branche->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-body">
                                            <h2>Assigned Board  Program</h2>
                                            <table class="table table-striped">
                                                <thead>
                                                    <th>Board</th>
                                                    <th >Program</th>

                                                </thead>
                                                <tbody class="body">
                                                </tbody>
                                            </table>
                                        </div>
                                      </div>           
                                    </div>
                                </div>
                            </td>


                            <td>{{ $Branche->address }}</td>

                            <td>
                                @if(Gate::check('erp_branches_edit'))
                                   {{--<a href="{{ route('admin.branches.edit',[$Branche->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>--}}
                                @endif

                                {{--@if(Gate::check('branches_destroy'))--}}
                                    {{--{!! Form::open(array(--}}
                                        {{--'style' => 'display: inline-block;',--}}
                                        {{--'method' => 'DELETE',--}}
                                        {{--'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",--}}
                                        {{--'route' => ['admin.branches.destroy', $Branche->id])) !!}--}}
                                    {{--{!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}--}}
                                    {{--{!! Form::close() !!}--}}
                                {{--@endif--}}
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    {{--modal--}}
    <div class="modal fade" id="branch_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div style="padding: 10px">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Name</th>
                            <th>City</th>
                        </tr>
                        <tr>
                            <td class="name"></td>
                            <td class="city"></td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <th>Fax</th>

                        </tr>
                        <tr>
                            <td class="phone"></td>
                            <td class="fax"></td>
                        </tr>
                        <tr>
                            <th>Mobile Number</th>
                            <th>Address</th>
                        </tr>
                        <tr>
                            <td class="cell"></td>
                            <td class="address"></td>
                        </tr>
                        <tr>
                            <th>Branch Code</th>
                        </tr>
                        <tr>
                            <td class="branch_code"></td>
                        </tr>
                    </table>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    {{--modal end--}}
@stop

@section('javascript') 
    <script>
        
        $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});

        $('.branch_detail').on('click',function(){


            var name =  $(this).data('branch-detail').name;
            var branch_code =  $(this).data('branch-detail').branch_code;
            var phone =  $(this).data('branch-detail').phone;
            var fax =  $(this).data('branch-detail').fax;
            var mobile_number =  $(this).data('branch-detail').cell;
            var address =  $(this).data('branch-detail').address;
            var city_name =  $(this).data('city-name')[0].name;

            $('.name').text(name);
            $('.city').text(city_name);
            $('.branch_code').text(branch_code);
            $('.phone').text(phone);
            $('.fax').text(fax);
            $('.cell').text(mobile_number);
            $('.address').text(address);
        });
        $(document).ready(function() {
          selectRefresh();
        });
        
        function selectRefresh() {
            $(".programs").select2({

            });
             $(".select2")[2].style.width = "100%";
        }

        $(document).ready(function(){
            $('.datatable').DataTable()
        });

        $(function(){
            $(".branch-board-program").on('click', function () {
               var id=  $(this).data('id');
               $.ajax({
                  url : '{{ route( 'admin.load-branch-board-programs' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {
                    var res='';
                    $.each (result, function (key, value) {
                    res +=
                    '<tr>'+
                        '<td>'+value.board_id+'</td>'+
                        '<td>'+value.program_id+'</td>'+
                   '</tr>';

                    });

                    $('.body').html(res);
                  },
                  error: function()
                 {
                     // alert('error...');
                 }
               });
            });
        });


        $("#board_select").on("change", function () {
            $board_id = $(this).find(":selected").val();
            console.log($board_id)
            getProgramByBoard($board_id);

        });

        $('.assign-modal-anchor').on("click", function () {

            $training_id = $(this).data("training-id");

            $("#training_id_input").val($training_id);

        });

        function getProgramByBoard($board_id) {

            $.ajax({
                 url: "{{url('admin/board/loadPrograms')}}/" + $board_id,
                success: function (res) {
                    console.log(res)
                    $html = "";
                    $('#program_select').html("");

                    res.forEach(data => {

                        $html += `<option value="${data[0].id}">${data[0].name}</option>`;
                    });

                    $('#program_select').append($html);

                }
            });

        }

    </script>
@endsection