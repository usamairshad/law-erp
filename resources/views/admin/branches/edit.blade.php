@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Branches</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Branch</h3>
            <a href="{{ route('admin.branches.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->


        {!! Form::model($Branche, ['method' => 'post', 'id' => 'validation-form', 'route' => ['admin.branches.update', $Branche->id]]) !!}
        <div class="box-body">
            <div class="form-group col-md-4 @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'10', 'required']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>

            <div class="form-group col-md-3 @if($errors->has('city_id')) has-error @endif">
                {!! Form::label('city_id', 'City', ['class' => 'control-label']) !!}
                {!! Form::select('city_id', $city->prepend('Select City', ''),old('city_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('city_id'))
                    <span class="help-block">
                        {{ $errors->first('city_id') }}
                    </span>
                @endif
            </div>

            <div class="form-group col-md-3 @if($errors->has('country_id')) has-error @endif">
                {!! Form::label('country_id', 'Country*', ['class' => 'control-label']) !!}
                {!! Form::select('country_id', $country->prepend('Select Country', ''),old('country_id'), ['class' => 'form-control']) !!}
                @if($errors->has('country_id'))
                    <span class="help-block">
                        {{ $errors->first('country_id') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('company_id')) has-error @endif">
                {!! Form::label('company_id', 'Company*', ['class' => 'control-label']) !!}
                {!! Form::select('company_id', $company->prepend('Select Company', ''),old('company_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('company_id'))
                    <span class="help-block">
                        {{ $errors->first('company_id') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('branch_code')) has-error @endif">
                {!! Form::label('branch_code', 'Branch Code*', ['class' => 'control-label']) !!}
                {!! Form::number('branch_code', old('branch_code'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'10','min'=>1, 'required']) !!}
                @if($errors->has('branch_code'))
                    <span class="help-block">
                        {{ $errors->first('branch_code') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('phone')) has-error @endif">
                {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
                {!! Form::number('phone', old('phone'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'10','min'=>1]) !!}
                @if($errors->has('phone'))
                    <span class="help-block">
                        {{ $errors->first('branch_code') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('fax')) has-error @endif">
                {!! Form::label('fax', 'Fax', ['class' => 'control-label']) !!}
                {!! Form::number('fax', old('fax'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'10','min'=>1,]) !!}
                @if($errors->has('fax'))
                    <span class="help-block">
                        {{ $errors->first('fax') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('cell')) has-error @endif">
                {!! Form::label('cell', 'Mobile Number', ['class' => 'control-label']) !!}
                {!! Form::number('cell', old('cell'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'10','min'=>1,]) !!}
                @if($errors->has('cell'))
                    <span class="help-block">
                        {{ $errors->first('cell') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-12 @if($errors->has('address')) has-error @endif">
                {!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
                {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'255', 'required']) !!}
                @if($errors->has('address'))
                    <span class="help-block">
                        {{ $errors->first('address') }}
                    </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
@endsection