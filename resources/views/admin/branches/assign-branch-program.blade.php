@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Board Programs</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Boad Program</h3>
            <a href="{{ route('admin.companies.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.boards.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="form-group col-md-3 @if($errors->has('board_name')) has-error @endif">
                {!! Form::label('board_name', 'Board Name*', ['class' => 'control-label']) !!}
                {!! Form::text('board_name', old('board_name'), ['id' => 'board_name' ,'class' => 'form-control', 'placeholder' => 'Enter Board Name','maxlength'=>'50', 'required']) !!}
                @if($errors->has('board_name'))
                    <span class="help-block">
                        {{ $errors->first('board_name') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('board_name')) has-error @endif">
                {!! Form::label('board_name', 'Board Name*', ['class' => 'control-label']) !!}
                {!! Form::text('board_name', old('board_name'), ['id' => 'board_name' ,'class' => 'form-control', 'placeholder' => 'Enter Board Name','maxlength'=>'50', 'required']) !!}
                @if($errors->has('board_name'))
                    <span class="help-block">
                        {{ $errors->first('board_name') }}
                    </span>
                @endif
            </div>
            
        </div>
        <div id="show" style="display: none">
            <div id="box" class="box-body">
                <h2>Create Program</h2>
                <div class="form-group col-md-3 @if($errors->has('program_name')) has-error @endif">
                    {!! Form::label('program_name', 'Program Name*', ['class' => 'control-label']) !!}
                   <input type="text" name="program_name[0]"  required="required" placeholder="Enter Program Name" class="form-control" />
                </div>
                <div class="form-group col-md-6 @if($errors->has('description')) has-error @endif">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                     <input type="text" name="description[0]" required="required" placeholder="Enter Description" class="form-control" />
                </div>
                <div class="form-group col-md-1">
                    <button style="margin-top: 22px" type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
         <button id="btn" disabled onclick="showBranch()" class="btn btn-success col-md-12">Add Program</button>
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
    
    document.getElementById("board_name").addEventListener("keyup", function() {
        var nameInput = document.getElementById('board_name').value;
        if (nameInput != "") {
            document.getElementById('btn').removeAttribute("disabled");
        } else {
            document.getElementById('btn').setAttribute("disabled", null);
        }
    });
 

    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" style="height:120px"><hr style="border-top:1px solid #222d32"><div class="form-group col-md-3"><input required="required" type="text" name="program_name[]" placeholder="Enter Program Name" class="form-control" /></div><div class="form-group col-md-6"><input type="text" required="required" name="description[]" placeholder="Enter Description" class="form-control" /></div><br> <div class="form-group col-md-2"><button style="margin-top: -27px;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    function showBranch() {
        var data= $('input[name="board_name"]').val();

        if(data)
        {
             $('#show').show();
             $('#btn').hide(); 
        }
    }
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

