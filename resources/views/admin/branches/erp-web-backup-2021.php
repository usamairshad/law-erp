<?php

use App\Models\Admin\Groups;

//Route::get('/admin/script', function () {
//
//    $onlySoftDeleted = Groups::onlyTrashed()->get();
//    foreach ($onlySoftDeleted as $groupp) {
//        $groupp->forceDelete();
//    }
//    $onlySoftDeleted->forceDelete();
//    dd($onlySoftDeleted);
//    Groups::whereNotNull('deleted_at')->forceDelete();
//
//    $groups = Groups::all();
//    //dd($groups);
//    $str = "";
//    $i = 1;
//    foreach ($groups as $group) {
//        $str .= $i . "=>
//           [
//           " . "'id'=>" . "'" . $group->id . "',
//           " . "'name'=>" . "'" . $group->name . "',
//           " . "'number'=>" . "'" . $group->number . "',
//           " . "'code'=>" . "'" . $group->code . "',
//           " . "'level'=>" . "'" . $group->level . "',
//           " . "'parent_id'=>" . "'" . $group->parent_id . "',
//           " . "'account_type_id'=>" . "'" . $group->account_type_id . "',
//           " . "'status'=>" . "'" . $group->status . "',
//           " . "'created_by'=>" . "'" . $group->created_by . "',
//           " . "'updated_by'=>" . "'" . $group->updated_by . "',
//           " . "'deleted_by'=>" . "'" . $group->deleted_by . "',
//           " . "'created_at'=>" . "'" . $group->created_at . "',
//           " . "'updated_at'=>" . "'" . $group->updated_at . "',
//           " . "'deleted_at'=>" . "'" . $group->deleted_at . "',
//           " . "'parent_type'=>" . "'" . $group->parent_type . "',
//           ],
//           ";
//        $i++;
//    }
//    print_r("<pre>" . $str . "</pre>");
//
//
//    die;
//    // array_push($mainArray, $groupArray);
//
//});


Route::get('/', function () { return redirect('/admin/home'); });

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');

Route::get('change-password',function(){
            return view('profile.change-password');
        });
        Route::post('change_password','Auth\ChangePasswordController@changePassword')->name('change_password');


// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::post('admin/schedule/details',  'HRM\ScheduleController@getCandidateScore')->name('admin.schedule.details');
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
   
    ////////////TRAINING MODULE START////////////////////////
    Route::resource("training", "HRM\TrainingController");
    Route::get("training/users-by/role/{id}", "HRM\TrainingController@getUsersByRole");
    Route::get("assign/training/to-users", "HRM\TrainingController@assignTraining");
    Route::get("training/acceptance/form/{id}", "HRM\TrainingController@trainingAcceptanceForm");
    ///////////TRAINING MODULE END////
    
    Route::post('load-branch-board-programs', ['uses' => 'Admin\BranchesController@loadBranchBoardPrograms', 'as' => 'load-branch-board-programs']);

    Route::post('branch-board-program-store', ['uses' => 'Admin\BranchesController@branchBoardProgramStore', 'as' => 'branch-board-program-store']);
    Route::post('load-program-compulsory-courses', ['uses' => 'Admin\ProgramController@loadProgramCompulsoryCourses', 'as' => 'load-program-compulsory-courses']);
    Route::post('compulsory-courses-store', ['uses' => 'Admin\ProgramController@compulsoryCoursesStore', 'as' => 'compulsory-courses-store']);
    Route::post('load-academics-group-courses', ['uses' => 'Admin\AcademicsGroupController@loadAcademicsGroupCourses', 'as' => 'load-academics-group-courses']);
    Route::post('academics-groups-courses-store', ['uses' => 'Admin\AcademicsGroupController@academicsGroupsCoursesStore', 'as' => 'academics-groups-courses-store']);
    Route::get('academics-groups-courses-create', ['uses' => 'Admin\AcademicsGroupController@academicsGroupsCoursesCreate', 'as' => 'academics-groups-courses-create']);
    Route::get('academics-groups-courses', ['uses' => 'Admin\AcademicsGroupController@academicsGroupsCourses', 'as' => 'academics-groups-courses']);
    Route::get('boardprogramindex', ['uses' => 'Admin\BoardController@boardProgramshow', 'as' => 'boardprogramindex']); 
    Route::get('assign-class-section', ['uses' => 'Admin\SectionController@assignClassSection', 'as' => 'classes-assign-class-section']);
    Route::post('existing-add-board-program', ['uses' => 'Admin\BoardController@addExistingBoardProgram', 'as' => 'existing-board-program']);
    Route::post('assign-class-section-store', ['uses' => 'Admin\SectionController@assignClassSectionStore', 'as' => 'section-assign-class-section-store']);

    
    Route::post( '/get/board-programs', 'Admin\BoardController@loadBoardPrograms' )->name( 'board-loadPrograms' );
     Route::post( '/get/board-program-class', 'Admin\BoardController@loadBoardProgramClass' )->name( 'load-board-program-class' );
     Route::post( '/get/board-program-program-group', 'Admin\CourseGroupController@loadBoardProgramProgramgroup' )->name( 'load-board-program-program-group' );
     
     Route::post( '/get/load-course-group-class-group', 'Admin\ClassGroupController@loadCourseGroupClassGroup' )->name( 'load-course-group-class-group' );
     Route::post( '/get/program-group-course-group', 'Admin\CourseGroupController@loadProgramGroupCourseGroup' )->name( 'load-program-group-courses-group' );
    Route::get('view/board-programs-classes', ['uses' => 'Admin\BoardController@viewBoardProgramsClasses', 'as' => 'view-board-program-class']);
    Route::get('view/board-programs/{id}', ['uses' => 'Admin\BoardController@viewBoardPrograms', 'as' => 'view-board-program']);
    
    Route::get('view-board-program-class-section', ['uses' => 'Admin\SectionController@viewBoardProgramClassSection', 'as' => 'view-board-program-class-section']);
    Route::post('add-board-program-class-store', ['uses' => 'Admin\ClassController@boardProgramClassStore', 'as' => 'board-program-class-store']);
    Route::get('class-group-courses', ['uses' => 'Admin\MultipleCoursesGroupController@classGroupCourseCreate', 'as' => 'class-group-course-create']);
    Route::get('course-group-course-create', ['uses' => 'Admin\MultipleCoursesGroupController@courseGroupCourseCreate', 'as' => 'course-group-course-create']);
    Route::resource('academics-groups', 'Admin\AcademicsGroupController');
    Route::resource('multiple-courses-groups', 'Admin\MultipleCoursesGroupController');
    Route::resource('class-group', 'Admin\ClassGroupController');
    Route::resource('course-group', 'Admin\CourseGroupController');
    Route::resource('program-group', 'Admin\ProgramController');
    Route::resource('courses', 'Admin\CourseController');
    Route::resource('section', 'Admin\SectionController');
    Route::resource('classes', 'Admin\ClassController');
    Route::resource('boards', 'Admin\BoardController');
    Route::get('assign-board-program-classes', ['uses' => 'Admin\ClassController@assignBoardProgramClass', 'as' => 'boards-program-classes']);
    Route::get('assign-branch-program', ['uses' => 'Admin\BranchesController@assignBranchProgram', 'as' => 'branch-program']);
    Route::get('assign-board-program', ['uses' => 'Admin\BoardController@assignBoardProgram', 'as' => 'board-program']);
    Route::post('add-board-program', ['uses' => 'Admin\BoardController@addBoardProgram', 'as' => 'boards-board-program-store']);
    Route::get('create-company-branch/{id}', ['uses' => 'Admin\BranchesController@create', 'as' => 'branch-create']);
    Route::get('company-branch/{id}', ['uses' => 'Admin\BranchesController@viewCompanyBranches', 'as' => 'view-company-branch']);
    Route::resource('schedule', 'HRM\ScheduleController');
    Route::post('/schedule/score', ['uses' => 'HRM\ScheduleController@score', 'as' => 'schedule.score']);

    Route::get('/candidate-hired', ['uses' => 'HRM\CandidateFormController@candidateHired', 'as' => 'candidate-hired']);
    Route::get('/candidate-hire/{id}', ['uses' => 'HRM\ScheduleController@hire', 'as' => 'schedule.hire']);
    Route::get('/candidate-reject/{id}', ['uses' => 'HRM\ScheduleController@notHired', 'as' => 'schedule.reject']);
    // Route::get('/reject/{id}', 'HRM\ScheduleController@reject')->name('admin.reject');s
    // Route::get('admin/get-candidate-score/{id}/details','HRM\ScheduleController@getCandidateScore');
      
    Route::get('/reject/{id}', ['uses' => 'HRM\ScheduleController@reject', 'as' => 'reject']);
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    // Financial Years Starts Here
    Route::patch('financial_years/active/{id}', ['uses' => 'Admin\FinancialYearsController@active', 'as' => 'financial_years.active']);
    Route::patch('financial_years/inactive/{id}', ['uses' => 'Admin\FinancialYearsController@inactive', 'as' => 'financial_years.inactive']);
    Route::resource('financial_years', 'Admin\FinancialYearsController');
    // Financial Years Ends Here

    // AccountTypes Resrouce Starts Here
    Route::patch('account_types/active/{id}', ['uses' => 'Admin\AccountTypesController@active', 'as' => 'account_types.active']);
    Route::patch('account_types/inactive/{id}', ['uses' => 'Admin\AccountTypesController@inactive', 'as' => 'account_types.inactive']);
    Route::resource('account_types', 'Admin\AccountTypesController');
    // AccountTypes Resrouce Ends Here

    // Payment Methods Resrouce Starts Here
    Route::patch('payment_methods/active/{id}', ['uses' => 'Admin\PaymentMethodsController@active', 'as' => 'payment_methods.active']);
    Route::patch('payment_methods/inactive/{id}', ['uses' => 'Admin\PaymentMethodsController@inactive', 'as' => 'payment_methods.inactive']);
    Route::resource('payment_methods', 'Admin\PaymentMethodsController');
    // Payment Methods Resrouce Ends Here

    // Department Resrouce Starts Here
    Route::resource('departments', 'Admin\DepartmentsController');
    // Department Resrouce Ends Here
    
    // Discount Type Resrouce Starts Here
    Route::resource('discount-type', 'Admin\DiscountTypeController');
    // Discount Type Resrouce Ends Here

    // Active Session Resrouce Starts Here
    Route::resource('active-session', 'Admin\ActiveSessionController');
    // Session Resrouce Ends Here

    // Session Resrouce Starts Here
    Route::resource('session', 'Admin\SessionController');
    // Session Resrouce Ends Here

    // Regions Resrouce Starts Here
    Route::resource('regions', 'Admin\RegionController');
    // Regions Resrouce Ends Here

    // Regions Resrouce Starts Here
    Route::resource('territory', 'Admin\TerritoryController');
    // Regions Resrouce Ends Here

    // Companies Resrouce Starts Here
    Route::resource('companies', 'Admin\CompanyController');
    // Companies Resrouce Ends Here

    // Branches Resrouce Starts Here
    Route::resource('branches', 'Admin\BranchesController');
    // Branches Resrouce Ends Here

    // Customers Resrouce Starts Here
    Route::patch('customers/active/{id}', ['uses' => 'Admin\CustomersController@active', 'as' => 'customers.active']);
    Route::patch('customers/inactive/{id}', ['uses' => 'Admin\CustomersController@inactive', 'as' => 'customers.inactive']);
    Route::resource('customers', 'Admin\CustomersController');
    // Customers Resrouce Ends Here

    // Local Purchase start Here
    Route::get('duty/segregate/index', ['uses' => 'Admin\DutySegController@indexDuties', 'as' => 'duty_seg.index']);
    Route::get('duty/segregate/datatables', ['uses' => 'Admin\DutySegController@datatables', 'as' => 'duty_seg.datatables']);
    Route::get('duty/segregate/detail/{id}', ['uses' => 'Admin\DutySegController@detailDuty', 'as' => 'duty_seg.detail']);
    Route::get('duty/segregate/duty', 'Admin\DutySegController@duty')->name('duty_seg.duty');
    Route::get('duty/segregate/invoices', 'Admin\DutySegController@Invoices')->name('duty_seg.invoices');
    Route::get('duty/segregate', 'Admin\DutySegController@viewDutySegrigation')->name('duty_seg.view');
    Route::post('duty/segregate', 'Admin\DutySegController@storeDutySegrigation')->name('duty_seg.store');
    // Local Purchase Ends Here

     // Local Purchase start Here
    Route::get('localpurchase/productlist/{order_id}', 'Admin\LocalPurchaseController@productList')->name('localpurchase.product_list');
     Route::resource('localpurchase', 'Admin\LocalPurchaseController');
     // Local Purchase Ends Here


    // Employees Resrouce Starts Here
    //Route::get('employees/show/{id}', ['uses' => 'Admin\EmployeesController@show', 'as' => 'employees.show']);

     //commented by asad
    // Route::patch('employees/active/{id}', ['uses' => 'Admin\EmployeesController@active', 'as' => 'employees.active']);
    // Route::patch('employees/inactive/{id}', ['uses' => 'Admin\EmployeesController@inactive', 'as' => 'employees.inactive']);
    // Route::get('employees/job/{id}', ['uses' => 'Admin\EmployeesController@update_employee_job_detail', 'as' => 'employees.job']);
    // Route::post('employees/job/{id}', ['uses' => 'Admin\EmployeesController@insert_employee_job_data', 'as' => 'employees.insertjobdata']);
    // Route::get('employees/salary/{id}', ['uses' => 'Admin\EmployeesController@update_employee_salary_detail', 'as' => 'employees.salary']);
    // Route::post('employees/salary/{id}', ['uses' => 'Admin\EmployeesController@update_employee_salary_data', 'as' => 'employees.salarydata']);
    // Route::get('employees/experience/{id}', ['uses' => 'Admin\EmployeesController@update_employee_experience_detail', 'as' => 'employees.experience']);
    // Route::post('employees/experience/{id}', ['uses' => 'Admin\EmployeesController@update_employee_experience_data', 'as' => 'employees.experiencedata']);
    // Route::get('employees/education/{id}', ['uses' => 'Admin\EmployeesController@update_employee_education_detail', 'as' => 'employees.education']);
    // Route::post('employees/education/{id}', ['uses' => 'Admin\EmployeesController@update_employee_education_data', 'as' => 'employees.educationdata']);
    // Route::get('employees/resignation/{id}', ['uses' => 'Admin\EmployeesController@update_employee_resignation_detail', 'as' => 'employees.resignation']);
    // Route::post('employees/resignation/{id}', ['uses' => 'Admin\EmployeesController@update_employee_resignation_data', 'as' => 'employees.resignationdata']);
    // Route::post('employees/get_branches_ajax', ['uses' => 'Admin\EmployeesController@get_branches_ajax', 'as' => 'employees.get_branches_ajax']);
    // Route::post('employees/get_territory_ajax', ['uses' => 'Admin\EmployeesController@get_territory_ajax', 'as' => 'employees.get_territory_ajax']);
    // Route::post('employees/get_job_title_by_departmen_id_ajax', ['uses' => 'Admin\EmployeesController@get_job_title_by_departmen_id_ajax', 'as' => 'employees.get_job_title_by_departmen_id_ajax']);

    // Route::get('employees/getBranches', ['uses' => 'Admin\EmployeesController@getBranches', 'as' => 'employees.getBranches']);
    // Route::get('EmpAvailable', ['uses' => 'Admin\EmployeesController@EmpAvailable', 'as' => 'employees.EmpAvailable']);
    // Route::post('employees/availableStatus', ['uses' => 'Admin\EmployeesController@availableStatus', 'as' => 'employees.availableStatus']);

   // Route::resource('employess', 'HRM\EmployeesController');
     Route::resource('documents', 'HRM\DocumentController');

     Route::resource('staff', 'HRM\StaffController');
     Route::get('staff-profile','HRM\StaffController@profile')->name('staff.profile');
     Route::patch('staff/active/{id}', ['uses' => 'HRM\StaffController@active', 'as' => 'staff.active']);
    Route::patch('staff/inactive/{id}', ['uses' => 'HRM\StaffController@inactive', 'as' => 'staff.inactive']);
    
     Route::resource('branchhr', 'HRM\BranchHrController');
     Route::patch('branchhr/active/{id}', ['uses' => 'HRM\BranchHrController@active', 'as' => 'branchhr.active']);
    Route::patch('branchhr/inactive/{id}', ['uses' => 'HRM\BranchHrController@inactive', 'as' => 'branchhr.inactive']);


     Route::get('/staff.create', ['uses' => 'HRM\StaffController@create', 'as' => 'staff.create']);
    // Employees Resrouce Ends Here
    //Banks Resrouce Starts Here
    Route::resource('banks', 'Admin\BanksController');
    Route::post('banks_mass_destroy', ['uses' => 'Admin\BanksController@massDestroy', 'as' => 'banks.mass_destroy']);
    // Banks Resrouce Ends Here
    //Banksdetail Resrouce Starts Here
    Route::resource('bank_detail', 'Admin\BankDetailController');
    Route::post('bank_detail_mass_destroy', ['uses' => 'Admin\BankDetailController@massDestroy', 'as' => 'bank_detail.mass_destroy']);
    // Banksdetail Resrouce Ends Here

    // Suppliers Resrouce Starts Here
    Route::post('suppliers/delete', ['uses' => 'Admin\SuppliersController@delete', 'as' => 'suppliers.delete']);
    Route::get('supliers/datatables', ['uses' => 'Admin\SuppliersController@datatables', 'as' => 'supliers.datatables']);
    Route::resource('suppliers', 'Admin\SuppliersController');
    Route::patch('suppliers/active/{id}', ['uses' => 'Admin\SuppliersController@active', 'as' => 'suppliers.active']);
    Route::patch('suppliers/inactive/{id}', ['uses' => 'Admin\SuppliersController@inactive', 'as' => 'suppliers.inactive']);
    Route::post('suppliers_mass_destroy', ['uses' => 'Admin\SuppliersController@massDestroy', 'as' => 'suppliers.mass_destroy']);
    // Suppliers Resrouce Ends Here

    // Sales Invoices Starts Here
    Route::get('/salesinvoice', ['uses' => 'Admin\SalesInvoiceController@index', 'as' => 'salesinvoice.index']);
    Route::get('/salesinvoice/create', ['uses' => 'Admin\SalesInvoiceController@create', 'as' => 'salesinvoice.create']);
    Route::get('/salesinvoice/view/{id}', ['uses' => 'Admin\SalesInvoiceController@view', 'as' => 'salesinvoice.view']);
    Route::get('/salesinvoice/view/downloadPDF/{id}', ['uses' => 'Admin\SalesInvoiceController@downloadPDF', 'as' => 'salesinvoice.pdf']);
    Route::post('/salesinvoice/store', ['uses' => 'Admin\SalesInvoiceController@store', 'as' => 'salesinvoice.store']);
    Route::post('salesinvoice/update/{id}', ['uses' => 'Admin\SalesInvoiceController@update', 'as' => 'salesinvoice.update']);
    Route::post('/salesinvoice/getOrderDetail', ['uses' => 'Admin\SalesInvoiceController@getOrderDetail', 'as' => 'salesinvoice.getOrderDetail']);
    Route::get('salesinvoice/datatables', ['uses' => 'Admin\SalesInvoiceController@datatables', 'as' => 'salesinvoice.datatables']);
    Route::post('salesinvoice/confirm', ['uses' => 'Admin\SalesInvoiceController@confirm', 'as' => 'salesinvoice.confirm']);
    Route::post('salesinvoice/update_invoice/{id}', ['uses' => 'Admin\SalesInvoiceController@update_invoice', 'as' => 'salesinvoice.update_invoice']);

    Route::get('/salesinvoice/{id}/edit', ['uses' => 'Admin\SalesInvoiceController@edit', 'as' => 'salesinvoice.edit']);
//    Route::resource('salesinvoice', 'Admin\SalesInvoiceController');
    //Orders Resrouce Starts Here
    Route::post('orders/delete', ['uses' => 'Admin\OrdersController@delete', 'as' => 'orders.delete']);
    Route::get('importindex', ['uses' => 'Admin\OrdersController@importindex', 'as' => 'orders.importindex']);
    Route::get('orders/datatables', ['uses' => 'Admin\OrdersController@datatables', 'as' => 'orders.datatables']);
    Route::get('orders/importdatatables', ['uses' => 'Admin\OrdersController@importdatatables', 'as' => 'orders.importdatatables']);
    Route::get('orders/productlist', 'Admin\OrdersController@productlist')->name('orders.productlist');
    Route::resource('orders', 'Admin\OrdersController');
    Route::post('orders_mass_destroy', ['uses' => 'Admin\OrdersController@massDestroy', 'as' => 'orders.mass_destroy']);
    Route::post('stockitems/delete', ['uses' => 'Admin\StockItemsController@delete', 'as' => 'stockitems.delete']);
    Route::post('stockitems/datatables', ['uses' => 'Admin\StockItemsController@datatables', 'as' => 'stockitems.datatables']);
    Route::get('stockitems/models/{model_no}', 'Admin\StockItemsController@modelno')->name('stockitems.models');
    Route::get('stockitems/performa/{lc_no}/{sup_id}', 'Admin\StockItemsController@performa')->name('stockitems.performa');
    Route::get('stockitems/inventoryproduct', 'Admin\StockItemsController@inventoryproduct')->name('stockitems.inventoryproduct');
    //Orders Resrouce End Here
    Route::get('cinvoice/datatables', ['uses' => 'Admin\ComercialInvoiceController@datatables', 'as' => 'cinvoice.datatables']);
    Route::resource('cinvoice', 'Admin\ComercialInvoiceController');
    Route::get('cinvoice/productlist/{listvalue}','Admin\ComercialInvoiceController@getprpforma')->name('cinvoice.productlist');
    Route::get('cinvoice/costing/{ComercialInvoiceModel}', 'Admin\ComercialInvoiceController@lccosting')->name('cinvoice.costing');
    Route::post('cinvoice/cost', 'Admin\ComercialInvoiceController@costcalculation')->name('cinvoice.cost');
    Route::post('grimport/upd/{id}', ['uses' => 'Admin\ComercialInvoiceController@update', 'as' => 'comercialinvoiceupd.update']);
    Route::get('cinvoice/viewduty/{ComercialInvoiceModel}', 'Admin\ComercialInvoiceController@viewduty')->name('cinvoice.viewduty');
    Route::patch('cinvoice/viewduty/active/{id}', ['uses' => 'Admin\ComercialInvoiceController@active', 'as' => 'comercialinvoice.active']);
    Route::patch('cinvoice/viewduty/inactive/{id}', ['uses' => 'Admin\ComercialInvoiceController@inactive', 'as' => 'comercialinvoice.inactive']);

    //Sales Resrouce Starts Here
    Route::get('sales/produtsdetail/{id}', 'Admin\SalesController@produtsdetail')->name('sales.produtsdetail');
    Route::get('sales/produtscol/{id}', 'Admin\SalesController@produtscol')->name('sales.produtscol');
    Route::resource('sales', 'Admin\SalesController');
    Route::patch('sales/active/{id}', ['uses' => 'Admin\SalesController@active', 'as' => 'sales.active']);
    Route::patch('sales/inactive/{id}', ['uses' => 'Admin\SalesController@inactive', 'as' => 'sales.inactive']);
    Route::post('sales_mass_destroy', ['uses' => 'Admin\SalesController@massDestroy', 'as' => 'sales.mass_destroy']);
    //Sales Resrouce Ends Here
    // goods receipt Resrouce Starts Here
    // Route::post('products/delete', ['uses' => 'Admin\GoodsIssueController@delete', 'as' => 'products.delete']);
    Route::get('inventorytransfer/stocklist', 'Admin\InventoryTransferController@stocklist')->name('inventorytransfer.stocklist');
    Route::get('inventorytransfer/datatables', ['uses' => 'Admin\InventoryTransferController@datatables', 'as' => 'inventorytransfer.datatables']);
    Route::get('inventorytransfer/items/{id}', 'Admin\InventoryTransferController@stockrecord')->name('inventorytransfer.items');
    Route::resource('inventorytransfer', 'Admin\InventoryTransferController');
    // goods receipt Resrouce Ends Here

    // goods receipt Resrouce Starts Here
    // Route::post('products/delete', ['uses' => 'Admin\GoodsIssueController@delete', 'as' => 'products.delete']);
    Route::get('goodsreceipt/datatables', ['uses' => 'Admin\GoodsReceiptController@datatables', 'as' => 'goodsreceipt.datatables']);
    Route::get('goodsreceipt/product/{lc_no}/{sup_id}', 'Admin\GoodsReceiptController@getproduct')->name('goodsreceipt.product');
    Route::resource('goodsreceipt', 'Admin\GoodsReceiptController');
    // goods receipt Resrouce Ends Here

    // goods issue Resrouce Starts Here
   // Route::post('products/delete', ['uses' => 'Admin\GoodsIssueController@delete', 'as' => 'products.delete']);
    Route::get('goodsissue/datatables', ['uses' => 'Admin\GoodsIssueController@datatables', 'as' => 'goodsissue.datatables']);
    Route::get('goodsissue/items/{id}', 'Admin\GoodsIssueController@stockrecord')->name('goodsissue.items');
    Route::resource('goodsissue', 'Admin\GoodsIssueController');
    // goods issue Resrouce Ends Here

    //Stockitems Resrouce Starts Here

    Route::get('stockitems/performa/{lc_no}', 'Admin\StockItemsController@performa')->name('stockitems.performa');
    Route::get('stockitems/report', 'Admin\StockItemsController@StockReport')->name('stockitems.report');
    Route::post('stockitems/report_filter', 'Admin\StockItemsController@FilterReport')->name('stockitems.filterreport');
    Route::resource('stockitems', 'Admin\StockItemsController');
    Route::patch('stockitems/active/{id}', ['uses' => 'Admin\StockItemsController@active', 'as' => 'stockitems.active']);
    Route::patch('stockitems/inactive/{id}', ['uses' => 'Admin\StockItemsController@inactive', 'as' => 'stockitems.inactive']);

    Route::post('stockitems_mass_destroy', ['uses' => 'Admin\StockItemsController@massDestroy', 'as' => 'stockitems.mass_destroy']);
    Route::post('add/stockItem', ['uses' => 'Admin\PerformaStockController@addItem', 'as' => 'performastock.add_item']);

    //Stockitems Resrouce Ends Here

    //fix_assets Resrouce Starts Here

    Route::resource('fix_assets', 'Admin\FixAssetsController');

    //fix_assets Resrouce Starts Here

    //purchase order Resrouce Starts Here
    Route::post('pono/delete', ['uses' => 'Admin\PoNoController@delete', 'as' => 'pono.delete']);
    Route::get('pono/datatables', ['uses' => 'Admin\PoNoController@datatables', 'as' => 'pono.datatables']);
    Route::resource('pono', 'Admin\PoNoController');
    Route::post('pono_mass_destroy', ['uses' => 'Admin\PoNoController@massDestroy', 'as' => 'pono.mass_destroy']);
    //purchase order Resrouce Ends Here


    // custome rout performa //

    Route::post('performastock/costing/{PerformaStockModel}', 'Admin\PerformaStockController@submitcosting')->name('performastock.costing');
 
    //GRImport Starts Here
    Route::get('grimport', ['uses' => 'Admin\LcBankController@indexGrImport', 'as' => 'grimport']);
    Route::get('grimport/create', ['uses' => 'Admin\LcBankController@createGrImport', 'as' => 'grimport.create']);
    Route::post('grimport/datatables', ['uses' => 'Admin\LcBankController@getGrImportDatatables', 'as' => 'grimport.datatables']);
    Route::get('grimport/view/{id}', ['uses' => 'Admin\LcBankController@viewGrImport', 'as' => 'grimport.view']);
    Route::get('grimport/viewbuffer/{id}', ['uses' => 'Admin\LcBankController@viewBuffer', 'as' => 'grimport.buffer.view']);
    Route::post('grimport/transfer_inventory', ['uses' => 'Admin\LcBankController@transferInventory', 'as' => 'grimport.transfer.inventory']);

    //Performa Stock Starts Here//
    Route::resource('performastock', 'Admin\PerformaStockController');
    Route::post('performastock_mass_destroy', ['uses' => 'Admin\PerformaStockController@massDestroy', 'as' => 'performastock.mass_destroy']);
    Route::get('performastock/costing/{performaStockModel}', 'Admin\PerformaStockController@lccosting')->name('performastock.costing');
    Route::get('performastock/sale_voucher/create', 'Admin\PerformaStockController@performaInvoice')->name('performastock.sale_voucher.create');
    Route::get('performastock/sale_voucher/get', 'Admin\PerformaStockController@performaInvoice')->name('performastock.sale_voucher.create');
    //Performa Stock Starts Here/

    //Lc Bank Starts Here//
    Route::get('lcbank/productlist/{listvalue}', 'Admin\PerformaStockController@productImport')->name('lcbank.productlist');
    Route::post('lcbank/payment', 'Admin\LcBankController@marginCalculate')->name('lcbank.payment');
    Route::post('lcbank/fluctuation', 'Admin\LcBankController@rateFluctuation')->name('lcbank.fluctuation');
    Route::post('lcbank/delete', ['uses' => 'Admin\LcBankController@delete', 'as' => 'lcbank.delete']);
    Route::get('lcbank/sheetcost', ['uses' => 'Admin\LcBankController@sheetCsoting', 'as' => 'lcbank.sheetcost']);
    Route::get('lcbank/datatablesSheet', ['uses' => 'Admin\LcBankController@sheet_datatables', 'as' => 'lcbank.datatablesSheet']);
    Route::get('lcbank/datatables', ['uses' => 'Admin\LcBankController@datatables', 'as' => 'lcbank.datatables']);
    Route::get('lcbank/shipment', 'Admin\LcBankController@shipmentSchedule')->name('lcbank.shipment');
    Route::get('lcbank/create/{model_no}', 'Admin\LcBankController@lcamount')->name('lcbank.create');
    Route::resource('lcbank', 'Admin\LcBankController');
    Route::post('lcbank_mass_destroy', ['uses' => 'Admin\LcBankController@massDestroy', 'as' => 'lcbank.mass_destroy']);
    //Lc Bank Starts Here/
    
    // Release stock routes starts here
    Route::get('lcbank/invoicelist/{invoice_id}', 'Admin\LcBankController@invoiceImport')->name('lcbank.invoice_list');
    Route::get('lcbank/lctype/{invoice_id}', 'Admin\LcBankController@lctype')->name('lcbank.lctype');
    Route::post('releaseStock', 'Admin\LcBankController@storeReleaseStock')->name('releasestock.store');
    // Release stock routes ends 
    
    //Groups Resrouce Starts Here
    Route::get('groups/getData', ['uses' => 'Admin\GroupsController@getData', 'as'=> 'groups.getData']);
    Route::post('groups_mass_destroy', ['uses' => 'Admin\GroupsController@massDestroy', 'as' => 'groups.mass_destroy']);
    Route::post('groups/destroy/{id}', ['uses' => 'Admin\GroupsController@destroy', 'as' => 'groups.destroy']);
    Route::post('groups/edit/{id}', ['uses' => 'Admin\GroupsController@edit', 'as' => 'groups.edit']);

    Route::resource('groups', 'Admin\GroupsController');
    //Groups Resrouce eNDS Here

    //Ledgers Resrouce Starts Here
    Route::resource('ledgers', 'Admin\LedgersController');
    Route::get('ledger_tree', ['uses' => 'Admin\LedgersController@ledger_tree', 'as' => 'ledger_tree']);
    Route::get('get_ledger_tree/{PID}', ['uses' => 'Admin\LedgersController@get_ledger_tree', 'as' => 'get_ledger_tree']);
    Route::post('ledgers_child_detail', ['uses' => 'Admin\LedgersController@ledgerChild', 'as' => 'ledgers.child.detail']);
    Route::post('ledgers_mass_destroy', ['uses' => 'Admin\LedgersController@massDestroy', 'as' => 'ledgers.mass_destroy']);
    //Ledgers Resrouce eNDS Here

    //Orders Resrouce Starts Here
    Route::resource('orders', 'Admin\OrdersController');
    Route::post('orders_mass_destroy', ['uses' => 'Admin\OrdersController@massDestroy', 'as' => 'orders.mass_destroy']);
    //Orders Resrouce Starts Here

    Route::patch('permissions/active/{id}', ['uses' => 'Admin\PermissionsController@active', 'as' => 'permissions.active']);
    Route::patch('permissions/inactive/{id}', ['uses' => 'Admin\PermissionsController@inactive', 'as' => 'permissions.inactive']);
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);

    Route::patch('roles/active/{id}', ['uses' => 'Admin\RolesController@active', 'as' => 'roles.active']);
    Route::patch('roles/inactive/{id}', ['uses' => 'Admin\RolesController@inactive', 'as' => 'roles.inactive']);
    Route::resource('roles', 'Admin\RolesController');

    Route::resource('users', 'Admin\UsersController');
    Route::post('user-store', ['uses' => 'Admin\UsersController@createUser', 'as' => 'user-create-store']);
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);

    Route::post('taxsettings/delete', ['uses' => 'Admin\TaxSettingsController@delete', 'as' => 'taxsettings.delete']);
    Route::post('taxsettings/active', ['uses' => 'Admin\TaxSettingsController@active', 'as' => 'taxsettings.active']);
    Route::post('taxsettings/inactive', ['uses' => 'Admin\TaxSettingsController@inactive', 'as' => 'taxsettings.inactive']);
    Route::post('taxsettings/datatables', ['uses' => 'Admin\TaxSettingsController@datatables', 'as' => 'taxsettings.datatables']);
    Route::get('taxsettings/details/{id}', ['uses' => 'Admin\TaxSettingsController@details', 'as' => 'taxsettings.details']);
    Route::post('taxsettings/getEmployeeDetail', ['uses' => 'Admin\TaxSettingsController@getEmployeeDetail', 'as' => 'taxsettings.getEmployeeDetail']);
    Route::post('taxsettings/transfer_datatables', ['uses' => 'Admin\TaxSettingsController@transfer_datatables', 'as' => 'taxsettings.transfer_datatables']);
    Route::post('taxsettings/transfer_taxsettings', ['uses' => 'Admin\TaxSettingsController@transfer_taxsettings', 'as' => 'taxsettings.transfer_taxsettings']);
    Route::resource('taxsettings', 'Admin\TaxSettingsController');

    //Settings Resrouce Starts Here
    Route::resource('settings', 'Admin\SettingsController');
    //Settings Resrouce Ends Here
    //Entries Resrouce Starts Here
    Route::get('entries/{entry}/entry', ['uses' => 'Admin\EntriesController@entry', 'as' => 'entries.entry']);
    Route::post('entries/activate/{entry}', ['uses' => 'Admin\EntriesController@active', 'as' => 'entries.active']);
    Route::post('entries/inactive/{entry}', ['uses' => 'Admin\EntriesController@inactive', 'as' => 'entries.inactive']);
    Route::post('entries/edit/{entry}', ['uses' => 'Admin\EntriesController@edit', 'as' => 'entries.edit']);
    Route::post('entries/destroy/{entry}', ['uses' => 'Admin\EntriesController@destroy', 'as' => 'entries.destroy']);
    Route::get('entries/getData', ['uses' => 'Admin\EntriesController@getData', 'as'=> 'entries.getData']);
    Route::get('entries/downloadPDF/{id}', ['uses' => 'Admin\EntriesController@downloadPDF', 'as' => 'entries.downloadPDF']);
    Route::resource('entries', 'Admin\EntriesController');//Entries Resrouce eNDS Here

    // Entries Group Started
    Route::group(['prefix' => 'voucher', 'as' => 'voucher.'], function () {
        // Cash Search Voucher Routes
        Route::get('cash/search', ['uses' => 'Admin\EntriesController@cash_search', 'as' => 'cash_search']);
        // Bank Search Voucher Routes
        Route::get('bank/search', ['uses' => 'Admin\EntriesController@bank_search', 'as' => 'bank_search']);
        // Cash & Bank Search Voucher Routes
        Route::get('cashbank/search', ['uses' => 'Admin\EntriesController@cashbank_search', 'as' => 'cashbank_search']);

        // Journal Voucher Routes
        Route::get('gjv/create', ['uses' => 'Admin\EntriesController@gjv_create', 'as' => 'gjv_create']);
        Route::post('gjv/store', ['uses' => 'Admin\EntriesController@gjv_store', 'as' => 'gjv_store']);
        Route::get('gjv/search', ['uses' => 'Admin\EntriesController@gjv_search', 'as' => 'gjv_search']);
        // Cash Receipt Voucher Routes
        Route::get('crv/create', ['uses' => 'Admin\EntriesController@crv_create', 'as' => 'crv_create']);
        Route::post('crv/store', ['uses' => 'Admin\EntriesController@crv_store', 'as' => 'crv_store']);
        Route::get('crv/search', ['uses' => 'Admin\EntriesController@crv_search', 'as' => 'crv_search']);
        // Cash Payment Voucher Routes
        Route::get('cpv/create', ['uses' => 'Admin\EntriesController@cpv_create', 'as' => 'cpv_create']);
        Route::post('cpv/store', ['uses' => 'Admin\EntriesController@cpv_store', 'as' => 'cpv_store']);
        Route::get('cpv/search', ['uses' => 'Admin\EntriesController@cpv_search', 'as' => 'cpv_search']);
        // Bank Receipt Voucher Routes
        Route::get('brv/create', ['uses' => 'Admin\EntriesController@brv_create', 'as' => 'brv_create']);
        Route::post('brv/store', ['uses' => 'Admin\EntriesController@brv_store', 'as' => 'brv_store']);
        Route::get('brv/search', ['uses' => 'Admin\EntriesController@brv_search', 'as' => 'brv_search']);
        // Bank Payment Voucher Routes
        Route::get('bpv/create', ['uses' => 'Admin\EntriesController@bpv_create', 'as' => 'bpv_create']);
        Route::post('bpv/store', ['uses' => 'Admin\EntriesController@bpv_store', 'as' => 'bpv_store']);
        Route::get('bpv/search', ['uses' => 'Admin\EntriesController@bpv_search', 'as' => 'bpv_search']);
        // LC Payment Voucher Routes
        Route::get('lcpv/create', ['uses' => 'Admin\EntriesController@lcpv_create', 'as' => 'lcpv_create']);
        Route::post('lcpv/store', ['uses' => 'Admin\EntriesController@lcpv_store', 'as' => 'lcpv_store']);
        Route::get('lcpv/search', ['uses' => 'Admin\EntriesController@lcpv_search', 'as' => 'lcpv_search']);
        // LC Product Voucher Routes
        Route::get('lrp/create', ['uses' => 'Admin\EntriesController@lrp_create', 'as' => 'lrp_create']);
        Route::post('lrp/store', ['uses' => 'Admin\EntriesController@lrp_store', 'as' => 'lrp_store']);
        Route::get('lrp_search/{listvalue}', ['uses' => 'Admin\EntriesController@lrp_search', 'as' => 'lrp_search']);
    });

    //Recruiment
    // Route::group(['prefix' => 'recruiment', 'as' => 'recruiment.'], function () {
    //    Route::resource('financial_years', 'Admin\FinancialYearsController');

    // });
    // Route::resource('recruitment', 'HRM\JobPostRequestController');   
    // Route::get('/recruitment', ['uses' => 'HRM\JobPostRequestController@index', 'as' => 'recruitment.index']); 
    Route::group(['prefix' => 'recruitment', 'as' => 'recruitment.'], function () {

        Route::get('job-request', ['uses' => 'HRM\JobPostRequestController@index', 'as' => 'job-request']);
        Route::get('job-request-create', ['uses' => 'HRM\JobPostRequestController@create', 'as' => 'job-request-create']);
        Route::post('job-request-store', ['uses' => 'HRM\JobPostRequestController@store', 'as' => 'job-request-store']);
        Route::get('job-request-edit/{id}', ['uses' => 'HRM\JobPostRequestController@edit', 'as' => 'job-request-edit']);
        Route::post('job-request-update', ['uses' => 'HRM\JobPostRequestController@update', 'as' => 'job-request-update']);

        Route::get('job-post', ['uses' => 'HRM\JobPostController@index', 'as' => 'job-post']);
        Route::get('job-post-create/{id}', ['uses' => 'HRM\JobPostController@create', 'as' => 'job-post-create']);
        Route::post('job-post-store', ['uses' => 'HRM\JobPostController@store', 'as' => 'job-post-store']);
        Route::get('job-post-edit/{id}', ['uses' => 'HRM\JobPostController@edit', 'as' => 'job-post-edit']);
        Route::post('job-post-update', ['uses' => 'HRM\JobPostController@update', 'as' => 'job-post-update']);
        Route::get('job-post/job-status-update-active/{id}', ['uses' => 'HRM\JobPostController@updateStatusActive', 'as' => 'job-status-update-active']);
        Route::get('job-post/job-status-update-inactive/{id}', ['uses' => 'HRM\JobPostController@updateStatusInactive', 'as' => 'job-status-update-inactive']);

    });    


    // Accounts Reports Started
    Route::group(['prefix' => 'reports', 'as' => 'account_reports.'], function () {
        // Duty Calculation Report
        Route::get('duty-calculation', ['uses' => 'Admin\DutyCalculationController@addDuty', 'as' => 'duty_create']);
        Route::post('duty-calculation', ['uses' => 'Admin\DutyCalculationController@storeDuty', 'as' => 'duty_store']);
        Route::get('duty-report', ['uses' => 'Admin\DutyCalculationController@getDutyReport', 'as' => 'duty_report']);
        Route::delete('duty_delete/{id}', ['uses' => 'Admin\DutyCalculationController@destroyy', 'as' => 'duty_delete']);
        Route::get('duty_report_calculation', ['uses' => 'Admin\DutyCalculationController@calculateDuty', 'as' => 'duty_calculation_report']);
        // route for ajax
        Route::get('duty_report_all_products', ['uses' => 'Admin\DutyCalculationController@getProducts', 'as' => 'duty_calculation_allproducts']);
        Route::get('duty_report_all_prices', ['uses' => 'Admin\DutyCalculationController@getProductPrices', 'as' => 'duty_calculation_allprices']);
        
        // demand Calculation Report
        Route::get('dutydemand_report', ['uses' => 'Admin\DutyCalculationController@DutyDemand', 'as' => 'dutydemand_report']);
        Route::post('dutydemand_report', ['uses' => 'Admin\DutyCalculationController@getProductlist', 'as' => 'duty_demand']);

        // Profit & Lost Report
        Route::post('load-groups', ['uses' => 'Admin\AccountReportsController@load_groups', 'as' => 'load_groups']);
        Route::get('profit-loss', ['uses' => 'Admin\AccountReportsController@profit_loss', 'as' => 'profit_loss']);
        Route::post('profit-loss-report', ['uses' => 'Admin\AccountReportsController@profit_loss_report', 'as' => 'profit_loss_report']);
        // Trial Balance Report
        Route::get('trial-balance', ['uses' => 'Admin\AccountReportsController@trial_balance', 'as' => 'trial_balance']);
        Route::post('trial-balance-report', ['uses' => 'Admin\AccountReportsController@trial_balance_report', 'as' => 'trial_balance_report']);
        // Ledger Statement Report
        Route::post('load-ledgers', ['uses' => 'Admin\AccountReportsController@load_ledgers', 'as' => 'load_ledgers']);
        Route::get('ledger-statement', ['uses' => 'Admin\AccountReportsController@ledger_statement', 'as' => 'ledger_statement']);
        Route::post('ledger-statement-report', ['uses' => 'Admin\AccountReportsController@ledger_statement_report', 'as' => 'ledger_statement_report']);
        // Balance Sheet Report
        Route::get('balance-sheet', ['uses' => 'Admin\AccountReportsController@balance_sheet', 'as' => 'balance_sheet']);
        Route::post('balance-sheet-report', ['uses' => 'Admin\AccountReportsController@balance_sheet_report', 'as' => 'balance_sheet_report']);
        Route::get('ledger', ['uses' => 'Reports\LedgerController@index', 'as' => 'ledger']);
        Route::post('ledger_print', ['uses' => 'Reports\LedgerController@ledger_print', 'as' => 'ledger_print']);
        Route::get('trial_balance', ['uses' => 'Reports\TrialBalanceController@index', 'as' => 'trial_balance']);
        Route::post('print_trial_balance', ['uses' => 'Reports\TrialBalanceController@get_trial_balance', 'as' => 'print_trial_balance']);
        Route::get('expense_summary', ['uses' => 'Reports\ExpenseSummaryController@index', 'as' => 'expense_summary']);
        Route::post('print_expense_summary', ['uses' => 'Reports\ExpenseSummaryController@print_report', 'as' => 'print_expense_summary']);
        Route::get('profit_loss', ['uses' => 'Reports\ProfitLossController@index', 'as' => 'profit_loss']);
        Route::post('print_profit_loss', ['uses' => 'Reports\ProfitLossController@print_report', 'as' => 'print_profit_loss']);
        Route::get('balance_sheet', ['uses' => 'Reports\BalanceSheetController@index', 'as' => 'balance_sheet']);
        Route::post('print_balance_sheet', ['uses' => 'Reports\BalanceSheetController@print_report', 'as' => 'print_balance_sheet']);
        Route::get('balance_sheet_tree', ['uses' => 'Reports\BalanceSheetController@balance_sheet_tree', 'as' => 'balance_sheet_tree']);
        Route::get('get_balance_sheet_tree', ['uses' => 'Reports\BalanceSheetController@get_balance_sheet_tree', 'as' => 'get_balance_sheet_tree']);

    });

    // Financial Years Starts Here
    Route::patch('financial_years/active/{id}', ['uses' => 'Admin\FinancialYearsController@active', 'as' => 'financial_years.active']);
    Route::patch('financial_years/inactive/{id}', ['uses' => 'Admin\FinancialYearsController@inactive', 'as' => 'financial_years.inactive']);
    Route::resource('financial_years', 'Admin\FinancialYearsController');


    // Sales Invoices Starts Here
    Route::get('/salesreturn', ['uses' => 'Admin\SalesReturnController@index', 'as' => 'salesreturn.index']);
    Route::get('/salesreturn/create', ['uses' => 'Admin\SalesReturnController@create', 'as' => 'salesreturn.create']);
    Route::get('/salesreturn/view/{id}', ['uses' => 'Admin\SalesReturnController@view', 'as' => 'salesreturn.view']);
    Route::get('/salesreturn/view/downloadPDF/{id}', ['uses' => 'Admin\SalesReturnController@downloadPDF', 'as' => 'salesreturn.pdf']);
    Route::post('/salesreturn/store', ['uses' => 'Admin\SalesReturnController@store', 'as' => 'salesreturn.store']);
    Route::post('/salesreturn/getOrderDetail', ['uses' => 'Admin\SalesReturnController@getOrderDetail', 'as' => 'salesreturn.getOrderDetail']);
    Route::get('salesreturn/datatables', ['uses' => 'Admin\SalesReturnController@datatables', 'as' => 'salesreturn.datatables']);
    Route::post('salesreturn/confirm', ['uses' => 'Admin\SalesReturnController@confirm', 'as' => 'salesreturn.confirm']);
    Route::resource('salesreturn', 'Admin\SalesReturnController');
    // Financial Years Ends Here

     // Sales Order Starts Here
     Route::get('saleorder_create/{id}', ['uses' => 'Admin\SaleOrderController@createSaleOrder', 'as' => 'saleorder.createe']);

     Route::get('saleorder/qoutes', ['uses' => 'Admin\SaleOrderController@approvedQoutes', 'as' => 'saleorder.qoutes']);
     Route::get('saleorder/getProductsByName', ['uses' => 'Admin\SaleOrderController@getProductsByName', 'as' => 'saleorder.getProductsByName']);
     Route::get('saleorder/pdf/{id}', ['uses' => 'Admin\SaleOrderController@generateInvoice', 'as' => 'saleorder.generatePDF']);
     Route::resource('saleorder', 'Admin\SaleOrderController');

    Route::resource('country', 'Admin\CountryController');

    Route::resource('city', 'Admin\CityController');
    Route::resource('fee_section', 'Admin\FeeSectionController');
    Route::resource('fee_head', 'Admin\FeeHeadController');
    Route::resource('currency','Admin\CurrencyController');

    Route::resource('academic_year', 'Admin\AcademicYearController');
    Route::patch('academic_year/active/{id}', ['uses' => 'Admin\AcademicYearController@active', 'as' => 'academic_year.active']);
    Route::patch('academic_year/inactive/{id}', ['uses' => 'Admin\AcademicYearController@inactive', 'as' => 'academic_year.inactive']);

//    Route::resource('program', 'Admin\ProgramController');
//    Route::post('program_mass_destroy', ['uses' => 'Admin\ProgramController@massDestroy', 'as' => 'program.mass_destroy']);
});

//Public route for job posts

Route::get('job-post/{id}','HRM\JobPostController@showPublicPost');
Route::resource('candidate-form','HRM\CandidateFormController');
Route::get('candidate-form-create/{id}','HRM\CandidateFormController@create')->name('candidate-form-create');

// tax Module Routes
require_once 'web.hrm.php';

Route::fallback(function () {
    return view('404');
});
