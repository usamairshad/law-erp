@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Customers</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('customers_create'))
                {{--<a href="{{ route('admin.customers.create') }}" class="btn btn-success pull-right">Add New Customer</a>--}}
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Customers) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Contact Person</th>
                    <th>Mobile</th>
                    <th>Phone</th>
                    <th>NTN</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($Customers) > 0)
                    @foreach ($Customers as $Customer)
                        <tr data-entry-id="{{ $Customer->id }}">
                            <td>{{ $Customer->id }}</td>
                            <td>{{ $Customer->name }}</td>
                            <td>@if($Customer->contact_person) {{ $Customer->contact_person }} @else N/A @endif</td>
                            <td>{{ $Customer->mobile_1 }}</td>
                            <td>{{ $Customer->phone_1 }}</td>
                            <td>{{ $Customer->ntn }}</td>
                            <td>{{ date('d-m-Y h:m a', strtotime($Customer->created_at ))}}</td>
                            <td>
                                @if($Customer->status && Gate::check('customers_inactive'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['admin.customers.inactive', $Customer->id])) !!}
                                    {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}
                                @elseif(!$Customer->status && Gate::check('customers_active'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['admin.customers.active', $Customer->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                                @endif

                                @if(Gate::check('customers_edit'))
                                    <a href="{{ route('admin.customers.edit',[$Customer->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                {{-- @if(Gate::check('customers_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.customers.destroy', $Customer->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif --}}
                            </td>
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection