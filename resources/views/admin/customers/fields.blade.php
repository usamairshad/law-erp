<div class="form-group col-md-3 @if($errors->has('branch_id')) has-error @endif">
    {!! Form::label('branch_id', 'Branch*', ['class' => 'control-label']) !!}
    {!! Form::select('branch_id', $Branches, old('branch_id'), ['style' => 'width: 100%;', 'class' => 'form-control select2','disabled' =>'true']) !!}
    <span id="branch_id_handler"></span>
    @if($errors->has('branch_id'))
        <span class="help-block">
                        {{ $errors->first('branch_id') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('employee_id')) has-error @endif">
    {!! Form::label('employee_id', 'Exmployee*', ['class' => 'control-label']) !!}
    {!! Form::select('employee_id', $Employees, old('employee_id'), ['style' => 'width: 100%;', 'class' => 'form-control select2','disabled' =>'true']) !!}
    <span id="employee_id_handler"></span>
    @if($errors->has('employee_id'))
        <span class="help-block">
                            {{ $errors->first('employee_id') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-6 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control' ,'disabled' =>'true']) !!}
    @if($errors->has('name'))
        <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-6 @if($errors->has('contact_person')) has-error @endif">
    {!! Form::label('contact_person', 'Contact Person', ['class' => 'control-label']) !!}
    {!! Form::text('contact_person', old('contact_person'), ['class' => 'form-control' ,'disabled' =>'true']) !!}
    @if($errors->has('contact_person'))
        <span class="help-block">
                        {{ $errors->first('contact_person') }}
                    </span>
    @endif
</div>
<div class="form-group col-sm-3 @if($errors->has('title_1')) has-error @endif">
    {!! Form::label('title_1', 'Title 1', ['class' => 'control-label']) !!}
    {!! Form::text('title_1', old('title_1'), ['class' => 'form-control' ,'disabled' =>'true']) !!}
    @if($errors->has('title_1'))
        <span class="help-block">
                        {{ $errors->first('title_1') }}
                    </span>
    @endif
</div>
<div class="form-group col-sm-3 @if($errors->has('title_2')) has-error @endif">
    {!! Form::label('title_2', 'Title 2', ['class' => 'control-label']) !!}
    {!! Form::text('title_2', old('title_2'), ['class' => 'form-control' ,'minlength'=>'3','maxlength'=>'20']) !!}
    @if($errors->has('title_2'))
        <span class="help-block">
                        {{ $errors->first('title_2') }}
                    </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('mobile_1')) has-error @endif">
    {!! Form::label('mobile_1', 'Mobile 1*', ['class' => 'control-label']) !!}
    {!! Form::number('mobile_1', old('phone_1'), ['class' => 'form-control' ,'disabled' =>'true']) !!}
    @if($errors->has('mobile_1'))
        <span class="help-block">
                        {{ $errors->first('mobile_1') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('mobile_2')) has-error @endif">
    {!! Form::label('mobile_2', 'Mobile 2', ['class' => 'control-label']) !!}
    {!! Form::text('mobile_2', old('phone_2'), ['class' => 'form-control' ,'pattern'=> '[0-9]+', 'maxlength'=>'15']) !!}
    @if($errors->has('mobile_2'))
        <span class="help-block">
                        {{ $errors->first('mobile_2') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('phone_1')) has-error @endif">
    {!! Form::label('phone_1', 'Phone 1*', ['class' => 'control-label']) !!}
    {!! Form::number('phone_1', old('phone_1'), ['class' => 'form-control' ,'disabled' =>'true']) !!}
    @if($errors->has('phone_1'))
        <span class="help-block">
                        {{ $errors->first('phone_1') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('phone_2')) has-error @endif">
    {!! Form::label('phone_2', 'Phone 2', ['class' => 'control-label']) !!}
    {!! Form::text('phone_2', old('phone_2'), ['class' => 'form-control' ,'pattern'=> '[0-9]+', 'maxlength'=>'15']) !!}
    @if($errors->has('phone_2'))
        <span class="help-block">
                        {{ $errors->first('phone_2') }}
                    </span>
    @endif
</div>
<div class="row"></div>
<div class="form-group col-md-3 @if($errors->has('fax_1')) has-error @endif">
    {!! Form::label('fax_1', 'Fax 1', ['class' => 'control-label']) !!}
    {!! Form::text('fax_1', old('fax_1'), ['class' => 'form-control','pattern'=> '[0-9]+', 'maxlength'=>'15']) !!}
    @if($errors->has('fax_1'))
        <span class="help-block">
                        {{ $errors->first('fax_1') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('fax_2')) has-error @endif">
    {!! Form::label('fax_2', 'Fax 2', ['class' => 'control-label']) !!}
    {!! Form::text('fax_2', old('fax_2'), ['class' => 'form-control','pattern'=> '[0-9]+', 'maxlength'=>'15']) !!}
    @if($errors->has('fax_2'))
        <span class="help-block">
                        {{ $errors->first('fax_2') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::email('email', old('email'), ['class' => 'form-control' ,'disabled' =>'true']) !!}
    @if($errors->has('email'))
        <span class="help-block">
                        {{ $errors->first('email') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('website')) has-error @endif">
    {!! Form::label('website', 'Website', ['class' => 'control-label']) !!}
    {!! Form::url('website', old('website'), ['class' => 'form-control' ,'disabled' =>'true']) !!}
    @if($errors->has('website'))
        <span class="help-block">
                        {{ $errors->first('website') }}
                    </span>
    @endif
</div>


<div class="form-group col-md-6 @if($errors->has('address_1')) has-error @endif">
    {!! Form::label('address_1', 'Address 1', ['class' => 'control-label']) !!}
    {!! Form::text('address_1', old('address_1'), ['class' => 'form-control' ,'disabled' =>'true']) !!}
    @if($errors->has('address_1'))
        <span class="help-block">
                        {{ $errors->first('address_1') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-6 @if($errors->has('address_2')) has-error @endif">
    {!! Form::label('address_2', 'Address 2', ['class' => 'control-label']) !!}
    {!! Form::text('address_2', old('address_2'), ['class' => 'form-control','maxlength'=>'100']) !!}
    @if($errors->has('address_2'))
        <span class="help-block">
                        {{ $errors->first('address_2') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('zip_code')) has-error @endif">
    {!! Form::label('zip_code', 'Post Code', ['class' => 'control-label']) !!}
    {!! Form::text('zip_code', old('zip_code'), ['class' => 'form-control' ,'pattern'=> '[0-9]+', 'maxlength'=>'6']) !!}
    @if($errors->has('zip_code'))
        <span class="help-block">
                        {{ $errors->first('zip_code') }}
                    </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('cnic')) has-error @endif">
    {!! Form::label('cnic', 'CNIC #', ['class' => 'control-label']) !!}
    {!! Form::text('cnic', old('cnic'), ['class' => 'form-control' ,'pattern'=> '[0-9]+', 'maxlength'=>'13']) !!}
    @if($errors->has('cnic'))
        <span class="help-block">
                        {{ $errors->first('cnic') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('gst')) has-error @endif">
    {!! Form::label('gst', 'GST #', ['class' => 'control-label']) !!}
    {!! Form::text('gst', old('gst'), ['class' => 'form-control'  ,'disabled' =>'true']) !!}
    @if($errors->has('gst'))
        <span class="help-block">
                        {{ $errors->first('gst') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('ntn')) has-error @endif">
    {!! Form::label('ntn', 'NTN #', ['class' => 'control-label']) !!}
    {!! Form::text('ntn', old('ntn'), ['class' => 'form-control'  ,'disabled' =>'true']) !!}
    @if($errors->has('ntn'))
        <span class="help-block">
                        {{ $errors->first('ntn') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('reference_person')) has-error @endif">
    {!! Form::label('reference_person', 'Reference Person', ['class' => 'control-label']) !!}
    {!! Form::text('reference_person', old('reference_person'), ['class' => 'form-control' ,'minlength'=>'3','maxlength'=>'20', 'pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
    @if($errors->has('reference_person'))
        <span class="help-block">
                        {{ $errors->first('reference_person') }}
                    </span>
    @endif
</div>

<!-- Credit and Discout Fields -->
<div class="form-group col-md-3 @if($errors->has('credit_limit')) has-error @endif">
    {!! Form::label('credit_limit', 'Credit Limit', ['class' => 'control-label']) !!}
    {!! Form::text('credit_limit', old('credit_limit'), ['class' => 'form-control','pattern'=> '[0-9]+', 'maxlength'=>'5']) !!}
    @if($errors->has('credit_limit'))
        <span class="help-block">
                        {{ $errors->first('credit_limit') }}
                    </span>
    @endif
</div> 
<div class="form-group col-md-3 @if($errors->has('credit_term_days')) has-error @endif">
    {!! Form::label('credit_term_days', 'Credit Limit Days', ['class' => 'control-label']) !!}
    {!! Form::text('credit_term_days', old('credit_term_days'), ['class' => 'form-control' ,'pattern'=> '[0-9]+', 'maxlength'=>'5']) !!}
    @if($errors->has('credit_term_days'))
        <span class="help-block">
                        {{ $errors->first('credit_term_days') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('credit_term_days')) has-error @endif">
    {!! Form::label('discount', 'Discount %', ['class' => 'control-label']) !!}
    {!! Form::text('discount', old('discount'), ['class' => 'form-control','pattern'=> '[0-9]+', 'maxlength'=>'5']) !!}
    @if($errors->has('discount'))
        <span class="help-block">
                        {{ $errors->first('discount') }}
                    </span>
    @endif
</div>

<!-- Bank Fields -->
<div class="form-group col-md-3 @if($errors->has('bank_name')) has-error @endif">
    {!! Form::label('bank_name', 'Bank Name', ['class' => 'control-label']) !!}
    {!! Form::text('bank_name', old('bank_name'), ['class' => 'form-control' ,'minlength'=>'3','maxlength'=>'20']) !!}
    @if($errors->has('bank_name'))
        <span class="help-block">
                        {{ $errors->first('bank_name') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('bank_ac_number')) has-error @endif">
    {!! Form::label('bank_ac_number', 'Bank A/C #', ['class' => 'control-label']) !!}
    {!! Form::text('bank_ac_number', old('bank_ac_number'), ['class' => 'form-control' ,'maxlength'=>'18']) !!}
    @if($errors->has('bank_ac_number'))
        <span class="help-block">
                        {{ $errors->first('bank_ac_number') }}
                    </span>
    @endif
</div>