<div class="form-group  col-md-3 @if($errors->has('device_id')) has-error @endif">
    {!! Form::label('device_id', 'Device Id*', ['class' => 'control-label']) !!}
    {!! Form::text('device_id', old('device_id'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'20']) !!}
    @if($errors->has('device_id'))
        <span class="help-block">
            {{ $errors->first('device_id') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('first_name')) has-error @endif">
    {!! Form::label('first_name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'50',  'pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
    @if($errors->has('first_name'))
        <span class="help-block">
            {{ $errors->first('first_name') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('father_name')) has-error @endif">
    {!! Form::label('father_name', 'Father Name*', ['class' => 'control-label']) !!}
    {!! Form::text('father_name', old('father_name'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'50' ,  'pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
    @if($errors->has('father_name'))
        <span class="help-block">
            {{ $errors->first('father_name') }}
        </span>
    @endif
</div>

{{--@if($request->segment(4) != 'edit' )--}}

    <div class="form-group  col-md-3 @if($errors->has('department_id')) has-error @endif">
        {!! Form::label('department_id', 'Department', ['class' => 'control-label']) !!}
        {!! Form::select('department_id', $Departments, old('department_id'),
         ['id' => 'department_id','class' => 'form-control select2' ,'onchange' => 'FormControls.departmentChange(this.value);']) !!}
        @if($errors->has('department_id'))
            <span class="help-block">
                {{ $errors->first('department_id') }}
            </span>
        @endif
    </div>
{{--@endif--}}
<div class="form-group  col-md-3 @if($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'50']) !!}
    @if($errors->has('email'))
        <span class="help-block">
            {{ $errors->first('email') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('cnic')) has-error @endif">
    {!! Form::label('cnic', 'CNIC*', ['class' => 'control-label']) !!}
    {!! Form::text('cnic', old('name'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'15']) !!}
    @if($errors->has('cnic'))
        <span class="help-block">
            {{ $errors->first('cnic') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('date_of_birth')) has-error @endif">
    {!! Form::label('date_of_birth', 'Date of Birth*', ['class' => 'control-label']) !!}
    {!! Form::text('date_of_birth', old('date_of_birth'), ['class' => 'form-control', 'placeholder' => '']) !!}
    @if($errors->has('date_of_birth'))
        <span class="help-block">
            {{ $errors->first('date_of_birth') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('gender')) has-error @endif">
    {!! Form::label('gender', 'Gender*', ['class' => 'control-label']) !!}
    {!! Form::select('gender',[ ''=>'Select Marital Status','male' => 'Male','female' => 'Female','other' => 'Other'],old('gender') , ['class' => 'form-control select2']) !!}
    @if($errors->has('gender'))
            <span class="help-block">
                {{ $errors->first('gender') }}
            </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('marital_status')) has-error @endif">
    {!! Form::label('marital_status', 'Marital Status*', ['class' => 'control-label']) !!}
    {!! Form::select('marital_status',[ ''=>'Select Marital Status','married' => 'Married','unmarried' => 'Unmarried','other' => 'Other'],old('marital_status') , ['class' => 'form-control']) !!}
    @if($errors->has('marital_status'))
        <span class="help-block">
            {{ $errors->first('marital_status') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('mobile')) has-error @endif">
    {!! Form::label('mobile', 'Mobile*', ['class' => 'control-label']) !!}
    {!! Form::text('mobile', old('mobile'), ['class' => 'form-control', 'placeholder' => '' ,'pattern'=> '[0-9]+', 'maxlength'=>'15']) !!}
    @if($errors->has('mobile'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('emergency_mobile')) has-error @endif">
    {!! Form::label('emergency_mobile', 'Emergency Mobile*', ['class' => 'control-label']) !!}
    {!! Form::text('emergency_mobile', old('emergency_mobile'), ['class' => 'form-control', 'placeholder' => '' ,'pattern'=> '[0-9]+', 'maxlength'=>'15']) !!}
    @if($errors->has('emergency_mobile'))
        <span class="help-block">
            {{ $errors->first('emergency_mobile') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('city')) has-error @endif">
    {!! Form::label('city', 'City', ['class' => 'control-label']) !!}
    {!! Form::text('city', old('city'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'50']) !!}
    @if($errors->has('city'))
        <span class="help-block">
            {{ $errors->first('city') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('address')) has-error @endif">
    {!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
    {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'100']) !!}
    @if($errors->has('address'))
        <span class="help-block">
            {{ $errors->first('address') }}
        </span>
    @endif
</div>

<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="about_featured">
        <div class="panel-group" id="accordion3">

            <div class="panel panel-default wow fadInLeft">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion3" href="#13">
                            <span class="fa fa-check-square-o"></span><b>Job Info </b>
                        </a>
                    </h4>
                </div>

                <div id="13" class="panel-collapse collapse in">

                    @include('admin.employees.job_fields')

                </div>
            </div>
        </div>

    </div>
</div>




<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="about_featured">
        <div class="panel-group" id="accordion4">

            <div class="panel panel-default wow fadInLeft">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion4" href="#14">
                            <span class="fa fa-check-square-o"></span><b>Salary Info </b>
                        </a>
                    </h4>
                </div>

                <div id="14" class="panel-collapse collapse in">
                    @include('admin.employees.salary_fields')
                </div>
            </div>
        </div>

    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="about_featured">
        <div class="panel-group" id="accordion5">

            <div class="panel panel-default wow fadInLeft">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion5" href="#15">
                            <span class="fa fa-check-square-o"></span><b>Education Detail </b>
                        </a>
                    </h4>
                </div>

                <div id="15" class="panel-collapse collapse in">
                    @include('admin.employees.education_fields')
                </div>
            </div>
        </div>

    </div>
</div>


<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="about_featured">
        <div class="panel-group" id="accordion6">

            <div class="panel panel-default wow fadInLeft">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion6" href="#16">
                            <span class="fa fa-check-square-o"></span><b> Resignation </b>
                        </a>
                    </h4>
                </div>

                <div id="16" class="panel-collapse collapse in">
                    @include('admin.employees.resignation_fields')
                </div>
            </div>
        </div>

    </div>
</div>
