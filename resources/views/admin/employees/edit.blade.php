@extends('layouts.app')
@inject('request', 'Illuminate\Http\Request')
@section('stylesheet')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/select2/dist/css/select2.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">


    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

@stop
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employees</h1>
    </section>
@stop

@section('content')


    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Employee's Personal Detail </h3>
            <a href="{{ route('admin.employees.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Employee, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.employees.update', $Employee->id]]) !!}
        <div class="box-body">
            @include('admin.employees.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- bootstrap datepicker -->
    {{--<script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>--}}

    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Select2 -->
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>

    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>
@endsection