<div class="form-group  col-md-3 @if($errors->has('bank_name')) has-error @endif">
    <?php
    $Bankname =\App\Models\Admin\BanksModel::pluck('bank_name','id');
    ?>
    {!! Form::label('bank_name', 'Bank Name', ['class' => 'control-label']) !!}
    {!! Form::select('bank_name', $Bankname->prepend('Select Bank Name', ''),old('bank_name'), ['class' => 'form-control']) !!}
    @if($errors->has('bank_name'))
        <span class="help-block">
                        {{ $errors->first('bank_name') }}
                    </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('account_number')) has-error @endif">
    {!! Form::label('account_number', 'Account Number', ['class' => 'control-label']) !!}
    {!! Form::text('account_number', old('account_number'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'16']) !!}
    @if($errors->has('account_number'))
        <span class="help-block">
                        {{ $errors->first('account_number') }}
                    </span>
    @endif
</div>

<div class="form-group  col-md-3 @if($errors->has('basic_salary')) has-error @endif">
    {!! Form::label('basic_salary', 'Basic Salary', ['class' => 'control-label']) !!}
    {!! Form::text('basic_salary', old('basic_salary'), ['class' => 'form-control', 'placeholder' => '','pattern'=> '[0-9]+', 'maxlength'=>'6']) !!}
@if($errors->has('basic_salary'))
        <span class="help-block">
                        {{ $errors->first('basic_salary') }}
                    </span>
    @endif
</div>

<div class="form-group col-md-3  @if($errors->has('is_sc')) has-error @endif" style="margin-top: 35px;">
    {!! Form::label('is_sc', 'Apply Social Security ? ', ['class' => 'control-label']) !!}
    {{ Form::checkbox('is_sc', 1, old('is_sc') , ['id' => 'is_sc',
    'onchange' => 'FormControls.CalculateGrandTotal();' ] ) }}

    @if($errors->has('is_sc'))
        <span class="help-block">
            {{ $errors->first('is_sc') }}
        </span>
    @endif
</div>

<div class="form-group  col-md-3 @if($errors->has('sc_amount')) has-error @endif">
    {!! Form::label('sc_amount', 'Social Security Amount', ['class' => 'control-label']) !!}
    {!! Form::text('sc_amount', old('sc_amount'), ['class' => 'form-control', 'placeholder' => '' ,'pattern'=> '[0-9]+', 'maxlength'=>'15']) !!}
    @if($errors->has('sc_amount'))
        <span class="help-block">
                        {{ $errors->first('sc_amount') }}
        </span>
    @endif
</div>




<div class="form-group col-md-3  @if($errors->has('is_eobi')) has-error @endif" style="margin-top: 35px;">
        {!! Form::label('is_eobi', 'Apply EOBI ? ', ['class' => 'control-label']) !!}
        {{ Form::checkbox('is_eobi', 1, old('is_eobi') , ['id' => 'is_eobi',
        'onchange' => 'FormControls.CalculateGrandTotal();' ] ) }}

        @if($errors->has('is_eobi'))
                <span class="help-block">
            {{ $errors->first('is_eobi') }}
        </span>
        @endif
</div>

<div class="form-group  col-md-3 @if($errors->has('eobi_amount')) has-error @endif">
        {!! Form::label('eobi_amount', 'EOBI Amount', ['class' => 'control-label']) !!}
        {!! Form::text('eobi_amount', old('eobi_amount'), ['class' => 'form-control', 'placeholder' => '' ,'pattern'=> '[0-9]+', 'maxlength'=>'6']) !!}
        @if($errors->has('eobi_amount'))
                <span class="help-block">
                        {{ $errors->first('eobi_amount') }}
        </span>
        @endif
</div>

<div class="form-group col-md-12">

<div class="form-group col-md-3  @if($errors->has('is_house_rent')) has-error @endif">
    {!! Form::label('is_house_rent', 'Apply House Rent ? ', ['class' => 'control-label']) !!}
    {{ Form::checkbox('is_house_rent', 1, old('is_house_rent') , ['id' => 'is_house_rent',
    'onchange' => 'FormControls.CalculateGrandTotal();' ] ) }}

    @if($errors->has('is_house_rent'))
        <span class="help-block">
            {{ $errors->first('is_house_rent') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3  @if($errors->has('is_utility')) has-error @endif">
    {!! Form::label('is_utility', 'Apply Utility ? ', ['class' => 'control-label']) !!}
    {{ Form::checkbox('is_utility', 1, old('is_utility') , ['id' => 'is_utility',
    'onchange' => 'FormControls.CalculateGrandTotal();' ] ) }}

    @if($errors->has('is_utility'))
        <span class="help-block">
            {{ $errors->first('is_utility') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3  @if($errors->has('is_overtime')) has-error @endif">
    {!! Form::label('is_overtime', 'Apply Over Time ? ', ['class' => 'control-label']) !!}
    {{ Form::checkbox('is_overtime', 1, old('is_overtime') , ['id' => 'is_overtime',
    'onchange' => 'FormControls.CalculateGrandTotal();' ] ) }}

    @if($errors->has('is_overtime'))
        <span class="help-block">
            {{ $errors->first('is_overtime') }}
        </span>
    @endif
</div>
</div>