<div class="form-group  col-md-3  col-md-3 @if($errors->has('edu_level')) has-error @endif">
    <?php
    $Education =\App\Models\HRM\Educations::pluck('name','id');
    ?>
    {!! Form::label('edu_level', 'Education Level', ['class' => 'control-label']) !!}
    {!! Form::select('edu_level', $Education->prepend('Select Education level', ''),old('edu_level'),
     ['class' => 'form-control' ]) !!}
    @if($errors->has('edu_level'))
        <span class="help-block">
                        {{ $errors->first('edu_level') }}
                    </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('edu_institute')) has-error @endif">
    {!! Form::label('edu_institute', 'Institute', ['class' => 'control-label']) !!}
    {!! Form::text('edu_institute', old('edu_institute'),
    ['class' => 'form-control', 'placeholder' => '','maxlength'=>'50']) !!}
    @if($errors->has('edu_institute'))
        <span class="help-block">
            {{ $errors->first('edu_institute') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('edu_specialization')) has-error @endif">
    {!! Form::label('edu_specialization', 'Major / Specialization', ['class' => 'control-label']) !!}
    {!! Form::text('edu_specialization', old('edu_specialization'),
    ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'50']) !!}
    @if($errors->has('edu_specialization'))
        <span class="help-block">
            {{ $errors->first('edu_specialization') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('edu_year')) has-error @endif">
    {!! Form::label('edu_year', 'Education Year', ['class' => 'control-label']) !!}
    {!! Form::number('edu_year', old('edu_year'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'10']) !!}
    @if($errors->has('edu_year'))
        <span class="help-block">
            {{ $errors->first('edu_year') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('edu_score')) has-error @endif">
    {!! Form::label('edu_score', 'GPA / Score', ['class' => 'control-label']) !!}
    {!! Form::text('edu_score', old('edu_score'),
     ['class' => 'form-control', 'placeholder' => '','maxlength'=>'10']) !!}
    @if($errors->has('edu_score'))
        <span class="help-block">
            {{ $errors->first('edu_score') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('edu_start_date')) has-error @endif">
    {!! Form::label('edu_start_date', 'Start Date', ['class' => 'control-label datepicker']) !!}
    {!! Form::text('edu_start_date', old('edu_start_date'),['class' => 'form-control datepicker', 'placeholder' => '']) !!}
@if($errors->has('edu_start_date'))
        <span class="help-block">
            {{ $errors->first('edu_start_date') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('edu_end_date')) has-error @endif">
    {!! Form::label('edu_end_date', 'End Date', ['class' => 'control-label']) !!}
    {!! Form::text('edu_end_date', old('edu_end_date'), ['class' => 'form-control datepicker', 'placeholder' => '']) !!}
    @if($errors->has('edu_end_date'))
        <span class="help-block">
            {{ $errors->first('edu_end_date') }}
        </span>
    @endif
</div>

