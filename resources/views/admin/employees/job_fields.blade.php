<div class="form-group  col-md-3 @if($errors->has('job_title')) has-error @endif">
    <?php
    $JobTitle =\App\Models\HRM\JobTitle::pluck('name','id');
    ?>
    {!! Form::label('job_title', 'Job Title', ['class' => 'control-label']) !!}
    {!! Form::select('job_title', $JobTitle->prepend('Select Job Title', '') ,old('job_title'),
    ['class' => 'form-control' ,'id'=> 'job_title', 'onchange' => 'FormControls.jobTitle(this.value);']) !!}
    @if($errors->has('job_title'))
        <span class="help-block">
            {{ $errors->first('job_title') }}
        </span>
    @endif
</div>
<div id="region_id_h" class="form-group  col-md-3 @if($errors->has('region_id')) has-error @endif">
    <?php
    $RegionName =\App\Models\Admin\Regions::pluck('name','id');
    ?>
    {!! Form::label('region_id', 'Region Name', ['class' => 'control-label']) !!}
    {!! Form::select('region_id', $RegionName->prepend('Select Region', ''),old('region_id'),
     ['class' => 'form-control','onchange' => 'FormControls.fetchBranches(this.value);']) !!}
    @if($errors->has('region_id'))
        <span class="help-block">
            {{ $errors->first('region_id') }}
        </span>
    @endif
</div>
<div id="branch_id_h" class="form-group  col-md-3 @if($errors->has('branch_id')) has-error @endif">

    {!! Form::label('branch_id', 'Branch Name', ['class' => 'control-label']) !!}
    @if(isset($Employee->id))
                  <?php $newBranches =\App\Models\Admin\Branches::where(['region_id'=> $Employee->region_id])->pluck('name','id'); ?>
        {!! Form::select('branch_id', $newBranches->prepend('Select Branch', '') ,old('branch_id'),
         ['class' => 'form-control' , 'id'=>'branch_id' ,'onchange' => 'FormControls.fetchTerritory(this.value);']) !!}
    @else

        {!! Form::select('branch_id', array() ,old('branch_id'),
         ['class' => 'form-control' , 'id'=>'branch_id' ,'onchange' => 'FormControls.fetchTerritory(this.value);']) !!}
    @endif
    @if($errors->has('branch_id'))
        <span class="help-block">
            {{ $errors->first('branch_id') }}
        </span>
    @endif
</div>
<div id="territory_id_h" class="form-group  col-md-3 @if($errors->has('territory_id')) has-error @endif">

    {!! Form::label('territory_id', 'Territory Name', ['class' => 'control-label']) !!}
        @if(isset($Employee->id))
        <?php $newTerr =\App\Models\Admin\Territory::where(['branch_id'=> $Employee->branch_id])->pluck('name','id'); ?>
            {!! Form::select('territory_id', $newTerr->prepend('Select Territory', ''),old('territory_id'),
             ['class' => 'form-control', 'id' => 'territory_id' , 'onchange' => 'FormControls.fetchsalesman();']) !!}
        @else
                {!! Form::select('territory_id', array(),old('territory_id'),
                ['class' => 'form-control', 'id' => 'territory_id' , 'onchange' => 'FormControls.fetchsalesman();']) !!}
        @endif
    @if($errors->has('territory_id'))
        <span class="help-block">
            {{ $errors->first('territory_id') }}
        </span>
    @endif
</div>


<div class="form-group  col-md-3 @if($errors->has('job_category')) has-error @endif">
    <?php
    $JobCategories =\App\Models\HRM\JobCategories::pluck('name','id');
    ?>
    {!! Form::label('job_category', 'Job category', ['class' => 'control-label']) !!}
    {!! Form::select('job_category', $JobCategories->prepend('Select a job category', ''),old('job_category'), ['class' => 'form-control']) !!}
    @if($errors->has('job_category'))
        <span class="help-block">
            {{ $errors->first('job_category') }}
        </span>
    @endif
</div>

<div class="form-group  col-md-3 @if($errors->has('employee_status')) has-error @endif">
    <?php
    $Departments =\App\Models\HRM\EmploymentStatuses::pluck('name','id');
    ?>
    {!! Form::label('employee_status', 'Employee Status', ['class' => 'control-label']) !!}
    {!! Form::select('employee_status', $Departments->prepend('Select Status', ''),old('employee_status'), ['class' => 'form-control']) !!}

    @if($errors->has('employee_status'))
        <span class="help-block">
            {{ $errors->first('employee_status') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('date_of_joining')) has-error @endif">
    {!! Form::label('date_of_joining', 'Date of Joining', ['class' => 'control-label']) !!}
    {!! Form::text('date_of_joining', old('date_of_joining'), ['class' => 'form-control datepicker', 'placeholder' => '']) !!}
    @if($errors->has('date_of_joining'))
        <span class="help-block">
            {{ $errors->first('date_of_joining') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('probation_period')) has-error @endif">
    {!! Form::label('probation_period', 'Probation Period', ['class' => 'control-label']) !!}
    {!! Form::select('probation_period', [ '0'=>'0 ','30' => '30','90' => '90'],old('probation_period') , ['class' => 'form-control select2' ]) !!}

    @if($errors->has('probation_period'))
        <span class="help-block">
            {{ $errors->first('probation_period') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('contract_start_date')) has-error @endif">
    {!! Form::label('contract_start_date', 'Stat Date of contract', ['class' => 'control-label']) !!}
    {!! Form::text('contract_start_date', old('contract_start_date'), ['id' => 'contract_start_date', 'class' => 'form-control datepicker ', 'placeholder' => '']) !!}
    @if($errors->has('contract_start_date'))
        <span class="help-block">
            {{ $errors->first('contract_start_date') }}
        </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('contract_end_date')) has-error @endif">
    {!! Form::label('contract_end_date', 'End Date of Contract', ['class' => 'control-label']) !!}
    {!! Form::text('contract_end_date', old('contract_end_date'), ['id' => 'contract_end_date','class' => 'form-control datepicker', 'placeholder' => '']) !!}
    @if($errors->has('contract_end_date'))
        <span class="help-block">
            {{ $errors->first('contract_end_date') }}
        </span>
    @endif
</div>


<div class="form-group  col-md-3 @if($errors->has('perm_start_date')) has-error @endif">
    {!! Form::label('perm_start_date', 'Permanent start date', ['class' => 'control-label']) !!}
    {!! Form::text('perm_start_date', old('perm_start_date'), ['class' => 'form-control datepicker', 'placeholder' => '']) !!}
    @if($errors->has('perm_start_date'))
        <span class="help-block">
            {{ $errors->first('perm_start_date') }}
        </span>
    @endif
</div>

<div class="form-group  col-md-3 @if($errors->has('perm_end_date')) has-error @endif">
    {!! Form::label('perm_end_date', 'Permanent end date', ['class' => 'control-label']) !!}
    {!! Form::text('perm_end_date', old('perm_end_date'), ['class' => 'form-control datepicker', 'placeholder' => '']) !!}
    @if($errors->has('perm_end_date'))
        <span class="help-block">
            {{ $errors->first('perm_start_date') }}
        </span>
    @endif
</div>




<div class="form-group col-md-3 @if($errors->has('report_to')) has-error @endif">
    {!! Form::label('report_to', 'Reports To *', ['class' => 'control-label']) !!}
    {!! Form::select('report_to', array(), old('report_to'), ['class' => 'form-control' , 'id' => 'report_to']) !!}
    @if($errors->has('report_to'))
        <span class="help-block">
            {{ $errors->first('report_to') }}
        </span>
    @endif
</div>

@if(isset($Employee->id))
    <input type="hidden" name="prev_report" id="prev_report" value="{{ $Employee->report_to }}">
@else
    <input type="hidden" name="prev_report" id="prev_report" value="0">
@endif

<script>
var allBranches = <?php echo $Branches; ?>;
var allTerritory = <?php echo $Territory; ?>;
var allSalesman = <?php echo $allSalesman; ?>;
console.log('allSalesman :', allSalesman);
<?php if(isset($Employee->id)){ ?>
        var jobTitlee =  "<?php echo $Employee->job_title; ?>";
<?php }
else{ ?>
    jobTitlee = 0;
<?php }
?>
</script>