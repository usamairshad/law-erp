@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee availability</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Check Availability</h3>
            <a href="{{ route('marketing.databanks.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['marketing.databanks.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            {{csrf_field()}}
            <div class="form-group col-md-3 @if($errors->has('employee_id')) has-error @endif">
                {!! Form::label('employee_id', 'Employee *', ['class' => 'control-label']) !!}
                {!! Form::select('employee_id', $Employees, old('employee_id'), [ 'id' => 'employee_id' , 'class' => 'form-control select2']) !!}
                @if($errors->has('employee_id'))
                    <span class="help-block">
                    {{ $errors->first('employee_id') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('from_date')) has-error @endif">
                {!! Form::label('from_date', 'Start Date*', ['class' => 'control-label']) !!}
                {!! Form::text('from_date', old('from_date'), ['id' => 'from_date', 'class' => 'form-control datepicker']) !!}
                @if($errors->has('from_date'))
                    <span class="help-block">
                            {{ $errors->first('from_date') }}
                    </span>
                @endif
            </div>

            <div class="form-group col-md-3 @if($errors->has('to_date')) has-error @endif">
                {!! Form::label('to_date', 'End Date*', ['class' => 'control-label']) !!}
                {!! Form::text('to_date', old('to_date'), ['id' => 'to_date', 'class' => 'form-control datepicker']) !!}
                @if($errors->has('to_date'))
                    <span class="help-block">
                            {{ $errors->first('to_date') }}
                    </span>
                @endif
            </div>
            
            
            <div class="col-md-3">

                <button id="search_button" onclick="FormControls.checkAvailable();" type="button" style="margin-top: 25px;border-radius: 10px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search </b> </button>
            </div>

        </div>

        <div class="panel-body pad table-responsive" id="available-table">
            <table class="table table-bordered table-striped datatable">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Employee</th>
                    <th>Leave Type</th>
                    <th>Leave Status</th>
                    <th>Leave Date</th>
                    <th>Create At</th>
                </tr>
                </thead>

                <tbody id="avail-body">
                </tbody>
            </table>
        </div>
        <div id="av_para" class="col-md-9">
            <p><b>Employee is available on this/these dates so far</p>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">

        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>
@endsection

