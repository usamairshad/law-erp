<table style="display: none;">
    <tbody id="line_item-container" >
    <tr id="line_item-######">
        <td >
            <div class="form-group" style="margin-bottom: 0px !important;">
                <div class="form-group" style="margin-bottom: 0px !important;">

                    <select class="form-control" name="line_items[tax_class][######]" id="line_items[tax_class][######]">
            <option disabled selected>--Select--</option>
            <option value="July 20 - June 21">July 20 - June 21</option>
            <option value="July 21 - June 22">July 21 - June 22</option>
            <option value="July 22 - June 23">July 22 - June 23</option>
            <option value="July 23 - June 24">July 23 - June 24</option>
            <option value="July 24 - June 25">July 24 - June 25</option>
        </select>
                
                </div>
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;" >

                {!! Form::number('line_items[fix_amount][######]',  old('fix_amount'), ['id' => 'line_item-fix_amount-######','class' => 'form-control', 'required' => 'true']) !!}

            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;" >

                {!! Form::number('line_items[tax_percent][######]',  old('tax_percent'), ['id' => 'line_item-tax_percent-######','class' => 'form-control', 'required' => 'true']) !!}

            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;" >

                {!! Form::number('line_items[start_range][######]',  old('start_range'), ['id' => 'line_item-start_range-######','class' => 'form-control', 'required' => 'true']) !!}

            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;" >

                {!! Form::number('line_items[end_range][######]',  old('end_range'), ['id' => 'line_item-end_range-######','class' => 'form-control', 'required' => 'true']) !!}

            </div>
        </td>

        <td><button id="line_item-del_btn-######" onclick="FormControls.destroyLineItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>
    </tbody>
</table>