

<div class="col-md-6"></div>


<div id="11" class="col-md-12">


        <div class="panel-body pad table-responsive">
                <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>R</u>ow</button>
                <table class="table table-bordered table-striped {{ count($TaxSettings) > 0 ? 'datatable' : '' }} " id="users-table">
                        <thead>
                        <tr>
                                <th>Tax Class</th>
                                <th>Fix Amount</th>
                                <th>Tax Percent</th>
                                <th>Start Range</th>
                                <th>End Range</th>
                                <th>Remove</th>


                        </tr>
                        </thead>
                        <tbody>
                                <?php  $counter=0 ?>
                                @if(count($TaxSettings) )

                                        @foreach ($TaxSettings as $key => $val)

                                            <?php  $counter++ ?>
                                            <tr id="line_item-{{$counter}}" >
                                            
                                                    <td> <div class="form-group  @if($errors->has('tax_class')) has-error @endif">

                                                    <select class="form-control" value ="{{$val->tax_class}}" name="line_items[tax_class][{{$counter}}]" id="line_items[tax_class][{{$counter}}]">
            <option>--Select--</option>
            @if($val->tax_class == "July 20 - June 21")

            <option value="July 20 - June 21" selected>July 20 - June 21</option>
            <option value="July 21 - June 22">July 21 - June 22</option>
            <option value="July 22 - June 23">July 22 - June 23</option>
            <option value="July 23 - June 24">July 23 - June 24</option>
            <option value="July 24 - June 25">July 24 - June 25</option>
            @elseif($val->tax_class == "July 21 - June 22")
            <option value="July 20 - June 21" >July 20 - June 21</option>
            <option value="July 21 - June 22" selected>July 21 - June 22</option>
            <option value="July 22 - June 23">July 22 - June 23</option>
            <option value="July 23 - June 24">July 23 - June 24</option>
            <option value="July 24 - June 25">July 24 - June 25</option>
            @elseif($val->tax_class == "July 22 - June 23")
            <option value="July 20 - June 21" >July 20 - June 21</option>
            <option value="July 21 - June 22" >July 21 - June 22</option>
            <option value="July 22 - June 23" selected>July 22 - June 23</option>
            <option value="July 23 - June 24">July 23 - June 24</option>
            <option value="July 24 - June 25">July 24 - June 25</option>
            @elseif($val->tax_class == "July 23 - June 24")
            <option value="July 20 - June 21" >July 20 - June 21</option>
            <option value="July 21 - June 22" >July 21 - June 22</option>
            <option value="July 22 - June 23" >July 22 - June 23</option>
            <option value="July 23 - June 24" selected>July 23 - June 24</option>
            <option value="July 24 - June 25">July 24 - June 25</option>
            @elseif($val->tax_class == "July 24 - June 25")
            <option value="July 20 - June 21" >July 20 - June 21</option>
            <option value="July 21 - June 22" >July 21 - June 22</option>
            <option value="July 22 - June 23" >July 22 - June 23</option>
            <option value="July 23 - June 24">July 23 - June 24</option>
            <option value="July 24 - June 25" selected>July 24 - June 25</option>
            
            
            @endif
        </select>
                                                        @if($errors->has('tax_class'))
                                                            <span class="help-block">
                                                                {{ $errors->first('tax_class') }}
                                                            </span>
                                                        @endif
                                                            </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group  @if($errors->has('line_fix_amount')) has-error @endif">

                                                            {!! Form::number('line_items[fix_amount]['.$counter.']',  $val->fix_amount, ['id' => 'line_item-fix_amount_id-1','class' => 'form-control']) !!}
                                                            @if($errors->has('line_fix_amount'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('line_fix_amount') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                <td>
                                                    <div class="form-group  @if($errors->has('line_tax_percent')) has-error @endif">

                                                        {!! Form::number('line_items[tax_percent]['.$counter.']',  $val->tax_percent, ['id' => 'line_item-tax_percent_id-1','class' => 'form-control']) !!}
                                                        @if($errors->has('line_tax_percent'))
                                                            <span class="help-block">
                                                                        {{ $errors->first('line_tax_percent') }}
                                                                    </span>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group  @if($errors->has('line_start_range')) has-error @endif">

                                                        {!! Form::number('line_items[start_range]['.$counter.']',  $val->start_range, ['id' => 'line_item-start_range_id-1','class' => 'form-control']) !!}
                                                        @if($errors->has('line_start_range'))
                                                            <span class="help-block">
                                                                {{ $errors->first('line_start_range') }}
                                                            </span>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group  @if($errors->has('line_end_range')) has-error @endif">

                                                        {!! Form::number('line_items[end_range]['.$counter.']',  $val->end_range, ['id' => 'line_item-end_range_id-1','class' => 'form-control']) !!}
                                                        @if($errors->has('line_end_range'))
                                                            <span class="help-block">
                                                                {{ $errors->first('line_end_range') }}
                                                            </span>
                                                        @endif
                                                    </div>
                                                </td>
                                             <td><button id="line_item-del_btn-{{$counter}}" onclick="FormControls.destroyLineItem('{{$counter}}');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>

                                            </tr>

                                        @endforeach
                                @else

                                @endif
                                <input type="hidden" id="line_item-global_counter" value="<?php  echo ++$counter ?>"   />

                        </tbody>

                </table>
        </div>


</div>