@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Branches</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Branch</h3>
            <a href="{{ route('admin.branches.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->


        {!! Form::model($Branche, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.branches.update', $Branche->id]]) !!}
        <div class="box-body">
            <div class="form-group @if($errors->has('region_id')) has-error @endif">
                <?php
                $Regions =\App\Models\Admin\Regions::pluck('name','id');
                ?>
                {!! Form::label('region_id', 'Regions*', ['class' => 'control-label']) !!}
                {!! Form::select('region_id', $Regions->prepend('Select a Region', ''),old('region_id'), ['class' => 'form-control']) !!}

                @if($errors->has('region_id'))
                    <span class="help-block">
                        {{ $errors->first('region_id') }}
                    </span>
                @endif
            </div>
            <div class="form-group @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
@endsection