@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Bank Account Details</h3>
            <a href="{{ route('admin.bank_detail.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Banks, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.bank_detail.update', $Banks->id]]) !!}
        <div class="box-body" style ="margin-top:50px;">
            <div  class="row">
                <div class="col-lg-12">
                    <input type="hidden" name="bank_name" value="{{$Banks->bank_id}}">
                    <input type="hidden" name="emp_name" value="{{$Banks->group_id}}">

                    <div class="col-xs-3 form-group">
                        <label>Select Branch</label>
                        <select class="form-control select2" name="branch_id">
                            {!! App\Models\Admin\Company::CompanyList($Banks->branch_id) !!}
                        </select>
                    </div>
                    <div class="col-xs-3 form-group">
                        <label>Select City</label>
                        <select class="form-control select2" name="city_id">
                            {!! App\Models\Admin\City::CityList($Banks->city_id) !!}
                        </select>
                    </div>
                    <div class="col-xs-3 form-group @if($errors->has('bank_name')) has-error @endif">
                        {!! Form::label('bank_name', 'Bank Name*', ['class' => 'control-label']) !!}

                        <select id="bank_name" name="bank_name" class="form-control select2">
                            <option value="">Select Bank Name</option>
                            @if ($Banks->count())
                                @foreach($Bankes as $Bank)
                                    <option @if($Bank->id) selected @endif value="{{ $Bank->id }}">{{ $Bank->bank_name }}</option>

                                @endforeach
                            @endif
                        </select>

                        <span id="bank_name_id"></span>
                        @if($errors->has('bank_name'))
                            <span class="help-block">
                            {{ $errors->has('bank_name') }}
                             </span>
                        @endif
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('branch_code')) has-error @endif">
                        {!! Form::label('branch_code', 'Branch Code *', ['class' => 'control-label']) !!}
                        {!! Form::text('branch_code', old('branch_code'), ['class' => 'form-control', 'placeholder' => '', 'required'=>'true']) !!}
                        @if($errors->has('branch_code'))
                            <span class="help-block">
                                {{ $errors->first('branch_code') }}
                                </span>
                        @endif
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('account_no')) has-error @endif">
                        {!! Form::label('account_no', 'Account No: *', ['class' => 'control-label']) !!}
                        {!! Form::text('account_no', old('account_no'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
                        @if($errors->has('account_no'))
                            <span class="help-block">
                                {{ $errors->first('account_no') }}
                                </span>
                        @endif
                    </div>

                </div>
                <div class="col-lg-12">
                    {{--<div class="col-xs-3 form-group @if($errors->has('acc_type')) has-error @endif">--}}
                        {{--{!! Form::label('accountType', 'Account Type', ['class' => 'control-label']) !!}--}}
                        {{--{!! Form::text('accountType', $Banks->accountType, ['id' =>'accountType' , 'class' => 'form-control','readonly']) !!}--}}
                        {{--<input type="hidden" name="acc_type" value="{{$Banks->accountType}}">--}}

                        {{--<span id="accountType"></span>--}}
                        {{--@if($errors->has('accountType'))--}}
                            {{--<span class="help-block">--}}
                            {{--{{ $errors->has('accountType') }}--}}
                             {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    <div class="col-xs-3 form-group @if($errors->has('phone_no')) has-error @endif">
                        {!! Form::label('phone_no', 'Phone No: *', ['class' => 'control-label']) !!}
                        {!! Form::text('phone_no', old('phone_no'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
                        @if($errors->has('phone_no'))
                            <span class="help-block">
                            {{ $errors->first('phone_no') }}
                        </span>
                        @endif
                    </div>

                    <div class="col-xs-6 form-group @if($errors->has('bank_address')) has-error @endif">
                        {!! Form::label('bank_address', 'Address', ['class' => 'control-label']) !!}
                        {!! Form::text('bank_address', old('bank_name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        @if($errors->has('bank_address'))
                            <span class="help-block">
                            {{ $errors->first('bank_address') }}
                        </span>
                        @endif
                    </div>

                </div>

            </div>

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    {{-- <script src="{{ url('js/admin/settings/create_modify.js') }}" type="text/javascript"></script> --}}
    <script src="{{ url('public/js/admin/bank_detail/bank_detail_create_modify.js') }}" type="text/javascript"></script>
@endsection