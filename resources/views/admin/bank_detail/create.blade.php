@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Bank Account Details</h3>
            <a href="{{ route('admin.bank_detail.index') }}" style = "float:right" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.bank_detail.store'], 'id' => 'validation-form']) !!}
        <div class="box-body" style = "margin-top:50px;">
            <div class="row">
                <div class="col-xs-12 form-group" style = "display:none;">
                    <div class="col-md-3" >
                        <label>
                            <input type="radio" name="orderType" id="orderType" value="1" checked> Company
                        </label>
                    </div>
                    {{--<div class="col-md-3">--}}
                        {{--<label>--}}
                            {{--<input type="radio" name="orderType" id="orderType" value="2"> Employee--}}
                        {{--</label>--}}
                    {{--</div>--}}
                </div>
            </div>
            {{--<div class="row">--}}
                {{--<div class="col-xs-12 form-group">--}}
                    {{--<div class="col-md-3">--}}
                        {{--<label>--}}
                            {{--<input type="radio" name="accountType" id="accountType" value="Current Account" checked> Current Account--}}
                        {{--</label>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-3">--}}
                        {{--<label>--}}
                            {{--<input type="radio" name="accountType" id="accountType" value="Saving Account"> Saving Account--}}
                        {{--</label>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div  class="row">
                <div class="col-lg-12">
              
                    <div class="col-xs-3 form-group">
                        <label>Select Company</label>
                        <select class="form-control select2" name="branch_id">
                            {!! App\Models\Admin\Company::CompanyList() !!}
                        </select>
                    </div>
                    <div class="col-xs-3 form-group">
                        <label>Select City</label>
                        <select class="form-control select2" name="city_id">
                            {!! App\Models\Admin\City::CityList() !!}
                        </select>
                    </div>
                    <div class="col-xs-12 form-group">
        {!! Form::label('parent_id', 'Parent Group', ['class' => 'control-label']) !!}
        <select name="parent_id" id="parent_id" class="form-control select2" style="width: 100%;">
            <option value=""> Select a Parent Group </option>
                {!! $Groups !!}
        </select>
        <span id="parent_id_handler"></span>
        @if($errors->has('parent_id'))
                <span class="help-block">
                        {{ $errors->first('parent_id') }}
                </span>
        @endif
</div>
                    <div class="col-xs-3 form-group @if($errors->has('bank_name')) has-error @endif">
                        {!! Form::label('bank_name', 'Bank Name*', ['class' => 'control-label']) !!}

                        <select id="bank_name" name="bank_name" class="form-control select2">
                            <option value="">Select Bank Name</option>
                            @if ($Banks->count())
                                @foreach($Banks as $Bank)
                                    <option value="{{ $Bank->id }}">{{ $Bank->bank_name }}</option>

                                @endforeach
                            @endif
                        </select>

                        <span id="bank_name_id"></span>
                        @if($errors->has('bank_name'))
                            <span class="help-block">
                            {{ $errors->has('bank_name') }}
                             </span>
                        @endif
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('branch_code')) has-error @endif">
                        {!! Form::label('branch_code', 'Branch Code *', ['class' => 'control-label']) !!}
                        {!! Form::text('branch_code', old('branch_code'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>50]) !!}
                        @if($errors->has('branch_code'))
                            <span class="help-block">
                                {{ $errors->first('branch_code') }}
                                </span>
                        @endif
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('account_no')) has-error @endif">
                        {!! Form::label('account_no', 'Account No: *', ['class' => 'control-label']) !!}
                        {!! Form::text('account_no', old('account_no'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>50]) !!}
                        @if($errors->has('account_no'))
                            <span class="help-block">
                                {{ $errors->first('account_no') }}
                                </span>
                        @endif
                    </div>

                </div>
            </div>


            <div  class="row">
                <div class="col-lg-12">
                    {{--<div class="col-xs-3 form-group @if($errors->has('acc_type')) has-error @endif">--}}
                        {{--{!! Form::label('acc_type', 'Account Type*', ['class' => 'control-label']) !!}--}}

                        {{--<select id="acc_type" name="acc_type" class="form-control" required>--}}
                            {{--<option value="">Select Account Type</option>--}}
                            {{--<option value="1">PKR</option>--}}
                            {{--<option value="2">EURO(€)</option>--}}
                            {{--<option value="3">US($)</option>--}}
                        {{--</select>--}}

                        {{--<span id="acc_type_id"></span>--}}
                        {{--@if($errors->has('acc_type'))--}}
                            {{--<span class="help-block">--}}
                            {{--{{ $errors->has('acc_type') }}--}}
                             {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}

                    <div class="col-xs-3 form-group @if($errors->has('phone_no')) has-error @endif">
                        {!! Form::label('phone_no', 'Phone No: *', ['class' => 'control-label']) !!}
                        {!! Form::text('phone_no', old('phone_no'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>50]) !!}
                        @if($errors->has('phone_no'))
                            <span class="help-block">
                            {{ $errors->first('phone_no') }}
                        </span>
                        @endif
                    </div>

                    <div class="col-xs-6 form-group @if($errors->has('bank_address')) has-error @endif">
                        {!! Form::label('bank_address', 'Address', ['class' => 'control-label']) !!}
                        {!! Form::text('bank_address', old('bank_name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>100]) !!}
                        @if($errors->has('bank_address'))
                            <span class="help-block">
                            {{ $errors->first('bank_address') }}
                        </span>
                        @endif
                    </div>

                </div>

            </div>

        </div>
            <!-- /.box-body -->

            <div class="box-footer">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/bank_detail/bank_detail_create_modify.js') }}" type="text/javascript"></script>
@endsection

