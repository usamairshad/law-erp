@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Bank Details</h3>
            {{--//@if(Gate::check('bankdetail_create'))--}}
            <a href="{{ route('admin.bank_detail.create') }}" style = "float:right;" class="btn btn-success pull-right">@lang('global.app_add_new')</a>
            {{--@endif--}}
        </div>
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-bBanked table-striped {{ count($BankDetail) > 0 ? 'datatable' : '' }} dt-select" id="users-table">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Bank Name</th>
                    <th>Branch Code</th>
                    <th>Account No</th>
                    <th>Bank Address</th>
                    <th>Phone No</th>
                    <th>Action</th>

                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($BankDetail) > 0)
                    @foreach ($BankDetail as $BankDetails)

                        <tr data-entry-id="{{ $BankDetails->id }}">
                            <td> {{$r}} </td>
                            <td>{{ $BankDetails->banksModel->bank_name }}</td>
                            <td>{{ $BankDetails->branch_code }}</td>
                            <td>{{ $BankDetails->account_no }}</td>
                            <td>{{ $BankDetails->bank_address }}</td>
                            <td>{{ $BankDetails->phone_no }}</td>
                            <td>
                                <a href="{{ route('admin.bank_detail.edit',[$BankDetails->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>

                                {{--{!! Form::open(array(--}}
                                    {{--'style' => 'display: inline-block;',--}}
                                    {{--'method' => 'DELETE',--}}
                                    {{--'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",--}}
                                    {{--'route' => ['admin.bank_detail.destroy', $BankDetails->id])) !!}--}}
                                {{--{!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}--}}
                                {{--{!! Form::close() !!}--}}
                            </td>

                        </tr>
                        @php($r++)
                    @endforeach

                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('#users-table').DataTable();
        window.route_mass_crud_entries_destroy = '{{ route('admin.bank_detail.mass_destroy') }}';
    </script>
@endsection