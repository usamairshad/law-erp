@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Loan</h1>
    </section>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ url('loan') }}/create" class="btn btn-success pull-right">Add New Loan</a>
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Loans) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Role</th>
                    <th>User</th>
                    <th>Amount</th>
                    <th>Type</th>
                    <th>Salary deduct</th>
                    <th>Paid Status</th>
                    <th>Return Date</th>
                    <th>Remaning</th>
                    
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($Loans) > 0)
                    @foreach ($Loans as $Loan)

                        <tr data-entry-id="{{ $Loan->id }}">
                            <td> {{$r}} </td>
                            <td>{{$Loan->role->name}}</td>
                            <td>{{$Loan->user->name}}</td>
                            <td>{{$Loan->amount}}</td>
                            <td>{{$Loan->type}}</td>
                            <td>{{$Loan->salary_deduct}}</td>
                            <td>{{$Loan->paid_status}}</td>
                            <td>{{$Loan->return_date}}</td>
                            <td>{{$Loan->remaining}}</td>
                            


                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection