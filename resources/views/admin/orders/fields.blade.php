<div class="row">
    <div class="col-xs-12 form-group">
        <div class="col-md-3">
            <label>
                <input type="radio" name="orderType" id="orderType" value="1"> Local
            </label>
        </div>
        <div class="col-md-3">
            <label>
                <input type="radio" name="orderType" id="orderType" value="2" checked > Import
            </label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 form-group import">
        <div class="col-xs-3 form-group import @if($errors->has('po_number')) has-error @endif">
            {!! Form::label('po_number', 'Po No*', ['class' => 'control-label']) !!}
            {!! Form::text('po_number', old('po_number'), ['class' => 'form-control', 'placeholder' => '','pattern'=> '[0-9]+' ,'min'=>"1",'required','maxlength'=>40]) !!}
        </div>
        <div class="col-xs-3 form-group @if($errors->has('perfomra_no')) has-error @endif">
            {!! Form::label('po_date', 'Po Date *', ['class' => 'control-label']) !!}
            {!! Form::text('po_date', old('po_date'), ['id'=>'po_date','class' => 'form-control datepicker', 'placeholder' => '', 'autocomplete' => 'off' ,'maxlength'=>'40']) !!}
        </div>
        <div class="col-xs-3 form-group @if($errors->has('perfomra_no')) has-error @endif">
            {!! Form::label('perfomra_no', 'Proforma Number*', ['class' => 'control-label']) !!}
            <input id="perfomra_no" class="form-control" type="text" name="perfomra_no" value="" min="1" pattern= "[0-9]+"  maxlength="50" required/>
        </div>
        <div class="col-xs-3 form-group @if($errors->has('sal_date')) has-error @endif">
            {!! Form::label('sal_date', 'Proforma Invoice Date*', ['class' => 'control-label']) !!}
            {!! Form::text('sal_date', old('sal_date'), ['id' => 'sal_date','class' => 'form-control datepicker', 'placeholder' => '' ,'required', 'autocomplete' => 'off']) !!}
            @if($errors->has('sal_date'))
                <span class="help-block">
                {{ $errors->first('sal_date') }}
                </span>
            @endif
        </div>
    </div>

    <div class="col-xs-12 form-group">
        <div class="col-xs-3 form-group @if($errors->has('sal_no')) has-error @endif">
            {!! Form::label('sale_no', 'Sales Order Number*', ['class' => 'control-label']) !!}
            <input id="sale_no" class="form-control" type="text" name="sale_no" value="" min="1" pattern= "[0-9]+" maxlength="40" required/>
        </div>
        <div class="col-xs-3 form-group @if($errors->has('valid_upto')) has-error @endif">
            {!! Form::label('valid_upto', 'On/About*', ['class' => 'control-label']) !!}
            {!! Form::text('valid_upto', old('valid_upto'), ['id' => 'valid_upto','class' => 'form-control datepicker', 'placeholder' => '','required', 'autocomplete' => 'off']) !!}
            @if($errors->has('valid_upto'))
                <span class="help-block">
                    {{ $errors->first('valid_upto') }}
                </span>
            @endif
        </div>
        <div class="col-xs-3 form-group @if($errors->has('soruce')) has-error @endif">
            {!! Form::label('soruce', 'From', ['class' => 'control-label']) !!}
            <input id="soruce" class="form-control" type="text" name="soruce" value=""  maxlength="50" minlength="3"/>
        </div>
        <div class="col-xs-3 form-group @if($errors->has('destination')) has-error @endif">
            {!! Form::label('destination', 'To', ['class' => 'control-label']) !!}
            <input id="destination" class="form-control" type="text" name="destination" value="" maxlength="50" minlength="3"/>
        </div>


    </div>
    <div class="col-xs-12 form-group">
        <div class="col-xs-3 form-group @if($errors->has('shipping')) has-error @endif">
            {!! Form::label('shipping', 'Shipping Via', ['class' => 'control-label']) !!}
            <input id="shipping" class="form-control" type="text" name="shipping" value="" maxlength="50" pattern ='[a-zA-Z][a-zA-Z ]{2,}'/>
        </div>
        <div class="col-xs-6 form-group @if($errors->has('payemnt_terms')) has-error @endif">
            {!! Form::label('payemnt_terms', 'Payment Terms*', ['class' => 'control-label']) !!}
            <input id="payemnt_terms" class="form-control" type="text" name="payemnt_terms" value="" maxlength="255" required />
        </div>

            <div class="col-xs-3 form-group @if($errors->has('sup_id')) has-error @endif" id="import_sup">
            {!! Form::label('sup_id', 'Brands *', ['class' => 'control-label']) !!}
            {!! Form::select('sup_id_for', $foreignSuppliersModel->prepend('Select Brands', ''),old('sup_id_for'), ['class' => 'form-control select2', 'id'=> 'sup_id_for']) !!}
                {{--<select id="sup_id" name="sup_id" class="form-control select2">--}}
                    {{--@if ($SuppliersModel->count())--}}
                        {{--<option value="">Select Brands</option>--}}
                            {{--@foreach($SuppliersModel as $Supplier)--}}
                                {{--<option value="{{ $Supplier->id }}">{{ $Supplier->sup_name }}</option>--}}
                            {{--@endforeach--}}
                    {{--@endif--}}
                {{--</select>--}}
                @if($errors->has('sup_id'))
                <span class="help-block">
                {{ $errors->first('sup_id') }}
                </span>
            @endif
                {!! Form::hidden('sup_name_for', old('sup_name'), ['id'=>'sup_name_for','class' => 'form-control datepicker', 'placeholder' => '']) !!}

            </div>
            <div class="col-xs-3 form-group @if($errors->has('sup_id')) has-error @endif" id="local_sup">
                {!! Form::label('sup_id', 'Brands *', ['class' => 'control-label']) !!}
                {!! Form::select('sup_id_loc', $localSuppliersModel->prepend('Select Brands', ''),old('sup_id'), ['class' => 'form-control select2', 'id' => 'sup_id_loc']) !!}
                    {{--<select id="sup_id" name="sup_id" class="form-control select2">--}}
                        {{--@if ($SuppliersModel->count())--}}
                            {{--<option value="">Select Brands</option>--}}
                                {{--@foreach($SuppliersModel as $Supplier)--}}
                                    {{--<option value="{{ $Supplier->id }}">{{ $Supplier->sup_name }}</option>--}}
                                {{--@endforeach--}}
                        {{--@endif--}}
                    {{--</select>--}}
                    @if($errors->has('sup_id'))
                    <span class="help-block">
                    {{ $errors->first('sup_id') }}
                    </span>
                @endif
                    {!! Form::hidden('sup_name_loc', old('sup_name_loc'), ['id'=>'sup_name_loc','class' => 'form-control', 'placeholder' => '']) !!}
                </div>

    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="about_featured">
        <div class="panel-group" id="accordion">

            <div class="panel panel-default wow fadInLeft">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#11">
                            <span class="fa fa-check-square-o"></span><b>Product List</b>
                        </a>
                    </h4>
                </div>

                <div id="11" class="panel-collapse collapse in">


                    <div class="panel-body pad table-responsive">
                        <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>R</u>ow</button>
                        <table class="table table-bordered table-striped" id="users-table">
                            <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>UOM</th>
                                <th>Country of Origin</th>
                                <th>Quantity</th>
                                <th>Unit Price ($)</th>
                                <th>Total Amount ($)</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php  $counter=0 ?>
                            <tr id="line_items-1">
                            <td width="30%"><div class="form-group  @if($errors->has('product_id')) has-error @endif">

                                    {!! Form::select('line_items[product_id][1]',array(), old('product_id'), ['id' => 'line_item-product_id-1','class' => 'form-control description-data-ajax select2 dublicate_product', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                                    @if($errors->has('product_id'))
                                        <span class="help-block">
                                {{ $errors->first('product_id') }}
                                </span>
                                    @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group  @if($errors->has('line_unit_name')) has-error @endif">

                                    {!! Form::select('line_items[unit_name][1]',$UnitsModel, old('unit_name'), ['id' => 'line_item-unit_name_id-1','class' => 'form-control select2', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                                    @if($errors->has('line_unit_name'))
                                        <span class="help-block">
                                {{ $errors->first('line_unit_name') }}
                                </span>
                                    @endif
                                </div>
                            </td>

                            <td>
                                <div class="form-group  @if($errors->has('line_unit_name')) has-error @endif">

                                    {!! Form::text('line_items[country][1]', old('country'), ['id' => 'line_item-country-1','class' => 'form-control', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                                    @if($errors->has('line_unit_name'))
                                        <span class="help-block">
                                {{ $errors->first('line_unit_name') }}
                                </span>
                                    @endif
                                </div>
                            </td>

                            <td> <div class="form-group  @if($errors->has('qunity')) has-error @endif">

                            {!! Form::number('line_items[qunity][1]', old('qunity'), ['id' => 'line_item-qunity_id-1','class' => 'form-control qty test' ,'data-row-id'=>'1', 'required' => 'true','min'=>"1",'step'=>"1"]) !!}
                                @if($errors->has('line_qunity'))
                                    <span class="help-block">
                                    {{ $errors->first('line_qunity') }}
                                    </span>
                                @endif
                            </div>
                            </td>

                          

                            <td>
                            <div class="form-group  @if($errors->has('p_amount')) has-error @endif">

                            {!! Form::number('line_items[p_amount][1]', old('p_amount'), ['id' => 'line_item-p_amount_id-1','class' => 'form-control test' ,'data-row-id'=>'1', 'required' => 'true','min'=>"0",'step'=>"any"]) !!}
                                @if($errors->has('p_amount'))
                                    <span class="help-block">
                                    {{ $errors->first('p_amount') }}
                                    </span>
                                @endif
                            </div>
                            </td>
                            <td>
                                <div class="form-group  @if($errors->has('amount')) has-error @endif">

                                    {!! Form::number('line_items[amount][1]', old('amount'), ['id' => 'line_item-amount_id-1','class' => 'form-control cal_sum' , 'required' => 'true','min'=>"1"]) !!}
                                    @if($errors->has('amount'))
                                        <span class="help-block">
                                {{ $errors->first('amount') }}
                                </span>
                                    @endif
                                </div>
                            </td>
                            <td><button id="line_items-del_btn-1" onclick="FormControls.destroyLineItem('1');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>

                            </tr>
                            <input type="hidden" id="line_item-global_counter" value="<?php  echo ++$counter ?>"   />

                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>
</div>

