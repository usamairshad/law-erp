@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Order List</h1>
    </section>
    {{--@if(Gate::check('orders_create'))--}}
        {{--<a href="{{ route('admin.orders.index') }}" class="btn btn-primary pull-lef" style="margin-left: 25px; margin-bottom: 5px;">Local Order</a>--}}
        {{--<a href="{{ route('admin.orders.importindex') }}" class="btn btn-primary pull-lef" style="margin-left: 5px; margin-bottom: 5px;">Import Order</a>--}}

    {{--@endif--}}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Purchase Orders</h3>
            @if(Gate::check('orders_create'))
                <a href="{{ route('admin.orders.create') }}" class="btn btn-success pull-right">Add New Order</a>
            @endif
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Orders) > 0 ? 'datatable' : '' }}" id="order-table">
                <thead>
                <tr>
                    <th width="8%">S: No</th>
                    <th>Brand</th>
                    <th>PO No </th>
                    <th>PO Date</th>
                    <th>Performa No</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            $('#order-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.orders.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'sup_id', name: 'sup_id' },
                    { data: 'po_number', name: 'po_number' },
                    { data: 'po_date', name: 'po_date' },
                    { data: 'perfomra_no', name: 'perfomra_no' },
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},

                ]
            });
        });
    </script>
@endsection


