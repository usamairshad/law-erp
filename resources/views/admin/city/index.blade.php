@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">City</h3>
            @if(Gate::check('erp_cities_create'))
                <a href="{{ route('admin.city.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New City</a>
            @endif
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($City) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>City Name</th>
                    <th>Country Name</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($City) > 0)
                    @foreach ($City as $Citys)
                        <tr data-entry-id="{{ $Citys->id }}">
                            <td> {{$r}} </td>
                            <td>{{ $Citys->name }}</td>
                            <td>{{$Citys->country['name']}}</td>
                            <td>
                                {{--@if(Gate::check('regions_edit'))--}}
                                    <a href="{{ route('admin.city.edit',[$Citys->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                {{--@endif--}}

                                {{-- @if(Gate::check('regions_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.regions.destroy', $Region->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif --}}
                            </td>


                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection