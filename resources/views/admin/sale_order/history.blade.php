@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>SalePipelines History</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($SalePipelines) > 0 ? 'datatable' : '' }} " id="users-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Assigned To</th>
                        <th>Data bank</th>
                        <th>Prospect</th>

                        <th>Stage</th>
                        <th>Next Visit</th>
                        <th>Action</th>

                    </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            //$('.datatable').DataTable()

            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('marketing.salepipelines.history_datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'emp_name', name: 'emp_name' },
                    { data: 'databank_id', name: 'databank_id' },
                    { data: 'name', name: 'name' },

                    { data: 'stage', name: 'stage' },

                    { data: 'next_visit', name: 'next_visit' },
                    { data: 'action', name: 'action' },


                ]
            });
        });
    </script>
@endsection