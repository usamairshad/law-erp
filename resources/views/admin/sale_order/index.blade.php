@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale Order</h1>
    </section>

    {{csrf_field()}}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($saleOrder) > 0 ? 'datatable' : '' }} " id="users-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Invoice No</th>
                        <th>Customer</th>
                        <th>Status</th>
                        <th>Total Payable</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($saleOrder as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->invoice_no}}</td>
                            <td>{{$order->CustomersModel['name']}}</td>
                            <td>
                                @if ($order->status == 1)
                                 <label class="label label-success">Confirm</label>   
                                 @endif
                            </td>
                            <td>{{$order->payable_amount}}</td>
                            <td>{{$order->created_at}}</td>
                            <td>
                                <a href="" class="btn btn-primary btn-sm">View</a>
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 

    {{-- <script src="{{ url('public/js/support/quotes/list.js') }}" type="text/javascript"></script> --}}

@endsection