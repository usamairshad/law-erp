@if(count($SalePipelines) )

    @foreach ($SalePipelines as $key => $val)


        <p>Earnest money due Rs {{$SalePipelines[$key]->earnest_money_amount}} </p>
URL <a href="{{url('/')}}/marketing/salepipelines/{{$SalePipelines[$key]->id}}/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Receive Earnest Money at</a>
        <br>
        <br>
    @endforeach

@endif

@if(count($Securities) )

    @foreach ($Securities as $key => $val)


        <p>Performance Security due Rs {{$Securities[$key]->performance_security}} </p>
        URL <a href="{{url('/')}}/marketing/salepipelines/{{$Securities[$key]->id}}/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Receive Performance Security at </a>
        <br>
        <br>
    @endforeach

@endif