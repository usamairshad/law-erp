@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Approved Quotes</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Quotes) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Assigned To</th>
                    <th>Databank</th>
                    <th>Product</th>
                    <th>Quote Date</th>
                    <th>Status</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($Quotes) > 0)
                    @foreach ($Quotes as $qoute)
                        <tr data-entry-id="{{ $qoute->id }}">
                            <td>{{ $qoute->id }}</td>
                            <td>{{ $qoute->name }}</td>
                            <td>{{ $qoute->Assigned->first_name }}</td>
                            <td>{{ $qoute->databank_name->name }}</td>
                            <td>{{ $qoute->products_name->products_short_name }}</td>
                            <td>{{ $qoute->quote_date }}</td>
                            <td>{{ Config::get('support.qoute_approval_status_array.'. $qoute->quote_status) }}</td>
                            <td>{{date('d-m-Y h:m a', strtotime( $qoute->created_at)) }}</td>
                            <td>
                                <a href="{{route('admin.saleorder.createe', $qoute->id)}}" class="btn btn-primary">Generate Sale Order</a>
                                <a href="{{route('admin.saleorder.generatePDF', $qoute->id)}}" class="btn btn-danger btn-sm">Generate PDF</a>
                            </td>
                           
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection