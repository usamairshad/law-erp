@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>sale Order</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Sale Order For <b>{{$Quote->name}}</b></h3>
            <a href="{{ route('admin.saleorder.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.saleorder.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('admin.sale_order.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">

        </div>
        {!! Form::close() !!}
        @include('admin.sale_order.items_template')
        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
    </div>
@stop

@section('javascript')
        <script src="{{ url('public/js/admin/sale_order/sale_order.js') }}" type="text/javascript"></script>
@endsection

