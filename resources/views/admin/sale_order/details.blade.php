@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Quotes & Estimates</h1>
    </section>
@stop

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body basic_info" >

                    <div class="box-header with-border">
                        <h3 class="box-title">General Information</h3>

                    </div>

                    <div class="panel-body pad table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                            
                                <tr>
                                    <th>Assigned To </th>
                                    <td colspan="3">@if($Quote->assigned_to){{ $Quote->emp_name }}@else{{ 'N/A' }}@endif</td>

                                </tr>
                                <tr>
                                    <th>Data bank </th>
                                    <td>@if($Quote->databank_id){{ $Quote->databank_name }}@else{{ 'N/A' }}@endif</td>
                                    <th>Products</th>
                                    <td>@if($Quote->product_id){{ $Quote->products_short_name }}@else{{ 'N/A' }}@endif</td>
                                </tr>
                                <tr>
                                    <th>Qoute Date</th>
                                    <td>@if($Quote->quote_date){{ $Quote->quote_date }}@else{{ 'N/A' }}@endif</td>
                                    <th>Year</th>
                                    <td>@if($Quote->year){{ $Quote->year }}@else{{ 'N/A' }}@endif</td>
                                </tr>

                                <tr>

                                    <th>Quote Status</th>
                                    <td colspan="3">@if($Quote->quote_status){{ Config::get('marketing.qoute_approval_status_array.'.$Quote->quote_status)  }}@else{{ 'N/A' }}@endif</td>
                                </tr>

                                <tr>
                                    <th>Description</th>
                                    <td colspan="3">@if($Quote->description){{ $Quote->description }}@else{{ 'N/A' }}@endif</td>

                                </tr>
                                <tr>
                                    <th>Warranty ?</th>
                                    <td >@if($Quote->is_warranty == 1){{ 'Yes' }}@else{{ 'N/A' }}@endif</td>
                                    <th>Warranty Terms</th>
                                    <td >@if($Quote->warranty_text){{ $Quote->warranty_text }}@else{{ 'N/A' }}@endif</td>

                                </tr>
                                <tr>
                                    <th>Validity ?</th>
                                    <td >@if($Quote->is_valid == 1){{ 'Yes' }}@else{{ 'N/A' }}@endif</td>
                                    <th>Validity Terms</th>
                                    <td >@if($Quote->valid_text){{ $Quote->valid_text }}@else{{ 'N/A' }}@endif</td>

                                </tr>
                                <tr>
                                    <th>Payment ?</th>
                                    <td >@if($Quote->is_payment == 1){{ 'Yes' }}@else{{ 'N/A' }}@endif</td>
                                    <th>Payment Terms</th>
                                    <td >@if($Quote->payment_text){{ $Quote->payment_text }}@else{{ 'N/A' }}@endif</td>

                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="box-body products_info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Details</h3>

                    </div>
                </div>
                <div class="panel-body pad table-responsive">
                    <table class="table table-bordered table-striped">

                        <tbody>

                        <tr>
                            <th>Any Discount ?</th>
                            <td >@if($Quote->is_discount == 1){{ 'Yes' }}@else{{ 'N/A' }}@endif</td>
                            <th>Discount Value</th>
                            <td >@if($Quote->discount_value){{ $Quote->discount_value }}@else{{ 'N/A' }}@endif</td>

                        </tr>
                        <tr>
                            <th>Discount Type</th>
                            <td >@if($Quote->discount_type){{ Config::get('marketing.discount_array.'.$Quote->discount_type)  }}@else{{ 'N/A' }}@endif</td>
                            <th>Discount Amount</th>
                            <td >@if($Quote->discount_amount){{ $Quote->discount_amount }}@else{{ 'N/A' }}@endif</td>

                        </tr>
                        <tr>
                            <th>Total </th>
                            <td >@if($Quote->total){{ $Quote->total }}@else{{ 'N/A' }}@endif</td>
                            <th>Total After Discount</th>
                            <td >@if($Quote->total_after_discount){{ $Quote->total_after_discount }}@else{{ 'N/A' }}@endif</td>

                        </tr>
                        <tr>
                            <th>Is Taxable ?</th>
                            <td >@if($Quote->is_taxable == 1){{ 'Yes' }}@else{{ 'N/A' }}@endif</td>
                            <th>Tax %age </th>
                            <td >@if($Quote->tax_percentage){{ $Quote->tax_percentage }}@else{{ '0' }}@endif</td>

                        </tr>
                        <tr>
                            <th>Tax Amount </th>
                            <td >@if($Quote->tax_amount ){{ $Quote->tax_amount }}@else{{ '0' }}@endif</td>
                            <th>Total After Tax</th>
                            <td >@if($Quote->total_after_tax){{ $Quote->total_after_tax }}@else{{ 'N/A' }}@endif</td>

                        </tr>

                        <tr>
                            <th>Service Charges </th>
                            <td >@if($Quote->is_service == 1){{ 'Yes' }}@else{{ 'N/A' }}@endif</td>
                            <th>Service Value</th>
                            <td >@if($Quote->service_value){{ $Quote->service_value }}@else{{ '0' }}@endif</td>

                        </tr>
                        <tr>
                            <th>Service Type</th>
                            <td >@if($Quote->service_type){{ Config::get('marketing.discount_array.'.$Quote->service_type)  }}@else{{ 'N/A' }}@endif</td>
                            <th>Service Amount</th>
                            <td >@if($Quote->service_amount){{ $Quote->service_amount }}@else{{ 'N/A' }}@endif</td>

                        </tr>

                        <tr>
                            <th>Total Payable</th>
                            <td colspan="3">@if($Quote->total_payable){{ $Quote->total_payable }}@else{{ 'N/A' }}@endif</td>
                        </tr>

                        </tbody>
                    </table>
                </div>

                <div class="box-body products_info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Product Details</h3>

                    </div>
                </div>
                <div class="panel-body pad table-responsive">
                    <table class="table table-bordered table-striped">

                        <thead>
                        <tr>
                            <th style="width:15%;" >Product</th>
                            <th style="width:7%;">Qty</th>
                            <th style="width:10%;">Unit Price</th>
                            <th style="width:10%;">Sale Price</th>

                        </tr>
                        </thead>
                        <tbody>

                        @if(count($LineItems) )

                            @foreach ($LineItems as $key => $val)
                                <tr>

                                    <td>@if($LineItems[$key]->part_id) {{$LineItems[$key]->products_short_name}}@else{{ 'N/A' }}@endif</td>
                                    <td>@if($LineItems[$key]->qty) {{$LineItems[$key]->qty}}@else{{ '1' }}@endif</td>
                                    <td>@if($LineItems[$key]->unit_price) {{$LineItems[$key]->unit_price}}@else{{ 'N/A' }}@endif</td>
                                    <td>@if($LineItems[$key]->sale_price) {{$LineItems[$key]->sale_price}}@else{{ 'N/A' }}@endif</td>

                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    {!! Form::label('is_header', 'Include Header and footer ? ', ['class' => 'control-label']) !!}
    {{ Form::checkbox('is_header', 1, old('is_header') , ['id' => 'is_header'] ) }}
    <button onclick="print_quote();" type="button" style="margin-bottom: 5px;" class="btn  btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Print Quote</button>
<?php
    $words = explode(" ", $Quote->emp_name);
    $acronym = "";

    foreach ($words as $w) {
    $acronym .= ucfirst($w[0]);
    }
    $q_date = date_create($Quote->quote_date);
    $formatted_date =  date_format($q_date,"F j, Y");
    ?>

    <!DOCTYPE html>

    <style>
        @page { size: auto;  margin: 0mm; }
    </style>
    <html>
    <head>
        <title>project name</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width ,initial-scale=1 ,shrink-to-fit=no">



    </head>
    <body>
    <div  style="display: none">
    <div id="header_info">
        <header  style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box; border-collapse: collapse;padding: 0;text-align: center;border-bottom: 1px solid black;margin-bottom: 5px;">
            <p style="float: left;font-family: sans-serif;width: 40%;margin: 0;margin-top: 15px;text-align: left;font-size: 14px;padding-left: 20px;">Distributor for Pakistan</p>
            <p style="color: lightblue;float: right;font-family: serif;padding-right: 230px;text-align: right;font-size: 36px;margin: 0;">OAG</p>
        </header>
    </div>
    <div style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
        <div id="basic_info">
        <!-- header-heading-section-starts -->
        <div style="width: 100%;margin: 0;float: left;" >
            <div style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                <div style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;margin-bottom: 0;">
                    <h4 style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;margin: 0;font-weight: bold;font-size: 16px;">
                        {{$Quote->databank_name}}<br> {{$Quote->street}}<BR> {{$Quote->city}}
                    </h4>
                    <h4 style="width: 12%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;min-height: 2px;font-weight: bold;font-size: 15px;"></h4>
                    <h4 style="width: 26%;padding-top: 10px;float: right;font-family: sans-serif;margin: 0;font-weight: bold;text-align: left;font-size: 16px;">
                        REF # OAG/{{$acronym}}/{{$QuoteCount}}/{{$Quote->year}}<br> DATE: {{$formatted_date}}
                    </h4>
                </div>
            </div>
            <div class=" message_div" style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                <p style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px;font-weight: 500;margin-bottom: 0;">Dear Sir,</p>
                <p style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px;font-weight: 500;">We have made visit on your {{ $Quote->products_short_name }} machine and have observed that following Consumables/parts are required to be replaced for the trouble free operations of your machine</p>
            </div>
        </div>
        <!-- header-heading-section-ends -->
        <!-- main_table_div_starts -->
        <div style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
            <table class="table_description" style="width: 100%;border-collapse: collapse;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                <tbody style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                <tr style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;padding: 1px 0;">
                    <th style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 17px;text-align: left;"><u>DESCRIPTION</u> </th>
                    <th style="width: 9.4%;float: right;font-family: sans-serif; text-align: left;font-size: 17px;"><u> PRICE</u> </th>
                </tr>

                @if(count($LineItems) )

                    @foreach ($LineItems as $key => $val)
                        <tr style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;padding: 1px 0;">
                            <td style="width: 80%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 14px; text-align: left;">
                                <b> Replacement of @if($LineItems[$key]->qty > 1) {{ $LineItems[$key]->qty  }} Nos. @endif
                                {{ $LineItems[$key]->products_short_name }}  @if($LineItems[$key]->qty > 1) @ Rs. {{ $LineItems[$key]->unit_price  }} each @endif </b> </td>
                            <td style="width: 20%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 14px; text-align: right;">
                                  @if($LineItems[$key]->sale_price)  @if(count($LineItems) - $key == 1) <u>  @endif  Rs. {{ number_format((float)$LineItems[$key]->sale_price, 2, '.', '') }}@else{{ '0' }}@endif </u> </td>
                        </tr>

                    @endforeach


                <tr style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;padding: 1px 0;">
                    <th style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px; text-align: left;">Total Amount</th>
                    <td style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px; text-align: right;">
                        Rs {{ number_format((float)$Quote->total, 2, '.', '')}}</td>
                </tr>
                    @if($Quote->is_discount)
                        <tr style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;padding: 1px 0;">
                            <td style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px; text-align: left;">
                                Discount {{$Quote->discount_value}} @if($Quote->discount_type == 2)  {{'%'}} @endif </td>
                            <td style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px; text-align: right;"> Rs {{ number_format((float)$Quote->discount_amount, 2, '.', '')}} </td>
                        </tr>
                    @endif

                    @if($Quote->is_taxable)
                        <tr style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;padding: 1px 0;">
                            <td style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px; text-align: left;">
                                Add {{$Quote->tax_percentage}}% General Sales Tax</td>
                            <td style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px; text-align: right;"> Rs {{ number_format((float)$Quote->tax_amount, 2, '.', '')}} </td>
                        </tr>
                    @endif

                    @if($Quote->is_service)
                        <tr style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;padding: 1px 0;">
                            <td style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px; text-align: left;">
                                Add {{$Quote->service_value}} @if($Quote->service_type == 2)  {{'%'}} @endif Service charges</td>
                            <td style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px; text-align: right;">
                                Rs {{ number_format((float)$Quote->service_amount, 2, '.', '')}} </td>
                        </tr>
                    @endif
                <tr style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;padding: 1px 0;">
                    <th style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px;text-align: left;">Total payable Amount</th>
                    <th style="width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse; text-align: right;"><u> Rs {{ number_format((float)$Quote->total_payable, 2, '.', '')}}</u> </th>
                </tr>
                @endif
                </tbody>
            </table>
        </div>
        <!-- main-table-div-ends -->
        <div class=" message_div_footer" style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
            <p style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;font-size: 15px;"> We are sure you will appericiate our timely information to your
                honored office to keep manage your machine trouble free and smooth with above replacement</p>
        </div>
        <!-- footer-table-div-starts -->
        <div style="margin-bottom: 10px;width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
            <table class="terms_condition_table" style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                <tbody style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                <tr style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                    <th style="font-size: 17px;width: 50%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;text-align: left;margin-bottom: 10px;">TERMS AND CONDITIONS: </th>
                </tr>
                @if($Quote->is_warranty)
                    <tr style="margin-bottom: 2px;width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                        <th style="float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;width: 16%;width: 15%;font-size: 15px;text-align: left;"><u>WARRANTY:</u> </th>
                        <td style=" float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;width: 85%;font-size: 15px;">{{$Quote->warranty_text}}</td>
                    </tr>
                @endif

                @if($Quote->is_payment)
                    <tr style="margin-bottom: 2px;width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                        <th style="float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;width: 16%;width: 15%;font-size: 15px;text-align: left;"><u>PAYMENT:</u> </th>
                        <td style=" float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;width: 85%;font-size: 15px;">{{$Quote->payment_text}}</td>
                    </tr>
                @endif

                @if($Quote->is_valid)
                    <tr style="margin-bottom: 2px;width: 100%;font-size: 15px;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
                        <th style="float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;width: 16%;width: 15%;font-size: 15px;text-align: left;"><u>VALIDITY:</u> </th>
                        <td style=" float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;width: 85%;font-size: 15px;">{{$Quote->valid_text}}</td>
                    </tr>

                @endif
                </tbody>
            </table>
        </div>
        <!-- footer-table-div-ends -->
        <div class=" message_div_footer" style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;">
            <p style="font-size: 15px;font-weight: 500;">We assure you our best cooperation at all time and looking forward for your kind prompt approval for the replacement of above Consumables/parts</p>
            <p style="font-size: 15px;font-weight: 500;margin:0 ;"> Yours Sincerely,</p>
        </div>
        <div style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;border-collapse: collapse;margin: 0;">
            <p style="font-size: 15px;font-weight: 500;margin-bottom: 0;"><b>SHAYAN TAHIR BUTT</b></p>
            <p style="font-size: 15px;font-weight: 500;"> CUSTOMER SERVICES MANAGER</p>
        </div>
        </div>

        <!-- footer-text-div-starts -->
        <div id="footer_info" >
        <footer  style="width: 100%;float: left;font-family: sans-serif;box-sizing: border-box;font-size: 7px;text-align: left; position: fixed; bottom: 0;">
            <table style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;text-align: left;">
                <!-- 0 -->
                <!-- <tr style="width: 3%;float: left;height: 10px;"></tr> -->
                <!-- 1 -->
                <tr style="width: 12%;float: left;font-size: 7px;font-family: sans-serif;text-align: left;">
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">KARACHI :</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">117 - Ceasam Tow.</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Shatrah-e-Faisal Road. Karam,</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Tel: 021-328 02670</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Fax: 92-21-328 02671</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">E-mail: karachi@oag.com.pk</td>
                </tr>
                <!-- 2 -->
                <tr style="width: 12%;float: left;font-size: 7px;font-family: sans-serif;text-align: left;">
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">ISLAMABAD :</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Sul 8 1. 2nd Floor</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Rata Mansion Rua Fazel-e-Haq Road, Blue Area.</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">islamabad</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Tel: 051-280 6356-58</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Fax: 92-51280 6359</td>
                </tr>
                <!-- 3 -->
                <tr style="width: 12%;float: left;font-size: 7px;font-family: sans-serif;text-align: left;">
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;"></td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">PESHAWAR</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Gul Hay Plaza : -• , Jund Raw. </td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">University R. ' :haw</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Tel: 091-584 .54: • -!C 1457</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Fax: 92-91.570 , ii</td>
                </tr>
                <!-- 4 -->
                <tr style="width: 12%;float: left;font-size: 7px;font-family: sans-serif;text-align: left;">
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;"></td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">FAISALABAD :</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">. Ground Roor, 6-Chanab Marke</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Medina Town. Faisalabad</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Tel: 041-850 3263 850 3264</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Fax: 92-41-850 3265</td>
                </tr>
                <!-- 5 -->
                <tr style="width: 12%;float: left;font-size: 7px;font-family: sans-serif;text-align: left;">
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;"></td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">SIALKOT :</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">31-33 Marna meal Market. </td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Paris Road Sabot</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Tel: 052.458 0301, 459 8534 </td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Fax: 92.52-459 3505</td>
                </tr>
                <!-- 6 -->
                <tr style="width: 12%;float: left;font-size: 7px;font-family: sans-serif;text-align: left;">
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;"></td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">MULTAN :</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Rashida Abed Chowk.</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Ibunewal Road. Mohan Market Wow Chowk. Gujran.ala</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Tel: 061-223 928</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Fax: 9261-223 928</td>
                </tr>
                <!-- 7 -->
                <tr style="width: 12%;float: left;font-size: 7px;font-family: sans-serif;text-align: left;">
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;"></td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">GUJRANWALA :</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Said Ram. !mil Corpse(</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Ibunewal Road. Mohan Market Wow Chowk. Gujran.ala</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Tel: 055.373 3442</td>
                    <td style="width: 100%;float: left;font-size: 7px;font-family: sans-serif;">Fax: 055-373 344:</td>
                </tr>
                <tr style="width: 16%;float: right;margin-top: -55px;">
                    <td style="width: 100%;float: left;text-align: center;margin: 0;margin-top: 10px;margin-bottom: -12px;"><img src ="../../../../public/adminlte/dist/img/mono.png" style="width: 45px;float: none;display: inline-block;"></td>
                    <td style="width: 100%;float: left;font-size: 13px;margin-top: 15px;text-align: center;">KONICA MINOLTA</td>
                    <td style="width: 100%;float: left;font-size: 9px;letter-spacing: 5px;text-align: center;">DIGITAL COPERS</td>
                </tr>
            </table>
        </footer>
        </div>
        <!-- footer-text-div-ends -->
    </div>
    </div>

    </body>
    </html>




@stop

@section('javascript')
    <script src="{{ url('public/js/support/quotes/create_modify.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
      //  print_quote();
function print_quote(){

   // console.log('I am printing it');

    var is_header = $('#is_header').is(':checked');

    var content = document.getElementById('basic_info').innerHTML;
    var headerContent = document.getElementById('header_info').innerHTML;
    var footerContent = document.getElementById('footer_info').innerHTML;
    var mywindow = window.open('', 'Print', 'height=600,width=800');
    if(is_header){
        mywindow.document.write('<html><head>'+ headerContent +'');
        mywindow.document.write('</head><body >');
    }

    mywindow.document.write(content);

    if(is_header){
        mywindow.document.write('<html><foot>'+ footerContent +'');
        mywindow.document.write('</foot><body >');
    }

    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.focus()
    mywindow.print();
    mywindow.close();

    return true;
}

    </script>
@endsection