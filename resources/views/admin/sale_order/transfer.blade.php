@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale Pipeline Transfer</h1>
    </section>
    {!! Form::open(['method' => 'POST', 'route' => ['marketing.salepipelines.transfer_salepipelines'], 'id' => 'validation-form']) !!}
    {{csrf_field()}}
    <input type="hidden" name="name" value="abc">
    <input type="hidden" name="selectedPipelines" value="" id="selectedPipelines">
    <div class="form-group col-md-3 @if($errors->has('transfer_from')) has-error @endif" id="from_div">
        {!! Form::label('transfer_from', 'Transfer From', ['class' => 'control-label']) !!}
        {!! Form::select('transfer_from', $ChildEmployees , old('transfer_from'),
         ['class' => 'form-control select'  ,'id' => 'transfer_from' , 'required' => 'true']) !!}
    </div>

    <div class="form-group col-md-2 @if($errors->has('search_button')) has-error @endif" >
        {!! Form::label('search_button', ' ', ['class' => 'control-label']) !!}
        <button id="search_button" onclick="FormControls.fetchFilterRecord();" type="button" style="margin-top: 5px;" class="btn  btn-sm btn-flat btn-primary form-control"><b>&nbsp;Search </b> </button>
    </div>

    <div class="form-group col-md-3 @if($errors->has('transfer_to')) has-error @endif" id="to_div">
        {!! Form::label('transfer_to', 'Transfer To', ['class' => 'control-label']) !!}
        {!! Form::select('transfer_to', $ChildEmployees , old('transfer_to'),
         ['class' => 'form-control'  ,'id' => 'transfer_to' , 'required' => 'true']) !!}
    </div>

    <div class="form-group col-md-2 @if($errors->has('transfer_button')) has-error @endif" >
        {!! Form::label('transfer_button', ' ', ['class' => 'control-label']) !!}
        {!! Form::submit('Transfer', ['class' => 'btn btn-danger form-control' , 'style' => 'margin-top: 5px;']) !!}

    </div>

    {!! Form::close() !!}

    <br>
    <br>
@stop

@section('content')
    <div class="box box-primary">

        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>

        </div>

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($SalePipelines) > 0 ? 'datatable' : '' }} " id="users-table">
                <thead>
                <tr>
                    <th>All <input type="checkbox" name="checkAll" value='checkAll' id="checkAll"> </th>
                    <th>ID</th>
                    <th>Assigned To</th>
                    <th>Data bank</th>
                    <th>Prospect</th>
                    <th>Stage</th>
                    <th>Next Visit</th>


                </tr>
                </thead>


            </table>
        </div>
    </div>
@stop

@section('javascript') 

    <script src="{{ url('public/js/marketing/salepipelines/transfer.js') }}" type="text/javascript"></script>
@endsection
