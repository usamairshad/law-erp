
<div class="form-group col-md-3 @if($errors->has('assigned_to')) has-error @endif">
        {!! Form::label('assigned_to', 'Assigned To *', ['class' => 'control-label']) !!}
        {!! Form::select('assigned_to', $Employees, $Quote->assigned_to, ['class' => 'form-control select2', 'readonly', 'disabled']) !!}
        @if($errors->has('assigned_to'))
            <span class="help-block">
                {{ $errors->first('assigned_to') }}
            </span>
        @endif
    </div>
    <input type="hidden" id="name" name="name" value=" Quote Detail" />
    <input type="hidden" id="branch_id" name="branch_id" value="{{$Employee->branch_id}}" />
    <input type="hidden" id="order_id" name="order_id" value="{{$order_id}}" />
    <input type="hidden" id="customer_id" name="customer_id" value="{{$Quote->databank_id}}" />
    
    <div class="form-group col-md-3 @if($errors->has('databank_id')) has-error @endif">
        {!! Form::label('databank_id', 'Data bank *', ['class' => 'control-label']) !!}
        {!! Form::select('databank_id', $Databanks, $Quote->databank_id, ['class' => 'form-control select2' , 'readonly', 'disabled']) !!}
    
        @if($errors->has('databank_id'))
            <span class="help-block">
                {{ $errors->first('databank_id') }}
            </span>
        @endif
    </div>
    
    <div class="form-group col-md-3 @if($errors->has('product_id')) has-error @endif">
        {!! Form::label('product_id', 'Product *', ['class' => 'control-label']) !!}
        {{-- {!! Form::select('product_id', old('product_id'), [ 'id'=>'product_id', 'class' => 'form-control select2' , 'readonly', 'disabled']) !!} --}}
        <input type="text" name="" value="{{$Quote->products->products_short_name}}" class="form-control" readonly>
        @if($errors->has('product_id'))
            <span class="help-block">
                {{ $errors->first('product_id') }}
            </span>
        @endif
    </div>
    
    <div class="form-group col-md-3  @if($errors->has('year')) has-error @endif">
        {!! Form::label('year', 'Year', ['class' => 'control-label']) !!}
        {{-- {!! Form::select('year', array() + Config::get('support.year_array'), old('year'), ['class' => 'form-control', 'readonly', 'disabled']) !!} --}}
        <input type="text" name="" value="{{$Quote->year}}" class="form-control" readonly>
        @if($errors->has('year'))
            <span class="help-block">
                {{ $errors->first('year') }}
            </span>
        @endif
    </div>
    
    
    <div class="form-group col-md-12 @if($errors->has('description')) has-error @endif">
            {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
            {!! Form::textarea('description',$Quote->description, ['size'=> '80x5','class' => 'form-control', 'readonly']) !!}
            @if($errors->has('description'))
                    <span class="help-block">
                {{ $errors->first('description') }}
            </span>
            @endif
    </div>
    
    
    <div class="form-group col-md-3  @if($errors->has('quote_date')) has-error @endif" >
        {!! Form::label('quote_date', 'Quote Date *', ['class' => 'control-label']) !!}
        {!! Form::text('quote_date', $Quote->quote_date, ['class' => 'form-control datepicker','readonly', 'disabled']) !!}
        @if($errors->has('quote_date'))
            <span class="help-block">
                {{ $errors->first('quote_date') }}
            </span>
        @endif
    </div>
    
    <div class="form-group col-md-3  @if($errors->has('quote_status')) has-error @endif">
        {!! Form::label('quote_status', 'Quote Status', ['class' => 'control-label']) !!}
        {!! Form::select('quote_status', array() + Config::get('support.qoute_approval_status_array'), old('quote_status'), ['class' => 'form-control', 'readonly', 'disabled']) !!}
        @if($errors->has('quote_status'))
            <span class="help-block">
                {{ $errors->first('quote_status') }}
            </span>
        @endif
    </div>
    
    <div  style="width: 100%; float: left;">
    
    </div>
    
    <div class="form-group col-md-3  @if($errors->has('is_warranty')) has-error @endif">
        {!! Form::label('is_warranty', 'Warranty ? ', ['class' => 'control-label']) !!}
        {{-- {{ Form::checkbox('is_warranty', $Quote->is_warranty, old('is_warranty')  ) }} --}}
        @if($Quote->is_warranty == 0)
            <input type="checkbox" name="" id="" disabled>
        @else
            <input type="checkbox" name="" id="" checked disabled>
        @endif
          
    </div>
    
    <div class="form-group col-md-9  @if($errors->has('warranty_text')) has-error @endif" >
        {!! Form::label('warranty_text', 'Warranty Terms', ['class' => 'control-label']) !!}
        {!! Form::text('warranty_text', $Quote->warranty_text, ['class' => 'form-control', 'readonly']) !!}
        @if($errors->has('warranty_text'))
            <span class="help-block">
                {{ $errors->first('warranty_text') }}
            </span>
        @endif
    </div>
    
    <div class="form-group col-md-3  @if($errors->has('is_valid')) has-error @endif">
        {!! Form::label('is_valid', 'Validity ?') !!}
        {{-- {{ Form::checkbox('is_valid', 1, old('is_valid')  ) }} --}}
        @if($Quote->is_valid == 0)
            <input type="checkbox" name="" id="" disabled>
        @else
            <input type="checkbox" name="" id="" checked disabled>
        @endif
    </div>
    
    <div class="form-group col-md-9  @if($errors->has('valid_text')) has-error @endif" >
        {!! Form::label('valid_text', 'Validity Terms ', ['class' => 'control-label']) !!}
        {!! Form::text('valid_text', $Quote->valid_text, ['class' => 'form-control', 'readonly']) !!}
        @if($errors->has('valid_text'))
            <span class="help-block">
                {{ $errors->first('valid_text') }}
            </span>
        @endif
    </div>
    
    <div class="form-group col-md-3  @if($errors->has('is_payment')) has-error @endif">
        {!! Form::label('is_payment', 'Payment ? ', ['class' => 'control-label']) !!}
        {{-- {{ Form::checkbox('is_payment', 1, old('is_payment')  ) }} --}}
        @if($Quote->is_payment == 0)
            <input type="checkbox" name="" id="" disabled>
        @else
            <input type="checkbox" name="" id="" checked disabled>
        @endif
    </div>
    
    <div class="form-group col-md-9  @if($errors->has('payment_text')) has-error @endif" >
        {!! Form::label('payment_text', 'Payment Terms', ['class' => 'control-label']) !!}
        {!! Form::text('payment_text', $Quote->payment_text, ['class' => 'form-control', 'readonly']) !!}
        @if($errors->has('payment_text'))
            <span class="help-block">
                {{ $errors->first('payment_text') }}
            </span>
        @endif
    </div>
    
    
    
    
    
    
    
    
    <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="about_featured">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default wow fadInLeft">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#11">
                                    <span class="fa fa-check-square-o"></span><b>Products </b>
                                </a>
                            </h4>
                        </div>
        
                        <div id="11" class="panel-collapse collapse in">
                            <div class="panel-body pad table-responsive">
                                {{-- <button onclick="FormControls.createOtherLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add Other <u>R</u>ow</button> --}}
                                <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add Product <u>R</u>ow</button>
                                <table class="table table-bordered table-striped" id="users-table">
                                    <thead>
                                        <tr>
                                            <th style="width:15%;" >Product</th>
                                            <th style="width:7%;">Quantity</th>
                                            <th style="width:12%;" colspan="2">List Price</th>
                                            <th style="width:7%;">Tax %</th>
                                            <th style="width:10%;">Tax Amount</th>
                                            <th style="width:10%;">Sale Price</th>
                                            <th style="width:15%;">Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  $product_counter = 0 ?>
                                            @if(count($LineItems) )
            
                                                @foreach ($LineItems as $key => $val)
            
                                                    <?php   $product_counter++ ?>
                                                    @if($LineItems[$key]->type == 'product')
            
                                                    <tr id="line_item-{{$product_counter}}">
                                                        <td> <div class="form-group ">
            
                                                            {!! Form::select('line_items[product_id]['.$product_counter.']', $Products, $LineItems[$key]->product_id, ['id' => 'line_item-product_id-'.$product_counter.'','class' => 'form-control select2',
                                                                'onchange' => 'FormControls.RefreshLineItem();',
                                                                ])
                                                            !!}
                                                            </div>
                                                        </td>
            
                                                        <td><div class="form-group  @if($errors->has('databank_id')) has-error @endif">
            
                                                                {!! Form::number('line_items[product_qty]['.$product_counter.']', $LineItems[$key]->product_qty,
                                                                ['class' => 'form-control','id' => 'line_item-product_qty-'.$product_counter.'', 'maxlength'=>'10', 'min'=> '1',
            
                                                                    'onkeydown' => 'FormControls.CalculateTotal('.$product_counter.');',
                                                                    'onkeyup' => 'FormControls.CalculateTotal('.$product_counter.');',
                                                                    'onblur' => 'FormControls.CalculateTotal('.$product_counter.');']) !!}
                                                            </div>
                                                        </td>
                                                        <td colspan="2">
                                                                <div class="form-group  @if($errors->has('list_price')) has-error @endif">
                
                                                                    {!! Form::text('line_items[list_price]['.$product_counter.']', $LineItems[$key]->list_price,
                                                                    ['class' => 'form-control','id' => 'line_item-list_price-'.$product_counter.'','maxlength'=>'20','min'=> '0',
                                                                    'onkeydown' => 'FormControls.CalculateTotal('.$product_counter.');',
                                                                        'onkeyup' => 'FormControls.CalculateTotal('.$product_counter.');',
                                                                        'onblur' => 'FormControls.CalculateTotal('.$product_counter.');'
                                                                    ]) !!}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                    <div class="form-group  @if($errors->has('tax_percent')) has-error @endif">
                    
                                                                        {!! Form::select('line_items[tax_percent]['.$product_counter.']', array() + Config::get('marketing.tax_array'), $LineItems[$key]->tax_percent,
                                                                        ['id' => 'line_item-tax_percent-'.$product_counter.'','class' => 'form-control',
                                                                            'onchange' => 'FormControls.CalculateTotal('.$product_counter.');'
                    
                                                                            ]) !!}
                                                                    </div>
                                                                </td>
                    
                                                            <td>
                                                                <div class="form-group" style="margin-bottom: 0px !important;">
                                                                    {!! Form::number('line_items[tax_amount]['.$product_counter.']', $LineItems[$key]->tax_amount,
                                                                    ['class' => 'form-control line_item_tax','id' => 'line_item-tax_amount-'.$product_counter.'', 'readonly' => 'true'
                                                                    ]) !!}
                                                                </div>
                                                            </td>
                
                                                        <td>
                                                            <div class="form-group  @if($errors->has('sale_price')) has-error @endif">
            
                                                                {!! Form::number('line_items[sale_price]['.$product_counter.']', $LineItems[$key]->sale_price,
                                                                ['class' => 'form-control','id' => 'line_item-sale_price-'.$product_counter.'', 'readonly' => 'true'
            
                                                                ]) !!}
                                                            </div>
                                                        </td>
                                                        <td><button id="line_item-del_btn-1" onclick="FormControls.destroyLineItem('{{$product_counter}}');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
            
                                                    </tr>
            
                                                    <tr id="desc_item-{{$product_counter}}">
            
                                                        <td >
                                                            <b>Discount</b>
                                                        </td>
            
                                                        <td>
                                                            <div class="form-group  @if($errors->has('discount')) has-error @endif">
            
                                                                {!! Form::number('line_items[discount]['.$product_counter.']', $LineItems[$key]->discount,
                                                                ['class' => 'form-control dr_discount','id' => 'line_item-discount-'.$product_counter.'','maxlength'=>'20', 'min'=> '1',
                                                                    'onkeydown' => 'FormControls.CalculateTotal('.$product_counter.');',
                                                                    'onkeyup' => 'FormControls.CalculateTotal('.$product_counter.');',
                                                                    'onblur' => 'FormControls.CalculateTotal('.$product_counter.');'
                                                                ]) !!}
                                                            </div>
                                                        </td>
            
                                                        <td>
                                                            <b>Misc Amount</b>
                                                        </td>
                                                        <td>
                                                            <div class="form-group" style="margin-bottom: 0px !important;">
                                                                {!! Form::number('line_items[misc_amount]['.$product_counter.']', $LineItems[$key]->misc_amount,
                                                                ['class' => 'form-control line_misc_amount','id' => 'line_item-misc_amount-'.$product_counter.'', 'maxlength'=>'20', 'min'=> '0',
                                                                'onkeydown' => 'FormControls.CalculateTotal('.$product_counter.');',
                                                                'onkeyup' => 'FormControls.CalculateTotal('.$product_counter.');',
                                                                'onblur' => 'FormControls.CalculateTotal('.$product_counter.');'
                                                                ]) !!}
                                                            </div>
                                                        </td>
            
                                                        <td>
                                                            <b>Total </b>
                                                        </td>
                                                        <td>
                                                            <div class="form-group" style="margin-bottom: 0px !important;">
                                                                {!! Form::number('line_items[total_amount]['.$product_counter.']', $LineItems[$key]->total_amount,
                                                                ['class' => 'form-control line_item_total','id' => 'line_item-total_amount-'.$product_counter.'',
                                                                'readonly' => 'true'
                                            
                                                                ]) !!}
                                                            </div>
                                                        </td>
            
                                                        <td>
                                                            <b>Net Income </b>
                                                        </td>
                                                        <td colspan="2">
                                                            <div class="form-group" style="margin-bottom: 0px !important;">
                                                                {!! Form::number('line_items[net_income]['.$product_counter.']', $LineItems[$key]->net_income,
                                                                ['class' => 'form-control line_net_income','id' => 'line_item-net_income-'.$product_counter.'',
                                                                'readonly' => 'true'
            
                                                                ]) !!}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @else
            
                                                        <tr id="line_item-{{$product_counter}}">
            
                                                            <td>
                                                                <div class="form-group  @if($errors->has('tax_percent')) has-error @endif">
            
                                                                    {!! Form::text('line_items[type]['.$product_counter.']', $LineItems[$key]->type,
                                                                    ['class' => 'form-control','id' => 'line_item-type-'.$product_counter.'' , 'readonly' => 'true' ]) !!}
            
                                                                </div>
                                                            </td>
            
            
                                                            <td> <div class="form-group ">
            
                                                                    {!! Form::select('line_items[product_id]['.$product_counter.']', $Products, $LineItems[$key]->product_id, ['id' => 'line_item-product_id-'.$product_counter.'','class' => 'form-control select2',
                                                                        'onchange' => 'FormControls.RefreshLineItem();',
                                                                        ])
                                                                    !!}
                                                                </div>
                                                            </td>
            
                                                            <td><div class="form-group  @if($errors->has('product_qty')) has-error @endif">
            
                                                                    {!! Form::number('line_items[product_qty]['.$product_counter.']', $LineItems[$key]->product_qty,
                                                                    ['class' => 'form-control','id' => 'line_item-product_qty-'.$product_counter.'', 'maxlength'=>'10', 'min'=> '1']) !!}
                                                                </div>
                                                            </td>
            
                                                            <td>
                                                                <div class="form-group  @if($errors->has('sale_price')) has-error @endif">
            
                                                                    {!! Form::number('line_items[sale_price]['.$product_counter.']', $LineItems[$key]->sale_price,
                                                                    ['class' => 'form-control','id' => 'line_item-sale_price-'.$product_counter.'', 'readonly' => 'true'
            
                                                                    ]) !!}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group" style="margin-bottom: 0px !important;">
                                                                    {!! Form::number('line_items[tax_amount]['.$product_counter.']', $LineItems[$key]->tax_amount,
                                                                    ['class' => 'form-control line_item_tax','id' => 'line_item-tax_amount-'.$product_counter.'', 'readonly' => 'true'
                                                                    ]) !!}
                                                                </div>
                                                            </td>
            
                                                            <td>
                                                                <div class="form-group  @if($errors->has('tax_percent')) has-error @endif">
            
                                                                    {!! Form::text('line_items[tax_percent]['.$product_counter.']', $LineItems[$key]->tax_percent,
                                                                    ['id' => 'line_item-tax_percent-'.$product_counter.'','class' => 'form-control','readonly' => 'true' ]) !!}
                                                                </div>
                                                            </td>
            
                                                            <td colspan="2">
                                                                <div class="form-group  @if($errors->has('list_price')) has-error @endif">
            
                                                                    {!! Form::text('line_items[list_price]['.$product_counter.']', $LineItems[$key]->list_price,
                                                                    ['class' => 'form-control','id' => 'line_item-list_price-'.$product_counter.'', 'readonly' => 'true']) !!}
                                                                </div>
                                                            </td>
            
            
            
            
                                                            <td><button id="line_item-del_btn-1" onclick="FormControls.destroyLineItem('{{$product_counter}}');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
            
                                                        </tr>
            
                                                        <tr id="desc_item-{{$product_counter}}">
            
                                                            <td >
                                                                <b>Discount</b>
                                                            </td>
            
                                                            <td>
                                                                <div class="form-group  @if($errors->has('discount')) has-error @endif">
            
                                                                    {!! Form::number('line_items[discount]['.$product_counter.']', $LineItems[$key]->discount,
                                                                    ['class' => 'form-control dr_discount','id' => 'line_item-discount-'.$product_counter.'']) !!}
                                                                </div>
                                                            </td>
            
                                                            <td>
                                                                <b>Misc Amount</b>
                                                            </td>
                                                            <td>
                                                                <div class="form-group" style="margin-bottom: 0px !important;">
                                                                    {!! Form::number('line_items[misc_amount]['.$product_counter.']', $LineItems[$key]->misc_amount,
                                                                    ['class' => 'form-control line_misc_amount','id' => 'line_item-misc_amount-'.$product_counter.'', 'readonly' => 'true',]) !!}
                                                                </div>
                                                            </td>
            
                                                            <td>
                                                                <b>Total </b>
                                                            </td>
                                                            <td>
                                                                <div class="form-group" style="margin-bottom: 0px !important;">
                                                                    {!! Form::number('line_items[total_amount]['.$product_counter.']', $LineItems[$key]->total_amount,
                                                                    ['class' => 'form-control line_item_total','id' => 'line_item-total_amount-'.$product_counter.'',
                                                                    'readonly' => 'true'
            
                                                                    ]) !!}
                                                                </div>
                                                            </td>
            
                                                            <td>
                                                                <b>Net Income </b>
                                                            </td>
                                                            <td colspan="2">
                                                                <div class="form-group" style="margin-bottom: 0px !important;">
                                                                    {!! Form::number('line_items[net_income]['.$product_counter.']', $LineItems[$key]->net_income,
                                                                    ['class' => 'form-control line_net_income','id' => 'line_item-net_income-'.$product_counter.'',
                                                                    'readonly' => 'true'
            
                                                                    ]) !!}
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                        @endif
                                            <input type="hidden" id="line_item-global_counter" value="<?php  echo ++$product_counter ?>"   />
            
                                        </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    <div   id="total_div">
        <div class="form-group col-md-2  @if($errors->has('is_discount')) has-error @endif">
            {!! Form::label('is_discount', 'Apply Discount ? ', ['class' => 'control-label']) !!}
            {{ Form::checkbox('is_discount', 1, old('is_discount') , ['id' => 'is_discount',
            'onchange' => 'FormControls.CalculateGrandTotal();' ] ) }}
    
            @if($errors->has('is_discount'))
                <span class="help-block">
                {{ $errors->first('is_discount') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2  @if($errors->has('discount_value')) has-error @endif" >
            {!! Form::label('discount_value', 'Discount Value', ['class' => 'control-label']) !!}
            {!! Form::number('discount_value', old('discount_value'), ['class' => 'form-control',
              'onkeydown' => 'FormControls.CalculateGrandTotal();',
                'onkeyup' => 'FormControls.CalculateGrandTotal();',
                'onblur' => 'FormControls.CalculateGrandTotal();'
    
              ]) !!}
            @if($errors->has('discount_value'))
                <span class="help-block">
                {{ $errors->first('discount_value') }}
            </span>
            @endif
        </div>
    
    
        <div class="form-group col-md-2  @if($errors->has('discount_type')) has-error @endif">
            {!! Form::label('discount_type', 'Discount Type  ', ['class' => 'control-label']) !!}
            {!! Form::select('discount_type', array() + Config::get('marketing.discount_array'), old('discount_type'),
              ['id' => 'discount_type','class' => 'form-control',
                 'onchange' => 'FormControls.CalculateGrandTotal();'
                 ]) !!}
            @if($errors->has('discount_type'))
                <span class="help-block">
                {{ $errors->first('discount_type') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2  @if($errors->has('discount_amount')) has-error @endif" >
            {!! Form::label('discount_amount', 'Discount Amount', ['class' => 'control-label']) !!}
            {!! Form::text('discount_amount', old('discount_amount'), ['class' => 'form-control', 'readonly' => 'true']) !!}
            @if($errors->has('discount_amount'))
                <span class="help-block">
                {{ $errors->first('discount_amount') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2 @if($errors->has('total')) has-error @endif" >
            {!! Form::label('total', 'Total *', ['class' => 'control-label']) !!}
            {!! Form::text('total', old('total'), ['class' => 'form-control', 'readonly' => 'true']) !!}
            @if($errors->has('total'))
                <span class="help-block">
                {{ $errors->first('total') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2 @if($errors->has('total_after_discount')) has-error @endif" >
            {!! Form::label('total_after_discount', 'Total After discount*', ['class' => 'control-label']) !!}
            {!! Form::text('total_after_discount', old('total_after_discount'), ['class' => 'form-control', 'readonly' => 'true']) !!}
            @if($errors->has('total_after_discount'))
                <span class="help-block">
                {{ $errors->first('total_after_discount') }}
            </span>
            @endif
        </div>
    
    
    
        <div class="form-group col-md-2  @if($errors->has('is_taxable')) has-error @endif">
            {!! Form::label('is_taxable', 'Apply Tax ? ', ['class' => 'control-label']) !!}
            {{ Form::checkbox('is_taxable', 1, old('is_taxable') ,['onchange' => 'FormControls.CalculateGrandTotal();'] ) }}
            @if($errors->has('is_taxable'))
                <span class="help-block">
                {{ $errors->first('is_taxable') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2  @if($errors->has('tax_percentage')) has-error @endif">
            {!! Form::label('tax_percentage', 'Tax % ', ['class' => 'control-label']) !!}
            {!! Form::select('tax_percentage', array() + Config::get('marketing.tax_array'), old('tax_percentage'),
              ['id' => 'tax_percentage','class' => 'form-control',
                 'onchange' => 'FormControls.CalculateGrandTotal();'
    
                 ]) !!}
            @if($errors->has('is_taxable'))
                <span class="help-block">
                {{ $errors->first('is_taxable') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2  @if($errors->has('tax_amount')) has-error @endif" >
            {!! Form::label('tax_amount', 'Tax Amount', ['class' => 'control-label']) !!}
            {!! Form::text('tax_amount', old('tax_amount'), ['class' => 'form-control' , 'readonly' => 'true']) !!}
            @if($errors->has('tax_amount'))
                <span class="help-block">
                {{ $errors->first('tax_amount') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2 @if($errors->has('total_after_tax')) has-error @endif" >
            {!! Form::label('total_after_tax', 'Total After Tax', ['class' => 'control-label']) !!}
            {!! Form::text('total_after_tax', old('total_after_tax'), ['class' => 'form-control', 'readonly' => 'true']) !!}
            @if($errors->has('total_after_tax'))
                <span class="help-block">
                {{ $errors->first('total_after_tax') }}
            </span>
            @endif
        </div>
    
    
        <div class="form-group col-md-2  @if($errors->has('is_service')) has-error @endif">
            {!! Form::label('is_service', 'Apply Service Charges ? ', ['class' => 'control-label' ,
             ]) !!}
            {{ Form::checkbox('is_service', 1, old('is_service') ,[ 'onchange' => 'FormControls.CalculateGrandTotal();'] ) }}
            @if($errors->has('is_service'))
                <span class="help-block">
                {{ $errors->first('is_service') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2  @if($errors->has('service_value')) has-error @endif" >
            {!! Form::label('service_value', 'Service Value', ['class' => 'control-label']) !!}
            {!! Form::number('service_value', old('service_value'), ['class' => 'form-control' ,
    
             'onkeydown' => 'FormControls.CalculateGrandTotal();',
             'onkeyup' => 'FormControls.CalculateGrandTotal();',
             'onblur' => 'FormControls.CalculateGrandTotal();' ]) !!}
            @if($errors->has('service_value'))
                <span class="help-block">
                {{ $errors->first('service_value') }}
            </span>
            @endif
        </div>
    
    
        <div class="form-group col-md-2  @if($errors->has('service_type')) has-error @endif">
            {!! Form::label('service_type', 'Service Type  ', ['class' => 'control-label']) !!}
            {!! Form::select('service_type', array() + Config::get('marketing.discount_array'), old('service_type'),
              ['id' => 'service_type','class' => 'form-control',
                 'onchange' => 'FormControls.CalculateGrandTotal();'
    
                 ]) !!}
            @if($errors->has('service_type'))
                <span class="help-block">
                {{ $errors->first('service_type') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2  @if($errors->has('service_amount')) has-error @endif" >
            {!! Form::label('service_amount', 'Service Amount', ['class' => 'control-label']) !!}
            {!! Form::text('service_amount', old('service_amount'), ['class' => 'form-control', 'readonly' => 'true']) !!}
            @if($errors->has('service_amount'))
                <span class="help-block">
                {{ $errors->first('service_amount') }}
            </span>
            @endif
        </div>
    
        <div class="form-group col-md-2  @if($errors->has('service_tax')) has-error @endif">
                {!! Form::label('service_tax', 'Tax % ', ['class' => 'control-label']) !!}
                {!! Form::select('service_tax', array() + Config::get('marketing.tax_array'), old('service_tax'),
                  ['id' => 'service_tax','class' => 'form-control',
                     'onchange' => 'FormControls.CalculateGrandTotal();'
        
                     ]) !!}
                @if($errors->has('is_taxable'))
                    <span class="help-block">
                    {{ $errors->first('is_taxable') }}
                </span>
                @endif
            </div>
            <div class="form-group col-md-2  @if($errors->has('service_tax_amount')) has-error @endif" >
                    {!! Form::label('service_tax_amount', 'Tax Amount', ['class' => 'control-label']) !!}
                    {!! Form::text('service_tax_amount', old('service_tax_amount'), ['id'=> 'service_tax_amount','class' => 'form-control' , 'readonly' => 'true']) !!}
                    @if($errors->has('service_tax_amount'))
                        <span class="help-block">
                        {{ $errors->first('tax_amount') }}
                    </span>
                    @endif
                </div>
    
    <div class="form-group col-md-2 @if($errors->has('total_payable')) has-error @endif">
        {!! Form::label('total_payable', 'Total Payable ', ['class' => 'control-label']) !!}
        {!! Form::text('total_payable', old('total_payable'), ['class' => 'form-control' , 'readonly' => 'true']) !!}
        @if($errors->has('total_payable'))
            <span class="help-block">
                {{ $errors->first('total_payable') }}
            </span>
        @endif
    </div>