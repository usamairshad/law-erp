@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Quotes & Estimates </h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Quotes & Estimates</h3>
            <a href="{{ route('support.quotes.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Quote, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['support.quotes.update', $Quote->id]]) !!}
        <div class="box-body">

            @include('support.quotes.fields')
        </div>
        <!-- /.box-body -->
        {!! Form::hidden('id',  $Quote->id  , ['id' => 'id'] ) !!}

        <div class="box-footer">

        </div>

        @include('support.quotes.items_template')
        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/support/quotes/create_modify.js') }}" type="text/javascript"></script>
@endsection