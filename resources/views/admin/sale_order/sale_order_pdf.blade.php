<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
    <title>OAG</title>
    
    <style type="text/css">
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;

        }

        .container{
            position:relative;
            height: auto;
            width:auto;
            padding: 10px 25px;
        }

        .logo{
            text-align: center;
            font-size:25px;
            top:0;
            border-bottom: 1px solid black;
        }
            .office-auto{
            color: #969291;
        }

        header{
            width: 100%;
        }

        .column{
            float: left;
            width: 50%;
        }

        .terms td{
            padding:1px 0px 5px 0px;
        }

        .invoice-table td{
            border: 1px solid #000;
            padding:1px 1px;
        }

        .invoice-table table {
            border-collapse: collapse;
        }

        .paragraph{
            padding:7px 0px 7px 0px;
            font-size: 16px;
        }
        .head-td td{
            font-size: 10px;
        }

    </style>
</head>

<body>
<section class="container">    
<header>
    <div class="logo">
        <h1>OAG</h1>	
    </div>
</header>

<table width="100%;" class="head-td">
    <tr align="left">
        <th width="70%" style="font-size: 20px;">Office Automation Group</th>
        <td align="left"><strong>Head Office:</strong>8/1 Habibullah ROad,<br>Office Davis Road,Lahore 54000 Pakistan</td>
    </tr>

    <tr align="left">
        <th width="50%"></th>
        <td><strong>Tel:</strong> 042-3636 2835-38</td>
    </tr>
    <tr align="left">
        <th width="50%"><strong>{{strtoupper($quote->databank_name->name) }}<br> {{strtoupper($quote->databank_name->city )}}</strong></th>
        <td><strong>Fax:</strong>92-42 3636 2834<br><strong>E-mail:</strong>info@oag.com.pk<br><strong>Web Site:</strong> www.oag.com<br><br><span style="font-size:14px;"><strong>REF# {{$quote->ref_num}}</strong></span><br><span style="font-size:14px;"><strong>Date: {{$quote->quote_date}}</strong></span></td>
    </tr>        
</table>
<br><br><br>
<div>
    <h3>Dear Sir,</h3>
    
    <p class="paragraph">{{$quote->description}}
    </p>
</div>


<div class="invoice-table">
    <table width="99.9%">
        <tr>
            <th align="left">DESCRIPTION</th>
            <th align="left">QUANTITY</th>
            <th>UNIT PRICE</th>
            <th>TOAL PRICE</th>
        </tr>
        @foreach ($saleInvoiceDetail as $detail)
            <tr>
                <td>{{ucfirst($detail->part_id)}}</td>
                <td align="center">{{$detail->qty}}</td>
                <td align="center">Rs.{{$detail->unit_price}}</td>
                <td align="center">Rs.{{$detail->qty * $detail->unit_price}}</td>
            </tr>
        @endforeach

        <tr>
    
            <td ><strong>Total Amount.	</strong></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center" ><strong>Rs.{{$quote->total}}</strong></td>
        </tr>
        @if($quote->is_discount != 0)
            <tr>
                <td>Add Discount.</td>
                <td align="center">-</td>
                <td align="center">-</td>
                <td align="center">Rs.{{$quote->discount_value}}</td>
            </tr>
        @endif

        @if($quote->is_taxable != 0)
            <tr>
                <td>Add 17% Genral Sales Tax.</td>
                <td align="center">-</td>
                <td align="center">-</td>
                <td align="center">Rs.{{$quote->tax_amount}}</td>
            </tr>
       
        @endif

        @if($quote->is_service != 0)
            @if($quote->service_amount != NULL)
                <tr>
                    <td>Add Services Charges</td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                    <td align="center">Rs.{{$quote->service_amount}}</td>

                </tr>
            @endif
            @if($quote->service_tax != NULL)
                <tr>
                    <td>Add 16% PST</td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                    <td align="center">Rs.{{$quote->service_tax_amount}}</td>
                </tr>
            @endif
        @endif 
            
        <tr>
            <td ><strong>Net Amount.</strong></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"><strong>Rs.{{$quote->total_payable}}</strong></td>
            
        </tr> 
    </table>
</div>

<div>
    <p class="paragraph">We are sure you will appreciate our timely information to your honored office to keep manage your photocopier truoble free and smooth with above replacements.</p>
  
    <h3>TERMS AND CONDITION</h3>
    <table class="terms">
        @if($quote->is_warranty == 1)
            <tr>
                <td ><strong><h4><U>WARRENTY:</U></h4></strong></td>
                <td>
                    {{$quote->warranty_text}}
                </td>
            </tr>
        @endif
        @if($quote->is_payment == 1)
            <tr>
                <td ><strong><h4><U>PAYMENT:</U></h4></strong></td>
                <td>
                    {{$quote->payment_text}}
                </td>
            </tr>
        @endif
        @if($quote->is_valid == 1)
            <tr>
                <td ><strong><h4><U>VALIDITY:</U></h4></strong></td>
                <td>
                    {{$quote->valid_text}}
                </td>
            </tr>
        @endif
    </table>
  
    <p>We assure you our best cooperation at all time and looking forward for your kind prompt approval for the replacement of above parts.</p>
    <div>&nbsp;</div>
    <h4>Yours Sincerely,</h4>
    <div>&nbsp;</div><div>&nbsp;</div>
    <h4>SHAYAN TAHIR BUTT</h4>
    <h5>CUSTOMER SERVICES MANAGER</h5>
</div>

</section>
</body>
</html>