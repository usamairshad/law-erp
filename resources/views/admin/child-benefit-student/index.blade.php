@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Child Benefit Student</h1>
    </section>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ url('child/benefit-student') }}/create" class="btn btn-success pull-right">Add New Child Benefit</a>
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($ChildBenefitStudents) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Staff</th>
                    <th>Student</th>
                    <th>Child Benefit</th>
                    <th>Discount</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($ChildBenefitStudents) > 0)
                    @foreach ($ChildBenefitStudents as $ChildBenefitStudent)

                        <tr data-entry-id="{{ $ChildBenefitStudent->id }}">
                            <td> {{$r}} </td>
                            <td>{{$ChildBenefitStudent->staff->first_name}}</td>
                            <td>{{$ChildBenefitStudent->student->first_name}}</td>
                            <td>{{$ChildBenefitStudent->childbenefitconf->child_name}}</td>
                            <td>{{$ChildBenefitStudent->childbenefitconf->discount}} %</td>
                            <td>
                                {{--@if(Gate::check('regions_edit'))--}}
                                    <a href="{{ url('child/benefit-student',[$ChildBenefitStudent->id]) }}/edit" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                {{--@endif--}}

                                {{-- @if(Gate::check('regions_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.regions.destroy', $Region->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif --}}
                            </td>


                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection