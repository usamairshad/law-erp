@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Child Benefit Config</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <div class="box-header with-border">
            <h3 class="box-title">Update Child Benefit Config</h3>
            <a href="{{ url('child/benefit-student') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
            {!! Form::model($ChildBenefitStudent, ['method' => 'PUT', 'id' => 'validation-form', 'url' => ['child/benefit-student', $ChildBenefitStudent->id]]) !!}
            <div class="box-body">
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                <div class="form-group col-md-4 @if($errors->has('staff_id')) has-error @endif">
                    {!! Form::label('name', 'Staff', ['class' => 'control-label']) !!}
                    {!! Form::select('staff_id',$staff->prepend('Select Staff', ''), old('staff_id'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    @if($errors->has('name'))
                        <span class="help-block">
                        {{ $errors->first('staff_id') }}
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-4 @if($errors->has('student_id')) has-error @endif">
                    {!! Form::label('name', 'Student', ['class' => 'control-label']) !!}
                    {!! Form::select('student_id', $students->prepend('Select Student', '') ,old('student_id'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    @if($errors->has('staff_id'))
                        <span class="help-block">
                    {{ $errors->first('staff_id') }}
                </span>
                    @endif
                </div>
                <div class="form-group col-md-4 @if($errors->has('child_benefit_config_id')) has-error @endif">
                    {!! Form::label('name', 'Child Benefit Config', ['class' => 'control-label']) !!}
                    {!! Form::select('child_benefit_config_id', $childbenefit->prepend('Select Child Benefit Config', '') ,old('child_benefit_config_id'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    @if($errors->has('child_benefit_config_id'))
                        <span class="help-block">
                {{ $errors->first('child_benefit_config_id') }}
            </span>
                    @endif
                </div>

                {{--            <div class="form-group col-md-4 @if($errors->has('country_id')) has-error @endif">--}}
                {{--                {!! Form::label('country_id', 'Country Name*', ['class' => 'control-label']) !!}--}}
                {{--                {!! Form::select('country_id', $country->prepend('Select Country', ''),old('country_id'), ['class' => 'form-control','required']) !!}--}}

                {{--                @if($errors->has('country_id'))--}}
                {{--                    <span class="help-block">--}}
                {{--                        {{ $errors->first('country_id') }}--}}
                {{--                    </span>--}}
                {{--                @endif--}}
                {{--            </div>--}}
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
            {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
@endsection