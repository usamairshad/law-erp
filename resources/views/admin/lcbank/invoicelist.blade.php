
<?php
$a=0;
?>
@if ($type->transaction_type == 1)
    
<div class="form-group col-md-4 @if($errors->has('cgroup_id')) has-error @endif" id="ledger_account">
   {!! Form::label('cgroup_id', 'Select Credit Account*', ['class' => 'control-label']) !!}
   <span id="cgroup_id_content">
       <select name="cgroup_id" id="cgroup_id" class="form-control select2" style="width: 100%;" required >
           <option value="1" selected> Select a Ledger </option>
           @if (count($Ledgers) > 0)
               @foreach ($Ledgers as $id => $data)
                   @if ($id == 0) @continue;
                   @else
                   <option value="{{ $id }}" @if ($id < 0) disabled="disabled" @endif >{{ $data['name'] }}</option>
                   @endif
                   @endforeach
           @endif
       </select>
   </span>
   <span id="cgroup_id_handler"></span>
</div>
@endif
{!! Form::hidden('lc_id', $ComercialInvoiceModel->lcno,['class' => 'form-control',]) !!}
<table class="table table-condensed" id="entry_items">
    <thead>
    <tr>
        <th>Product Name</th>
        <th>Total Qty</th>
        <th>Rel. Qty</th>
        <th>Bal. Qty</th>
        <th>Unit Price</th>
        <th>Segregate. Qty</th>
        <th>Total Amount</th>
        <th>Paid Amount</th>
        <th>Balance Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($performastock as $result)
        <?php
        $a++;
       if($type->transaction_type == 1){
           $unit_price =  $result->costing_do_price;
           $totaldoAmount = round($result->balanace_qty * $result->costing_do_price,4);
           $unit_price_i =  $result->costing_u_price;
           $totaldoAmount_i = round($result->balanace_qty * $result->costing_u_price,4);
       }else{
           $unit_price =  $result->costing_u_price;
           $totaldoAmount = round($result->balanace_qty * $result->costing_u_price,4);
       }

        ?>   
    <tr>
        <td>
            {!! Form::text('product_detail[]', $result->productsModel->products_short_name .'-'. $result->productsModel->products_name,['id' => 'product_detail_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
            {!! Form::hidden('product_detail_id[]', $result->product_name,['id' => 'product_detail_id_'.$a, 'class' => 'form-control',]) !!}
            {!! Form::hidden('row_id[]', $result->id,['id' => 'row_id_'.$a, 'class' => 'form-control',]) !!}
            {!! Form::hidden('unit_price_i[]', $unit_price_i,['id' => 'row_id_'.$a, 'class' => 'form-control',]) !!}
        </td>
        <td>
            {!! Form::text('total_qty[]',$result->balanace_qty,  ['id' => 'total_qty_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
        </td>
        <td>
           {!! Form::number('rel_qty[]', old('rel_qty'), ['id' => 'rel_qty_'.$a, 'class' => 'form-control test','data-row-id'=>"$a"]) !!}

        </td>
        <td>
           {!! Form::text('bal_qty[]', old('balance_qty'), ['id' => 'bal_qty_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}

        </td>
        <td>
            {!! Form::text('unit_price[]', $unit_price, ['id' => 'unit_price_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
        </td>



        <td>
            {!! Form::number('seg_qty[]', old('seg_qty'), ['id' => 'seg_qty_'.$a, 'class' => 'form-control','data-row-id'=>"$a", 'required', 'min'=>'1']) !!}
 
         </td>
        <td>
            {!! Form::text('total_amount[]',$totaldoAmount, ['id' => 'total_amount_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
        </td>
        <td>
            {!! Form::text('total_paid[]', 0, ['id' => 'total_paid_'.$a, 'class' => 'form-control cal_sum cal_total', 'readonly' => 'true']) !!}
        </td>
        <td>
            {!! Form::text('bal_amount[]', old('bal_amount'), ['id' => 'bal_amount_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
        </td>
    </tr>
        @endforeach
        <tr>
            <th colspan="7" class="text-right">TOTAL PAID AMOUNT</th>
            <th colspan="1">
                {!! Form::text('paid_amount_total', old('paid_amount_total'), ['id' => 'paid_amount_total', 'class' => 'form-control ', 'readonly' => 'true','required'=>'required']) !!}
            </th>         
        </tr>
    </tbody>
</table>

@section('javascript')
<script>
$('.select2').select2();
</script>
@endsection