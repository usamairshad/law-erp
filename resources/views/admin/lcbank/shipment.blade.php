@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Shipment Schedule</h1>
    </section>
@stop
    
@section('content')

<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-list"></i><h3 class="box-title">List</h3>
    </div>


        <div class="panel-body pad table-responsive">
            <table class="table table-bBanked table-striped {{ count($ComercialInvoiceModel) > 0 ? 'datatable' : '' }} dt-select" id="users-table">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Po#</th>
                    <th>Items Detail(QTY)</th>
                    <th>Bank</th>
                    <th>LC/TT</th>
                    <th>Amount USD</th>
                    <th>MOS</th>
                    <th>BL#</th>
                    <th>ATP</th>
                    <th>ETD</th>
                    <th>ETA</th>
                    <th>Clearance Date</th>
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($ComercialInvoiceModel) > 0)
                    @foreach ($ComercialInvoiceModel as $ComercialInvoiceModels)


                            <?php
                           $doubleOrder = App\Models\Admin\LcBankModel::doubleOrder($ComercialInvoiceModels->lcno);
                           ?>
                                <tr data-entry-id="{{ $ComercialInvoiceModels->id }}">
                                    <td> {{$r}} </td>
                                    <td>
                                        <table class="table">
                                            @foreach ($doubleOrder as $doubleOrders)
                                                <tr>
                                                    <td>{{date('d-m-Y', strtotime($doubleOrders->po_date))}}</td>
                                                    <td>{{ $doubleOrders->suppliersModel->sup_name}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                    <td>
                                        @foreach ($ComercialInvoiceModels->performaStockModel as $orderdetail)

                                            @if($loop->last)
                                                {{$orderdetail->productsModel->products_name}} ({{$orderdetail->total_qty}})
                                            @else
                                                {{$orderdetail->productsModel->products_name}} ({{$orderdetail->total_qty}}),
                                            @endif
                                        @endforeach
                                    </td>

                                    <td>

                                        @if($ComercialInvoiceModels->lCmodel->transaction_type  == 2 && $ComercialInvoiceModels->lCmodel->lc_type  == 2 )
                                        {{ $ComercialInvoiceModels->lCmodel->suppliersModel->sup_name}}
                                         @else
                                            {{ $ComercialInvoiceModels->lCmodel->banksModel->bank_name}}
                                          @endif
                                    </td>
                                    @if( $ComercialInvoiceModels->lCmodel->transaction_type === 1 )
                                        <td>LC</td>
                                    @else
                                        <td>TT</td>
                                    @endif
                                    <td>{{ $ComercialInvoiceModels->usd_amt}}</td>
                                    <td>{{ $ComercialInvoiceModels->mos}}</td>
                                    <td>{{ $ComercialInvoiceModels->bl_no}}</td>
                                    <td>{{date('d-m-Y', strtotime( $ComercialInvoiceModels->eta))}}</td>
                                    <td>{{date('d-m-Y', strtotime( $ComercialInvoiceModels->atp))}}</td>
                                    <td>{{ date('d-m-Y', strtotime($ComercialInvoiceModels->etd))}}</td>
                                    @if($ComercialInvoiceModels->close_date != '')
                                    <td>{{ date('d-m-Y', strtotime($ComercialInvoiceModels->close_date))}}</td>
                                        @else
                                        <td>{{$ComercialInvoiceModels->close_date}}</td>
                                    @endif
                                </tr>
                        @php($r++)
                    @endforeach

                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
</div>
@stop

@section('javascript')
    <script>
        $('#users-table').DataTable();
    </script>
@endsection