@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>LC/TT Costing</h1>
    </section>
@stop    

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            {{--<a href="{{ route('admin.lcbank.create')}}" class="btn btn-success pull-right">Add New Lc</a>--}}
        </div>
        <!-- /.box-header -->

        <div class="box-body pad table-responsive">
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped" id="costing_table">
                    <thead>
                    <tr>
                        <th>S:#</th>
                        <th>LC/TT #</th>
                        <th>LC/TT Amount($)</th>
                        <th>Bank Name</th>
                        <th>Opening Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            $('#costing_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.lcbank.datatablesSheet') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'lc_no', name: 'lc_no' },
                    { data: 'lc_amt', name: 'lc_amt' },
                    { data: 'bnk_id', name: 'bnk_id' },
                    { data: 'open_date', name: 'open_date' },
                    { data: 'lc_status', name: 'lc_status' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},

                ]
            });
        });
    </script>
@endsection