@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Transaction</h1>
    </section>
@stop
   
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ route('admin.lcbank.create')}}" class="btn btn-success pull-right">Add New Lc</a>
        </div>
        <!-- /.box-header -->

        <div class="box-body pad table-responsive">
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped" id="lc_table">
                    <thead>
                    <tr>
                        <th>S:#</th>
                        <th>LC/TT</th>
                        <th>Type</th>
                        <th>Performa#</th>
                        <th>LC/TT#</th>
                        <th>LC/TT Amount</th>
                        <th>Bank Name</th>
                        {{-- <th>Approx.Dollar Rate</th> --}}
                        {{-- <th>Confirm Dollar Rate</th> --}}
                        <th>Opening Date</th>
                        {{-- <th>Expiry Date</th> --}}
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            $('#lc_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.lcbank.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'transaction_type', name: 'transaction_type' },
                    { data: 'lc_type', name: 'lc_type' },
                    { data: 'pf_no', name: 'pf_no' },
                    { data: 'lc_no', name: 'lc_no' },
                    { data: 'lc_amt', name: 'lc_amt' },
                    { data: 'bnk_id', name: 'bnk_id' },
                    // { data: 'dollar_rate', name: 'dollar_rate' },
                    // { data: 'today_doller', name: 'today_doller' },
                    { data: 'open_date', name: 'open_date' },
                    // { data: 'lc_expiry_date', name: 'lc_expiry_date' },
                    { data: 'lc_status', name: 'lc_status' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},

                ]
            });
        });
    </script>
@endsection