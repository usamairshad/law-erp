

@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1> Update LC/TT</h1>
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">LC/TT 1st Stage Updation</h3>
            <a href="{{ route('admin.lcbank.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
    
        <div class="box-body">
        <div class="row">
            <div class="col-md-12 form-group">
                <span class="alert alert-success" id="massage"> Record Has been inserted successfully.</span>
            </div>

        </div>

            {!! Form::model($LcBanks, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.lcbank.update', $LcBanks->id]]) !!}
            {!! Form::hidden('dr_id',$LcBanks->id, ['id'=>'dr_id','class' => 'form-control', 'placeholder' => '']) !!}
            {!! Form::hidden('lc_amt',$LcBanks->lc_amt, ['class' => 'form-control', 'placeholder' => '']) !!}
            {!! Form::hidden('bank_id',$LcBanks->bnk_id, ['class' => 'form-control', 'placeholder' => '']) !!}
            {!! Form::hidden('lc_status',$LcBanks->lc_status, ['class' => 'form-control', 'placeholder' => '']) !!}
            {!! Form::hidden('transaction_type',$LcBanks->transaction_type, ['class' => 'form-control', 'placeholder' => '']) !!}
            {{csrf_field()}}
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-3  form-group @if($errors->has('transaction_type')) has-error @endif">
                        {!! Form::label('transaction_type', 'Mode of Payment*', ['class' => 'control-label']) !!}
                        {!! Form::select('transaction_type',  array('' => 'Select class') + Config::get('admin.mode_of_payment'), old('transaction_type'), ['id' => 'transaction_type','class' => 'form-control','required','disabled']) !!}
                        <span id="transaction_type_handler"></span>
                        @if($errors->has('transaction_type'))
                            <span class="help-block">
                                {{ $errors->first('transaction_type') }}
                            </span>
                        @endif
                    </div>

                    @if($LcBanks->transaction_type == 1 || $LcBanks->transaction_type == 2 && $LcBanks->lc_type == 1)
                        <input value="{{$LcBanks->bnk_id}}" type="hidden" id="banksid">
                        <div class="col-xs-3 form-group @if($errors->has('bnk_name')) has-error @endif">
                            {!! Form::label('bnk_name', 'Bank Name*', ['class' => 'control-label']) !!}
                            <select id="bnk_name" name="bnk_name" class="form-control" disabled>
                                <option value="">Select Bank Name</option>
                                @if ($Banks->count())
                                    @foreach($Banks as $Bank)
                                        <option value="{{ $Bank->id }}" {{ $Bank->id == $LcBanks->bnk_id ? 'selected':''}}>{{ $Bank->bank_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span id="bnk_name_handler"></span>
                            @if($errors->has('bnk_name'))
                                <span class="help-block">
                            {{ $errors->first('bnk_name') }}
                            </span>
                            @endif
                        </div>
                      @endif
                      @if($LcBanks->transaction_type == 1)
                           <div class="col-xs-2 form-group @if($errors->has('lc_type')) has-error @endif" >
                               {!! Form::label('lc_type', 'LC Types*', ['class' => 'control-label']) !!}
                               {!! Form::select('lc_type', array() + Config::get('admin.lc_types'), $LcBanks->lc_type, ['id' => 'lc_type','class' => 'form-control','required']) !!}
                               <span id="lc_type_handler"></span>
                               @if($errors->has('lc_type'))
                                   <span class="help-block">
                                       {{ $errors->first('lc_type') }}
                                       </span>
                               @endif
                           </div>
                       @else
                        <div class="col-xs-2 form-group @if($errors->has('lc_type')) has-error @endif" >
                            {!! Form::label('lc_type', 'LC Types*', ['class' => 'control-label']) !!}
                            {!! Form::select('lc_type', array() + Config::get('admin.tt_types'), $LcBanks->lc_type, ['id' => 'lc_type','class' => 'form-control','required']) !!}
                            <span id="lc_type_handler"></span>
                            @if($errors->has('lc_type'))
                                <span class="help-block">
                                       {{ $errors->first('lc_type') }}
                                       </span>
                            @endif
                        </div>
                       @endif


                       <div class="col-xs-2  form-group @if($errors->has('delivery_term')) has-error @endif">
                           {!! Form::label('delivery_term', 'Delivery Terms*', ['class' => 'control-label']) !!}
                           {!! Form::select('delivery_term', array() + Config::get('admin.delivery_term'), old('delivery_term'), ['id' => 'delivery_term','class' => 'form-control','required']) !!}
                           <span id="delivery_term_handler"></span>
                           @if($errors->has('delivery_term'))
                               <span class="help-block">
                               {{ $errors->first('delivery_term') }}
                               </span>
                           @endif
                       </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col-xs-12">
                   <div class="col-xs-3  form-group @if($errors->has('pf_no')) has-error @endif">
                    
                       {!! Form::label('pf_no', 'Selected Performa', ['class' => 'control-label']) !!}
                      <input type="hidden" id="spf_total"  class="form-control">
                      <input type="hidden" id="pf_total"  class="form-control">
                        <select id="spf_no" multiple="multiple" name="spf_no[]" class="form-control spf_no" readonly>
                                @if (count($multiperforma) > 0)
                                    @foreach($multiperforma as $role)
                                    @php
                                        
                                        $usedOrder = App\Models\Admin\Orders::whereIn('id', [$role])->first();
                                    @endphp
                                         <option value="{{ $usedOrder->id }}" data-price="{{ $usedOrder->total_order_amt }}">{{ $usedOrder->perfomra_no }}</option>
                                    @endforeach
                                @endif
                            </select>
                      
                       <span id="pf_no_handler"></span>
                       @if($errors->has('pf_no'))
                                <span class="help-block">
                                    {{ $errors->first('pf_no') }}
                               </span>
                       @endif
                   </div>
                   <div class="col-xs-3  form-group @if($errors->has('pf_no')) has-error @endif">
                    
                        {!! Form::label('pf_no', 'New Performa*', ['class' => 'control-label']) !!}
                       {{-- <input type="text" id="pf_no" multiple="multiple" name="pf_no[]" class="form-control pf_no" readonly> --}}
                         <select id="pf_no" multiple="multiple" name="pf_no[]" class="form-control pf_no" readonly required>
                                 @if (count($Orders) > 0)
                                     @foreach($Orders as $order)
                                          <option value="{{ $order->id }}" data-price="{{ $order->total_order_amt }}">{{ $order->perfomra_no }}</option>
                                     @endforeach
                                 @endif
                             </select>
                       
                        <span id="pf_no_handler"></span>
                        @if($errors->has('pf_no'))
                                 <span class="help-block">
                                     {{ $errors->first('pf_no') }}
                                </span>
                        @endif
                    </div>
                   <div class="col-xs-3 form-group @if($errors->has('dollar_rate')) has-error @endif">
                       {!! Form::label('totl_lc_amt', 'Total Amount($)', ['class' => 'control-label']) !!}
                       {!! Form::text('totl_lc_amt', $LcBanks->lc_amt, ['id' =>'totl_lc_amt' , 'class' => 'form-control','readonly']) !!}
                       <span id="totl_lc_amt_handler"></span>
                       @if($errors->has('totl_lc_amt'))
                           <span class="help-block">
                                    {{ $errors->first('totl_lc_amt') }}
                               </span>
                       @endif
                   </div>
                   <div class="col-xs-3 form-group @if($errors->has('amountrs')) has-error @endif">
                       {!! Form::label('totl_lc_rs', 'Total Amount(Rs)', ['class' => 'control-label']) !!}
                       {!! Form::text('totl_lc_rs', round($LcBanks->lc_amt*$LcBanks->dollar_rate), ['id' =>'totl_lc_rs' , 'class' => 'form-control','readonly']) !!}
                       <span id="totl_lc_amt_handler"></span>
                       @if($errors->has('totl_lc_rs'))
                           <span class="help-block">
                                    {{ $errors->first('totl_lc_rs') }}
                               </span>
                       @endif
                   </div>

                   </div>
               </div>

               <div class="row">
                   <div class="col-xs-12">
                   <div class="col-xs-2 form-group @if($errors->has('dollar_rate')) has-error @endif">
                       {!! Form::label('dollar_rate', 'Dollar Rate*', ['class' => 'control-label']) !!}
                       {!! Form::number('dollar_rate', $LcBanks->dollar_rate, ['id' =>'dollar_rate' , 'class' => 'form-control', 'placeholder' => '','required'=>'true','min'=>'1','maxlength'=>'10']) !!}
                       <span id="dollar_rate_handler"></span>
                       @if($errors->has('dollar_rate'))
                           <span class="help-block">
                                    {{ $errors->first('dollar_rate') }}
                               </span>
                       @endif
                   </div>
                   <div class="col-xs-3 form-group @if($errors->has('open_date')) has-error @endif">
                       {!! Form::label('open_date', 'Opening Date*', ['class' => 'control-label']) !!}
                       {!! Form::text('open_date', $LcBanks->open_date,  ['id' =>'open_date' , 'class' => 'form-control datepicker','required'=>'true']) !!}
                       <span id="open_date_handler"></span>
                       @if($errors->has('open_date'))
                           <span class="help-block">
                                    {{ $errors->first('open_date') }}
                               </span>
                       @endif
                   </div>
                   <div class="col-xs-3 form-group @if($errors->has('lc_no')) has-error @endif">
                       {!! Form::label('lc_no', 'LC/TT#', ['class' => 'control-label']) !!}
                       {!! Form::text('lc_no',$LcBanks->lc_no, ['id' =>'lc_no' , 'class' => 'form-control', 'placeholder' => '','maxlength'=>'50']) !!}
                       @if($errors->has('lc_no'))
                           <span class="help-block">
                                           {{ $errors->first('lc_no') }}
                                       </span>
                       @endif
                   </div>
                   <div class="col-xs-3 form-group @if($errors->has('description')) has-error @endif">
                       {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
                       {!! Form::textarea('description', $LcBanks->description, ['class' => 'form-control', 'placeholder' => '', 'rows' => 4, 'cols' => 10 ,'required'=>'true']) !!}
                       @if($errors->has('description'))
                           <span class="help-block">
                                   {{ $errors->first('description') }}
                               </span>
                       @endif
                   </div>
                   </div>
               </div>
               @if($LcBanks->transaction_type == 2 && $LcBanks->lc_type == 2)

                           <div class="row">
                               <div class="col-xs-2 form-group @if($errors->has('Flut_rate')) has-error @endif">
                                   {!! Form::label('Flut_rate', 'Rate Fluctuations($)', ['class' => 'control-label']) !!}
                                   {!! Form::text('Flut_rate', old('Flut_rate'), ['id' =>'Flut_rate' , 'class' => 'form-control']) !!}
                                   <span id="Flut_rate_handler"></span>
                                   @if($errors->has('Flut_rate'))
                                       <span class="help-block">
                                       {{ $errors->first('Flut_rate') }}
                                       </span>
                                   @endif
                               </div>

                               <div class="form-group col-md-2">
                                   <input id="rate_btn" class="btn-default" type="button" value="Rate Update" />
                               </div>
                           </div>
                      

               @endif
               @if($LcBanks->transaction_type == 1)
               <div class="panel-group" id="accordion">
                   <div class="panel panel-default wow fadInLeft">
                       <div class="panel-heading">
                           <h4 class="panel-title">
                               <a data-toggle="collapse" data-parent="#accordion" href="#12">
                                   <span class="fa fa-check-square-o"></span><b>Margin / Murabaha History</b>
                               </a>
                           </h4>
                       </div>
                       <div id="12" class="panel-collapse collapse in">
                           <div class="panel-body pad table-responsive">
                               <div class="row">
                                  {{-- <input id="murabha_id" type="checkbox" name="murabha" value="1">Click To Pay Murabha --}}
                                  
                                  
                                        
                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" role="dialog">
                                            <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Margin Calculate</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6 form-group @if($errors->has('account_rate')) has-error @endif">
                                                            {!! Form::label('account_rate', 'Rate($)', ['class' => 'control-label']) !!}
                                                            {!! Form::text('account_rate', old('account_rate'), ['id' =>'account_rate' , 'class' => 'form-control margin_rate','maxlegth'=>'10']) !!}
                                                            <span id="account_rate_handler"></span>
                                                            @if($errors->has('account_rate'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('account_rate') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-6 form-group @if($errors->has('margin_rate')) has-error @endif">
                                                            {!! Form::label('margin_rate', 'Margin(%)', ['class' => 'control-label']) !!}
                                                            {!! Form::text('margin_rate', old('margin_rate'), ['id' =>'margin_rate' , 'class' => 'form-control margin_rate','maxlegth'=>'2']) !!}
                                                            <span id="margin_rate_handler"></span>
                                                            @if($errors->has('margin_rate'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('margin_rate') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-6 form-group @if($errors->has('margin_amount')) has-error @endif">
                                                            {!! Form::label('margin_amount', 'Margin Amount(Rs)', ['class' => 'control-label']) !!}
                                                            {!! Form::text('margin_amount', old('margin_amount'), ['id' =>'margin_amount' , 'class' => 'form-control']) !!}
                                                            <span id="margin_amount_handler"></span>
                                                            @if($errors->has('margin_amount'))
                                                                <span class="help-block">
                                                                {{ $errors->first('margin_amount') }}
                                                                </span>
                                                            @endif
                                                        </div>
                            
                                                        <div class="form-group col-md-6 @if($errors->has('cgroup_id')) has-error @endif">
                                                            {!! Form::label('cgroup_id', 'Select Credit Account*', ['class' => 'control-label']) !!}
                                                            <span id="cgroup_id_content">
                                                                <select name="cgroup_id" id="cgroup_id" class="form-control select2" style="width: 100%;" required>
                                                                    <option value="1" selected> Select a Ledger </option>
                                                                    @if (count($Ledgers) > 0)
                                                                        @foreach ($Ledgers as $id => $data)
                                                                            @if ($id == 0) @continue;
                                                                            @else
                                                                            <option value="{{ $id }}" @if ($id < 0) disabled="disabled" @endif >{{ $data['name'] }}</option>
                                                                            @endif
                                                                            @endforeach
                                                                    @endif
                                                                </select>
                                                            </span>
                                                            <span id="cgroup_id_handler"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                                <input id="btn" class="btn btn-primary" type="button" value="Calculate Margin" />
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                       
                                  
                                  
                                   <div class="form-group col-md-2">
                                       <input id="btnMargin" class="btn btn-primary" type="button" value="Show History" data-toggle="tooltip" data-placement="top" title="Show margin history"/>
                                      
                                   </div>
                                   <div class="form-group col-md-2">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="top" title="Calculate Margin">
                                                Calculate Margin
                                            </button>
                                      
                                   </div>
                               </div>
                               <div class="row">
                                   @if($LcBanks->lc_no)
                                   <div class="col-xs-3">
                                       <table class="table table-bordered table-striped" dt-select id="margin_table">
                                           <thead>
                                           <tr>
                                               <th>Rate($)</th>
                                               <th>Margin(%)</th>
                                               <th>Margin Amount(Rs)#</th>
                                           </tr>
                                           </thead>
                                           <tbody>
                                               @if (count($MarginTable) > 0)
                                                   @foreach ($MarginTable as $MarginTables)
                                                       <tr data-entry-id="{{ $MarginTables->id }}">
                                                           <td>{{ $MarginTables->account_rate }}</td>
                                                           <td>{{ $MarginTables->margin_rate }}</td>
                                                           <td>{{ $MarginTables->margin_amount }}</td>
                                                       </tr>
                                                   @endforeach
                                               @else
                                                   <tr>
                                                       <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                                                   </tr>
                                               @endif
                                           </tbody>
                                       </table>
                                   </div>
                                       @else
                                   No History
                                       @endif
                               </div>
                           </div>

                       </div>
                   </div>
               </div>
               @endif
               {{--<div class="panel-group" id="accordion">--}}
                   {{--<div class="panel panel-default wow fadInLeft">--}}
                           {{--<div class="panel-heading">--}}
                               {{--<h4 class="panel-title">--}}
                                   {{--<a data-toggle="collapse" data-parent="#accordion" href="#11">--}}
                                       {{--<span class="fa fa-check-square-o"></span><b>PAD</b>--}}
                                   {{--</a>--}}
                               {{--</h4>--}}
                           {{--</div>--}}
                         {{--<div id="11" class="panel-collapse collapse in">--}}
                             {{--<div class="panel-body pad table-responsive">--}}
                                  {{--<div class="row">--}}
                                {{--@if($LcBanks->transaction_type == 1)--}}
                                          {{--<div class="col-xs-4 form-group @if($errors->has('margin_rate')) has-error @endif">--}}
                                              {{--{!! Form::label('margin_rate', 'LC Margin', ['class' => 'control-label']) !!}--}}
                                              {{--{!! Form::text('margin_rate',$LcBanks->margin_rate, ['id' =>'margin_rate' ,'class' => 'form-control', 'placeholder' => '','maxlength'=>10]) !!}--}}
                                              {{--@if($errors->has('margin_rate'))--}}
                                                  {{--<span class="help-block">--}}
                                                        {{--{{ $errors->first('margin_rate') }}--}}
                                                    {{--</span>--}}
                                              {{--@endif--}}
                                          {{--</div>--}}

                                          {{--<div class="col-xs-4 form-group @if($errors->has('mu_profit')) has-error @endif">--}}
                                              {{--{!! Form::label('mu_profit', 'Murabaha Profit(%)', ['class' => 'control-label']) !!}--}}
                                              {{--{!! Form::text('mu_profit',$LcBanks->murahabah_profit, ['id' =>'mu_profit' , 'class' => 'form-control', 'placeholder' => '','maxlength'=>'20']) !!}--}}
                                              {{--@if($errors->has('mu_profit'))--}}
                                                  {{--<span class="help-block">--}}
                                               {{--{{ $errors->first('mu_profit') }}--}}
                                           {{--</span>--}}
                                              {{--@endif--}}
                                          {{--</div>--}}
                                      {{--@endif--}}
                                     {{--<div class="col-xs-4 form-group @if($errors->has('lc_no')) has-error @endif">--}}
                                       {{--{!! Form::label('lc_no', 'LC/TT#', ['class' => 'control-label']) !!}--}}
                                       {{--{!! Form::text('lc_no',$LcBanks->lc_no, ['id' =>'lc_no' , 'class' => 'form-control', 'placeholder' => '','maxlength'=>'50']) !!}--}}
                                       {{--@if($errors->has('lc_no'))--}}
                                           {{--<span class="help-block">--}}
                                               {{--{{ $errors->first('lc_no') }}--}}
                                           {{--</span>--}}
                                       {{--@endif--}}
                                   {{--</div>--}}


                               {{--</div>--}}
                              {{----}}
                             {{--</div>--}}
                       {{--</div>--}}
                   {{--</div>--}}
               {{--</div>--}}
           </div>
           <div class="box-footer">
               {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
           </div>
           {!! Form::close() !!}
       </div>
   @stop
   @section('javascript')
       <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
       <script src="{{ url('public/js/admin/lcbanks/lcbanks_update_modify.js') }}" type="text/javascript"></script>
   <script>
       $(document).ready(function () {
        $('#cgroup_id').select2();
        var Totalamount = $('#totl_lc_amt').val();
        $('#spf_total').val(Totalamount);
        //alert(Totalamount);
           $('#margin_table').hide();
           $('#massage').hide();
           $('#flu_massage').hide();
           var values = <?php echo json_encode($multiperforma)?>;
           console.log('ddd:',values);
           $('#spf_no').val(values).change();

           
           $(document).on('blur','.margin_rate',function(){
               var account_rate = $('#account_rate').val();
               var margin_rate = $('#margin_rate').val();
               var totl_lc_rs = $('#totl_lc_amt').val() * account_rate;
               var totalm_amt = (totl_lc_rs/100) * margin_rate;
                   totalm_amt = totalm_amt.toFixed();
               $('#margin_amount').val(totalm_amt);
           });

           $('.spf_no').select2().change(function() {
            

               var price = 0;
               var totalPrice = 0;
               var preLCAmount = 0;
               $('option:selected', $(this)).each(function() {
                   price +=  parseFloat($(this).data('price'));
               });
               //var preLCAmount = $('#pf_total').val();
               //alert(preLCAmount);
               //totalPrice = parseFloat(preLCAmount) + parseFloat(price);
               $('#totl_lc_amt').val(price);
               $('#spf_total').val(price);
               var dollar_rate = $('#dollar_rate').val();
        
                var lcRS = price*dollar_rate;
           
               $('#totl_lc_rs').val(lcRS.toFixed());
           });



           $('.pf_no').select2().change(function() {
           
               var price = 0;
               var totalPrice = 0;
               $('option:selected', $(this)).each(function() {
                   price +=  parseFloat($(this).data('price'));
               });
               var preLCAmount = $('#spf_total').val();
                totalPrice = parseFloat(preLCAmount) + parseFloat(price);
               $('#totl_lc_amt').val(totalPrice);
               $('#pf_total').val(totalPrice);
                var dollar_rate = $('#dollar_rate').val();
                 var lcRS = totalPrice*dollar_rate;
               $('#totl_lc_rs').val(lcRS.toFixed());
           });





           $('#btn').on('click', function(){
            
               var margin = $('#margin_amount').val();
               var banksid = $('#banksid').val();
               var dr_id = $('#dr_id').val();
               var cgroup_id = $('#cgroup_id').select2().val();
               var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
               if(margin!='' && cgroup_id!=''){
                   var url = '{{ url("admin/lcbank/payment") }}';
                   $.ajax({
                       type:"post",
                       url:url,
                       data:{
                          lc_no:$('#lc_no').val(),
                          account_rate : $('#account_rate').val(),
                          margin_rate : $('#margin_rate').val(),
                          murabha_id : $("#murabha_id").is(':checked'),
                          margin :margin,
                          dr_id :dr_id,
                          banksid :banksid,
                          cgroup_id :cgroup_id,
                           _token: CSRF_TOKEN
                       },
                       success:function(data){
                           
                           if(data === 'ok' ){
                               $('#massage').show();
                               $('#exampleModal').modal('hide');
                           }
                       }
                   })
                   setTimeout(function() {
                        $('#massage').fadeOut('fast');
                    }, 3000); 
                   $('#btn').attr("disabled", true);
                   setTimeout(function() {
                       $("#btn").removeAttr("disabled");
                   }, 3000);
               }

//               $("#validation-form").submit();
//               return true;
           });
           $('#btnMargin').click(function(){
               $('#margin_table').toggle();
           });
           $('#rate_btn').on('click', function(){
               //alert('hiiiii');
               var Flut_rate = $('#Flut_rate').val();
               var dr_id = $('#dr_id').val();
               var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
               if(Flut_rate!=''){
                   var url = '{{ url("admin/lcbank/fluctuation") }}';
                   $.ajax({
                       type:"post",
                       url:url,
                       data:{
                           Flut_rate : Flut_rate,
                           dr_id :dr_id,
                           _token: CSRF_TOKEN
                       },
                       success:function(data){
                           if(data === 'ok' ){
                               $('#flu_massage').show();
                           }
                       }

                   })
               }

           });
       })
   </script>

   @endsection
