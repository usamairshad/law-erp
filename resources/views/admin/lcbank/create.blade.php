@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Transaction</h1>
    </section>  
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Lc/TT</h3>
            <a href="{{ route('admin.lcbank.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.lcbank.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3  form-group @if($errors->has('transaction_type')) has-error @endif">
                            {!! Form::label('transaction_type', 'Mode of Payment*', ['class' => 'control-label']) !!}
                            {!! Form::select('transaction_type', array() + Config::get('admin.mode_of_payment'), null, ['id' => 'transaction_type','class' => 'form-control select2','required']) !!}

                        <span id="transaction_handler"></span>
                            @if($errors->has('transaction_type'))
                                <span class="help-block">
                            {{ $errors->first('transaction_type') }}
                            </span>
                            @endif
                        </div>
                    <div class="lctypes"style="display: none">
                        <div class="col-xs-3 form-group @if($errors->has('lc_type')) has-error @endif" >
                            {!! Form::label('lc_type', 'LC Types*', ['class' => 'control-label']) !!}
                            {!! Form::select('lc_type', array() + Config::get('admin.lc_types'), null, ['id' => 'lc_type','class' => 'form-control','required']) !!}
                            <span id="lc_type_handler"></span>
                            @if($errors->has('lc_type'))
                                <span class="help-block">
                                    {{ $errors->first('lc_type') }}
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="tttypes"style="display: none">
                        <div class="col-xs-3 form-group @if($errors->has('tt_types')) has-error @endif" >
                            {!! Form::label('tt_types', 'TT Types*', ['class' => 'control-label']) !!}
                            {!! Form::select('tt_types', array() + Config::get('admin.tt_types'), null, ['id' => 'tt_types','class' => 'form-control','required']) !!}
                            <span id="tt_types_handler"></span>
                            @if($errors->has('tt_types'))
                                <span class="help-block">
                                    {{ $errors->first('tt_types') }}
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-3  form-group @if($errors->has('delivery_term')) has-error @endif">
                        {!! Form::label('delivery_term', 'Delivery Terms*', ['class' => 'control-label']) !!}
                        {!! Form::select('delivery_term', array() + Config::get('admin.delivery_term'), null, ['id' => 'delivery_term','class' => 'form-control select2','required']) !!}
                        <span id="delivery_term_handler"></span>
                        @if($errors->has('delivery_term'))
                            <span class="help-block">
                            {{ $errors->first('delivery_term') }}
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-4  form-group @if($errors->has('pf_no')) has-error @endif">
                        {!! Form::label('pf_no', 'Performa No*', ['class' => 'control-label']) !!}
                        <select name="pf_no[]" multiple="multiple" class="form-control select2 pf_no" id="pf_no" required>
                            @if ($Orders->count())
                                @foreach($Orders as $role)
                                    {{--<option value="{{ $role->id }}" data-price="{{ $role->total_order_amt }}">{{ $role->po_number }} - {{ $role->suppliersModel->sup_name }}- {{ $role->total_order_amt }}</option>--}}
                                    <option value="{{ $role->id }}" data-price="{{ $role->total_order_amt }}">{{ $role->perfomra_no }}</option>
                                @endforeach
                            @endif
                        </select>
                        <span id="pf_no_handler"></span>
                        @if($errors->has('pf_no'))
                            <span class="help-block">
                            {{ $errors->first('pf_no') }}
                             </span>
                        @endif
                    </div>
                    <div class="col-xs-2 form-group @if($errors->has('lcamt')) has-error @endif">
                        {!! Form::label('lcamt', 'Total Lc Amount($)', ['class' => 'control-label']) !!}
                        {!! Form::text('lcamt', old('lcamt'), ['id'=>'lcamt','class' => 'form-control' ,'placeholder' => '','readonly']) !!}

                        @if($errors->has('lcamt'))
                            <span class="help-block">
                            {{ $errors->first('lcamt') }}
                         </span>
                        @endif
                    </div>
                    <div class="col-xs-3 form-group bnk_name @if($errors->has('name')) has-error @endif">
                    {!! Form::label('bnk_name', 'Bank Name*', ['class' => 'control-label']) !!}
                    <select id="bnk_name" name="bnk_name" class="form-control select2">
                        <option value="">Select Bank Name</option>
                        @if ($Banks->count())
                            @foreach($Banks as $Bank)
                                <option value="{{ $Bank->id }}">{{ $Bank->bank_name }}</option>

                            @endforeach
                        @endif
                    </select>
                    <span id="bnk_name_handler"></span>
                    @if($errors->has('bnk_name'))
                        <span class="help-block">
                        {{ $errors->first('bnk_name') }}
                        </span>
                    @endif
                </div>
                    <div class="col-xs-3 form-group @if($errors->has('dollar_rate')) has-error @endif">
                    {!! Form::label('dollar_rate', 'Dollar Rate*', ['class' => 'control-label']) !!}
                    {!! Form::text('dollar_rate', old('dollar_rate'), ['class' => 'form-control' ,'placeholder' => '','min'=>"1",'maxlength'=>10,'required']) !!}

                    @if($errors->has('dollar_rate'))
                        <span class="help-block">
                            {{ $errors->first('dollar_rate') }}
                         </span>
                    @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-6 form-group @if($errors->has('description')) has-error @endif">
            {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
            {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 4, 'cols' => 10,'required'=>'true']) !!}
            @if($errors->has('description'))
                <span class="help-block">
            {{ $errors->first('description') }}
            </span>
                @endif
        </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-xs-12">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('javascript')
    <script src="{{ url('public/js/admin/lcbanks/lcbanks_create_modify.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            $("#transaction_type").on('change',function(){
                var orderval = $(this).val();
                    if(orderval === '1'){
                        $('.lctypes').show();
                        $('.tttypes').hide();
                    }else{
                        $('.lctypes').hide();
                        $('.tttypes').show();
                    }

                });
            $("#tt_types").on('change',function(){
                var tt_types = $(this).val();
                if(tt_types === '2'){
                    $('.bnk_name').hide();
                }else{
                    $('.bnk_name').show();
                }
            });

                $('.pf_no').select2().change(function() {
                    var price = 0;
                    $('option:selected', $(this)).each(function() {
                        console.log($(this).data('price'));
                        price +=  parseFloat($(this).data('price'));
                    });
                    $('#lcamt').val(price);
                });

               
        });
    </script>


@endsection