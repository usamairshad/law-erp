@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Costing</h3>
  
    <div class="box-header with-border">
        <a href="{{ route('admin.cinvoice.index') }}" class="btn btn-success pull-right">Back</a>&nbsp;&nbsp;&nbsp;
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            {{--<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-default">--}}
                {{--Calculate Costing--}}
            {{--</button>--}}
            <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header btn-primary">
                            <h4 class="modal-title">Costing</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['method' => 'POST', 'route' => ['admin.cinvoice.cost'], 'id' => 'validation-form']) !!}
                            <div class="box-body">
                                <input type="hidden"name="lcid" value="{{$ComercialInvoiceModel->id}}">
                                <input type="hidden"name="usd_amount" value="{{$ComercialInvoiceModel->usd_amt}}">
                                <div class="col-xs-6 form-group @if($errors->has('insurance')) has-error @endif">
                                    {!! Form::label('insurance', 'Insurance', ['class' => 'control-label']) !!}
                                    {!! Form::number('insurance', old('insurance'), ['class' => 'form-control', 'placeholder' => '','min'=>0,'maxlength'=>30]) !!}
                                    @if($errors->has('insurance'))
                                        <span class="help-block">
                                            {{ $errors->first('insurance') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-6 form-group @if($errors->has('bank_charg')) has-error @endif">
                                    {!! Form::label('bank_charg', 'Bank Charges', ['class' => 'control-label']) !!}
                                    {!! Form::number('bank_charg', old('bank_charg'), ['class' => 'form-control', 'placeholder' => '','min'=>0,'maxlength'=>30]) !!}
                                    @if($errors->has('bank_charg'))
                                        <span class="help-block">
                                            {{ $errors->first('bank_charg') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-6 form-group @if($errors->has('freight')) has-error @endif">
                                    {!! Form::label('freight', 'Freight', ['class' => 'control-label']) !!}
                                    {!! Form::number('freight', old('freight'), ['class' => 'form-control', 'placeholder' => '','min'=>0,'maxlength'=>30]) !!}
                                    @if($errors->has('freight'))
                                        <span class="help-block">
                                            {{ $errors->first('freight') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-6 form-group @if($errors->has('pinvest')) has-error @endif">
                                    {!! Form::label('pinvest', 'Profit Investment', ['class' => 'control-label']) !!}
                                    {!! Form::number('pinvest', old('pinvest'), ['class' => 'form-control', 'placeholder' => '','min'=>0,'maxlength'=>30]) !!}
                                    @if($errors->has('pinvest'))
                                        <span class="help-block">
                                            {{ $errors->first('pinvest') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-6 form-group @if($errors->has('m_profit')) has-error @endif">
                                    {!! Form::label('m_profit', 'Murabaha Profit', ['class' => 'control-label']) !!}
                                    {!! Form::number('m_profit', old('m_profit'), ['class' => 'form-control', 'placeholder' => '','min'=>0,'maxlength'=>30]) !!}
                                    @if($errors->has('m_profit'))
                                        <span class="help-block">
                                            {{ $errors->first('m_profit') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-6 form-group @if($errors->has('mv_bndg')) has-error @endif">
                                    {!! Form::label('mv_bndg', 'Mvmnt/In Bndg', ['class' => 'control-label']) !!}
                                    {!! Form::number('mv_bndg', old('mv_bndg'), ['class' => 'form-control', 'placeholder' => '','min'=>0,'maxlength'=>30]) !!}
                                    @if($errors->has('mv_bndg'))
                                        <span class="help-block">
                                            {{ $errors->first('mv_bndg') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="col-xs-6 form-group @if($errors->has('duty')) has-error @endif">
                                    {!! Form::label('duty', 'Duty', ['class' => 'control-label']) !!}
                                    {!! Form::number('duty', old('duty'), ['class' => 'form-control', 'placeholder' => '','min'=>0,'maxlength'=>30]) !!}
                                    @if($errors->has('duty'))
                                        <span class="help-block">
                                            {{ $errors->first('duty') }}
                                        </span>
                                    @endif
                                </div>

                            </div>
                    </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Calculate</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <table  id="costing_table" class="table table-bordered table-striped {{ count($Performastock) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Cost Per Unit($)</th>
                        <th>Amount($)</th>
                        <th>Rate($)</th>
                        <th>Amount Rs.</th>
                        <th>Insurance</th>
                        <th>Bank Charges</th>
                        <th>Freight</th>
                        <th>Profit Investment</th>
                        <th>Morabaha Profit</th>
                        <th>Movement/In-bndg</th>
                        <th>Duty</th>
                        <th>Total Amount</th>
                        <th>Total Qty</th>
                        <th>Rel. Qty</th>
                        <th>Bal. Qty</th>
                        <th>Cost Per Unit</th>
                        <th>Balance LC</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($Performastock) > 0)
                       <?php
                        $total_amount = 0;
                        $amount_rs = 0;
                        $insurance = 0;
                        $bank_charges = 0;
                        $freight = 0;
                        $profit_investment = 0;
                        $murahba_profit = 0;
                        $mvmnt_bndg = 0;
                        $Duty = 0;
                        $f_total_amount = 0;
                        $total_qty = 0;
                        $rel_qty = 0;
                        $balQTY = 0;
                        $costprice = 0;
                        $balanceLC = 0;?>
                        @foreach ($Performastock as $Performastocks)
                            <?php
                                $Relqty = $Performastocks->total_qty - $Performastocks->balanace_qty;
                                if($Performastocks->f_total_amount >0){
                                    $cost_unit = round($Performastocks->f_total_amount / $Performastocks->total_qty);
                                }else{
                                    $cost_unit = round($Performastocks->amount_rs / $Performastocks->total_qty);
                                }
                                $balanc_lc = round(($Performastocks->balanace_qty*$cost_unit));
                                $total_amount = round(($total_amount + $Performastocks->total_amount));
                                $amount_rs = round(($amount_rs + $Performastocks->amount_rs));
                                $insurance = round(($insurance + $Performastocks->insurance));
                                $bank_charges = round(($bank_charges + $Performastocks->bank_charges));
                                $freight = round(($freight + $Performastocks->freight));
                                $profit_investment = round(($profit_investment + $Performastocks->profit_investment));
                                $murahba_profit = round(($murahba_profit + $Performastocks->murahba_profit));
                                $mvmnt_bndg = round(($mvmnt_bndg + $Performastocks->mvmnt_bndg));
                                $Duty = round(($Duty + $Performastocks->Duty));
                                $f_total_amount = round(($f_total_amount + $Performastocks->f_total_amount));
                                $total_qty = round(($total_qty + $Performastocks->total_qty));
                                $rel_qty = round(($rel_qty + $Relqty));
                                $balQTY = round(($balQTY + $Performastocks->balanace_qty));
                                $costprice = round(($costprice + $cost_unit));
                                $balanceLC = round(($balanceLC + $balanc_lc));
                            ?>
                            <tr data-entry-id="{{ $Performastocks->id }}">
                                <td>{{ $Performastocks->productsModel->products_short_name }}  -  {{ $Performastocks->productsModel->item_code}}</td>
                                <td>{{ $Performastocks->amount}}</td>
                                <td>{{ $Performastocks->total_amount}}</td>
                                <td>{{$doller_rate}}</td>
                                <td>{{ $Performastocks->amount_rs}}</td>
                                <td>{{ $Performastocks->insurance}}</td>
                                <td>{{ $Performastocks->bank_charges}}</td>
                                <td>{{ $Performastocks->freight}}</td>
                                <td>{{ $Performastocks->profit_investment}}</td>
                                <td>{{ $Performastocks->murahba_profit}}</td>
                                <td>{{ $Performastocks->mvmnt_bndg}}</td>
                                <td>{{ $Performastocks->Duty}}</td>
                                <td>
                                    @if($Performastocks->f_total_amount)
                                        {{ $Performastocks->f_total_amount}}
                                    @else
                                        {{ $Performastocks->amount_rs}}
                                     @endif

                                </td>
                                <td>{{ $Performastocks->total_qty}}</td>
                                <td>{{$Relqty }}</td>
                                <td>{{ $Performastocks->balanace_qty }}</td>
                                <td>{{ $cost_unit}}</td>
                                <td>{{ $balanc_lc}}</td>
                            </tr>
                        @endforeach
                       <tr>
                           <th>Total Amount</th>
                           <th></th>
                           <th>{{ number_format($total_amount , 4, '.', ',')}}</th>
                           <th></th>
                           <th>{{ number_format($amount_rs , 4, '.', ',')}}</th>
                           <th>{{  number_format($insurance , 4, '.', ',')}}</th>
                           <th>{{  number_format($bank_charges , 4, '.', ',')}}</th>
                           <th>{{  number_format($freight , 4, '.', ',')}}</th>
                           <th>{{  number_format($profit_investment , 4, '.', ',')}}</th>
                           <th>{{  number_format($murahba_profit , 4, '.', ',')}}</th>
                           <th>{{  number_format($mvmnt_bndg , 4, '.', ',')}}</th>
                           <th>{{  number_format($Duty , 4, '.', ',')}}</th>
                           <th>{{  number_format($f_total_amount , 4, '.', ',')}}</th>
                           <th>{{ $total_qty }}</th>
                           <th>{{  $rel_qty }}</th>
                           <th>{{  $balQTY }}</th>
                           <th>{{  number_format($costprice , 4, '.', ',')}}</th>
                           <th>{{  number_format($balanceLC , 4, '.', ',')}}</th>
                       </tr>
                    @else
                        <tr>
                            <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
//            $('#costing_table').DataTable({
//                "columnDefs": [ {
//                    "targets": 0,
//                    "orderable": false
//                } ]
//            });

            $('#costing_table').dataTable( {
                "ordering": false,
//                "aoColumnDefs": [
//                    { 'bSortable': false, 'aTargets': [ 0] }
//                ]
            });

//            $(document).ready(function(){
//                $('#datatable').DataTable()
//            });

        });
    </script>
    {{--<script>--}}
{{--window.route_mass_crud_entries_destroy = '{{ route('admin.performastock.mass_destroy') }}';--}}
{{--</script>--}}
@endsection