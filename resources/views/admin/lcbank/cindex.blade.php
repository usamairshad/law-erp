@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
  
@section('content')
    <div class="box-body pad table-responsive">
        <div class="panel-body table-responsive">
            <table class="table table-bordered" style="width: 50%; margin: auto;">
                <tbody>
                <tr>
                    <td>Do Calculate</td>
                    <td>{{ $LcBankModel->open_date}}</td>
                </tr>
                <tr>
                    <td>LC / {{ $LcBankModel->banksModel->bank_name}} {{ $LcBankModel->lc_no}} ${{ $ComercialInvoiceModel->usd_amt}}</td>
                    <td><strong>Murabaha Expiry:  </strong>{{ $ComercialInvoiceModel->lc_expiry_date}}</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    {{--<h3 class="page-title">Do Calculate</h3>--}}

    <div class="box-header with-border">
        <a href="{{ route('admin.cinvoice.index') }}" class="btn btn-success pull-right">Back</a>&nbsp;
        {{--<a href="{{ route('admin.lcbank.costing',[$LcBankModel->id]) }}" class="btn btn-success">LcCosting</a>--}}
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table  id="datatable" class="table table-bordered table-striped {{ count($Performastock) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Product Name</th>
                        <th>Total Qty</th>
                        <th>Balance Qty</th>
                        <th>Amount($)</th>
                        <th>Total Amount($)</th>
                        <th>Rate($)</th>
                        <th>Amount Rs.</th>
                        <th>Less(30%)Margin</th>
                        <th>Total Murabaha</th>
                        <th>Murabaha Profit</th>
                        <th>Net Murabaha</th>
                        <th>Cost Per Unit</th>
                        <th>Balance Murabaha</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($Performastock) > 0)
                        <?php
                        $a = 1;
                        $totalqty = 0;
                        $totalbqty = 0;
                        $totalamt = 0;
                        $totalamt_rs = 0;
                        $totalmargin = 0;
                        $totalmurahbah = 0;
                        $total_murahbah_prft = 0;
                        $total_net_murahbah = 0;
                        $total_bal_murahbah = 0;
                        ?>
                        @foreach ($Performastock as $key)
                            <?php
                            $totalqty += $key['total_qty'];
                            $totalbqty += $key['bal_qty'];
                            $totalamt += $key['total_amount'];
                            $totalamt_rs += $key['amount_rs'];
                            $totalmargin += $key['margin'];
                            $totalmurahbah += $key['total_murahba'];
                            $total_murahbah_prft += $key['murahba_profit'];
                            $total_net_murahbah += $key['net_murahba'];
                            $total_bal_murahbah += $key['balance_murahba'];
                            ?>
                            <tr>
                                <td><?php echo $a++; ?></td>
                                <td>{{ $key['product_name']}}</td>
                                <td>{{ $key['total_qty']}}</td>
                                <td>{{ $key['bal_qty']}}</td>
                                <td>{{ $key['amount']}}</td>
                                <td>{{ $key['total_amount']}}</td>
                                <td>{{ $ComercialInvoiceModel['today_doller']}}</td>
                                <td>{{ $key['amount_rs']}}</td>
                                <td>{{ $key['margin']}}</td>
                                <td>{{ $key['total_murahba']}}</td>
                                <td>{{ $key['murahba_profit']}}</td>
                                <td>{{ $key['net_murahba']}}</td>
                                <td>{{ $key['cost_per_unit']}}</td>
                                <td>{{ $key['balance_murahba']}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th>Total</th>
                            <th></th>
                            <th>{{$totalqty}}</th>
                            <th>{{$totalbqty}}</th>
                            <th></th>
                            <th>{{ number_format($totalamt , 4, '.', ',')}}</th>
                            <td></td>
                            <th>{{ number_format($totalamt_rs , 4, '.', ',')}}</th>
                            <th>{{ number_format($totalmargin , 4, '.', ',')}}</th>
                            <th>{{ number_format($totalmurahbah , 4, '.', ',')}}</th>
                            <th>{{ number_format($total_murahbah_prft , 4, '.', ',')}}</th>
                            <th>{{ number_format($total_net_murahbah , 4, '.', ',')}}</th>
                            <td></td>
                            <th>{{ number_format($total_bal_murahbah , 4, '.', ',')}}</th>
                        </tr>
                    @else
                        <tr>
                            <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('#datatable').DataTable({
//                "sScrollY": "200px",
//                "sScrollX": "100%"
            });
        });
    </script>
@endsection