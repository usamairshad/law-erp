@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>View GR Import</h1>
    </section>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ route('admin.grimport') }}" class="btn btn-success pull-right">Back</a>
        </div>


        <div class="panel-body pad table-responsive">
            <table class="table table-bBanked table-striped dt-select" id="users-table">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Shipment Id</th>
                    <th>Product Name</th>
                    <th>Release Quantity</th>
                    <th>Created Date</th>
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @foreach ($result as $results)

                    <tr>
                        <td>{{$r}}</td>
                        {{--<td> {{$results->lcModel->lc_no}} </td>--}}
                        <td> {{$results->cinvoiceModel->ci_no}} </td>
                        <td> {{$results->productsModel->products_short_name.'-'.$results->productsModel->item_code}} </td>
                        <td> {{$results->rel_qty}} </td>
                        <td> {{$results->created_at}} </td>
                    </tr>
                    @php($r++)
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('#users-table').DataTable();
    </script>
@endsection