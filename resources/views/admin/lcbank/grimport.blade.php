@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>GR Import</h1>
    </section>
    {{ csrf_field() }}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ route('admin.grimport.create')}}" class="btn btn-success pull-right">Add New GR</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped datatable" id="users-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Invoice#</th>
                    <th>LC</th>
                    <th>Dollar Rate</th>
                    <th>Total Amount $</th>
                    <th>Net Amount (PKR)</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if (count($cInvoice) > 0)
                    @foreach ($cInvoice as $lcs)
                        <tr data-entry-id="{{ $lcs->id }}">

                            <td>{{ $lcs->id}}</td>
                            <td>{{ $lcs->ci_no}}</td>
                            <td>{{ $lcs->lCmodel->lc_no }}</td>
                            <td>{{ $lcs->today_doller }}</td>
                            <td>{{ $lcs->usd_amt }}</td>
                            <td>{{ $lcs->usd_amt * $lcs->today_doller }}</td>
                            <td>{{ 'In Proccess' }}</td>
                            <td>
                                
                                <button  href="#"type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#edit-modal{{$lcs->id}}">

                                    Clearence Date
                                </button>
                                <a href="{{ route('admin.grimport.view',$lcs->id)}}" class="btn btn-success btn-xs">View</a>
                                @if ($lcs->lCmodel->transaction_type == 1)
                                
                                    <a href="{{ route('admin.grimport.buffer.view',$lcs->id)}}" class="btn btn-primary btn-xs">In-bound Quantity</a>
                                @endif
                              

                            </td>
                            </td>

                        </tr>
                        <div class="modal fade" id="edit-modal{{$lcs->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        {!! Form::model($lcs, ['method' => 'POST', 'route' => ['admin.comercialinvoiceupd.update',$lcs->id]]) !!}

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title" align="center"><b>Closing Date</b></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box-body">
                                            <div class="form-group col-md-6 @if($errors->has('close_date')) has-error @endif" >
                                                {!! Form::label('close_date', 'Closing Date', ['class' => 'control-label']) !!}
                                                {!! Form::text('close_date',old('close_date'), ['id' => 'close_date' ,'class' => 'form-control datepicker']) !!}
                                                @if($errors->has('close_date'))
                                                    <span class="help-block">
                                                        {{ $errors->first('close_date') }}
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger ']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>



@stop

@section('javascript')
    <script>
        var FormControls = function(){
            $('.datatable').DataTable();
            $(".datepicker").datepicker({ format: 'yyyy-mm-dd' });
            baseFunction = function(){

            }

            return{
                init: function(){
                    baseFunction();
                },
                fetchLcProducts: fetchLcProducts

            }
        }();

        $(document).ready(function(){
            FormControls.init();

        });
        $(document).on('click', 'button[data-id]', function (e) {
            var requested_to = $(this).attr('data-id');
console.log(requested_to);
            // Do whatever else you need to do.
        });
    </script>
    <!-- <script src="{{ url('js/admin/lcbanks/grimport_list.js') }}" type="text/javascript"></script> -->
@endsection