@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Stock Detail</h1>
    </section>
    {{ csrf_field() }}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ route('admin.grimport')}}" class="btn btn-success pull-right">Back</a>

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
                <div class="row">
                    <div class="col-md-4 pull-right">
                            <form action="{{route('admin.grimport.transfer.inventory')}}" method="post">
                                    {{csrf_field()}}
                                    <select name="warehouse_id" id="" class="form-control">
                                        <option value="">Select Warehouse</option>
                                        @foreach ($warehouse as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="c_id" value="{{$id}}">
                
                                    <input type="submit" value="Transfer Inventory" class="btn btn-primary pull-right">
                                </form>
                    </div>
                </div>
            <table class="table table-bordered table-striped datatable" id="users-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Product</th>
                    <th>Release quantity</th>
                    <th>unit_price</th>
                    <th>status</th>
                    <th>Created At</th>
                </tr>
                </thead>
                <tbody>
                @if (count($result) > 0)
                    @foreach ($result as $buffer)
                        <tr>    
                            <td>{{$buffer->id}}</td>
                            <td>{{$buffer->products->products_short_name}}</td>
                            <td>{{$buffer->rel_qty}}</td>
                            <td>{{$buffer->unit_price}}</td>
                            <td>
                                @if ($buffer->status == 0)
                                    <label for="" class="label label-warning">Not Transfer</label>
                                @else
                                    <label for="" class="label label-success">Transfer</label>
                                @endif
                            </td>
                            <td>{{$buffer->created_at->format('Y-m-d')}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>



@stop

