<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sale Tax Invoice</title>

    <style>
        * {
            box-sizing: border-box;
        }

        /* Create four equal columns that floats next to each other */
        .column {
            float: left;
            width: 50%;

        /* Should be removed. Only for demonstration */
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
    </style>


</head>


<style>

    body{
        padding: 0 30px;
        line-height: 14px;
    }
</style>

<body>

<!--header-->

<div class="header">



    <h6 style="border-bottom: 1px solid black; font-size: 36px; padding-bottom: 7px; margin-bottom: 20px !important;">
        OAG <Span style="margin-left: 100px;">Office Automation Group</Span>
    </h6>

    <p style="font-size: 9px; margin-top: -20px; margin-bottom: 20px;">
        Head Office: 8/1 Habibullah Road, off Davis Road, Lahore 54000 Tel: 042-636 2835 Fax: 92-42-636 2834
        Email: info@oag.com.pk Wweb Site: www.oag.com.pk
    </p>




</div>


<table style="border-top:1px solid #000000;  margin:10px 0 0px 0;" width="100%" >
    <tbody>
    <tr>
        <th  style="font-size: 14px;">Invoice #</th>
        <td style="font-size: 14px">{{$SalesInvoiceMode->invoice_no}}</td>
        <td style="font-size: 18px;" ></td>
        <td width="25%" style="font-size: 12px;">Print Date: {{ Date('d/m/Y') }}</td>


    </tr>
    <tr>
        <th  style="font-size: 14px;">Invoice Date:</th>
        <td style="font-size: 14px">{{date('d/m/Y', strtotime($SalesInvoiceMode->invoice_date)) }}</td>
        <td style="font-size: 18px; font-weight: bolder;" >Sales Invoice</td>
        <td style="font-size: 12px;" >Print Time: {{date("h:i:sa") }}</td>


    </tr>
    <tr>
        <th width="15%" style="font-size: 14px;">Due Date:</th>
        <td style="font-size: 14px">{{date('d/m/Y',strtotime($SalesInvoiceMode->due_date))}} </td>



    </tr>

    </tbody>
</table>




<div class="supplier-info" style="width: 100%; border: 1px solid black; border-radius: 10px;">


    <table style="  margin:5px 0 5px 0; " width="100%" >
        <tbody  style="font-size: 14px">
        <tr>
            <th>Name:</th>
            <td style="font-size: 14px">{{ $SalesInvoiceMode->CustomersModel->name }}</td>

        </tr>
        <tr>
            <th>Adress:</th>
            <td style="font-size: 14px">{{$SalesInvoiceMode->CustomersModel->street}} {{$SalesInvoiceMode->CustomersModel->city}}</td>

        </tr>
        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>

        </tr>
        <tr>
            <th>Phone:</th>
            <td style="font-size: 14px">{{ $SalesInvoiceMode->CustomersModel->office_phone }}</td>

        </tr>
        <tr>
            <th>Email :</th>
            <td style="font-size: 14px">{{ $SalesInvoiceMode->CustomersModel->primary_email }}</td>

        </tr>
        <tr>
            <th>NTN :</th>
            <td style="font-size: 14px">{{ $SalesInvoiceMode->CustomersModel->ntn }}</td>

        </tr>
        <tr>
            <th>GST #:</th>
            <td style="font-size: 14px">{{ $SalesInvoiceMode->CustomersModel->gst }}</td>

        </tr>
        </tbody>
    </table>



</div>



<table style="  margin:5px 0 5px 0; " width="100%" >
    <tbody style="font-size: 14px;">
    <tr>
        <th style="">P.O.#:</th>
        <td>{{$SalesInvoiceMode->po_no}}</td>
        <th style="">R.R.#:</th>
        <td>{{$SalesInvoiceMode->rr_no}}</td>
        <th></th>
        <td>F.P</td>
    </tr>
    <tr style="margin-top: 20px;">
        <th>Date:</th>
        <td>{{date('d/m/Y', strtotime($SalesInvoiceMode->invoice_date))}}</td>
        {{-- <th  style="">Date:</th>
        <td></td>
        <th >Date:</th>
        <td>09/09/2019</td> --}}
    </tr>


    </tbody>
</table>


<div class="invoice table" style="width: 100%; margin-top: 0px;">


    <table width="100%;"  style="border-collapse: collapse; font-size: 14px;">
        <tr>
            <th style="border: 1px solid #000; text-align: left; padding-left: 8px;">Sr.#</th>
            <th style="border: 1px solid #000; text-align: left; padding-left: 8px;">Description</th>
            <th style="border: 1px solid #000; text-align: left; padding-left: 8px;">Machine counter</th>
            <th style="border: 1px solid #000; text-align: left; padding-left: 8px;">Qty</th>
            <th style="border: 1px solid #000; text-align: left; padding-left: 8px;">Rate</th>
            <th style="border: 1px solid #000; text-align: left; padding-left: 8px;">Amount</th>

        </tr>
        <?php $counter = 1?>
        @foreach($Products as $key => $val)
            <tr>
                <td style="border: 1px solid #000; text-align: left; padding-left: 8px;">{{$counter++}}</td>
                <td style="border: 1px solid #000; text-align: left; padding-left: 8px;"> {{ $val->print_name }} [ {{$val->serialNo}}]</td>
                <td style="border: 1px solid #000; text-align: left; padding-left: 8px;">{{$val->total_count}}</td>
                <td style="border: 1px solid #000; text-align: left; padding-left: 8px;">{{$val->quantity}}</td>
                <td style="border: 1px solid #000; text-align: left; padding-left: 8px;">{{$val->unit_price}}</td>
                <td style="border: 1px solid #000; text-align: left; padding-left: 8px;">{{$val->total_price}}</td>

            </tr>
        @endforeach


<?php $padding = $counter * 15;
        ?>
        <tr>
            <td style="border: 1px solid #000; text-align: left; padding-top: {{ 270 - $padding}}px;"></td>
            <td style="border: 1px solid #000; text-align: left; padding-top: {{ 270 - $padding}}px;"></td>
            <td style="border: 1px solid #000; text-align: left; padding-top: {{ 270 - $padding}}px;"></td>
            <td style="border: 1px solid #000; text-align: left; padding-top: {{ 270 - $padding}}px;"></td>
            <td style="border: 1px solid #000; text-align: left; padding-top: {{ 270 - $padding}}px;"></td>
            <td style="border: 1px solid #000; text-align: left; padding-top: {{ 270 - $padding}}px;"></td>
        </tr>




    </table>


</div>


<div class="row" style="">
    <div class="column" style=" font-size: 14px;">


        <div class="listing" style="font-size: 12px;">

            <h3>
                E&OE:
            </h3>
            <ol>
                <li>Goods once sold cannot be taken back or exchanged.</li>
                <li>Goods remain property of company until full & final payment.</li>
                <li>In case of any dispute only Lahore High Court will havethe jurisdiction.</li>
                <li style="font-weight: bold">All payments by crossed cheque payee's account only.</li>
            </ol>


        </div>




    </div>

    <div class="column" style=" font-size: 14px;">


        <table style="  margin:30px 0 50px 0; text-align: right; " width="100%" >
            <tbody style="">
            <tr>
                <td style="font-size: 14px; font-weight: bold;">
                    <span style="font-size: 14px; font-weight: bold; margin-right: 10px;">Amount:</span>
                    {{ $SalesInvoiceMode->net_income + $SalesInvoiceMode->overall_discount }}
                </td>
            </tr>
            @if ($SalesInvoiceMode->total_discount == null)
            <tr>
                <td style="font-size: 14px;">
                    <span style="font-size: 14px; font-weight: bold; margin-right: 40px;">Discount:</span>
                    <spam style=" border-bottom: 1px solid black;">{{$SalesInvoiceMode->overall_discount}}</spam>
                </td>
            </tr>
            @else
            <tr>
                <td style="font-size: 14px;">
                    <span style="font-size: 14px; font-weight: bold; margin-right: 40px;">Discount:</span>
                    <spam style=" border-bottom: 1px solid black;">{{$SalesInvoiceMode->total_discount}}</spam>
                </td>
            </tr>
            @endif
            @if($SalesInvoiceMode->invoice_type == "taxable")
                <tr>
                    <td style="font-size: 14px;">17.00%
                        <span style="font-size: 14px; font-weight: bold; margin-right: 10px;">GST:</span>
                        <spam style=" border-bottom: 1px solid black;">{{$SalesInvoiceMode->total_tax}}</spam>

                    </td>
                </tr>
            @endif
            <tr>

                <td style="font-size: 14px; font-weight: bold;">
                    <span style="font-size: 14px; font-weight: bold; margin-right: 10px;">Net Amount:</span>
                    {{$SalesInvoiceMode->payable_amount}}
                </td>
            </tr>

            <tr>
                <td style="font-size: 14px; font-weight: bold;">
                    <span style="font-size: 14px; font-weight: bold; margin-right: 10px;">Amount Payable:</span>
                    <spam style=" border-bottom: 1px solid black;">{{ $SalesInvoiceMode->payable_amount }}</spam>

                </td>
            </tr>
            <tr>
                <td  style="font-size: 14px;">
                    Pak Rupees: {{$SalesInvoiceMode->payable_amount}}
                </td>
            </tr>

            </tbody>
        </table>

    </div>

</div>

<!--footer-->

<div class="authorised-sig" style=" margin-top: 1px;">

    <p style="border-top: 1px solid black; font-weight: bold; display: inline-block; margin-left: 250px;">
        Authorized Signature
    </p>


</div>

<div class="footer-list">

    <P style="font-weight: bold; font-size: 20px">
        Panasonic
    </P>
    <p style="margin-left: 100px; margin-top: -40px; font-size: 14px;">
        Facimiles &nbsp;&nbsp;Lasers &nbsp;&nbsp;PABX &nbsp;&nbsp;Computers &nbsp;&nbsp;Print Board &nbsp;&nbsp;Photo copiers
    </p>
    <div class="footer-logo" style="text-align: right; margin-top: -80px;">
        <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/c4/Konica_Minolta.svg/220px-Konica_Minolta.svg.png" width="15%">

    </div>
</div>

</body>
</html>