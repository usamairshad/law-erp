@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale Invoice</h1>
    </section>
@stop
<style>
    .box-header a:last-child{
        margin-right: 10px;
    }
</style>
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">View</h3>
            <a href="{{ route('admin.salesinvoice.index') }}" class="btn btn-success pull-right">Back</a>
            <a href="downloadPDF/{{$SalesInvoiceMode->id}}" target="_blank" class="btn btn-warning pull-right">Download <u>P</u>df</a>

            {{--<button id="downloadPdf" class="btn btn-warning pull-right">Download <u>P</u>df</button>--}}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th width="10%">Invoice No</th>
                    <td>{{ $SalesInvoiceMode->invoice_no }}</td>
                    <th width="10%">Customer Name</th>
                    <td>{{ $SalesInvoiceMode->CustomersModel->name }}</td>

                    <th width="5%">Credit Limit</th>
                    <td id="credit_limit">{{ $credit_limit }}</td>
                    <th width="10%">Balance</th>
                    <td id="customer_credit">{{ $customer_credit }}</td>

                </tr>

                <tr>
                    <th>Invoice Date</th>
                    <td >{{ $SalesInvoiceMode->invoice_date }}</td>
                    <th width="5%">RR No</th>
                    <td>{{ $SalesInvoiceMode->rr_no }}</td>
                    <th width="5%">PO. No</th>
                    <td>{{ $SalesInvoiceMode->po_no }}</td>
                    <th width="10%">Amount</th>
                    <td>{{ $SalesInvoiceMode->net_income + $SalesInvoiceMode->overall_discount }}</td>


                </tr>
                <tr>
                    <th width="10%">Discount</th>
                    <td>
                        @if ($SalesInvoiceMode->total_discount == null)
                            {{$SalesInvoiceMode->overall_discount }}
                        @else
                            {{ $SalesInvoiceMode->total_discount}}
                        @endif
                    </td>
                    <th width="10%">Grand Total</th>
                    <td>{{ $SalesInvoiceMode->net_income}}</td>
                    <th width="10%">Tax Amount</th>
                    <td>{{ $SalesInvoiceMode->total_tax}}</td>

                    <th width="10%" id="">Amount Payable</th>
                    <td id="payable_amount">{{ $SalesInvoiceMode->payable_amount}}</td>
                </tr>
                <tr>
                    <th width="10%">Invoice Type</th>
                    <td>{{ $SalesInvoiceMode->invoice_type == 'taxable' ? 'Taxable' : 'Non Taxable' }}</td>

                </tr>
                </tbody>
            </table>

            <!-- Slip Area Started -->
            <section class="content-header" style="padding: 10px 15px !important;">
                <h1>Product View</h1>
            </section>


            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Print Name</th>
                    <th>Machine Counter</th>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                @forelse($Products as $product)
                    <tr>
                        <td><b>{{ $product->productsModel->products_name.$product->productsModel->products_description }}</b> [{{$product->serialNo}}]</td>
                        <td>{{ $product->print_name ? $product->print_name : 'N/A' }}</td>
                        <td>{{ $product->total_count }}</td>
                        <td>{{ $product->quantity }}</td>
                        <td>{{ $product->unit_price }}</td>
                        <td>{{ $product->total_price }}</td>
                    </tr>
                    @if($product->productsModel->products_description)
                        <tr>
                            <th>Product Description</th>
                        </tr>
                        <tr>
                            <td>{{ $product->productsModel->products_description }}</td>
                        </tr>
                    @else
                        <tr>
                            <th>Product Description</th>
                        </tr>
                        <tr>
                            <td>No Description of product was added</td>
                        </tr>
                    @endif
                @empty
                    <tr>
                        <td colspan="5">No Record Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>



           

        </div>
        <!-- /.box-body -->
    </div>






@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
        $("#downloadPdf").click(function(){
            var inv_id = {{$SalesInvoiceMode->id}};
            var payable = $('#payable_amount').html();
            var credit_limit = parseInt($('#credit_limit').html());
            var customer_credit = $('#customer_credit').html();
            var payable = $('#payable_amount').html();
            var newPayable = parseInt(payable)  + parseInt(customer_credit);
            if(newPayable > credit_limit){
                if (confirm('Customer invoice is exceeding the credit limit. Do You still want to generate invoice?')) {
                    window.open('downloadPDF/'+inv_id, '_blank');
                } else {
                    return false
                }

            }

//if()
//
        });
    </script>
@endsection

@section('javascript')
    <script>
        {{--window.route_mass_crud_entries_destroy = '{{ route('admin.products.mass_destroy') }}';--}}
    </script>
@endsection

