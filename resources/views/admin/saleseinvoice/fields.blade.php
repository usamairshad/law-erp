
<div id="11" class="panel-collapse collapse in col-md-12">

    {{ csrf_field() }}
    <div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif" id="order_no_div">
        {!! Form::label('order_no', 'Order No ', ['class' => 'control-label']) !!}
        {!! Form::select('order_no', $Sales , old('order_no'),
        ['class' => 'form-control select2'  ,'id' => 'order_no', 'onchange' => 'FormControls.fetchFilterrecord();']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('invoice_date')) has-error @endif">
        {!! Form::label('invoice_date', 'Invoice Date', ['class' => 'control-label']) !!}
        {!! Form::text('invoice_date', old('invoice_date'), ['class' => 'form-control','readonly' => 'true','id'=> 'invoice_date']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('due_date')) has-error @endif">
        {!! Form::label('due_date', 'Due Date', ['class' => 'control-label']) !!}
        {!! Form::text('due_date', old('due_date'), ['class' => 'form-control datepicker','readonly' => 'true','id'=> 'due_date']) !!}
        @if($errors->has('due_date'))
            <span class="help-block">
                {{ $errors->first('due_date') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('rr_no')) has-error @endif">
        {!! Form::label('rr_no', 'R.R No', ['class' => 'control-label']) !!}
        {!! Form::text('rr_no', old('rr_no'), ['class' => 'form-control','readonly' => 'true','id'=> 'rr_no','maxlength'=>'10']) !!}
        @if($errors->has('rr_no'))
            <span class="help-block">
                {{ $errors->first('rr_no') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('delivery_type')) has-error @endif" id="delivery_type_div">
        {!! Form::label('delivery_type', 'Delivery Type', ['class' => 'control-label']) !!}
        {!! Form::select('delivery_type', array('' => 'Select Delivery Type') + Config::get('hrm.delivery_type_array') , old('delivery_type'),
         ['class' => 'form-control select2'  ,'id' => 'delivery_type', 'disabled' => 'true']) !!}
        @if($errors->has('delivery_type'))
            <span class="help-block">
                {{ $errors->first('delivery_type') }}
            </span>
        @endif
    </div>


    <div class="form-group col-md-3 @if($errors->has('installation_date')) has-error @endif">
        {!! Form::label('installation_date', 'Installation Date', ['class' => 'control-label']) !!}
        {!! Form::text('installation_date', old('installation_date'), ['class' => 'form-control datepicker','id'=> 'installation_date',
        , 'onchange' => 'FormControls.setWarrenty();']) !!}
        @if($errors->has('installation_date'))
            <span class="help-block">
                {{ $errors->first('installation_date') }}
            </span>
        @endif
    </div>


    <div class="form-group col-md-3 @if($errors->has('warrenty')) has-error @endif">
        {!! Form::label('warrenty', 'Warrenty Months', ['class' => 'control-label']) !!}
        {!! Form::text('warrenty', old('warrenty'), ['class' => 'form-control','id'=> 'warrenty' , 'onchange' => 'FormControls.setWarrenty();]) !!}
        @if($errors->has('warrenty'))
            <span class="help-block">
                {{ $errors->first('warrenty') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('expires_at')) has-error @endif">
        {!! Form::label('expires_at', 'Expires At', ['class' => 'control-label']) !!}
        {!! Form::text('expires_at', old('expires_at'), ['class' => 'form-control datepicker','readonly' => 'true','id'=> 'expires_at']) !!}
        @if($errors->has('expires_at'))
            <span class="help-block">
                {{ $errors->first('expires_at') }}
            </span>
        @endif
    </div>

    {!! Form::hidden('services_amount', old('services_amount'), [ 'id' => 'services_amount']) !!}
    {!! Form::hidden('products_amount', old('products_amount'), [ 'id' => 'products_amount']) !!}
    {!! Form::hidden('services_tax', old('services_tax'), [ 'id' => 'services_tax']) !!}
    {!! Form::hidden('products_tax', old('products_tax'), [ 'id' => 'products_tax']) !!}

    {!! Form::hidden('amount', old('amount'), [ 'id' => 'amount']) !!}
    {!! Form::hidden('tax_amount', old('tax_amount'), [ 'id' => 'tax_amount']) !!}
    {!! Form::hidden('total_amount', old('total_amount'), [ 'id' => 'total_amount']) !!}
    {!! Form::hidden('payable_amount', old('payable_amount'), [ 'id' => 'payable_amount']) !!}


    {{--<input type="text" style="display: none" id="amount" name="amount">--}}
    {{--<input type="text" style="display: none" id="tax_amount" name="tax_amount">--}}
    {{--<input type="text" style="display: none" id="total_amount" name="total_amount">--}}
    <input type="text" style="display: none" id="order_id" name="order_id">
    <input type="text" style="display: none" id="customer_id" name="customer_id">

</div>