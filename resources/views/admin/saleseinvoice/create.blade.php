@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale Invoice List</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Sales Invoice Detail</h3>
            @if(Gate::check('invoice_manage'))
                <a href="{{ route('admin.salesinvoice.index') }}" class="btn btn-success pull-right">back</a>
            @endif
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.salesinvoice.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">

            @include('admin.salesinvoice.fields')

            <hr>
            <div><b>Products</b> </div>
            <div class="panel-body pad table-responsive">
                <table class="table table-bordered table-striped" id="users-table">
                    <thead>
                    <tr>
                        <th>Sr#</th>
                        <th>Serial Nos</th>
                        <th>Print Name</th>
                        <th>No of print</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody id="product-table-body">

                    </tbody>
                </table>
            </div>

            <hr>



            <div class="panel-body pad table-responsive">
                <table class="table table-bordered table-striped" id="gross-table">
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th></th>--}}
                        {{--<th></th>--}}
                        {{--<th></th>--}}
                        {{--<th></th>--}}

                    {{--</tr>--}}
                    {{--</thead>--}}
                    <tbody id="gross-table-body">

                    </tbody>
                </table>
            </div>
            <div id="grosstotal"> </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger', 'disabled' => 'true', 'id' => 'btn_save']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <!-- <script src="{{ url('public/js/admin/salesinvoice/list.js') }}" type="text/javascript"></script> -->
    <script>
        /**
         * Created by mustafa.mughal on 12/7/2017.
         */

            //== Class definition
        var FormControls = function () {
                //== Private function

                $('#btn_save').on('click', function(event){
                    if($('#due_date').val() == '' || $('#rr_no').val() == '' || $('#delivery_type').val() == ''){
                        alert('Please fill all the fields');
                        event.preventDefault();
                    }
                });
                document.getElementById('users-table').style.display = "none";

                var baseFunction = function(){

                }

                var fetchFilterrecord = function () {
                    var dateToday = new Date('yyyy-mm-dd');
                    var order_no = $('#order_no').val();
                    console.log('dateToday :', dateToday);
                    var token = $("input[name=_token]").val();
                    $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd', //format: 'DD-MM-YYYY H:m:s A',
                        startDate: '-0m'

                    });
//                    $("#due_date").datepicker({startDate: '-0m'});
                    if(order_no){
                        $.ajax({
                            type: "POST",
                            url: 'getOrderDetail',
                            data: {
                                'order_no' : order_no,
                                '_token': token,
                            },
                            success: function(result){
                                console.log(result);
                                //console.log(result.data[0][1]);

                                $("#users-table tbody > tr").remove();
                                displayOrderDetail( result );
                                //addTable(result.data, result.gross_amount, result.tax_amount, result.tax_percent);
                                //$('#invoice_date').val(result.sales.expected_closing_date);
                                $('#order_id').val(result.order_id);
                                $('#customer_id').val(result.sales.databank_id);
                                $('#po_no').val(result.sales.po_no);

                                document.getElementById('due_date').removeAttribute('readonly');
                                document.getElementById('rr_no').removeAttribute('readonly');
                                document.getElementById('btn_save').removeAttribute('disabled');
                                document.getElementById('delivery_type').removeAttribute('disabled');
                            }
                        });
                    }
                    else{
                        alert('Please Select an Order First');
                    }

                }

                 var displayOrderDetail =  function (data) {
                    var tablehtml ='';
                    var total_amount = 0;
                     var total_tax = 0;
                     var product_total=0;
                     var product_tax=0;

                     var serviceshtml ='';
                     var services_total = 0;
                     var services_tax = 0;

                    var products_array = data.products_array;
                     var services_array = data.services;
                    for(var i=0; i<products_array.length; i++){
                        tablehtml += '<tr>';
                        tablehtml += '<td>'+ (i + 1) + '</td>';
                        tablehtml += '<td><input type="text" name="serials[]" class="serials"> </td>';
                        tablehtml += '<td><input type="text" name="print_name[]" class="print_name"> </td>';
                        tablehtml += '<td><input type="text" name="total_count[]" class="total_count"> </td>';
                        tablehtml += '<td> ' + products_array[i].p_name + ' </td>';
                        tablehtml += '<td> ' + products_array[i].product_qty + ' </td>';
                        tablehtml += '<td> ' + products_array[i].sale_price + ' </td>';
                        tablehtml += '<td> ' + (products_array[i].sale_price * products_array[i].product_qty) + ' </td>';
                        tablehtml += '</tr>';
                        product_total += (products_array[i].sale_price * products_array[i].product_qty);
                        product_tax += (products_array[i].tax_amount * products_array[i].product_qty);
                    }

                     tablehtml += '<tr>';
                     tablehtml += '<td colspan="5"></td>';
                     tablehtml += '<td><b>Amount</b></td>';
                     tablehtml += '<td> ' + product_total + '</td>';
                     tablehtml += '</tr>';

                     tablehtml += '<tr>';
                     tablehtml += '<td colspan="5"></td>';
                     tablehtml += '<td><b> Discount</b></td>';
                     tablehtml += '<td> <input type="number" name="discount" id="discount" class="discount" value="0" min="0"  onchange="FormControls.applyDiscount(this.value)"  onkeydown="FormControls.applyDiscount(this.value)" onkeyup="FormControls.applyDiscount(this.value)" onblur="FormControls.applyDiscount(this.value)"></td>';
                     tablehtml += '</tr>';

                     tablehtml += '<tr>';
                     tablehtml += '<td colspan="5"></td>';
                     tablehtml += '<td><b> Gross Amount</b></td>';
                     tablehtml += '<td id="gross"> '+ (product_total ) +'</td>';
                     tablehtml += '</tr>';

                     tablehtml += '<tr>';
                     tablehtml += '<td colspan="5"></td>';
                     tablehtml += '<td><b>17% GST</b></td>';
                     tablehtml += '<td  id="product_tax"> '+ product_tax +'</td>';
                     tablehtml += '</tr>';



                     tablehtml += '<tr>';
                     tablehtml += '<td colspan="5"></td>';
                     tablehtml += '<td><b> Amount Payable</b></td>';
                     tablehtml += '<td id="payable"> '+ (product_total + product_tax) +'</td>';
                     tablehtml += '</tr>';




                     for(var i=0; i<services_array.length; i++){
                         serviceshtml += '<tr>';
                         serviceshtml += '<td>'+ (i + 1) + '</td>';

                         serviceshtml += '<td> ' + services_array[i].name + ' </td>';

                         serviceshtml += '<td> ' + services_array[i].sale_price + ' </td>';
                         serviceshtml += '<td> ' + services_array[i].sale_price + ' </td>';

                         serviceshtml += '</tr>';
                         services_total += services_array[i].sale_price;
                         services_tax += services_array[i].tax_amount;
                     }

                     serviceshtml += '<tr>';
                     serviceshtml += '<td colspan="2"></td>';
                     serviceshtml += '<td ><b>Amount</b></td>';
                     serviceshtml += '<td> ' + services_total + '</td>';
                     serviceshtml += '</tr>';

                     serviceshtml += '<tr>';
                     serviceshtml += '<td colspan="2"></td>';
                     serviceshtml += '<td><b>16% GST</b></td>';
                     serviceshtml += '<td> '+ services_tax +'</td>';
                     serviceshtml += '</tr>';

                     serviceshtml += '<tr>';
                     serviceshtml += '<td colspan="2"></td>';
                     serviceshtml += '<td><b>Total Amount</b></td>';
                     serviceshtml += '<td> '+ (services_total + services_tax) +'</td>';
                     serviceshtml += '</tr>';


                     total_amount = product_total ;
                     total_tax = product_tax ;


                     $('#services_amount').val(services_total.toFixed(2));
                     $('#products_amount').val(product_total.toFixed(2));
                     $('#services_tax').val(services_tax);
                     $('#products_tax').val(product_tax);



                     $('#amount').val(total_amount.toFixed(2));
                     $('#tax_amount').val(total_tax.toFixed(2));
//                     $('#total_amount').val(data.sales.grand_total);
                     $('#product_total').val(product_total);

//                     $('#services-table-body').html(serviceshtml);
                     $('#product-table-body').html(tablehtml);

                     var grosshtml = '<tr>';

//                     grosshtml += '<td style="text-align: right"><b>Gross Total</b></td> <td style="text-align: center">'+ data.sales.grand_total +'</td></tr>';

                     grosshtml += '<td style="text-align: right"><b>Gross Total</b></td> <td style="text-align: center" id="gross_total">'+ (product_total + total_tax) +'</td></tr>';
                     $('#gross-table-body').html(grosshtml);


                     $('#users-table').css('display', 'inline-table');
                     applyDiscount(0);
                     calculate_tax();

                 }
                 var applyDiscount =  function ( value) {
                    if(!value){
                        value=0;
                    }
//                    var total_amount = $("#total_amount").val();
//                     var payable_amount = parseInt(total_amount).toFixed(2) - parseInt(value).toFixed(2);
                     var amount = $("#amount").val();
                     var gross = parseInt(amount).toFixed(2) - parseInt(value).toFixed(2);
                     // check if invoice is taxable or not





//                     $('#payable').html(payable_amount);

                     $('#gross').html(gross);
                     $('#total_amount').val(gross.toFixed(0));

                     // this part should be in tax version
                     calculate_tax();
//                     var tax_amount = gross * (0.17);
//                     var total_after_tax = gross + tax_amount;
//                     $('#product_tax').html(tax_amount.toFixed(0));
//                     $('#payable').html(total_after_tax.toFixed(0));
//
//
//                     $('#tax_amount').val(tax_amount.toFixed(0));
//
//                     $('#products_tax').val(tax_amount.toFixed(0));
//
//                     $('#payable_amount').val(total_after_tax.toFixed(0));

                 }
                var invoice_type = function (obj) {
                    calculate_tax();
                }

                var calculate_tax = function () {
                    var gross = parseInt( $('#total_amount').val() );

                    if($('#taxable_invoice').is(':checked')){

                        var tax_amount = gross * (0.17);
                        var total_after_tax = parseInt(gross) + tax_amount;
                        console.log('checked :',gross, tax_amount, total_after_tax);
                        $('#product_tax').html(tax_amount.toFixed(0));
                        $('#payable').html(total_after_tax.toFixed(0));


                        $('#tax_amount').val(tax_amount.toFixed(0));

                        $('#products_tax').val(tax_amount.toFixed(0));

                        $('#payable_amount').val(total_after_tax.toFixed(0));
                        $('#gross_total').html(total_after_tax.toFixed(0));
                    }
                    else {

//                        alert('Non Taxable');
                        console.log(' un checked :',gross, tax_amount, total_after_tax);
                        $('#product_tax').html(0);
                        $('#payable').html(gross.toFixed(0));


                        $('#tax_amount').val(0);

                        $('#products_tax').val(0);

                        $('#payable_amount').val(gross.toFixed(0));
                        $('#gross_total').html(gross.toFixed(0));

                    }

                }

                var addTable = function(table_data, gross_amount, tax_amount, tax_percent){


                    if(table_data.length > 0){
                        var table = document.getElementById("product-table-body");
                        document.getElementById('users-table').style.display = "table";

                        for (var i = 0; i < table_data.length; i++) {
                            var row = table.insertRow(i);

                            for (var j = 0; j < table_data[i].length; j++) {
                                var cell = row.insertCell(j);
                                cell.innerHTML = table_data[i][j];
                            }

                        }

                        //For Amount
                        var row = table.insertRow(table_data.length);
                        for(var i=0; i < 6; i++){
                            var cell = row.insertCell(i);
                            if(i==4){
                                cell.setAttribute("style", "font-weight: bold;");
                                cell.innerHTML = "Amount";
                            }
                            else if(i==5){
                                cell.innerHTML = "Rs. "+gross_amount.toFixed(2);
                            }
                            else
                                cell.innerHTML = " ";
                        }

                        //For Tax
                        var row = table.insertRow(table_data.length+1);
                        for(var i=0; i < 6; i++){
                            var cell = row.insertCell(i);
                            if(i==4){

                                cell.setAttribute("style", "font-weight: bold;");
                                cell.innerHTML = tax_percent+"% GST";
                            }
                            else if(i==5){
                                cell.innerHTML = "Rs. "+tax_amount.toFixed(2);
                            }
                            else
                                cell.innerHTML = " ";
                        }

                        //For Gross Amount
                        var row = table.insertRow(table_data.length+2);
                        for(var i=0; i < 6; i++){
                            var cell = row.insertCell(i);
                            if(i==4){
                                cell.setAttribute("style", "font-weight: bold;");
                                cell.innerHTML = "Gross Amount";
                            }
                            else if(i==5){
                                cell.innerHTML = "Rs. "+ (+tax_amount.toFixed(2) + +gross_amount.toFixed(2));
                            }
                            else
                                cell.innerHTML = " ";
                        }


                    }
                    else{
                        document.getElementById('users-table').style.display = "none";
                        document.getElementById('due_date').setAttribute('readonly', 'true');
                        document.getElementById('rr_no').setAttribute('readonly', 'true');
                        document.getElementById('btn_save').setAttribute('disabled', 'true');
                        document.getElementById('delivery_type').setAttribute('disabled', 'true');
                    }
                }

                var setWarrenty = function () {
                    var installation_date = $('#installation_date').val();
                    var months = $('#warrenty').val();
//                    var expires_at =

//                    var moment_date  = moment(installation_date).format('YYYY-MM-DD');
                    var endDateMoment = moment(installation_date); // moment(...) can also be used to parse dates in string format
                    endDateMoment.add(parseInt(months), 'months');
                    $('#expires_at').val(moment(endDateMoment).format('YYYY-MM-DD'))
//                    console.log('endDateMoment : ', moment(endDateMoment).format('YYYY-MM-DD'));

                }

                return {
                    // public functions
                    init: function() {
                        baseFunction();
                    },

                    setWarrenty : setWarrenty,
                    fetchFilterrecord : fetchFilterrecord,
                    addTable : addTable,
                    applyDiscount:applyDiscount,
                    invoice_type: invoice_type,
                    calculate_tax: calculate_tax
                };
            }();

        jQuery(document).ready(function() {
            FormControls.init();
        });
    </script>
@endsection


