@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1> Products</h1>
    </section>
@stop

<style>
    input:-moz-read-only { /* For Firefox */
        background-color: white !important;
    }
    input:read-only  {
        background-color: white !important;
    }
</style>
@section('content')
    <div class="box box-primary">

        <!-- /.box-header -->
        <div class="box-header with-border">
            <h3 class="box-title">Purchase Order</h3>
            <a href="{{ route('admin.saleorder.index') }}" class="btn btn-success pull-right">Back</a>
        </div>

        <!-- /.box-header -->


        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.saleorder.sale_invoice_store'], 'id' => 'validation-form']) !!}
        <div class="box-body">

    <div class="row">
        <div class="col-xs-12 form-group">

            {{--INVOICE NO--}}
            <div class="form-group col-md-4  @if($errors->has('request_date')) has-error @endif">
                {!! Form::label('invoice_no', 'Invoice No', ['class' => 'control-label']) !!}

                <input class="form-control" type="text" disabled readonly  placeholder="??????"  />
                <input class="form-control" name="invoice_no" type="hidden"   placeholder="??????" value="{{ $SalesOrderModel->order_no }}" />
            </div>
            {{--INVOICE NO--}}

            {{--INVOICE Date--}}
            <div class="form-group col-md-4  @if($errors->has('require_date')) has-error @endif">
                {!! Form::label('invoice_date', 'Invoice Date', ['class' => 'control-label']) !!}
               <?php $today = date("Y/m/d"); ?>
                <input class="form-control" name="invoice_date" type="text" readonly  value=" {{$today}}" />
            </div>
            {{--INVOICE Date--}}

            {{--CUSTOMER NAME--}}
            <div class="form-group col-md-4 @if($errors->has('vendor')) has-error @endif">
                {!! Form::label('customer_name', 'Customer Name', ['class' => 'control-label']) !!}
                <input class="form-control" name="customer_name" type="hidden"  value="{{ $SalesOrderModel->customer_id }}" />
                <input class="form-control" type="text" readonly  value="{{ $SalesOrderModel->CustomersModel->name }}" />
            </div>
            {{--CUSTOME NAME--}}
    </div>

<div class="col-lg-12">
    <div class="manually_product_list col-lg-12 col-md-12 col-sm-12">
        <div class="about_featured">
            <div class="panel-group" id="accordion">

                <div class="panel panel-default wow fadInLeft">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#11">
                                <span class="fa fa-check-square-o"></span><b>Product List</b>
                            </a>
                        </h4>
                    </div>

                    <div id="11" class="panel-collapse collapse in">


                        <div class="panel-body pad table-responsive">
                            <table class="table table-bordered table-striped" id="users-table">
                                <thead>
                                <tr>

                                    <th>Sr</th>
                                    <th>Product Name</th>
                                    <th>Uom</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Total</th>
                                    <th>Weight</th>
                                    <th>Total Weight</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php  $counter=1 ?>
                                @forelse ($Products as $result)
                                    <tr id="line_item-{{$counter}}">
                                        <td>{{$counter}}</td>
                                        <td>
                                            {{ $result->productsModel->products_name}}
                                            <input class="form-control" name="line_items[product_id][{{$counter}}]" type="hidden"  value="{{ $result->prouduct_id}}" />
                                        </td>

                                        <td>
                                            <input  class="form-control"  name="line_items[uom][{{$counter}}]" type="text"  value="{{ $result->uom_id}}" id="line_items_uom_id-{{$counter}}" required readonly="true" />
                                        </td>

                                        <td>
                                            <input  class="form-control"  name="line_items[quantity][{{$counter}}]" type="number"  value="{{ $result->quantity}}" id="line_items_quantity_id-{{$counter}}" required readonly="true" />
                                        </td>

                                        <td>
                                            <input id="line_items_uprice_id-{{$counter}}" class="form-control" data-row-id="{{$counter}}" name="line_items[unit_price][{{$counter}}]" type="number" value="{{ $result->unit_price}}" required  readonly="true" />
                                        </td>

                                        <td>
                                            <input  id="line_items_total_id-{{$counter}}"  class="form-control cal_sum"  name="line_items[total_price][{{$counter}}]" type="text" value="{{ $result->total_price}}"  min='1'   required readonly="true"/>
                                        </td>

                                        <td>
                                            <input id="line_items_Weight_id-{{$counter}}" class="form-control "  name="line_items[weight][{{$counter}}]" type="number" value="{{ $result->weight}}" min='1' required readonly="true" />
                                        </td>

                                        <td>
                                            <input id="line_items_Total_Weight_id-{{$counter}}" class="form-control"  name="line_items[total_weight][{{$counter}}]"  min='1' type="number" value="{{ $result->total_weight}}" required readonly="true" />
                                        </td>
                                    </tr>
                                    <?php $counter++; ?>
                                @empty
                                    <tr><td colspan="3">No Record Found</td></tr>
                                @endforelse

                                </tbody>

                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('admin.saleseorder.invoice_fotter')
</div>



    </div>
            <div class="box-footer">

                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>


            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/saleorder/saleInvoice_create_modify.js') }}" type="text/javascript"></script>
@endsection


