@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Course Labs</h3>
    <div class="box box-primary">
        <div class="box-header with-border">
            <a href="{{  route('admin.assign-course-lab-view') }}" style = "float:right"  class="btn btn-success pull-right">View Assigned Course Lab
                        </a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.assign-course-lab-store'], 'id' => 'validation-form']) !!}
        <div id="show">
            <div id="box" class="box-body" style = "padding-top:50px;">
         
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif" style = "float:left;">
                    {!! Form::label('board_id', 'Boards*', ['class' => 'control-label']) !!}
                        <select id="board" class="form-control select2 boards" name="board_id" required="required">
                            <option selected="selected" disabled="disabled">---Select----</option>
                            @foreach($boards as $key => $board)
                            <option value="{{$board['id']}}">{{$board['name']}}</option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif"  style = "float:left;">
                    {!! Form::label('programs', 'Programs*', ['class' => 'control-label']) !!}

                    <select class="form-control select2 programs" name="program_id" id="programs" required="required"></select>

                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif"  style = "float:left;">
                    {!! Form::label('class_id', 'Class*', ['class' => 'control-label']) !!}
                        {{---<select class="form-control select2 classes" name="class_id" required="required">
                             <option selected="selected" disabled="disabled">---Select----</option>
                            @foreach($classes as $key => $class)
                            <option value="{{$class['id']}}">{{$class['name']}}</option>
                            @endforeach
                        </select>--}}

                        <select class="form-control select2 classes" name="class_id" id="classes" required="required"></select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif"  style = "float:left;">
                    {!! Form::label('course_id', 'Courses*', ['class' => 'control-label']) !!}
                        <select class="form-control select2 courses" name="course_id" required="required">
                          <option selected="selected" disabled="disabled">---Select----</option>
                            @foreach($courses as $key => $course)
                            <option value="{{$course['id']}}">{{$course['name']}}</option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif"  style = "float:left;">
                    {!! Form::label('lab_id', 'Lab*', ['class' => 'control-label']) !!}
                        <select id="labs" class="form-control select2 labs" name="lab_id" required="required">
                          <option selected="selected" disabled="disabled">---Select----</option>
                            @foreach($labs as $key => $lab)
                            <option value="{{$lab['id']}}">{{$lab['name']}}</option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif"  style = "float:left;">
                    {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
                        <textarea name="description" class="form-control">
                          
                        </textarea>
                </div>
                {{--<div class="form-group col-md-1">
                    <button style="margin-top: 22px" type="button" name="add" id="add" class="btn btn-success add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>--}}
            </div> 
            <div id="secondbox">
                
            </div>  
            <button id="btn"  class="btn btn-success col-md-12" > Save</button>
            
        </div>

        <!-- /.box-body -->
       
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById('btn').setAttribute("disabled",null);
            $( document ).change(function(){
                var e = document.getElementById("labs");
                var strUser = e.value;
                if (strUser != "") {
                document.getElementById('btn').removeAttribute("disabled");
                } else {
                    document.getElementById('btn').setAttribute("disabled", null);
                }
            })
            
        });
         $(".boards").select2({

            });
    </script>
    <script type="text/javascript">
        function selectRefresh() {
            $(".programs").select2({

            });
        }
        function selectRefresh() {
            $(".classes").select2({

            });
        }
        function selectRefresh() {
            $(".courses").select2({

            });
        }
        function selectRefresh() {
            $(".labs").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
    <script type="text/javascript">


    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" style="height:120px"><hr style="border-top:1px solid #222d32"><div class="form-group col-md-3"><select id="board" class="form-control select2 boards" name="board_id[]">@foreach($boards as $key => $board)<option value="{{$board['id']}}">{{$board['name']}}</option>@endforeach</select></div><div class="form-group col-md-3"><select class="form-control select2 programs" name="programs" id="programs"></select></div><br> <div class="form-group col-md-2"><button style="margin-top: -27px;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');
            $('.select2').select2(); 

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    // function showBranch() {
    //     var data= $('input[name="board_name"]').val();

    //     if(data)
    //     {
    //          $('#show').show();
    //          $('#btn').hide(); 
    //     }
    // }
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
    <script>
        $(function(){
            $('#board').change(function(){
               $("#programs option").remove();;
               var id = $(this).val();
               $.ajax({
                  url : '{{ route( 'admin.board-loadPrograms' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {
                        console.log(result)
                        $('#programs').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        $.each( result, function(k, v) {
                           $('#programs').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });

        $(function(){
            $('#programs').change(function(){
               $("#classes option").remove();;
               var id = $(this).val();
               var board_id =$("select[name*='board_id']").val();

               console.log("board_id "+board_id);
               console.log("program_id "+id);
               $.ajax({
                  url : '{{ route( 'admin.load-board-program-class' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "program_id": id,
                    "board_id":board_id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {     
                        console.log(result)
                        $('#classes').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        $.each( result, function(k, v) {
                            $('#classes').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
@endsection

