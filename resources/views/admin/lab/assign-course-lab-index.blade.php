@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Course Labs</h3>
            <a  href="{{route('admin.course-lab')}}" style="float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Board</th>
                    <th>Program</th>
                    <th>Course</th>
                    <th>Lab</th>
                </tr>
                </thead>

                <tbody>
                @if (count($data) > 0)

                   @foreach ($data as $d)
                        <tr data-entry-id="{{ $d['id'] }}">
                            <td>{{ App\Helpers\Helper::activeSessionIdToBoard($d->active_session_id)}}</td>
                            <td>{{ App\Helpers\Helper::activeSessionIdToProgram($d->active_session_id) }}</td>
                            <td>{{ App\Helpers\Helper::subjectIdToName($d->course_id) }}</td>
                            <td>{{ App\Helpers\Helper::labIdToName($d->lab_id) }}</td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection