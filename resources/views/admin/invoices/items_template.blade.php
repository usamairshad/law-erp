<table style="display: none;">
    <tbody id="line_item-container">
    <tr id="item_header-######" style="
    background: powderblue;">
        <td> Line Item </td>
        <td colspan="5"></td>
    </tr>
    <tr id="line_item-######">

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('line_items[type][######]', 'product',
                  ['class' => 'form-control','id' => 'line_item-type-######',
                      'readonly'=> 'true']) !!}
            </div>
        </td>

        <td >
            <div class="form-group" style="margin-bottom: 0px !important;">

                <div class="form-group" style="margin-bottom: 0px !important;">

                    <select id="line_item-product_id-######" name="line_items[product_id][######]" class="form-control" style="width:100%;"
                            onkeydown = 'FormControls.CalculateTotal();'
                            onchange = 'FormControls.CalculateTotal();'
                            onkeyup = 'FormControls.CalculateTotal();'
                            onblur = 'FormControls.CalculateTotal();'>
                </div>

            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('line_items[product_qty][######]', old('product_qty'),
                 ['class' => 'form-control','id' => 'line_item-product_qty-######',
                    'maxlength'=>'10', 'min'=> '1',
                     'onkeydown' => 'FormControls.CalculateTotal(######);',
                    'onkeyup' => 'FormControls.CalculateTotal(######);',
                    'onblur' => 'FormControls.CalculateTotal(######);']) !!}
            </div>
        </td>


        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('line_items[print_name][######]', old('print_name'),
                 ['class' => 'form-control','id' => 'line_item-print_name-######','maxlength'=>'20'
                    ]) !!}
            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[no_of_print][######]', old('no_of_print'),
                 ['class' => 'form-control','id' => 'line_item-no_of_print-######','maxlength'=>'20'
                    ]) !!}
            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('line_items[serial_no][######]', old('serial_no'),
                 ['class' => 'form-control','id' => 'line_item-serial_no-######','maxlength'=>'255'
                    ]) !!}
            </div>
        </td>
    </tr>

    <tr id="price_disc-######">
        <td >
            <b>Unit Price</b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[unit_price][######]', old('unit_price'),
                 ['class' => 'form-control line_unit_price','id' => 'line_item-unit_price-######','maxlength'=>'20',
                    'onkeydown' => 'FormControls.CalculateTotal(######);',
                    'onkeyup' => 'FormControls.CalculateTotal(######);',
                    'onblur' => 'FormControls.CalculateTotal(######);'
                 ]) !!}
            </div>
        </td>
        <td>
            <b>Dicount</b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[discount_val][######]',  old('discount_val'),
                 ['id' => 'line_item-discount_val-######','class' => 'form-control',
                    'onkeydown' => 'FormControls.CalculateTotal(######);',
                    'onkeyup' => 'FormControls.CalculateTotal(######);',
                    'onblur' => 'FormControls.CalculateTotal(######);'

                    ]) !!}
            </div>
        </td>

        <td>
            <b>Discount Type</b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::select('line_items[disc_type][######]', array() + Config::get('marketing.discount_array'), old('disc_type'),
                    ['class' => 'form-control', 'id'=> 'line_item-disc_type-######',
                       'onchange' => 'FormControls.CalculateTotal(######);'
                       ]) !!}
            </div>
        </td>

    </tr>



    <tr id="tax_disc-######">
        <td >
            <b>Tax % </b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::select('line_items[tax_percent][######]', array() + Config::get('marketing.tax_array'), old('tax_percent'),
                 ['id' => 'line_item-tax_percent-######','class' => 'form-control',
                    'onchange' => 'FormControls.CalculateTotal(######);'
                    ]) !!}
            </div>
        </td>


        <td >
            <b>Tax Amount </b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[tax_amount][######]', old('tax_amount'),
                 ['id' => 'line_item-tax_amount-######','class' => 'form-control line_tax_amount', 'readonly' => 'true'

                    ]) !!}
            </div>
        </td>


        <td>
            <b> Discount Amount </b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[disc_amount][######]',  old('disc_amount'),
                 ['id' => 'line_item-disc_amount-######','class' => 'form-control line_disc_amount', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>


    </tr>

    <tr id="price_tax-######">
        <td>
            <b> Sale Price </b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[sale_price][######]',  old('sale_price'),
                 ['id' => 'line_item-sale_price-######','class' => 'form-control', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>


        <td>
            <b>Total</b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[total_price][######]',  old('total_price'),
                 ['id' => 'line_item-total_price-######','class' => 'form-control line_item_total_price', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>


        <td>
            <b>Line Total</b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[line_total][######]',  old('line_total'),
                 ['id' => 'line_item-line_total-######','class' => 'form-control line_gross_amount', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>


    </tr>

    <tr id="desc_item-######">
        <td >
            <b>Misc</b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[misc_amount][######]', old('misc_amount'),
                ['class' => 'form-control dr_misc_amount','id' => 'line_item-misc_amount-######','maxlength'=>'20', 'min'=> '0',
                    'onkeydown' => 'FormControls.CalculateTotal(######);',
                    'onkeyup' => 'FormControls.CalculateTotal(######);',
                    'onblur' => 'FormControls.CalculateTotal(######);'
                ]) !!}
            </div>
        </td>

        <td>
            <b>Net Income </b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[net_income][######]', old('net_income'),
                ['class' => 'form-control line_net_income','id' => 'line_item-net_income-######','maxlength'=>'20',
                 'readonly' => 'true'

                ]) !!}
            </div>
        </td>
        <td>
            <b>Remove</b>
        </td>
        <td><button id="line_item-del_btn-######" onclick="FormControls.destroyLineItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>

    </tbody>
</table>




<table style="display: none;">
    <tbody id="others-container" >
    <tr id="item_header-######" style="
    background: powderblue;">
        <td> Other Item </td>
        <td colspan="5"></td>
    </tr>

    <tr id="line_item-######">

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('line_items[type][######]', 'acc',
                  ['class' => 'form-control','id' => 'line_item-type-######',
                      'readonly'=> 'true']) !!}
            </div>
        </td>

        <td >
            <div class="form-group" style="margin-bottom: 0px !important;">

                <div class="form-group" style="margin-bottom: 0px !important;">

                    <select id="line_item-product_id-######" name="line_items[product_id][######]" class="form-control" style="width:100%;"
                            onkeydown = 'FormControls.CalculateTotal();'
                            onchange = 'FormControls.CalculateTotal();'
                            onkeyup = 'FormControls. ();'
                            onblur = 'FormControls.CalculateTotal();'>
                </div>

            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[product_qty][######]', 0 ,
                 ['class' => 'form-control','id' => 'line_item-product_qty-######',
                    'maxlength'=>'10', 'min'=> '1' ]) !!}
            </div>
        </td>


        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('line_items[print_name][######]', 'N/A',
                 ['class' => 'form-control','id' => 'line_item-print_name-######','maxlength'=>'20', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[no_of_print][######]', 'N/A',
                 ['class' => 'form-control','id' => 'line_item-no_of_print-######','maxlength'=>'20'
                    ]) !!}
            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('line_items[serial_no][######]', old('serial_no'),
                 ['class' => 'form-control','id' => 'line_item-serial_no-######','maxlength'=>'20'
                    ]) !!}
            </div>
        </td>
    </tr>

    <tr id="price_disc-######">
        <td >
            <b>Unit Price</b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[unit_price][######]', '0',
                 ['class' => 'form-control line_unit_price','id' => 'line_item-unit_price-######','maxlength'=>'20' , 'readonly' => 'true']) !!}
            </div>
        </td>
        <td>
            <b>Dicount</b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[discount_val][######]',  0,
                 ['id' => 'line_item-discount_val-######','class' => 'form-control' , 'readonly' => 'true'


                    ]) !!}
            </div>
        </td>

        <td>
            <b>Discount Type</b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::select('line_items[disc_type][######]', array() + Config::get('marketing.discount_array'), old('disc_type'),
                    ['class' => 'form-control', 'id'=> 'line_item-disc_type-######',  'readonly' => 'true'

                       ]) !!}
            </div>
        </td>

    </tr>




    <tr id="tax_disc-######">
        <td >
            <b>Tax % </b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::select('line_items[tax_percent][######]', array() + Config::get('marketing.tax_array'), old('tax_percent'),
                 ['id' => 'line_item-tax_percent-######','class' => 'form-control', 'readonly' => 'true'

                    ]) !!}
            </div>
        </td>


        <td >
            <b>Tax Amount </b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[tax_amount][######]', 0 ,
                 ['id' => 'line_item-tax_amount-######','class' => 'form-control line_tax_amount', 'readonly' => 'true'

                    ]) !!}
            </div>
        </td>


        <td>
            <b> Discount Amount </b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[disc_amount][######]',  0 ,
                 ['id' => 'line_item-disc_amount-######','class' => 'form-control line_disc_amount', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>


    </tr>

    <tr id="price_tax-######">
        <td>
            <b> Sale Price </b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[sale_price][######]',  0,
                 ['id' => 'line_item-sale_price-######','class' => 'form-control', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>


        <td>
            <b>Total</b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[total_price][######]',  0,
                 ['id' => 'line_item-total_price-######','class' => 'form-control line_item_total_price', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>


        <td>
            <b>Line Total</b>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[line_total][######]',  0 ,
                 ['id' => 'line_item-line_total-######','class' => 'form-control line_gross_amount', 'readonly' => 'true'
                    ]) !!}
            </div>
        </td>


    </tr>

    <tr id="desc_item-######">
        <td >
            <b>Misc</b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[misc_amount][######]', 0,
                ['class' => 'form-control dr_misc_amount','id' => 'line_item-misc_amount-######', 'readonly' => 'true'

                ]) !!}
            </div>
        </td>

        <td>
            <b>Net Income </b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[net_income][######]', 0,
                ['class' => 'form-control line_net_income','id' => 'line_item-net_income-######','maxlength'=>'20',
                 'readonly' => 'true'

                ]) !!}
            </div>
        </td>
        <td>
            <b>Remove</b>
        </td>
        <td><button id="line_item-del_btn-######" onclick="FormControls.destroyLineItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>

    </tbody>
</table>


<table style="display: none;">
    <tbody id="service_item-container" >
    <tr id="service_item-######">
        <td colspan="3">
            <div class="form-group" style="margin-bottom: 0px !important;">
                <div class="form-group" style="margin-bottom: 0px !important;">
                    {!! Form::text('service_items[service][######]', old('service'), ['class' => 'form-control','id' => 'service_items-service-######', 'placeholder' => 'Service detail','maxlength'=>'100']) !!}

                </div>
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('service_items[sale_price][######]', old('sale_price'),
                 ['class' => 'form-control','id' => 'service_item-sale_price-######', 'readonly' => 'true'

                    ]) !!}
            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('service_items[tax_amount][######]', old('tax_amount'),
                 ['class' => 'form-control line_item_tax','id' => 'service_item-tax_amount-######', 'readonly' => 'true'
                 ]) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::select('service_items[tax_percent][######]', array() + Config::get('marketing.tax_array'), old('tax_percent'),
                 ['id' => 'service_item-tax_percent-######','class' => 'form-control',
                    'onchange' => 'FormControls.CalculateServiceTotal(######);'

                    ]) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('service_items[list_price][######]', old('list_price'),
                ['class' => 'form-control','id' => 'service_item-list_price-######','maxlength'=>'20', 'min'=> '0',
                'onkeydown' => 'FormControls.CalculateServiceTotal(######);',
                    'onkeyup' => 'FormControls.CalculateServiceTotal(######);',
                    'onblur' => 'FormControls.CalculateServiceTotal(######);'
                ]) !!}
            </div>
        </td>


        <td><button id="service_item-del_btn-######" onclick="FormControls.destroyServiceItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    <tr id="service_total_item-######">


        <td >
            <b>Discount</b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('service_items[discount][######]', old('discount'),
                ['class' => 'form-control dr_discount','id' => 'service_item-discount-######','maxlength'=>'20', 'min'=> '0',
                    'onkeydown' => 'FormControls.CalculateServiceTotal(######);',
                    'onkeyup' => 'FormControls.CalculateServiceTotal(######);',
                    'onblur' => 'FormControls.CalculateServiceTotal(######);'
                ]) !!}
            </div>
        </td>

        <td>
            <b>Misc. Amount</b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('service_items[misc_amount][######]', old('tax_amount'),
                 ['class' => 'form-control line_misc_amount','id' => 'service_item-misc_amount-######',
                  'maxlength'=>'20','min'=> '0',
                  'onkeydown' => 'FormControls.CalculateServiceTotal(######);',
                    'onkeyup' => 'FormControls.CalculateServiceTotal(######);',
                    'onblur' => 'FormControls.CalculateServiceTotal(######);'
                 ]) !!}
            </div>
        </td>

        <td>
            <b>Total </b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('service_items[total_amount][######]', old('total_amount'),
                ['class' => 'form-control line_item_total','id' => 'service_item-total_amount-######','maxlength'=>'20',
                 'readonly' => 'true'

                ]) !!}
            </div>
        </td>

        <td>
            <b>Net Income </b>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('service_items[net_income][######]', old('net_income'),
                ['class' => 'form-control line_net_income','id' => 'service_item-net_income-######','maxlength'=>'20',
                 'readonly' => 'true'

                ]) !!}
            </div>
        </td>


    </tr>
    </tr>
    </tbody>
</table>



<table style="display: none;">
    <tbody id="tender_form-container" >
    <tr id="tender_line-######">
        <td >
            <div class="form-group" style="margin-bottom: 0px !important;">
                <div class="form-group" style="margin-bottom: 0px !important;">
                    {!! Form::text('tenders[supplier][######]', old('service'), ['class' => 'form-control','id' => 'tenders-service-######', 'maxlength'=>'100','placeholder' => 'Supplier']) !!}

                </div>
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('tenders[brand][######]', old('sale_price'),
                 ['class' => 'form-control','id' => 'tender-brand-######','maxlength'=>'100'

                    ]) !!}
            </div>
        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::text('tenders[model][######]', old('model'),
                 ['class' => 'form-control line_item_tax','id' => 'tender-model-######' ,'maxlength'=>'100'
                 ]) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('tenders[ppm][######]', old('ppm'),
                 ['class' => 'form-control line_item_tax','id' => 'tender-ppm-######' ,'maxlength'=>'100', 'min'=> '1'
                 ]) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('tenders[price][######]', old('price'),
                ['class' => 'form-control','id' => 'tender-price-######' ,'maxlength'=>'100' ,'min'=> '1'

                ]) !!}
            </div>
        </td>


        <td><button id="tender-del_btn-######" onclick="FormControls.destroyTenderItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>

    </tr>
    </tbody>
</table>