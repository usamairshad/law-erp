<div id="11" class="panel-collapse collapse in col-md-12">

    {{ csrf_field() }}

    <div class="form-group col-md-12 @if($errors->has('invoice_to')) has-error @endif" id="div_invoice_to">

        {!! Form::label('invoice_to', 'Invoice To', ['class' => 'control-label']) !!}
        {!! Form::radio('invoice_to', 'customer', true , ['onclick' => 'FormControls.invoice_to("customer")', 'id' => 'customer'] ) !!} Customer
        {!! Form::radio('invoice_to', 'branch',null, ['onclick' => 'FormControls.invoice_to("branch")',  'id' => 'branch'] ) !!} Branch
        {!! Form::radio('invoice_to', 'dealer',null, ['onclick' => 'FormControls.invoice_to("dealer")',  'id' => 'dealer'] ) !!} Dealer
        @if($errors->has('invoice_to'))
            <span class="help-block">
                {{ $errors->first('invoice_to') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('sales_person')) has-error @endif" id="sales_person_div">
        {!! Form::label('sales_person', 'Sales Person ', ['class' => 'control-label']) !!}
        {!! Form::select('sales_person', $Employees , old('sales_person'),
        ['class' => 'form-control select2'  ,'id' => 'sales_person']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('invoice_dept')) has-error @endif" id="invoice_dept_div">
        {!! Form::label('invoice_dept', 'Invoice Dept ', ['class' => 'control-label']) !!}
        {!! Form::select('invoice_dept', Config::get('admin.invoice_depts') , old('invoice_dept'),
        ['class' => 'form-control select2'  ,'id' => 'invoice_dept']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('customer_id')) has-error @endif" id="customer_id_div">
        {!! Form::label('customer_id', 'Customer ', ['class' => 'control-label']) !!}
        {!! Form::select('customer_id', $Customers , old('customer_id'),
        ['class' => 'form-control select2'  ,'id' => 'customer_id']) !!}
    </div>


    <div class="form-group col-md-3 @if($errors->has('branch_id')) has-error @endif" id="branch_id_div">
        {!! Form::label('branch_id', 'Branch ', ['class' => 'control-label']) !!}
        {!! Form::select('branch_id', $Branches , old('branch_id'),
        ['class' => 'form-control select2'  ,'id' => 'branch_id']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('dealer_id')) has-error @endif" id="dealer_id_div">
        {!! Form::label('dealer_id', 'Dealer ', ['class' => 'control-label']) !!}
        {!! Form::select('dealer_id', $Dealers , old('dealer_id'),
        ['class' => 'form-control select2'  ,'id' => 'dealer_id']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('invoice_date')) has-error @endif">
        {!! Form::label('invoice_date', 'Invoice Date', ['class' => 'control-label']) !!}
        {!! Form::text('invoice_date', old('invoice_date'), ['class' => 'form-control datepicker','id'=> 'invoice_date']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('due_date')) has-error @endif">
        {!! Form::label('due_date', 'Payment Due Date *', ['class' => 'control-label']) !!}
        {!! Form::text('due_date', old('due_date'), ['class' => 'form-control datepicker','id'=> 'due_date', 'required']) !!}
        @if($errors->has('due_date'))
            <span class="help-block">
                {{ $errors->first('due_date') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('rr_no')) has-error @endif">
        {!! Form::label('rr_no', 'R.R No *', ['class' => 'control-label']) !!}
        {!! Form::text('rr_no', old('rr_no'), ['class' => 'form-control','id'=> 'rr_no','maxlength'=>'10', 'required']) !!}
        @if($errors->has('rr_no'))
            <span class="help-block">
                {{ $errors->first('rr_no') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('delivery_type')) has-error @endif" id="delivery_type_div">
        {!! Form::label('delivery_type', 'Delivery Type *', ['class' => 'control-label']) !!}
        {!! Form::select('delivery_type', array('' => 'Select Delivery Type') + Config::get('hrm.delivery_type_array') , old('delivery_type'),
         ['class' => 'form-control select2'  ,'id' => 'delivery_type', 'required']) !!}
        @if($errors->has('delivery_type'))
            <span class="help-block">
                {{ $errors->first('delivery_type') }}
            </span>
        @endif
    </div>


    <div class="form-group col-md-3 @if($errors->has('installation_date')) has-error @endif">
        {!! Form::label('installation_date', 'Installation Date', ['class' => 'control-label']) !!}
        {!! Form::text('installation_date', old('installation_date'), ['class' => 'form-control datepicker','id'=> 'installation_date'
        , 'onchange' => 'FormControls.setWarrenty();']) !!}
        @if($errors->has('installation_date'))
            <span class="help-block">
                {{ $errors->first('installation_date') }}
            </span>
        @endif
    </div>


    <div class="form-group col-md-3 @if($errors->has('warrenty')) has-error @endif">
        {!! Form::label('warrenty', 'Warrenty Months', ['class' => 'control-label']) !!}
        {!! Form::text('warrenty', old('warrenty'), ['class' => 'form-control','id'=> 'warrenty' , 'onchange' => 'FormControls.setWarrenty();']) !!}
        @if($errors->has('warrenty'))
            <span class="help-block">
                {{ $errors->first('warrenty') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('expires_at')) has-error @endif">
        {!! Form::label('expires_at', 'Expires At', ['class' => 'control-label']) !!}
        {!! Form::text('expires_at', old('expires_at'), ['class' => 'form-control datepicker','id'=> 'expires_at']) !!}
        @if($errors->has('expires_at'))
            <span class="help-block">
                {{ $errors->first('expires_at') }}
            </span>
        @endif
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="about_featured">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default wow fadInLeft">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#11">
                                <span class="fa fa-check-square-o"></span><b>Products </b>
                            </a>
                        </h4>
                    </div>

                    <div id="11" class="panel-collapse collapse in">
                        <div class="panel-body pad table-responsive">
                            <button onclick="FormControls.createOtherLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add Other <u>R</u>ow</button>
                            <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add Product <u>R</u>ow</button>
                            <table class="table table-bordered table-striped datatable" id="users-table">
                                <thead>
                                <tr>

                                    <th>Type</th>
                                    <th>Product</th>
                                    <th>QTY</th>
                                    <th>Printer Name</th>
                                    <th>Counter</th>
                                    <th>Serial No's</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php  $product_counter = 0 ?>
                                @if(isset($InvoiceDetailsModel) && count($InvoiceDetailsModel) )

                                    @foreach ($InvoiceDetailsModel as $key => $val)

                                        <?php   $product_counter++ ?>
                                        @if($InvoiceDetailsModel[$key]->type == 'product')

                                            <tr id="item_header-{{ $product_counter }}" style="background: powderblue;">
                                                <td> Line Item </td>
                                                <td colspan="5"></td>
                                            </tr>
                                            <tr id="line_item-{{ $product_counter }}">

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::text('line_items[type]['. $product_counter .']', 'product',
                                                          ['class' => 'form-control','id' => 'line_item-type-'. $product_counter .'',
                                                              'readonly'=> 'true']) !!}
                                                    </div>
                                                </td>

                                                <td >
                                                    <div class="form-group" style="margin-bottom: 0px !important;">

                                                        <div class="form-group" style="margin-bottom: 0px !important;">


                                                            {!! Form::select('line_items[product_id]['. $product_counter .']', $Products, $val->product_id,
                                                                                ['class' => 'form-control', 'id'=> 'line_item-product_id-'. $product_counter .''
                                                           ]) !!}

                                                        </div>

                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::text('line_items[product_qty]['. $product_counter .']', $val->quantity,
                                                         ['class' => 'form-control','id' => 'line_item-product_qty-'. $product_counter .'',
                                                            'maxlength'=>'10', 'min'=> '1',
                                                            'onkeydown' => 'FormControls.CalculateTotal('. $product_counter .');',
                                                            'onkeyup' => 'FormControls.CalculateTotal('. $product_counter .');',
                                                            'onblur' => 'FormControls.CalculateTotal('. $product_counter .');'
                                                            ]) !!}
                                                    </div>
                                                </td>


                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::text('line_items[print_name]['. $product_counter .']', $val->print_name,
                                                         ['class' => 'form-control','id' => 'line_item-print_name-'. $product_counter .'','maxlength'=>'20'
                                                            ]) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[no_of_print]['. $product_counter .']', $val->total_count,
                                                         ['class' => 'form-control','id' => 'line_item-no_of_print-'. $product_counter .'','maxlength'=>'20'
                                                            ]) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::text('line_items[serial_no]['. $product_counter .']', $val->serialNo,
                                                         ['class' => 'form-control','id' => 'line_item-serial_no-'. $product_counter .'','maxlength'=>'255'
                                                            ]) !!}
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr id="price_disc-{{ $product_counter }}">
                                                <td >
                                                    <b>Unit Price</b>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[unit_price]['. $product_counter .']', $val->unit_price,
                                                         ['class' => 'form-control line_unit_price','id' => 'line_item-unit_price-'. $product_counter .'','maxlength'=>'20',
                                                            'onkeydown' => 'FormControls.CalculateTotal('. $product_counter .');',
                                                            'onkeyup' => 'FormControls.CalculateTotal('. $product_counter .');',
                                                            'onblur' => 'FormControls.CalculateTotal('. $product_counter .');'
                                                         ]) !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <b>Dicount</b>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[discount_val]['. $product_counter .']',  $val->discount,
                                                         ['id' => 'line_item-discount_val-'. $product_counter .'','class' => 'form-control',
                                                            'onkeydown' => 'FormControls.CalculateTotal('. $product_counter .');',
                                                            'onkeyup' => 'FormControls.CalculateTotal('. $product_counter .');',
                                                            'onblur' => 'FormControls.CalculateTotal('. $product_counter .');'

                                                            ]) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <b>Discount Type</b>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::select('line_items[disc_type]['. $product_counter .']', array() + Config::get('marketing.discount_array'), $val->discount_type,
                                                            ['class' => 'form-control', 'id'=> 'line_item-disc_type-'. $product_counter .'',
                                                               'onchange' => 'FormControls.CalculateTotal('. $product_counter .');'
                                                               ]) !!}
                                                    </div>
                                                </td>

                                            </tr>



                                            <tr id="tax_disc-{{ $product_counter }}">
                                                <td >
                                                    <b>Tax % </b>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::select('line_items[tax_percent]['. $product_counter .']', array() + Config::get('marketing.tax_array'), $val->tax_percent,
                                                         ['id' => 'line_item-tax_percent-'. $product_counter .'','class' => 'form-control',
                                                            'onchange' => 'FormControls.CalculateTotal('. $product_counter .');'
                                                            ]) !!}
                                                    </div>
                                                </td>


                                                <td >
                                                    <b>Tax Amount </b>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[tax_amount]['. $product_counter .']', $val->tax_amount,
                                                         ['id' => 'line_item-tax_amount-'. $product_counter .'','class' => 'form-control line_tax_amount', 'readonly' => 'true'

                                                            ]) !!}
                                                    </div>
                                                </td>


                                                <td>
                                                    <b> Discount Amount </b>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[disc_amount]['. $product_counter .']',  $val->discount_amount ,
                                                         ['id' => 'line_item-disc_amount-'. $product_counter .'','class' => 'form-control line_disc_amount', 'readonly' => 'true'
                                                            ]) !!}
                                                    </div>
                                                </td>


                                            </tr>

                                            <tr id="price_tax-{{ $product_counter }}">
                                                <td>
                                                    <b> Sale Price </b>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[sale_price]['. $product_counter .']',  $val->sale_price,
                                                         ['id' => 'line_item-sale_price-'. $product_counter .'','class' => 'form-control', 'readonly' => 'true'
                                                            ]) !!}
                                                    </div>
                                                </td>


                                                <td>
                                                    <b>Total</b>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[total_price]['. $product_counter .']',  $val->total_price,
                                                         ['id' => 'line_item-total_price-'. $product_counter .'','class' => 'form-control line_item_total_price', 'readonly' => 'true'
                                                            ]) !!}
                                                    </div>
                                                </td>


                                                <td>
                                                    <b>Line Total</b>
                                                </td>

                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[line_total]['. $product_counter .']',  $val->line_total,
                                                         ['id' => 'line_item-line_total-'. $product_counter .'','class' => 'form-control line_gross_amount', 'readonly' => 'true'
                                                            ]) !!}
                                                    </div>
                                                </td>


                                            </tr>

                                            <tr id="desc_item-{{ $product_counter }}">
                                                <td >
                                                    <b>Misc</b>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[misc_amount]['. $product_counter .']', $val->misc_amount,
                                                        ['class' => 'form-control dr_misc_amount','id' => 'line_item-misc_amount-'. $product_counter .'','maxlength'=>'20', 'min'=> '0',
                                                            'onkeydown' => 'FormControls.CalculateTotal('. $product_counter .');',
                                                            'onkeyup' => 'FormControls.CalculateTotal('. $product_counter .');',
                                                            'onblur' => 'FormControls.CalculateTotal('. $product_counter .');'
                                                        ]) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <b>Net Income </b>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[net_income]['. $product_counter .']', $val->net_income,
                                                        ['class' => 'form-control line_net_income','id' => 'line_item-net_income-'. $product_counter .'','maxlength'=>'20',
                                                         'readonly' => 'true'

                                                        ]) !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <b>Remove</b>
                                                </td>
                                                <td><button id="line_item-del_btn-{{ $product_counter }}" onclick="FormControls.destroyLineItem(''. $product_counter .'');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
                                            </tr>

                                        @else

                                            <tr id="line_item-{{$product_counter}}">

                                                <td>
                                                    <div class="form-group  @if($errors->has('tax_percent')) has-error @endif">

                                                        {!! Form::text('line_items[type]['.$product_counter.']', $LineItems[$key]->type,
                                                        ['class' => 'form-control','id' => 'line_item-type-'.$product_counter.'' , 'readonly' => 'true' ]) !!}

                                                    </div>
                                                </td>


                                                <td> <div class="form-group ">

                                                        {!! Form::select('line_items[product_id]['.$product_counter.']', $Products, $LineItems[$key]->product_id, ['id' => 'line_item-product_id-'.$product_counter.'','class' => 'form-control select2',
                                                            'onchange' => 'FormControls.RefreshLineItem();',
                                                            ])
                                                        !!}
                                                    </div>
                                                </td>

                                                <td><div class="form-group  @if($errors->has('product_qty')) has-error @endif">

                                                        {!! Form::number('line_items[product_qty]['.$product_counter.']', $LineItems[$key]->product_qty,
                                                        ['class' => 'form-control','id' => 'line_item-product_qty-'.$product_counter.'', 'maxlength'=>'10', 'min'=> '1']) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="form-group  @if($errors->has('sale_price')) has-error @endif">

                                                        {!! Form::number('line_items[sale_price]['.$product_counter.']', $LineItems[$key]->sale_price,
                                                        ['class' => 'form-control','id' => 'line_item-sale_price-'.$product_counter.'', 'readonly' => 'true'

                                                        ]) !!}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[tax_amount]['.$product_counter.']', $LineItems[$key]->tax_amount,
                                                        ['class' => 'form-control line_item_tax','id' => 'line_item-tax_amount-'.$product_counter.'', 'readonly' => 'true'
                                                        ]) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="form-group  @if($errors->has('tax_percent')) has-error @endif">

                                                        {!! Form::text('line_items[tax_percent]['.$product_counter.']', $LineItems[$key]->tax_percent,
                                                        ['id' => 'line_item-tax_percent-'.$product_counter.'','class' => 'form-control','readonly' => 'true' ]) !!}
                                                    </div>
                                                </td>

                                                <td colspan="2">
                                                    <div class="form-group  @if($errors->has('list_price')) has-error @endif">

                                                        {!! Form::text('line_items[list_price]['.$product_counter.']', $LineItems[$key]->list_price,
                                                        ['class' => 'form-control','id' => 'line_item-list_price-'.$product_counter.'', 'readonly' => 'true']) !!}
                                                    </div>
                                                </td>


                                                <td><button id="line_item-del_btn-1" onclick="FormControls.destroyLineItem('{{$product_counter}}');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>

                                            </tr>

                                            <tr id="desc_item-{{$product_counter}}">

                                                <td >
                                                    <b>Discount</b>
                                                </td>

                                                <td>
                                                    <div class="form-group  @if($errors->has('discount')) has-error @endif">

                                                        {!! Form::number('line_items[discount]['.$product_counter.']', $LineItems[$key]->discount,
                                                        ['class' => 'form-control dr_discount','id' => 'line_item-discount-'.$product_counter.'']) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <b>Misc Amount</b>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[misc_amount]['.$product_counter.']', $LineItems[$key]->misc_amount,
                                                        ['class' => 'form-control line_misc_amount','id' => 'line_item-misc_amount-'.$product_counter.'', 'readonly' => 'true',]) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <b>Total </b>
                                                </td>
                                                <td>
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[total_amount]['.$product_counter.']', $LineItems[$key]->total_amount,
                                                        ['class' => 'form-control line_item_total','id' => 'line_item-total_amount-'.$product_counter.'',
                                                        'readonly' => 'true'

                                                        ]) !!}
                                                    </div>
                                                </td>

                                                <td>
                                                    <b>Net Income </b>
                                                </td>
                                                <td colspan="2">
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        {!! Form::number('line_items[net_income]['.$product_counter.']', $LineItems[$key]->net_income,
                                                        ['class' => 'form-control line_net_income','id' => 'line_item-net_income-'.$product_counter.'',
                                                        'readonly' => 'true'

                                                        ]) !!}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                                <input type="hidden" id="line_item-global_counter" value="<?php  echo ++$product_counter ?>"   />

                                </tbody>

                            </table>
                        </div>
                    </div>



                    <div class="form-group col-md-3 @if($errors->has('misc_amount')) has-error @endif">
                        {!! Form::label('misc_amount', 'Misc Amount ', ['class' => 'control-label']) !!}
                        {!! Form::number('misc_amount', old('misc_amount'),
                        ['class' => 'form-control misc_amount','id' => 'misc_amount', 'readonly' => 'true']) !!}
                    </div>


                    <div class="form-group col-md-3 @if($errors->has('total_discount')) has-error @endif">
                        {!! Form::label('total_discount', 'Total Discount ', ['class' => 'control-label']) !!}
                        {!! Form::number('total_discount', old('total_discount'),
                        ['class' => 'form-control total_discount','id' => 'total_discount', 'readonly' => 'true']) !!}
                    </div>

                    <div class="form-group col-md-3 @if($errors->has('gross_total')) has-error @endif">
                        {!! Form::label('gross_total', 'Gross total', ['class' => 'control-label']) !!}
                        {!! Form::number('gross_total', old('gross_total'),
                        ['class' => 'form-control gross_total','id' => 'gross_total', 'readonly' => 'true']) !!}
                    </div>


                    <div class="form-group col-md-3  @if($errors->has('is_service')) has-error @endif" style='margin-top : 33px'>
                        {!! Form::label('is_service', 'Apply Service Charges ? ', ['class' => 'control-label' ]) !!}
                        {{ Form::checkbox('is_service', 1, old('is_service') ,[ 'onchange' => 'FormControls.CalculateGrandTotal();',  ] ) }}
                        @if($errors->has('is_service'))
                            <span class="help-block">
                                {{ $errors->first('is_service') }}
                            </span>
                        @endif
                    </div>


                    <div class="form-group col-md-3  @if($errors->has('service_amount')) has-error @endif" >
                        {!! Form::label('service_amount', 'Service Amount', ['class' => 'control-label']) !!}
                        {!! Form::text( 'service_amount', old('service_amount'), ['class' => 'form-control',

                          'onkeydown' => 'FormControls.CalculateGrandTotal();',
                         'onkeyup' => 'FormControls.CalculateGrandTotal();',
                         'onblur' => 'FormControls.CalculateGrandTotal();' ] ) !!}

                        @if($errors->has('service_amount'))
                            <span class="help-block">
                                {{ $errors->first('service_amount') }}
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-md-3  @if($errors->has('service_tax')) has-error @endif">
                        {!! Form::label('service_tax', 'Serives Tax % ', ['class' => 'control-label']) !!}
                        {!! Form::select('service_tax', array() + Config::get('marketing.tax_array'), old('service_tax'),
                          ['id' => 'service_tax','class' => 'form-control',
                             'onchange' => 'FormControls.CalculateGrandTotal();'

                             ]) !!}
                        @if($errors->has('service_tax'))
                            <span class="help-block">
                                {{ $errors->first('service_tax') }}
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-md-3  @if($errors->has('service_tax_amount')) has-error @endif" >
                        {!! Form::label('service_tax_amount', 'Service Tax Amount', ['class' => 'control-label']) !!}
                        {!! Form::text('service_tax_amount', old('service_tax_amount'), ['id'=> 'service_tax_amount','class' => 'form-control' , 'readonly' => 'true']) !!}
                        @if($errors->has('service_tax_amount'))
                            <span class="help-block">
                                {{ $errors->first('tax_amount') }}
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-md-3  @if($errors->has('total_after_service_tax')) has-error @endif" >
                        {!! Form::label('total_after_service_tax', 'Total After Service Charges', ['class' => 'control-label']) !!}
                        {!! Form::text('total_after_service_tax', old('total_after_service_tax'), ['id'=> 'total_after_service_tax','class' => 'form-control' , 'readonly' => 'true']) !!}
                        @if($errors->has('total_after_service_tax'))
                            <span class="help-block">
                                {{ $errors->first('total_after_service_tax') }}
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-md-3 @if($errors->has('overall_discount')) has-error @endif">
                        {!! Form::label('overall_discount', 'Overall Discount ', ['class' => 'control-label']) !!}
                        {!! Form::number('overall_discount', old('overall_discount'),
                        ['class' => 'form-control overall_discount','id' => 'overall_discount',
                         'onchange' => 'FormControls.CalculateGrandTotal();',
                            'onkeydown' => 'FormControls.CalculateGrandTotal();',
                            'onkeyup' => 'FormControls.CalculateGrandTotal();',
                            'onblur' => 'FormControls.CalculateGrandTotal();'
                          ]) !!}
                    </div>

                    <div class="form-group col-md-3 @if($errors->has('total_tax')) has-error @endif">
                        {!! Form::label('total_tax', 'Total Tax', ['class' => 'control-label']) !!}
                        {!! Form::number('total_tax', old('total_tax'),
                        ['class' => 'form-control total_tax','id' => 'total_tax', 'readonly' => 'true']) !!}
                    </div>




                    <div class="form-group col-md-3 @if($errors->has('net_income')) has-error @endif">
                        {!! Form::label('net_income', 'Net Income', ['class' => 'control-label']) !!}
                        {!! Form::number('net_income', old('net_income'),
                        ['class' => 'form-control net_income','id' => 'net_income', 'readonly' => 'true']) !!}
                    </div>

                    <div class="form-group col-md-3 @if($errors->has('payable_amount')) has-error @endif">
                        {!! Form::label('payable_amount', 'Total Payable ', ['class' => 'control-label']) !!}
                        {!! Form::number('payable_amount', old('payable_amount'),
                        ['class' => 'form-control payable_amount','id' => 'total_payable', 'readonly' => 'true']) !!}
                    </div>

                </div>

            </div>
        </div>
    </div>

</div>