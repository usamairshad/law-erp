@extends('layouts.app')

@section('stylesheet')

    <style>
        .example-template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sales Invoice</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"> Edit Sales Invoice </h3>
            <a href="{{ route('admin.salesinvoice.index') }}" class="btn btn-success pull-right"> Back </a>
        </div>

        {!! Form::model($SalesInvoiceDetailModel , ['method' => 'POST', 'id' => 'validation-form','route' => ['admin.salesinvoice.update_invoice', $SalesInvoiceDetailModel->id]]) !!}

        <div class="box-body">
            @include('admin.invoices.fields')

            <div class="panel-body pad table-responsive">
                <table class="table table-bordered table-striped" id="gross-table">

                    <tbody id="gross-table-body">

                    </tbody>
                </table>
            </div>
            <div id="grosstotal"> </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}

        @include('admin.invoices.items_template')

    </div>
@stop

@section('javascript')
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/js/admin/invoices/create_modify.js') }}" type="text/javascript"></script>
@endsection