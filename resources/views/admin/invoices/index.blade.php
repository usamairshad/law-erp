@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale Invoice List</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('invoice_create'))
                <a href="{{ route('admin.salesinvoice.create') }}" class="btn btn-success pull-right">Add New Sale Invoice</a>
            @endif
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($SalesInvoiceDetailModel) > 0 ? 'datatable' : '' }}" id="order-table">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Invoice No</th>
                    <th>Invoice Date</th>
                    <th>Customer  Name</th>
                    <th>PO No.</th>
                    <th>Due Date</th>
                    <th>R.R No</th>
                    <th>Total Amount</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            $('#order-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.salesinvoice.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'invoice_no', name: 'invoice_no' },
                    { data: 'invoice_date', name: 'invoice_date' },
                    { data: 'customer_name', name: 'customer_name' },
                    { data: 'po_no', name: 'po_no' },
                    { data: 'due_date', name: 'due_date' },
                    { data: 'rr_no', name: 'rr_no' },
                    { data: 'payable_amount', name: 'payable_amount' },
                    { data: 'confirm_status', name: 'confirm_status' },
                    { data: 'created_by', name: 'created_by' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},
                ]
            });
        });
    </script>
@endsection


