@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Generate Sale Invoice</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Sales Invoice Detail</h3>
            @if(Gate::check('invoice_manage'))
                <a href="{{ route('admin.salesinvoice.index') }}" class="btn btn-success pull-right">back</a>
            @endif
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.salesinvoice.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">

            @include('admin.invoices.fields')

            <div class="panel-body pad table-responsive">
                <table class="table table-bordered table-striped" id="gross-table">

                    <tbody id="gross-table-body">

                    </tbody>
                </table>
            </div>
            <div id="grosstotal"> </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger',  'id' => 'btn_save']) !!}
        </div>

        {!! Form::close() !!}
        @include('admin.invoices.items_template')
    </div>
@stop

@section('javascript')
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/js/admin/invoices/create_modify.js') }}" type="text/javascript"></script>
@endsection

@section('javascript')

    <script src="{{ url('public/js/admin/invoices/create_modify.js') }}" type="text/javascript"></script>

@endsection


