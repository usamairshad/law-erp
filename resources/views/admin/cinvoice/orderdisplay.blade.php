@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Order Products</h1>
    </section>
@stop
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">View</h3>
            <a href="{{ route('admin.orders.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th width="10%">Performa Number</th>
                    <td>{{ $Orders->perfomra_no }}</td>
                    <th width="10%">Sales Order Number</th>
                    <td>{{ $Orders->sale_order_no }}</td>
                    <th width="10%">Shipping Via</th>
                    <td>{{ $Orders->shipping }}</td>
                </tr>
                <tr>
                    <th>Po No</th>
                    <td>{{ $Orders->po_number }}</td>
                    <th>Purchase Date</th>
                    <td>{{ $Orders->sal_date }}</td>
                    <th>Po Date</th>
                    <td>{{ $Orders->po_date }}</td>
                </tr>
                <tr>
                    <th>On/About</th>
                    <td>{{ $Orders->valid_upto }}</td>
                    <th>Brands </th>
                    <td>{{$Orders->suppliersModel->sup_name}}</td>
                </tr>

                <tr>
                    <th>From</th>
                    <td>{{ $Orders->soruce }}</td>
                    <th>To</th>
                    <td colspan="3">{{ $Orders->destination }}</td>
                </tr>

                <tr>
                </tr>
                </tbody>
            </table>

            <!-- Slip Area Started -->

            <section class="content-header" style="padding: 10px 15px !important;">
                <h1>Product View</h1>
            </section>


            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Id</th>
                    <th>Unit Name</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                @forelse($Products as $product)
                    <tr>
                        <td>{{ $product->productsModel->products_name }}</td>
                        <td>{{ $product->unitsModel->shortcode }}</td>
                        <td>{{ $product->qty }}</td>
                        <td>{{ $product->unit_price }}</td>
                        <td>{{ $product->amount }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No Record Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
        <!-- /.box-body -->
    </div>






@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.products.mass_destroy') }}';
    </script>
@endsection

