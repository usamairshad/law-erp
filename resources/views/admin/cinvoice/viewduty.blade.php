@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty View</h1>
    </section>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ route('admin.cinvoice.index') }}" class="btn btn-success pull-right">Back</a>&nbsp;
        </div>


        <div class="panel-body pad table-responsive">
            <table class="table table-bBanked table-striped dt-select" id="users-table">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Duty Name</th>
                    <th>Amount</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @php($r = 1)
                    @foreach ($result as $results)
                        <tr data-entry-id="{{ $results->ledger_id }}">
                            <td>{{$r}}</td>
                            <td>{{$results->dutyModel->duty}}</td>
                            <td>{{$results->amount}}</td>
                            <td>
                            @if($results->status)
                                {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'PATCH',
                                        'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                        'route' => ['admin.comercialinvoice.inactive', $results->id])) !!}
                                {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                {!! Form::close() !!}
                            @elseif(!$results->status)
                                {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'PATCH',
                                        'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                        'route' => ['admin.comercialinvoice.active', $results->id])) !!}
                                {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                {!! Form::close() !!}
                            @endif
                            </td>
                        </tr>
                        @php($r++)
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('#users-table').DataTable();
    </script>
@endsection