@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Commercial Invoice List</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Commercial Invoice</h3>
            @if(Gate::check('orders_create'))
                <a href="{{ route('admin.cinvoice.create') }}" class="btn btn-success pull-right">New Commercial Invoice</a>
            @endif
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($ComercialInvoiceModel) > 0 ? 'datatable' : '' }}" id="order-table">
                <thead>
                <tr>
                    <th width="8%">S: No</th>
                    <th>Invoice#</th>
                    <th>ATP</th>
                    <th>ETD</th>
                    <th>ETA</th>
                    <th>Lc#</th>
                    <th>Dollar Rate</th>
                    <th>Amount($)</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            $('#order-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.cinvoice.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'ci_no', name: 'ci_no' },
                    { data: 'atp', name: 'atp' },
                    { data: 'etd', name: 'etd' },
                    { data: 'eta', name: 'eta' },
                    { data: 'lcno', name: 'lcno' },
                    { data: 'dollar_rate', name: 'dollar_rate' },
                    { data: 'usd_amt', name: 'usd_amt' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},

                ]
            });
        });
    </script>
@endsection


