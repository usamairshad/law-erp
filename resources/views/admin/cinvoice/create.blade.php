@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Commercial Invoice</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Commercial Invoice</h3>
            <a href="{{ route('admin.cinvoice.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.cinvoice.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">

            <div class="row">
               <div class="col-xs-12 form-group">

                    <div class="col-xs-3 form-group @if($errors->has('document_date')) has-error @endif">
                        {!! Form::label('document_date', 'Document Date*', ['class' => 'control-label']) !!}
                        {!! Form::text('document_date',old('document_date'), [ 'id' =>'document_date' , 'class' => 'form-control datepicker', 'placeholder' => '','required']) !!}
                        @if($errors->has('document_date'))
                            <span class="help-block">
                                {{ $errors->first('document_date') }}
                            </span>
                        @endif
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('total_days')) has-error @endif">
                        {!! Form::label('total_days', 'Total Days*', ['class' => 'control-label']) !!}
                        {!! Form::text('total_days', old('total_days'), ['id' =>'total_days' ,'class' => 'form-control', 'placeholder' => '','maxlegth'=>'5']) !!}
                        @if($errors->has('total_days'))
                            <span class="help-block">
                                {{ $errors->first('total_days') }}
                            </span>
                        @endif
                    </div>
                    <div class="col-xs-3 form-group @if($errors->has('lc_expiry_date')) has-error @endif">
                        {!! Form::label('lc_expiry_date', 'Lc Expiry Date*', ['class' => 'control-label']) !!}
                        {!! Form::text('lc_expiry_date', old('lc_expiry_date'), ['id' =>'lc_expiry_date' ,'class' => 'form-control dayss', 'placeholder' => '']) !!}
                        @if($errors->has('lc_expiry_date'))
                            <span class="help-block">
                                {{ $errors->first('lc_expiry_date') }}
                            </span>
                        @endif
                    </div>
                    <div class="col-xs-3 form-group @if($errors->has('today_doller')) has-error @endif">
                        {!! Form::label('today_doller', 'Confirm Dollar Rate*', ['class' => 'control-label']) !!}
                        {!! Form::text('today_doller',old('today_doller'), ['id' =>'today_doller' ,'class' => 'form-control', 'placeholder' => '','min'=>"0" ,'maxlength'=>10]) !!}
                        @if($errors->has('today_doller'))
                            <span class="help-block">
                            {{ $errors->first('today_doller') }}
                        </span>
                        @endif
                    </div>
               </div>
            </div>


         

            <div class="row">
                <div class="col-xs-12 form-group">
                   
                    <div class="col-xs-3 form-group @if($errors->has('document_received')) has-error @endif">
                        {!! Form::label('document_received', 'Document Received', ['class' => 'control-label']) !!}
                        {!!  Form::select('document_received', [
                        '' => 'select Place',
                        '1' => 'In Bank',
                        '2' => 'In Office'],
                        null,
                        ['class' => 'form-control'])
                        !!}
                        @if($errors->has('document_received'))
                            <span class="help-block">
                                {{ $errors->first('document_received') }}
                            </span>
                        @endif
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('bl_no')) has-error @endif">
                        {!! Form::label('bl_no', 'BL#', ['class' => 'control-label']) !!}
                        {!! Form::text('bl_no',old('bl_no'), ['id' =>'bl_no' , 'class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'15']) !!}
                        @if($errors->has('bl_no'))
                            <span class="help-block">
                                {{ $errors->first('bl_no') }}
                            </span>
                        @endif
                    </div>
                   
                    <div class="col-xs-3 form-group @if($errors->has('container_status')) has-error @endif">
                        {!! Form::label('container_status', 'Container Status', ['class' => 'control-label']) !!}
                        {!! Form::select('container_status', [
                        '' => 'select status',
                        '1' => 'LCL',
                        '2' => '20 Ft',
                        '3' => '40 Ft'],
                        null,
                        ['class' => 'form-control'])
                        !!}
                        @if($errors->has('container_status'))
                            <span class="help-block">
                                {{ $errors->first('container_status') }}
                            </span>
                        @endif
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('ci_no')) has-error @endif">
                        {!! Form::label('ci_no', 'Commercial Invoice*', ['class' => 'control-label']) !!}
                        <input id="ci_no" class="form-control" type="text" name="ci_no" value="" min="1" maxlength="50" required pattern= "[0-9]+"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                   
                    <div class="col-xs-4 form-group @if($errors->has('atp')) has-error @endif">
                        {!! Form::label('atp', 'ATP*', ['class' => 'control-label']) !!}
                        {!! Form::text('atp', old('atp'), ['id' => 'atp','class' => 'form-control datepicker', 'placeholder' => '','required']) !!}
                        @if($errors->has('atp'))
                            <span class="help-block">
                    {{ $errors->first('atp') }}
                    </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('etd')) has-error @endif">
                        {!! Form::label('etd', 'ETD*', ['class' => 'control-label']) !!}
                        {!! Form::text('etd', old('etd'), ['id' => 'etd','class' => 'form-control datepicker', 'placeholder' => '','required']) !!}
                        @if($errors->has('etd'))
                            <span class="help-block">
                    {{ $errors->first('etd') }}
                    </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('eta')) has-error @endif">
                        {!! Form::label('eta', 'ETA*', ['class' => 'control-label']) !!}
                        {!! Form::text('eta', old('eta'), ['id' => 'eta','class' => 'form-control datepicker', 'placeholder' => '','required']) !!}
                        @if($errors->has('eta'))
                            <span class="help-block">
                    {{ $errors->first('eta') }}
                    </span>
                        @endif
                    </div>

                    <div class="col-xs-4 form-group @if($errors->has('mos')) has-error @endif">
                            {!! Form::label('mos', 'MOS*', ['class' => 'control-label']) !!}
                            <input id="mos" class="form-control" type="text" name="mos" value="" maxlength="100" required/>
                        </div>
                        <div class="col-xs-4  form-group @if($errors->has('lc_no')) has-error @endif">
                            {!! Form::label('lc_no', 'LC/TT#*', ['class' => 'control-label']) !!}
                            <select name="lc_no" class="form-control select2" id="lc_no" required>
                                <option value="">Select LC/TT</option>
                                @if ($LcBankModel->count())
                                    @foreach($LcBankModel as $role)
                                        <option value="{{ $role->id }}">{{ $role->lc_no }} - {{array_get( Config::get('admin.mode_of_payment'), $role->transaction_type)}}- {{ $role->lc_amt }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span id="lc_no_handler"></span>
                            @if($errors->has('lc_no'))
                                <span class="help-block">
                            {{ $errors->first('lc_no') }}
                             </span>
                            @endif
                        </div>
                </div>
            </div>

            <div id="productmodel_col"></div>
            <!-- /.box-body -->
            <div class="col-lg-12 amount_section">

                <div class="col-lg-4 pull-right">
                    <div class="form-group">
                        <div class="no-padding col-lg-3">
                            <label class="pull-left">Total($)</label>
                        </div>
                        <div class="col-lg-9">
                            <input name="total_of_product" type="text" class="form-control" id="total_of_product"  required readonly maxlength="15" min="1" value=""/>
                        </div>
                    </div>
                </div>
            </div>

        <div class="box-footer">
            <input type="submit" class='btn btn-danger' id="submit" value="Save">
            {{--{!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}--}}
        </div>
        {!! Form::close() !!}
        </div>
    </div>
@stop

@section('javascript')
 <script src="{{ url('public/js/admin/cinvoice/create_modify.js') }}" type="text/javascript"></script>
 <script src="https://oagerp.netrootserp.com/public/adminlte/bower_components/moment/min/moment.min.js"></script>
 <script>
      $(".datepicker").datepicker({ format: 'yyyy-mm-dd' });
      $(document).on('blur','#total_days',function(){
               var total_days = $(this).val();
               var openpdate = $('#document_date').val();
               console.log(total_days);
               if(openpdate !=''){
                   var start = moment(openpdate, 'YYYY-MM-DD');
                   endDate = start.add( total_days, 'days');
                   newdate = endDate.format('YYYY-MM-DD');
                   $('#lc_expiry_date').val(newdate);
               }else{
                   alert('Please Enter Document Date!');
                   return false;
               }

           });
     $(document).ready(function(){
         $("#lc_no").on('change',function() {
             $('#productmodel_col').empty();
             var feildvalue = $(this).val();
             if(feildvalue!=''){
                 var url = '{{ url('admin/cinvoice/productlist') }}';
                 url = url +'/'+ feildvalue;
                 $.ajax({
                     type:"get",
                     url : url,
                     success:function(data){
                         if(data!=''){
                             //product_select= data;
                             $('#productmodel_col').html(data);
                         }else{
                             alert("There is no product.");
                         }


                     }
                 });
             }

         });

         $(document).on('click','.product_checker',function () {
             var $current = $(this).val();
             if ($(this).is(":checked")) {
                 $('#prof_'+$current).show();
             }else{
                 $('#prof_'+$current).hide();
             }
         });
     });
 </script>
@endsection

