<table style="display: none;">
    <tbody id="line_item-container" >
    <tr id="line_items-######">
        <td >
            <div class="form-group" style="margin-bottom: 0px !important;">
                <div class="form-group" style="margin-bottom: 0px !important;">

                 {!! Form::select('line_items[product_id][######]',array(), old('product_id'), ['id' => 'line_item-product_id-######','class' => 'form-control dublicate_product description-data-ajax', 'required' => 'true']) !!}
            </div>
            </div>
        </td>
        <td>

            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::select('line_items[unit_name][######]',$UnitsModel, old('unit_name'), ['id' => 'line_item-unit_name_id-######','class' => 'form-control select2', 'style'=>'width:100%;' , 'required' => 'true']) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;" >

                {!! Form::number('line_items[qunity][######]', old('qunity'), ['id' => 'line_item-qunity_id-######','class' => 'form-control qty test' ,'data-row-id'=>'######', 'required' => 'true','min'=>"1"]) !!}

            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[p_amount][######]', old('p_amount'), ['id' => 'line_item-p_amount_id-######','class' => 'form-control test' ,'data-row-id'=>'######', 'required' => 'true','min'=>"1"]) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_items[amount][######]', old('amount'), ['id' => 'line_item-amount_id-######','class' => 'form-control cal_sum' , 'required' => 'true','min'=>"1"]) !!}
            </div>
        </td>
        <td><button id="line_items-del_btn-######" onclick="FormControls.destroyLineItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>
    </tbody>
</table>