<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-4 form-group @if($errors->has('margin_rate')) has-error @endif">
            {!! Form::label('margin_rate', 'LC Margin*', ['class' => 'control-label']) !!}
            {!! Form::text('margin_rate',old('margin_rate'),  ['id' =>'margin_rate' ,'class' => 'form-control', 'placeholder' => '','maxlength'=>10,'required']) !!}
            @if($errors->has('margin_rate'))
                <span class="help-block">
                    {{ $errors->first('margin_rate') }}
                </span>
            @endif
        </div>
        <div class="col-xs-4 form-group @if($errors->has('mu_profit')) has-error @endif">
            {!! Form::label('mu_profit', 'Murabaha Profit(%)*', ['class' => 'control-label']) !!}
            {!! Form::text('mu_profit',old('mu_profit'),['id' =>'mu_profit' , 'class' => 'form-control', 'placeholder' => '','maxlength'=>'20','required']) !!}
            @if($errors->has('mu_profit'))
                <span class="help-block">
                   {{ $errors->first('mu_profit') }}
               </span>
            @endif
        </div>
    </div>
</div>
<input type="hidden" name="lcorder" value="{{json_encode($proforma,TRUE)}}"/>
@foreach ($proforma as $results)

    <?php
    $proformano =  App\Models\Admin\Orders::where(['id' => $results])->where(['status' => '2'])->first();
    $orderlist =  App\Models\Admin\OrderDetailModel::where('perchas_id',$results)->where('remaining_qty','>','0')->get();
    $a = 1;
    ?>
   <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-2">
                <input id="procheck_{{$results}}" name="line_items[{{$results}}][pfno]" class="product_checker" type="checkbox" value="{{$results}}" checked>
            </div>
            <div class="col-xs-4">
                <label class="label label-success">Performa No: {{$proformano->perfomra_no}}</label>
            </div>
        </div>
    </div>
    <div id="prof_{{$results}}">
        <table class="table table-condensed" id="entry_items">
            <thead>
            <tr>
                <th>Item</th>
                <th>UOM</th>
                <th>Qty</th>
                <th>Rec. Qty*</th>
                <th>Unit Price($)</th>
                <th>Total Amount($)</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($orderlist as $result)
                <tr>
                    <td>
                       {{$result->productsModel->products_short_name}} - {{$result->productsModel->item_code}}
                        <input type="hidden" name="line_items[{{$results}}][product_id][]" value="{{$result->product_id}}">
                        <input type="hidden" name="line_items[{{$results}}][detail_id][]" value="{{$result->id}}">
                   </td>
                    <td>
                        {{$result->unitsModel->shortcode}}
                    </td>
                    <td>
                        {!! Form::text('line_items['.$results.'][total_qty][]',$result->remaining_qty,  ['id'=>'rem_qty_'.$a.$results,'class' => 'form-control', 'readonly' => 'true','data-row-id'=>$a.$results]) !!}
                    </td>
                    <td>
                        {!! Form::number('line_items['.$results.'][rel_qty][]', 0, ['id'=>'rel_qty_'.$a.$results,'class' => 'form-control calculate','required' => 'true','maxlength'=>10,'data-row-id'=>$a.$results]) !!}
                        {{--<input type="number" name="line_items[{{$results}}][rel_qty][]" value=""  class="form-control" required maxlength="10" min="0">--}}
                    </td>
                    <td>
                        {!! Form::text('line_items['.$results.'][unit_price][]', $result->unit_price, ['id'=>'unit_price_'.$a.$results,'class' => 'form-control', 'readonly' => 'true']) !!}
                    </td>
                    <td>
                        {!! Form::text('line_items['.$results.'][total_amount][]',old('total_amount'), ['id'=>'total_amount_'.$a.$results,'class' => 'form-control cal_sum', 'readonly' => 'true','data-row-id'=>$a.$results]) !!}
                    </td>
                </tr>
                <?php $a++; ?>
            @endforeach
            </tbody>
        </table>
    </div>
@endforeach


