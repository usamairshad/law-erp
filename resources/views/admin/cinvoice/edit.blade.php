@extends('layouts.app')

@section('stylesheet')

    <style>
        .example-template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Proforma Invoice</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add Performa Detail</h3>
            <a href="{{ route('admin.orders.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::model($Orders, ['method' => 'PUT', 'id' => 'validation-form','route' => ['admin.orders.update', $Orders->id]]) !!}

        <div class="box-body">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-4 form-group @if($errors->has('perfomra_no')) has-error @endif">
                        {!! Form::label('perfomra_no', 'Proforma Number*', ['class' => 'control-label']) !!}
                        <input id="perfomra_no" class="form-control" type="number" name="perfomra_no" value="{{$Orders->perfomra_no}}" min="1" maxlength="50" required/>
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('sal_no')) has-error @endif">
                        {!! Form::label('sale_no', 'Sales Order Number*', ['class' => 'control-label']) !!}
                        <input id="sale_no" class="form-control" type="number" name="sale_no" value="{{$Orders->sale_order_no}}" min="1" maxlength="50" required/>
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('shipping')) has-error @endif">
                        {!! Form::label('shipping', 'Shipping Via*', ['class' => 'control-label']) !!}
                        <input id="shipping" class="form-control" type="text" name="shipping" value="{{$Orders->shipping}}" maxlength="50" required/>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-4 form-group @if($errors->has('sal_date')) has-error @endif">
                        {!! Form::label('sal_date', 'Proforma Invoice Date*', ['class' => 'control-label']) !!}
                        {!! Form::text('sal_date', old('sal_date'), ['id' => 'sal_date','class' => 'form-control datepicker', 'placeholder' => '' ,'required' => 'true']) !!}
                        @if($errors->has('sal_date'))
                            <span class="help-block">
                            {{ $errors->first('sal_date') }}
                            </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('valid_upto')) has-error @endif">
                        {!! Form::label('valid_upto', 'On/About*', ['class' => 'control-label']) !!}
                        {!! Form::text('valid_upto', old('valid_upto'), ['id' => 'valid_upto','class' => 'form-control datepicker', 'placeholder' => '' ,'required' => 'true']) !!}
                        @if($errors->has('valid_upto'))
                            <span class="help-block">
                                {{ $errors->first('valid_upto') }}
                                </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('soruce')) has-error @endif">
                        {!! Form::label('soruce', 'From*', ['class' => 'control-label']) !!}
                        <input id="soruce" class="form-control" type="text" name="soruce" value="{{$Orders->soruce}}"  maxlength="50" required/>
                    </div>


                </div>

                <div class="col-xs-12 form-group">

                    <div class="col-xs-4 form-group @if($errors->has('destination')) has-error @endif">
                        {!! Form::label('destination', 'To*', ['class' => 'control-label']) !!}
                        <input id="destination" class="form-control" type="text" name="destination" value="{{$Orders->destination}}" maxlength="50" required/>
                    </div>
                </div>
                <div class="col-xs-12 form-group">

                    <div class="col-xs-4 form-group @if($errors->has('po_number')) has-error @endif">
                        {!! Form::label('po_number', 'Po No*', ['class' => 'control-label']) !!}
                        {!! Form::number('po_number', $Orders->po_number, ['class' => 'form-control', 'placeholder' => '' , 'required' => 'true','min'=>"1",'maxlength'=>50]) !!}
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('po_date')) has-error @endif">
                        {!! Form::label('po_date', 'Po Date *', ['class' => 'control-label']) !!}
                        {!! Form::text('po_date', old('po_date'), ['id'=>'po_date','class' => 'form-control datepicker', 'placeholder' => '' , 'required' => 'true']) !!}
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('sup_id')) has-error @endif">
                        {!! Form::label('sup_id', 'Brands *', ['class' => 'control-label']) !!}
                        <select id="sup_id" name="sup_id" class="form-control select2" disabled>
                            @if ($SuppliersModel->count())
                                <option value="">Select Brands</option>
                                @foreach($SuppliersModel as $Supplier)
                                    <option value="{{ $Supplier->id }}"{{$Orders->sup_id == $Supplier->id ? 'selected="selected"' : ''}}>{{ $Supplier->sup_name }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('sup_id'))
                            <span class="help-block">
                                {{ $errors->first('sup_id') }}
                                </span>
                        @endif
                    </div>
                </div>
            <div class="panel-body pad table-responsive">
                <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>R</u>ow</button>
                <table class="table table-bordered table-striped" id="users-table">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>UOM</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Total Amount</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php  $counter=0 ?>
                    <?php
                    $a = 0;
                    ?>
                    @foreach ($OrderDetail as $Details)
                        <?php
                        $a++;
                        ?>
                    <tr id="line_items-1">
                        <td width="30%"><div class="form-group  @if($errors->has('product_id')) has-error @endif">

                                <select id="product_id_" name="product_id[]" class="form-control select2 dublicate_product" required>
                                    @if ($Products->count())
                                        <option value="">Select Product</option>
                                        @foreach($Products as $Product)
                                            <option value="{{ $Product->id }}"{{$Details->product_id == $Product->id ? 'selected="selected"' : ''}}>{{ $Product->products_short_name  }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('product_id'))
                                    <span class="help-block">
                                {{ $errors->first('product_id') }}
                                </span>
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="form-group  @if($errors->has('line_unit_name')) has-error @endif">

                                <select id="unit_name" name="unit_name[]" class="form-control select2" required>
                                    @if ($UnitsModel->count())
                                        <option value="">Select Unit of Measurement</option>
                                        @foreach($UnitsModel as $Units)
                                            <option value="{{ $Units->id }}"{{$Details->unit_name == $Units->id ? 'selected="selected"' : ''}}>{{ $Units->shortcode  }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('line_unit_name'))
                                    <span class="help-block">
                                {{ $errors->first('line_unit_name') }}
                                </span>
                                @endif
                            </div>
                        </td>
                        <td> <div class="form-group  @if($errors->has('qunity')) has-error @endif">

                                {!! Form::number('qunity[]', $Details->qty, ['id' => 'qunity_'.$a, 'class' => 'form-control test', 'placeholder' => '','data-row-id'=>$a , 'required' => 'true','min'=>"1",'maxlength'=>4]) !!}
                            @if($errors->has('line_qunity'))
                                    <span class="help-block">
                                    {{ $errors->first('line_qunity') }}
                                    </span>
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="form-group  @if($errors->has('p_amount')) has-error @endif">

                                {!! Form::number('p_amount[]', $Details->unit_price, ['id' => 'p_amount_'.$a, 'class' => 'form-control test', 'placeholder' => '','data-row-id'=>$a , 'required' => 'true','min'=>"1",'maxlength'=>6]) !!}
                                @if($errors->has('p_amount'))
                                    <span class="help-block">
                                    {{ $errors->first('p_amount') }}
                                    </span>
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="form-group  @if($errors->has('amount')) has-error @endif">

                                {!! Form::number('amount[]', $Details->amount, ['id' => 'amount_'.$a, 'class' => 'form-control cal_sum', 'placeholder' => '','data-row-id'=>$a , 'required' => 'true','min'=>"1",'maxlength'=>10 ,'readonly']) !!}
                                @if($errors->has('amount'))
                                    <span class="help-block">
                                {{ $errors->first('amount') }}
                                </span>
                                @endif
                            </div>
                            {!! Form::hidden('perchas_id[]', $Details->id, ['id' => 'perchas_id', 'class' => 'form-control', 'placeholder' => '']) !!}
                        </td>
                        <td>N/A</td>

                    </tr>
                    @endforeach
                    <input type="hidden" id="line_new-global_counter" value="<?php  echo $a ?>"   />

                    </tbody>

                </table>
            </div>
            <div class="col-lg-12 amount_section">

                <div class="col-lg-4 pull-right">
                    <div class="form-group">
                        <div class="no-padding col-lg-3">
                            <label class="pull-left">Total</label>
                        </div>
                        <div class="col-lg-9">
                            <input name="total_of_product" type="text" class="form-control" id="total_of_product"  required readonly maxlength="15" min="1" value="<?php echo $Orders->total_order_amt ;?>"/>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.orders.items_update')

            <div class="box-footer">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
            </div>


        </div>
        <!-- /.box-body -->

        {!! Form::close() !!}
        {{--@include('admin.entries.voucher.sale_voucher.entries_template')--}}
    </div>
@stop

@section('javascript')

    <script src="{{ url('update_modify.js') }}" type="text/javascript"></script>

    <script>
            $(document).on('blur','.test',function () {
                calculated_amount = '0';
                rowId = $(this).attr('data-row-id');
                quantity = $('#qunity_'+rowId).val();
                amount = $('#p_amount_'+rowId).val();
                total = amount * quantity;
                $('#amount_'+rowId).val(total.toFixed(2));
                $('.cal_sum').each(function () {
                    totalvalue = $(this).val();
                    if(totalvalue != '' && totalvalue != '0')
                    {
                        calculated_amount = parseFloat(calculated_amount) + parseFloat(totalvalue);
                        $('#total_of_product').val(calculated_amount);
                    }

                });
            })

    $(document).on('change','select.dublicate_product',function () {
        var $current = $(this);
        $('select.dublicate_product').each(function() {
            if ($(this).val() == $current.val() && $(this).attr('id') != $current.attr('id'))
            {
                alert('Duplicate Product Found!');
                $current.addClass('duplicate');
                $current.empty();
                return false;
            }
        });


    })
    </script>
@endsection