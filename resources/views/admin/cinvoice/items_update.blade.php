<table style="display: none;">
    <tbody id="line_new-container" >
    <tr id="line_new-######">
        <td >
            <div class="form-group" style="margin-bottom: 0px !important;">
                <div class="form-group" style="margin-bottom: 0px !important;">

                 {!! Form::select('line_new[product_id][######]',array(), old('product_id'), ['id' => 'line_new-product_id-######','class' => 'form-control dublicate_product', 'required' => 'true']) !!}
            </div>
            </div>
        </td>
        <td>

            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::select('line_new[unit_name][######]',$UnitsModels, old('unit_name'), ['id' => 'line_new-unit_name_id-######','class' => 'form-control', 'style'=>'width:100%;' , 'required' => 'true']) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;" >

                {!! Form::number('line_new[qunity][######]', old('qunity'), ['id' => 'qunity_######','class' => 'form-control test' ,'data-row-id'=>'######', 'required' => 'true','min'=>"1",'maxlength'=>4]) !!}

            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_new[p_amount][######]', old('p_amount'), ['id' => 'p_amount_######','class' => 'form-control test' ,'data-row-id'=>'######', 'required' => 'true','min'=>"1",'maxlength'=>6]) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('line_new[amount][######]', old('amount'), ['id' => 'amount_######','class' => 'form-control cal_sum' , 'required' => 'true','min'=>"1",'maxlength'=>10 ,'readonly']) !!}
            </div>
        </td>
        <td><button id="line_new-del_btn-######" onclick="FormControls.destroyLineItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>
    </tbody>
</table>