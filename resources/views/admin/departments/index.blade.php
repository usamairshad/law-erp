@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Departments</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('departments_create'))
                <a href="{{ route('admin.departments.create') }}" class="btn btn-success pull-right">Add New Department</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Departments) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Branch Name</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>

                @if (count($Departments) > 0)
                    @foreach ($Departments as $Department)
                        <tr data-entry-id="{{ $Department->id }}">
                            <td>{{ $Department->name }}</td>
                            <td>{{ $Department->branch['name'] }}</td>
                            <td>{{ date('d-m-Y h:m a', strtotime($Department->created_at ))}}</td>
                            <td>
                                @if(Gate::check('departments_edit'))
                                    <a href="{{ route('admin.departments.edit',[$Department->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                {{--@if(Gate::check('departments_destroy'))--}}
                                    {{--{!! Form::open(array(--}}
                                        {{--'style' => 'display: inline-block;',--}}
                                        {{--'method' => 'DELETE',--}}
                                        {{--'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",--}}
                                        {{--'route' => ['admin.departments.destroy', $Department->id])) !!}--}}
                                    {{--{!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}--}}
                                    {{--{!! Form::close() !!}--}}
                                {{--@endif--}}
                            </td>
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection