@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Departments</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Department</h3>
            <a href="{{ route('admin.departments.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Department, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.departments.update', $Department->id]]) !!}
        <div class="box-body">
            <div class="form-group col-md-6 @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'50','pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-4 @if($errors->has('branch_id')) has-error @endif">
                {!! Form::label('branch_id', 'Branch Name*', ['class' => 'control-label']) !!}
                {!! Form::select('branch_id', $branch->prepend('Select Branch', ''),old('branch_id'), ['class' => 'form-control','required']) !!}

                @if($errors->has('branch_id'))
                    <span class="help-block">
                        {{ $errors->first('branch_id') }}
                    </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/departments/create_modify.js') }}" type="text/javascript"></script>
@endsection