@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Assign Board Program Class To Terms</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            
            <a  href="{{route('admin.bpc-term.create')}}" class="btn btn-success pull-right">Create</a>
            
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($class_sections) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Board</th>
                    <th>Program</th>
                    <th>Class</th>
                    <th>Terms</th>
                </tr>
                </thead>

                <tbody>
                @if (count($class_sections) > 0)
                    @foreach ($class_sections as $cs)
                        <tr>
                            <td>{{ $cs['board']->name }}</td>
                            <td>{{ $cs['program']->name }}</td>
                            <td>{{ $cs['class']->name }}</td>
                            <td>
                            @foreach($cs['terms'] as $term)
                                <span class="badge badge-success">{{$term}}</span>
                            @endforeach
                            </td>
                        </tr>
                        
                    @endforeach
                    
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection