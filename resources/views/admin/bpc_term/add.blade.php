@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Board Program Class Term</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <a href="{{  route('admin.bpc-term.index') }}"  class="btn btn-success pull-right">View Board Program Class Terms
                        </a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.bpc-term.store'], 'id' => 'validation-form']) !!}
        <div id="show">
            <div id="box" class="box-body">
                <h2>Assign Class Section</h2>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('board_id', 'Boards*', ['class' => 'control-label']) !!}
                        <select id="board" class="form-control select2 boards" name="board_id" required="required">
                            <option selected="selected" disabled="disabled">---Select----</option>
                            @foreach($boards as $key => $board)
                            <option value="{{$board['id']}}">{{$board['name']}}</option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif">
                    {!! Form::label('programs', 'Programs*', ['class' => 'control-label']) !!}

                    <select class="form-control select2 programs" name="program_id" id="programs" required="required"></select>

                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('class_id', 'Class*', ['class' => 'control-label']) !!}

                        <select class="form-control select2 classes" name="class_id" id="classes" required="required"></select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('section_id', 'Terms*', ['class' => 'control-label']) !!}
                        <select id="terms" class="form-control select2 terms" name="term_id[]" multiple="multiple" required="required">
                            @foreach($terms as $key => $term)
                            <option value="{{$term['id']}}">{{$term['name']}}</option>
                            @endforeach
                        </select>
                </div>
                {{--<div class="form-group col-md-1">
                    <button style="margin-top: 22px" type="button" name="add" id="add" class="btn btn-success add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>--}}
            </div> 
            <div id="secondbox">
                
            </div>  
            <button id="btn"  class="btn btn-success col-md-12" > Save</button>
            
        </div>

        <!-- /.box-body -->
       
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById('btn').setAttribute("disabled",null);
            $( document ).change(function(){
                var e = document.getElementById("terms");
                var strUser = e.value;
                if (strUser != "") {
                document.getElementById('btn').removeAttribute("disabled");
                } else {
                    document.getElementById('btn').setAttribute("disabled", null);
                }
            })
            
        });
         $(".boards").select2({

            });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.terms').select2();
        });
        function selectRefresh() {
            $(".classes").select2({

            });
        }
        function selectRefresh() {
            $(".sections").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
    <script type="text/javascript">


    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" style="height:120px"><hr style="border-top:1px solid #222d32"><div class="form-group col-md-3"><select id="board" class="form-control select2 boards" name="board_id[]">@foreach($boards as $key => $board)<option value="{{$board['id']}}">{{$board['name']}}</option>@endforeach</select></div><div class="form-group col-md-3"><select class="form-control select2 programs" name="programs" id="programs"></select></div><br> <div class="form-group col-md-2"><button style="margin-top: -27px;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');
            $('.select2').select2(); 

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    // function showBranch() {
    //     var data= $('input[name="board_name"]').val();

    //     if(data)
    //     {
    //          $('#show').show();
    //          $('#btn').hide(); 
    //     }
    // }
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
    <script>
        $(function(){
            $('#board').change(function(){
               $("#programs option").remove();;
               var id = $(this).val();
               $.ajax({
                  url : '{{ route( 'admin.board-loadPrograms' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {
                        console.log(result)
                        $('#programs').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        $.each( result, function(k, v) {
                           $('#programs').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });

        $(function(){
            $('#programs').change(function(){
               $("#classes option").remove();;
               var id = $(this).val();
               var board_id =$("select[name*='board_id']").val();

               console.log("board_id "+board_id);
               console.log("program_id "+id);
               $.ajax({
                  url : '{{ route( 'admin.load-board-program-class' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "program_id": id,
                    "board_id":board_id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {     
                        console.log(result)
                        $('#classes').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        $.each( result, function(k, v) {
                            $('#classes').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
@endsection

