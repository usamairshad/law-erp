@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sales</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Sales) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                <tr>
                    {{--<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>--}}
                    <th width="8%">S: No</th>
                    <th>Sale no</th>
                    <th>Sale Date </th>
                    <th>Customer</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($Sales) > 0)
                    @foreach ($Sales as $Sale)

                        <tr data-entry-id="{{ $Sale->id }}">
                            <th width="8%"> {{$r}} </td>
                            <td>{{ $Sale->id }}</td>
                            <td>{{ $Sale->qoute_date }}</td>

                            <td>{{ $Sale->databank_name }}</td>
                            <td> New Order </td>
                            <td>

                                <a href="{{ route('marketing.salepipelines.details',[$Sale->id]) }}" class="btn btn-xs btn-success">@lang('View')</a>
                                {{--<a href="{{ route('marketing.salepipelines.details',[$Sale->id]) }}" class="btn btn-xs btn-primary">Generate Invoice</a>--}}
                                {{--@if(Gate::check('sales_edit'))--}}
                                        {{--<a href="{{ route('admin.sales.edit',[$Sale->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>--}}
                                {{--@endif--}}
                                @if(Gate::check('sales_destroy'))
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'DELETE',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.sales.destroy', $Sale->id])) !!}
                                        {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!}
                                @endif
                            </td>
                            </td>

                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.sales.mass_destroy') }}';
    </script>
@endsection

