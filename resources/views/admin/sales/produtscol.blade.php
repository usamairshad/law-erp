
@if (count($produtscol ) > 0)
<div id="produtscol_inn_body_{{ $produtscol->id }}" class="produtscol_inn_body" style="float: left; width: 100%;">

    <input id="produtscol_{{ $produtscol->id}}" type="hidden" name ="id[]" value="{{ $produtscol->id }}">

    <div class="col-xs-2 form-group @if($errors->has('quantity')) has-error @endif">
            <input id="quantity_{{ $produtscol->opening_stock}}" type="text" name ="quantity[]" value="@if ($produtscol->opening_stock <= 0) N/A @else {{$produtscol->opening_stock }} @endif" class="form-control" readonly="readonly">
            @if($errors->has('quantity'))
                <span class="help-block">
		{{ $errors->first('quantity') }}
		</span>
            @endif
        </div>

    @if ($produtscol->opening_stock <= 0)
        <p>Oop! Sorry <strong>{{ $produtscol->opening_stock  }}</strong> Quantity of this Produts !</p>
    @else
        <div class="col-xs-2 form-group @if($errors->has('st_model')) has-error @endif">
            <input id="st_model_{{ $produtscol->id}}" type="text" name ="st_unit_price[]" value="{{ $produtscol->st_model }}" class="form-control" readonly="readonly">
            @if($errors->has('st_model'))
                <span class="help-block">
		{{ $errors->first('st_model') }}
		</span>
            @endif
        </div>
        <div class="col-xs-2 form-group @if($errors->has('st_unit_price')) has-error @endif">
            <input id="st_unit_price_{{ $produtscol->id}}" type="text" name ="st_unit_price[]" value="{{ $produtscol->st_unit_price }}" class="form-control" readonly="readonly">
            @if($errors->has('st_unit_price'))
                <span class="help-block">
		{{ $errors->first('st_unit_price') }}
		</span>
            @endif
        </div>
        <div class="col-xs-2 form-group @if($errors->has('qunity')) has-error @endif">

            {{ Form::text('sale_quantity[]', $value = '' , $attributes = ['class' => 'tax_gst_ship form-control', 'id' => 'sale_quantity_'.$produtscol->id,'placeholder' => '0','min'=>'1','max'=>$produtscol->opening_stock]) }}

            @if($errors->has('qunity'))
                <span class="help-block">
		{{ $errors->first('qunity') }}
		</span>
            @endif
        </div>
        <div class="col-xs-2 form-group @if($errors->has('amount')) has-error @endif">
            {!! Form::text('sale_amount[]', old('amount'), ['id' => 'amount_'.$produtscol->id,'class' => 'amount_field form-control', 'readonly' => 'readonly']) !!}
            @if($errors->has('amount'))
                <span class="help-block">
		{{ $errors->first('amount') }}
		</span>
            @endif
        </div>
        <div class="col-xs-2 form-group text-center">
            <div style="border: 1px solid rgb(221, 221, 221); padding: 1px 0px;" class="text-center">
                <button class="del btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
            </div>
        </div>
    @endif


    @else
        <div class="col-xs-12">
            <p>There is no Data of this Produt!</p>
        </div>

</div>

@endif


<script>

    $(document).keyup("#sale_quantity_{{ $produtscol->id }}", function(){
        var qunity = $("#sale_quantity_{{ $produtscol->id }}").val() ;
        var st_unit_price = $("#st_unit_price_{{ $produtscol->id }}").val();
        var totalAmount = qunity*st_unit_price;
        var mycol = $("#amount_{{ $produtscol->id }}").val(totalAmount);
        calculateSum();



//        var st_total_balance = $("#total_balance").val();
//        var Total_Sum = mycol+st_total_balance;
//        var st_total_all = $("#total_balance").val(Total_Sum);
//        alert(st_total_all);


    });


    {{--$(document).ready(function(){--}}
        {{--$(document).keyup("#qunity_{{ $produtscol->id }}", function(){--}}
            {{--var qunity = $("#qunity_{{ $produtscol->id }}").val() ;--}}
            {{--var st_unit_price = $("#st_unit_price_{{ $produtscol->id }}").val();--}}
            {{--var totalAmount = qunity*st_unit_price;--}}
            {{--var mycol = $("#amount_{{ $produtscol->id }}").val(totalAmount);--}}
            {{--calculateSum();--}}
        {{--});--}}
    {{--});--}}



</script>




