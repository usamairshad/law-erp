<div class="row">
    <div class="col-xs-12">
        <div class="sales_row row">
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('sal_no')) has-error @endif">
                {!! Form::label('sal_no', 'Sale Code *', ['class' => 'control-label']) !!}
                {{--{!! Form::text('sal_no', old('sal_no'), ['class' => 'form-control', 'readonly' => 'true', 'placeholder' => 'Sale-?????']) !!}--}}
                {{--{!! Form::text('sal_no', old('sal_no'), ['value' => 'saasd' ,'class' => 'form-control', 'readonly' => 'true', 'placeholder' => 'Sale-????']) !!}--}}
                @if (count($Salno)>0)
                    @php( $val_1 = $Salno[0]->id)
                    @php( $val_2 = 1)
                    @php( $val_3 = $val_1+$val_2)
                    {{--@php( $val_4 = 000)--}}
                    <input id="sal_no" class="form-control" type="hidden" name="sal_no" value="sale-0000{{ $val_3 }}" readonly="readonly" />
                    <input id="new" class="form-control" type="text" name="new" value="sale-????" readonly="readonly" />
                    {{--@elseif($val_1 = $Salno->id)--}}
                    {{--{!! Form::text('sal_no', old('sal_no'), ['class' => 'form-control', 'readonly' => 'true', 'placeholder' => '']) !!}--}}
                @else
                    <input id="sal_no" class="form-control" type="hidden" name="sal_no" value="sale-00001" readonly="readonly" />
                    <input id="temp_field" class="form-control" type="text" name="temp_field" value="sale-????" readonly="readonly" />
                @endif
                @if($errors->has('sal_no'))
                    <span class="help-block">
				{{ $errors->first('sal_no') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('customer_id')) has-error @endif">
                {!! Form::label('customers_id', 'Customers', ['class' => 'control-label']) !!}
                <select id="customer_id" name="customer_id" class="form-control select2">
                    @if ($Customers->count())
                        <option value="">Select Customers</option>
                        @foreach($Customers as $Customer)
                            <option value="{{ $Customer->id }}">{{ $Customer->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if($errors->has('customers_id'))
                    <span class="help-block">
				{{ $errors->first('customers_id') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('branch_id')) has-error @endif">
                {!! Form::label('branch_id', 'Branche', ['class' => 'control-label']) !!}
                <select id="branch_id" name="branch_id" class="form-control select2">
                    @if ($Branches->count())
                        <option value="">Select Branches</option>
                        @foreach($Branches as $Branche)
                            <option value="{{ $Branche->id }}">{{ $Branche->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if($errors->has('branch_id'))
                    <span class="help-block">
				{{ $errors->first('branch_id') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('employee_id')) has-error @endif">
                {!! Form::label('employee_id', 'Employee', ['class' => 'control-label']) !!}
                <select id="employee_id" name="employee_id" class="form-control select2">
                    @if ($Employees->count())
                        <option value="">Select Employee</option>
                        @foreach($Employees as $Employee)
                            <option value="{{ $Employee->id }}">{{ $Employee->name }}</option>
                        @endforeach
                    @endif
                </select>
                @if($errors->has('employee_id'))
                    <span class="help-block">
				{{ $errors->first('employee_id') }}
				</span>
                @endif
            </div>
        </div>
        <div class="sales_row row">
            <div class="col-xs-6 col-lg-6 form-group @if($errors->has('sal_date')) has-error @endif">
                {!! Form::label('sal_date', 'Sale Date', ['class' => 'control-label']) !!}
                {!! Form::text('sal_date', old('sal_date'), ['class' => 'form-control', 'placeholder' => '' ]) !!}
                @if($errors->has('sal_date'))
                    <span class="help-block">
				{{ $errors->first('sal_date') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 col-lg-6 form-group @if($errors->has('valid_upto')) has-error @endif">
                {!! Form::label('valid_upto', 'Valid Upto', ['class' => 'control-label']) !!}
                {!! Form::text('valid_upto', old('valid_upto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                @if($errors->has('valid_upto'))
                    <span class="help-block">
				{{ $errors->first('valid_upto') }}
				</span>
                @endif
            </div>
        </div>
        <div class="sales_row row">
            <div class="col-xs-6 col-lg-6 form-group @if($errors->has('products_description')) has-error @endif">
                {!! Form::label('description', 'Description *', ['class' => 'control-label']) !!}
                {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 1, 'cols' => 1]) !!}
                @if($errors->has('description'))
                    <span class="help-block">
				{{ $errors->first('description') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 col-lg-6 form-group @if($errors->has('remarks')) has-error @endif">
                {!! Form::label('remarks', 'Remarks *', ['class' => 'control-label']) !!}
                {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 1, 'cols' => 1]) !!}
                @if($errors->has('remarks'))
                    <span class="help-block">
				{{ $errors->first('remarks') }}
				</span>
                @endif
            </div>
        </div>
        <div class="sales_brands_deatial_row">
            <div class="sales_row row">
                <div class="col-xs-6 col-lg-3 form-group @if($errors->has('sup_id')) has-error @endif">
                    {!! Form::label('sup_id', 'Brands', ['class' => 'control-label']) !!}
                    <select id="sup_id" name="sup_id" class="form-control select2">
                        @if ($Suppliers->count())
                            <option value="">Select Brands</option>
                            @foreach($Suppliers as $Supplier)
                                <option value="{{ $Supplier->id }}">{{ $Supplier->sup_name }}</option>
                            @endforeach
                        @endif
                    </select>
                    @if($errors->has('sup_id'))
                        <span class="help-block">
					{{ $errors->first('sup_id') }}
					</span>
                    @endif
                </div>
            </div>
            <div class="sales_row row">
                <div id="productdetail_row" class="productmodel_row">
                    <div class="col-xs-12">
                        <div class="controls_inn" style="margin-bottom: 10px; float: left; width: 100%; padding: 4px 8px; border: 1px solid rgb(221, 221, 221);">
                            <label style="float: left; margin-top: 4px;" class="control-label">Sales Detail</label>
                        </div>
                    </div>
                    <div class="productmodel_row_inn_h" style="float: left; width: 100%;">
                        <div class="col-xs-2"> {!! Form::label('st_name', 'Product Name *', ['class' => 'control-label']) !!} </div>
                        <div style="padding: 0 !important;" class="col-xs-10 00form-group">
                            <div class="col-xs-2"> {!! Form::label('quantity', 'Avilable quantity *', ['class' => 'control-label']) !!} </div>
                            <div class="col-xs-2"> {!! Form::label('st_model', 'Model *', ['class' => 'control-label']) !!} </div>
                            <div class="col-xs-2"> {!! Form::label('st_name', 'Unit Price *', ['class' => 'control-label']) !!} </div>
                            <div class="col-xs-2"> {!! Form::label('qunity', 'Quantity *', ['class' => 'control-label']) !!} </div>
                            <div class="col-xs-2 amount_balance"> {!! Form::label('amount', 'Amount *', ['class' => 'control-label']) !!} </div>
                            <div class="col-xs-2 text-center"> {!! Form::label('Action', 'Action', ['class' => 'control-label', 'style' => 'width: 100%;']) !!} </div>
                        </div>
                    </div>
                </div>
                <hr style="float:left;width: 100%;margin:4px 0;">
                <div id="amount_balance_row" class="col-lg-12 form-group @if($errors->has('total_balance')) has-error @endif">
                    <div class="no-padding col-lg-6 text-right">
                        {!! Form::label('total_balance', 'Total Balance:  *', ['class' => 'tax_gst_ship control-label','readonly'=>'readonly']) !!}
                    </div>
                    <div class="amount_balance col-lg-3">
                        {!! Form::text('total_balance', old('0'), [ 'class' => 'form-control', 'placeholder' => '','readonly'=>'readonly']) !!}
                        @if($errors->has('total_balance'))
                            <span class="help-block">
						{{ $errors->first('total_balance') }}
						</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="sales_row row">
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('tax')) has-error @endif">
                {!! Form::label('tax', 'Tax (in %) *', ['class' => 'tax_gst_ship control-label']) !!}
                {{ Form::text('tax', $value = '' , $attributes = ['class' => 'tax_gst_ship form-control', 'id' => 'tax','placeholder' => '0','min'=>'1','max'=>'100']) }}
                @if($errors->has('tax'))
                    <span class="help-block">
				{{ $errors->first('tax') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('gst')) has-error @endif">
                {!! Form::label('gst', 'GST (In %) *', ['class' => 'tax_gst_ship control-label']) !!}

                {{ Form::text('gst', $value = '' , $attributes = ['class' => 'tax_gst_ship form-control', 'id' => 'gst','placeholder' => '0','min'=>'1','max'=>'100']) }}
                @if($errors->has('gst'))
                    <span class="help-block">
				{{ $errors->first('gst') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('shipping_charges')) has-error @endif">
                {!! Form::label('shipping_charges', 'Shipping Charges *', ['class' => 'tax_gst_ship control-label']) !!}
                {{ Form::text('shipping_charges', $value='' , $attributes = ['class' => 'form-control', 'id' => 'shipping_charges','placeholder' => '0']) }}

            @if($errors->has('shipping_charges'))
                    <span class="help-block">
				{{ $errors->first('shipping_charges') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('total_amount')) has-error @endif">
                {!! Form::label('total_amount', 'Total Amount *', ['class' => 'total_amount control-label']) !!}
                {!! Form::text('total_amount', old('total_amount'), ['class' => 'form-control','placeholder'=>'','readonly'=>'readonly']) !!}
                @if($errors->has('total_amount'))
                    <span class="help-block">
				{{ $errors->first('total_amount') }}
				</span>
                @endif
            </div>
        </div>
        <div class="sales_row row">
            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('discount')) has-error @endif">
                {!! Form::label('discount', 'Discount (In %)', ['class' => 'control-label']) !!}
                {{ Form::text('discount', $value='' , $attributes = ['class' => 'form-control', 'id' => 'discount','placeholder' => '0','min'=>'1','max'=>'100']) }}

                @if($errors->has('discount'))
                    <span class="help-block">
				{{ $errors->first('discount') }}
				</span>
                @endif
            </div>
            <div class="col-xs-6 form-group @if($errors->has('final_amount')) has-error @endif">
                {!! Form::label('final_amount', 'Final Amount *', ['class' => 'control-label']) !!}
                {!! Form::text('final_amount', old('final_amount'), ['class' => 'form-control','placeholder'=>'','readonly'=>'readonly']) !!}
                @if($errors->has('final_amount'))
                    <span class="help-block">
				{{ $errors->first('final_amount') }}
				</span>
                @endif
            </div>
        </div>
    </div>
</div>