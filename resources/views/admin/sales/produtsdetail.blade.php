@if (count($results ) > 0)
    <div class="produtsdetail_inn_body" style="float: left; width: 100%;">
<div class="col-xs-2 form-group @if($errors->has('st_name')) has-error @endif">
    <select id="st_name_{{ $sup_id }}" data-sup="{{$sup_id}}" name="st_name[]" class="st_name form-control select2" required>
        @if ($results->count())
            <option value="">Select Product</option>
            @foreach($results as $result)
                <option value="{{ $result->id }}">{{ $result->st_name }}</option>
            @endforeach
        @endif
    </select>
    @if($errors->has('st_name'))
        <span class="help-block">
            {{ $errors->first('st_name') }}
            </span>
    @endif
</div>
    <div id="produtscol_sup_{{ $sup_id }}" class="produtscol_row col-xs-10" style="padding: 0 !important;">
        <p id="here_p">product detail are here...</p>
    </div>

</div>
@else
    <div class="col-xs-12">
        <p class="btn-warning" style="padding: 3px 6px; border-radius: 4px; text-align: center;">There is no produt of such Brand...!</p>
    </div>

@endif