@extends('layouts.app')
@section('stylesheet')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/select2/dist/css/select2.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <style>
        .products_template > li {
            display: inline-block;
            float: left;
            margin: 0 0px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
        .heading_ul label{margin-bottom: 0 !important;}
    </style>
@stop
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale</h1>
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Sale</h3>
            <a href="{{ route('admin.sales.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.sales.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('admin.sales.fields')
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
        {{--@include('admin.entries.voucher.sale_voucher.sales_template')--}}
    </div>
@stop
@section('javascript')
    <script>
        //Brands Ajax Here...
        $(document).ready(function(){
            $(".productmodel_row_inn_h").hide();
            $("#amount_balance_roe").hide();

            $('#sup_id').on("change",function(){
                $('#st_name').select2();
                $(".productmodel_row").show();
                $(".productmodel_row_inn_h").show();
                $("#amount_balance_roe").show();

                var val = $(this).val();
                if(val != ''){
                    var url = '{{ url('admin/sales/produtsdetail') }}';
                    url = url +'/'+ val;
                    $.ajax({
                        type: "get",
                        url: url,
                        success:function(data){
                            //alert(data);
                            if(data) {
                                console.log(data);
                                $("#productdetail_row").append(data);
                            }
                            else{
                                alert('No data founded....!');
                            }
                        }
                    });
                    return false;
                }
            });
        });

        $(document).on('click', '.produtsdetail_inn_body .del', function(event) {
            var target = $(event.target);
            var row = target.closest('.produtsdetail_inn_body');
            row.remove();
        });


        // Stock Items Ajax Here...

        //$(window).find('.st_name').on('change', function(event) {
        $('.sales_row').on('change', '.st_name', function(event) {
            //alert('here');
            var st_val = $(this).val();
            var sup = $(this).attr('data-sup');
            //alert(st_val);
            if(st_val != ''){
                var url = '{{ url('admin/sales/produtscol') }}';
                url = url +'/'+ st_val;
                $.ajax({
                type: "get",
                url: url,
                    success:function(data){
                        //alert(data);
                    if(data) {
                        $("#produtscol_sup_"+sup).html(data);
                    }
                    else{
                        alert('No data founded....!');
                    }
                    }
                });
                return false;
            }
        });

        // Calculate Sum Items Here...
        function calculateSum() {
            var sum = 0;
            //iterate through each textboxes and add the values
            $(".amount_field").each(function() {
                //add only if the value is number
                if(!isNaN(this.value) && this.value.length!=0) {
                    sum += parseFloat(this.value);
                }

            });
            //.toFixed() method will roundoff the final sum to 2 decimal places
            $("#total_balance").val(sum.toFixed(2));
            //$("#total_amount").val(sum.toFixed(2));
            //$("#final_amount").val(sum.toFixed(2));
        }

        // Calculate Tax, GST and Shipping Charges sale Items Here...

        $(document).ready(function(){
            $("#tax").keyup(function() {
                var taxvalue = $(this).val();
                if(taxvalue > 100){
                    alert("Please enter less then 100 and greater 0");
                }else{
                    var total_balance = $("#total_balance").val();
                    var final_amount = $("#final_amount").val();
                    var result = parseFloat(parseInt(taxvalue, 10) * parseInt(total_balance, 10))/100 ;
                    var TaxTotal = parseFloat(result) + parseFloat( total_balance );
                    //The isNaN() function determines whether a value is an illegal number (Not-a-Number)
                    if( !isNaN(TaxTotal) ) {
                       var total_amount_var =  $("#total_amount" ).val(TaxTotal.toFixed(2));
                       //var final_amount_var =  $("#final_amount").val(TaxTotal.toFixed(2));
                    }else{
                        $("#total_balance").val();
                    }
                }

            });

            $("#gst").keyup(function() {
                var nan =  0;
                var gstvalue = $(this).val();
                var total_amount_tax = $("#total_amount").val();
                //var final_amount_tax = $("#final_amount").val();
                var gst_result = parseFloat(parseInt(gstvalue, 10) * parseInt(total_amount_tax, 10))/100 ;
                var GSTTotalAmount = parseFloat( gst_result ) + parseFloat( total_amount_tax );
                //The isNaN() function determines whether a value is an illegal number (Not-a-Number)
                if( !isNaN( GSTTotalAmount ) ) {
                    $( "#total_amount" ).val(GSTTotalAmount.toFixed(2));
                }else{
                    $("#total_balance").val();
                }

            });

            $("#shipping_charges").keyup(function() {
                var shipping_charges_value = $(this).val();
                var total_amount_gst = $("#total_amount").val();
                //var final_amount_tax = $("#final_amount").val();
                var Shp_TotalAmount = parseFloat( shipping_charges_value ) + parseFloat( total_amount_gst );
                //The isNaN() function determines whether a value is an illegal number (Not-a-Number)
                if( !isNaN( Shp_TotalAmount ) ) {
                    $( "#total_amount" ).val(Shp_TotalAmount.toFixed(2));
                    //$("#final_amount").val(Shp_TotalAmount.toFixed(2));
                }

            });


            // Calculate Discount Charges sale Items Here...

            $("#discount").keyup(function() {
                var discount_value = $(this).val();
                //var final_amount_new = $("#final_amount").val();
                var final_amount_new = $("#total_amount").val();
                var discount_result = parseFloat(parseInt(discount_value, 10) * parseInt(final_amount_new, 10))/100 ;
                //alert(discount_result);
                var discount_result = parseFloat( final_amount_new )- parseFloat( discount_result );
                //The isNaN() function determines whether a value is an illegal number (Not-a-Number)
                if( !isNaN( discount_result ) ) {
                    $( "#final_amount" ).val(discount_result.toFixed(2));
                }else{
                    $( "#final_amount" ).val(final_amount_new.toFixed(2));
                }

            });
        });



    </script>
    <!-- date-range-picker -->
    <script src="{{ url('adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    {{--<script src="{{ url('adminlte') }}/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>--}}
    <script src="{{ url('adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Select2 -->
    <script src="{{ url('adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('js/admin/sales/sales_create_modify.js') }}" type="text/javascript"></script>
@endsection