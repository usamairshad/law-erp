@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('stylesheet')
    <!-- netroots_assets -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/netroots_assets/css/style.css">
@stop
@section('content')
    <h3 class="page-title"> Sale Details</h3>
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ $Sales ? 'datatable' : '' }} dt-select">
                <thead>
                <tr>
                    <th width="20%">Name</th>
                    <th width="80%">Information</th>
                </tr>
                </thead>
                <tbody>
                <tr><th width="20%">Sale No</th><td width="80%">{{ $Sales->sal_no }}</td></tr>
                {{--<tr><th width="20%">Customer Name</th><td width="80%">{{ $Sales->customer_id }}</td></tr>--}}
                </tbody>
            </table>
        </div>
    </div>
@stop
