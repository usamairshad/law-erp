@extends('layouts.app')
@section('stylesheet')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/select2/dist/css/select2.min.css">
    <!-- date range picker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- bootstrap date time picker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <style>
        .products_template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sales</h1>
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Sale</h3>
            <a href="{{ route('admin.sales.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Sales, ['method' => 'PUT', 'id' => 'validation-form','route' => ['admin.sales.update', $Sales->id]]) !!}
        <div class="box-body">
           <div class="row">
                <div class="col-xs-12">
                    <div class="sales_row row">
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('sal_no')) has-error @endif">
                            {!! Form::label('sal_no', 'Sale Code *', ['class' => 'control-label']) !!}
                            {!! Form::text('sal_no', old('sal_no'), ['class' => 'form-control', 'readonly' => 'true', 'placeholder' => 'Sale-?????']) !!}
                            @if($errors->has('sal_no'))
                                <span class="help-block">
				{{ $errors->first('sal_no') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('customer_id')) has-error @endif">
                            {!! Form::label('customers_id', 'Customers', ['class' => 'control-label']) !!}
                            <select id="customer_id" name="customer_id" class="form-control select2" disabled>
                                @if ($Customers->count())
                                    <option value="">Select Customers</option>
                                    @foreach($Customers as $Customer)
                        <option value="{{ $Customer->id }}" @if($Sales->id==$Customer->id) {{'selected="selected"'}} @endif>{{ $Customer->name }}</option>

                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has('customers_id'))
                                <span class="help-block">
				{{ $errors->first('customers_id') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('branch_id')) has-error @endif">
                            {!! Form::label('branch_id', 'Branche', ['class' => 'control-label']) !!}
                            <select id="branch_id" name="branch_id" class="form-control select2">
                                @if ($Branches->count())
                                    <option value="">Select Branches</option>
                                    @foreach($Branches as $Branche)
                                        <option value="{{ $Branche->id }}" @if($Sales->id==$Branche->id) {{'selected="selected"'}} @endif>{{ $Branche->name }}</option>

                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has('branch_id'))
                                <span class="help-block">
				{{ $errors->first('branch_id') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('employee_id')) has-error @endif">
                            {!! Form::label('employee_id', 'Employee', ['class' => 'control-label']) !!}
                            <select id="employee_id" name="employee_id" class="form-control select2">
                                @if ($Employees->count())
                                    <option value="">Select Employee</option>
                                    @foreach($Employees as $Employee)
                                        <option value="{{ $Employee->id }}" @if($Sales->id==$Employee->id) {{'selected="selected"'}} @endif>{{ $Employee->name }}</option>

                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has('employee_id'))
                                <span class="help-block">
				{{ $errors->first('employee_id') }}
				</span>
                            @endif
                        </div>
                    </div>
                    <div class="sales_row row">
                        <div class="col-xs-6 col-lg-6 form-group @if($errors->has('sal_date')) has-error @endif">
                            {!! Form::label('sal_date', 'Sale Date', ['class' => 'control-label']) !!}
                            {!! Form::text('sal_date', old('sal_date'), ['class' => 'form-control', 'placeholder' => '' ]) !!}
                            @if($errors->has('sal_date'))
                                <span class="help-block">
				{{ $errors->first('sal_date') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 col-lg-6 form-group @if($errors->has('valid_upto')) has-error @endif">
                            {!! Form::label('valid_upto', 'Valid Upto', ['class' => 'control-label']) !!}
                            {!! Form::text('valid_upto', old('valid_upto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            @if($errors->has('valid_upto'))
                                <span class="help-block">
				{{ $errors->first('valid_upto') }}
				</span>
                            @endif
                        </div>
                    </div>
                    <div class="sales_row row">
                        <div class="col-xs-6 col-lg-6 form-group @if($errors->has('products_description')) has-error @endif">
                            {!! Form::label('description', 'Description *', ['class' => 'control-label']) !!}
                            {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 1, 'cols' => 1]) !!}
                            @if($errors->has('description'))
                                <span class="help-block">
				{{ $errors->first('description') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 col-lg-6 form-group @if($errors->has('remarks')) has-error @endif">
                            {!! Form::label('remarks', 'Remarks *', ['class' => 'control-label']) !!}
                            {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 1, 'cols' => 1]) !!}
                            @if($errors->has('remarks'))
                                <span class="help-block">
				{{ $errors->first('remarks') }}
				</span>
                            @endif
                        </div>
                    </div>
                    <div class="sales_brands_deatial_row">
                        <div class="sales_row row">
                            <div class="col-xs-6 col-lg-3 form-group @if($errors->has('sup_id')) has-error @endif">
                                {!! Form::label('sup_id', 'Brands', ['class' => 'control-label']) !!}
                                <select id="sup_id" name="sup_id" class="form-control select2" disabled>
                                    @if ($Suppliers->count())
                                        <option value="">Select Brands</option>
                                        @foreach($Suppliers as $Supplier)
                                            <option value="{{ $Supplier->id }}" @if($Sales->id==$Supplier->id) {{'selected="selected"'}} @endif>{{$Supplier->sup_name}}</option>

                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('sup_id'))
                                    <span class="help-block">
					{{ $errors->first('sup_id') }}
					</span>
                                @endif
                            </div>
                        </div>
                        <div class="sales_row row">
                            <div id="productdetail_row_nooo" class="productmodel_row_nooo hide">
                                <div class="col-xs-12">
                                    <div class="controls_inn" style="margin-bottom: 10px; float: left; width: 100%; padding: 4px 8px; border: 1px solid rgb(221, 221, 221);">
                                        <label style="float: left; margin-top: 4px;" class="control-label">Sales Detail</label>
                                    </div>
                                </div>
                                <div class="productmodel_row_inn_h" style="float: left; width: 100%;">
                                    <div class="col-xs-2"> {!! Form::label('st_name', 'Product Name *', ['class' => 'control-label']) !!} </div>
                                    <div style="padding: 0 !important;" class="col-xs-10 00form-group">
                                        <div class="col-xs-2"> {!! Form::label('quantity', 'Avilable quantity *', ['class' => 'control-label']) !!} </div>
                                        <div class="col-xs-2"> {!! Form::label('st_name', 'Unit Price *', ['class' => 'control-label']) !!} </div>
                                        <div class="col-xs-2"> {!! Form::label('qunity', 'Quantity *', ['class' => 'control-label']) !!} </div>
                                        <div class="col-xs-2 amount_balance"> {!! Form::label('amount', 'Amount *', ['class' => 'control-label']) !!} </div>
                                        <div class="col-xs-2 text-center"> {!! Form::label('Action', 'Action', ['class' => 'control-label', 'style' => 'width: 100%;']) !!} </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="float:left;width: 100%;margin:4px 0;">
                            <div id="amount_balance_row" class="col-lg-12 form-group @if($errors->has('total_balance')) has-error @endif">
                                <div class="no-padding col-lg-6 text-right">
                                    {!! Form::label('total_balance', 'Total Balance:  *', ['class' => 'tax_gst_ship control-label','readonly'=>'readonly']) !!}
                                </div>
                                <div class="amount_balance col-lg-3">
                                    {!! Form::text('total_balance', old('0'), [ 'class' => 'form-control', 'placeholder' => '','readonly'=>'readonly']) !!}
                                    @if($errors->has('total_balance'))
                                        <span class="help-block">
						{{ $errors->first('total_balance') }}
						</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sales_row row">
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('tax')) has-error @endif">
                            {!! Form::label('tax', 'Tax (in %) *', ['class' => 'tax_gst_ship control-label']) !!}
                            {{--{{ Form::text('tax', $value = '' , $attributes = ['class' => 'tax_gst_ship form-control', 'id' => 'tax','placeholder' => '0','min'=>'1','max'=>'100']) }}--}}
                            {!! Form::text('tax', old('tax'), ['class' => 'tax_gst_ship form-control', 'readonly' => 'true','min'=>'1','max'=>'100']) !!}

                        @if($errors->has('tax'))
                                <span class="help-block">
				{{ $errors->first('tax') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('gst')) has-error @endif">
                            {!! Form::label('gst', 'GST (In %) *', ['class' => 'tax_gst_ship control-label']) !!}

                            {!! Form::text('gst', old('gst'), ['class' => 'tax_gst_ship form-control', 'readonly' => 'true','min'=>'1','max'=>'100']) !!}

                        @if($errors->has('gst'))
                                <span class="help-block">
				{{ $errors->first('gst') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('shipping_charges')) has-error @endif">
                            {!! Form::label('shipping_charges', 'Shipping Charges *', ['class' => 'tax_gst_ship control-label']) !!}
                            {!! Form::text('shipping_charges', old('shipping_charges'), ['class' => 'tax_gst_ship form-control', 'readonly' => 'true']) !!}


                        @if($errors->has('shipping_charges'))
                                <span class="help-block">
				{{ $errors->first('shipping_charges') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('total_amount')) has-error @endif">
                            {!! Form::label('total_amount', 'Total Amount *', ['class' => 'total_amount control-label']) !!}
                            {!! Form::text('total_amount', old('total_amount'), ['class' => 'form-control','placeholder'=>'','readonly'=>'readonly']) !!}
                            @if($errors->has('total_amount'))
                                <span class="help-block">
				{{ $errors->first('total_amount') }}
				</span>
                            @endif
                        </div>
                    </div>
                    <div class="sales_row row">
                        <div class="col-xs-6 col-lg-3 form-group @if($errors->has('discount')) has-error @endif">
                            {!! Form::label('discount', 'Discount (In %)', ['class' => 'control-label']) !!}
                            {{--{{ Form::text('discount', $value='' , $attributes = ['class' => 'form-control', 'id' => 'discount','placeholder' => '0']) }}--}}
                            {!! Form::text('discount', old('discount'), ['class' => 'tax_gst_ship form-control', 'readonly' => 'true','min'=>'1','max'=>'100']) !!}

                        @if($errors->has('discount'))
                                <span class="help-block">
				{{ $errors->first('discount') }}
				</span>
                            @endif
                        </div>
                        <div class="col-xs-6 form-group @if($errors->has('final_amount')) has-error @endif">
                            {!! Form::label('final_amount', 'Final Amount *', ['class' => 'control-label']) !!}
                            {!! Form::text('final_amount', old('final_amount'), ['class' => 'form-control','placeholder'=>'','readonly'=>'readonly']) !!}
                            @if($errors->has('final_amount'))
                                <span class="help-block">
				{{ $errors->first('final_amount') }}
				</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
        </div>
        {!! Form::close() !!}
        {{--@include('admin.entries.voucher.sale_voucher.entries_template')--}}
    </div>
@stop
@section('javascript')
    <script>
        //Brands Ajax Here...
        $(document).ready(function(){
            $(".productmodel_row_inn_h").hide();
            $("#amount_balance_roe").hide();

            $('#sup_id').on("change",function(){
                $('#st_name').select2();
                $(".productmodel_row").show();
                $(".productmodel_row_inn_h").show();
                $("#amount_balance_roe").show();

                var val = $(this).val();
                if(val != ''){
                    var url = '{{ url('admin/sales/produtsdetail') }}';
                    url = url +'/'+ val;
                    $.ajax({
                        type: "get",
                        url: url,
                        success:function(data){
                            //alert(data);
                            if(data) {
                                console.log(data);
                                $("#productdetail_row").append(data);
                            }
                            else{
                                alert('No data founded....!');
                            }
                        }
                    });
                    return false;
                }
            });
        });

        $(document).on('click', '.produtsdetail_inn_body .del', function(event) {
            var target = $(event.target);
            var row = target.closest('.produtsdetail_inn_body');
            row.remove();
        });


        // Stock Items Ajax Here...

        //$(window).find('.st_name').on('change', function(event) {
        $('.sales_row').on('change', '.st_name', function(event) {
            //alert('here');
            var st_val = $(this).val();
            var sup = $(this).attr('data-sup');
            //alert(st_val);
            if(st_val != ''){
                var url = '{{ url('admin/sales/produtscol') }}';
                url = url +'/'+ st_val;
                $.ajax({
                    type: "get",
                    url: url,
                    success:function(data){
                        //alert(data);
                        if(data) {
                            $("#produtscol_sup_"+sup).html(data);
                        }
                        else{
                            alert('No data founded....!');
                        }
                    }
                });
                return false;
            }
        });

        // Calculate Sum Items Here...
        function calculateSum() {
            var sum = 0;
            //iterate through each textboxes and add the values
            $(".amount_field").each(function() {
                //add only if the value is number
                if(!isNaN(this.value) && this.value.length!=0) {
                    sum += parseFloat(this.value);
                }

            });
            //.toFixed() method will roundoff the final sum to 2 decimal places
            $("#total_balance").val(sum.toFixed(2));
            //$("#total_amount").val(sum.toFixed(2));
            //$("#final_amount").val(sum.toFixed(2));
        }

        // Calculate Tax, GST and Shipping Charges sale Items Here...

        $(document).ready(function(){
            $("#tax").keyup(function() {
                var taxvalue = $(this).val();
                if(taxvalue > 100){
                    alert("Please enter less then 100 and greater 0");
                }else{
                    var total_balance = $("#total_balance").val();
                    var final_amount = $("#final_amount").val();
                    var result = parseFloat(parseInt(taxvalue, 10) * parseInt(total_balance, 10))/100 ;
                    var TaxTotal = parseFloat(result) + parseFloat( total_balance );
                    //The isNaN() function determines whether a value is an illegal number (Not-a-Number)
                    if( !isNaN(TaxTotal) ) {
                        var total_amount_var =  $("#total_amount" ).val(TaxTotal.toFixed(2));
                        //var final_amount_var =  $("#final_amount").val(TaxTotal.toFixed(2));
                    }else{
                        $("#total_balance").val();
                    }
                }

            });

            $("#gst").keyup(function() {
                var nan =  0;
                var gstvalue = $(this).val();
                var total_amount_tax = $("#total_amount").val();
                //var final_amount_tax = $("#final_amount").val();
                var gst_result = parseFloat(parseInt(gstvalue, 10) * parseInt(total_amount_tax, 10))/100 ;
                var GSTTotalAmount = parseFloat( gst_result ) + parseFloat( total_amount_tax );
                //The isNaN() function determines whether a value is an illegal number (Not-a-Number)
                if( !isNaN( GSTTotalAmount ) ) {
                    $( "#total_amount" ).val(GSTTotalAmount.toFixed(2));
                }else{
                    $("#total_balance").val();
                }

            });

            $("#shipping_charges").keyup(function() {
                var shipping_charges_value = $(this).val();
                var total_amount_gst = $("#total_amount").val();
                //var final_amount_tax = $("#final_amount").val();
                var Shp_TotalAmount = parseFloat( shipping_charges_value ) + parseFloat( total_amount_gst );
                //The isNaN() function determines whether a value is an illegal number (Not-a-Number)
                if( !isNaN( Shp_TotalAmount ) ) {
                    $( "#total_amount" ).val(Shp_TotalAmount.toFixed(2));
                    //$("#final_amount").val(Shp_TotalAmount.toFixed(2));
                }

            });


            // Calculate Discount Charges sale Items Here...

            $("#discount").keyup(function() {
                var discount_value = $(this).val();
                //var final_amount_new = $("#final_amount").val();
                var final_amount_new = $("#total_amount").val();
                var discount_result = parseFloat(parseInt(discount_value, 10) * parseInt(final_amount_new, 10))/100 ;
                //alert(discount_result);
                var discount_result = parseFloat( final_amount_new )- parseFloat( discount_result );
                //The isNaN() function determines whether a value is an illegal number (Not-a-Number)
                if( !isNaN( discount_result ) ) {
                    $( "#final_amount" ).val(discount_result.toFixed(2));
                }else{
                    $( "#final_amount" ).val(final_amount_new.toFixed(2));
                }

            });
        });



    </script>
    <!-- date-range-picker -->
    <script src="{{ url('adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    {{--<script src="{{ url('adminlte') }}/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>--}}
    <script src="{{ url('adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Select2 -->
    <script src="{{ url('adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('js/admin/sales/sales_create_modify.js') }}" type="text/javascript"></script>
@endsection