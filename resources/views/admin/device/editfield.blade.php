<div class="form-group col-md-3 @if($errors->has('branch_id')) has-error @endif" style = "float:left;">
    {{ Form::label('Branch Name*') }}
    {!! Form::select('branch_id', $branchList,old('branch_name'), ['class' => 'form-control select2']) !!}
</div>

<div class="form-group col-md-3 @if($errors->has('device_no')) has-error @endif" style = "float:left;">
    {!! Form::label('device_no', 'Device No*', ['class' => 'control-label']) !!}
    <input type="text" name="device_no" value="{{$devices->device_no}}" placeholder="Enter Device No" class="form-control" required unique>
</div>
<div class="form-group col-md-3 @if($errors->has('ip_address')) has-error @endif" style = "float:left;">
    {!! Form::label('ip_address', 'IP Address*', ['class' => 'control-label']) !!}
    <input type="text" name="ip_address" value="{{$devices->ip_address}}" placeholder="Enter IP Address" class="form-control" required>
</div>
<div class="form-group col-md-3 @if($errors->has('port')) has-error @endif" style = "float:left;">
    {!! Form::label('port', 'Port*', ['class' => 'control-label']) !!}
    <input type="number" name="port"  value="{{$devices->port}}" placeholder="Enter Port No" class="form-control" required>
</div>