@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Device List</h3>
                <a href="{{ route('admin.device.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Device</a>
          
        </div>


        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($devices) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Branch Name</th>
                    <th>Device Name</th>
                    <th>Port</th>
                    <th>IP Address</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($devices) > 0)
                    @foreach ($devices as $device)
                        <tr data-entry-id="{{ $device->id }}">
                            <td> {{$r}} </td>
                            <td>{{ \App\Helpers\Helper::branchIdToName($device->branch_id) }}</td>
                            <td>{{$device->device_no}}</td>
                            <td>{{$device->ip_address}}</td>
                            <td>{{$device->port}}</td>
                            <td>
                                <a href="{{ route('admin.device.edit',[$device->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                               
                                {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.device.destroy', $device->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                               
                            </td>
            


                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection