@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Tax Law Rates</h3>
            
                <a href="{{ route( 'tax-law-rate-add' ) }}" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Define New Tax Law Rates</a>
            
        </div>
        <!-- /.box-header -->
      
									<table style = " width:100%" id="example" class="table table-bordered table-striped {{ count($Law) > 0 ? 'datatable' : '' }}">
										<thead>
											<tr>
                                            <th>S.No</th>
                                            <th>Law Section</th>
                                            <th>Description</th>
                                            <th>Type</th>
                                            <th>For</th>
                                            <th>Percentage</th>
                                            <th>Amount Limit</th>
                                            <th>Action</th>
                                         
											</tr>
										</thead>
										<tbody>
											
										
									
                                            @if (count($Law) > 0)
                    @foreach ($Law as $settings)
                        <tr data-entry-id="{{ $settings->id }}">
                            <td>{{ $settings->law_rate_id}}</td>
                            <td>{{ $settings->law_section }}</td>
                            <td>{{ $settings->description }}</td>
                            <td>{{ $settings->type }}</td>
                            <td>{{ $settings->status }}</td>
                            <td>{{ $settings->percentage }}</td>
                            <td>{{ $settings->amount_limit }}</td>
                            <td style = "float:left;">
                                      

                                      <a href="{{ route('tax-law-rate-edit', [$settings->law_rate_id]) }}"
                                          class="btn btn-xs btn-info">Edit</a>
                                 

                                      {!! Form::open([
                                      'style' => 'display: inline-block;',
                                      'method' => 'DELETE',
                                      'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                      'route' => ['destroy-rate', $settings->law_rate_id],
                                      ]) !!}
                                      {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger'])
                                      !!}
                                      {!! Form::close() !!}  


                              </td> 
                            
                        </tr>
                    @endforeach

                @endif
										</tbody>
									</table>
								
	
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection