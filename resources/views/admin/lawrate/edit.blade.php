@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Tax Laws Rate</h3>
            

    <div class="box box-primary">
         <a  href="{{route('tax-law-rate')}}" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['tax-law-rate-update'], 'id' => 'validation-form']) !!}
        <div id="show" style="padding:5%;">
            <div id="box" class="box-body">
                <div class="form-group col-md-2 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Select Law*', ['class' => 'control-label']) !!}
                    <select name="name"   required="required"  class="form-control" >
                    <option value = "">Select Law</option>
                    @foreach($laws as $law)
                    @if($Branche->law_id == $law->law_id)
                            <option value = '{{ $law->law_id}}' selected>{{ $law->law_section}}</option>
                            @else

                            <option value = '{{ $law->law_id}}'>{{ $law->law_section}}</option>
                            @endif
                            @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'For*', ['class' => 'control-label']) !!}
                    <select name="status" value="{{$Branche->staus}}"  required="required"  class="form-control" >
                    <option value = "">Select For</option>
                  
                    @if($Branche->type=='AOP / Individual')
                    <option value = 'AOP / Individual ' selected>AOP / Individual</option>
                            <option value = 'Company'>Company</option>
                            @else
                            <option value = 'AOP / Individual '>AOP / Individual</option>
                            <option value = 'Company' selected>Company</option>
                            @endif
                        
                    </select>
                </div>
                <div class="form-group col-md-2 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Select Filer Type*', ['class' => 'control-label']) !!}
                    <select name="filer" value="{{$Branche->type}}"  required="required"  class="form-control" >
                    <option value = "">Select Filer</option>
               
               @if($Branche->type=='Filer')
                            <option value = 'Filer' selected>Filer</option>
                            <option value = 'Non-Filer'>Non-Filer</option>
                            @else
                            <option value = 'Filer' >Filer</option>
                            <option value = 'Non-Filer' selected>Non-Filer</option>
                            @endif
                        
                    </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;display:none;">
                    {!! Form::label('description', 'Law Rate In %', ['class' => 'control-label']) !!}
                     <input type="number" name="id" value="{{$Branche->law_rate_id}}" required="required" placeholder="%" class="form-control" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
                    {!! Form::label('description', 'Law Rate In %', ['class' => 'control-label']) !!}
                     <input type="number" name="per" value="{{$Branche->percentage}}" required="required" placeholder="%" class="form-control" />
                </div>
                <div class="form-group col-md-2 @if($errors->has('amount')) has-error @endif" style = "float:left;">
                    {!! Form::label('description', 'Amount Limit', ['class' => 'control-label']) !!}
                     <input type="number" name="amount" value="{{$Branche->amount_limit}}" required="required" placeholder="Enter Limit" class="form-control" />
                </div>
               
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
  
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

