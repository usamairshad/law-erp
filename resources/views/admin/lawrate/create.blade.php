@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Define Tax Laws Rate</h3>
            

    <div class="box box-primary">
         <a  href="{{route('tax-law-rate')}}" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['tax-law-rate-store'], 'id' => 'validation-form']) !!}
        <div id="show" style="padding:5%;">
            <div id="box" class="box-body">
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Select Law Section*', ['class' => 'control-label']) !!}
                    <select name="name"  required="required"  class="form-control" >
                    <option value = "">Select Law</option>
                    @foreach($laws as $law)
                            <option value = '{{ $law->law_id}}'>{{ $law->law_section}}</option>
                            @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'For*', ['class' => 'control-label']) !!}
                    <select name="status"  required="required"  class="form-control" >
                    <option value = "">Select For</option>
               
                            <option value = 'AOP / Individual '>AOP / Individual</option>
                            <option value = 'Company'>Company</option>
                        
                    </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Select Filer Type*', ['class' => 'control-label']) !!}
                    <select name="filer"  required="required"  class="form-control" >
                    <option value = "">Select Filer</option>
               
                            <option value = 'Filer'>Filer</option>
                            <option value = 'Non-Filer'>Non-Filer</option>
                        
                    </select>
                </div>
                
                <div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
                    {!! Form::label('description', 'Law Rate In %', ['class' => 'control-label']) !!}
                     <input type="number" name="per" required="required" placeholder="%" class="form-control" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('amount')) has-error @endif" style = "float:left;">
                    {!! Form::label('description', 'Amount Limit', ['class' => 'control-label']) !!}
                     <input type="number" name="amount" required="required" placeholder="Enter Limit" class="form-control" />
                </div>
               
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
  
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

