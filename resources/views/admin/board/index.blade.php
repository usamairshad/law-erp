@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Board Programs</h3>

            <a href="{{ route('admin.board-program') }}" style = "float:right;" class="btn btn-success pull-right">Create Board Programs</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($boards) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>View Programs</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($boards) > 0)
                        @foreach ($boards as $board)
                            <tr data-entry-id="{{ $board->id }}">
                                <td>{{ $board->name }}</td>
                                <td><a href="{{ route('admin.view-board-program', [$board->id]) }}"><button
                                            class="btn btn-primary" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;">View Programs</button></a></td>
                                <td>
                                    <!-- a href="{{ route('admin.view-board-program', [$board->id]) }}"><button
                                            class="btn btn-primary" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;">Edit</button></a -->
                                    <a data-toggle="modal" data-target="#exampleModal1{{ $board->id }}"><button
                                            class="btn btn-primary" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;">Add More Program</button></a>


                                </td>

                                <div class="modal fade" id="exampleModal1{{ $board->id }}" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                {!! Form::open(['route' => 'admin.existing-board-program', 'method' =>
                                                'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data'])
                                                !!}
                                                <input type="hidden" name="board_id" value="{{ $board->id }}">
                                                <div class="form-group">
                                                    <label for="scheduled_time" class="col-form-label">Board</label>
                                                    <input type="text"
                                                        value="{{ \App\Helpers\Helper::boardIdToName($board->id) }}"
                                                        readonly class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('program_name', 'Program Name*', ['class' =>
                                                    'control-label']) !!}
                                                    <select name="program_id" class="form-control">
                                                        @foreach ($programs as $program)
                                                            <option value="{{ $program['id'] }}">
                                                                {{ $program['name'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('description', 'Description', ['class' =>
                                                    'control-label']) !!}
                                                    <input type="text" name="description" required="required"
                                                        placeholder="Enter Description" class="form-control" />
                                                </div>
                                                <div style="margin-top: 20px">

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>

                            </tr>

                        @endforeach

                    @else
                        <tr>
                            <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>
@endsection
