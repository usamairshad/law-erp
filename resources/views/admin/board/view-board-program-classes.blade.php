@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Board Program Class</h3>
            <a  href="{{route('admin.boards-program-classes')}}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($board_program_classes) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Board</th>
                    <th>Program</th>
                    <th>Class</th>
                </tr>
                </thead>

                <tbody>
                @if (count($board_program_classes) > 0)

                    @foreach ($board_program_classes as $program)
                        <tr data-entry-id="{{ $program['id'] }}">
                            <td>{{ \App\Helpers\Helper::boardIdToName($program->board_id) }}</td>
                            <td>{{ \App\Helpers\Helper::programIdToName($program->program_id) }}</td>
                    
                            <td>{{ \App\Helpers\Helper::classIdToName($program->class_id) }}</td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection