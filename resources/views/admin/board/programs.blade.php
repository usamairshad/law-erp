@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Programs</h3>

    <div class="box box-primary">
        <div class="box-header with-border">
            <a  href="{{route('admin.boards.create')}}" style = "float:right;" class="btn btn-success pull-right">Create</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($programs) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Compulsory Courses</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @if (count($programs) > 0)

                    @foreach ($programs as $program)
                        <tr data-entry-id="{{ $program['id'] }}">
                            <td>{{ $program->id}}</td>
                            <td>{{ $program->name}}</td>
                            <td>{{ $program->description }}</td>
                            <td><a  data-id="{{$program->id}}" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-xs btn-primary academics-courses" data-toggle="modal" data-target="#view-modal{{$program->id}}"><span>View Courses</span></a></td>
                            <td> <a href = "compulsory-course/{{ $program->id}}"  data-target="#exampleModal1{{$program->id}}"><button style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-primary">Add Compulsory Course</button></a>
                                <!--a href="{{route('admin.boards.edit',[$program->id])}}"><button style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-primary">Edit</button></a -->
                            
                            </td>

                   
                            <div class="modal fade" id="view-modal{{$program->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Program Compulsory Courses</h2>
                                      
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                          <div id="days_{{$program->id}}">
                                              
                                          </div>
                           
                                        </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        
                                    </div>
                                  </div>
                                   
                                </div>
                            </div>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable({
               "order": [[ 0, "asc" ]]
            });
            selectRefresh();
        });
     
        function selectRefresh() {
            $(".courses").select2({

            });
        }
        $(function() {
           $(".academics-courses").on('click', function () {
  
                   var id=  $(this).data('id');

                   console.log(id+'-----------------')

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('admin.load-program-compulsory-courses') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            program_id: id
                        },
                        success: function (response) {
                            $.each( response, function(k, v) {
                                if(k == 0)
                                {
                                    $("#days_"+v.program_id).empty();
                                }
                                $("#days_"+v.program_id).append('<input readonly type="text" class="form-control" value="'+v.course_id+'"><br/>');
                            });
                        }
                    });
            })
        });
    </script>
@endsection