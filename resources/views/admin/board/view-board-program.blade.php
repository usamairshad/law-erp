@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Board Programs Details</h3>
            <a  href="{{route('admin.boards.show',1)}}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($board_programs) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                   
                </tr>
                </thead>

                <tbody>
                @if (count($board_programs) > 0)

                    @foreach ($board_programs as $program)
                        <tr data-entry-id="{{ $program->id}}">
                            <td>{{ $program->name }}</td>
                             <td>{{ $program->description }}</td>
                    
                          

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection