@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Ledgers</h3>
            @if(Gate::check('erp_ledgers_create'))
                <a href="{{ route('admin.ledgers.create') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Add New Ledger</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th style="text-align: right;">Opening Balance</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if (count($Ledgers) > 0)
                    @foreach ($Ledgers as $id => $data)
                        @if ($id == 0) @continue; @endif
                        {{--@if(isset($data['level']))--}}
                        {{--@if($data['level']==1)--}}
                        {{--@php ($bgcolor= '#83b7f7')--}}
                        {{--@elseif($data['level']==2)--}}
                        {{--@php ($bgcolor = '#aaf783')--}}
                        {{--@elseif($data['level']==3)--}}
                        {{--@php ($bgcolor = '#f28a8a')--}}
                        {{--@elseif($data['level']==4)--}}
                        {{--@php ($bgcolor = '#75f0d5')--}}
                        {{--@elseif($data['level']==5)--}}
                        {{--@php ($bgcolor = '#fa93fa')--}}
                        {{--@else--}}
                        {{--@php ($bgcolor = '#e7fa93')--}}
                        {{--@endif--}}
                        {{--@else--}}
                        {{--@php ($bgcolor = 'white')--}}
                        {{--@endif--}}

                        <tr>
                            <td>
                                @if ($id < 0)
                                    <?php echo $data['number'] ?>
                                @else
                                    <?php echo $data['number'] ?>
                                @endif
                            </td>
                            <td>


                                @if ($id < 0)
                                    <?php echo $data['name'] ?>
                                @else
                                    {{--@if(Gate::check('ledgers_edit'))--}}
                                    <a href="{{ route('admin.ledgers.edit',[$id]) }}"> <?php echo $data['name'] ?> </a>
                                    {{--@endif--}}
                                @endif
                                {{--{{ $data['name'] }}--}}
                            </td>
                            <td>@if ($id < 0) Group @else Ledger @endif</td>
                            <td align="right">@if ($id < 0) N/A @else {{ $data['opening_balance'] }} @endif</td>
{{--                            <td align="right">@if ($id < 0) N/A @else {{ $DefaultCurrency->symbol . ' ' . $Currency::curreny_format($data['opening_balance']) }} @endif</td>--}}
                            <td>
                                @if ($id > 0)
                                    {{--@if(Gate::check('ledgers_edit'))--}}
                                    <a href="{{ route('admin.ledgers.edit',[$id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                    {{--@endif--}}

                                    {{--@if(Gate::check('ledgers_destroy'))--}}
                                    {!! Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                'route' => ['admin.ledgers.destroy', $id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    {{--@endif--}}
                                @else
                                    N/A
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.ledgers.mass_destroy') }}';
    </script>
@endsection