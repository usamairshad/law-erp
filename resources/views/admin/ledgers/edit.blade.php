@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Ledgers</h3>
            <a href="{{ route('admin.ledgers.index') }}" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Ledger, ['method' => 'PUT', 'route' => ['admin.ledgers.update', $Ledger->id]]) !!}
            <div class="box-body" style ="margin-top:50px;">
                @include('admin.ledgers.fields')
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger ']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/admin/ledgers/create_modify.js') }}" type="text/javascript"></script>
@endsection