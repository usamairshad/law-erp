@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Ledgers</h3>
            <a href="{{ route('admin.ledgers.index') }}" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.ledgers.store'], 'id' => 'validation-form']) !!}
            <div class="box-body" style = "margin-top:60px">
                @include('admin.ledgers.fields')
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <!-- Select2 -->
    {{--<script src="{{ url('adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>--}}
    <script src="{{ url('public/js/admin/ledgers/create_modify.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $("#branch_select").select2();
        $("#branch_selector_checkbox").click(function(){
            if($("#branch_selector_checkbox").is(':checked') ){
                $("#branch_select > option").prop("selected","selected");
                $("#branch_select").trigger("change");
            }else{
                $("#branch_select > option").removeAttr("selected");
                $("#branch_select").trigger("change");
                $('#branch_select').val(null).trigger('change');

            }
        });
    </script>
@endsection