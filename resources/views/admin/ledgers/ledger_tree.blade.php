@extends('layouts.app')
@section('stylesheet')

  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('public') }}/dist/css/adminlte.min.css">
    
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Chart Of Accounts</h3>
                           
<style type="text/css">
    .menu .accordion-heading {  position: relative; }
    .menu .accordion-heading .edit {
        position: absolute;
        top: 8px;
        right: 30px;
    }
    .accordion{
        padding: 50px;
    }
    ul, ol {
    padding: 0;
    margin: 0 0 10px 0px !important;
}
    .menu .area { border-left: 4px solid #f38787; }
    .menu .equipamento { border-left: 4px solid #65c465; }
    .menu .ponto { border-left: 4px solid #98b3fa; }
    .menu .collapse.in { overflow: visible; }
    .accordion-inner .nav>li{ border-bottom: 1px solid #ecf1ec;}
    .collapse:not(.show)
    {
        display:block;
    }
    .main-profile-menu.nav-link {
        display:none;
    }
</style>
<!------ Include the above in your HEAD tag ---------->
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="col-md-12">
                <div class="menu">
                    <div class="accordion">
                        <div class="accordion" id="accordionExample">
  
                    
                        @foreach($Groups as $group)
                            @if($group->parent_id==0)
                            <div class="accordion-group">
                      
                                <div class="accordion-heading area">
                                    <a class="accordion-toggle" data-toggle="collapse" href=
                                    "#{{$group->number}}" onclick="get_group_ledger(this, {{$group->id}}, {{$group->level}});" val="{{$group->number}}">{{$group->number}}-{{$group->name}}</a>
                                </div>
                         
                                <div class="accordion-body collapse" id="{{$group->number}}">
                                    <div class="accordion-inner">
                                        <div class="accordion" id="equipamento1">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                    </div>
                </div>
            </div>
        <div class="panel-body pad table-responsive">
        </div>
    </div>
        </div>
        </div>
@stop
@section('javascript')

    <script src="{{ url('public/js/admin/ledgers_tree/create_modify.js') }}" type="text/javascript"></script>
    

@endsection