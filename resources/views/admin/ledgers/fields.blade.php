<div class="form-group col-md-6 @if($errors->has('name')) has-error @endif" style = "float:left;">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>250]) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>
 <div class="col-md-6 form-group" style = "float:left;">
    {!! Form::label('company_id', 'Company Name ', ['class' => 'control-label']) !!}
    {!! Form::select('company_id', $company->prepend('Select Company', ''),old('company_id'), ['class' => 'form-control','required' => 'required',]) !!}
    <span id="company_id_handler"></span>
    @if($errors->has('company_id'))
            <span class="help-block">
                    {{ $errors->first('company_id') }}
            </span>
    @endif
</div> 
<div class="col-md-6 form-group" style = "float:left;">
    {!! Form::label('city_id', 'City Name ', ['class' => 'control-label']) !!}
    {!! Form::select('city_id', $city->prepend('Select City', ''),old('city_id'), ['class' => 'form-control','required' => 'required']) !!}
    <span id="city_id_handler"></span>
    @if($errors->has('city_id'))
            <span class="help-block">
                    {{ $errors->first('city_id') }}
            </span>
    @endif
</div> 

<div class="col-md-6 form-group @if($errors->has('branch_id')) has-error @endif"  style = "float:left;">

        {!! Form::label('branch', 'Branches', ['class' => 'control-label', 'id' => 'branch_selector']) !!}
        {!! Form::select('branches_id[]', $Branches, null,[ 'id' => 'branch_select' , 'class' => 'form-control select2', 'required' => 'required', 'multiple' => 'multiple']) !!}
</div>



<div class=" col-md-6 form-group @if($errors->has('opening_balance')) has-error @endif" style = "float:left;margin-left:1px;">
    {!! Form::label('opening_balance', 'Opening Balance', ['class' => 'control-label']) !!}
    {!! Form::text('opening_balance', old('opening_balance'), ['class' => 'form-control' , 'pattern'=> '[0-9]+','maxlength'=>10]) !!}
    @if($errors->has('opening_balance'))
        <span class="help-block">
            {{ $errors->first('opening_balance') }}
        </span>
    @endif
</div>
<div class=" col-md-6 form-group @if($errors->has('closing_balance')) has-error @endif" style = "float:left;margin-left:-1px;">
    {!! Form::label('closing_balance', 'Closing Balance', ['class' => 'control-label']) !!}
    {!! Form::number('closing_balance', old('closing_balance'), ['readonly' => true, 'class' => 'form-control']) !!}
    @if($errors->has('closing_balance'))
        <span class="help-block">
            {{ $errors->first('closing_balance') }}
        </span>
    @endif
</div>
<div class="col-xs-12 form-group">
        {!! Form::label('parent_id', 'Parent Group', ['class' => 'control-label']) !!}
        <select name="group_id" id="group_id" class="form-control select2" style="width: 100%;">
            <option value=""> Select a Parent Group </option>
                {!! $Groups !!}
        </select>
        <span id="parent_id_handler"></span>
        @if($errors->has('parent_id'))
                <span class="help-block">
                        {{ $errors->first('parent_id') }}
                </span>
        @endif
</div>


<div class="row"></div>
