@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">Suppliers</h3>
    @if(Gate::check('suppliers_create'))
    <p>
        <a href="{{ route('admin.suppliers.create') }}" class="btn btn-success pull-right">@lang('global.app_add_new')</a>
    </p>
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($Suppliers) > 0 ? 'datatable' : '' }} dt-select" id="sup-table">
                <thead>
                    <tr>
                        <th>S: No</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Contact</th>
                        <th>Email</th>
                        <th>Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('#sup-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.supliers.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'sup_name', name: 'sup_name' },
                    { data: 'sup_address', name: 'sup_address' },
                    { data: 'sup_contact', name: 'sup_contact' },
                    { data: 'sup_email', name: 'sup_email' },
                    { data: 'sup_type', name: 'sup_type' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},

                ]
            });
        });
    </script>
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.suppliers.mass_destroy') }}';
    </script>
@endsection