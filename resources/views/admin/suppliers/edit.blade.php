@extends('layouts.app')
@section('content')
    <h3 class="page-title">@lang('Update Suppliers')</h3>
    <a href="{{ route('admin.suppliers.index') }}" class="btn btn-success pull-right">Back</a>
    {!! Form::model($Suppliers, ['method' => 'PUT', 'route' => ['admin.suppliers.update', $Suppliers->id]]) !!}
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_edit')
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="brands_row row">
                        <div class="col-xs-6 form-group">
                            {!! Form::label('sup_name', 'Name *', ['class' => 'control-label']) !!}
                            {!! Form::text('sup_name', old('sup_name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'100',  'minlength'=>'3', 'required']) !!}
                        </div>
                        <div class="col-xs-3 form-group">
                            {!! Form::label('sup_address', 'Address', ['class' => 'control-label']) !!}
                            {!! Form::text('sup_address', old('sup_address'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'100']) !!}
                        </div>

                        <div class="col-xs-3 form-group">
                            {!! Form::label('sup_country', 'Country', ['class' => 'control-label']) !!}
                            {!! Form::text('sup_country', old('sup_country'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'30','pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
                        </div>
                        <div class="col-xs-3 form-group">
                            {!! Form::label('sup_city', 'City', ['class' => 'control-label']) !!}
                            {!! Form::text('sup_city', old('sup_city'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'30','pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
                        </div>

                        <div class="col-xs-3 form-group">
                            {!! Form::label('sup_contact', 'Contact', ['class' => 'control-label']) !!}
                            {!! Form::number('sup_contact', old('sup_contact'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'11','min'=>'0']) !!}
                        </div>
                        <div class="col-xs-3 form-group">
                            {!! Form::label('sup_email', 'Email', ['class' => 'control-label']) !!}
                            {!! Form::email('sup_email', old('sup_email'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'25','pattern'=>"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$", 'title'=>"please enter valid email [test@test.com]." ]) !!}
                        </div>

                        <div class="col-xs-3 form-group">
                            {!! Form::label('sup_mobile', 'Mobile', ['class' => 'control-label']) !!}
                            {!! Form::number('sup_mobile', old('sup_mobile'), ['class' => 'form-control', 'placeholder'=>'','maxlength'=>'11', 'pattern'=> '[0-9]+'])!!}
                        </div>
                        <div class="col-xs-3 form-group">
                            {!! Form::label('sup_website', 'Website', ['class' => 'control-label']) !!}
                            {!! Form::text('sup_website', old('sup_website'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'30','pattern' => '{5,}']) !!}
                        </div>

                        <div class="col-xs-3 form-group">
                            {!! Form::label('sup_bank_account', 'Bank Account', ['class' => 'control-label']) !!}
                            {!! Form::number('sup_bank_account', old('sup_bank_account'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'20']) !!}
                        </div>
                        <div class="col-xs-6 form-group">
                            {!! Form::label('sup_type', 'Type', ['class' => 'control-label']) !!}
                            {!!  Form::select('sup_type', [
                            '' => 'Select type',
                            '1' => 'Local supplier',
                            '2' => 'Foreign supplier'],
                            null,
                            ['class' => 'form-control', 'required'])
                            !!}
                        </div>
                    </div>
                    <div class="brands_row">
                        {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('javascript')
    <script src="{{ url('js/admin/suppliers/suppliers_create_modify.js') }}" type="text/javascript"></script>
@endsection
<style>
    .brands_row{
        float:left;
        width: 100%;
    }
</style>