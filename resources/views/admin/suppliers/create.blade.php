@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Suppliers</h1>
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create</h3>
            <a href="{{ route('admin.suppliers.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.suppliers.store'], 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="brands_row row">
                            <div class="col-xs-6 form-group @if($errors->has('sup_name')) has-error @endif">
                                {!! Form::label('sup_name', 'Name *', ['class' => 'control-label']) !!}
                                {!! Form::text('sup_name', old('sup_name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'100', 'minlength'=>'3']) !!}
                                @if($errors->has('sup_name'))
                                    <span class="help-block">
                                    {{ $errors->first('sup_name') }}
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-6 form-group @if($errors->has('sup_address')) has-error @endif">
                                {!! Form::label('sup_address', 'Address', ['class' => 'control-label']) !!}
                                {!! Form::text('sup_address', old('sup_address'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'100']) !!}
                                @if($errors->has('sup_address'))
                                    <span class="help-block">
                                        {{ $errors->first('sup_address') }}
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="brands_row row">
                            <div class="col-xs-6 form-group @if($errors->has('sup_email')) has-error @endif">
                                {!! Form::label('sup_email', 'Email', ['class' => 'control-label']) !!}
                                {!! Form::email('sup_email', old('sup_email'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'25','pattern'=>"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$", 'title'=>"please enter valid email [test@test.com]." ]) !!}
                                @if($errors->has('sup_email'))
                                    <span class="help-block">
                                        {{ $errors->first('sup_email') }}
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-6 form-group @if($errors->has('sup_mobile')) has-error @endif">
                                {!! Form::label('sup_mobile', 'Mobile', ['class' => 'control-label']) !!}
                                {!! Form::number('sup_mobile', old('sup_mobile'), ['class' => 'form-control', 'placeholder'=>'','maxlength'=>'11', 'pattern'=> '[0-9]+'])!!}
                                @if($errors->has('sup_mobile'))
                                    <span class="help-block">
                                        {{ $errors->first('sup_mobile') }}
							        </span>
                                @endif
                            </div>

                        </div>


                        <div class="brands_row row">
                            <div class="col-xs-6 form-group @if($errors->has('sup_country')) has-error @endif">
                                {!! Form::label('sup_country', 'Country', ['class' => 'control-label']) !!}
                                {!! Form::text('sup_country', old('sup_country'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'30','pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
                                @if($errors->has('sup_country'))
                                    <span class="help-block">
                                        {{ $errors->first('sup_country') }}
							        </span>
                                @endif
                            </div>
                            <div class="col-xs-6 form-group @if($errors->has('sup_city')) has-error @endif">
                                {!! Form::label('sup_city', 'City', ['class' => 'control-label']) !!}
                                {!! Form::text('sup_city', old('sup_city'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'30','pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
                                @if($errors->has('sup_city'))
                                    <span class="help-block">
                                        {{ $errors->first('sup_city') }}
							        </span>
                                @endif
                            </div>
                        </div>
                        <div class="brands_row row">
                            <div class="col-xs-6 form-group @if($errors->has('sup_contact')) has-error @endif">
                                {!! Form::label('sup_contact', 'Contact', ['class' => 'control-label']) !!}
                                {!! Form::number('sup_contact', old('sup_contact'), ['class' => 'form-control', 'placeholder' => '' ,'maxlength'=>'11','min'=>'0']) !!}
                                @if($errors->has('sup_contact'))
                                    <span class="help-block">
							            {{ $errors->first('sup_contact') }}
							        </span>
                                @endif
                            </div>


                            <div class="col-xs-6 form-group">
                                {!! Form::label('sup_website', 'Website', ['class' => 'control-label']) !!}
                                {!! Form::text('sup_website', old('sup_website'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'30','minlength'=>'5']) !!}
                            </div>

                        </div>
                        <div class="brands_row row">
                            <div class="col-xs-6 form-group">
                                {!! Form::label('sup_bank_account', 'Bank Account', ['class' => 'control-label']) !!}
                                {!! Form::number('sup_bank_account', old('sup_bank_account'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'20']) !!}
                            </div>
                            <div class="col-xs-6 form-group">
                                {!! Form::label('sup_type', 'Type*', ['class' => 'control-label']) !!}
                                {!!  Form::select('sup_type', [
                                '' => 'select type',
                                '1' => 'Local supplier',
                                '2' => 'Foreign supplier'],
                                null,
                                ['class' => 'form-control','required'])
                                !!}
                            </div>
                        </div>
                    </div>
                    <div class="brands_row">
                        <div class="col-xs-6 form-group">
                            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    </div>
@stop
@section('javascript')
    <script src="{{ url('public/js/admin/suppliers/suppliers_create_modify.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
//            $(".btn-danger").click(function () {
//                $(".btn-danger").attr("disabled", true);
//                $("#validation-form").submit();
//                return true;
//            });
        });
    </script>
@endsection

<style>
    .brands_row{
        float:left;
        width: 100%;
    }
</style>