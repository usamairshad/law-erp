@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Items & Stock</h1>
    </section>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.stockitems.filterreport'], 'id' => 'validation-form']) !!}
        {{csrf_field()}}
        <input type="hidden" name="name" value="abc">
        <input type="hidden" name="selectedDatabanks" value="" id="selectedDatabanks">
        <div class="form-group col-md-3 @if($errors->has('sup_id')) has-error @endif" id="from_div">
            {!! Form::label('sup_id', 'Select Brand', ['class' => 'control-label']) !!}
            {!! Form::select('sup_id', $SuppliersModel , old('sup_id'),
            ['class' => 'form-control select2'  ,'id' => 'sup_id' , 'required' => 'true']) !!}
        </div>
        <div class="form-group col-md-3 @if($errors->has('cat_id')) has-error @endif" id="from_div">
            {!! Form::label('cat_id', 'Select Category', ['class' => 'control-label']) !!}
            {!! Form::select('cat_id', $StockCategoryModel , old('cat_id'),
            ['class' => 'form-control select2'  ,'id' => 'cat_id' , 'required' => 'true']) !!}
        </div>
        <div class="form-group col-md-3 @if($errors->has('product_id')) has-error @endif" id="from_div">
            {!! Form::label('product_id', 'Select Product', ['class' => 'control-label']) !!}
            {!! Form::select('product_id', $ProductModel , old('product_id'),
            ['class' => 'form-control select2'  ,'id' => 'product_id' , 'required' => 'true']) !!}
        </div>
        <div class="form-group col-md-2 @if($errors->has('search_button')) has-error @endif" >
            {!! Form::label('search_button', ' ', ['class' => 'control-label']) !!}
            {!! Form::submit('Load', ['class' => 'btn btn-primary globalSaveBtn', 'style'=>'margin-top:25px']) !!}
        </div>
    {!! Form::close() !!}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            {{--@if(Gate::check('stockitems_create'))--}}
                {{--<a href="{{ route('admin.stockitems.create') }}" class="btn btn-success pull-right">Add New Items & Stock</a>--}}
            {{--@endif--}}
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="">S No:</th>
                    {{-- <th>Product_id</th> --}}
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Stock Value</th>
                </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                        $totalStock = 0;
                        $totalQty = 0;
                    @endphp
                    @foreach ($StockItems as $item)
                    <?php 
                        $totalStock = $totalStock + $item->stockvalue; 
                        $totalQty = $totalQty + $item->QTy;
                    ?>
                        <tr>
                            <td>{{$i++}}</td>
                            {{-- <td>{{$item->st_name}}</td> --}}
                            <td>{{$item->products_short_name}}</td>
                            <td>{{$item->QTy}}</td>
                            <td>{{$item->stockvalue}}</td>
                        </tr>
                    @endforeach
                    <tr style="font-size:15px; border:2px solid #3c8dbc; ">
                        <td colspan="2"><b>TOTAL</b></td>
                        <td ><b>{{$totalQty}}</b></td>
                        <td ><b>{{$totalStock}}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/stockitems/list.js') }}" type="text/javascript"></script>

@endsection

