@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('Update Stock Items')</h3>
    
    {!! Form::model($StockItems, ['method' => 'PUT', 'id' => 'validation-form','route' => ['admin.stockitems.update', $StockItems->id]]) !!}
    {!! Form::hidden('ledger_id', $StockItems->ledger_id, ['id' => 'ledger_id','class' => 'form-control', 'placeholder' => '']) !!}
    {!! Form::hidden('ledger_product', $StockItems->st_name, ['id' => 'ledger_product','class' => 'form-control', 'placeholder' => '']) !!}
    {!! Form::hidden('group_id', $StockItems->group_id, ['id' => 'group_id','class' => 'form-control', 'placeholder' => '']) !!}


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_edit')
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-3 form-group">
                        {!! Form::label('branch_id', 'Branch', ['class' => 'control-label']) !!}
                        <select id="branch_id" name="branch_id" class="form-control select2" disabled>
                            @if ($Branches->count())
                                <option value="">Select a Branch</option>
                                @foreach($Branches as $key =>$value)
                                    <option value="{{$key}}"{{$StockItems->branch_id == $key ? 'selected="selected"' : ''}}>{{ $value }}</option>
                                @endforeach
                            @endif
                        </select>
                        <span id="branch_id_handler"></span>
                        @if($errors->has('branch_id'))
                            <span class="help-block">
                            {{ $errors->first('branch_id') }}
                            </span>
                        @endif
                    </div>
                    <div id="selecthide" class="col-xs-3 form-group @if($errors->has('product_id')) has-error @endif">
                        {!! Form::label('st_name', 'Product *', ['class' => 'control-label']) !!}
                        <select id="st_name" name="st_name" class="form-control select2" disabled>
                            @if ($results->count())
                                <option value="">Select Product</option>
                                @foreach($results as $result)
                                    <option value="{{ $result->id }}"{{$StockItems->st_name == $result->id? 'selected="selected"' : ''}}>{{ $result->products_name  }} - {{$result->products_model}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('st_name'))
                            <span class="help-block">
                                {{ $errors->first('st_name') }}
                                </span>
                        @endif

                    </div>

                        <div class="col-xs-3 form-group @if($errors->has('st_unit_price')) has-error @endif">
                            {!! Form::label('st_unit_price', 'Unit Price', ['class' => 'control-label']) !!}
                            {!! Form::text('st_unit_price', old('st_unit_price'), ['id' => 'st_unit_price','class' => 'form-control', 'placeholder' => '']) !!}
                            @if($errors->has('st_unit_price'))
                                <span class="help-block">
							{{ $errors->first('st_unit_price') }}
							</span>
                            @endif
                        </div>
                        <div class="col-xs-3 form-group @if($errors->has('quantity')) has-error @endif">
                            {!! Form::label('quantity', 'Quantity', ['class' => 'control-label']) !!}
                            {!! Form::text('quantity', old('quantity'), ['id' => 'quantity','class' => 'form-control', 'placeholder' => '']) !!}
                            @if($errors->has('quantity'))
                                <span class="help-block">
							{{ $errors->first('quantity') }}
							</span>
                            @endif
                        </div>


                <div id="stockitems_submit" class="col-xs-12">
                    <div class="col-xs-6">
                        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/stockitems/stockitems_update_modify.js') }}" type="text/javascript"></script>

    {{--<script>--}}
        {{--$( document ).ready(function() {--}}
            {{--$('#cat_id').on('change', function() {--}}
                {{--var fieldvalue = $(this).val();--}}
                {{--var sup_id = $('#sup_id').val();--}}
                {{--if(fieldvalue != ''){--}}
                    {{--var url = '{{ url('admin/stockitems/performa') }}';--}}
                    {{--url = url +'/'+ fieldvalue+'/'+sup_id;--}}
                    {{--$.ajax({--}}
                        {{--type: "get",--}}
                        {{--url: url,--}}
                        {{--success:function(data){--}}
                            {{--$("#selecthide").hide();--}}
                            {{--//alert(data);--}}
                            {{--$("#prodcut").html(data);--}}
                            {{--$('#st_name').select2();--}}
                        {{--}--}}
                    {{--});--}}
                    {{--$('#prodcut').show();--}}
                {{--}else {--}}
                    {{--$('#prodcut').hide();--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection