@extends('layouts.app')
@section('stylesheet')

@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Items & Stock</h1>
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create</h3>
            <a href="{{ route('admin.stockitems.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.stockitems.store'], 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="col-xs-5 form-group">
                            {!! Form::label('parent_id', 'Chart Of Account', ['class' => 'control-label']) !!}
                            <select name="parent_id" id="parent_id" class="form-control select2" style="width: 100%;" required>
                                <option value=""> Select Chart Of Account </option>
                                {!! $Groups !!}
                            </select>
                            <span id="parent_id_handler"></span>
                            @if($errors->has('parent_id'))
                                <span class="help-block">
                            {{ $errors->first('parent_id') }}
                            </span>
                            @endif
                        </div>
                        <div class="col-xs-5 form-group">
                            {!! Form::label('branch_id', 'Branch', ['class' => 'control-label']) !!}
                            <select id="branch_id" name="branch_id" class="form-control select2" required>
                                @if ($Branches->count())
                                    <option value="">Select a Branch</option>
                                    @foreach($Branches as $key =>$value)
                                        <option value="{{$key}}">{{ $value }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <span id="branch_id_handler"></span>
                            @if($errors->has('branch_id'))
                                <span class="help-block">
                            {{ $errors->first('branch_id') }}
                            </span>
                            @endif
                        </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="about_featured">
                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default wow fadInLeft">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#11">
                                                <span class="fa fa-check-square-o"></span><b>Product List</b>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="11" class="panel-collapse collapse in">


                                        <div class="panel-body pad table-responsive">
                                            <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>R</u>ow</button>
                                            <table class="table table-bordered table-striped" id="users-table">
                                                <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>UOM</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price</th>
                                                    <th>Stock Value</th>
                                                    <th>Action</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php  $counter=0 ?>
                                                <tr id="line_item-1">
                                                    <td width="30%"><div class="form-group  @if($errors->has('product_id')) has-error @endif">

                                                            {!! Form::select('InventoryItems[product_id][1]',array(), old('product_id'), ['id' => 'InventoryItems-product_id-1','class' => 'form-control description-data-ajax select2 dublicate_product', 'style'=>'width:100%;' , 'required' => 'true']) !!}

                                                        @if($errors->has('product_id'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('product_id') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group  @if($errors->has('line_unit_name')) has-error @endif">

                                                            {!! Form::select('InventoryItems[unit_name][1]',$UnitsModel, old('unit_name'), ['id' => 'InventoryItems-unit_name_id-1','class' => 'form-control', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                                                            @if($errors->has('line_unit_name'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('line_unit_name') }}
                                                                        </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td> <div class="form-group  @if($errors->has('qunity')) has-error @endif">

                                                            {!! Form::number('InventoryItems[qunity][1]', old('qunity'), ['id' => 'InventoryItems-qunity_id-1','class' => 'form-control qty' ,'data-row-id'=>'1', 'required' => 'true','min'=>'1']) !!}
                                                            @if($errors->has('line_qunity'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('line_qunity') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group  @if($errors->has('p_amount')) has-error @endif">

                                                            {!! Form::number('InventoryItems[p_amount][1]', old('p_amount'), ['id' => 'InventoryItems-p_amount_id-1','class' => 'form-control test' ,'data-row-id'=>'1', 'required' => 'true', 'min'=>'1']) !!}
                                                            @if($errors->has('p_amount'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('p_amount') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group  @if($errors->has('amount')) has-error @endif">

                                                            {!! Form::number('InventoryItems[amount][1]', old('amount'), ['id' => 'InventoryItems-amount_id-1','class' => 'form-control' , 'required' => 'true', 'min'=>'1']) !!}
                                                            @if($errors->has('amount'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('amount') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td><button id="InventoryItems-del_btn-1" onclick="FormControls.destroyLineItem('0');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>

                                                </tr>
                                                <input type="hidden" id="InventoryItems-global_counter" value="<?php  echo ++$counter ?>"   />

                                                </tbody>

                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    @include('admin.stockitems.items_template')
                </div>
                <div id="stockitems_submit" class="col-xs-12">
                    <div class="col-xs-6">
                        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    {!! Form::close() !!}
    </div>
@stop
@section('javascript')
    <script src="{{ url('public/js/admin/stockitems/stockitems_create_modify.js') }}" type="text/javascript"></script>
@endsection


