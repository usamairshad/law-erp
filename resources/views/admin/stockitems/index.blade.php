@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Items & Stock</h1>
    </section>

    {{csrf_field()}}
    <input type="hidden" name="name" value="abc">
    <input type="hidden" name="selectedDatabanks" value="" id="selectedDatabanks">
    <div class="form-group col-md-3 @if($errors->has('sup_id')) has-error @endif" id="from_div">
        {!! Form::label('sup_id', 'Select Brand', ['class' => 'control-label']) !!}
        {!! Form::select('sup_id', $SuppliersModel , old('sup_id'),
         ['class' => 'form-control select2'  ,'id' => 'sup_id' , 'required' => 'true']) !!}
    </div>
    <div class="form-group col-md-3 @if($errors->has('cat_id')) has-error @endif" id="from_div">
        {!! Form::label('cat_id', 'Select Category', ['class' => 'control-label']) !!}
        {!! Form::select('cat_id', $StockCategoryModel , old('cat_id'),
         ['class' => 'form-control select2'  ,'id' => 'cat_id' , 'required' => 'true']) !!}
    </div>
    <div class="form-group col-md-2 @if($errors->has('search_button')) has-error @endif" >
        {!! Form::label('search_button', ' ', ['class' => 'control-label']) !!}
        <button id="search_button" onclick="FormControls.fetchFilterRecord();" type="button" style="margin-top: 5px;" class="btn  btn-sm btn-flat btn-primary form-control"><b>&nbsp;Search </b> </button>
    </div>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            {{--@if(Gate::check('stockitems_create'))--}}
                {{--<a href="{{ route('admin.stockitems.create') }}" class="btn btn-success pull-right">Add New Items & Stock</a>--}}
            {{--@endif--}}
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($StockItems) > 0 ? 'datatable' : '' }}" id="stock-table">
                <thead>
                <tr>
                    <th width="">S No:</th>
                    <th>Description</th>
                    <th>Short Name</th>
                    <th>Warehouse</th>
                    <th>Category</th>
                    <th>UOM</th>
                    <th>Unit Price (Rs)</th>
                    <th>In Stock</th>
                    <th>Stock Value</th>
                    <th>Created At</th>
                    {{--<th>Purchase Date</th>--}}
                    {{--<th>Actions</th>--}}
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/stockitems/list.js') }}" type="text/javascript"></script>

@endsection

