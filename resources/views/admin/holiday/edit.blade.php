@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Holiday</h3>
            <a href="{{ route('admin.holiday.index') }}" style ="float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::open(['method' => 'PUT','route' => ['admin.holiday.update',$holidays->id]]) !!}

        {!! Form::token(); !!}
        <div class="box-body" style = "margin-top:50px;">
            @include('admin.holiday.editfield')
        </div>
        <!-- /.box-body -->

        <div class="box-footer"  style = "padding-top: 27px;">
              <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/staff/create_modify.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function(){
 
        })
    </script>

@endsection