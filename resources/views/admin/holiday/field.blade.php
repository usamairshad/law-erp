
<div class="form-group col-md-3 @if($errors->has('holiday_name')) has-error @endif" style = "float:left;">
    {!! Form::label('holiday_name', 'Holiday Name*', ['class' => 'control-label']) !!}
    <input type="text" name="holiday_name" placeholder="Enter Holiday Name" class="form-control" required unique>
</div>
<div class="form-group col-md-3 @if($errors->has('holiday_date')) has-error @endif" style = "float:left;">
    {!! Form::label('holiday_date', 'Holiday Date*', ['class' => 'control-label']) !!}
    <input type="date" name="holiday_date" placeholder="Enter Holiday Date" class="form-control" required>
</div>