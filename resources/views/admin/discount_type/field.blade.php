
<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    <input type="text" name="name" placeholder="Name" class="form-control">
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif">
    {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
    <input type="text" name="description" placeholder="Description" class="form-control">
</div>
<div class="form-group  col-md-3 @if($errors->has('percentage')) has-error @endif">
    {!! Form::label('percentage', 'Percentage*', ['class' => 'control-label']) !!}
    <input type="number" name="percentage" min="0" required="required" class="form-control" />
</div>
<div class="form-group  col-md-3 @if($errors->has('status')) has-error @endif">
    {!! Form::label('status', 'Status*', ['class' => 'control-label']) !!}
    {!! Form::select('status',[ ''=>'Select  Status','Active' => 'Active','InActive' => 'InActive','Pending' => 'Pending'],old('status') , ['class' => 'form-control', 'required']) !!}
    @if($errors->has('status'))
        <span class="help-block">
            {{ $errors->first('status') }}
        </span>
    @endif
</div>