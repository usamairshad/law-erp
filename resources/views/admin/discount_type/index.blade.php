@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Discount Type</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
                @if(Gate::check('erp_discount_type_create'))
                <a href="{{ route('admin.discount-type.create') }}" class="btn btn-success pull-right">Add New Discount Type</a>
                @endif
        
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($discount_type) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Percentage</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($discount_type) > 0)
                    @foreach ($discount_type as $discount)
                        <tr data-entry-id="{{ $discount->id }}">
                          
                            <td>{{ $discount->name }}</td>
                            <td>{{ $discount->description }}</td>
                            <td>{{ $discount->percentage }}</td>
                            <td>
                                @if(Gate::check('erp_discount_type_edit'))
                                <a href="{{ route('admin.discount-type.edit',[$discount->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif
                                @if(Gate::check('erp_discount_type_destroy'))
                                {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.discount-type.destroy', $discount->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection