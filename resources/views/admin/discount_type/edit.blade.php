@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Discount Type</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Discount Type</h3>
            <a href="{{ route('admin.discount-type.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::open(['method' => 'PUT','route' => ['admin.discount-type.update',$discount_type->id]]) !!}

        {!! Form::token(); !!}
        <div class="box-body">
            @include('admin.discount_type.editfield')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
              <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/staff/create_modify.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function(){
 
        })
    </script>

@endsection