@inject('request', 'Illuminate\Http\Request')
@inject('CoreAccounts', 'App\Helpers\CoreAccounts')
@if($request->get('medium_type') != 'web')
    @if($request->get('medium_type') == 'pdf')
        @include('partials.pdf_head')
    @else
        @include('partials.head')
    @endif
    <style type="text/css">
        @page {
            margin: 10px 20px;
        }
        @media print {
            table {
                font-size: 12px;
            }
            .tr-root-group {
                background-color: #F3F3F3;
                color: rgba(0, 0, 0, 0.98);
                font-weight: bold;
            }
            .tr-group {
                font-weight: bold;
            }
            .bold-text {
                font-weight: bold;
            }
            .error-text {
                font-weight: bold;
                color: #FF0000;
            }
            .ok-text {
                color: #006400;
            }
        }
    </style>
@endif
@inject('CoreAccounts', '\App\Helpers\CoreAccounts')
<div class="panel-body pad table-responsive">
    <div class="col-md-6" style = "float:left;">
        <h4>Profit & Loss as on {{ $end_date }}</h4>
    </div>
    @if($request->get('medium_type') == 'web')
        <div class="col-md-6" style = "float:left;">
            <div class="text-center pull-right">
                <button onclick="FormControls.printReport('excel');"  type="button" style = "background: #199c19; color: white;"  class="btn bg-olive btn-flat"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</button>
                <button onclick="FormControls.printReport('pdf');" type="button" class="btn btn-danger btn-flat"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</button>
                <button onclick="FormControls.printReport('print');" type="button" style = "background: #817ba7; color: white;" class="btn btn-flat"><i class="fa fa-print"></i>&nbsp;Print</button>
            </div>
        </div>
    @endif
    <div class="clear clearfix"></div>
    <!-- Liabilities and Assets -->

    <div class="col-md-12">
        <!-- Assets -->
        <table class="table">
            <thead>
                <tr>
                    <th class="th-style">Incomes (Cr)</th>
                    <th class="th-style" width="20%" style="text-align: right;">Amount ({{ $DefaultCurrency->code }})</th>
                </tr>
            </thead>
            <tbody>
                {!! $pandl['gross_incomes_data'] !!}
                @if ($CoreAccounts::calculate($pandl['gross_income_total'], 0, '>='))
                    <tr class="bold-text bg-filled">
                        <td>Gross Incomes</td>
                        <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">{{ $CoreAccounts::toCurrency('d', $pandl['gross_income_total']) }}</td>
                    </tr>
                @else
                    <tr class="dc-error bold-text bg-filled">
                        <td>Gross Incomes</td>
                        <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right" class="show-tooltip" data-toggle="tooltip" data-original-title="Expecting positive Dr Balance">{{ $CoreAccounts::toCurrency('d', $pandl['gross_income_total']) }}</td>
                    </tr>
                @endif
                <tr class="bold-text">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>

        <!-- Liabilities -->
        <table class="table">
            <thead>
            <tr>
                <th class="th-style">Expenses (Dr)</th>
                <th class="th-style" width="20%" style="text-align: right;">Amount ({{ $DefaultCurrency->code }})</th>
            </tr>
            </thead>
            <tbody>
            {!! $pandl['gross_expenses_data'] !!}
            @if ($CoreAccounts::calculate($pandl['gross_expense_total'], 0, '>='))
                <tr class="bold-text bg-filled">
                    <td>Gross Expenses</td>
                    <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">{{ $CoreAccounts::toCurrency('c', $pandl['gross_expense_total']) }}</td>
                </tr>
            @else
                <tr class="dc-error bold-text bg-filled">
                    <td>Gross Expenses</td>
                    <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right" class="show-tooltip bg-filled" data-toggle="tooltip" data-original-title="Expecting positive Cr balance">{{ $CoreAccounts::toCurrency('c', $pandl['gross_expense_total']) }}</td>
                </tr>
            @endif
            <tr class="bold-text">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
</div>