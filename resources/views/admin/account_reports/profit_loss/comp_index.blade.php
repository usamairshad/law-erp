@extends('layouts.app')

@section('stylesheet')
    <!-- Pace style -->
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/plugins/pace/pace.min.css">
    <!-- Select2 -->
    {{--<link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/select2/dist/css/select2.min.css">--}}
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Account Reports</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Profit and Loss Report</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <div class="form-group col-md-3  @if($errors->has('date_range')) has-error @endif">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('date_range', null, ['id' => 'date_range', 'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group col-md-3 ">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('date_range_previous', null, ['id' => 'date_range_previous', 'class' => 'form-control']) !!}
                </div>
            </div>
            
            <div class="form-group col-md-3 @if($errors->has('branch_id')) has-error @endif">
                {!! Form::select('branch_id', $Branches, null, ['id' => 'branch_id', 'style' => 'width: 100%;', 'class' => 'form-control select2']) !!}
                <span id="branch_id_handler"></span>
            </div>
       
            <div class="form-group col-md-2 @if($errors->has('group_id')) has-error @endif">
                <a href="javascript:void(0);" onclick="FormControls.loadReport();" id="load_report" class="btn btn-success">Load Report</a>
            </div>

            <div class="clear clearfix"></div>

            <div id="content"></div>

            {!! Form::open(['method' => 'POST', 'target' => '_blank', 'route' => ['admin.account_reports.profit_loss_report_cmp'], 'id' => 'report-form']) !!}
                {!! Form::hidden('date_range', null, ['id' => 'date_range-report']) !!}
                {!! Form::hidden('date_range_previous', null, ['id' => 'date_range_previous-report']) !!}
                {!! Form::hidden('branch_id', null, ['id' => 'branch_id-report']) !!}
                {!! Form::hidden('group_id', null, ['id' => 'group_id-report']) !!}
                {!! Form::hidden('medium_type', null, ['id' => 'medium_type-report']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('javascript')
    <!-- PACE -->
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Select2 -->
    {{--<script src="{{ url('adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>--}}
    <script src="{{ url('public/js/admin/account_reports/comp_profit.js') }}" type="text/javascript"></script>
@endsection