@inject('request', 'Illuminate\Http\Request')
@inject('CoreAccounts', 'App\Helpers\CoreAccounts')
@if($request->get('medium_type') != 'web')
    @if($request->get('medium_type') == 'pdf')
        @include('partials.pdf_head')
    @else
        @include('partials.head')
    @endif
    
@endif
<style type="text/css">
    table {
 width: 100%;
}

thead, tbody, tr, td, th { display: block; }

tr:after {
 content: ' ';
 display: block;
 visibility: hidden;
 clear: both;
}

thead th {
 height: 60px;

 /*text-align: left;*/
}

tbody {
 height: 500px;
 overflow-y: auto;
}

thead {
 /* fallback */
}


tbody td, thead th {
 width: 50%;
 float: left;
}
     
 </style>
@inject('CoreAccounts', '\App\Helpers\CoreAccounts')
<div class="panel-body pad table-responsive" >
    <div class="col-md-6" style ="float:left;">
        <h4>Balance Sheet as on {{ $end_date }}</h4>
    </div>
    @if($request->get('medium_type') == 'web')
        <div class="col-md-6"style ="float:left;">
            <div class="text-center pull-right">

                <button onclick="FormControls.printReport('excel');" type="button" style = "background: #199c19; color: white;" class="btn bg-olive btn-flat"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</button>
                <button onclick="FormControls.printReport('pdf');" type="button" class="btn btn-danger btn-flat"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</button>
                <button onclick="FormControls.printReport('print');" type="button" style = "background: #817ba7; color: white;" class="btn btn-flat"><i class="fa fa-print"></i>&nbsp;Print</button>

            </div>
        </div>
    @endif
    <div class="clear clearfix"></div>
    <!-- Liabilities and Assets -->

    <div class="col-md-12">
        <!-- Assets -->
        <table class="table">
            <thead>
                <tr>
                    <th class="th-style">Assets (Dr)</th>
                    <th class="th-style" width="20%" style="text-align: right;">Amount ({{ $DefaultCurrency->code }})</th>
                </tr>
            </thead>
            <tbody>
                {!! $bsheet['assets_data'] !!}
                @if ($CoreAccounts::calculate($bsheet['assets_total'], 0, '>='))
                    <tr class="bold-text bg-filled">
                        <td>Total Assets</td>
                        <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">{{ $CoreAccounts::toCurrency('d', $bsheet['assets_total']) }}</td>
                    </tr>
                @else
                    <tr class="dc-error bold-text bg-filled">
                        <td>Total Assets</td>
                        <td style="text-align: border-top: 1px solid black; border-bottom: 1px solid black;" align="right" class="show-tooltip" data-toggle="tooltip" data-original-title="Expecting positive Dr Balance">{{ $CoreAccounts::toCurrency('d', $bsheet['assets_total']) }}</td>
                    </tr>
                @endif
                <tr class="bold-text">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>

        <!-- Liabilities -->
        <table class="table">
            <thead>
            <tr>
                <th class="th-style">Liabilities and Owners Equity (Cr)</th>
                <th class="th-style" width="20%" style="text-align: right;">Amount ({{ $DefaultCurrency->code }})</th>
            </tr>
            </thead>
            <tbody>
            {!! $bsheet['liabilities_data'] !!}
            @if ($CoreAccounts::calculate($bsheet['liabilities_total'], 0, '>='))
                <tr class="bold-text bg-filled">
                    <td>Total Liability and Owners Equity</td>
                    <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">{{ $CoreAccounts::toCurrency('c', $bsheet['liabilities_total']) }}</td>
                </tr>
            @else
                <tr class="dc-error bold-text bg-filled">
                    <td>Total Liability and Owners Equity</td>
                    <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right" class="show-tooltip bg-filled" data-toggle="tooltip" data-original-title="Expecting positive Cr balance">{{ $CoreAccounts::toCurrency('c', $bsheet['liabilities_total']) }}</td>
                </tr>
            @endif
            <tr class="bold-text">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>
</div>