@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Certificate of Collection or Deduction of Income Tax</h1>
    </section>
@stop
@section('stylesheet')
    <!-- Pace style -->
    {{--<link rel="stylesheet" href="{{ url('public/adminlte') }}/plugins/pace/pace.min.css">--}}
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('content')

    <div class="box box-primary">
        <br>
     
<h2 style = "text-align:center;">Certificate of Collection or Deduction of Income Tax (including salary)</h2> 	
<h4 style = "text-align:center;">Under Rule 42</h4> 	

<table class="table table-bordered">

                <tr>
                 <td>S.No</td>
                 <td style ="width:200px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
                 <td style ="text-align:center;"><b style ="text-align:center;">Original / Duplicate</b></td>
                 <td>Date of Issue</td>
                 <td style ="width:200px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{date('d-m-Y')}}</td>
                </tr>
                </table>
                <table class="table table-bordered">
                <tr>
                <td>Certified that the sum of Rupees</td>
                <td style ="width:200px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{$tax_amount}}</td>
                <td style = "text-align:center;">(Amount of tax collected/deducted in figures)	</td>
                </tr>
            </table>
            <table class="table table-bordered">
                <tr>
                <td style = "width:300px"></td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{ $tax_word}}</td>
                <td style = "text-align:center;">	(amount in words)		</td>
                </tr>
            </table>
            <table class="table table-bordered">
                <tr>
                <td style = "width:300px">on account of income tax has been</td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
                <td style = "text-align:center;">	</td>
                </tr>
                <tr>
                <td style = "width:300px">deducted/collected from (Name and</td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
                <td style = "text-align:center;">	</td>
                </tr>
                <tr>
                <td style = "width:300px"> Address of the person from whom tax collected/deducted)	</td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
                <td style = "text-align:center;">	</td>
                </tr>
                <tr>
                <td style = "width:300px"> </td>
                <td style ="width:600px; text-align:center;">In case of an individual, his/her name in full and in case of an association of persons / company, name and style of the association of persons/company				
	</td>
                <td style = "text-align:center;">	</td>
                </tr>
                <tr>
                <td style = "width:300px">having National Tax Number </td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{$get_data->first_name}} {{$get_data->middle_name}} {{$get_data->last_name}} </td>
                <td style = "text-align:center;">	(if any) and</td>
                </tr>
                <tr>
                <td style = "width:300px">	holder of CNIC No.</td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{$get_data->cnic}}</td>
                <td style = "text-align:center;">	(In case of an individual)</td>
                </tr>
                <tr>
                <td style = "width:300px">On</td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
                <td style = "text-align:center;">	(Date of collection/ deduction)</td>
                </tr>
                </table>
                <table  class="table table-bordered">
                <tr>
                <td style = "width:300px">	Or during the period</td>
                <td style = "width:300px">	From</td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{ $period_array[0] }}</td>
                <td style = "width:300px">	To</td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"> {{ $period_array[1] }}</td>
                <td style = "text-align:center;">	(Date of collection/ deduction)</td>
                </tr>

            </table>
            <table class="table table-bordered">
            <tr>
                <td style = "width:300px">	under section </td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">149</td>
                <td style = "text-align:center;">	(specify the Section of Income Tax Ordinance,2001)</td>
                </tr>
            </table>		
            <table class="table table-bordered">
            <tr>
                <td style = "width:300px">		On account of   </td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">Salary</td>
                <td style = "text-align:center;">	(specify nature)</td>
                </tr>
        
            <tr>
                <td style = "width:300px">		vide </td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{$total}}</td>
                <td style = "text-align:center;">	(particulars of LC, Contract etc.)</td>
                </tr>
                <tr>
                <td style = "width:300px">on the value/amount of Rupee	 </td>
                <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
                <td style = "text-align:center;">	(Gross amount on which tax deducted/collected in figures)</td>
                </tr>

            </table>	
            <table class="table table-bordered">
            <tr>
                <td style = "width:300px">		This is to further certify that the tax collected/deducted has been deposited in the Fedral Government Account as per the following details:								
		 </td>
                </tr>
        
            <tr>
            </table>
            <table class="table table-bordered">
            <tr>
            <td>Date of deposit.</td>
            <td>SBP/NBP/  </td>
            <td>Treasury</td>
            <td>Branch/City</td>
                    <td>Account  (Rupees)</td>
                    <td>	Challan                   /Treasury No/CPR No.</td>
            </tr>
            <tr>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">Different Dates</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            </tr>
            <tr>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            </tr>
            <tr>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            </tr>
            <tr>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>
            </tr>
            </table>				
            <table class="table table-bordered">
            <tr>
            <td>Company / office etc. collecting/deducting the tax:		</td>
            </tr>		
            </table>
            <table class="table table-bordered">
            <tr>
            <td></td>		
            <td>Name.	</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{$get_data->name}}</td>	
			<td></td>	
            <td>	Signature	</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>	
			<td></td>			
            </tr>
            <tr>
            <td></td>
            <td>Address.</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{$get_data->address}}</td>	
			<td></td>
            <td>Name	</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">Iftikhar Qayyum Awan</td>	
			<td></td>	
            </tr>
            <tr>
            <td></td>	
	        <td>NTN (if any)</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{$get_data->ntn }}</td>	
            <td></td>
            <td>Designation</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">GM Finance</td>	
            <td></td>	
            </tr>	

	<tr>
            <td></td>	
	        <td>Date</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black">{{date('d-m-Y')}}</td>	
            <td></td>
            <td>Seal</td>
            <td style ="width:600px; text-align:center; background:lightgrey; border-bottom:1px solid black"></td>	
            <td></td>	
            </tr>						
			</table>
     
    </div>
@stop
@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection						
