@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
.form-group
{
    float:left;
}
.daterangepicker
{
	width:35% !important;
} 
.left
{
	float:left;
	width:38% !important;
	margin-left:2%;
	margin: 5px;

}
.right
{
	float:left;
	width:39% !important;
	margin: 5px;

}
.range_inputs
{
    padding:10px;
}
.cancelBtn
{
    background: lightgrey;
    
}
.daterangepicker.show-calendar .ranges {
    margin-top: 8px;
    width: 19%;
}
.fa
{
	display:none !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Trail Balance Sheet</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <div class="form-group col-md-3  @if($errors->has('date_range')) has-error @endif">
                {{--{!! Form::label('date_range', 'Date Range*', ['class' => 'control-label']) !!}--}}
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('date_range', null, ['id' => 'date_range', 'class' => 'form-control']) !!}
                </div>
            </div>
            <!--div class="form-group col-md-3 @if($errors->has('branch_id')) has-error @endif">
{{--                {!! Form::label('branch_id', 'Branch', ['class' => 'control-label']) !!}--}}
                {!! Form::select('branch_id', $Branches, null, ['id' => 'branch_id', 'style' => 'width: 100%;', 'class' => 'form-control select2']) !!}
                <span id="branch_id_handler"></span>
            </div-->
           
        
            <div class="form-group col-md-3 @if($errors->has('account_type_id')) has-error @endif">
{{--                {!! Form::label('account_type_id', 'Account Types', ['class' => 'control-label']) !!}--}}
                {!! Form::select('account_type_id', array()+Config::get('admin.account_level'), null, ['id' => 'account_type_idd', 'style' => 'width: 100%;', 'class' => 'form-control']) !!}
                <span id="account_type_id_handler"></span>
            </div>
            <div class="form-group col-md-4 @if($errors->has('group_id')) has-error @endif">
                {{--{!! Form::label('group_id', 'Group', ['class' => 'control-label']) !!}--}}
                <span id="group_id_content">
                    <select name="group_id" id="group_id" class="form-control select2" style="width: 100%;">
                        <option value=""> Select a Parent Group </option>
                        {!! $Groups !!}
                    </select>
                </span>
                <span id="group_id_handler"></span>
            </div>
            <div class="form-group col-md-2 @if($errors->has('group_id')) has-error @endif" >
                <a href="javascript:void(0);" onclick="FormControls.loadReport();" id="load_report" class="btn btn-success">Load Report</a>
            </div>

            <div id="content"></div>

            {!! Form::open(['method' => 'POST', 'target' => '_blank', 'route' => ['admin.account_reports.trial_balance_report'], 'id' => 'report-form']) !!}
                {!! Form::hidden('date_range', null, ['id' => 'date_range-report']) !!}
                {!! Form::hidden('branch_id', null, ['id' => 'branch_id-report']) !!}
                {!! Form::hidden('employee_id', null, ['id' => 'employee_id-report']) !!}
                {!! Form::hidden('department_id', null, ['id' => 'department_id-report']) !!}
                {!! Form::hidden('entry_type_id', null, ['id' => 'entry_type_id-report']) !!}
                {!! Form::hidden('account_type_id', null, ['id' => 'account_type_id-report']) !!}
                {!! Form::hidden('group_id', null, ['id' => 'group_id-report']) !!}
                {!! Form::hidden('medium_type', null, ['id' => 'medium_type-report']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('javascript')
    <!-- PACE -->
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Select2 -->
    {{--<script src="{{ url('adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>--}}
    <script src="{{ url('public/js/admin/account_reports/trial_balance.js') }}" type="text/javascript"></script>
@endsection