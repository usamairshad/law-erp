@inject('request', 'Illuminate\Http\Request')
@if($request->get('medium_type') != 'web')
    @if($request->get('medium_type') == 'pdf')
        @include('partials.pdf_head')
    @else
        @include('partials.head')
    @endif
    <style type="text/css">
        @page {
            margin: 10px 20px;
        }
        @media print {
            table {
                font-size: 12px;
            }
            .tr-root-group {
                background-color: #F3F3F3;
                color: rgba(0, 0, 0, 0.98);
                font-weight: bold;
            }
            .tr-group {
                font-weight: bold;
            }
            .bold-text {
                font-weight: bold;
            }
            .error-text {
                font-weight: bold;
                color: #FF0000;
            }
            .ok-text {
                color: #006400;
            }
            .th
            {
                color:black;
                font-size:17px;
            }

        }
    </style>
@endif
@inject('CoreAccounts', '\App\Helpers\CoreAccounts')
<div class="panel-body pad table-responsive">
    <div class="col-md-6" style = "float:left">
        <h4>Trial Balance from {{ $start_date }} to {{ $end_date }}</h4>
    </div>
    @if($request->get('medium_type') == 'web')
        <div class="col-md-6" style = "float: left; margin-bottom: 30px;">
            <div class="text-center pull-right">
                <button onclick="FormControls.printReport('excel');" type="button" style = "background: #199c19; color: white;" class="btn bg-olive btn-flat"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</button>
                <button onclick="FormControls.printReport('pdf');" type="button" class="btn btn-danger btn-flat"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</button>
                <button onclick="FormControls.printReport('print');" type="button" style = "background: #817ba7; color: white;" class="btn btn-flat"><i class="fa fa-print"></i>&nbsp;Print</button>
            </div>
        </div>
    @endif

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th style = "font-size: 14px;color: black;">Account Name</th>
            <th style = "font-size: 14px;color: black;">Account Type</th>
            <th style="text-align: right; font-size: 14px;color: black;">Opening Dr</th>
            <th style="text-align: right;font-size: 14px;color: black;">Opening Cr</th>
            <th style="text-align: right;font-size: 14px;color: black;">Debit ({{ $DefaultCurrency->code }})</th>
            <th style="text-align: right; font-size: 14px;color: black;">Credit ({{ $DefaultCurrency->code }})</th>
            <th style="text-align: right; font-size: 14px;color: black;">Closing Dr</th>
            <th style="text-align: right;font-size: 14px;color: black;">Closing Cr</th>
        </tr>
        </thead>
        <tbody>
            {!! $ReportData !!}
            <tr class="bold-text @if($CoreAccounts::calculate($accountlist->dr_total, $accountlist->cr_total, '==')) ok-text @else error-text @endif">
                <td align="right" colspan="2">Grand Total</td>
                <td align="right">{{ $CoreAccounts::toCurrency('d', $accountlist->op_dr_total) }}</td>
                <td align="right">{{ $CoreAccounts::toCurrency('c', $accountlist->op_cr_total) }}</td>
                <td align="right">{{ $CoreAccounts::toCurrency('d', $accountlist->dr_total) }}</td>
                <td align="right">{{ $CoreAccounts::toCurrency('c', $accountlist->cr_total) }}</td>
                <td align="right">{{ $CoreAccounts::toCurrency('d', $accountlist->cl_dr_total) }}</td>
                <td align="right">{{ $CoreAccounts::toCurrency('c', $accountlist->cl_cr_total) }}</td>
                {{--<td><span class="glyphicon @if($CoreAccounts::calculate($accountlist->dr_total, $accountlist->cr_total, '==')) glyphicon-ok-sign @else glyphicon-remove-sign @endif"></span></td>--}}
            </tr>
        </tbody>
    </table>
</div>