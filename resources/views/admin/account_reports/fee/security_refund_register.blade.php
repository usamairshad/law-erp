@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Fee (MIS Reconciliation) - Security Refundable Register</h1>
    </section>
@stop
@section('stylesheet')
    <!-- Pace style -->
    {{--<link rel="stylesheet" href="{{ url('public/adminlte') }}/plugins/pace/pace.min.css">--}}
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('content')

    <div class="box box-primary">
        <br>
        <form id="ledger-form">
            {{ csrf_field()}}
         
            <div class="col-md-3">
                <div class="form-group">
                    <label>Select Company</label>
                    <select name = "company_id" class="form-control input-sm select2" id="mySelect" onchange="myFunction()">
                        <option value="">---Select---</option>
                        {!! \App\Models\Admin\Company::CompanyList() !!}
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Select City</label>
                    <select  name='city_id' class="form-control input-sm select2" id="cID">
                        <option value="">---Select---</option>
                        {!! \App\Models\Admin\City::CityList() !!}
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Select Branch</label>
                    <select  name='branch_id' class="form-control input-sm select2 branches" id="bID">
                        <option value="">---Select---</option>
                        
                    </select>
                </div>
            </div>
          
            <div class="col-md-2" style="margin-top: 25px;">
                <div class="form-group">
                    <button type="button" class="btn btn-sm btn-primary" onclick="fetch_ledger()"><i class="fa fa-search"></i> </button>
                    <button type="submit" class="btn btn-sm btn-default" formaction="{{ route( 'security-refund-register-report-save' ) }}?type=print" formmethod="post" formtarget="_blank"><i class="fa fa-print"></i> </button>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>Admission No</th>
                    <th>Campus</th>
                    <th>Name</th>
                    <th style="">Total Balance</th>
                    <th style="">Security Received</th>
                    <th style="text-align: right;">Paid Date</th>
                    <th style="text-align: right;">Refunded Amount</th>
                    <th style="text-align: right;">Balance(if any)</th>
                    <th style="text-align: right;">Date of Refund</th>
                </tr>
                </thead>
                <tr id="fetch_ob"></tr>
                <tbody id="getData"></tbody>
            </table>
        </div>
    </div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
 
    function fetch_ledger() {
        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : '{{ route( 'security-refund-register-report-save' ) }}',
            data:$("#ledger-form").serialize(),
            success:function (data) {
                console.log(data);
                var htmlData="";
                var k=1;
                for(i in data.data){
                    if(k >1)
                    {
                    //console.log(data.data[i].vt);
                    htmlData+='<tr>';
                    htmlData+='<td>'+data.data[i].id+'</td>';
                    htmlData+='<td>'+data.data[i].reg_no+'</td>';
                    
                    htmlData+='<td>'+data.data[i].branch+'</td>';
                    htmlData+='<td>'+data.data[i].first_name+'</td>';
                    htmlData+='<td>'+data.data[i].amount+'</td>';
                    htmlData+='<td>'+data.data[i].received+'</td>';
                    htmlData+='<td>'+data.data[i].date+'</td>';
                    htmlData+='<td>'+data.data[i].amount_cr+'</td>';
                    htmlData+='<td>'+data.data[i].balance+'</td>';
                    htmlData+='<td>'+data.data[i].refund+'</td>';
                    htmlData+='</tr>';
                    }
                    k++;
                }
             /*    htmlData+='<tr>';
                    htmlData+='<td colspan="4"></td>';
                    htmlData+='<th>'+data.total_dr+'</th>';
                    htmlData+='<th>'+data.total_cr+'</th>';
                    htmlData+='<th style="text-align: right;">'+data.balance+'</th>';
                htmlData+='</tr>'; */
                $("#getData").html(htmlData);
                $('#fetch_ob').html(data.ob);
            }
        });
    }

</script>
<script>
function myFunction() {
  var getid = document.getElementById("mySelect").value;
  $.ajax({
      url: '{{url('security-company')}}',
      type: 'get',
      data: {
          "company_id": getid
      },
      success: function(data){
          console.log(data);
          $("#bID").empty();
          
          $("#bID").append("<option>---Select---</option>");
          for(i in data){
              $("#bID").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
          }
      }
  })
}
    </script>

@section('javascript')
    
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    

@endsection