@inject('request', 'Illuminate\Http\Request')
@include('partials.head')
<style type="text/css">
    @page {
        margin: 10px 20px;
    }
    table{
        text-align: center;
    }
    @media print {
        table {
            font-size: 12px;
        }
        .tr-root-group {
            background-color: #F3F3F3;
            color: rgba(0, 0, 0, 0.98);
            font-weight: bold;
        }
        .tr-group {
            font-weight: bold;
        }
        .bold-text {
            font-weight: bold;
        }
        .error-text {
            font-weight: bold;
            color: #FF0000;
        }
        .ok-text {
            color: #006400;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding: 2px !important;
        }

    }
    .table th{ text-align: center; }
    .last-th th{border-bottom: 1px solid}
</style>

@include('partials.print_head')
    


        <br>
     
        <div class="clearfix"></div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
        <h1 style = "text-align:center; font-size:25px">Fee (MIS Reconciliation) - Recovery Report</h1>
  
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>Campus</th>
                    <th>Revenue Receivable</th>
                    <th>Revenue Recovered</th>
                    <th>No of Students</th>
                    <th>% of Recovery</th>
                </tr>
                </thead>
                <tbody>
        @foreach($array as $data)
        <tr>
        @foreach($data as $d)
         
                <td>{{$d}}</td>
            
            @endforeach
            </tr>
        @endforeach
      
        </tbody>
            </table>
        </div>
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection