@inject('request', 'Illuminate\Http\Request')
@include('partials.head')
<style type="text/css">
    @page {
        margin: 10px 20px;
    }
    table{
        text-align: center;
    }
    @media print {
        table {
            font-size: 12px;
        }
        .tr-root-group {
            background-color: #F3F3F3;
            color: rgba(0, 0, 0, 0.98);
            font-weight: bold;
        }
        .tr-group {
            font-weight: bold;
        }
        .bold-text {
            font-weight: bold;
        }
        .error-text {
            font-weight: bold;
            color: #FF0000;
        }
        .ok-text {
            color: #006400;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding: 2px !important;
        }

    }
    .table th{ text-align: center; }
    .last-th th{border-bottom: 1px solid}
</style>
<div class="panel-body pad table-responsive">
    @include('partials.print_head')
    <table align="center">
        <tbody>
        <tr>
            <td><h3 align="center"><span style="border-bottom: double;">Fee (MIS Reconciliation) - Security Refundable Register</span></h3>
             </td>
        </tr>
        <tr>
            <td align="center"><span></span>
            </td>
        </tr>
        </tbody>
    </table>
    <table class="table">
        <thead>
        <tr>
        <th>S.No</th>
                    <th>Admission No</th>
                    <th>Campus</th>
                    <th>Name</th>
                    <th style="">Total Balance</th>
                    <th style="">Security Received</th>
                    <th style="text-align: right;">Paid Date</th>
                    <th style="text-align: right;">Refunded Amount</th>
                    <th style="text-align: right;">Balance(if any)</th>
                    <th style="text-align: right;">Date of Refund</th>
        </tr>
        </thead>
        <tbody>
        @foreach($array as $data)
        <tr>
        @foreach($data as $d)
         
                <td>{{$d}}</td>
            
            @endforeach
            </tr>
        @endforeach
      
        </tbody>
    </table>
</div>
