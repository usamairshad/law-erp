@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

					<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3>Fee (MIS Reconciliation) - Recovery Report</h3>
								</div>
                                <form id="ledger-form">
            {{ csrf_field()}}
          
            <div class="col-md-3" style = "float:left;">
                <div class="form-group">
                    <label>Select Company</label>
                    <select  name='company_id' class="form-control input-sm" onchange="myFunction()" id="mySelect" required>
                        <option value="">---Select---</option>
                        {!! \App\Models\Admin\Company::CompanyList() !!}
                    </select>
                </div>
            </div>
     
            
            <div class="col-md-2" style = "float:right;">
                <div class="form-group">
                    <button type="button" style = "width:55px;float:right" class="btn btn-primary btn-block" onclick="fetch_ledger()"><i class="fa fa-search"></i> </button>
                    <button type="submit" style = "width: 55px;
    float: right;
    margin-right: 20px;
    margin-top: 0px;" class="btn btn-light btn-block" formaction="fee_recover_report_print?type=print" formmethod="post" formtarget="_blank"><i class="fa fa-print"></i> </button>
                 </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>Campus</th>
                    <th>Revenue Receivable</th>
                    <th>Revenue Recovered</th>
                    <th>No of Students</th>
                    <th>% of Recovery</th>
                </tr>
                </thead>
                <tr id="fetch_ob"></tr>
                <tbody id="getData"></tbody>
            </table>
        </div>
							
							</div>
						</div>
		

  
@stop


@section('content')

    <div class="box box-primary">
        <br>
       
    </div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Select an option',
            allowClear: true,
        });
        $('#search_ledger_id').select2({
            placeholder: 'Select an option',
            allowClear: true,
            ajax: {
                url: route('admin.voucher.gjv_search'),
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    return {
                        item: params.term, branch_id:$("#bID").val(),
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
            }
        });

        $( function() {
            $( "#datepicker" ).daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                }
            });
        });

    });
    function fetch_ledger() {
        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'{{ route( 'fee-recovery-report-save' ) }}',
            data:$("#ledger-form").serialize(),
            success:function (data) {
                console.log(data);
                var htmlData="";
                for(i in data.data){
                    //console.log(data.data[i].vt);
                    htmlData+='<tr>';
                    htmlData+='<td>'+data.data[i].id+'</td>';
                    htmlData+='<td>'+data.data[i].branch+'</td>';
                    htmlData+='<td>'+data.data[i].balance+'</td>';
                    htmlData+='<td>'+data.data[i].tcr+'</td>';
                    htmlData+='<td>'+data.data[i].no+'</td>';
                    htmlData+='<td>'+data.data[i].rec+'%</td>';
                    htmlData+='</tr>';
                }
             /*    htmlData+='<tr>';
                    htmlData+='<td colspan="4"></td>';
                    htmlData+='<th>'+data.total_dr+'</th>';
                    htmlData+='<th>'+data.total_cr+'</th>';
                    htmlData+='<th style="text-align: right;">'+data.balance+'</th>';
                htmlData+='</tr>'; */
                $("#getData").html(htmlData);
                $('#fetch_ob').html(data.ob);
            }
        })
    }

</script>
<script>
function myFunction() {
  var getid = document.getElementById("mySelect").value;
  $.ajax({
      url: '{{url('security-company')}}',
      type: 'get',
      data: {
          "company_id": getid
      },
      success: function(data){
          console.log(data);
          $("#bID").empty();
          
          $("#bID").append("<option>---Select---</option>");
          for(i in data){
              $("#bID").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
          }
      }
  })
}
    </script>
@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection