@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Bank Loan Details</h3>
            <a href="{{ url('admin/bank_loan') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.bank_loan.store'], 'id' => 'validation-form']) !!}
        <div class="box-body" style = "margin-top:40px;">
            
            <div class="form-group col-md-3 @if($errors->has('bank_id')) has-error @endif" style = "float:left;">
                {!! Form::label('bank_id', 'Bank Name*', ['class' => 'control-label']) !!}
                {!! Form::select('bank_id', $bank->prepend('Select Bank', ''),old('bank_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('bank_id'))
                    <span class="help-block">
                        {{ $errors->first('bank_id') }}
                    </span>
                @endif
            </div>

    
            <div class="form-group col-md-3 @if($errors->has('amount')) has-error @endif" style = "float:left;">
                {!! Form::label('amount', 'Amount*', ['class' => 'control-label']) !!}
                <input type="number" name="amount" class="form-control" value="{{ old('amount') }}" required maxlength="50">
            </div>

           
            <div class="form-group col-md-3 @if($errors->has('installment_plan')) has-error @endif" style = "float:left;">
                {!! Form::label('installment_plan', 'Installment Plan*', ['class' => 'control-label']) !!}
                <input type="text" name="installment_plan" class="form-control" value="{{ old('installment_plan') }}" required >
            </div>
            <div class="form-group col-md-3 @if($errors->has('deduction_each_month')) has-error @endif" style = "float:left;">
                {!! Form::label('deduction_each_month', 'Deduction Each Month*', ['class' => 'control-label']) !!}
                <input type="number" name="deduction_each_month" class="form-control" value="{{ old('deduction_each_month') }}" required >
            </div>
            <div class="form-group col-md-3 @if($errors->has('bank_intrest')) has-error @endif" style = "float:left;">
                {!! Form::label('bank_intrest', 'Bank Intrest*', ['class' => 'control-label']) !!}
                <input type="number" name="bank_intrest" class="form-control" value="{{ old('bank_intrest') }}" required >
            </div>
            <div class="form-group col-md-3 @if($errors->has('kabis')) has-error @endif" style = "float:left;">
                {!! Form::label('kabis', 'Kabis*', ['class' => 'control-label']) !!}
                <input type="number" name="kabis" class="form-control" value="{{ old('kabis') }}" required >
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer" style = "padding-top:110px;">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/departments/create_modify.js') }}" type="text/javascript"></script>
@endsection

