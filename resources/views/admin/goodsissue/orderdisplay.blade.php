@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Issue Detail</h1>
    </section>
@stop
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <a href="{{ route('admin.goodsissue.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <!-- Slip Area Started -->

            <section class="content-header" style="padding: 10px 15px !important;">
                <h1>Product View</h1>
            </section>


            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Name</th>
                    <th>qty</th>
                </tr>
                </thead>
                <tbody>
                @forelse($GoodsIssueDetailModel as $GoodsIssueDetails)
                    <tr>
                        <td>{{ $GoodsIssueDetails->products->products_name }}</td>
                        <td>{{ $GoodsIssueDetails->quantity }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No Record Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
        <!-- /.box-body -->
    </div>






@stop

