@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Purchase Order</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            {{--@if(Gate::check('pono_create'))--}}
                <a href="{{ route('admin.pono.create') }}" class="btn btn-success pull-right">Add New Purchase Order</a>
            {{--@endif--}}
        </div>
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Ponumber) > 0 ? 'datatable' : '' }} " id="users-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Brand</th>
                        <th>Po No</th>
                        <th>Po Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.pono.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'brand_id', name: 'brand_id' },
                    { data: 'po_no', name: 'po_no' },
                    { data: 'po_date', name: 'po_date' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},

                ]
            });
        });
    </script>
@endsection