@extends('layouts.app')
@section('stylesheet')
    <!-- Select2 -->

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">


    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">

@stop
@section('content')

    <div class="box-header with-border">
        <h3 class="box-title">Update Purchase Order</h3>
        <a href="{{ route('admin.pono.index') }}" class="btn btn-success pull-right">Back</a>
    </div>

    {!! Form::model($Ponumber, ['method' => 'PUT', 'id' => 'validation-form','route' => ['admin.pono.update', $Ponumber->id]]) !!}
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_edit')
        </div>
        <div class="panel-body">
            <div class="brands_row row">
                <div class="col-xs-6 form-group @if($errors->has('brand_id')) has-error @endif">
                    {!! Form::label('brand_id', 'Brands *', ['class' => 'control-label']) !!}
                    <select id="brand_id" name="brand_id" class="form-control select2">
                        @if ($SuppliersModel->count())
                            <option value="">Select Brands</option>
                            @foreach($SuppliersModel as $Supplier)
                                <option value="{{ $Supplier->id }}"@if($Ponumber->brand_id==$Supplier->id) {{'selected="selected"'}} @endif>{{ $Supplier->sup_name }}</option>
                            @endforeach
                        @endif
                    </select>
                    @if($errors->has('brand_id'))
                        <span class="help-block">
                                    {{ $errors->first('brand_id') }}
                                    </span>
                    @endif
                </div>
            </div>

            <div class="brands_row row">
                <div class="col-xs-6 form-group @if($errors->has('po_no')) has-error @endif">
                    {!! Form::label('po_no', 'Po No *', ['class' => 'control-label']) !!}
                    {!! Form::text('po_no', $Ponumber->po_no, ['class' => 'form-control', 'placeholder' => '']) !!}
                    @if($errors->has('po_no'))
                        <span class="help-block">
							{{ $errors->first('po_no') }}
							</span>
                    @endif
                </div>

            </div>
            <div class="brands_row row">
                <div class="col-xs-6 form-group @if($errors->has('po_date')) has-error @endif">
                    {!! Form::label('po_date', 'Po Date', ['class' => 'control-label']) !!}
                    {!! Form::text('po_date', $Ponumber->po_date, ['class' => 'form-control', 'placeholder' => '']) !!}

                    @if($errors->has('po_date'))
                        <span class="help-block">
                                {{ $errors->first('po_date') }}
                                </span>
                    @endif
                </div>
            </div>
            <div class="brands_row row">
                <div class="col-xs-6">
                    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                </div>
            </div>

        </div>
    </div>
    </div>
    </div>
    {!! Form::close() !!}
@stop
@section('javascript')
    <script src="{{ url('adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('js/admin/pono/pono_update_modify.js') }}" type="text/javascript"></script>
@endsection
<style>
    .brands_row{
        float:left;
        width: 100%;
    }
</style>