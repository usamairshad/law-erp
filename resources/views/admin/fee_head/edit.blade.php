@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Fee Head</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Fee Head</h3>
            <a href="{{ route('admin.fee_head.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->



        {!! Form::model($feeHead, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.fee_head.update', $feeHead->id]]) !!}
        <div class="box-body">
            <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'10', 'required']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>

            <div class="form-group col-md-3 @if($errors->has('city_id')) has-error @endif">
                {!! Form::label('fee_section_id', 'Fee Section*', ['class' => 'control-label']) !!}
                {!! Form::select('fee_section_id', $feeSection->prepend('Select Fee Section', ''),old('fee_section_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('fee_section_id'))
                    <span class="help-block">
                        {{ $errors->first('fee_section_id') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('program_id')) has-error @endif">
                {!! Form::label('program_id', 'Program', ['class' => 'control-label']) !!}
                {!! Form::select('program_id', $program->prepend('Select Program', ''),old('program_id'), ['class' => 'form-control','required']) !!}
                @if($errors->has('program_id'))
                    <span class="help-block">
                        {{ $errors->first('program_id') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-12 @if($errors->has('description')) has-error @endif">
                {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
                {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'255', 'required']) !!}
                @if($errors->has('description'))
                    <span class="help-block">
                        {{ $errors->first('description') }}
                    </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
@endsection