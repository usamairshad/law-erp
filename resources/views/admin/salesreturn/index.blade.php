@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sales Return List</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('sales_return_create'))
                <a href="{{ route('admin.salesreturn.create') }}" class="btn btn-success pull-right">Add New Sales Return</a>
            @endif
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($SaleReturn) > 0 ? 'datatable' : '' }}" id="order-table">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Invoice No</th>
                    <th>Return Date</th>
                    <th>Description</th>
                    <th>Created By</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            $('#order-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.salesreturn.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'invoice_id', name: 'invoice_id' },
                    { data: 'return_date', name: 'return_date' },
                    { data: 'description', name: 'description' },
                    { data: 'created_by', name: 'created_by' },

                    { data: 'action', name: 'action' , orderable: false, searchable: false},
                ]
            });
        });
    </script>
@endsection


