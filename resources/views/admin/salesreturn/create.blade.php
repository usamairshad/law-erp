@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Generat Sales Return</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Sales Return Detail</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.salesreturn.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">

            @include('admin.salesreturn.fields')

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger',  'id' => 'btn_save']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <!-- <script src="{{ url('public/js/admin/salesinvoice/list.js') }}" type="text/javascript"></script> -->
    <script>
        /**
         * Created by mustafa.mughal on 12/7/2017.
         */

            //== Class definition
        var FormControls = function () {
                //== Private functions

                $('#btn_save').on('click', function(event){
                    if($('#due_date').val() == '' || $('#rr_no').val() == '' || $('#delivery_type').val() == ''){
                        alert('Please fill all the fields');
                        event.preventDefault();
                    }
                });
              //  document.getElementById('users-table').style.display = "none";

                var baseFunction = function(){
                    $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd' //format: 'DD-MM-YYYY H:m:s A',
                    });

                }

                var fetchFilterrecord = function () {
                    console.log('fetchFilterrecord');
                    var invoice_id = $('#invoice_id').val();
                    var token = $("input[name=_token]").val();

                    if(invoice_id){
                        $.ajax({
                            type: "POST",
                            url: 'getOrderDetail',
                            data: {
                                'invoice_id' : invoice_id,
                                '_token': token,
                            },
                            success: function(result){
                                console.log(result);
                                //console.log(result.data[0][1]);

                                $("#users-table tbody > tr").remove();
                                displayOrderDetail( result );
                                //addTable(result.data, result.gross_amount, result.tax_amount, result.tax_percent);
                                var invoice = result.invoice;
                                $('#invoice_date').val(invoice.invoice_date);
                                $('#rr_no').val(invoice.rr_no);
                                $('#due_date').val(invoice.due_date);

                                $('#amount').val(invoice.net_income);
                                $('#tax_amount').val(invoice.total_tax);
                                $('#grand_total').val(invoice.payable_amount);


                            }
                        });
                    }
                    else{
                        alert('Please Select an Order First');
                    }

                }
                 var displayOrderDetail =  function (data) {
                    var tablehtml ='';
                    var total_amount = 0;
                     var total_tax = 0;
                     var product_total=0;
                     var product_tax=0;

                     var serviceshtml ='';
                     var services_total = 0;
                     var services_tax = 0;

                    var products_array = data.products_array;
                     var services_array = data.services;
                    for(var i=0; i<products_array.length; i++){
                        tablehtml += '<tr>';
                        tablehtml += '<td>'+ (i + 1) + '</td>';
                        tablehtml += '<td> ' + products_array[i].p_name + ' </td>';

                        tablehtml += '<td> ' + products_array[i].quantity + ' </td>';

                        tablehtml += '<td> ' + products_array[i].serialNo + ' </td>';
                        tablehtml += '<td> ' + products_array[i].unit_price + ' </td>';
                        tablehtml += '<td> ' + products_array[i].total_price  + ' </td>';
                        var return_qty = 0 ;
                        if( products_array[i].return_qty){
                            return_qty = products_array[i].quantity - products_array[i].return_qty ;
                        }else{
                            return_qty = 0 ;
                        }
//                        tablehtml += '<td style="text-align: center;"> ' + products_array[i].return_qty ? products_array[i].return_qty : 0 + ' </td>';
                        tablehtml += '<td style="text-align: center;"> ' + return_qty  + ' </td>';
                        tablehtml += '<td><input type="number" name="qty['+products_array[i].product_id+']" class="qty" max='+ products_array[i].quantity +' min="0" value = 0 > </td>';
                        tablehtml += '<td><input type="text" name="serials['+products_array[i].product_id+']" class="serials"  value="'+ products_array[i].serialNo +'" > </td>';
                        tablehtml += '</tr>';

                    }

//
                     $('#product-table-body').html(tablehtml);

                     var grosshtml = '<tr>';
//                     grosshtml += '<td colspan="2"></td>';
//                     grosshtml += '<td style="text-align: right"><b>Gross Total</b></td> <td style="text-align: center">'+ data.sales.grand_total +'</td></tr>';
//                     $('#gross-table-body').html(grosshtml);

                     $('#users-table').css('display', 'inline-table')

                 }
                var addTable = function(table_data, gross_amount, tax_amount, tax_percent){


                    if(table_data.length > 0){
                        var table = document.getElementById("product-table-body");
                        document.getElementById('users-table').style.display = "table";

                        for (var i = 0; i < table_data.length; i++) {
                            var row = table.insertRow(i);

                            for (var j = 0; j < table_data[i].length; j++) {
                                var cell = row.insertCell(j);
                                cell.innerHTML = table_data[i][j];
                            }

                        }

                        //For Amount
                        var row = table.insertRow(table_data.length);
                        for(var i=0; i < 6; i++){
                            var cell = row.insertCell(i);
                            if(i==4){
                                cell.setAttribute("style", "font-weight: bold;");
                                cell.innerHTML = "Amount";
                            }
                            else if(i==5){
                                cell.innerHTML = "Rs. "+gross_amount.toFixed(2);
                            }
                            else
                                cell.innerHTML = " ";
                        }

                        //For Tax
                        var row = table.insertRow(table_data.length+1);
                        for(var i=0; i < 6; i++){
                            var cell = row.insertCell(i);
                            if(i==4){

                                cell.setAttribute("style", "font-weight: bold;");
                                cell.innerHTML = tax_percent+"% GST";
                            }
                            else if(i==5){
                                cell.innerHTML = "Rs. "+tax_amount.toFixed(2);
                            }
                            else
                                cell.innerHTML = " ";
                        }

                        //For Gross Amount
                        var row = table.insertRow(table_data.length+2);
                        for(var i=0; i < 6; i++){
                            var cell = row.insertCell(i);
                            if(i==4){
                                cell.setAttribute("style", "font-weight: bold;");
                                cell.innerHTML = "Gross Amount";
                            }
                            else if(i==5){
                                cell.innerHTML = "Rs. "+ (+tax_amount.toFixed(2) + +gross_amount.toFixed(2));
                            }
                            else
                                cell.innerHTML = " ";
                        }


                    }
                    else{
                        document.getElementById('users-table').style.display = "none";
                        document.getElementById('due_date').setAttribute('readonly', 'true');
                        document.getElementById('rr_no').setAttribute('readonly', 'true');
                        //document.getElementById('btn_save').setAttribute('disabled', 'true');
                        document.getElementById('delivery_type').setAttribute('disabled', 'true');
                    }
                }

                return {
                    // public functions
                    init: function() {
                        baseFunction();
                    },
                    fetchFilterrecord : fetchFilterrecord,
                    addTable : addTable,
                };
            }();

        jQuery(document).ready(function() {
            FormControls.init();
        });
    </script>
@endsection


