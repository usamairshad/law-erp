<!DOCTYPE>
<!DOCTYPE html>
<html>
<head>
    <title>ERP Invoice Table</title>
</head>
<body>
    <!-------------------
    heading  sufi wheat
   -------------------->
<div style="background: #000; color: #ffffff; margin-bottom: 0;">
    <h2 style="  margin-top: 5px; margin-bottom: 0; text-align: center"><strong>OAG</strong></h2>
    <h3  style=" margin-top: 5px; margin-bottom: 0;  text-align: center">Addrees</h3>
    <h4  style=" margin: 5px 0 0 0;  text-align: center">CREDIT BOOK</h4>
</div>

    <!-------------------
     Gate-Pass Details
    -------------------->


    <table style="border:1px solid #000000;  margin:0 0 20px 0; " width="100%" >
        <tbody style="padding: 100px;">
        <tr>
            <th width="10%">Invoice No</th>
            <td>{{ $SalesInvoiceMode->invoice_no }}</td>
            <th width="10%">Invoice Date</th>
            <td>{{ $SalesInvoiceMode->invoice_date }}</td>
            <th width="10%">Customer Name</th>
            <td>{{ $SalesInvoiceMode->CustomersModel->name }}</td>
        </tr>

        <tr>

            <th width="10%">RR No</th>
            <td>{{ $SalesInvoiceMode->rr_no }}</td>
            {{--<th width="10%">Amount</th>--}}
            {{--<td>{{ $SalesInvoiceMode->amount }}</td>--}}
            {{--<th width="10%">Tax Amount</th>--}}
            {{--<td>{{ $SalesInvoiceMode->tax_amount}}</td>--}}
        </tr>
        {{--<tr>--}}
            {{--<th width="10%">Grand Total</th>--}}
            {{--<td>{{ $SalesInvoiceMode->total_amount}}</td>--}}
        {{--</tr>--}}

        </tbody>
    </table>

        <!-------------------
        Invoice Products Details
        -------------------->

        <table style="border-collapse:collapse; border: 1px solid black; text-align: left; margin:20px 0; " width="100%" cellpadding="5">
            <thead>
            <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            @forelse($Products as $product)
                <tr>
                    <td> <b> {{ $product->productsModel->products_name.$product->productsModel->products_description }} </b> [{{$product->serialNo}}]</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->unit_price }}</td>
                    <td>{{ $product->total_price }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">No Record Found</td>
                </tr>
            @endforelse

            @if(count($Services) > 0 )
                <tr>
                    <td colspan="4"><hr></td>
                </tr>
                @foreach($Services as $service)
                <tr>
                    <td>{{ $service->description }}</td>
                    <td> // </td>
                    <td>{{ $service->rate }}</td>
                    <td>{{ $service->amount }}</td>

                </tr>
                @endforeach
            @endif
            </tbody>
        </table>

            <!-------------------
             Invoice fotter
             -------------------->

    <table width="50%" style="float: right" cellspacing="10">
        <tbody>
        <tr>
            <td  style="width: 50%; float: left;">
                <strong>Products Amount</strong>
            </td>

            <td style="width: 50%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left;">
                    {{$SalesInvoiceMode->products_amount}}
                </span>
            </td>
        </tr>
        <tr>
            <td  style="width: 70%; float: left;">
                <strong>Products Tax Amount</strong>
            </td>

            <td style="width: 30%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left;">
                    {{$SalesInvoiceMode->products_tax}}
                </span>
            </td>
        </tr>
@if(count($Services) > 0)
        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Services Amount</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left;">
                    {{$SalesInvoiceMode->services_amount}}
                </span>
            </td>
        </tr>
        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Services Tax Amount</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left;">
                    {{$SalesInvoiceMode->services_tax}}
                </span>
            </td>
        </tr>
@endif
        <tr>
            <td  style="width: 20%; float: left;">
                    <strong>Tax Amount</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left;">
                    {{$SalesInvoiceMode->tax_amount}}
                </span>
            </td>

        </tr>
        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Amount</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left; border-bottom: 1px solid #000000;">
                    {{$SalesInvoiceMode->amount}}
                </span>
            </td>
        </tr>
        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Total Amount</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000;">
                <span style="width: 65%;  float: left; border-bottom: 1px solid #000000;">
                    {{$SalesInvoiceMode->total_amount}}
                </span>
            </td>
        </tr>
        <tr>
            <td  colspan="2" >
                {{ $digit->format($SalesInvoiceMode->total_amount) }}
            </td>
        </tr>


        <tr>
            <td  style="width: 20%; float: left;">
                <strong>Signature</strong>
            </td>

            <td style="width: 80%;  float: left; border-bottom: 1px solid #000000; ">
            </td>
        </tr>


        </tbody>
    </table>


</body>
</html>