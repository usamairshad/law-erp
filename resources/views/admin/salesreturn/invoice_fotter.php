

                                   <div class="col-lg-12 amount_section">

                                       <div class="col-lg-4 pull-right">
                                           <div class="form-group">
                                               <div class="no-padding col-lg-3">
                                                   <label class="pull-left">Total</label>
                                               </div>
                                               <div class="col-lg-9">
                                                    <input name="total_of_product" type="text" class="form-control" id="total_of_product"  required readonly/>
                                               </div>
                                           </div>

                                           <div class="form-group">
                                               <div class="no-padding col-lg-3">
                                                   <label class="pull-left">Tax</label>
                                               </div>
                                               <div class="col-lg-9">
                                                   <input name="total_tax" type="text" class="form-control" min="0" id="total_tax" required/>
                           </div>
                                           </div>
                                           <div class="form-group">
                                               <div class="no-padding col-lg-3">
                                                   <label class="pull-left">Freight</label>
                                               </div>
                                               <div class="col-lg-9">
                                                   <input name="total_freight" type="text" class="form-control" min="0" id="total_freight" required />
                                               </div>
                                           </div>
                                           <div class="form-group">
                                               <div class="no-padding col-lg-3">
                                                   <label class="pull-left">Discount</label>
                                               </div>
                                               <div class="col-lg-9">
                                             <input name="total_discount" type="text" class="form-control"  min="0" id="total_discount" required />

                                               </div>
                                           </div>
                                           <div class="form-group">
                                               <div class="no-padding col-lg-3">
                                                   <label class="pull-left">Grand Total</label>
                                               </div>
                                               <div class="col-lg-9">
                                                   <input name="grand_total" type="text" class="form-control" id="grand_total" required />
                                               </div>
                                           </div>

                                       </div>
                                   </div>
