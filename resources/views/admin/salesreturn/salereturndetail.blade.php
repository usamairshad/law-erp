@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale Return Detail</h1>
    </section>
@stop
<style>
    .box-header a:last-child{
        margin-right: 10px;
    }
</style>
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">View</h3>
            <a href="{{ route('admin.salesreturn.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th width="10%">Invoice No</th>
                        <td>{{ $SaleReturn->invoice->invoice_no }}</td>
                        <th width="10%">Return Date</th>
                        <td>{{ $SaleReturn->return_date }}</td>
                        <th width="10%">Customer Name</th>
                        <td>{{ $SalesInvoiceDetailModel->CustomersModel->name }}</td>
                    </tr>
                    <tr>
                        <th width="10%">Description</th>
                        <td colspan="5">{{ $SaleReturn->description ? $SaleReturn->description : 'N/A' }}</td>
                    </tr>

                </tbody>
            </table>

            <!-- Slip Area Started -->
            <section class="content-header" style="padding: 10px 15px !important;">
                <h1>Product View</h1>
            </section>


            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Return Quantity</th>
                    <th>Original Quantity</th>
                    <th>Returned Serial</th>
                </tr>
                </thead>
                <tbody>
                @forelse($SaleReturnDetail as $detail)
                    <tr>
                        <td><b>{{ $detail->productsModel->products_name }}</b> </td>
                        <td>{{ $detail->return_qty }}</td>
                        <td>{{ $detail->original_qty }}</td>
                        <td>{{ $detail->return_serial }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No Record Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>


        </div>
        <!-- /.box-body -->
    </div>






@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.products.mass_destroy') }}';
    </script>
@endsection

