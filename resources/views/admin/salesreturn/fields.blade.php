
<div id="11" class="panel-collapse collapse in col-md-12">

    {{ csrf_field() }}
    <div class="form-group col-md-3 @if($errors->has('invoice_id')) has-error @endif" id="order_no_div">
        {!! Form::label('invoice_id', 'Invoice No. ', ['class' => 'control-label']) !!}
        {{-- {!! Form::select('invoice_id', $invoices , old('invoice_id'),
        ['class' => 'form-control select2'  ,'id' => 'invoice_id', 'onchange' => 'FormControls.fetchFilterrecord();']) !!} --}}
       
        <select name="invoice_id" id="invoice_id" class="form-control select2" onchange="FormControls.fetchFilterrecord();">
            <option value="">Select Invoice</option>
            @foreach ($invoices as $item)
                <option value="{{$item->id}}">{{$item->invoice_no}}</option>
            @endforeach
        </select>
    </div>


    <div class="form-group col-md-3 @if($errors->has('return_date')) has-error @endif">
        {!! Form::label('return_date', 'Return Date', ['class' => 'control-label']) !!}
        {!! Form::text('return_date', old('return_date'), ['class' => 'form-control datepicker','id'=> 'return_date']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('invoice_date')) has-error @endif">
        {!! Form::label('invoice_date', 'Invoice Date', ['class' => 'control-label']) !!}
        {!! Form::text('invoice_date', old('invoice_date'), ['class' => 'form-control','readonly' => 'true','id'=> 'invoice_date']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('due_date')) has-error @endif">
        {!! Form::label('due_date', 'Due Date', ['class' => 'control-label']) !!}
        {!! Form::text('due_date', old('due_date'), ['class' => 'form-control','readonly' => 'true','id'=> 'due_date']) !!}
        @if($errors->has('due_date'))
            <span class="help-block">
                {{ $errors->first('due_date') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('rr_no')) has-error @endif">
        {!! Form::label('rr_no', 'R.R No', ['class' => 'control-label']) !!}
        {!! Form::text('rr_no', old('rr_no'), ['class' => 'form-control','readonly' => 'true','id'=> 'rr_no']) !!}
        @if($errors->has('rr_no'))
            <span class="help-block">
                {{ $errors->first('rr_no') }}
            </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('amount')) has-error @endif">
        {!! Form::label('amount', 'Amount', ['class' => 'control-label']) !!}
        {!! Form::text('amount', old('amount'), ['class' => 'form-control','readonly' => 'true','id'=> 'amount']) !!}
        @if($errors->has('amount'))
            <span class="help-block">
                {{ $errors->first('amount') }}
            </span>
        @endif
    </div>



    <div class="form-group col-md-3 @if($errors->has('tax_amount')) has-error @endif">
        {!! Form::label('tax_amount', 'Tax Amount', ['class' => 'control-label']) !!}
        {!! Form::text('tax_amount', old('tax_amount'), ['class' => 'form-control','readonly' => 'true','id'=> 'tax_amount']) !!}
        @if($errors->has('tax_amount'))
            <span class="help-block">
                {{ $errors->first('tax_amount') }}
            </span>
        @endif
    </div>



    <div class="form-group col-md-3 @if($errors->has('grand_total')) has-error @endif">
        {!! Form::label('grand_total', 'Grand Total', ['class' => 'control-label']) !!}
        {!! Form::text('grand_total', old('grand_total'), ['class' => 'form-control','readonly' => 'true','id'=> 'grand_total']) !!}
        @if($errors->has('grand_total'))
            <span class="help-block">
                {{ $errors->first('grand_total') }}
            </span>
        @endif
    </div>


    <div class="form-group col-md-12 @if($errors->has('description')) has-error @endif">
        {!! Form::label('des', 'Description :', ['class' => 'control-label']) !!}
        {!! Form::textarea('description', old('description'), ['class' => 'form-control','id'=> 'description']) !!}
        @if($errors->has('description'))
            <span class="help-block">
                {{ $errors->first('description') }}
            </span>
        @endif
    </div>


    {!! Form::hidden('services_amount', old('services_amount'), [ 'id' => 'services_amount']) !!}


</div>
<div class="panel-body pad table-responsive">
    <table class="table table-bordered table-striped " id="product-table">
        <thead>
        <tr>
            <th>Serials</th>
            <th>Product</th>
            <th>Quantity</th>
            <th>Serials</th>
            <th>Rate</th>
            <th>Amount</th>
            <th>Already Returned</th>
            <th>Return Quantity</th>
            <th>Serial</th>
        </tr>
        </thead>
        <tbody id="product-table-body">

        </tbody>
    </table>
</div>