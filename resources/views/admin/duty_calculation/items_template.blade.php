<table style="display: none;">
    <tbody id="line_item-container" >
    <tr id="InventoryItems-######">
        <td>
            <div class="form-group  @if($errors->has('line_duty_name')) has-error @endif">

                {!! Form::select('InventoryItems[duty_name][########]',$DutyModel, old('duty_name'), ['id' => 'InventoryItems-duty_name_id-1','class' => 'form-control', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                @if($errors->has('line_duty_name'))
                        <span class="help-block">
                            {{ $errors->first('line_duty_name') }}
                            </span>
                @endif
            </div>
        </td>
      
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('InventoryItems[percent][########]', old('percent'), ['id' => 'InventoryItems-percent-1','class' => 'form-control' , 'required' => 'true','min'=>'1']) !!}
            </div>
        </td>
        <td><button id="InventoryItems-del_btn-######" onclick="FormControls.destroyLineItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>
    </tbody>
</table>