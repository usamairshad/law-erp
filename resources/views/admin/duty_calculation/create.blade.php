@extends('layouts.app')
@section('stylesheet')

@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty Calculation</h1>
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create</h3>
            <a href="{{ route('admin.account_reports.duty_report') }}" class="btn btn-success pull-right">Back</a>
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.account_reports.duty_store'], 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        
                       
                </div>
            </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="about_featured">
                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default wow fadInLeft">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#11">
                                                <span class="fa fa-check-square-o"></span><b>Duty List</b>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="11" class="panel-collapse collapse in">


                                        <div class="panel-body pad table-responsive">
                                            <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>D</u>uty</button>
                                            <table class="table table-bordered table-striped" id="users-table">
                                                <thead>
                                                <tr>
                                                    <th>Duty</th>
                                                    <th>Percentage(%)</th>
                                                    <th>Action</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php  $counter=0 ?>
                                                <tr id="line_item-1">
                                                    <td>
                                                        <div class="form-group  @if($errors->has('line_duty_name')) has-error @endif">

                                                            {!! Form::select('InventoryItems[duty_name][1]',$DutyModel, old('duty_name'), ['id' => 'InventoryItems-unit_name_id-1','class' => 'form-control', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                                                            @if($errors->has('line_duty_name'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('line_duty_name') }}
                                                                        </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                   
                                                    <td>
                                                        <div class="form-group  @if($errors->has('amount')) has-error @endif">

                                                            {!! Form::number('InventoryItems[percent][1]', old('percent'), ['id' => 'InventoryItems-percent-1','class' => 'form-control' , 'required' => 'true','min'=>'1']) !!}
                                                            @if($errors->has('amount'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('amount') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td><button id="InventoryItems-del_btn-1" onclick="FormControls.destroyLineItem('0');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>

                                                </tr>
                                                <input type="hidden" id="InventoryItems-global_counter" value="<?php  echo ++$counter ?>"   />

                                                </tbody>

                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    @include('admin.duty_calculation.items_template')
                </div>
                <div id="stockitems_submit" class="col-xs-12">
                    <div class="col-xs-6">
                        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    {!! Form::close() !!}
    </div>
@stop
@section('javascript')
    <script src="{{ url('public/js/admin/duty_calculation/duty_calculation_create_modify.js') }}" type="text/javascript"></script>
@endsection


