@extends('layouts.app')
@section('stylesheet')
    <style>
        .example-template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty Demand <span> <button onclick="myFunction()" class="btn btn-primary pull-right">Print</button></span> </h1>
       
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title text-center" style="text-align: center; display:block; font-weight:bold ">DUTY DEMAND</h3>
           
        </div>
       
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-6 form-group">
                        <h4>M/S OFFICE AUTOMATION GROUP.</h4>
                        <p>Lahore -- Pakistan</p>
                    </div>
                    <div class="col-xs-6 form-group text-right">
                        DATE: {{$mytime = Carbon\Carbon::now()}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <p>ATTN:</p>
                        <p>SUBJECT:</p>
                        <p>GOODS:</p>
                    </div>
                    <div class="col-xs-10 ">
                            <p>Mr.TAHIR SAEED BUTT</p>
                            <p>CUSTOM HANDLING OF CONSIGNMENT AT KILL</p>
                            <input type="text" id="product" class="form-control pro">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="col-xs-4 form-group @if($errors->has('lc_bank')) has-error @endif">
                            {!! Form::label('lc_bank', 'Lc Bank *', ['class' => 'control-label']) !!}
                            <select id="lc_bank" name="lc_bank" class="form-control select2">
                                @if ($Lc->count())
                                    <option value="">Select LC</option>
                                    @foreach($Lc as $lc)
                                        <option value="{{ $lc->id }}">{{ $lc->lc_no }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span id="sup_id_handler"></span>
                            @if($errors->has('sup_id'))
                                <span class="help-block">
                                    {{ $errors->first('sup_id') }}
                                 </span>
                            @endif
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
       
    </div>
 
    </div>
@stop
@section('javascript')

<script type="text/javascript">
function myFunction() {
  window.print();
}
    $(document).ready(function () {
        $('.pro').hide();

        $(document).on('change', '#lc_bank', function () {
             console.log("hmm its change");

            var lc_bank = $(this).val();
            console.log(lc_bank);
            var div = $(this).parent();

            var op = " ";

            $.ajax({
                type: 'get',
                url: '{!!URL::to('admin/reports/duty_products')!!}',
                data: {'id': lc_bank},
                success: function (data) {
                    console.log(data);
                    $('.pro').show();
                    $('#product').val(data);

                    div.innerHtml = 'Extra stuff';
                    // op+='<option value="0" selected disabled>chose product</option>';
                    for (var i = 0; i < data.length; i++) {
                        op += '<option value="' + data[i].id + '">' + data[i].products_name + ' ' + '</option>';
                        //alert(data[i].fname);
                    }
                    $('#product_id').html(" ");
                    $('#product_id').html(" ");
                    $('#product_id').append(op);
                },
                error: function () {

                }
            });
        });

        $(document).on('change', '#product_id', function () {
            var product_id = $(this).val();

            var a = $(this).parent();
           // console.log(product_id);
            var op = "";
            $.ajax({
                type: 'get',
                url: '{!!URL::to('Reports')!!}',
                data: {'product_id': product_id},
                dataType: 'json',//return data will be json
                success: function (data) {
                    //console.log("price");
                    // console.log(data.unit_price);
                    
                    //$('#price').html(" ");
                    $('#price').val(data.unit_price);
                    $('#quantity').val(data.qty);
                    $('#duty').show();

                    var percent_sum=0;
                    var value_sum=0;

                    $('.cal_per').each(function () {
                        
                        var rowId = $(this).attr('data-column-id');
                        var value = $('#percent-'+rowId).html();
                        var price = data.unit_price;
                        var calculate = value * price / 100;
                        $('#value-'+rowId).val(calculate);

                        percent_sum = percent_sum + parseInt(value);
                        value_sum = value_sum + parseInt(calculate);

                        $('#total_percent').val(percent_sum);
                        $('#total_value').val(value_sum);
                    //console.log(sum);
                });

                },
                error: function () {

                }
            });


        });


    });
</script>

@endsection


