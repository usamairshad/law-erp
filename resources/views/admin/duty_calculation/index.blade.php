@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty Calculation</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('stockitems_create'))
                <a href="{{ route('admin.account_reports.duty_create') }}" class="btn btn-success pull-right">Add New Duty</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped" id="stock-table">
                <thead>
                    <tr>
                        <th width="">Duty</th>
                        <th width="">Percentage</th>
                        <th width="">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($duties as $duty)
                    <tr>
                        <td width =70%>{{$duty->dutyName['duty']}}</td>
                        <td width="">{{ $duty->percentage }}%</td>
                        <td>
                            <form method="POST" class="delete" action="{{ route('admin.account_reports.duty_delete', $duty->id)}}">

                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" name="submit" value="Delete" class="btn btn-danger btn-xs">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   
@endsection

