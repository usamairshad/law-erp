@extends('layouts.app')
@section('stylesheet')
    <style>
        .example-template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty Calculation <span> <button onclick="myFunction()" class="btn btn-primary pull-right">Print</button></span> </h1>
       
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title text-center" style="text-align: center; display:block; font-weight:bold ">DUTY CALCULATION</h3>
           
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.inventorytransfer.store'], 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="col-xs-4 form-group @if($errors->has('lc_bank')) has-error @endif">
                            {!! Form::label('lc_bank', 'Lc Bank *', ['class' => 'control-label']) !!}
                            <select id="lc_bank" name="lc_bank" class="form-control select2">
                                @if ($Lc->count())
                                    <option value="">Select LC</option>
                                    @foreach($Lc as $lc)
                                        <option value="{{ $lc->id }}">{{ $lc->lc_no }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span id="sup_id_handler"></span>
                            @if($errors->has('sup_id'))
                                <span class="help-block">
                                    {{ $errors->first('sup_id') }}
                                 </span>
                            @endif
                        </div>
                        <div class="col-xs-4 form-group @if($errors->has('lc_bank')) has-error @endif">
                            {!! Form::label('product_id', 'Select Product', ['class' => 'control-label']) !!}
                            <select id="product_id" name="product_id" class="form-control select2">
                                <option value="">Select Product</option>
                            </select>
                            @if($errors->has('product_id'))
                                    <span class="help-block">
                                            {{ $errors->first('product_id') }}
                                    </span>
                            @endif
                        </div>
                        <div class="col-xs-2 form-group @if($errors->has('price')) has-error @endif">
                            {!! Form::label('price', 'Price', ['class' => 'control-label']) !!}
                            <input type="text" name="" id="price" class="form-control" readonly>
                          
                        </div>
                        <div class="col-xs-2 form-group @if($errors->has('quantity')) has-error @endif">
                            {!! Form::label('quantity', 'quantity', ['class' => 'control-label']) !!}
                            <input type="text" name="" id="quantity" class="form-control" readonly>
                          
                        </div>

                    </div>
                </div>
                {{-- <div class="box-footer">
                    {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                </div> --}}
            </div>
            <div class="panel-body pad table-responsive" id="duty">
                <table class="table table-bordered table-striped" id="stock-table">
                    <thead>
                        <tr>
                            <th width="">Duty</th>
                            <th width="">Percentage (%)</th>
                            <th width="">Calculation</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($duties as $duty)
                        @php
                            $count = $i++;
                        @endphp
                        <tr>
                        {{-- <td>{{$i++}}</td> --}}
                            <td width =70% >{{$duty->dutyName['duty']}}</td>
                            <td data-row-id="{{$count}}" id="percent-{{$count}}" class="cal_per">{{ $duty->percentage }}</td>
                            <td data-row-id="{{$count}}" id="value">
                                <input type="text" name="" id="value-{{$count}}" class="form-control" readonly value="">
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="2" class="text-center">Cost Available for Sale</th>
                    </tr>
                    <tr>
                        <th>Distributer GP</th>
                        <th><input type="text" name="" id="total_percent" class="form-control" readonly></th>
                        <th><input type="text" name="" id="total_value" class="form-control" readonly></th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
       
    </div>
    {!! Form::close() !!}
    </div>
@stop
@section('javascript')

<script type="text/javascript">
function myFunction() {
  window.print();
}
    $(document).ready(function () {
        $('#duty').hide();

        $(document).on('change', '#lc_bank', function () {
             console.log("hmm its change");

            var lc_bank = $(this).val();
            console.log(lc_bank);
            var div = $(this).parent();

            var op = " ";

            $.ajax({
                type: 'get',
                url: '{!!URL::to('Reports')!!}',
                data: {'id': lc_bank},
                success: function (data) {
                    //console.log('success');

                    op+='<option value="0" selected disabled>chose product</option>';
                    for (var i = 0; i < data.length; i++) {
                        op += '<option value="' + data[i].id + '">' + data[i].products_name + ' ' + '</option>';
                        //alert(data[i].fname);
                    }

                    $('#product_id').html(" ");
                    $('#product_id').append(op);
                },
                error: function () {

                }
            });
        });

        $(document).on('change', '#product_id', function () {
            var product_id = $(this).val();

            var a = $(this).parent();
           console.log(product_id);
            var op = "";
            $.ajax({
                type: 'get',
                url: '{!!URL::to('admin/reports/duty_report_all_prices')!!}',
                data: {'product_id': product_id},
                dataType: 'json',//return data will be json
                success: function (data) {
                    //console.log("price");
                    // console.log(data.unit_price);
                    
                    //$('#price').html(" ");
                    $('#price').val(data.unit_price);
                    $('#quantity').val(data.qty);
                    $('#duty').show();

                    var percent_sum=0;
                    var value_sum=0;

                    $('.cal_per').each(function () {
                        
                        var rowId = $(this).attr('data-row-id');
                        var value = $('#percent-'+rowId).html();
                        var price = data.unit_price;
                        var calculate = value * price / 100;
                        $('#value-'+rowId).val(calculate);

                        percent_sum = percent_sum + parseInt(value);
                        value_sum = value_sum + parseInt(calculate);

                        $('#total_percent').val(percent_sum);
                        $('#total_value').val(value_sum);
                    //console.log(sum);
                });

                },
                error: function () {

                }
            });


        });


    });
</script>

@endsection


