@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty Calculation</h1>
        <h1><button onclick="myFunction()" class="btn btn-primary pull-right">Print</button></h1>
    </section>
@stop

@section('content')

<div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title text-center" style="text-align: center; display:block; font-weight:bold ">DUTY DEMAND</h3>
           
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.account_reports.duty_demand'], 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="col-xs-4 form-group @if($errors->has('lc_bank')) has-error @endif">
                            {!! Form::label('lc_bank', 'Lc Bank *', ['class' => 'control-label']) !!}
                            <select id="lc_bank" name="lc_bank" class="form-control select2" required>
                                @if ($Lc->count())
                                    <option value="">Select LC</option>
                                    @foreach($Lc as $lc)
                                        <option value="{{ $lc->id }}">{{ $lc->lc_no }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span id="sup_id_handler"></span>
                            @if($errors->has('sup_id'))
                                <span class="help-block">
                                    {{ $errors->first('sup_id') }}
                                 </span>
                            @endif
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
         <div class="box-footer">
            {!! Form::submit(trans('Load Report'), ['class' => 'btn btn-danger pull-right']) !!}
        </div>
    </div>
    {!! Form::close() !!}
    


    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('stockitems_create'))
            @endif
        </div>
        <!-- /.box-header -->
       
        <div class="panel-body pad table-responsive">

            <div class="row">
                    <div class="col-xs-2">
                        <p>GOODS:</p>
                    </div>
                    <div class="col-xs-10 ">
                        @foreach ($data as $item)
                            
                        <p>{{$item->productsModel['products_name']}} Qty({{$item->qty}})</p>
                        @endforeach
                    </div>
            </div>
            <div class="panel-body pad table-responsive" id="duty">
                <table class="table table-bordered table-striped" id="stock-table">
                    <thead>
                    <tr>
                        <th width="">Duty</th>
                        <th width="">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($lc_duty as $duty)
                        @php
                            $count = $i++;
                        @endphp
                        <tr>
                            {{-- <td>{{$i++}}</td> --}}
                            <td width =70% >{{$duty->dutyModel['duty']}}</td>
                            <td data-row-id="{{$count}}" id="percent-{{$count}}" class="cal_per">{{ $duty->sum }}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <br>
          
        </div>
    </div>
@stop

@section('javascript')
<script type="text/javascript">
    function myFunction() {
      window.print();
    }
    </script>
@endsection

