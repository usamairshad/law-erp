@extends('layouts.app')
@section('stylesheet')
    <style>
        .example-template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty Demand <span> <button onclick="myFunction()" class="btn btn-primary pull-right">Print</button></span> </h1>
       
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title text-center" style="text-align: center; display:block; font-weight:bold ">DUTY DEMAND</h3>
           
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.account_reports.duty_demand'], 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <div class="col-xs-4 form-group @if($errors->has('lc_bank')) has-error @endif">
                            {!! Form::label('lc_bank', 'Lc Bank *', ['class' => 'control-label']) !!}
                            <select id="lc_bank" name="lc_bank" class="form-control select2" required>
                                @if ($Lc->count())
                                    <option value="">Select LC</option>
                                    @foreach($Lc as $lc)
                                        <option value="{{ $lc->id }}">{{ $lc->lc_no }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span id="sup_id_handler"></span>
                            @if($errors->has('sup_id'))
                                <span class="help-block">
                                    {{ $errors->first('sup_id') }}
                                 </span>
                            @endif
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
         <div class="box-footer">
            {!! Form::submit(trans('Load Report'), ['class' => 'btn btn-danger pull-right']) !!}
        </div>
    </div>
    {!! Form::close() !!}
    </div>
@stop
@section('javascript')

<script type="text/javascript">
function myFunction() {
  window.print();
}
    $(document).ready(function () {
        $('.pro').hide();

        $(document).on('change', '#lc_bank', function () {
             console.log("hmm its change");

            var lc_bank = $(this).val();
            console.log(lc_bank);
            var div = $(this).parent();

            var op = " ";

            $.ajax({
                type: 'get',
                url: '{!!URL::to('Reports')!!}',
                data: {'id': lc_bank},
                success: function (data) {
                    console.log(data);
                },
                error: function () {

                }
            });
        });
    });
</script>

@endsection


