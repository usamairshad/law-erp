@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Payment Methods</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('payment_methods_create'))
                <a href="{{ route('admin.payment_methods.create') }}" class="btn btn-success pull-right">Add New Payment Method</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($PaymentMethods) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Payment Type</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($PaymentMethods) > 0)
                    @foreach ($PaymentMethods as $PaymentMethod)
                        <tr data-entry-id="{{ $PaymentMethod->id }}">
                            <td>{{ $PaymentMethod->id }}</td>
                            <td>{{ $PaymentMethod->name }}</td>
                            <td>{{ Config::get('constants.payment_type')[$PaymentMethod->payment_type] }}</td>
                            <td>{{ $PaymentMethod->created_at }}</td>
                            <td>
                                @if($PaymentMethod->status && Gate::check('payment_methods_inactive'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['admin.payment_methods.inactive', $PaymentMethod->id])) !!}
                                    {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}
                                @elseif(!$PaymentMethod->status && Gate::check('payment_methods_active'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['admin.payment_methods.active', $PaymentMethod->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                                @endif

                                @if(Gate::check('payment_methods_edit'))
                                    <a href="{{ route('admin.payment_methods.edit',[$PaymentMethod->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                @if(Gate::check('payment_methods_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.payment_methods.destroy', $PaymentMethod->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection