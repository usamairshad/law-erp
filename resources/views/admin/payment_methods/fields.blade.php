<div class="form-group col-md-6 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
    @if($errors->has('name'))
        <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
    @endif
</div>
<div class="form-group col-md-6 @if($errors->has('payment_type')) has-error @endif">
    {!! Form::label('payment_type', 'Payment Type*', ['class' => 'control-label']) !!}
    {!! Form::select('payment_type', array('' => 'Select a Payment Type') + Config::get('constants.payment_type'), old('payment_type'), ['class' => 'form-control']) !!}
    @if($errors->has('payment_type'))
        <span class="help-block">
            {{ $errors->first('payment_type') }}
        </span>
    @endif
</div>