@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Payment Methods</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Payment Method</h3>
            <a href="{{ route('admin.payment_methods.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($PaymentMethod, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.payment_methods.update', $PaymentMethod->id]]) !!}
        <div class="box-body">
            @include('admin.payment_methods.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/admin/payment_methods/create_modify.js') }}" type="text/javascript"></script>
@endsection