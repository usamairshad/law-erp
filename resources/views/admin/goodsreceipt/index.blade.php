@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Goods Receipt</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('stockitems_create'))
                <a href="{{ route('admin.goodsreceipt.create') }}" class="btn btn-success pull-right">Add New Goods</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($GoodsReceiptModel) > 0 ? 'datatable' : '' }}" id="receipt-table">
                <thead>
                <tr>
                    <th width="">S No:</th>
                    <th>Branch From</th>
                    <th>Branch To</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            $('#receipt-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.goodsreceipt.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'branch_to', name: 'branch_to' },
                    { data: 'branch_from', name: 'branch_from' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},

                ]
            });
        });
    </script>
@endsection

