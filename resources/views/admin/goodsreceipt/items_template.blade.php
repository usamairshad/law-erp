<table style="display: none;">
    <tbody id="line_item-container" >
    <tr id="InventoryItems-######">
        <td >
            <div class="form-group" style="margin-bottom: 0px !important;">
                <div class="form-group" style="margin-bottom: 0px !important;">

                 {!! Form::select('InventoryItems[product_id][######]',array(), old('product_id'), ['id' => 'InventoryItems-product_id-######','class' => 'form-control select2 dublicate_product', 'required' => 'true']) !!}
            </div>
            </div>
        </td>
        <td>

            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::select('InventoryItems[unit_name][######]',$UnitsModel, old('unit_name'), ['id' => 'InventoryItems-unit_name_id-######','class' => 'form-control select2', 'style'=>'width:100%;' , 'required' => 'true']) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;" >

                {!! Form::number('InventoryItems[qunity][######]', old('qunity'), ['id' => 'InventoryItems-qunity_id-######','class' => 'form-control qty' , 'required' => 'true','min'=>'1']) !!}

            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('InventoryItems[p_amount][######]', old('p_amount'), ['id' => 'InventoryItems-p_amount_id-######','class' => 'form-control test' ,'data-row-id'=>'######', 'required' => 'true','min'=>'1']) !!}
            </div>
        </td>
        <td>
            <div class="form-group" style="margin-bottom: 0px !important;">
                {!! Form::number('InventoryItems[amount][######]', old('amount'), ['id' => 'InventoryItems-amount_id-######','class' => 'form-control' , 'required' => 'true','min'=>'1']) !!}
            </div>
        </td>
        <td><button id="InventoryItems-del_btn-######" onclick="FormControls.destroyLineItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>
    </tbody>
</table>