
            <div class="col-xs-4 form-group @if($errors->has('st_name')) has-error @endif">
                {!! Form::label('st_name', 'Product *', ['class' => 'control-label']) !!}
                <select id="st_name" name="st_name" class="form-control select2">
                    @if ($results->count())
                        <option value="">Select Product</option>
                        @foreach($results as $result)
                            <option value="{{ $result->id }}">{{ $result->products_name}}</option>
                        @endforeach
                    @endif
                </select>
                @if($errors->has('st_name'))
                    <span class="help-block">
                    {{ $errors->first('st_name') }}
                    </span>
                @endif
            </div>