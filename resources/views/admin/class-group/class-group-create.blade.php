@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Class Group</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <a href="{{  route('admin.class-group.index') }}"  class="btn btn-success pull-right">Back
                        </a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.class-group.store'], 'id' => 'validation-form']) !!}
        <div id="show">
            <div id="box" class="box-body">
                <h2>Create Class Group</h2>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('board_id', 'Boards*', ['class' => 'control-label']) !!}
                        <select id="board" class="form-control select2 boards" name="board_id" required="required">
                            <option selected="selected" disabled="disabled">---Select----</option>
                            @foreach($boards as $key => $board)
                            <option value="{{$board['id']}}">{{$board['name']}}</option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif">
                    {!! Form::label('programs', 'Programs*', ['class' => 'control-label']) !!}

                    <select class="form-control select2 programs" name="program_id" id="programs" required="required"></select>

                </div>

                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('class_id', 'Program Group*', ['class' => 'control-label']) !!}
                        <select class="form-control select2 classes" name="program_group_id" id="classes" required="required"></select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('course_group_id', 'Course Group*', ['class' => 'control-label']) !!}

                        <select class="form-control select2 courses" name="course_group_id" id="courses" required="required"></select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('class_id', 'Class Group Name', ['class' => 'control-label']) !!}
                        <input type="text" placeholder="Enter Course Group Name" name="name" class="form-control" id="courses">
                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('class_id', 'Select Class', ['class' => 'control-label']) !!}
                        <select name="class_id" class="form-control select2">
                          <option selected="selected" disabled="disabled"></option>
                          @foreach($classes as $class)
                          <option value="{{$class->id}}">{{$class->name}}</option>
                          @endforeach
                        </select>
                </div>
          
                {{--<div class="form-group col-md-1">
                    <button style="margin-top: 22px" type="button" name="add" id="add" class="btn btn-success add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>--}}
            </div> 
            <div id="secondbox">
                
            </div>  
            <button id="btn"  class="btn btn-success col-md-12" > Save</button>
            
        </div>

        <!-- /.box-body -->
       
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
       $( document ).ready(function() {
            document.getElementById('btn').setAttribute("disabled",null);
            $( document ).change(function(){
                var e = document.getElementById("courses");
                var strUser = e.value;
                if (strUser != "") {
                document.getElementById('btn').removeAttribute("disabled");
                } else {
                    document.getElementById('btn').setAttribute("disabled", null);
                }
            })
            
        });
    
         $(".boards").select2({

            });
    </script>
    <script type="text/javascript">
        function selectRefresh() {
            $(".programs").select2({

            });
        }
        function selectRefresh() {
            $(".classes").select2({

            });
        }
        function selectRefresh() {
            $(".courses").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
    <script type="text/javascript">

    // function showBranch() {
    //     var data= $('input[name="board_name"]').val();

    //     if(data)
    //     {
    //          $('#show').show();
    //          $('#btn').hide(); 
    //     }
    // }
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
    <script>
        $(function(){
            $('#board').change(function(){
               $("#programs option").remove();;
               var id = $(this).val();
               $.ajax({
                  url : '{{ route( 'admin.board-loadPrograms' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {
  
                        $('#programs').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        $.each( result, function(k, v) {
                           $('#programs').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });

        $(function(){
            $('#programs').change(function(){
               $("#classes option").remove();;
               var id = $(this).val();
               var board_id =$("select[name*='board_id']").val();

               $.ajax({
                  url : '{{ route( 'admin.load-board-program-program-group' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "program_id": id,
                    "board_id":board_id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {     
                        
                        $('#classes').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        
                        $.each( result, function(k, v) {
                           $('#classes').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
        $(function(){
            $('#classes').change(function(){
               $("#courses option").remove();;
               // var id = $(this).val();
               var program_group_id =$("select[name*='program_group_id']").val();
         
               $.ajax({
                  url : '{{ route( 'admin.load-program-group-courses-group' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",

                    "program_group_id":program_group_id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {     
                        console.log(result)    
                        $('#courses').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        
                        $.each( result, function(k, v) {
                           $('#courses').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
@endsection

