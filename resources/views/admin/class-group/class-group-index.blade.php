@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Class Group</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a  href="{{route('admin.class-group.create')}}" class="btn btn-success pull-right">Create Class Group</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($cg) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Board</th>
                    <th>Program</th>
                    <th>Course Group Name</th>
                </tr>
                </thead>

                <tbody> 
                @if (count($cg) > 0)
                    @foreach ($cg as $c)
                        <tr data-entry-id="{{ $c['id'] }}">
                            <td>{{ $c->courseGroup->programGroup->boardProgram->board->name}}</td>
                            <td>{{ $c->courseGroup->programGroup->boardProgram->program->name}}</td>
                            <td>{{ $c['name'] }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection