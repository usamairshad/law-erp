@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Country</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('erp_countries_create'))
                <a href="{{ route('admin.country.create') }}" class="btn btn-success pull-right">Add New Country</a>
            @endif
        </div>


        <div class="panel-body table-responsive">
            <table class="table table-bBanked table-striped {{ count($Country) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>S: No</th>
                        <th>Country Name</th>
                        <th>Action</th>

                    </tr>
                </thead>

                <tbody>
                    @php($r = 1)
                    @if (count($Country) > 0)
                        @foreach ($Country as $Countrys)

                            <tr data-entry-id="{{ $Countrys->id }}">
                                <td> {{$r}} </td>
                                <td>{{ $Countrys->name }}</td>
                                <td>
                                    <a href="{{ route('admin.country.edit',[$Countrys->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                    {{--{!! Form::open(array(--}}
                                        {{--'style' => 'display: inline-block;',--}}
                                        {{--'method' => 'DELETE',--}}
                                        {{--'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",--}}
                                        {{--'route' => ['admin.banks.destroy', $Bank->id])) !!}--}}
                                    {{--{!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}--}}
                                    {{--{!! Form::close() !!}--}}
                                </td>

                            </tr>
                            @php($r++)
                        @endforeach

                    @else
                        <tr>
                            <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')

@endsection