@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Country</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Country</h3>
            <a href="{{ route('admin.country.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Country, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.country.update', $Country->id]]) !!}
        <div class="box-body">
            <div class="col-xs-6 form-group @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Country Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>30,'required','minlength'=>2]) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                            {{ $errors->first('name') }}
                        </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/country/country_create_modify.js') }}" type="text/javascript"></script>
@endsection