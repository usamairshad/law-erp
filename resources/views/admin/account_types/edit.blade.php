@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Account Types</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Account Type</h3>
            <a href="{{ route('admin.account_types.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($AccountType, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.account_types.update', $AccountType->id]]) !!}
        <div class="box-body">
            @include('admin.account_types.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/admin/account_types/create_modify.js') }}" type="text/javascript"></script>
@endsection