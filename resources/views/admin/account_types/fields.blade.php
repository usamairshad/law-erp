<div class="form-group col-md-8 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'3','maxlength'=>'20',  'pattern' => '[a-zA-Z][a-zA-Z ]{2,}']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>
<div class="form-group col-md-4 @if($errors->has('code')) has-error @endif">
    {!! Form::label('code', 'Code*', ['class' => 'control-label']) !!}
    {!! Form::text('code', old('code'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'3']) !!}
    @if($errors->has('code'))
        <span class="help-block">
            {{ $errors->first('code') }}
        </span>
    @endif
</div>