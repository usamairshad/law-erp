@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Account Types</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('account_types_create'))
                <a href="{{ route('admin.account_types.create') }}" class="btn btn-success pull-right">Add New Account Type</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($AccountTypes) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th width="50%">Name</th>
                    <th>Code</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($AccountTypes) > 0)
                    @foreach ($AccountTypes as $AccountType)
                        <tr data-entry-id="{{ $AccountType->id }}">
                            <td>{{ $AccountType->id }}</td>
                            <td>{{ $AccountType->name }}</td>
                            <td>{{ $AccountType->code }}</td>
                            <td>{{ $AccountType->created_at }}</td>
                            <td>
                                @if($AccountType->status && Gate::check('account_types_inactive'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['admin.account_types.inactive', $AccountType->id])) !!}
                                    {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}
                                @elseif(!$AccountType->status && Gate::check('account_types_active'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['admin.account_types.active', $AccountType->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                                @endif

                                @if(Gate::check('account_types_edit'))
                                    <a href="{{ route('admin.account_types.edit',[$AccountType->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                @if(Gate::check('account_types_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.account_types.destroy', $AccountType->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection