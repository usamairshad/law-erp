@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Currency</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('erp_currencies_create'))
                <a href="{{ route('admin.currency.create') }}" class="btn btn-success pull-right">Add New Currency</a>
            @endif
        </div>


        <div class="panel-body table-responsive">
            <table class="table table-bBanked table-striped {{ count($currencys) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>S: No</th>
                        <th>Currency Name</th>
                        <th>Code</th>
                        <th>Symbol</th>
                        <th>Rate</th>
                        <th>Action</th>

                    </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($currencys) > 0)
                    @foreach ($currencys as $currency)
                        <tr data-entry-id="{{ $currency->id }}">
                            <td> {{$r}} </td>
                            <td>{{ $currency->name }}</td>
                            <td>{{ $currency->code}}</td>
                            <td>{{ $currency->symbol}}</td>
                            <td>{{ $currency->rate}}</td>
                            <td>
                                {{--@if(Gate::check('regions_edit'))--}}
                                <a href="{{ route('admin.currency.edit',[$currency->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                {{--@endif--}}

                                {{-- @if(Gate::check('regions_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.regions.destroy', $Region->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif --}}
                            </td>
                            </td>

                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')

@endsection