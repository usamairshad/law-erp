@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Currency</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create</h3>
            <a href="{{ route('admin.currency.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.currency.store'], 'id' => 'validation-form']) !!}
            <div class="box-body">
                <div class="col-xs-6 form-group @if($errors->has('name')) has-error @endif">
                    {!! Form::label('name', 'Currency Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>30,'required','minlength'=>2]) !!}
                    @if($errors->has('name'))
                        <span class="help-block">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                </div>
                <div class="col-xs-6 form-group @if($errors->has('code')) has-error @endif">
                    {!! Form::label('code', 'Code*', ['class' => 'control-label']) !!}
                    {!! Form::text('code', old('code'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>30,'required','minlength'=>2]) !!}
                    @if($errors->has('code'))
                        <span class="help-block">
                            {{ $errors->first('code') }}
                        </span>
                    @endif
                </div>
                <div class="col-xs-6 form-group @if($errors->has('symbol')) has-error @endif">
                    {!! Form::label('symbol', 'Symbol*', ['class' => 'control-label']) !!}
                    {!! Form::text('symbol', old('symbol'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>30,'required','minlength'=>2]) !!}
                    @if($errors->has('symbol'))
                        <span class="help-block">
                            {{ $errors->first('symbol') }}
                        </span>
                    @endif
                </div>
                <div class="col-xs-6 form-group @if($errors->has('rate')) has-error @endif">
                    {!! Form::label('rate', 'Rate*', ['class' => 'control-label']) !!}
                    {!! Form::text('rate', old('rate'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>30,'required','minlength'=>2]) !!}
                    @if($errors->has('rate'))
                        <span class="help-block">
                            {{ $errors->first('rate') }}
                        </span>
                    @endif
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')


<script src="{{ url('public/js/admin/currency/currency_create_modify.js') }}" type="text/javascript"></script>
@endsection