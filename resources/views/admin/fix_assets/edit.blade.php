@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Products</h1>
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create</h3>
            <a href="{{ route('admin.products.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.fix_assets.update'],'enctype' => 'multipart/form-data', 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-3 form-group @if($errors->has('products_name')) has-error @endif">
                        {!! Form::label('products_name', 'Product Name *', ['class' => 'control-label']) !!}
                        {!! Form::text('products_name', old('products_name'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                        @if($errors->has('products_name'))
                            <span class="help-block">
							{{ $errors->first('products_name') }}
							</span>
                        @endif
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('products_date')) has-error @endif">
                        {!! Form::label('products_date', 'Date*', ['class' => 'control-label']) !!}
                        {!! Form::text('products_date', old('products_date'), ['class' => 'form-control datepicker', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('products_date') }}
                                </span>
                        @endif
                    </div>

                    <div class="col-xs-6 form-group">
                        {!! Form::label('parent_id', 'Chart Of Account', ['class' => 'control-label']) !!}
                        <select name="parent_id" id="parent_id" class="form-control select2" style="width: 100%;" required>
                            <option value=""> Select Chart Of Account </option>
                            {!! $Groups !!}
                        </select>
                        <span id="parent_id_handler"></span>
                        @if($errors->has('parent_id'))
                            <span class="help-block">
                            {{ $errors->first('parent_id') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-3 form-group @if($errors->has('products_name')) has-error @endif">
                        {!! Form::label('item_is', 'Item is*', ['class' => 'control-label col-xs-12 row']) !!}
                        {!! Form::label('item_is', 'Use &emsp;  ', ['class' => '']) !!}
                        {!! Form::radio('item_is', '1', (old('item_is') ==  '1'), array('id'=>'item_is')) !!}
                        &emsp;
                        {!! Form::label('item_is', 'New &emsp; ', ['class' => '']) !!}
                        {!! Form::radio('item_is', '0', (old('item_is') ==  '0'), array('id'=>'item_is')) !!}
                    </div>

                    <div class="col-xs-3 form-group @if($errors->has('products_name')) has-error @endif">
                        {!! Form::label('customer_name', 'Customer Name *', ['class' => 'control-label']) !!}
                        {!! Form::text('customer_name', old('customer_name'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                        @if($errors->has('customer_name'))
                            <span class="help-block">
customer_name							</span>
                        @endif
                    </div>

                    <div class="col-xs-6 form-group @if($errors->has('branch')) has-error @endif">
                        <div class="@if($errors->has('branch')) has-error @endif">
                            {!! Form::label('branch', 'Branch*', ['class' => 'control-label']) !!}
                            <select id="branch" name="branch" class="form-control select2" data-placeholder="Select Branch" required>
                                <option value="">Select branch Type</option>
                                @foreach($Branches as $key =>$value)
                                    <option value="{{$key}}">{{ $value }}</option>
                                @endforeach
                            </select>
                            <span id="branch_id_handler"></span>
                            @if($errors->has('products_type'))
                                <span class="help-block">
                                    {{ $errors->first('products_type') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-4 form-group @if($errors->has('po_number')) has-error @endif">
                        {!! Form::label('po_number', 'Po Number*', ['class' => 'control-label']) !!}
                        {!! Form::number('po_number', old('po_number'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('po_number') }}
                                </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('retail_price')) has-error @endif">
                        {!! Form::label('serial_number', 'Serial No*', ['class' => 'control-label']) !!}
                        {!! Form::number('serial_number', old('serial_number'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('retail_price') }}
                                </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('warranty_expires')) has-error @endif">
                        {!! Form::label('warranty_expires', 'Warranty Expires*', ['class' => 'control-label']) !!}
                        {!! Form::text('warranty_expires', old('warranty_expires'), ['class' => 'form-control datepicker', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('warranty_expires') }}
                                </span>
                        @endif
                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-6 form-group @if($errors->has('remarks')) has-error @endif">
                        {!! Form::label('remarks', 'Remarks', ['class' => 'control-label']) !!}
                        {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 4, 'cols' => 10]) !!}

                        @if($errors->has('remarks'))
                            <span class="help-block">
                                    {{ $errors->first('remarks') }}
                                </span>
                        @endif
                    </div>
                    <div class="col-xs-6 form-group @if($errors->has('products_desc')) has-error @endif">
                        {!! Form::label('products_desc', 'Description', ['class' => 'control-label']) !!}
                        {!! Form::textarea('products_desc', old('products_desc'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 4, 'cols' => 10]) !!}

                        @if($errors->has('products_desc'))
                            <span class="help-block">
                                    {{ $errors->first('products_desc') }}
                                </span>
                        @endif
                    </div>

                </div>

                <div class="brands_row">
                    <div class="col-xs-6">
                        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
    {!! Form::close() !!}
    </div>
    {{----}}

    {{----}}
    </div>
@stop
@section('javascript')
    <script src="{{ url('public/js/admin/fixed_assets/fix_assets_update_modify.js') }}" type="text/javascript"></script>
@endsection

<style>
    .brands_row{
        float:left;
        width: 100%;
    }
</style>