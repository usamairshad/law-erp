@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assets</h3>
  
                <a href="{{ route('admin.fix_assets.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Asset</a>
        
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($FixedAssetsModel) > 0 ? 'datatable' : '' }}" id="product-table">
                <thead>
                <tr>
                <tr>
                    {{--<th width="8%">S: No</th>--}}
                    <th>Product Name</th>
                    <th>price</th>
                    <th>Vendor </th>
                    <th>Stock Quantity</th>
                    <th>Branch</th>
                    <th>Expiry Date</th>
                </tr>
                </thead>
                <tbody>
                @if (count($FixedAssetsModel) > 0)
                    @foreach ($FixedAssetsModel as $Unit)
                        <tr data-entry-id="{{ $Unit->id }}">
                            <td>{{ $Unit->product_name }}</td>
                            <td>{{ $Unit->price }}</td>
                             <td>{{ \App\Helpers\Helper::vendorIdToName($Unit->suplier_id) }}</td>
                            <td>{{ $Unit->closing_stock }}</td>
                            <td>{{ \App\Helpers\Helper::branchIdToName($Unit->branch) }}</td>
                            
                            <td>
                                {{ date('d-m-Y ', strtotime($Unit->warranty_expires))}}
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
           $('.datatable').DataTable()

            {{--$('#product-table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{!! route('admin.products.datatables') !!}',--}}
                {{--columns: [--}}
                    {{--{ data: 'id', name: 'id' },--}}
                    {{--{ data: 'products_name', name: 'products_name' },--}}
                    {{--{ data: 'suplier_id', name: 'suplier_id' },--}}
                    {{--{ data: 'cat_id', name: 'cat_id' },--}}
                    {{--{ data: 'landing_cost', name: 'landing_cost' },--}}
                    {{--{ data: 'branch_cost', name: 'branch_cost' },--}}
                    {{--{ data: 'dealer_cost', name: 'dealer_cost' },--}}
                    {{--{ data: 'retail_price', name: 'retail_price' },--}}
                    {{--{ data: 'products_type', name: 'products_type' },--}}
                    {{--{ data: 'action', name: 'action' , orderable: false, searchable: false},--}}

                {{--]--}}
            {{--});--}}
        });
    </script>
@endsection

@section('javascript')
  
@endsection

