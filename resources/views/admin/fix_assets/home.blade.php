@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('content')
    <h3 class="page-title">Brands</h3>
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('global.app_list')
        </div>
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ $Supplier ? 'datatable' : '' }} dt-select">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $Supplier->sup_name }}</td>
                    <td>{{ $Supplier->sup_address }}</td>
                    <td>{{ $Supplier->sup_contact }}</td>
                    <td>{{ $Supplier->sup_email }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
{{--@section('javascript')--}}
    {{--<script>--}}
        {{--window.route_mass_crud_entries_destroy = '{{ route('admin.suppliers.mass_destroy') }}';--}}
    {{--</script>--}}
{{--@endsection--}}
