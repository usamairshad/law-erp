@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create</h3>
            <a href="{{ route('admin.fix_assets.index') }}" style = "float:right;"class="btn btn-success pull-right">Back</a>
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.fix_assets.store'],'enctype' => 'multipart/form-data', 'id' => 'validation-form']) !!}
        <div class="panel panel-default">
            <div class="panel-body" style = "margin-top:40px;">
                <div class="row">
                    <div class=" form-group col-md-3 @if($errors->has('products_name')) has-error @endif">
                        {!! Form::label('products_name', 'Assets Name *', ['class' => 'control-label']) !!}
                        {!! Form::text('products_name', old('products_name'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
                        @if($errors->has('products_name'))
                            <span class="help-block">
							{{ $errors->first('products_name') }}
							</span>
                        @endif
                    </div>

                     <div class="form-group col-md-3 @if($errors->has('products_date')) has-error @endif">
                        {!! Form::label('products_date', 'Product Date*', ['class' => 'control-label']) !!}
                        <input type="date" name="products_date" class="form-control" required maxlength="10">
                    </div>

                    <div class="form-group col-md-3 @if($errors->has('staff_type')) has-error @endif">
                        {{ Form::label('Select Credit Account*') }}
                        

                          {{ Form::select('ledger_id', $Ledgers, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required')) }}
                          
                       
                    </div>
                    
                        <div class="form-group col-md-3 @if($errors->has('suplier_id')) has-error @endif">
                            {!! Form::label('suplier_id', 'Vendor*', ['class' => 'control-label']) !!}

                              {{ Form::select('suplier_id', $supplier, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required')) }}
                    </div>      
                </div>

                <div class="row">
                    <div class="col-md-3 form-group @if($errors->has('products_name')) has-error @endif">
                        {!! Form::label('item_is', 'Item is *', ['class' => 'control-label col-xs-12 row']) !!}
                        {!! Form::label('item_is', 'Use &emsp;  ', ['class' => '']) !!}
                        {!! Form::radio('item_is', '1', true, (old('item_is') ==  '1'), array('id'=>'item_is')) !!}
                        &emsp;
                        {!! Form::label('item_is', 'New &emsp; ', ['class' => '']) !!}
                        {!! Form::radio('item_is', '0', (old('item_is') ==  '0'), array('id'=>'item_is')) !!}
                    </div>



                    <div class="col-md-3 form-group @if($errors->has('branch')) has-error @endif">
                        <div class="@if($errors->has('branch')) has-error @endif">
                            {!! Form::label('branch', 'Branch*', ['class' => 'control-label']) !!}
                            <select id="branch" name="branch" class="form-control select2" data-placeholder="Select Branch" required>
                                <option value="">Select Branch Type</option>
                                @foreach($Branches as $key =>$value)
                                    <option value="{{$key}}">{{ $value }}</option>
                                @endforeach
                            </select>
                         
                        </div>
                    </div>

                    <div class="col-md-3 form-group @if($errors->has('price')) has-error @endif">
                        {!! Form::label('price', 'Price*', ['class' => 'control-label']) !!}
                        {!! Form::number('price', old('price'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('price') }}
                                </span>
                        @endif
                    </div>

                    <div class="col-md-3 form-group @if($errors->has('closing_stock')) has-error @endif">
                        {!! Form::label('closing_stock', 'Closing Stock*', ['class' => 'control-label']) !!}
                        {!! Form::number('closing_stock', old('closing_stock'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('closing_stock') }}
                                </span>
                        @endif
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4 form-group @if($errors->has('po_number')) has-error @endif">
                        {!! Form::label('po_number', 'Po Number*', ['class' => 'control-label']) !!}
                        {!! Form::number('po_number', old('po_number'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('po_number') }}
                                </span>
                        @endif
                    </div>
                    <div class="col-md-4 form-group @if($errors->has('serial_number')) has-error @endif">
                        {!! Form::label('serial_number', 'Serial No*', ['class' => 'control-label']) !!}
                        {!! Form::number('serial_number', old('serial_number'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('retail_price') }}
                                </span>
                        @endif
                    </div>
                    <div class="col-md-4 form-group @if($errors->has('warranty_expires')) has-error @endif">
                        {!! Form::label('warranty_expires', 'Warranty Expires*', ['class' => 'control-label']) !!}
                        {!! Form::text('warranty_expires', old('warranty_expires'), ['class' => 'form-control datepicker', 'placeholder' => '','required']) !!}
                        @if($errors->has('model'))
                            <span class="help-block">
                                    {{ $errors->first('warranty_expires') }}
                                </span>
                        @endif
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 form-group @if($errors->has('remarks')) has-error @endif">
                        {!! Form::label('remarks', 'Remarks', ['class' => 'control-label']) !!}
                        {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 4, 'cols' => 10]) !!}

                        @if($errors->has('remarks'))
                                <span class="help-block">
                                    {{ $errors->first('remarks') }}
                                </span>
                        @endif
                    </div>
                    <div class="col-md-6 form-group @if($errors->has('products_desc')) has-error @endif">
                        {!! Form::label('products_desc', 'Description', ['class' => 'control-label']) !!}
                        {!! Form::textarea('products_desc', old('products_desc'), ['class' => 'form-control', 'placeholder' => '', 'rows' => 4, 'cols' => 10]) !!}

                        @if($errors->has('products_desc'))
                            <span class="help-block">
                                    {{ $errors->first('products_desc') }}
                                </span>
                        @endif
                    </div>

                </div>

                <div class="brands_row">
                    <div class="col-xs-6">
                        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                    </div>
                </div>

             </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    {{----}}

    {{----}}
    </div>
@stop
@section('javascript')
    <script src="{{ url('public/js/admin/fixed_assets/fix_assets_create_modify.js') }}" type="text/javascript"></script>
@endsection

<style>
    .brands_row{
        float:left;
        width: 100%;
    }
</style>