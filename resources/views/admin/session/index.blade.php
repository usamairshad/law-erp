@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Session</h3>
                @if(Gate::check('erp_session_create'))
                <a href="{{ route('admin.session.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Session</a>
                @endif
        
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($session) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Start Year</th>
                    <th>End Year</th>
                    <th>Timestamp</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($session) > 0)
                    @foreach ($session as $Session)
                        <tr data-entry-id="{{ $Session->id }}">
                            
                            <td>{{ $Session->title }}</td>
                            <td>{{ $Session->start_year }}</td>
                            <td>{{ $Session->end_year}}</td>
                            <td>{{ $Session->created_at}}</td>

                            <td>
                                @if(Gate::check('erp_session_edit'))
                                <a href="{{ route('admin.session.edit',[$Session->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif
                                 @if(Gate::check('erp_session_destroy'))
                                {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.session.destroy', $Session->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                         
                        </tr>


                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>

@endsection