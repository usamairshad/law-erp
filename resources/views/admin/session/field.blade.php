
<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
    {!! Form::label('name', 'Title*', ['class' => 'control-label']) !!}
    <input type="text" name="title" placeholder="Title" class="form-control" required="required" >
</div>
<div class="form-group col-md-3 @if($errors->has('start_year')) has-error @endif" style = "float:left;">
    {!! Form::label('start_year', 'Start Year*', ['class' => 'control-label']) !!}
    <input type="date" name="start_year" min="1900" max="2099" class="form-control"/>
</div>
<div class="form-group  col-md-3 @if($errors->has('end_year')) has-error @endif" style = "float:left;">
    {!! Form::label('end_year', 'End Year*', ['class' => 'control-label']) !!}
    <input type="date" name="end_year" min="1900" max="2099" class="form-control" />
</div>
<div class="form-group  col-md-3 @if($errors->has('status')) has-error @endif" style = "float:left;">
    {!! Form::label('status', 'Status*', ['class' => 'control-label']) !!}
    {!! Form::select('status',[ ''=>'Select  Status','Active' => 'Active','InActive' => 'InActive','Pending' => 'Pending'],old('status') , ['class' => 'form-control', 'required']) !!}
    @if($errors->has('status'))
        <span class="help-block">
            {{ $errors->first('status') }}
        </span>
    @endif
</div>