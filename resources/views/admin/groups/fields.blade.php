<div class="col-xs-12 form-group">
        {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
        {!! Form::text('name', old('name'), ['class' => 'form-control','maxlength'=>200]) !!}
        @if($errors->has('name'))
                <span class="help-block">
                        {{ $errors->first('name') }}
                </span>
        @endif
</div>
{{--<div class="col-xs-6 form-group">--}}
        {{--{!! Form::label('code', 'Code', ['class' => 'control-label']) !!}--}}
        {{--{!! Form::text('code', old('code'), ['class' => 'form-control']) !!}--}}
        {{--@if($errors->has('code'))--}}
                {{--<span class="help-block">--}}
                        {{--{{ $errors->first('code') }}--}}
                {{--</span>--}}
        {{--@endif--}}
{{--</div>--}}
 <div class="col-xs-4 form-group">
        {!! Form::label('company_id', 'Company Name (Optional)', ['class' => 'control-label']) !!}
        {!! Form::select('company_id', $company->prepend('Select Company', ''),old('company_id'), ['class' => 'form-control']) !!}
        <span id="company_id_handler"></span>
        @if($errors->has('company_id'))
                <span class="help-block">
                        {{ $errors->first('company_id') }}
                </span>
        @endif
</div>
<div class="col-xs-4 form-group">
        {!! Form::label('city_id', 'City Name (Optional)', ['class' => 'control-label']) !!}
        {!! Form::select('city_id', $city->prepend('Select City', ''),old('city_id'), ['class' => 'form-control']) !!}
        <span id="city_id_handler"></span>
        @if($errors->has('city_id'))
                <span class="help-block">
                        {{ $errors->first('city_id') }}
                </span>
        @endif
</div>
<div class="col-xs-4 form-group">
        {!! Form::label('branch_id', 'Branch Name (Optional)', ['class' => 'control-label']) !!}
        {!! Form::select('branch_id', $branches->prepend('Select Branch', ''), old('branch_id'), ['class' => 'form-control']) !!}
        <span id="branch_id_handler"></span>
        @if($errors->has('branch_id'))
                <span class="help-block">
                        {{ $errors->first('branch_id') }}
                </span>
        @endif
</div>
<div class="col-xs-12 form-group">
        {!! Form::label('parent_id', 'Parent Group', ['class' => 'control-label']) !!}
        <select name="parent_id" id="parent_id" class="form-control select2" style="width: 100%;">
            <option value=""> Select a Parent Group </option>
                {!! $Groups !!}
        </select>
        <span id="parent_id_handler"></span>
        @if($errors->has('parent_id'))
                <span class="help-block">
                        {{ $errors->first('parent_id') }}
                </span>
        @endif
</div>