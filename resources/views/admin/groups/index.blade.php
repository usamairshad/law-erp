@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Groups</h3>
                                {{--@if(Gate::check('groups_create'))--}}
                <a style = "float:right; margin-bottom:20px;" href="{{ route('admin.groups.create') }}" class="btn btn-success pull-right">Add New Group</a>
            {{--@endif--}}
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="5%">ID</th>
                    {{--<th>Level</th>--}}
                    <th width="20%">Account Number</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($Groups) > 0)
                    @foreach ($Groups as $id => $Group)
                        <tr data-entry-id="{{ $id }}">
                            <td>{{ sprintf('%04d',$id) }}</td>
                            {{--<th>{{ $Group['level'] }}</th>--}}
                            <td>{{ $Group['number'] }}</td>
                            <td>{{ $Group['name'] }}</td>
                            <td>
                                @if (!in_array($id, Config::get('constants.accounts_main_heads')))
                                    {{--@if(Gate::check('groups_edit'))--}}
                                        <a href="{{ route('admin.groups.edit',[$id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                    {{--@endif--}}

                                    {{--@if(Gate::check('groups_destroy'))--}}
                                        {{-- {!! Form::open(array(
                                    'style' => 'display: inline-block;',
                                    'method' => 'DELETE',
                                    'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                    'route' => array('admin.groups.destroy', $id))) !!}
                                     {{ csrf_field() }}
                                        {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!} --}}
                                    {{--@endif--}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>


								</div>

                                </div>
                                </div>
                              
          
        </div>
        <!-- /.box-header -->
      
@stop

@section('javascript')
    {{--<script>--}}
        {{--$(document).ready(function(){--}}
            {{--$('.datatable').DataTable()--}}
        {{--});--}}
    {{--</script>--}}
@endsection