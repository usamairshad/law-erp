@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Create Local Purchase</h1>
    </section>
    {{ csrf_field() }}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Local Purchase</h3>
            {{--<a href="{{ route('admin.grimport') }}" class="btn btn-success pull-right">Back</a>--}}
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.localpurchase.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">

                    <div class="col-xs-3 form-group bnk @if($errors->has('order_no')) has-error @endif">
                        {!! Form::label('order_no', 'Order*', ['class' => 'control-label']) !!}
                        <select id="order_no" name="order_no" class="form-control select2 account" required>
                            <option value="">Select Order</option>
                            @if ($order->count())
                                @foreach($order as $invoice)
                                    {{--$lcs->pf_no.'-'.$lcs->lc_no--}}
                                    <option value="{{ $invoice->id }}">{{ $invoice->sale_order_no }}</option>
                                @endforeach
                            @endif
                        </select>
                        <span id="lc_no_handler"></span>
                        @if($errors->has('lc_no'))
                            <span class="help-block">
                                {{ $errors->first('lc_no') }}
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-md-3  @if($errors->has('due_date')) has-error @endif">
                        {!! Form::label('due_date', 'Due Date*', ['class' => 'control-label']) !!}
                        {!! Form::text('due_date',old('due_date'), ['class' => 'form-control datepicker']) !!}
                        @if($errors->has('due_date'))
                            <span class="help-block">
                        {{ $errors->first('due_date') }}
                    </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3  @if($errors->has('warehouse_id')) has-error @endif">
                        {!! Form::label('warehouse_id', 'Warehouse*', ['class' => 'control-label']) !!}
                        <select name="warehouse_id" id="" class="form-control">
                            <option value="">Select Warehouse</option>
                            @foreach ($warehouse as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('warehouse_id'))
                            <span class="help-block">
                        {{ $errors->first('warehouse_id') }}
                    </span>
                        @endif
                    </div>
                  
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div id="productmodel_col">

                    </div>
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-xs-12">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
        </div>
    {!! Form::close() !!}
        <!-- /.box-header -->

    </div>
@stop
   
@section('javascript')
    <!-- <script src="{{ url('js/admin/lcbanks/grimport_create_modify.js') }}" type="text/javascript"></script> -->
    <script>

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd' //format: 'DD-MM-YYYY H:m:s A',
        });
        $('.select2').select2();
       
        
        $(document).on('keyup','.test',function () {
            calculated_amount = 0;
            rel_qty = $(this).val();
            console.log('rel qty ', rel_qty);
            rowId = $(this).attr('data-row-id');
            
            total_qty = $('#total_qty_'+rowId).val();
            
            balanc_qty = total_qty - rel_qty;
            $('#bal_qty_'+rowId).val(balanc_qty);
            unit_price = $('#unit_price_'+rowId).val();
            remianing_balance = unit_price * balanc_qty;
            $('#bal_amount_'+rowId).val(remianing_balance);
            total_amount = $('#total_amount_'+rowId).val();
            total_paid = total_amount - remianing_balance;
            $('#total_paid_'+rowId).val(total_paid);

            if(parseInt(rel_qty) > parseInt(total_qty) ){
                console.log('val greater');
                alert('Release quantity should be less than total quantity');
                $('#bal_amount_'+rowId).val(' ');
                $('#bal_qty_'+rowId).val(' ');
                $('#total_paid_'+rowId).val(' ');
                rel_qty = $(this).val(' ');
                $('#bal_amount_total').val(' ');
                return false;
            }

            var value_sum=0;
            console.log('acc');
            $('.cal_total').each(function () {
               var curr =  $(this).val();
                if(curr){
                    value_sum = value_sum + parseInt (curr);
                $('#paid_amount_total').val(value_sum);

                }
            });
        })

    </script>

    <script>
        $(document).ready(function(){
            $("#order_no").on('change',function() {
                var order_id = $(this).val();
                if(order_id!=''){
                    var url = '{{ url('admin/localpurchase/productlist') }}';
                    url = url +'/'+ order_id;
                    $.ajax({
                        type:"get",
                        url : url,
                        success:function(data){
                            if(data!=''){
                                $('#productmodel_col').html(data);
                                    $(document).ready(function(){
                                    $('.select2').select2();
                                    });
                            }else{
                                alert("There is no product.");
                            }


                        }
                    });
                }

            });
            $( "#validation-form" ).validate({
                // define validation rules
                errorElement: 'span',
                errorClass: 'help-block',
                rules: {
                    warehouse: {
                        required: true,
                    },
                    lc_no: {
                        required: true,
                    },
                },
                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
            });
        });

        // $(document).ready(function(){
        //     $('#ledger_account').hide();
        //     $(".account").on('change',function() {
        //         var invoice_id = $(this).val();
                
        //         if(invoice_id!=''){
        //             var url = '{{ url('admin/lcbank/lctype') }}';
        //             url = url +'/'+ invoice_id;
        //             $.ajax({
        //                 type:"get",
        //                 url : url,
        //                 success:function(data){
        //                     if(data!=''){
        //                         //product_select= data;
        //                         console.log(data);
        //                         if(data == 1){
        //                             $('#ledger_account').show();
        //                         }else{
        //                             $('#ledger_account').hide();
        //                         }
        //                     }else{
        //                         alert("There is no product.");
        //                     }


        //                 }
        //             });
        //         }

        //     });
           
        // });
    </script>
@endsection