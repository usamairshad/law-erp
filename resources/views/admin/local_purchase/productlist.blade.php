
<?php
$a=0;
?>

<table class="table table-condensed" id="entry_items">
    <thead>
    <tr>
        <th>Product Name</th>
        <th>Total Qty</th>
        <th>Rel. Qty</th>
        <th>Bal. Qty</th>
        <th>Unit Price</th>
        <th>Total Amount</th>
        <th>Paid Amount</th>
        <th>Balance Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($orderDetail as $result)
    @php
        
        $a++;
    @endphp
    <tr>
        <td>
            {!! Form::text('product_detail[]', $result->productsModel->products_short_name .'-'. $result->productsModel->products_name,['id' => 'product_detail_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
            {!! Form::hidden('product_detail_id[]', $result->productsModel->id,['id' => 'product_detail_id_'.$a, 'class' => 'form-control',]) !!}
            {!! Form::hidden('row_id[]', $result->id,['id' => 'row_id_'.$a, 'class' => 'form-control',]) !!}
           
        </td>
        <td>
            {!! Form::text('total_qty[]',$result->remaining_qty,  ['id' => 'total_qty_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
        </td>
        <td>
           {!! Form::number('rel_qty[]', old('rel_qty'), ['id' => 'rel_qty_'.$a, 'class' => 'form-control test','data-row-id'=>"$a"]) !!}

        </td>
        <td>
           {!! Form::text('bal_qty[]', old('balance_qty'), ['id' => 'bal_qty_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}

        </td>
        <td>
            {!! Form::text('unit_price[]', $result->unit_price, ['id' => 'unit_price_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
        </td>

        <td>
            {!! Form::text('total_amount[]',$result->amount, ['id' => 'total_amount_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
        </td>
        <td>
            {!! Form::text('total_paid[]', 0, ['id' => 'total_paid_'.$a, 'class' => 'form-control cal_sum cal_total', 'readonly' => 'true']) !!}
        </td>
        <td>
            {!! Form::text('bal_amount[]', old('bal_amount'), ['id' => 'bal_amount_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
        </td>
    </tr>
        @endforeach
        <tr>
            <th colspan="6" class="text-right">TOTAL PAID AMOUNT</th>
            <th colspan="1">
                {!! Form::text('paid_amount_total', old('paid_amount_total'), ['id' => 'paid_amount_total', 'class' => 'form-control ', 'readonly' => 'true','required'=>'required']) !!}
            </th>         
        </tr>
    </tbody>
</table>

@section('javascript')
<script>
$('.select2').select2();
</script>
@endsection