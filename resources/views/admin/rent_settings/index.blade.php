@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Rent Tax Slabs</h3>
            
                <a href="{{ route('rental-tax-settings-create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Rent Tax</a>
            
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($TaxSettings) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Tax Class</th>
                    <th>Fix Amount</th>
                    <th>Tax percentage</th>
                    <th>Start Range</th>
                    <th>End Range</th>

                </tr>
                </thead>

                <tbody>

                @if (count($TaxSettings) > 0)
                    @foreach ($TaxSettings as $settings)
                        <tr data-entry-id="{{ $settings->id }}">
                            <td>{{ $settings->tax_class}}</td>
                            <td>{{ $settings->fix_amount }}</td>
                            <td>{{ $settings->tax_percent }}</td>
                            <td>{{ $settings->start_range }}</td>
                            <td>{{ $settings->end_range }}</td>
                        </tr>
                    @endforeach

                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection