@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">User Update</h3>

    </div>
    <div class="box box-primary">
        <!-- /.box-header -->
        <!-- form start -->
                <form id="add_new" method="post" action="{{route('create-user.update',$user->id)}}">
                    {!! Form::open(['method' => 'PUT', 'enctype' => 'multipart/form-data', 'route' => ['create-user.update',$user->id], 'id' => 'validation-form']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <!-- <input type="hidden" name="id" id="hidden_id" value=""> -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Name:</label>
                                    <input type="text" class="form-control" placeholder="Enter Name" name="name" value="{{$user->name}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Role Type:</label>
                                    <select class="form-control" name="role_type" id="role_type">
                                        <option value="erp" selected>{{$user->user_type}}</option>
                                  
                                        
                                    </select>
                                    {{--<select name="section" class="form-control" required="required">
                                        @foreach($sections as $section)
                                            @if($section == $data->section)
                                                <option value="{{$section}}" selected>{{$section}}</option>
                                            @else
                                                <option value="{{$section}}">{{$section}}</option>
                                            @endif
                                        @endforeach
                                    </select>--}}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Role:</label>
                                    <select class="form-control" name="role_id" id="roles">
                                        
                                        @foreach($roles as $key => $role)
                                            @if($user->role_id == $key)
                                                <option value="{{$key}}" selected>{{$role}}</option>
                                            @else
                                            <option value="{{$key}}">{{$role}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Branch:</label>
                                    <select class="form-control" name="branch_id" id="branch_id">
                                        <option disabled selected>{{App\Helpers\Helper::branchIdToName($user->branch_id)}}</option>
                                        {!! App\Models\Admin\Branches::branchList() !!}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Email:</label>
                                    <input type="text" placeholder="Enter Email" class="form-control" value="{{$user->email}}" name="email" id="email">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Password:</label>
                                    <input type="password" placeholder="Enter Password" class="form-control" value="" name="password" id="password" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="resetform()" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
               {!! Form::close() !!}
    </div>

@stop

@section('javascript')
    <script>

        $(function(){
            $('#role_type').change(function(){
               $("#roles option").remove();;
               var id = $(this).val();
               console.log(id)
               $.ajax({
                  url : '{{ route( 'admin.load-roles' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'get',
                  dataType: 'json',
                  success: function( result )
                  {     
                        console.log(result)
                        $('#roles').append($('<option disabled selected>--Select--</option>'));
                        $.each( result, function(k, v) {
                            $('#roles').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
@endsection