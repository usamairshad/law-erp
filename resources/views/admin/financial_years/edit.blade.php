@extends('layouts.app')

@section('stylesheet')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>FinancialYears</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Financial Year</h3>
            <a href="{{ route('admin.financial_years.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($FinancialYear, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.financial_years.update', $FinancialYear->id]]) !!}
        <div class="box-body">
            @include('admin.financial_years.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <!-- bootstrap datepicker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ url('public/js/admin/financial_years/create_modify.js') }}" type="text/javascript"></script>
@endsection