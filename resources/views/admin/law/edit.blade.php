@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Tax Laws</h3>
            

    <div class="box box-primary">
         <a  href="{{route('tax-law')}}" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['tax-law-update'], 'id' => 'validation-form']) !!}
        <div id="show" style="padding:5%;">
            <div id="box" class="box-body">
            <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left; display:none">
                    {!! Form::label('name', 'Law Section*', ['class' => 'control-label']) !!}
                   <input type="text" name="id"  required="required" value="{{$Branche->law_id}}" placeholder="Enter Course Name" class="form-control" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Law Section*', ['class' => 'control-label']) !!}
                   <input type="text" name="name"  required="required" value="{{$Branche->law_section}}" placeholder="Enter Course Name" class="form-control" />
                </div>
                <div class="form-group col-md-6 @if($errors->has('description')) has-error @endif" style = "float:left;">
                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                     <input type="text" name="description" required="required" value="{{$Branche->description}}" placeholder="Enter Description" class="form-control" />
                </div>
                <div class=" col-md-6 form-group" style = "float:left;">
        {!! Form::label('parent_id', 'Parent Group', ['class' => 'control-label']) !!}
        <select name="parent_id[]" value="{{$Branche->group_id}}" id="parent_id" class="form-control select2" style="width: 100%;">
            <option value="{{$Branche->description}}"> Select a Parent Group </option>
                {!! $Groups !!}
        </select>
        <span id="parent_id_handler"></span>
        @if($errors->has('parent_id'))
                <span class="help-block">
                        {{ $errors->first('parent_id') }}
                </span>
        @endif
</div>
              
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script type="text/javascript">
   
    var i = 0;
    
    $("#add").click(function(){
  
        ++i;
        $("#secondbox").append('<div id="remove" ><div class="form-group col-md-3" style = "float:left;"><input required="required" type="text" name="name[]" placeholder="Enter Course Name" class="form-control" /></div><div style = "float:left;" class="form-group col-md-6"><input type="text" required="required" name="description[]" placeholder="Enter Description" class="form-control" /></div><br> <div class="form-group col-md-2" style = "float:left;"><button " style = "float:left;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

