@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<?php $per = PermissionHelper::getUserPermissions();?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Tax Laws</h3>
               @can('erp_laws_create',$per)
                <a href="{{ route( 'tax-law-add' ) }}" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Define New Tax Law</a>
                @endcan
            
        </div>
        <!-- /.box-header -->
      
									<table style = " width:100%" id="example" class="table table-bordered table-striped {{ count($Law) > 0 ? 'datatable' : '' }}">
										<thead>
											<tr>
                                            <th>S.No</th>
                                            <th>Law Section</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                
											</tr>
										</thead>
										<tbody>
											
										
									
                                            @if (count($Law) > 0)
                    @foreach ($Law as $settings)
                        <tr data-entry-id="{{ $settings->id }}">
                            <td>{{ $settings->law_id}}</td>
                            <td>{{ $settings->law_section }}</td>
                            <td>{{ $settings->description }}</td>
                            <td style = "float:left;">
                                      

                                        <a href="{{ route('tax-law-edit', [$settings->law_id]) }}"
                                            class="btn btn-xs btn-info">Edit</a>
                                   

                                        {!! Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['destroy-law', $settings->law_id],
                                        ]) !!}
                                        {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger'])
                                        !!}
                                        {!! Form::close() !!}  


                                </td> 
                       
                        </tr>
                    @endforeach

                @endif
										</tbody>
									</table>
								
	
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection