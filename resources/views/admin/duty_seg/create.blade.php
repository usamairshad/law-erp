@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty Segregation</h1>
    </section>
    {{ csrf_field() }}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Duty Segregation</h3>
            <a href="{{ route('admin.duty_seg.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        {!! Form::open(['method' => 'POST', 'route' => ['admin.duty_seg.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">

                    <div class="col-xs-3 form-group bnk @if($errors->has('lc_no')) has-error @endif">
                        {!! Form::label('lc_no', 'Shipment*', ['class' => 'control-label']) !!}
                        <select id="lc_no" name="lc_no" class="form-control select2 account" required>
                            <option value="">Select Lc</option>
                            @if ($LcBank->count())
                                @foreach($LcBank as $lc)
                                    {{--$lcs->pf_no.'-'.$lcs->lc_no--}}
                                    <option value="{{ $lc->id }}">{{ $lc->lc_no }}</option>
                                @endforeach
                            @endif
                        </select>
                        <span id="lc_no_handler"></span>
                        @if($errors->has('lc_no'))
                            <span class="help-block">
                                {{ $errors->first('lc_no') }}
                            </span>
                        @endif
                    </div>


                    {{-- <div class="col-xs-3 form-group bnk @if($errors->has('warehouse')) has-error @endif">
                        {!! Form::label('warehouse', 'Invoice*', ['class' => 'control-label']) !!}
                        <select id="cinvoice" name="cinvoice" class="form-control select2" required>
                           
                        </select>
                        <span id="warehouse"></span>
                        @if($errors->has('warehouse'))
                            <span class="help-block">
                                {{ $errors->first('warehouse') }}
                            </span>
                        @endif
                    </div> --}}
                  
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div id="productmodel_col">

                    </div>
                </div>
            </div>

            <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="about_featured">
                            <div class="panel-group" id="">
    
                                <div class="panel panel-default wow fadInLeft">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#11">
                                                <span class="fa fa-check-square-o"></span><b>Product List</b>
                                            </a>
                                        </h4>
                                    </div>
    
                                    <div id="11" class="panel-collapse collapse in">
    
    
                                        <div class="panel-body pad table-responsive">
                                            <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>R</u>ow</button>
                                            <table class="table table-bordered table-striped" id="users-table">
                                                <thead>
                                                <tr>
                                                    <th>Duty Name</th>
                                                    <th>Amount</th>
                                                    <th>Action</th>
    
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php  $counter=0 ?>
                                                <tr id="line_item-1">
                                                    <td style="width: 50%;"><div class="form-group  @if($errors->has('duty_id')) has-error @endif">
    
                                                            {!! Form::select('InventoryItems[duty_id][1]',array(), old('duty_id'), ['id' => 'InventoryItems-duty_id-1','class' => 'form-control description-data-ajax select2 dublicate_product', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                                                            @if($errors->has('duty_id'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('duty_id') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td> <div class="form-group  @if($errors->has('amount')) has-error @endif">
    
                                                            {!! Form::number('InventoryItems[amount][1]', old('amount'), ['id' => 'InventoryItems-amount-1','class' => 'form-control maxQty' ,'data-row-id'=>'1', 'required' => 'true','min'=>'1']) !!}
                                                            @if($errors->has('line_qunity'))
                                                                <span class="help-block">
                                                                    {{ $errors->first('line_qunity') }}
                                                                    </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td style="width: 5%;"><button id="InventoryItems-del_btn-1" onclick="FormControls.destroyLineItem('0');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    
                                                </tr>
                                                <input type="hidden" id="InventoryItems-global_counter" value="<?php  echo ++$counter ?>"   />
    
                                                </tbody>
    
                                            </table>
                                        </div>
    
    
                                    </div>
                                </div>
                            </div>
    
                        </div>
                    </div>
    
                    @include('admin.duty_seg.items_template')
                </div>
        </div>

       
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-xs-12">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
        </div>
    {!! Form::close() !!}
        <!-- /.box-header -->

    </div>
@stop
   
@section('javascript')
    <script src="{{ url('public/js/admin/dutysegregation/duty_seg.js') }}" type="text/javascript"></script> 
    <script>

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd' //format: 'DD-MM-YYYY H:m:s A',
        });
        $('.select2').select2();
       
        
        $(document).on('keyup','.test',function () {
            calculated_amount = 0;
            rel_qty = $(this).val();
            console.log('rel qty ', rel_qty);
            rowId = $(this).attr('data-row-id');
            
            total_qty = $('#total_qty_'+rowId).val();
            
            balanc_qty = total_qty - rel_qty;
            $('#bal_qty_'+rowId).val(balanc_qty);
            unit_price = $('#unit_price_'+rowId).val();
            remianing_balance = unit_price * balanc_qty;
            $('#bal_amount_'+rowId).val(remianing_balance);
            total_amount = $('#total_amount_'+rowId).val();
            total_paid = total_amount - remianing_balance;
            $('#total_paid_'+rowId).val(total_paid);

            var value_sum=0;
            console.log('acc');
            $('.cal_total').each(function () {
               var curr =  $(this).val();
                if(curr){
                    value_sum = value_sum + parseInt (curr);
                $('#paid_amount_total').val(value_sum);

                }
            });
        })

    </script>

    <script>




        $(document).ready(function(){

            $(document).on('change', '#lc_no', function () {
                // console.log("hmm its change");

                var lc_id = $(this).val();
                console.log(lc_id);
                var div = $(this).parent();

                var op = " ";

                $.ajax({
                    type: 'get',
                    url: '{!!URL::to('admin/duty/segregate/invoices')!!}',
                    data: {'id': lc_id},
                    success: function (data) {
                        //console.log('success');

                        if(data!=''){
                            $('#productmodel_col').html(data);
                                
                        }else{
                            alert("There is no Entry.");
                        }

                        //console.log(data.length);
                        op+='<option value="0" selected disabled>Select Invoice</option>';
                        for (var i = 0; i < data.length; i++) {
                            op += '<option value="' + data[i].id + '">' + data[i].ci_no + ' ' + '</option>';
                            //alert(data[i].fname);
                        }

                        $('#cinvoice').html(" ");
                        $('#cinvoice').append(op);
                    },
                    error: function () {

                    }
                });
            });
         
            $( "#validation-form" ).validate({
                // define validation rules
                errorElement: 'span',
                errorClass: 'help-block',
                rules: {
                    warehouse: {
                        required: true,
                    },
                    lc_no: {
                        required: true,
                    },
                },
                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
            });
        });

    </script>
@endsection