@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Duty Segregation List</h1>
    </section>
    {{--@if(Gate::check('orders_create'))--}}
        {{--<a href="{{ route('admin.orders.index') }}" class="btn btn-primary pull-lef" style="margin-left: 25px; margin-bottom: 5px;">Local Order</a>--}}
        {{--<a href="{{ route('admin.orders.importindex') }}" class="btn btn-primary pull-lef" style="margin-left: 5px; margin-bottom: 5px;">Import Order</a>--}}

    {{--@endif--}}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Duty Segregation</h3>
            @if(Gate::check('orders_create'))
                <a href="{{ route('admin.duty_seg.index') }}" class="btn btn-success pull-right">Duty Segregation</a>
            @endif
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($duty) > 0 ? 'datatable' : '' }}" id="order-table">
                <thead>
                <tr>
                    <th width="8%">S: No</th>
                    <th>Duty Name</th>
                    <th>Amount</th>
                </tr>
                </thead>
                @php
                    $i = 1;
                @endphp
                <tbody>
                    @foreach ($duty as $item)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$item->dutyModel->duty}}</td>
                            <td>{{$item->amount}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop



