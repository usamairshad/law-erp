{{-- <style type="text/css">
    table {
 width: 100%;
}

thead, tbody, tr, td, th { display: block; }

tr:after {
 content: ' ';
 display: block;
 visibility: hidden;
 clear: both;
}

thead th {
 height: 60px;

 /*text-align: left;*/
}

tbody {
 height: 500px;
 overflow-y: auto;
}

thead {
 /* fallback */
}


tbody td, thead th {
 width: 50%;
 float: left;
}
     
 </style> --}}
 <div class="form-group col-md-4 @if($errors->has('cgroup_id')) has-error @endif" id="ledger_account">
        {!! Form::label('cgroup_id', 'Select Invoice*', ['class' => 'control-label']) !!}
        <span id="cgroup_id_content">
            <select name="invoice_id" id="invoice_id" class="form-control select2" style="width: 100%;" required >
                <option value="" selected> Select Invoice </option>
                @if (count($data) > 0)
                    @foreach ($data as $invoice)
                        <option value="{{ $invoice->id }}" >{{ $invoice->ci_no }}</option>
                        @endforeach
                @endif
            </select>
        </span>
        <span id="cgroup_id_handler"></span>
     </div>
<?php
$a=0;
?>
    <table class="table" id="entry_items">
        <thead>
        <tr>
            <th>Narration</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
            @php
                $total= 0;
            @endphp
        @foreach ($entry as $result)
        @php
            $total = $total + $result->amount;
            $a++;
        @endphp
        <tr>
            <td>
                {{-- {!! Form::text('product_detail[]', $result->productsModel->products_short_name .'-'. $result->productsModel->products_name,['id' => 'product_detail_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!} --}}
                {!! Form::text('product_detail_id[]', $result->narration,['id' => 'product_detail_id_'.$a, 'class' => 'form-control', 'readonly' => 'true']) !!}
                {!! Form::hidden('row_id[]', $result->id,['id' => 'row_id_'.$a, 'class' => 'form-control',]) !!}
            
            </td>
            <td>
                {!! Form::text('total_qty[]',$result->amount,  ['id' => 'total_qty_'.$a, 'class' => 'form-control', 'readonly' => 'true', 'style' => 'width:200px;']) !!}
            </td>
            
        </tr>
            @endforeach
            <tr>
                <th colspan="" class="text-right">TOTAL PAID AMOUNT</th>
                <th >
                    {!! Form::text('paid_amount_total', $total, ['id' => 'paid_amount_total', 'class' => 'form-control ', 'readonly' => 'true', 'style' => 'width:200px;']) !!}
                </th>         
            </tr>
        </tbody>
    </table>


