@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Settings</h1>
    </section>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a href="{{ route('admin.settings.create') }}" class="btn btn-success pull-right">Add New Setting</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body pad table-responsive">
            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Short Code</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    @if (count($Settings) > 0)
                        @foreach ($Settings as $Setting)
                            <tr data-entry-id="{{ $Setting->id }}">
                                <td>{{ $Setting->name }}</td>
                                <td>
                                    @if($Setting->id == Config::get('constants.currency_symbols_setting_id'))
                                        {{ Config::get('constants.currency_symbols')[$Setting->description] }}
                                    @elseif($Setting->id == Config::get('constants.date_format_setting_id'))
                                        {{ Config::get('constants.date_format')[$Setting->description] }}
                                    @elseif(
                                        $Setting->id == Config::get('constants.accounts_cash_banks_head_setting_id') ||
                                        $Setting->id == Config::get('constants.accounts_cash_head_setting_id') ||
                                        $Setting->id == Config::get('constants.accounts_banks_head_setting_id') ||
                                        $Setting->id == Config::get('constants.accounts_lc_banks_head_setting_id') ||
                                        $Setting->id == Config::get('constants.accounts_stock_items_head_setting_id') ||
                                        $Setting->id == Config::get('constants.accounts_lc_tt_local_head_setting_id') ||
                                        $Setting->id == Config::get('constants.accounts_lc_head_setting_id') ||
                                        $Setting->id == Config::get('constants.accounts_tt_head_setting_id') ||
                                        $Setting->id == Config::get('constants.accounts_local_head_setting_id')
                                    )
                                        @if(isset($Groups[$Setting->description]))
                                            {{ $Groups[$Setting->description]->number . ' - ' . $Groups[$Setting->description]->name }}
                                        @else
                                            N/A
                                        @endif
                                    @else
                                        {{ $Setting->description }}
                                    @endif
                                </td>
                                <td>
                                    @if(Gate::check('settings_edit'))
                                        <a href="{{ route('admin.settings.edit',[$Setting->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i> @lang('global.app_edit')</a>
                                    @endif

                                    @if(Gate::check('settings_destroy'))
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'DELETE',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['admin.settings.destroy', $Setting->id])) !!}
                                        {!! Form::button('<i class="fa fa-trash"></i>&nbsp;'.trans('global.app_delete'), array('type' => 'submit', 'class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/admin/settings/index.js') }}" type="text/javascript"></script>
@endsection