@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Settings</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Setting</h3>
            <a href="{{ route('admin.settings.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Setting, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.settings.update', $Setting->id]]) !!}
        <div class="box-body">
            <div class="form-group @if($errors->has('description')) has-error @endif">
                @if($Setting->id == Config::get('constants.currency_symbols_setting_id'))
                    {!! Form::hidden('name', $Setting->name) !!}
                    {!! Form::label('description', $Setting->name, ['class' => 'control-label']) !!}
                    {!! Form::select('description', Config::get('constants.currency_symbols'), old('description'), ['class' => 'form-control']) !!}
                @elseif($Setting->id == Config::get('constants.date_format_setting_id'))
                    {!! Form::hidden('name', $Setting->name) !!}
                    {!! Form::label('description', $Setting->name, ['class' => 'control-label']) !!}
                    {!! Form::select('description', Config::get('constants.date_format'), old('description'), ['class' => 'form-control']) !!}
                @elseif(
                    $Setting->id == Config::get('constants.accounts_cash_banks_head_setting_id') ||
                    $Setting->id == Config::get('constants.accounts_cash_head_setting_id') ||
                    $Setting->id == Config::get('constants.accounts_banks_head_setting_id') ||
                    $Setting->id == Config::get('constants.accounts_lc_banks_head_setting_id') ||
                    $Setting->id == Config::get('constants.accounts_stock_items_head_setting_id') ||
                    $Setting->id == Config::get('constants.accounts_lc_tt_local_head_setting_id') ||
                    $Setting->id == Config::get('constants.accounts_lc_head_setting_id') ||
                    $Setting->id == Config::get('constants.accounts_tt_head_setting_id') ||
                    $Setting->id == Config::get('constants.accounts_local_head_setting_id')
                )
                    <!-- Banks & Cash Head -->
                    {!! Form::hidden('name', $Setting->name) !!}
                        <div class="col-xs-12 form-group">
                            {!! Form::label('description', $Setting->name, ['class' => 'control-label']) !!}
                            <select name="description" id="description" class="form-control select2" style="width: 100%;">
                                <option value=""> Select a Parent Group </option>
                                {!! $Groups !!}
                            </select>
                        </div>
                @else
                    {!! Form::hidden('name', $Setting->name) !!}
                    {!! Form::label('description', $Setting->name, ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '']) !!}
                @endif
                @if($errors->has('description'))
                    <p class="help-block">
                        {{ $errors->first('description') }}
                    </p>
                @endif
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/admin/settings/create_modify.js') }}" type="text/javascript"></script>
@endsection