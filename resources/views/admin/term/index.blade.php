@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Terms</h3>
                {{-- @if(Gate::check('erp_term_create')) --}} 
                    <a href="{{ route('admin.term.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Term</a>
                {{-- @endif --}}
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($terms) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>

                <tbody>
                @if (count($terms) > 0)
                    @foreach ($terms as $term)
                        <tr data-entry-id="{{ $term->id }}">
                            <td>{{ $term->id }}</td>
                            <td>{{ $term->name }}</td>
                            <td>{{ $term->description }}</td>
                            
                            
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection