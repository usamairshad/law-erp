@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Academic Year</h3>
                @if(Gate::check('erp_academic_years_create'))
                    <a href="{{ route('admin.academic_year.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Academic Year</a>
                @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($AcademicYear) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Start Month</th>
                    <th>End Month</th>
                    <th>Start_Year</th>
                    <th>End Year</th>
                     <th>Status</th>
                    <th>TimeStamp</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($AcademicYear) > 0)
                    @foreach ($AcademicYear as $AcademicYears)
                        <tr data-entry-id="{{ $AcademicYears->id }}">
                            <td>{{ $AcademicYears->id }}</td>
                            <td>{{ $AcademicYears->name }}</td>
                            <td>{{ $AcademicYears->start_month }}</td>
                            <td>{{ $AcademicYears->end_month }}</td>
                            <td>{{ $AcademicYears->start_year }}</td>
                            <td>{{ $AcademicYears->end_year }}</td>
                            <td>{{ $AcademicYears->status }}</td>
                            <td>{{ $AcademicYears->created_at }}</td>
                            <td>
                                @if($AcademicYears->status == 1)
                                 {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['admin.academic_year.inactive', $AcademicYears->id])) !!}
                                    {!! Form::submit('InActivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}

                                    @elseif($AcademicYears->status == 0)
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['admin.academic_year.active', $AcademicYears->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                                    @endif
                            </td>
                            
                            
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection