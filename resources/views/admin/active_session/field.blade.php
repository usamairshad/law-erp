
<div class="form-group col-md-3 @if($errors->has('session_id')) has-error @endif" style = "float:left;">
    {{ Form::label('Session Name*') }}
    {{ Form::select('session_id', $sessionList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required')) }}
</div>
<div class="form-group col-md-3 @if($errors->has('board_id')) has-error @endif" style = "float:left;">
    {{ Form::label('Board Name*') }}
    {{ Form::select('board_id', $boardList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required')) }}
</div>

<div class="form-group col-md-3 @if($errors->has('program_id')) has-error @endif" style = "float:left;">
    {{ Form::label('Program Name*') }}
    {{ Form::select('program_id', $programList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required')) }}
</div>
<div class="form-group col-md-3 @if($errors->has('class_id')) has-error @endif" style = "float:left;">
    {{ Form::label('Class Name*') }}
    {{ Form::select('class_id', $classList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required')) }}
</div>
<div class="form-group col-md-3 @if($errors->has('branches')) has-error @endif" style = "float:left;">
    {!! Form::label('branches', 'Branches*', ['class' => 'control-label']) !!}
        <select class="form-control select2 branches" name="branches[]" required="required" multiple="multiple">
            @foreach($branches as $key => $branch)
            <option value="{{$branches[$key]['id']}}">{{$branches[$key]['name']}}</option>
            @endforeach
        </select>
</div>
<div class="form-group  col-md-3 @if($errors->has('status')) has-error @endif" style = "float:left;">
    {!! Form::label('status', 'Status*', ['class' => 'control-label']) !!}
    {!! Form::select('status',[ ''=>'Select  Status','Active' => 'Active','InActive' => 'InActive','Pending' => 'Pending'],old('status') , ['class' => 'form-control', 'required']) !!}
    @if($errors->has('status'))
        <span class="help-block">
            {{ $errors->first('status') }}
        </span>
    @endif
</div>

