@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Active Session</h3>
                <a href="{{ route('admin.active-session.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Active Session</a>
            
        
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($active_sessions) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Session Name</th>
                    <th>Board Name</th>
                    <th>Branch Name</th>
                    <th>Program Name</th>
                    <th>Class Name</th>
                </tr>
                </thead>

                <tbody>
                @if (count($active_sessions) > 0)
                    @foreach ($active_sessions as $active_session)
                        <tr data-entry-id="{{ $active_session->id }}">
                            <td>{{ $active_session->title }}</td>
                            <td>{{\App\Helpers\Helper::sessionIdToName($active_session->session_id)}}</td>
                            <td>{{\App\Helpers\Helper::boardIdToName($active_session->board_id)}}</td>
                            <td>{{\App\Helpers\Helper::branchIdToName($active_session->branch_id)}}</td>
                            <td>{{\App\Helpers\Helper::programIdToName($active_session->program_id)}}</td>
                            <td>{{\App\Helpers\Helper::classIdToName($active_session->class_id)}}</td>                    
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection