@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Session</h3>
            @if(Gate::check('erp_session_create'))
            <a href="{{ route('admin.active-session.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
            @endif

        </div>

       {!! Form::open(['method' => 'POST', 'route' => ['admin.active-session.store'], 'id' => 'validation-form']) !!}
       {!! Form::token(); !!}
        <div class="box-body" style = "margin-top:40px;">
            @include('admin.active_session.field')
        </div>

        <div class="box-footer">
            <button class="btn btn-primary" style = "margin-top:27px;" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
        })
    </script>

    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById('btn').setAttribute("disabled",null);
            $( document ).change(function(){
                var e = document.getElementById("classes");
                var strUser = e.value;
                if (strUser != "") {
                document.getElementById('btn').removeAttribute("disabled");
                } else {
                    document.getElementById('btn').setAttribute("disabled", null);
                }
            })
            
        });
         $(".branches").select2({

            });
    </script>
    <script type="text/javascript">
        function selectRefresh() {
            $(".branches").select2({

            });
        }
        function selectRefresh() {
            $(".classes").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
    <script type="text/javascript">


    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" style="height:120px"><hr style="border-top:1px solid #222d32"><div class="form-group col-md-3"><select id="board" class="form-control select2 branches" name="board_id[]">@foreach($branches as $key => $branch)<option value="{{$branch['id']}}">{{$branch['name']}}</option>@endforeach</select></div><div class="form-group col-md-3"><select class="form-control select2 branches" name="branches" id="branches"></select></div><br> <div class="form-group col-md-2"><button style="margin-top: -27px;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');
            $('.select2').select2(); 

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    // function showBranch() {
    //     var data= $('input[name="branch_name"]').val();

    //     if(data)
    //     {
    //          $('#show').show();
    //          $('#btn').hide(); 
    //     }
    // }
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
     <script>
        $(function(){
            $('#board').change(function(){
               $("#programs option").remove();;
               var id = $(this).val();
               $.ajax({
                  url : '{{ route( 'admin.board-loadPrograms' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {
                        $.each( result, function(k, v) {
                            $('#branches').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
@endsection