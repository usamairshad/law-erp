@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Fee Section</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Fee Section</h3>
            <a href="{{ route('admin.fee_section.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.fee_section.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="form-group col-md-4 @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Fee Section*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>40,'required','minlength'=>2]) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-4 @if($errors->has('description')) has-error @endif">
                {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
                {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>40,'required','minlength'=>2]) !!}
                @if($errors->has('description'))
                    <span class="help-block">
                        {{ $errors->first('description') }}
                    </span>
                @endif
            </div>

        </div>

        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/fee_section/fee_section_create_modify.js') }}" type="text/javascript"></script>
@endsection

