@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Academic Groups</h3>
    <div class="box box-primary">
        <a  href="{{route('admin.academics-groups.index')}}" style = "float:right" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.academics-groups.store'], 'id' => 'validation-form']) !!}
        <div id="show">
            <div id="box" class="box-body" style="padding-top:40px;">
                <div class="form-group col-md-5 @if($errors->has('program_name')) has-error @endif" style = "float:left;">
                    {!! Form::label('program_id', 'Select Program*', ['class' => 'control-label']) !!}
                   <select class="form-control" name="program_id[0]" required="required">
                       <option disabled="disabled" selected="selected">---Select----</option>
                       @foreach($programs as $program)
                            <option value="{{$program->id}}">{{$program->name}}</option>
                       @endforeach
                   </select>
                </div>
                <div class="form-group col-md-6 @if($errors->has('description')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Academic Group Name', ['class' => 'control-label']) !!}
                     <input type="text" name="name[0]"  placeholder="Enter Academic Group Name" class="form-control" />
                </div>
                <div class="form-group col-md-1" style = "float:left;">
                    <button style="margin-top: 22px" type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
        </div>

        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop
@section('css')
<style type="text/css">

</style>
@endsection
@section('javascript')
    <script type="text/javascript">
    
    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" ><div class="form-group col-md-5" style = "float:left;"><select class="form-control select2" name="program_id[]"><option selected="selected" disabled="disabled">---Select---</option>@foreach($programs as $key => $program)<option value="{{$program['id']}}">{{$program['name']}}</option>@endforeach</select></div><div class="form-group col-md-6" style = "float:left;"><input type="text" required="required" name="name[]" placeholder="Enter Academics Group Name" class="form-control" /></div><br> <div class="form-group col-md-1" style = "float:left;"><button type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

