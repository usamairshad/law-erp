@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Academic Groups</h3>
            <a  href="{{route('admin.academics-groups.create')}}" style = "float:right;" class="btn btn-success pull-right">Create</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($ag) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Program</th>
                    <th>Academic Group Name</th>
                    <th>Academic Group Courses</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
               @if (count($ag) > 0)

                    @foreach ($ag as $key => $program)
                        <tr data-entry-id="{{ $program['id'] }}">
                            <td>{{ App\Helpers\Helper::programIdToName($program->program_id)}}</td>
                            <td>{{ $program->name }}</td>
                             <td><a  data-id="{{$program->id}}" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-xs btn-primary academics-courses" data-toggle="modal" data-target="#exampleModal2{{$program->id}}"><span>View Courses</span></a></td>

                             <td> <a href = "Academic-group-courses\{{ $program['id'] }}"><button style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-primary">Add Academic Group Course</button></a></td>
                             <div class="modal fade" id="exampleModal1{{$program->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-body">
                                      {!! Form::open(['route' => 'admin.academics-groups-courses-store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      <input type="hidden" name="academics_group_id" value="{{$program->id}}">
                                        <div class="form-group">
                                            {!! Form::label('course_id', 'Courses', ['class' => 'control-label']) !!}
                                             <select style="width: 100%" name="course_id[]" class="form-control select2 courses" multiple="multiple">
                                                <option selected="selected" disabled="disabled">---Select---</option>
                                                @if(count($courses) > 0)
                                                    @foreach($courses as $course)
                                                        <option value="{{$course->id}}">{{$course->name}}</option>
                                                    @endforeach
                                                @endif    
                                             </select>
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>           
                                </div>
                            </div>
                            <div class="modal fade" id="exampleModal2{{$program->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Academics Groups Courses</h2>
                                      
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                          <div id="days_{{$program->id}}">
                                              
                                          </div>
                           
                                        </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        
                                    </div>
                                  </div>
                                   
                                </div>
                            </div>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
        $(document).ready(function() {
          selectRefresh();
        });
        function selectRefresh() {
            $(".courses").select2({

            });
        }
        $(function() {
           $(".academics-courses").on('click', function () {   
                   var id=  $(this).data('id');
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('admin.load-academics-group-courses') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            academics_group_id: id
                        },
                        success: function (response) {
                            $.each( response, function(k, v) {
                                if(k == 0)
                                {
                                    $("#days_"+v.academics_group_id).empty();
                                }
                                $("#days_"+v.academics_group_id).append('<input readonly type="text" class="form-control" value="'+v.course_id+'"><br/>');
                            });
                        }
                    });
            })
        });
    </script>
@endsection