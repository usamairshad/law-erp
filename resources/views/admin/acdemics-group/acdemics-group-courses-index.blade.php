@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Academics Group Course</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a  href="{{route('admin.academics-groups-courses-create')}}" class="btn btn-success pull-right">Create</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            {{--<table class="table table-bordered table-striped {{ count($ag) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Program</th>
                    <th>Academic Group Name</th>
                </tr>
                </thead>

                <tbody>
               @if (count($ag) > 0)

                    @foreach ($ag as $program)
                        <tr data-entry-id="{{ $program['id'] }}">
                            <td>{{ App\Helpers\Helper::programIdToName($program->program_id)}}</td>
                            <td>{{ $program->name }}</td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>--}}
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection