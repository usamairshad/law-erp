@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Salary Deduction</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Salary Deduction</h3>
            <a href="{{ url('salary-deduction') }}" class="btn btn-success pull-right">Back</a>
        </div>


        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'url' => ['salary-deduction'], 'id' => 'validation-form']) !!}
        <div class="box-body">


            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
                <div class="form-group col-md-4 @if($errors->has('deduction')) has-error @endif">
                    {!! Form::label('deduction', 'Deduction', ['class' => 'control-label']) !!}
                    {!! Form::text('deduction', old('deduction'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>40,'required','minlength'=>2]) !!}
                    @if($errors->has('deduction'))
                        <span class="help-block">
                        {{ $errors->first('deduction') }}
                    </span>
                    @endif
                </div>
                <div class="orm-group col-md-4 @if($errors->has('fixed')) has-error @endif">
                    {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
                    <select name="type" id="discount" class="form-control" placeholder="required">
                         <option selected disabled="">--Select</option>
                        <option value="1">Fixed</option>
                        <option value="2">Percent</option>
                    </select>
                </div>
                <div class="form-group col-md-4 @if($errors->has('deduction')) has-error @endif">
                    {!! Form::label('amount', 'Amount', ['class' => 'control-label']) !!}
                    {!! Form::text('amount', old('amount'), ['class' => 'form-control', 'placeholder' => '', 'required']) !!}
                    @if($errors->has('amount'))
                        <span class="help-block">
                        {{ $errors->first('amount') }}
                    </span>
                    @endif
                </div>
{{--            <div class="form-group col-md-4 @if($errors->has('country_id')) has-error @endif">--}}
{{--                {!! Form::label('country_id', 'Country Name*', ['class' => 'control-label']) !!}--}}
{{--                {!! Form::select('country_id', $country->prepend('Select Country', ''),old('country_id'), ['class' => 'form-control','required']) !!}--}}

{{--                @if($errors->has('country_id'))--}}
{{--                    <span class="help-block">--}}
{{--                        {{ $errors->first('country_id') }}--}}
{{--                    </span>--}}
{{--                @endif--}}
{{--            </div>--}}
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')


    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
@endsection

