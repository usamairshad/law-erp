@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Child Benefit Configuration</h1>
    </section>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>

        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($childbenefitconfigs) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Number of children</th>
                    <th>Discount</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @php($r = 1)
                @if (count($childbenefitconfigs) > 0)
                    @foreach ($childbenefitconfigs as $childbenefitconfig)
                        <tr data-entry-id="{{ $childbenefitconfig->id }}">
                            <td> {{$r}} </td>
                            <td>{{ $childbenefitconfig->child_name }}</td>
                            <td>{{$childbenefitconfig->discount}}</td>
                            <td>
                                {{--@if(Gate::check('regions_edit'))--}}
                                    <a href="{{ url('child/benefit-config',[$childbenefitconfig->id]) }}/edit" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                {{--@endif--}}

                                {{-- @if(Gate::check('regions_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.regions.destroy', $Region->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif --}}
                            </td>


                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection