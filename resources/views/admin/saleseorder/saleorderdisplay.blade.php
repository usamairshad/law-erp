@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1> Sale Order</h1>
    </section>
@stop
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">View</h3>
            <a href="{{ route('admin.saleorder.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th width="10%">Order Date</th>
                    <td>{{ $SalesOrderModel->order_date }}</td>
                    <th width="10%">Delivery Date</th>
                    <td>{{ $SalesOrderModel->delivery_date }}</td>
                    <th width="10%">Customer Name</th>
                    <td>{{ $SalesOrderModel->CustomersModel->name }}</td>
                </tr>




                </tbody>
            </table>

            <!-- Slip Area Started -->
            <section class="content-header" style="padding: 10px 15px !important;">
                <h1>Product View</h1>
            </section>


            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Price</th>
                    {{--<th>Weight</th>--}}
                    {{--<th>Total Weight</th>--}}
                </tr>
                </thead>
                <tbody>

                @forelse($Products as $product)
                    <tr>
                        <td>{{ $product->products_name }}</td>
                        <td>{{ $product->quantity }}</td>
                        <td>{{ $product->unit_price }}</td>
                        <td>{{ $product->total_price }}</td>
                        {{--<td>{{ $product->weight }}{{ $product->UnitModel->shortcode }}</td>--}}
                        {{--<td>{{ $product->total_weight }}{{ $product->UnitModel->shortcode }}</td>--}}
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No Record Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
        <!-- /.box-body -->
    </div>






@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.products.mass_destroy') }}';
    </script>
@endsection

