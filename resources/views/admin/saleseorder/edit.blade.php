@extends('layouts.app')

@section('stylesheet')

    <style>
        .example-template > li {
            display: inline-block;
            float: left;
            margin: 0 8px 8px 0;
        }
        .list-style-none{
            list-style: none;
        }
    </style>
@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Purchase Order</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add Performa Detail</h3>
            <a href="{{ route('admin.orders.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::model($Orders, ['method' => 'PUT', 'id' => 'validation-form','route' => ['admin.orders.update', $Orders->id]]) !!}

        <div class="box-body">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-4 form-group @if($errors->has('perfomra_no')) has-error @endif">
                        {!! Form::label('perfomra_no', 'Performa Number*', ['class' => 'control-label']) !!}
                        <input id="perfomra_no" class="form-control" type="text" name="perfomra_no" value="{{$Orders->perfomra_no}}" />
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('sal_no')) has-error @endif">
                        {!! Form::label('sale_no', 'Sales Order Number*', ['class' => 'control-label']) !!}
                        <input id="sale_no" class="form-control" type="text" name="sale_no" value="{{$Orders->sale_order_no}}" />
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('shipping')) has-error @endif">
                        {!! Form::label('shipping', 'Shipping Via*', ['class' => 'control-label']) !!}
                        <input id="shipping" class="form-control" type="text" name="shipping" value="{{$Orders->shipping}}" />
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-4 form-group @if($errors->has('sal_date')) has-error @endif">
                        {!! Form::label('sal_date', 'Purchase Date', ['class' => 'control-label']) !!}
                        {!! Form::text('sal_date', old('sal_date'), ['id' => 'sal_date','class' => 'form-control datepicker', 'placeholder' => '' ]) !!}
                        @if($errors->has('sal_date'))
                            <span class="help-block">
                            {{ $errors->first('sal_date') }}
                            </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('valid_upto')) has-error @endif">
                        {!! Form::label('valid_upto', 'On/About', ['class' => 'control-label']) !!}
                        {!! Form::text('valid_upto', old('valid_upto'), ['id' => 'valid_upto','class' => 'form-control datepicker', 'placeholder' => '']) !!}
                        @if($errors->has('valid_upto'))
                            <span class="help-block">
                                {{ $errors->first('valid_upto') }}
                                </span>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('soruce')) has-error @endif">
                        {!! Form::label('soruce', 'From*', ['class' => 'control-label']) !!}
                        <input id="soruce" class="form-control" type="text" name="soruce" value="{{$Orders->soruce}}" />
                    </div>


                </div>

                <div class="col-xs-12 form-group">

                    <div class="col-xs-4 form-group @if($errors->has('destination')) has-error @endif">
                        {!! Form::label('destination', 'To*', ['class' => 'control-label']) !!}
                        <input id="destination" class="form-control" type="text" name="destination" value="{{$Orders->destination}}" />
                    </div>
                    <div class="col-xs-4  form-group @if($errors->has('status')) has-error @endif">
                        {!! Form::label('status', 'Order Status *', ['class' => 'control-label']) !!}
                        {!!  Form::select('status', [
                            '' => 'Select Order Status',
                            '1' => 'In Process',
                            '2' => 'Approved',
                            '3' => 'In Bound',
                            '4' => 'In Transit',
                            '5' => 'Completed',],
                            null,
                            ['class' => 'form-control select2'])
                        !!}

                        @if($errors->has('status'))
                            <span class="help-block">
                        {{ $errors->first('status') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 form-group">

                    <div class="col-xs-4 form-group @if($errors->has('po_number')) has-error @endif">
                        {!! Form::label('po_number', 'Po No*', ['class' => 'control-label']) !!}
                        {!! Form::text('po_number', $Orders->po_number, ['class' => 'form-control', 'placeholder' => '']) !!}
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('perfomra_no')) has-error @endif">
                        {!! Form::label('po_date', 'Po Date *', ['class' => 'control-label']) !!}
                        {!! Form::text('po_date', old('po_date'), ['id'=>'po_date','class' => 'form-control datepicker', 'placeholder' => '']) !!}
                    </div>
                    <div class="col-xs-4 form-group @if($errors->has('sup_id')) has-error @endif">
                        {!! Form::label('sup_id', 'Brands *', ['class' => 'control-label']) !!}
                        <select id="sup_id" name="sup_id" class="form-control select2">
                            @if ($SuppliersModel->count())
                                <option value="">Select Brands</option>
                                @foreach($SuppliersModel as $Supplier)
                                    <option value="{{ $Supplier->id }}"{{$Orders->sup_id == $Supplier->id ? 'selected="selected"' : ''}}>{{ $Supplier->sup_name }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('sup_id'))
                            <span class="help-block">
                {{ $errors->first('sup_id') }}
                </span>
                        @endif
                    </div>
                </div>

                    <div id="productmodel_col"></div>
                    <div class="edit-area col-xs-12">

                        <div class="controls">
                            <div class="box-header with-border">
                                <h3 class="box-title">Update Product Detail</h3>
                            </div>

                            <ul class="heading_ul list-style-none text-center" style="float: left; width: 100%; margin-bottom:8px; padding: 0px;">

                                <li class="col-xs-3">
                                    {!! Form::label('product_id', 'Products', ['class' => 'control-label']) !!}
                                </li>
                                <li class="col-xs-2">
                                    {!! Form::label('unit_name', 'Unit Name', ['class' => 'control-label']) !!}
                                </li>
                                <li class="col-xs-2">
                                    {!! Form::label('qty', 'Qunity', ['class' => 'control-label']) !!}
                                </li>
                                <li class="col-xs-2">
                                    {!! Form::label('amount', 'Per Unit Amount', ['class' => 'control-label']) !!}
                                </li>
                                <li class="col-xs-2">
                                    {!! Form::label('amount', 'Total Amount', ['class' => 'control-label']) !!}
                                </li>
                            </ul>

                        </div>
                    </div>
                    {{--{{dd($OrderDetail)}}--}}
                    <div class="col-xs-12">
                        <div>
                            <?php
                                $a = 0;
                            ?>
                            @foreach ($OrderDetail as $Details)
                                    <?php
                                    $a++;
                                    ?>
                            <ul class="example-template" style="float: left; width: 100%; margin-bottom:8px; padding: 0px;">
                                <li class="col-xs-3">
                                    <select id="product_id_" name="product_id[]" class="form-control select2">
                                        @if ($Products->count())
                                            <option value="">Select Product</option>
                                            @foreach($Products as $Product)
                                                <option value="{{ $Product->id }}"{{$Details->product_id == $Product->id ? 'selected="selected"' : ''}}>{{ $Product->products_name  }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('stock_item_id'))
                                        <span class="help-block">
                                            {{ $errors->first('stock_item_id') }}
                                            </span>
                                    @endif
                                </li>

                                <li class="col-xs-2">
                                    <select id="unit_name" name="unit_name[]" class="form-control select2">
                                        @if ($UnitsModel->count())
                                            <option value="">Select Unit of Measurement</option>
                                            @foreach($UnitsModel as $Units)
                                                <option value="{{ $Units->id }}"{{$Details->unit_name == $Units->id ? 'selected="selected"' : ''}}>{{ $Units->shortcode  }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    {{--{!! Form::text('unit_name[]', $Details->unit_name, ['id' => 'unit_name','class' => 'form-control', 'placeholder' => '']) !!}--}}
                                </li>
                                <li class="col-xs-2">
                                    {!! Form::text('qunity[]', $Details->qty, ['id' => 'qunity_'.$a, 'class' => 'form-control', 'placeholder' => '','data-row-id'=>$a]) !!}
                                </li>
                                <li class="col-xs-2">
                                    {!! Form::text('p_amount[]', $Details->unit_price, ['id' => 'p_amount_'.$a, 'class' => 'form-control test', 'placeholder' => '','data-row-id'=>$a]) !!}
                                </li>
                                <li class="col-xs-2">
                                    {!! Form::text('amount[]', $Details->amount, ['id' => 'amount_'.$a, 'class' => 'form-control', 'placeholder' => '','data-row-id'=>$a]) !!}
                                </li>
                                <li class="col-xs-2">
                                    {!! Form::hidden('perchas_id[]', $Details->id, ['id' => 'perchas_id', 'class' => 'form-control', 'placeholder' => '']) !!}
                                </li>
                            </ul>
                            @endforeach
                        </div>

                    </div>
            </div>


        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
        {{--@include('admin.entries.voucher.sale_voucher.entries_template')--}}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/admin/orders/update_modify.js') }}" type="text/javascript"></script>

    <script>
          $(document).on('blur','.test',function () {
            amount = $(this).val();
            rowId = $(this).attr('data-row-id');
            quantity = $('#qunity_'+rowId).val();
            total = amount * quantity;
            $('#amount_'+rowId).val(total.toFixed(2));
        })
    </script>
@endsection