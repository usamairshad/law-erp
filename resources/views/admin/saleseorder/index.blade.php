@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Sale order List</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('orders_create'))
                <a href="{{ route('admin.saleorder.create') }}" class="btn btn-success pull-right">Add New Sale Order</a>
            @endif
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($SalesOrderModel) > 0 ? 'datatable' : '' }}" id="order-table">
                <thead>
                <tr>
                    <th width="8%">S: No</th>
                    <th>Order Date</th>
                    <th>Delivery  Date</th>
                    <th>Customer  Name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){

            $('#order-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.saleorder.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'order_date', name: 'order_date' },
                    { data: 'delivery_date', name: 'delivery_date' },
                    { data: 'customer_id', name: 'customer_id' },
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},
                ]
            });
        });
    </script>
@endsection


