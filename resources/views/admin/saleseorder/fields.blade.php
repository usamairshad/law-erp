
{{--{{dd($SuppliersModel)}}--}}
<div class="row">
    <div class="col-xs-12 form-group">

        {{--document_date--}}
        <div class="form-group col-md-4  @if($errors->has('request_date')) has-error @endif">
            {!! Form::label('order_date', 'Order Date*', ['class' => 'control-label']) !!}
            {!! Form::text('order_date',old('order_date'), ['class' => 'form-control datepicker']) !!}
            @if($errors->has('order_date'))
                <span class="help-block">
            {{ $errors->first('order_date') }}
        </span>
            @endif
        </div>
        {{--document_date--}}


        {{--Due Date--}}
        <div class="form-group col-md-4  @if($errors->has('require_date')) has-error @endif">
            {!! Form::label('delivery_date', 'Delivery Date*', ['class' => 'control-label']) !!}
            {!! Form::text('delivery_date',old('delivery_date'), ['class' => 'form-control datepicker']) !!}
            @if($errors->has('delivery_date'))
                <span class="help-block">
            {{ $errors->first('delivery_date') }}
        </span>
            @endif
        </div>
        {{--Due Date--}}

        {{--Customer Name--}}
        <div class="form-group col-md-4  @if($errors->has('customer_name')) has-error @endif">
            {!! Form::label('Customer_name', 'Customer Name*', ['class' => 'control-label']) !!}
            <select id="Customer_name" name="Customer_name" class="form-control select2" data-placeholder="Select Customer">
                <option value="">Select Customer</option>
                @foreach($CustomersModel  as $Customers)
                    <option value="{{$Customers->id}}">{{$Customers->name}}</option>
                @endforeach
            </select>

            <span id="Customer_id_handler"></span>
            @if($errors->has('document_no'))
                <span class="help-block">
                        {{ $errors->first('Customer_name') }}
                     </span>
            @endif
        </div>
        {{--Customer Name--}}


    </div>
</div>


<div class="manually_product_list col-lg-12 col-md-12 col-sm-12">
    <div class="about_featured">
        <div class="panel-group" id="accordion">

            <div class="panel panel-default wow fadInLeft">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#11">
                            <span class="fa fa-check-square-o"></span><b>Product List</b>
                        </a>
                    </h4>
                </div>

                <div id="11" class="panel-collapse collapse in">


                    <div class="panel-body pad table-responsive">
                        <button onclick="FormControls.createLineItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>R</u>ow</button>
                        <table class="table table-bordered table-striped" id="users-table">
                            <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Uom</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Total</th>
                                {{--<th>Weight</th>--}}
                                {{--<th>Total Weight</th>--}}
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php  $counter=1 ?>
                            <tr id="line_item-1">
                            {{--Product Name--}}
                            <td>
                                <div class="form-group  @if($errors->has('product_id')) has-error @endif">

                                    {!! Form::select('line_items[product_id_r][1]',array(), old('product_id'), ['id' => 'line_item-product_id-1','class' => 'form-control description-data-ajax select2 dublicate_product', 'style'=>'width:100%;' , 'required' => 'true']) !!}
                                    @if($errors->has('product_id'))
                                          <span class="help-block">
                                             {{ $errors->first('product_id') }}
                                         </span>
                                    @endif
                                </div>
                            </td>
                            {{--Product Name--}}

                            {{--vendor--}}
                            <td>

                                    <div class="form-group @if($errors->has('vendor')) has-error @endif">
                                        <select id="uom-id-1" name="line_items[uom_r][1]" class="form-control select2" required>
                                            <option value="">Select Uom</option>
                                            @foreach($UnitsModel  as $Unit)
                                                <option value="{{$Unit->id}}">{{$Unit->shortcode}}</option>
                                            @endforeach
                                        </select>

                                        <span id="uom_handler"></span>
                                        @if($errors->has('uom'))
                                            <span class="help-block">
                                                {{ $errors->first('uom') }}
                                            </span>
                                        @endif
                                    </div>

                            </td>
                            {{--vendor--}}

                                {{--qunity--}}
                                <td>
                                    <div class="form-group  @if($errors->has('qunity')) has-error @endif">
                                        {!! Form::number('line_items[qunity_r][1]', old('qunity'), ['id' => 'line_items_quantity_id-1','class' => 'form-control manual_quantity maxQty' ,'data-row-id'=>'1', 'required' => 'true' ,'min'=>1]) !!}
                                        @if($errors->has('unit_price'))
                                            <span class="help-block">
                                    {{ $errors->first('unit_price') }}
                                    </span>
                                        @endif
                                    </div>
                                </td>
                                {{--qunity--}}

                            {{--unit_price--}}
                            <td>
                                <div class="form-group  @if($errors->has('unit_price')) has-error @endif">
                                    {!! Form::number('line_items[Unit_price_r][1]', old('qunity'), ['id' => 'line_items_Unit_price_id-1','class' => 'form-control manual_unit_price' ,'data-row-id'=>'1', 'required' => 'true' , 'min'=>1]) !!}
                                    @if($errors->has('unit_price'))
                                        <span class="help-block">
                                    {{ $errors->first('unit_price') }}
                                    </span>
                                    @endif
                                </div>
                            </td>
                            {{--unit_price--}}


                            {{--total_price--}}
                            <td>
                            <div class="form-group  @if($errors->has('total_price')) has-error @endif">
                            {!! Form::text('line_items[total_price_r][1]', old('qunity'), ['id' => 'line_items_total_price_id-1', 'class' => 'form-control cal_sum' , 'required' => 'true' ,'readonly'=>'true' , 'min'=>1 ]) !!}
                                @if($errors->has('total_price'))
                                    <span class="help-block">
                                    {{ $errors->first('total_price') }}
                                    </span>
                                @endif
                            </div>
                            </td>
                            {{--total_price--}}


                                {{--Weight--}}
                                {{--<td>--}}
                                    {{--<div class="form-group  @if($errors->has('weight')) has-error @endif">--}}
                                        {{--{!! Form::number('line_items[weight_r][1]', old('weight'), ['id' => 'line_items_weight_id-1', 'class' => 'form-control manual_weight' , 'data-row-id'=>'1', 'required' => 'true', 'min'=>1]) !!}--}}
                                        {{--@if($errors->has('weight'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--{{ $errors->first('weight') }}--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                                {{--Weight--}}

                                {{--total_Weight--}}
                                {{--<td>--}}
                                    {{--<div class="form-group  @if($errors->has('total_weight')) has-error @endif">--}}
                                        {{--{!! Form::number('line_items[total_weight_r][1]', old('qunity'), ['id' => 'line_items_total_weight_id-1', 'class' => 'form-control' , 'readonly'=>'true' ,'required' => 'true', 'min'=>1]) !!}--}}
                                        {{--@if($errors->has('total_weight'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--{{ $errors->first('total_weight') }}--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                                {{--total_Weight--}}


                            {{--Submitt button--}}
                            <td><button id="line_item-del_btn-1" onclick="FormControls.destroyLineItem('0');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
                            {{--Submitt button--}}

                            </tr>


                            <input type="hidden" id="line_item-global_counter" value="<?php  echo ++$counter ?>"   />

                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>
</div>

