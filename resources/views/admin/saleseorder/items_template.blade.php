<table style="display: none;">
    <tbody id="line_item-container" >
    <tr id="line_item-######">
        <td >
            <div class="form-group" style="margin-bottom: 0px !important;">
                <div class="form-group" style="margin-bottom: 0px !important;">

                 {!! Form::select('line_items[product_id_r][######]',array(), old('product_id'), ['id' => 'line_item-product_id-######','class' => 'form-control dublicate_product', 'required' => 'true']) !!}
                    <span id="product_id_handler"></span>
                    @if($errors->has('uom'))
                        <span class="help-block">
                          {{ $errors->first('product_id') }}
                    </span>
                    @endif
            </div>
            </div>
        </td>

        <td>

            <div class="form-group  @if($errors->has('vendor')) has-error @endif">
                <select id="uom-id-######" name="line_items[uom_r][######]" class="form-control select2" required>
                    <option value="">Select Uom</option>
                    @foreach($UnitsModel  as $Unit)
                        <option value="{{$Unit->id}}">{{$Unit->shortcode}}</option>
                    @endforeach
                </select>

                </select>

                <span id="uom_handler"></span>
                @if($errors->has('uom'))
                    <span class="help-block">
                          {{ $errors->first('uom') }}
                    </span>
                @endif
            </div>

        </td>

        <td>
            <div class="form-group" style="margin-bottom: 0px !important;" >
                {!! Form::number('line_items[qunity_r][######]', old('qunity'), ['id' => 'line_items_quantity_id-######',  'class' => 'form-control manual_quantity' , 'data-row-id'=>'######', 'required' => 'true']) !!}
            </div>
        </td>


        <td>
            <div class="form-group  @if($errors->has('unit_price')) has-error @endif">
                {!! Form::number('line_items[Unit_price_r][######]', old('qunity'), ['id' => 'line_items_Unit_price_id-######','class' => 'form-control manual_unit_price' ,'data-row-id'=>'######', 'required' => 'true']) !!}
                @if($errors->has('unit_price'))
                    <span class="help-block">
                       {{ $errors->first('unit_price') }}
                    </span>
                @endif
            </div>
        </td>


        <td>
            <div class="form-group  @if($errors->has('total_price')) has-error @endif">
                {!! Form::text('line_items[total_price_r][######]', old('qunity'), ['id' => 'line_items_total_price_id-######','class' => 'form-control cal_sum' , 'required' => 'true' ,'readonly'=>'true' , 'min'=>1]) !!}
                @if($errors->has('total_price'))
                    <span class="help-block">
                        {{ $errors->first('total_price') }}
                    </span>
                @endif
            </div>
        </td>


        {{--Weight--}}
        {{--<td>--}}
            {{--<div class="form-group  @if($errors->has('weight')) has-error @endif">--}}
                {{--{!! Form::number('line_items[weight_r][######]', old('weight'), ['id' => 'line_items_weight_id-######', 'class' => 'form-control  manual_weight' , 'data-row-id'=>'######', 'required' => 'true' , 'min'=>1]) !!}--}}
                {{--@if($errors->has('weight'))--}}
                    {{--<span class="help-block">--}}
                         {{--{{ $errors->first('weight') }}--}}
                    {{--</span>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</td>--}}
        {{--Weight--}}

        {{--total_Weight--}}
        {{--<td>--}}
            {{--<div class="form-group  @if($errors->has('total_weight')) has-error @endif">--}}
                {{--{!! Form::number('line_items[total_weight_r][######]', old('qunity'), ['id' => 'line_items_total_weight_id-######', 'readonly'=>'true' , 'class' => 'form-control ' , 'required' => 'true', 'min'=>1 ]) !!}--}}
                {{--@if($errors->has('total_weight'))--}}
                    {{--<span class="help-block">--}}
                          {{--{{ $errors->first('total_weight') }}--}}
                    {{--</span>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</td>--}}
        {{--total_Weight--}}



        <td><button id="line_item-del_btn-######" onclick="FormControls.destroyLineItem('######');" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
    </tr>
    </tbody>
</table>