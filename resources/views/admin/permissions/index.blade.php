@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Permissions</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('erp_permissions_create'))
                <a href="{{ route('admin.permissions.create') }}" class="btn btn-success pull-right">Add New Permission</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Permissions) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Name</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($Permissions) > 0)
                    @foreach ($Permissions as $Permission)
                        <tr data-entry-id="{{ $Permission->id }}">
                            <td>{{ $Permission->id }}</td>
                            <td>{{ $Permission->title }}</td>
                            <td>{{ $Permission->name }}</td>
                            <td>{{ $Permission->created_at }}</td>
                            <td>
                                @if($Permission->status && Gate::check('erp_permissions_inactive'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['admin.permissions.inactive', $Permission->id])) !!}
                                    {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}
                                @elseif(!$Permission->status && Gate::check('permissions_active'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['admin.permissions.active', $Permission->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                                @endif

                                @if(Gate::check('erp_permissions_edit'))
                                    <a href="{{ route('admin.permissions.edit',[$Permission->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                @if(Gate::check('permissions_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.permissions.destroy', $Permission->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection