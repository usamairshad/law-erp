@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Entries</h3>
            @if(Gate::check('erp_entries_create'))
            <div class="dropdown" style = "float:right; margin-bottom:20px; margin-right:50px" >
	<button aria-expanded="false" aria-haspopup="true" class="btn ripple btn-info"
	data-toggle="dropdown" type="button">Create Voucher<i class="fas fa-caret-down ml-1"></i></button>
	<div class="dropdown-menu tx-13">
		<a class="dropdown-item" href="{{ route('admin.voucher.gjv_create') }}">GJV - Journal Voucher</a>
         <a class="dropdown-item" href="{{ route('admin.voucher.crv_create') }}" >CRV - Cash Receipt Voucher</a>
		<a class="dropdown-item" href="{{ route('admin.voucher.cpv_create') }}">CPV - Cash Payment Voucher</a>
        <a  class="dropdown-item" href="{{ route('admin.voucher.brv_create') }}">BRV - Bank Receipt Voucher</a>
        <a   class="dropdown-item" href="{{ route('admin.voucher.bpv_create') }}">BPV - Bank Payment Voucher</a>
	</div>
</div>

                {{--<a href="{{ route('admin.entries.create') }}" class="btn btn-success pull-right">Add New Entrie</a>--}}
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped" id="entries_table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Entry Type</th>
                    <th>Voucher Date</th>
                    <th>Number</th>
                    <th>Narration</th>
                    <th style="text-align: right;">Dr Amount</th>
                    <th style="text-align: right;">Cr Amount</th>
                    <th width="20%">Actions</th>
                </tr>
                </thead>

                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('#entries_table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.entries.getData') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'entry_type', name: 'entry_type' },
                    { data: 'voucher_date', name: 'voucher_date' },
//                    { data: 'number', name: 'number' },
                    { data: 'number', render: function ( data, type, row ) {
                        return row.voucher_month +'/'+ row.number + '';
                    }},
                    { data: 'narration', name: 'narration' },
                    { data: 'dr_total', name: 'dr_total' },
                    { data: 'cr_total', name: 'cr_total' },
                    { data: 'action', name: 'action' , orderable: false, searchable: false},

                ]
            });
        });
    </script>
@endsection