@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Banks</h3>
            <a href="{{ route('admin.banks.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Banks, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.banks.update', $Banks->id]]) !!}
        <div class="box-body" style ="margin-top:60px;">
            <div class="col-xs-6 form-group @if($errors->has('bank_name')) has-error @endif">
                {!! Form::label('bank_name', 'Bank Name *', ['class' => 'control-label']) !!}
                {!! Form::text('bank_name', old('bank_name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>100,'required','minlength'=>2]) !!}
                @if($errors->has('bank_name'))
                    <span class="help-block">
                            {{ $errors->first('bank_name') }}
                        </span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/admin/banks/create_modify.js') }}" type="text/javascript"></script>
@endsection