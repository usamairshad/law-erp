@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Banks</h3>
        <a href="{{ route('admin.banks.create') }}" class="btn btn-success pull-right" style = "float:right;">@lang('global.app_add_new')</a>
    </div>

    <div class="panel panel-default">
        <div class="panel-body table-responsive">
            <table class="table table-bBanked table-striped {{ count($Banks) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th>S: No</th>
                        <th>Bank Name</th>
                        <th>Action</th>

                    </tr>
                </thead>

                <tbody>
                    @php($r = 1)
                    @if (count($Banks) > 0)
                        @foreach ($Banks as $Bank)

                            <tr data-entry-id="{{ $Bank->id }}">
                                <td> {{$r}} </td>
                                <td>{{ $Bank->bank_name }}</td>
                                <td>
                                    <a href="{{ route('admin.banks.edit',[$Bank->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                    {{--{!! Form::open(array(--}}
                                        {{--'style' => 'display: inline-block;',--}}
                                        {{--'method' => 'DELETE',--}}
                                        {{--'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",--}}
                                        {{--'route' => ['admin.banks.destroy', $Bank->id])) !!}--}}
                                    {{--{!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}--}}
                                    {{--{!! Form::close() !!}--}}
                                </td>

                            </tr>
                            @php($r++)
                        @endforeach

                    @else
                        <tr>
                            <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.banks.mass_destroy') }}';
    </script>
@endsection