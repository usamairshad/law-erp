@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
.form-group{
    float:left;
}
.design
{

}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Company Details</h3>
        <div class="box-header with-border">

            <a href="{{ url()->previous() }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <div class="panel-body pad table-responsive">

            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>

                </div>
            @endif
                <table class="table table-bordered table-striped">
                    @foreach($company_details as $company_detail)
                    <tr>
                        <th>Name</th>
                        <td>{{$company_detail->name ?? ''}}</td>
                        <th>Description</th>
                        <td>{{$company_detail->description ?? ''}}</td>

                    </tr>
                    <tr>
                        <th>NTN NO</th>
                        <td>{{$company_detail->ntn ?? ''}}</td>
                        <th>Tell No</th>
                        <td>{{$company_detail->phone ?? ''}}</td>

                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>{{$company_detail->address ?? '' }}</td>
                        <th>Fax</th>
                        <td>{{$company_detail->fax ?? ''}}</td>
                    </tr>
                    <tr>
                        <th>Company Logo</th>
                        <td> <img src="{{ asset('images/' . $company_detail->logo) ?? ''}}" /> </td>
                    </tr>

                    @endforeach
                </table>
        </div>
    </div>




@stop


