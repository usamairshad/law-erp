@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Companies</h3>
            <a href="{{ route('admin.companies.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.companies.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('company_name', old('name'), ['id' => 'name' ,'class' => 'form-control', seletecd 'placeholder' => 'Enter Company Name','maxlength'=>'50', 'required']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-2 @if($errors->has('ntn')) has-error @endif">
                    {!! Form::label('ntn', 'NTN No', ['class' => 'control-label']) !!}
                     <input type="text" name="ntn" placeholder="Enter NTN" class="form-control" />
            </div>
            <div class="form-group col-md-2 @if($errors->has('ntn')) has-error @endif">
                    {!! Form::label('phone', 'Tel No*', ['class' => 'control-label']) !!}
                     <input type="tel" data-mask="000-00000000" placeholder="0XX-XXXXXXX" name="phone" required="required" class="form-control home_phone" />
            </div>
            <div class="form-group col-md-2 @if($errors->has('ntn')) has-error @endif">
                    {!! Form::label('fax', 'Fax', ['class' => 'control-label']) !!}
                     <input type="text" name="fax" placeholder="Enter Fax" class="form-control" />
            </div>
            <div class="form-group col-md-3 @if($errors->has('logo')) has-error @endif">
                    {!! Form::label('logo', 'Company Logo*', ['class' => 'control-label', ]) !!}
                     <input type="file" name="logo" required="required" class="form-control" />
            </div>

            <div class="form-group col-md-5 @if($errors->has('c_address')) has-error @endif">
                {!! Form::label('c_address', 'Address*', ['class' => 'control-label']) !!}
                {!! Form::text('c_address', old('c_address'), ['class' => 'form-control', 'placeholder' => 'Enter Description','minlength'=>'2','maxlength'=>'255', 'required']) !!}
                <span id="field_error"></span>
                @if($errors->has('c_address'))

                    <span class="help-block">
                        {{ $errors->first('c_address') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-6 @if($errors->has('description')) has-error @endif">
                {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Enter Description','minlength'=>'2','maxlength'=>'255']) !!}
                <span id="field_error"></span>
                @if($errors->has('description'))

                    <span class="help-block">
                        {{ $errors->first('description') }}
                    </span>
                @endif
            </div>
            
        </div>
        <div id="show" style="display: none">
            <div id="box" class="box-body">
                <h2>Create Branch</h2>
                <div class="form-group col-md-3 @if($errors->has('branch_name')) has-error @endif">
                    {!! Form::label('branch_name', 'Name*', ['class' => 'control-label']) !!}
                   <input type="text" name="branch_name[0]"  required="required" placeholder="Enter Branch Name" class="form-control" />
                </div>
                <div class="form-group col-md-2 @if($errors->has('branch_code')) has-error @endif">
                    {!! Form::label('branch_code', 'Branch Code*', ['class' => 'control-label']) !!}
                     <input type="text" name="branch_code[0]" required="required" placeholder="Enter Branch Code" class="form-control" />
                </div>
                <div class="form-group col-md-3 @if($errors->has('branch_code')) has-error @endif">
                    {!! Form::label('city', 'Select City*', ['class' => 'control-label']) !!}
                     <select required="required" class="form-control" name="city[0]">
                        <option selected="selected" disabled="disabled">Select City</option>
                        @foreach($cities as $city)
                            <option value="{{$city->id}}">{{$city->name}}</option>
                        @endforeach
                     </select>
                </div>
                <div class="form-group col-md-2 @if($errors->has('branch_phone')) has-error @endif">
                    {!! Form::label('branch_phone', 'Tel No*', ['class' => 'control-label']) !!}
                     <input type="tel" data-mask="000-00000000" placeholder="0XX-XXXXXXX" name="branch_phone[0]" required="required" class="form-control home_phone" />
                </div>
                <div class="form-group col-md-2 @if($errors->has('branch_fax')) has-error @endif">
                    {!! Form::label('branch_fax', 'Fax', ['class' => 'control-label']) !!}
                     <input type="text" name="branch_fax[0]" placeholder="Enter Fax" class="form-control" />
                </div>
                <div class="form-group col-md-2 @if($errors->has('branch_cell')) has-error @endif">
                    {!! Form::label('branch_cell', 'Mobile No*', ['class' => 'control-label']) !!}
                     <input type="text" data-mask="0000-0000000"  placeholder="03XX-XXXXXXX" name="branch_cell[0]" required="required" class="form-control phone" />
                </div>

                <div class="form-group col-md-5 @if($errors->has('address')) has-error @endif">
                    {!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
                    <input type="text" required="required" name="address[0]" placeholder="Enter Branch Address" class="form-control" />
                </div>
                <div class="form-group col-md-1">
                    <button style="margin-top: 22px" type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div> 
            <div id="secondbox">
                
            </div>  
            
        </div>
        <button  class="btn btn-success col-md-12" > Save</button>
        <!-- /.box-body -->
        {!! Form::close() !!}
    </div>
@stop




