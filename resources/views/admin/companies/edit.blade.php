@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
.form-group{
    float:left;
}
.design
{

}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Companies</h3>
            <a href="{{ route('admin.companies.index') }}" style ="float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::model($Company, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.companies.update', $Company->id]]) !!}
        <div class="box-body" style = "margin-top:40px;">
            <div class="form-group col-md-4 @if($errors->has('name')) has-error @endif">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>'50', 'required']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
                {!! Form::label('ntn', 'NTN', ['class' => 'control-label']) !!}
                <input type="text" name="ntn" value="{{$Company->ntn}}" class="form-control">
            </div>
            <div class="form-group col-md-2 @if($errors->has('name')) has-error @endif">
                {!! Form::label('phone', 'Tel No*', ['class' => 'control-label']) !!}
                <input type="tel" data-mask="000-00000000" placeholder="0XX-XXXXXXX" name="phone" value="{{$Company->phone}}" required="required" class="form-control home_phone">
            </div>
            <div class="form-group col-md-2 @if($errors->has('name')) has-error @endif">
                {!! Form::label('fax', 'Fax', ['class' => 'control-label']) !!}
                <input type="text" name="fax" value="{{$Company->fax}}" class="form-control">
            </div>
            <div class="form-group col-md-12 @if($errors->has('name')) has-error @endif">
                {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                <input type="text" name="description" value="{{$Company->description}}" class="form-control">
            </div>

            <div class="form-group col-md-12 @if($errors->has('address')) has-error @endif">
                {!! Form::label('address', 'Address', ['class' => 'control-label']) !!}
                {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'255', 'required']) !!}
                @if($errors->has('address'))
                    <span class="help-block">
                        {{ $errors->first('address') }}
                    </span>
                @endif
            </div>
            
            
                @foreach($groups as $group)
                    <div class="col-md-4" style = "display:none;">
                        <div class="form-group">
                            <label for="group">{{$group['name']}}</label>
                            <input type="checkbox" name="groups[]" value="{{$group['id']}}" {{ (is_array(old('groups')) and in_array($group['id'], old('groups'))) ? ' checked' : '' }} >
                        </div>
                    </div>
                @endforeach 
        </div>
        <!-- /.box-body -->

        <div class="box-footer" style = "float:left;">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.phone').mask('0000-0000000');
            $('.cnic').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');   
        })
    </script>
@endsection