@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Companies</h3>
            @if(Gate::check('erp_companies_create'))
                <a href="{{ route('admin.companies.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Company</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Companies) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>View Company Branches</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($Companies) > 0)
                    @foreach ($Companies as $Company)
                        <tr data-entry-id="{{ $Company->id }}">
                            <td>{{ $Company->name }}</td>
                            <td>{{ $Company->address}}</td>
                             <td>
                                 <a href="{{route('admin.view-company-branch',[$Company->id])}}"><button class="btn btn-primary">View Branches</button></a>
                                 <a href="{{url('companies-list',$Company->id)}}"><button class="btn btn-primary">Detail</button></a>
                             </td>
                            <td>
                                @if(Gate::check('erp_companies_edit'))
                                    <a href="{{ route('admin.companies.edit',[$Company->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif
                                @if(Gate::check('companies_destroy'))
                                    {{--{!! Form::open(array(--}}
                                        {{--'style' => 'display: inline-block;',--}}
                                        {{--'method' => 'DELETE',--}}
                                        {{--'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",--}}
                                        {{--'route' => ['admin.companies.destroy', $Company->id])) !!}--}}
                                    {{--{!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}--}}
                                    {{--{!! Form::close() !!}--}}
                                @endif
                            </td>


                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection