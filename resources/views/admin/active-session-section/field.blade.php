




<div class="form-group col-md-6 @if($errors->has('section_id')) has-error @endif" style = "float:left;">
    {!! Form::label('section_id', 'Section*', ['class' => 'control-label']) !!}
        <select class="form-control  section_id" name="section_id" required="required"> 
            <option value="0" selected="disable">--select</option>
            @foreach($sessionList as $session)
            <option value="{{$session->id}}">{{$session->name}}</option>
            @endforeach
        </select>
</div>

<div class="form-group col-md-6 @if($errors->has('section_id')) has-error @endif"  style = "float:left;">
    {!! Form::label('active_session_id', 'Active Session*', ['class' => 'control-label']) !!}
        <select class="form-control  section_id" name="active_session_id" required="required"> 
            <option value="0" selected="disable">--select</option>
            @foreach($activesessionList as $activesession)
            <option value="{{$activesession->id}}">{{$activesession->title}}</option>
            @endforeach
        </select>
</div>


