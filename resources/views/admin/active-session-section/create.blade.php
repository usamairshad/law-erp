@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Active Session Section</h3>
            
            <a href="{{ url('active-session-section') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
            

        </div>

       {!! Form::open(['method' => 'POST', 'url' => ['active-session-section'], 'id' => 'validation-form']) !!}
       {!! Form::token(); !!}
        <div class="box-body" style = "margin-top:40px;">
            @include('admin.active-session-section.field')
        </div>

        <div class="box-footer">
            <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/branches/create_modify.js') }}" type="text/javascript"></script>
@endsection