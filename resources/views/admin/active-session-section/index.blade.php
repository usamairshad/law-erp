@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Active Session Section</h3>
                
                <a href="{{ url('active-session-section/create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Active Session Section</a>
            
        
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($active_sessions) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Session</th>
                    <th>Active Session Section</th>
                    
                </tr>
                </thead>

                <tbody>
                    @php($r = 1)
                @if (count($active_sessions) > 0)
                    @foreach ($active_sessions as $active_session)
                        <tr data-entry-id="{{ $active_session->id }}">
                            <td> {{$r}} </td>
                            <td>{{ $active_session->section->name}}</td>
                            <td>{{ $active_session->activesession->title}}</td>                 
                        </tr>
                        @php($r++)
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection