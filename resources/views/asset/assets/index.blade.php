@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Asset</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
                
                <a href="{{ route('admin.asset.create') }}" class="btn btn-success pull-right">Add New Asset</a>
               

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($assets) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Asset Type</th>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Warranty</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($assets) > 0)
                        @foreach ($assets as $asset)
                            <tr data-entry-id="{{ $asset->id }}">
                                <td>{{ $asset->name }}</td>
                                <td>{{ $asset->asset_type_id }}</td>
                                <td>{{ $asset->value}}</td>
                                <td>{{ $asset->description}}</td>
                                <td>{{ $asset->quantity}}</td>
                                <td>{{ $asset->warranty}}</td>
                                <td>{{ $asset->status}}</td>
                                <td>
                                        <a href="{{ route('admin.asset.show', [$asset->id]) }}"
                                            class="btn btn-xs btn-info">View</a>

                                        <a href="{{ route('admin.asset.edit', [$asset->id]) }}"
                                            class="btn btn-xs btn-info">Edit</a>
                                   

                                        {!! Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['admin.asset.destroy', $asset->id],
                                        ]) !!}
                                        {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger'])
                                        !!}
                                        {!! Form::close() !!}  


                                        @if($asset->status == 'Active')
                                            {!! Form::open(array(
                                                        'style' => 'display: inline-block;',
                                                        'method' => 'PATCH',
                                                        'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                        'route' => ['admin.asset.inactive', $asset->id])) !!}
                                            {!! Form::submit('InActivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                            {!! Form::close() !!}

                                        @elseif($asset->status == 'InActive')
                                            {!! Form::open(array(
                                                    'style' => 'display: inline-block;',
                                                    'method' => 'PATCH',
                                                    'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                                    'route' => ['admin.asset.active', $asset->id])) !!}
                                            {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                            {!! Form::close() !!}
                                    @endif
                                </td> 
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
