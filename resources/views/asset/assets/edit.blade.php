@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Asset</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Asset</h3>
            <a href="{{ route('admin.asset.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::open(['method' => 'PUT','route' => ['admin.asset.update',$assets->id]]) !!}

        {!! Form::token(); !!}
        <div class="box-body">
            @include('asset.assets.edit_field')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
              <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
  
@endsection