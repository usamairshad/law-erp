<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    <input type="text" name="name" value="{{$assets->name}}" class="form-control" required maxlength="30">
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif">
    {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
    <input type="text" name="description" value="{{$assets->description}}" class="form-control" required maxlength="10">
</div>
<div class="form-group col-md-3 @if($errors->has('value')) has-error @endif">
    {!! Form::label('value', 'Value*', ['class' => 'control-label']) !!}
    <input type="number" name="value" value="{{$assets->father_name}}" class="form-control" min="1" required maxlength="100000">
</div>
<div class="form-group col-md-3 @if($errors->has('asset_type_id')) has-error @endif">
    {!! Form::label('asset_type_id ', 'Asset Type*', ['class' => 'control-label']) !!}
    {!! Form::select('asset_type_id', $asset_type->prepend('Select Asset Type Name', ''),old('asset_type'), ['class' => 'form-control','required']) !!}
</div>
<div class="form-group col-md-3 @if($errors->has('warranty')) has-error @endif">
    {!! Form::label('warranty', 'Warranty In Years*', ['class' => 'control-label']) !!}
    <input type="number" name="warranty" value="{{$assets->warranty}}" min="1" class="form-control" required>
</div>
<div class="form-group col-md-3 @if($errors->has('expire_date')) has-error @endif">
    {!! Form::label('expire_date', 'Expire Date*', ['class' => 'control-label']) !!}
    <input type="date" name="expire_date" value="{{$assets->expire_date}}" class="form-control" required maxlength="10">
</div>
<div class="form-group col-md-3 @if($errors->has('quantity')) has-error @endif">
    {!! Form::label('quantity', 'Quantity*', ['class' => 'control-label']) !!}
    <input type="number" name="quantity" value="{{$assets->quantity}}" class="form-control" min="1" required maxlength="100000">
</div>
