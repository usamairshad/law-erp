@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assets Return Policy</h3>


            <a href="" class="btn btn-success pull-right" style ="float:right;" data-toggle="modal" data-target="#newModal">Add Return Policy</a>


        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                    @foreach($records as $record)
                        <tr data-entry-id="">
                            <td>{{$record->name}}</td>
                            <td>{{$record->description}}</td>
                            <td>{{$record->status}}</td>
                            <td>
                                <a data-toggle="modal" data-target="#view" data-detail="{{$record}}" href="#"
                                   class="btn btn-xs btn-info view">View</a>

                                <a href="#"
                                   class="btn btn-xs btn-info edit" data-toggle="modal" data-target="#newModal" data-return-policy="{{$record}}">Edit</a>
                                <form method="post" action="{{url('return-policy',$record->id)}}">
                                    {{ method_field('delete') }}
                                    {!! csrf_field() !!}
                                <button type="submit" href="#"
                                   class="btn btn-xs btn-danger">Delete</button>
                                </form>
                                @if($record->status == 'Active')
                                <a href="{{url('status-inactive',$record->id)}}"
                                   class="btn btn-xs btn-primary">InActive</a>
                                    @elseif($record->status == 'InActive')
                                    <a href="{{url('status-active',$record->id)}}"
                                       class="btn btn-xs btn-primary">Active</a>
                                    @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--add new modal popup--}}
    <div class="modal fade exampleModal" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('return-policy')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Name:</label>
                                    <input type="text"  class="form-control" value="" name="name" id="name">
                                    <input type="hidden"  class="hidden_id" value="" name="id">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Description:</label>
                                    <input type="text" class="form-control" value="" name="description" id="description" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new modal popup end--}}

    {{--view modal--}}
    <div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div style="padding: 10px">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <td class="name"></td>
                            <td class="description"></td>
                            <td class="status"></td>
                        </tr>

                    </table>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    {{--view modal end--}}

@stop

@section('javascript')

    <script>
        $(".edit").on("click",function() {
            var name = $(this).data('return-policy').name;
            var id = $(this).data('return-policy').id;
            var description = $(this).data('return-policy').description;
            $('#name').val(name);
            $('#description').val(description);
            $('.hidden_id').val(id);


        });
        $(".view").on("click",function() {
            var name = $(this).data('detail').name;
            var description = $(this).data('detail').description;
            var status = $(this).data('detail').status;
            $('.name').text(name);
            $('.description').text(description);
            $('.status').text(status);



        });
    </script>
@endsection
