<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    <input type="text" name="name"  value="{{$asset_types->name}}" class="form-control" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif"  style = "float:left;">
    {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
    <input type="text" name="description"  value="{{$asset_types->description}}" class="form-control" required maxlength="50">
</div>