@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assets</h3>

                
                <a href="{{ route('admin.asset_type.create') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Add New Asset Type</a>
               

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($asset_types) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th> Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($asset_types) > 0)
                        @foreach ($asset_types as $asset_type)
                            <tr data-entry-id="{{ $asset_type->id }}">
                                <td>{{ $asset_type->name }}</td>
                                <td>{{ $asset_type->description }}</td>
                                <td>{{ $asset_type->status }}</td>
                                <td style = "float:left;">
                                        <a href="{{ route('admin.asset_type.show', [$asset_type->id]) }}"
                                            class="btn btn-xs btn-info">View</a>

                                        <a href="{{ route('admin.asset_type.edit', [$asset_type->id]) }}"
                                            class="btn btn-xs btn-info">Edit</a>
                                   

                                        {!! Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['admin.asset_type.destroy', $asset_type->id],
                                        ]) !!}
                                        {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger'])
                                        !!}
                                        {!! Form::close() !!}  


                                        @if($asset_type->status == 'Active')
                                            {!! Form::open(array(
                                                        'style' => 'display: inline-block;',
                                                        'method' => 'PATCH',
                                                        'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                        'route' => ['admin.asset_type.inactive', $asset_type->id])) !!}
                                            {!! Form::submit('InActivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                            {!! Form::close() !!}

                                        @elseif($asset_type->status == 'InActive')
                                            {!! Form::open(array(
                                                    'style' => 'display: inline-block;',
                                                    'method' => 'PATCH',
                                                    'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                                    'route' => ['admin.asset_type.active', $asset_type->id])) !!}
                                            {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                            {!! Form::close() !!}
                                    @endif
                                </td> 
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
