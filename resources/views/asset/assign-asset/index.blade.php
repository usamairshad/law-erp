@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;" xmlns="http://www.w3.org/1999/html">
        <h1>Assign Asset</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>

            <a href="" class="btn btn-success pull-right" data-toggle="modal" id="reset" data-target="#newModal">Assign Asset</a>


        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Asset Name</th>
                    <th>Assign To</th>
                    <th>Assign Quantity</th>
                    <th>Return Period</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($assign_assets as $assign_asset)

                    <tr data-entry-id="">
                        <td>{{$assign_asset['asset']->name?? ''}}</td>
                        <td>{{$assign_asset->users->name ?? ''}}</td>
                        <td>{{$assign_asset->quantity ?? ''}}</td>
                        <td>{{$assign_asset->return_period ?? ''}}</td>
                        <td>
                            <a href="#"
                               class="btn btn-xs btn-info edit" data-toggle="modal" data-assign_asset="{{$assign_asset ?? ''}}" data-target="#newModal" data-target="#MymodalPreventHTML" data-toggle="modal" data-backdrop="static" data-keyboard="false">UnAssign</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--add new modal popup--}}
    <div class="modal fade exampleModal" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form" method="post" action="{{route('asset-assign.store')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="something">
                                        Asset:</label>
                              <select name="asset" class="form-control" id="asset" required>
                                  <option selected disabled >--Select--</option>
                                  {!! \App\Models\Asset\Asset::assets() !!}
                              </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                  
                                       <label for="formGroupExampleInput">Roles:</label>
                              
                              <select name="roles" onchange="rolesID(this)" class="form-control" id="asset" required>
                                  <option selected disabled >--Select--</option>
                                  {!! \App\Models\Role::roleList() !!}
                              </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Quantity:</label>
                                    <input type="text" class="form-control" value="" name="quantity" id="quantity" required>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Return Period:</label>
                                    <input type="text" class="form-control" value="" name="return_period" id="return_period" required>
                                    <input type="hidden" id="hide" name="hide_id" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                      <label for="something">
                                        Assign TO User:</label>
                                <select name="assign_to" class="form-control" id="assign_to" required>
                                  <option selected disabled >--Select--</option>
                             
                              </select>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new modal popup end--}}

    {{--view modal--}}
    <div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div style="padding: 10px">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <td class="name"></td>
                            <td class="description"></td>
                            <td class="status"></td>
                        </tr>

                    </table>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    {{--view modal end--}}

@stop

@section('javascript')

    <script>
        $(".edit").on("click",function() {
            var asset_name = $(this).data('assign_asset')['asset'].id;
            var assign_to = $(this).data('assign_asset').assign_to;
            var quantity = $(this).data('assign_asset').quantity;
            var return_period = $(this).data('assign_asset').return_period;
            var hidden = $(this).data('assign_asset').id;
            $('#asset').val(asset_name);
            $('#something').val(assign_to);
            $('#quantity').val(quantity);
            $('#return_period').val(return_period);
            $('#hide').val(hidden);

        });
       $("#reset").on('click', function(){
           $("#form").trigger("reset");
       });


 function rolesID(id){
        var getid = id[id.selectedIndex].value;
        $.ajax({
            url: 'role-related-user',
            type: 'GET',
            data: {
                "role_id": getid
            },
            success: function(data){
                for(i in data){

                  
                    $("#assign_to").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");


                }
            }
        });

    }
    </script>
@endsection
