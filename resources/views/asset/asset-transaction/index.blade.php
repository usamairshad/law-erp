@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assets Transactions</h3>

    <div class="panel panel-default">
        <div class="box-header with-border">

            <a href="" style = "float:right;" class="btn btn-success pull-right" data-toggle="modal" data-target="#newModal" onclick="resetform()">Add Asset Transaction</a>
            <a href="" class="btn btn-primary pull-right remove_quantity" data-toggle="modal" style="margin-right: 5px; float:right;" data-target="#newModal" onclick="resetform()">Remove Quantity</a>


        </div>



        <div class="panel-body table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                    <th>Asset Name</th>
                    <th>Vendor</th>
                    <th>Model</th>
                    <th>Make</th>
                    <th>Quantity</th>
                </tr>
                </thead>

                <tbody>

                    @foreach($records as  $record)
                    <tr>
                        <td>{{$record['asset']['name']}}</td>
                        <td>{{$record->vendor}}</td>
                        <td>{{$record->model}}</td>
                        <td>{{$record->make_asset}}</td>
                        <td>{{$record->quantity}}</td>
{{--                        <td>--}}
                            <form method="post" action="{{route('asset-transaction.destroy',$record->id)}}">
                                {{ method_field('delete') }}
                                {!! csrf_field() !!}
{{--                                <a  class="btn btn-primary update btn-sm approve" data-toggle="modal" data-target="#newModal" data-record="{{$record}}" data-agenda="">Update</a>--}}

{{--                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>--}}
                            </form>

{{--                        </td>--}}
                    </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--add new modal popup--}}
    <div class="modal fade update" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form  method="post" action="{{route('asset-transaction.store')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Asset:</label>
                                    <select name="asset_id" class="form-control">
                                        <option value="" disabled selected>--Select--</option>
                                        {!! App\Models\Asset\Asset::assets() !!}
                                    </select>
                                    <input type="hidden" name="button_value" id="button_value" class="hidden" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Vendor:</label>
                                    <input type="text" class="form-control" value="" name="vendor" id="vendor" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Quantity:</label>
                                    <input type="number" class="form-control" value="" name="quantity" id="quantity" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Make:</label>
                                    <input type="text" class="form-control" value="" name="make_asset" id="make_asset">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Model:</label>
                                    <input type="number" class="form-control"  name="model" id="model" >
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new modal popup end--}}

@stop

@section('javascript')
    <script>
        $(".remove_quantity").on("click",function() {
            var button_value = $('.remove_quantity').text();
            $('#button_value').val(button_value);


        });
        $(".approve").on("click",function() {
            var id = $(this).data('agenda_id');

            $('.agenda_request_id').val(id);


        });
        function resetform(){

            $("#add_new").trigger("reset");
        }

    </script>
@endsection