@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assets</h3>


            <a href="{{ route('admin.asset.create') }}" style = "float:right;" class="btn btn-success pull-right" >Add Asset</a>


        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('delete'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Model</th>
                    <th>Quantity</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($records as $record)
                    <tr data-entry-id="">
                        <td>{{$record->name}}</td>
                        <td>{{$record->description}}</td>
                        <td>{{$record->model}}</td>
                        <td>{{$record->quantity}}</td>
                        <td>{{$record->status}}</td>
                        <td>
                            <form method="post" action="{{route('admin.asset.destroy',$record->id)}}">
                                {{ method_field('delete') }}
                                {!! csrf_field() !!}
                            <button type="submit"
                               class="btn btn-xs btn-danger view">Delete</button>

                            </form>
                            <a href="{{ route('admin.asset.edit',$record->id) }}"
                               class="btn btn-xs btn-info edit" >Edit</a>


                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(".edit").on("click",function() {
            var name = $(this).data('return-policy').name;
            var id = $(this).data('return-policy').id;
            var description = $(this).data('return-policy').description;
            $('#name').val(name);
            $('#description').val(description);
            $('.hidden_id').val(id);


        });
        $(".view").on("click",function() {
            var name = $(this).data('detail').name;
            var description = $(this).data('detail').description;
            var status = $(this).data('detail').status;
            $('.name').text(name);
            $('.description').text(description);
            $('.status').text(status);



        });
    </script>
@endsection
