@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Assets</h3>

{{--            <a href="" class="btn btn-success pull-right">Back</a>--}}
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($message = Session::get('deleted'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        @foreach($records as $record)
        <div class="box-body" style ="margin-top:40px;">
         <div class="container-fluid">
                    <form method="PUT" action="{{route('admin.asset.update',$record->id)}}">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                    <div class="col-md-4">
                        <label>Name:</label>
                        <input type="text" value="{{$record->name}}" name="name" class="form-control">
                    </div>
                        <div class="col-md-4">
                            <label>Value:</label>
                            <input type="number" value="{{$record->value}}" name="value" class="form-control">
                        </div>
                        <div class="col-md-4">
                       <label>Model:</label>
                            <input type="text" name="model" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Asset Type:</label>
                            <select class="form-control" name="asset_type_id">
                                {!! App\Models\Asset\AssetType::assetsType() !!}

                            </select>
                        </div>
                        <div class="col-md-4">
                           
                                      <label>Description:</label>
                            <textarea class="form-control" name="description">{{$record->description}}</textarea>

                        </div>
                    </div>
                        <div class="row">
                            <div class="col-md-4" style="margin-top: 10px">
                                <button id="btn"  class="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>

        @endforeach
    </div>
@stop

@section('javascript')

@endsection