@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Assets</h3>

{{--            <a href="" class="btn btn-success pull-right">Back</a>--}}
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($message = Session::get('deleted'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
        <div class="box-body" style ="margin-top:40px">
                <div class="container-fluid">
                    <form method="post" action="{{route('admin.asset.store')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                    <div class="col-md-4">
                        <label>Name:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                        <div class="col-md-4">
                            <label>Value:</label>
                            <input type="number" name="value" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Model:</label>
                            <input type="text" class="form-control" name="model">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-4">
                            <label>Asset Type:</label>
                            <select class="form-control" name="asset_type_id">
                                <option value="" disabled selected>--Select--</option>

                                {!! App\Models\Asset\AssetType::assetsType() !!}

                            </select>
                        </div>
                        <div class="col-md-4" >
                            <label>Description:</label>
                            <textarea class="form-control" name="description"></textarea>

                        </div>
                    </div>
                        <div class="row">
                            <div class="col-md-4" style="margin-top: 10px">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>



    </div>
@stop

