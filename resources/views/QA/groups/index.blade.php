@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Events Groups </h1>
    </section>
    {{ csrf_field() }}
    <input type="hidden" name="name" value="abc">

@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>

            @can('erp_group_event_create')

                <a href="{{ route('admin.groupss.create') }}" class="btn btn-success pull-right">Add New Event Group</a>

            @endcan

            <a href="{{ url('admin/group/archive/view') }}" class="btn btn-success pull-right">archive Table</a>




        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($groups) > 0 ? 'datatable' : '' }}"
                id="users-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Group</th>
                        <th>Student</th>
                        {{-- <th>Date</th> --}}
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($groups as $key => $group)

                        @if ($group->archive != 1)

                            <tr>

                                <td>{{ ++$key }}</td>
                                <td>{{ $group->category }}</td>
                                <td>

                                    <ul>
                                        @if ($group->groupstudentevent)


                                            @foreach ($group->groupstudentevent as $student)

                                                <li>{{ $student->student->first_name }}</li>

                                            @endforeach
                                        @endif

                                    </ul>


                                </td>
                                {{-- <td></td> --}}
                                <td>


                                    @can('erp_group_event_edit')


                                        <a href="{{ route('admin.groupss.edit', ['id' => $group->id]) }}"
                                            class="btn btn-xs btn-info">Edit</a>

                                    @endcan


                                    @can('erp_group_event_destroy')


                                        <form method="POST" action="{{ url("admin/groupss/$group->id") }}"
                                            accept-charset="UTF-8" style="display: inline-block;"
                                            onsubmit="return confirm('Are you sure?');">

                                            {{ method_field('delete') }}
                                            {{ csrf_field() }}

                                            <input class="btn btn-xs btn-danger" type="submit" value="Delete">
                                        </form>
                                    @endcan


                                    <a href="{{ url("admin/group/archive/$group->id") }}"
                                        class="btn btn-xs btn-primary">archive</a>



                                    <a href="{{ url("admin/group/send/notification/$group->id") }}"
                                        onclick="return confirm('Are you sure you want to send notification')"
                                        class="btn btn-xs btn-primary"> Send Notification </a>


                                    <a href="{{ url("admin/group/document/download/$group->id") }}"
                                        class="btn btn-xs btn-primary"> Download Document </a>

                                </td>


                            </tr>

                        @endif


                    @endforeach





                </tbody>




            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>





@endsection
