@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Groups</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Group</h3>
            <a href="{{ route('admin.groupss.index') }}" class="btn btn-success pull-right">Back</a>


            <form action="{{ route('admin.groupss.store') }}" method="POST" enctype="multipart/form-data">

                <input class="pull-right" name="files[]" type="file" multiple>

           
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">

            <div class="container-fuild">

                <div class="row">

                   

                        {{ csrf_field() }}

                        <div class="col-md-4">

                            <label for="">Group Name</label>
                            <br>
                            <input style="width: 100%" name="category" type="text">

                        </div>

                        <div class="col-md-4">

                            <label for="">Branch</label>
                            <br>
                            <select style="width: 100%" class="js-example-basic-multiple" name="" id="branch">

                                <option value="">Select Branch</option>

                                @foreach ($branches as $branch)

                                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>

                                @endforeach


                            </select>


                        </div>
                        <div class="col-md-4">

                            <label for="">Board</label>
                            <br>
                            <select style="width: 100%" class="js-example-basic-multiple" name="" id="boards">

                                <option value=""></option>

                            </select>


                        </div>


                        <div class="col-md-4">

                            <label for="">Program</label>
                            <br>
                            <select style="width: 100%" class="js-example-basic-multiple" name="" id="programs">

                                <option value=""></option>

                            </select>


                        </div>

                        <div class="col-md-4">

                            <label for="">Class</label>
                            <br>
                            <select style="width: 100%" class="js-example-basic-multiple" name="" id="classes">

                                <option value=""></option>

                            </select>

                        </div>

                        <div class="col-md-4">

                            <label for="">Select Students</label>
                            <br>
                            <select style="width: 100%" multiple="multiple" class="js-example-basic-multiple"
                                name="student_ids[]" id="student">

                                <option value=""></option>

                            </select>

                        </div>


                        <div class="col-md-12">

                            <button class="btn btn-default" type="submit">Submit</button>

                        </div>
                    </form>

                </div>




            </div>





        </div>
        <!-- /.box-body -->


    </div>


    <!--------------------------------hidden input fields-------------------->
    <input name="branch_id" id="hidden_branch_id" type="hidden">
    <input name="board_id" id="hidden_board_id" type="hidden">
    <input name="program_id" id="hidden_program_id" type="hidden">
    <input name="class_id" id="hidden_class_id" type="hidden">



@stop

@section('javascript')
    <!-- Moment Javascript -->
    <script src="{{ url('adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/js/hrm/work_shifts/create_modify.js') }}" type="text/javascript"></script>


    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });


        $("#branch").on("change", function() {

            $branch_id = $(this).find(":selected").val();
            $("#hidden_branch_id").val($branch_id);
            boards = loadBorad($branch_id);

        });

        function loadBorad($branch_id) {
            $.ajax({
                url: `{{ url('admin/getBoard/') }}/${$branch_id}`,

                success: function(response) {
                    $html = '<option value="null">Select board</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.board_id}>${res.board.name}</option>`;

                    });

                    $("#boards").data("branch-id", $branch_id);

                    $("#boards").html($html);
                }
            });
        }



        ////////////////on board change get program/////////////// 
        $("#boards").on("change", function() {

            $board_id = $(this).find(":selected").val();
            $("#hidden_board_id").val($board_id);
            $branch_id = $(this).data('branch-id');

            loadProgram($branch_id, $board_id);
        });

        function loadProgram($branch, $board) {
            $.ajax({
                url: `{{ url('admin/getProgram/') }}/${$branch}/${$board}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select program</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.program_id}>${res.program.name}</option>`;

                    });

                    $("#programs").data('branch-id', $branch);
                    $("#programs").data('board-id', $board);
                    $("#programs").html($html);
                }
            });
        }
        ////////////////on program change get class/////////////// 
        $("#programs").on("change", function() {

            $board_id = $(this).data("board-id");
            $branch_id = $(this).data('branch-id');
            $program_id = $(this).find(':selected').val();

            $("#hidden_program_id").val($program_id);

            console.log($board_id, $branch_id, $program_id);

            loadClasses($branch_id, $board_id, $program_id);
        });

        function loadClasses($branch, $board, $program) {
            $.ajax({
                url: `{{ url('admin/getClasses/') }}/${$branch}/${$board}/${$program}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select class</option>';

                    response.forEach((res) => {

                        $html +=
                            `<option data-active-s-id=${res.id}  value=${res.get_class.id}>${res.get_class.name}</option>`;

                    });

                    $("#classes").html($html);
                }
            });
        }

        $("#classes").on("change", function() {

            $class_id = $(this).find(":selected").val();

            $branch_id = $("#hidden_branch_id").val();
            $board_id = $("#hidden_board_id").val();
            $program_id = $("#hidden_program_id").val();
            //$program_id = $("#hidden_program_id").val();


            $.ajax({
                url: `{{ url('admin/getStudents/') }}/${$branch_id}/${$board_id}/${$program_id}/${$class_id}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select students</option>';

                    response.forEach((res) => {

                        $html +=
                            `<option value=${res.id}>${res.first_name}</option>`;
                    });

                    $("#student").html($html);
                }
            });




        });

    </script>





@endsection
