@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>


<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">List</h3>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            <table class="table table-bordered table-striped {{ count($agendas) > 0 ? 'datatable' : '' }} ">
                <thead>
                <tr>
                    <th>Created By</th>
                    <th>Reg No</th>
                    <th>Employee Role</th>
                    <th>Branch Name</th>
                    <th>City</th>
                    <th>Title</th>
                    <th>Subject</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th>Reason</th>             
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                    @if (count($agendas) > 0)
                    @foreach($agendas as $agenda)
                    <tr data-entry-id="{{ $agenda->id }}">
                        <td>{{ \App\Helpers\Helper::userIdToName($agenda->created_by) }}</td>
                        <td>{{ \App\Helpers\Helper::userIdToStaffRegNo($agenda->created_by) }}</td>
                        <td>{{ \App\Helpers\Helper::userIdToRole($agenda->created_by) }}</td>
                        <td>{{ \App\Helpers\Helper::userIdToBranchName($agenda->created_by) }}</td>
                        <td>{{ \App\Helpers\Helper::userIdToCityName($agenda->created_by) }}</td>
                        <td>{{$agenda->title}}</td>
                        <td>{{$agenda->subject}}</td>
                        <td>{{$agenda->status}}</td>
                        <td>{{$agenda->description}}</td>
                      
                        <td>{{$agenda->reason}}</td>
                      
                        <td>
                            @if($agenda->status == 'pending')
                            <a href="#" class="btn btn-xs btn-info approve" data-toggle="modal" data-target="#approve" data-agenda_id="{{$agenda->id}}" data-status="{{$agenda->status}}" data-return-policy="">Approve</a>

                            <a href="#" class="btn btn-xs btn-danger reject" data-toggle="modal" data-target="#reject" data-agenda_id="{{$agenda->id}}" data-status="{{$agenda->status}}" data-return-policy="">Reject</a>

                                @elseif($agenda->status == 'scheduled')
                                <a href="#" class="btn btn-xs btn-info approve" data-toggle="modal" data-target="#approve" data-agenda_id="{{$agenda->id}}" data-status="{{$agenda->status}}" data-return-policy="">ReSchedule</a>
                            @elseif($agenda->status == 'rescheduled')
                                <a href="#" class="btn btn-xs btn-info approve" data-toggle="modal" data-target="#approve" data-agenda_id="{{$agenda->id}}" data-status="{{$agenda->status}}" data-return-policy="">ReSchedule</a>

                            @endif

                            <form method="post" action="{{url('agenda',$agenda->id)}}">
                                {{ method_field('delete') }}
                                {!! csrf_field() !!}
                                <button type="submit"
                                        class="btn btn-xs btn-danger">Delete</button>
                            </form>
                            @if($agenda->status == 'scheduled' || $agenda->status == 'rescheduled')
                            <button type="button" class="btn btn-primary btn-xs " data-toggle="modal" data-target="#schedule1" data-target="#MymodalPreventHTML" data-toggle="modal" data-backdrop="static" data-keyboard="false"  onclick="schedule1({{    $agenda->id}})">Schedule Detail</button>
                            @endif

                        </td>
                    </tr>


                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>

<script type="text/javascript">
      
        $(".approve").on("click",function() {
         
            var id = $(this).data('agenda_id');
            var status = $(this).data('status');

            $('.agenda_request_id').val(id);
            $('#status').val(status);


        });
        

        function schedule1(id){
        $.ajax({
            url: 'schedule-date-time/'+id,
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data){
                for(i in data) {
                    $(".time").html(data[i]['time']);
                    $(".date").html(data[i]['date']);
                    $(".location").html(data[i]['location']);
                    $(".description").html(data[i]['description']);
                    if (data[i]['in_campus']==1){
                        $(".campus").html("Yes");
                    }
                    else{
                        $(".campus").html("No");
                    }
                }
            }

        })
      }
    </script>
    
@endsection






    {{--add new approve popup--}}
    <div class="modal fade " id="approve" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('agenda-approve')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="hidden_id" class="agenda_request_id" name="agenda_request_id"  value="">
                    <input type="hidden" id="status" class="agenda_request_id" name="status"  value="">
                    <div class="modal-body">
                        <div class="row">
                        <div class="col-md-12">
                                <div class="form-group" style="margin-top: 10px;">
                                    <label for="formGroupExampleInput">In Campus:</label><br>
                                    <input type="radio" value="1" name="radio" />Yes
                                    <input type="radio" value="0" name="radio" />No
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Time:</label>
                                    <input type="time" class="form-control" value="" name="time" id="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Date:</label>
                                    <input type="date" class="form-control" value="" name="date" id="" >
                                </div>
                            </div>
                         
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="formGroupExampleInput">Location:</label>

                                <input type="search" name="location" class="form-control">
                            </div>
                            <div class="col-md-12">
                                <label for="formGroupExampleInput">Description:</label>

                                <textarea name="description" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Approve</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new approve popup end--}}




    {{--add new reject popup--}}
    <div class="modal fade " id="reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('agenda-reject')}}/{{$agenda->id}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="hidden_id" class="agenda_request_id" name="agenda_request_id"  value="">
                    <input type="hidden" id="status" class="agenda_request_id" name="status"  value="">
                    <div class="modal-body">
                       
                        <div class="row">
                          
                            <div class="col-md-10">
                                <label for="formGroupExampleInput">Reason:</label>

                                <textarea name="reason" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Reject</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new approve popup end--}}

    {{--Schedule detail popup--}}
    <div class="modal fade" id="schedule1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                </div>
                <div class="panel-body table-responsive">

                    <table class="table table-bordered table-striped" id="tablebody">
                        <tr>
                            <th>Schedule Time</th>
                            <th>Schedule Date</th>

                        </tr>
                        <tr>
                            <td class="time"></td>
                            <td class="date"></td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <th>Description</th>
                        </tr>
                        <tr>
                            <td class="location"></td>
                            <td class="description"></td>
                        </tr>
                        <tr>
                            <th>In Campus</th>
                        </tr>
                        <tr>
                            <td class="campus"></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary schdule" id="removeDiv" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    {{--Schedule detail popup end--}}
@stop


    
