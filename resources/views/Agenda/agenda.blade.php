@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Agenda</h3>

            <a href="" class="btn btn-success pull-right" data-toggle="modal" style = "float:right;" data-target=".exampleModal" onclick="resetform()">Add New Agenda</a>

        </div>



        <div class="panel-body table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <table class="table table-bordered table-striped {{ count($agendas) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Created By </th>
                    <th>Branch Name</th>
                    <th>Title</th>
                    <th>Subject</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Reject Reason</th>
                    <th>Action</th>
                  
                </tr>
                </thead>

                <tbody>
                    @if (count($agendas) > 0)    
                    @foreach($agendas as $agenda)
                    <tr>
                        <td>{{ \App\Helpers\Helper::userIdToName($agenda->created_by) }}</td>
                        <td>{{ \App\Helpers\Helper::userIdToBranchName($agenda->created_by) }}</td>
                        <td>{{$agenda->title}}</td>
                        <td>{{$agenda->subject}}</td>
                        <td>{{$agenda->description}}</td>
                        <td>{{$agenda->status}}</td>
                        <td>{{$agenda->reason}}</td> 
                            <td>
                                <form method="post" action="{{url('agenda',$agenda->id)}}">
                                    {{ method_field('delete') }}
                                    {!! csrf_field() !!}
                                    @if($agenda->status == 'pending')
                                    <a  class="btn btn-primary update btn-sm approve" data-toggle="modal" data-target="#newModal" data-agenda="{{$agenda}}">Edit</a>
                                    
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    @endif
                                    @if($agenda->status == 'scheduled' || $agenda->status == 'rescheduled')
                                    <button type="button" class="btn btn-primary btn-sm " data-toggle="modal" data-target="#schedule" data-target="#MymodalPreventHTML" data-toggle="modal" data-backdrop="static" data-keyboard="false"  onclick="schedule({{$agenda->id}})">Schedule Detail</button>
                                    @endif
                                    @if($agenda->status == 'rejected')
                                     <p style="color:red"><b> Rejected </b> </p>
                                    @endif
                                </form>

                            </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    {{--add new modal popup--}}
    <div class="modal fade exampleModal" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"><b>Add New Agenda</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('agenda')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="hidden_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Title:</label>
                                    <input type="text" class="form-control" value="" name="title" id="title">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Subject:</label>
                                    <input type="text" class="form-control" value="" name="subject" id="subject" >
                                    <input type="hidden" class="form-control" value="{{\Illuminate\Support\Facades\Auth::id()}}" name="auth_id" id="subject" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Description:</label>
                                    <textarea  class="form-control"  name="description" id="description" ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new modal popup end--}}

    {{--add new approve popup--}}
    <div class="modal fade " id="approve" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('agenda-approve')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" class="agenda_request_id" name="agenda_request_id"  value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Time* :</label>
                                    <input type="time" class="form-control" value="" name="time" id="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Date* :</label>
                                    <input type="date" class="form-control" value="" name="date" id="" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="margin-top: 10px">
                                    <label for="formGroupExampleInput">In Campus* :</label><br>
                                    <input type="radio" value="1" name="radio" />Yes
                                    <input type="radio" value="0" name="radio" />No
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="formGroupExampleInput">Location:</label>

                                <input type="search" name="location" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="formGroupExampleInput">Description* :</label>

                                <textarea name="description" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                        <button type="submit" class="btn btn-primary">Approve</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--add new approve popup end--}}

    {{--Schedule detail popup--}}
    <div class="modal fade" id="schedule" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                </div>
                <div class="panel-body table-responsive">

                    <table class="table table-bordered table-striped" id="tablebody">
                        <tr>
                            <th>Schedule Time</th>
                            <th>Schedule Date</th>

                        </tr>
                        <tr>
                            <td class="time"></td>
                            <td class="date"></td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <th>Description</th>
                        </tr>
                        <tr>
                            <td class="location"></td>
                            <td class="description"></td>
                        </tr>
                        <tr>
                            <th>In Campus</th>
                        </tr>
                        <tr>
                            <td class="campus"></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary schdule" id="removeDiv" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    {{--Schedule detail popup end--}}
@stop

@section('javascript')
    <script>
        $(".update").on("click",function() {
            var id = $(this).data('agenda').id;
            var title = $(this).data('agenda').title;
            var description = $(this).data('agenda').description;
            var subject = $(this).data('agenda').subject;

            $('#title').val(title);
            $('#description').val(description);
            $('#subject').val(subject);
            $('#hidden_id').val(id);

        });
        $(".approve").on("click",function() {
            var id = $(this).data('agenda_id');

            $('.agenda_request_id').val(id);


        });
         function resetform(){

            $("#add_new").trigger("reset");
        }
      function schedule(id){
        $.ajax({
            url: 'schedule-date-time/'+id,
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data){
                for(i in data) {
                    $(".time").html(data[i]['time']);
                    $(".date").html(data[i]['date']);
                    $(".location").html(data[i]['location']);
                    $(".description").html(data[i]['description']);
                    if (data[i]['in_campus']==1){
                        $(".campus").html("Yes");
                    }
                    else{
                        $(".campus").html("No");
                    }
                }
            }

        })
      }
      // $(".schdule").on('click', function(){
      //     $("#tablebody").find("tr:gt(0)").empty();
      // });
    </script>
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection