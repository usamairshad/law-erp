<div class="box-body" style = "margin-top:40px;">
                <div class="col-lg-4 form-group"  style = "float:left;">
                    <label>Select Company</label>
                    <select name="select_company" id="select_company" class="form-control select_company">
                        <option>Select Company</option>
                          @foreach($data as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>                                  
                          @endforeach    
                    </select>
                </div>
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Branch</label>
                    <select name="select_branch" id="select_branch" class="form-control select_branch">
                        

                          
                    </select>
                </div>



                <!-- /.box-body -->


                
        
            </div>



    <div class="panel-body pad table-responsive col-md-12">
            <table class="table table-bordered table-striped datatable" >
                <thead>
                    <tr>
                        

                        <th>Staff </th>
                        <th>Branch</th>
                        <th>Company</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Month</th>
                        <th>Year</th>
                    </tr>
                </thead>

                <tbody id="company-wise">
                    
                   
                </tbody>
                <tbody id="branch-wise">
                    
                   
                </tbody>
            </table>
        </div>







@section('javascript')

<script type = "text/javascript">
//Find Branches based upon Company
$(document).on('change','.select_company',function () 
{

            var company_id=$('#select_company').val();                  
            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_company_wise_eobiepf')!!}',
                data:{'company_id':company_id},
                dataType:'json',//return data will be json
                success:function(data){
                                    
                    console.log(data);



                    $html = '<option value="null">select course</option>';
                    data.branches.forEach((dat) => {
                        $html += `<option value=${dat.id}>${dat.name}</option>`;
                    });

                    $("#select_branch").html($html);

                    $html1='';

                    data.eobi_epf_data.forEach((dat)=>{
                        $html1+=`
                        <tr>
                            <td>
                                <input type="text" value="${dat.staff_id}">
                            </td>
                            <td>
                                <input type="text" value="${dat.branch_id}">
                            </td>
                            <td>
                                <input type="text" value="${dat.company_id}">
                            </td>
                            <td>
                                <input type="text" value="${dat.amount}">
                            </td>
                            <td>
                                <input type="text" value="${dat.status}">
                            </td>
                            <td>
                                <input type="text" value="${dat.month}">
                            </td>
                            <td>
                                <input type="text" value="${dat.year}">
                            </td>


                        </tr>
                        `
                    });

                    $("#company-wise").html($html1);


                    
                },
                error:function(){
                }
            });

});
//Company



$(document).on('change','.select_branch',function () 
{



            var company_id=$('#select_company').val();
            var branch_id=$('#select_branch').val(); 
            
            $('#company-wise').hide();

            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_company_branch_wise_eobiepf')!!}',
                data:{'company_id':company_id,'branch_id':branch_id},
                dataType:'json',//return data will be json
                success:function(data){
                                    
                    console.log(data);          



                    $html1='';

                    data.forEach((dat)=>{
                        $html1+=`
                        <tr>
                            <td>
                                <input type="text" value="${dat.staff_id}">
                            </td>
                            <td>
                                <input type="text" value="${dat.branch_id}">
                            </td>
                            <td>
                                <input type="text" value="${dat.company_id}">
                            </td>
                            <td>
                                <input type="text" value="${dat.amount}">
                            </td>
                            <td>
                                <input type="text" value="${dat.status}">
                            </td>
                            <td>
                                <input type="text" value="${dat.month}">
                            </td>
                            <td>
                                <input type="text" value="${dat.year}">
                            </td>


                        </tr>
                        `
                    });

                    $("#branch-wise").html($html1);


                    
                },
                error:function(){
                }
            });

});
//Company





$(document).on('change','.data_id',function () 
{
    var rowId = $(this).attr('data-row-id');
    var value = $('#staff-'+rowId).val();    
    alert(value);
});

$(document).on('change','.leaves',function () 
{
    var rowId = $(this).attr('data-row-id');
    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    


    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);

    var gross_before_adj=salary-deduction;


    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous;
 

    $('#leave_deduction-'+rowId).val(deduction);
    $('#gross_before_adj-'+rowId).val(gross_before_adj);

    $('#net_salary-'+rowId).val(net_salary);


});


$(document).on('change','.miscellaneous',function () 
{
    var rowId = $(this).attr('data-row-id');

    

    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var loan_amount = $('#loan-'+rowId).val();
 

    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);
    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous;
     


    $('#leave_deduction-'+rowId).val(deduction);
    $('#net_salary-'+rowId).val(net_salary);


});




$(document).on('change','.loan',function () 
{
    var rowId = $(this).attr('data-row-id');

 

    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var loan_amount = $('#loan-'+rowId).val(); 

    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);
    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous - loan_amount;
 


    $('#leave_deduction-'+rowId).val(deduction);
    $('#net_salary-'+rowId).val(net_salary);


});










</script>



@endsection

