@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Student Detail Report</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title"></h3>

{{--            <a href="" class="btn btn-success pull-right" data-toggle="modal" data-target=".exampleModal" onclick="resetform()">Add New Agenda</a>--}}

        </div>



        <div class="panel-body table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
                <div class="box-body">
                    <div class="container-fluid">
                        <form method="post" action="{{url('store-active-session-section')}}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Branch:</label>
                                  <select class="form-control" name="branch" onchange="branchID(this)">
                                      <option value="" disabled selected>--Select--</option>
                                      {!!App\Models\Admin\Branches::branchList() !!}
                                      <option>Board</option>
                                  </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Board:</label>
                                    <select class="form-control" id="board" name="board" onchange="boardID(this)">
                                        <option value="" disabled selected>--Select--</option>

                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Program:</label>
                                    <select class="form-control" id="program" name="program" onchange="programID(this)">
                                        <option value="" disabled selected>--Select--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-4">
                                    <label>Class:</label>
                                    <select class="form-control" name="class" id="class" onchange="classID(this)">
                                        <option value="" disabled selected>--Select--</option>
                                    </select>
                                </div>
                                <div class="col-md-4" >
                                    <label>Section:</label>
                                    <select class="form-control" name="section" id="section">
                                        <option value="" disabled selected>--Select--</option>
                                    </select>

                                </div>
                                <div class="col-md-4" >
                                    <label>Student:</label>
                                    <select class="form-control" name="student">
                                        <option value="" disabled selected>--Select--</option>
                                    </select>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4" style="margin-top: 10px">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <button type="button"  class="form-control btn btn-success" id="Add_payees" onclick="Append_row()">Add Row <i class="fa fa-plus"></i> </button>
                                    </div>
                                </div>
                            </div>
                            <div class="append_row">

                            </div>
                        </form>
                    </div>

                </div>
        </div>
    </div>

@stop
<script>
    function Append_row(id) {

        $('.append_row').append(
            '<div class="row main_div" id="remove_div">'+
            '<div class="col-md-3">'+
            '<div class="form-group" id="payees_append">'+
            '<select class="form-control" name="student_group" id="student_group">'+
            '<option value="" disabled="" selected="">--Select--</option>'+
            '</select>'+
            '</div>'+
            '</div>'+
            '<div class="col-md-3">'+
            '<div class="form-group" id="amount_append">'+

            '<select class="form-control" name="student_perimeter" id="student_perimeter">'+
            '<option value="" disabled="" selected="">--Select--</option>'+
            '</select>'+
            '</div>'+

            '</div>'+
            '<div class="col-md-3">'+
            '<div class="form-group" id="amount_append">'+

            '<input type="text" name="amount"  placeholder="amount" class="form-control" id="recipient-name" required>'+
            '</div>'+

            '</div>'+
            '<div class="col-md-3">'+
            '<button  class="btn btn-danger btn_delete removeButton" onclick="remove_div()" type="button">Delete</button>'+
            '</div>'+

            '</div>'


        );

    }
    function remove_div(){
        $(document).on('click','.removeButton',function() {
            $(this).closest("div.row").remove();
        });

    }
    function branchID(id){
        var getid = id[id.selectedIndex].value;
        $.ajax({
            url: 'student-report-branch-id',
            type: 'GET',
            data: {
                "branch_id": getid
            },
            success: function(data){
                for(i in data){

                    $("#board").append("<option value='"+data[i]['board'].id+"'>"+data[i]['board'].name+"</option>");


                }
            }
        });

    }
    function boardID(id){
        var getid = id[id.selectedIndex].value;
        $.ajax({
            url: 'student-report-board-id',
            type: 'GET',
            data: {
                "board_id": getid
            },
            success: function(data){
                for(i in data){

                    // console.log(data);
                    $("#program").append("<option value='"+data[i]['program'].id+"'>"+data[i]['program'].name+"</option>");


                }
            }
        });

    }
    function programID(id){
        var getid = id[id.selectedIndex].value;
        $.ajax({
            url: 'student-report-program-id',
            type: 'GET',
            data: {
                "program_id": getid
            },
            success: function(data){
                for(i in data){

                    // console.log(data);
                    $("#class").append("<option value='"+data[i]['classes'].id+"'>"+data[i]['classes'].name+"</option>");


                }
            }
        });

    }
    function classID(id){
        var getid = id[id.selectedIndex].value;
        $.ajax({
            url: 'student-report-class-id',
            type: 'GET',
            data: {
                "class_id": getid
            },
            success: function(data){
                for(i in data){

                    $("#section").append("<option value='"+data[0].id+"'>"+data[0].name+"</option>");


                }
            }
        });

    }
</script>