@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Payment Sheet List</h3>
           
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped" id="entries_table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Sheet Code</th>
                    <th>Sheet Type</th>
                    <th>Created At</th>
                    <th>Prepared BY</th>
                    <th>Accountant Approval</th>
                    <th>Deputy GM Approval</th>
                    <th>GM Approval</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
@foreach($Entries as $ent)
<tr>
    <td>{{$ent->sheet_id}}</td>
    <td>{{$ent->sheet_code}}</td>
    <td>{{$ent->sheet_type}}</td>
    <td>{{$ent->created_at}}</td>
    <td>{{$ent->prepared_by}}</td>
    <td>{{$ent->approved_by_accountant}}</td>
    <td>{{$ent->approval_by_dgm}}</td>
    <td>{{$ent->approval_by_gm}}</td>
       <td style = "float:left;">

       @if($ent->sheet_type=='Rent Payment Sheet')
                                        <a href="{{ route('rent-sheet-details', [$ent->sheet_id]) }}"
                                            class="btn btn-xs btn-info">View</a>
                                            @else
                                            <a href="{{ route('payment-sheet-details', [$ent->sheet_id]) }}"
                                            class="btn btn-xs btn-info">View</a>
                                            @endif

                                        <a href="{{ route('payment-sheet-edit', [$ent->sheet_id]) }}"
                                            class="btn btn-xs btn-info">Edit</a>
                                            @if($ent->sheet_type=='Bank Payment Sheet')
                                            <a href="{{ route('payment-sheet-print', [$ent->sheet_id]) }}"
                                            class="btn btn-xs btn-info">Print</a>
                                            @elseif($ent->sheet_type=='Cash Payment Sheet')
                                            <a href="{{ route('cash-payment-sheet-print', [$ent->sheet_id]) }}"
                                            class="btn btn-xs btn-info">Print</a>
                                            @else
                                            <a href="{{ route('rent-sheet-print', [$ent->sheet_id]) }}"
                                            class="btn btn-xs btn-info">Print</a>
                                            @endif
                                            </td>
                                          
</tr>
@endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
  
@endsection