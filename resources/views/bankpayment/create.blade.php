@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')


@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Bank Payment Sheet</h3>



            <a href="{{ route('bank-payment') }}" style = "float:right;margin-bottom:20px;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['bank-payment-sheet-store'], 'id' => 'validation-form']) !!}
            <div class="box-body" style = "margin-top:40px;">
            <div class="col-xs-3 col-md-2 form-group" style = "float:left;">
                        <label>Select Company</label>
                        <select class="form-control select2" name="company_id">
                            {!! App\Models\Admin\Company::CompanyList() !!}
                        </select>
                    </div>
                    
                    
            <div class="col-xs-3 col-md-2 form-group" style = "float:left;">
                    <label>Select City</label>
                    <select  name='city_id' class="form-control input-sm select2" id="city_id">
                        <option value="">---Select---</option>
                        {!! \App\Models\Admin\City::CityList() !!}
                    </select>
               


                    </div>
                    <div class="col-xs-3 col-md-3 form-group" style="margin-bottom: 0px !important;float:left;">
                            {!! Form::label('narration', 'Bank A/C*', ['class' => 'control-label']) !!}
                            {!! Form::select('bank_ledger_id', array(), null, ['id' => 'entry_item-ledger_id-1', 'style' => 'width: 100%;', 'class' => 'form-control base-data-ajax']) !!}
                        </div>
               
                @include('bankpayment.fields')
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
            </div>
        {!! Form::close() !!}
        @include('bankpayment.entries_template')
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/admin/entries/voucher/bank_voucher/bank_payment_sheet/create_modify.js') }}" type="text/javascript"></script>



<script type = "text/javascript">
var total_sum = 0;



function foo(name,id)
{


    alert(name);
    alert(id);
    
}




function invoice_total(name,id) 
{
    // body...

    //variables

    var amount_row;
    var amount_row_id;
    
    var invoice_total_row;
    var invoice_total_id;
    var invoice_total_amount;

    var advance_row;
    var advance_id;
    var advance_amount;

    var pra_row;
    var pra_id;
    var pra_amount;

    var amount;
    var dr_total=0;




    //current input field
    invoice_total_id=name+id;


    //names
    amount_row= "#entry_item-dr_amount-";
    pra_row="#pra-";
    advance_row="#advance-";

    //row ids
    amount_row_id=amount_row+id;
    pra_id=pra_row+id;
    advance_id= advance_row+id;

    
    //amounts
    invoice_total_amount=$(invoice_total_id).val();
    advance_amount=$(advance_id).val();
    pra_amount=$(pra_id).val();



    //calculations
    amount=invoice_total_amount - advance_amount - pra_amount;


    //Assignment of Values
    

    $(amount_row_id).val(amount);




    



    $('#dr_total').val(amount);
}

function advance(name,id) 
{
    // body...

    //variables
    var amount_row;
    var amount_row_id;
    
    var invoice_total_row;
    var invoice_total_id;
    var invoice_total_amount;

    var advance_row;
    var advance_id;
    var advance_amount;

    var pra_row;
    var pra_id;
    var pra_amount;

    var amount;
    var dr_total;

    //current input field
    advance_id=name+id;

    //names
    amount_row= "#entry_item-dr_amount-";
    invoice_total_row="#invoice_total-"
    pra_row="#pra-";

    //row ids
    amount_row_id=amount_row+id;
    invoice_total_id= invoice_total_row+id;
    pra_id=pra_row+id;
  
    //amounts
    invoice_total_amount=$(invoice_total_id).val();
    advance_amount=$(advance_id).val();
    pra_amount=$(pra_id).val();

    //calculations
    amount=invoice_total_amount - advance_amount - pra_amount;



    //Assignments
    $(amount_row_id).val(amount);
    $('#dr_total').val(amount);
}



function pra(name,id) 
{
    // body...

    //variables
    var amount_row;
    var amount_row_id;

    var invoice_total_row;
    var invoice_total_id;
    var invoice_total_amount;

    var advance_row;
    var advance_id;
    var advance_amount;

    var pra_id;
    var pra_amount;

    var amount;
    var dr_total;

    //current input field
    pra_id=name+id;

    //names
    amount_row= "#entry_item-dr_amount-";
    invoice_total_row="#invoice_total-";
    advance_row="#advance-";


    //row ids
    amount_row_id=amount_row+id;
    invoice_total_id= invoice_total_row+id;
    advance_id= advance_row+id;
  
    //amounts
    invoice_total_amount=$(invoice_total_id).val();
    advance_amount=$(advance_id).val();
    pra_amount=$(pra_id).val();



    //calculations
    amount=invoice_total_amount - advance_amount - pra_amount;



    //Assignments
    $(amount_row_id).val(amount);
    $('#dr_total').val(amount);
}










function EachRow(id)
{
    var invoice_total_amount = 0;
    var advance_amount = 0;
    var tax_amount = 0;
    var global = id;
    var name="#entry_item-dr_amount-";
    var name="#entry_item-dr_amount-";
    var amount_id = name + global;
    var inv_total_id= "#invoice_total-"+global;
    var advance_id= "#advance-"+global;
    var inv_amount = $(inv_total_id).val();









    var name="#pay-";
    var payee_type="#pay_filer-";
    var payee_category="#pay_category-";
    var tax_law_id="#law-";
    var law_rate="#law_rate-";
    var pra="#pra-";
    
     pay_id = name + global;
     var payee_type_id=payee_type+global;
     var payee_category_id=payee_category+global;
     var law_id=tax_law_id+global;
     var law_rate_id=law_rate+global;

    var pra_name = pra+global;

    var payee_type_name=$(payee_type_id).val();
    var payee_category_name=$(payee_category_id).val();
    var law_name=$(law_id).val();
    $(amount_id).val(inv_amount);
     var advance_amount = $(advance_id).val();
  
   


    $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_tax_precentage')!!}',
                data:{'payee_type_name':payee_type_name,'payee_category_name':payee_category_name,'law_name':law_name},
                dataType:'json',//return data will be json
                success:function(data){
                   
                    if(data.percentage != 0)
                    {
                        var per = parseInt(data.percentage);
                        $(law_rate_id).val(per);
                        var total = parseInt(inv_amount);
                        var value  = total/100;
                        value = value * per;
                        total = total-advance_amount-value;
                        total =total-parseInt($(pra_name).val());
                      //  var diff = parseInt(inv_amount)-parseInt(advance_amount);

                        $(amount_id).val(total);
                      

                        


                    }
                    else
                    {

                    }
                    
                   

                },
                error:function(){
                }
            });

    


    


            
            









   
    // $(amount_id).val(diff);
}










function getRecord(id)
{
  
    var global = id;
    var name="#pay-";
    var payee_type="#pay_filer-";
    var payee_category="#pay_category-"
    var pay_id = name + global;
    var payee_type_id=payee_type+global;
    var payee_category_id=payee_category+global;
    var inv_total_id= "#invoice_total-"+global;
    


            
            var payee_id=$(pay_id).val();
            var invoice = $(inv_total_id).val();
            
            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_vendor_id')!!}',
                data:{'pay_id':payee_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);


                    
                    var payee_name=data.type;
                    var payee_category_name=data.category;

                   

                    $(payee_type_id).val(payee_name);
                    $(payee_category_id).val(payee_category_name);



                   // alert(payee_name);
                   

                },
                error:function(){
                }
            });

}






// function calculate($id)
// {
//  var invoice_total_amount = 0;
//  var advance_amount = 0;
//  var tax_amount = 0;
//  var global = $("#entry_item-global_counter").val();
//      var name="#entry_item-dr_amount-";
//     var id = name + global;

//      var namei= "#invoice_total-"+global;
//      alert(namei);
// }

//     var name="#entry_item-dr_amount-";
//     var id = name + global;

//     var namei= "#invoice_total-"+global;
//     alert(namei);

// $(namei).click(function(){

//     alert(id);
//     invoice_total_amount=this.value;

    
// });

// $(".advance").on("change",function(){

//     advance_amount=this.value;
    



// });
// $(".tax_rate").on("change",function(){

//     tax_amount=this.value;

    
// });




// $('.amount').click(function(){
   

// alert(global);



// var total= invoice_total_amount+advance_amount;


// $(id).val(total);

// });    



















// $(document).on('change','#pay_loan',function() {
//         // body...

//         var pay_loan=$('#pay_loan').val(); 

        


//         var remaining_amount=$('#remaining_amount1').val();




//         remaining_amount=remaining_amount - pay_loan;


        


//         var staff_salary=$('#salary').val();


//         var deduction=$('#deduction').val();
//         var med=$('#med_all').val();
//         var rent=$('#rent_all').val();
//         var eobi=$('#eobi').val();
//         var provident_fund=$('#provident_fund').val();
//         var tax_amount=$('#tax_amount').val();

        
//         var con=$('#con_all').val();
//         var training_all=$('#training_all').val();




//         deduction=parseInt(deduction);
//         rent=parseInt(rent);
//         con=parseInt(con);
//         med=parseInt(med);
//         training_all=parseInt(training_all); 
       
       


//         var net_salary=staff_salary - deduction;  



//         net_salary=parseInt(net_salary);
        
//         net_salary=net_salary+med+rent+con+training_all;

        
//         net_salary=net_salary- pay_loan;
//         net_salary=net_salary - eobi;
//         net_salary=net_salary- provident_fund;
//         net_salary=net_salary - tax_amount;
        
//         $("#remaining_amount").val(remaining_amount);
//         $("#net_salary").val(net_salary);
//     })


// </script>


@endsection




