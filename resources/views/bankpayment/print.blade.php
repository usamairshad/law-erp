@inject('request', 'Illuminate\Http\Request')
@include('partials.head')
<style type="text/css">
body
{
    background-color:white;
}
    @page {
        margin: 10px 20px;
    }
    table{
        text-align: center;
    }
    @media print {
        table {
            font-size: 12px;
        }
        .tr-root-group {
            background-color: #F3F3F3;
            color: rgba(0, 0, 0, 0.98);
            font-weight: bold;
        }
        .tr-group {
            font-weight: bold;
        }
        .bold-text {
            font-weight: bold;
        }
        .error-text {
            font-weight: bold;
            color: #FF0000;
        }
        .ok-text {
            color: #006400;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding: 2px !important;
        }

    }
    .table th{ text-align: center; }
    .last-th th{border-bottom: 1px solid}
</style>
<div class="panel-body pad table-responsive">
    @include('partials.print_head')
    <table align="center">
        <tbody>
        <tr>
            <td><h3 align="center"><span style="border-bottom: double;">Bank Payment Sheet</span></h3>
            
            
            <p>Date: {{date('d-m-Y',strtotime( $date->created_at ))}}</p>
           
            
            </td>
        </tr>
        <tr>
            <td align="center"><span></span>
            </td>
        </tr>
        </tbody>
    </table>
            <table class="table table-bordered table-striped">
                <tr>
                <th>S.No</th>
                    <th>Ledger Head</th>
                    <th>Branch</th>
                    <th>Payee Name</th>
                    <th>Invoice Amount</th>
                    <th>Advance</th>
                    <th>Balance</th>
                    <th>PRA</th>
                    <th>Law Section</th>
                    <th>Tax Rate</th>
                    <th>Tax Amount</th>
                    <th>Amount</th>
                    <th>Cheque Amount</th>
                    <th>Remarks</th>
                </tr>
                @foreach($Entries as $ent)
               <tr>
               <td>{{$ent->sheet_id}}</td>
               <td>{{$ent->name}}</td>
               <td>{{$ent->branch_name}}</td>
               <td>{{$ent->vendor_name}}</td>
                   <td>{{$ent->inv_amount}}</td>
                   <td>{{$ent->advance}}</td>
                   <td>{{$ent->inv_amount-$ent->advance}}</td>
                   <td>{{$ent->pra}}</td>
                   <td>{{$ent->law_section}}</td>
                   <td>{{$ent->tax_rate}}%</td>
                   <td>{{$ent->inv_amount/100 * $ent->tax_rate}}</td>
                   <td>{{$ent->amount}}</td>
                   <td>{{$ent->cheque_amount}}</td>
                   <td>{{$ent->remarks}}</td>

</tr>
@endforeach

            </table>

         
        </div>
    </div>


