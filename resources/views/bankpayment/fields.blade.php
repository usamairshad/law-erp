
<div class="row">
    {!! Form::hidden('entry_type_id', old('entry_type_id', '5')) !!}
    {!! Form::hidden('suppliers_id', old('suppliers_id', '5')) !!}
</div>
<style>
    .tab-content {
    overflow: visible  !important;
}
    </style>
<!-- Slip Area Started -->
<div class="nav-tabs-custom" style = "margin-top:90px;">


    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <button onclick="FormControls.createEntryItem();" type="button" style="margin-bottom: 5px;" class="btn pull-right btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i>&nbsp;Add <u>R</u>ow</button>
            <table class="table table-bordered table-striped" id="entry_items">
                <thead>
                    <tr>
                        <th colspan="2">Account</th>
                        <th >Payee</th>
                        <th style = "display:none;">Filer Status</th>
                        <th style = "display:none;">Category</th>

                        <th >Invoice Total</th>
                        <th >Advance</th>
                        <th >Pra</th>
                        <th >Tax Law</th>
                        <th >Tax Rate</th>
                        <th >Amount</th>

                        
                       

                        <th >Narration</th>
                        <th >Action</th>
                    </tr>
                </thead>
                <tbody>
              
                        <tr id="entry_item-2">
                            <td colspan="2" >
                                <div class="form-group " style="margin-bottom: 0px !important;">
                                <select name = "ledger[]" id = 'entry_item-ledger_id-2' class ="form-control description-data-ajax" style = "width:150px;" required>
                                
                                
                                        </select>
                                </div>
                            
                            </td>
                            <td style = "width:10%;">
                                <div class="form-group" style="margin-bottom: 0px !important;">
                                  <select name = "pay[]" onchange="getRecord(2)" id="pay-2" class ="form-control" style = "width:100px;" required>
                                    <option> Select Payee</option>
                                    @foreach($asset_types as $pay )
                                    <option value = "{{$pay->vendor_id}}">{{$pay->vendor_name}}</option>
                                    @endforeach

                                  </select>
                                </div>
                             
                            </td>
                            <td  style = "width:10%;display:none;"  >
                                <div class="form-group" style="width:107px; margin-bottom: 0px !important;">
                                <input type = "text" id="pay_filer-2"  name ="pay_filer[]" placeholder = 'Filer Status' class = "form-control invoice_total"  required/>

                                 
                                </div>
                            </td>
                            <td  style = "width:10%;display:none;" >
                                <div class="form-group" style="width:107px; margin-bottom: 0px !important;">
                                <input type = "text" id="pay_category-2"  name ="pay_category[]" placeholder = 'Invoice Total' class = "form-control invoice_total"  required/>

                                 
                                </div>
                            </td>
                            <td  style = "width:10%" >
                                <div class="form-group" style="width:107px; margin-bottom: 0px !important;">
                                <input type = "number" onchange="invoice_total('#invoice_total-',2)" id="invoice_total-2"  name ="invoice_total[]"  placeholder = 'Invoice Total' class = "form-control invoice_total"  required/>

                                 
                                </div>
                            </td>
                            <td style = "width:9%">
                                <div class="form-group" style="width:80px; margin-bottom: 0px !important;">
                                <input type = "number" id = "advance-2" name ="advance[]" placeholder = 'Advance' class = "form-control advance" onchange="advance('#advance-',2)"  required/>

                                </div>
                            </td>
                            <td style = "width:9%">
                                <div class="form-group" style="width:60px; margin-bottom: 0px !important;">
                                <input type = "number"  id="pra-2" name ="pra[]" placeholder = 'PRA' value ='0' class = "form-control tax_rate" onchange="pra('#pra-',2)"  required/>
                              
                                </div>
                            </td>
                            <td style = "width:9%">
                                <div class="form-group" style="width:80px; margin-bottom: 0px !important;">
                                <select name = "law[]" id = 'law-2' onchange="EachRow(2)" class ="form-control" style = "width:80px;" required>
                                    <option> Select Law</option>
                                    @foreach($laws as $pay )
                                    <option value = "{{$pay->law_id}}">{{$pay->law_section}}</option>
                                    @endforeach

                                  </select>
                                </div>
                            </td>
                            <td style = "width:9%">
                                <div class="form-group" style="width:70px; margin-bottom: 0px !important;">
                                <input type = "number"  id="law_rate-2" name ="law_rate[]" placeholder = 'Tax Rate' class = "form-control tax_rate"  required/>
                              
                                </div>
                            </td>
                           
                            <td style = "width:25%">
                                <div class="form-group" style="width:70px; margin-bottom: 0px !important;">
                                <input type = "number"   id = 'entry_item-dr_amount-2', name ="amount[]" onblur = "FormControls.CalculateTotal()" onkeyup = "FormControls.CalculateTotal()"  onkeydown = "FormControls.CalculateTotal()" placeholder = 'Amount' class = "form-control entry_items-dr_amount amount"  required/>

                                </div>
                            </td>
                            <td style = "width:25%">
                                <div class="form-group"  style="width:100px;margin-bottom: 0px !important;">
                                <input type = "text"   name ="narration[]"  placeholder = 'Narration' class = "form-control"  required/>

                                </div>
                            </td>
                            <td><button onclick="FormControls.destroyEntryItem('2');" id="entry_item-del_btn-2" type="button" class="btn btn-block btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
                        </tr>
                 
                    <tr>
                        <td align="right" colspan="8" style="padding-top: 12px;" width="54%"><b>Total Amount</b></td>
                        <td>
                            <div class="form-group @if($errors->has('dr_total')) has-error @endif">
                                {!! Form::number('dr_total', old('dr_total', 0.00), ['id' => 'dr_total', 'class' => 'form-control', 'readonly' => 'true', 'readonly' => 'true']) !!}
                                @if($errors->has('dr_total'))
                                    <span class="help-block">
                                        {{ $errors->first('dr_total') }}
                                    </span>
                                @endif
                            </div>
                        </td>
                        <td colspan="2"></td>
                        <input type="hidden"  id="entry_item-global_counter" value="2" />
       
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.tab-pane -->
        
    </div>
    <!-- /.tab-content -->
</div>




