@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Payment Sheet Details</h3>

                
            <a href="{{ route('bank-payment') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Back</a>
         
       
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                <th>S.No</th>
                    <th>Ledger Head</th>
                    <th>Branch</th>
                    <th>Payee Name</th>
                    <th>Invoice Amount</th>
                    <th>Advance</th>
                    <th>Balance</th>
                    <th>PRA</th>
                    <th>Law Section</th>
                    <th>Tax Rate</th>
                    <th>Tax Amount</th>
                    <th>Amount</th>
                    <th>Cheque Amount</th>
                    <th>Remarks</th>
                </tr>
                @foreach($Entries as $ent)
               <tr>
               <td>{{$ent->sheet_id}}</td>
               <td>{{$ent->name}}</td>
               <td>{{$ent->branch_name}}</td>
               <td>{{$ent->vendor_name}}</td>
                   <td>{{$ent->inv_amount}}</td>
                   <td>{{$ent->advance}}</td>
                   <td>{{$ent->inv_amount-$ent->advance}}</td>
                   <td>{{$ent->pra}}</td>
                   <td>{{$ent->law_section}}</td>
                   <td>{{$ent->tax_rate}}%</td>
                   <td>{{$ent->inv_amount/100 * $ent->tax_rate}}</td>
                   <td>{{$ent->amount}}</td>
                   <td>{{$ent->cheque_amount}}</td>
                   <td>{{$ent->remarks}}</td>

</tr>
@endforeach

            </table>
            @if(Auth::user()->hasRole('Deputy GM Finance')) 
                                            {!! Form::open(array(
                                                        'style' => 'display: inline-block;',
                                                        'method' => 'post',
                                                        'style'=>'float:left',
                                                        'onsubmit' => "return confirm('Are you sure to Approved this record?');",
                                                        'route' => ['bank-payment-sheet-dgm-approval',$id])) !!}
                                            {!! Form::submit('Deputy GM Approval', array('class' => 'btn btn-xs btn-warning')) !!}
                                            {!! Form::close() !!}
                                            @endif

                                            @if(Auth::user()->hasRole('GM Finance')) 
                                            {!! Form::open(array(
                                                    'style' => 'display: inline-block;',
                                                    'method' => 'post',
                                                    'onsubmit' => "return confirm('Are you sure to Approved this record?');",
                                                    'route' => ['bank-payment-sheet-gm-approval',$id])) !!}
                                            {!! Form::submit('GM Approval', array('class' => 'btn btn-xs btn-primary')) !!}
                                            {!! Form::close() !!}
                                               @endif     
                                               @if(Auth::user()->hasRole('Accountant')) 
                                            {!! Form::open(array(
                                                    'style' => 'display: inline-block;',
                                                    'method' => 'post',
                                                    'onsubmit' => "return confirm('Are you sure to Approved this record?');",
                                                    'route' => ['bank-payment-sheet-accountant-approval',$id])) !!}
                                            {!! Form::submit('Accountant Approval', array('class' => 'btn btn-xs btn-primary')) !!}
                                            {!! Form::close() !!}
                                            @endif    
         
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
