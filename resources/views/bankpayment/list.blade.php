@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
   
@stop
@section('breadcrumbs')

@section('content')
<style type = "text/css">
        .nav-tabs .nav-link.active {
            background: linear-gradient( 45deg, #efd7db, #efd7db) !important;
    color:black !important;
        }
        .tab-content {
            background: linear-gradient( 45deg, #efd7db, #efd7db) !important;
        }
        </style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Payment Sheet Details</h3>

                
            <a href="{{ route('bank-payment') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Back</a>
         
       
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
        <div class="row">
		
        <div class="col-md-12 ">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Pending Bank Payment List</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Pending Cash Payment List</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Pending Rental Payment List</a>
            </div>
            </nav>
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="col-lg-12 col-md-12">
                <table class="table table-bordered table-striped">
                <thead>
                <tr>
          
                    <th  style = "color:Black;">Ledger Head</th>
                    <th  style = "color:Black;">Branch</th>
                    <th  style = "color:Black;">Payee Name</th>
                    <th  style = "color:Black;">Invoice Amount</th>
                    <th style = "color:Black;">Advance</th>
                    <th  style = "color:Black;">Balance</th>
                    <th  style = "color:Black;">Law Section</th>
                    <th  style = "color:Black;">Tax Rate</th>
                    <th  style = "color:Black;">Tax Amount</th>
                    <th  style = "color:Black;">Amount</th>
                    <th  style = "color:Black;">Cheque Amount</th>
                    <th  style = "color:Black;">Remarks</th>
                    <th  style = "color:Black;">Action</th>
                </tr>
                </thead>

                <tbody>
             
                @foreach($Entries as $ent)
               <tr>
               <td>{{$ent->name}}</td>
               <td>{{$ent->branch_name}}</td>
               <td>{{$ent->vendor_name}}</td>
                   <td>{{$ent->inv_amount}}</td>
                   <td>{{$ent->advance}}</td>
                   <td>{{$ent->inv_amount-$ent->advance}}</td>
                   <td>{{$ent->law_section}}</td>
                   <td>{{$ent->tax_rate}}%</td>
                   <td>{{$ent->inv_amount/100 * $ent->tax_rate}}</td>
                   <td>{{$ent->amount}}</td>
                   <td>{{$ent->cheque_amount}}</td>
                   <td>{{$ent->remarks}}</td>
                   <td>
                   {!! Form::open(array(
                                                        'style' => 'display: inline-block;',
                                                        'method' => 'post',
                                                        'style'=>'float:right',
                                                        'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                        'route' => ['generate-voucher-payment',$ent->ps_id])) !!}
                                            {!! Form::submit('Release', array('class' => 'btn btn-xs btn-warning')) !!}
                                            {!! Form::close() !!}
                                         
                   </td>

</tr>
@endforeach
                </tbody>
            </table>
                </div>

            </div>
                <div class="tab-pane fade" id="nav-profile"   role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="col-lg-12 col-md-12">
                <table class="table table-bordered table-striped">
                <thead>
                <tr>
         
                    <th  style = "color:Black;">Ledger Head</th>
                    <th  style = "color:Black;">Branch</th>
                    <th  style = "color:Black;">Payee Name</th>
                    <th  style = "color:Black;">Invoice Amount</th>
                    <th style = "color:Black;">Advance</th>
                    <th  style = "color:Black;">Balance</th>
                    <th  style = "color:Black;">Law Section</th>
                    <th  style = "color:Black;">Tax Rate</th>
                    <th  style = "color:Black;">Tax Amount</th>
                    <th  style = "color:Black;">Amount</th>
                    <th  style = "color:Black;">Cheque Amount</th>
                    <th  style = "color:Black;">Remarks</th>
                    <th  style = "color:Black;">Action</th>
                </tr>
                </thead>

                <tbody>
             
                @foreach($cash_entries as $ent)
               <tr>
           
               <td>{{$ent->name}}</td>
               <td>{{$ent->branch_name}}</td>
               <td>{{$ent->vendor_name}}</td>
                   <td>{{$ent->inv_amount}}</td>
                   <td>{{$ent->advance}}</td>
                   <td>{{$ent->inv_amount-$ent->advance}}</td>
                   <td>{{$ent->law_section}}</td>
                   <td>{{$ent->tax_rate}}%</td>
                   <td>{{$ent->inv_amount/100 * $ent->tax_rate}}</td>
                   <td>{{$ent->amount}}</td>
                   <td>{{$ent->cheque_amount}}</td>
                   <td>{{$ent->remarks}}</td>
                   <td>
                   {!! Form::open(array(
                                                        'style' => 'display: inline-block;',
                                                        'method' => 'post',
                                                        'style'=>'float:right',
                                                        'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                        'route' => ['generate-voucher-payment',$ent->ps_id])) !!}
                                            {!! Form::submit('Release', array('class' => 'btn btn-xs btn-warning')) !!}
                                            {!! Form::close() !!}
                                            {!! Form::open(array(
                                                        'style' => 'display: inline-block; background-color:#c13535 !important;',
                                                        'method' => 'post',
                                                        'style'=>'float:right',
                                                        'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                        'route' => ['generate-voucher-payment',$ent->ps_id])) !!}
                                            {!! Form::submit('Hold', array('class' => 'btn btn-xs btn-danger')) !!}
                                            {!! Form::close() !!}
                   </td>

</tr>
@endforeach
                                            </tbody>
                                            </table>
                </div>
            </div>
                <div class="tab-pane fade" id="nav-contact"  role="tabpanel" aria-labelledby="nav-contact-tab">
                <div class="col-lg-12 col-md-12">
                <table class="table table-bordered table-striped">
                <thead>
                <tr>
            
                    <th  style = "color:Black;">Ledger Head</th>
                    <th  style = "color:Black;">Branch</th>
                    <th  style = "color:Black;">Payee Name</th>
                    <th  style = "color:Black;">Invoice Amount</th>
                    <th style = "color:Black;">Advance</th>
                    <th  style = "color:Black;">Balance</th>
                    <th  style = "color:Black;">Law Section</th>
                    <th  style = "color:Black;">Tax Rate</th>
                    <th  style = "color:Black;">Tax Amount</th>
                    <th  style = "color:Black;">Amount</th>
                    <th  style = "color:Black;">Cheque Amount</th>
                    <th  style = "color:Black;">Remarks</th>
                    <th  style = "color:Black;">Action</th>
                </tr>
                </thead>

                <tbody>
             
                @foreach($rental_entries as $ent)
               <tr>
           
               <td>{{$ent->name}}</td>
               <td>{{$ent->branch_name}}</td>
               <td>{{$ent->vendor_name}}</td>
                   <td>{{$ent->inv_amount}}</td>
                   <td>{{$ent->advance}}</td>
                   <td>{{$ent->inv_amount-$ent->advance}}</td>
                   <td>{{$ent->law_section}}</td>
                   <td>{{$ent->tax_rate}}%</td>
                   <td>{{$ent->inv_amount/100 * $ent->tax_rate}}</td>
                   <td>{{$ent->amount}}</td>
                   <td>{{$ent->cheque_amount}}</td>
                   <td>{{$ent->remarks}}</td>
                   <td>
                   {!! Form::open(array(
                                                        'style' => 'display: inline-block;',
                                                        'method' => 'post',
                                                        'style'=>'float:right',
                                                        'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                        'route' => ['generate-voucher-payment',$ent->ps_id])) !!}
                                            {!! Form::submit('Release', array('class' => 'btn btn-xs btn-warning')) !!}
                                            {!! Form::close() !!}
                                            {!! Form::open(array(
                                                        'style' => 'display: inline-block; background-color:#c13535 !important;',
                                                        'method' => 'post',
                                                        'style'=>'float:right',
                                                        'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                        'route' => ['generate-voucher-payment',$ent->ps_id])) !!}
                                            {!! Form::submit('Hold', array('class' => 'btn btn-xs btn-danger')) !!}
                                            {!! Form::close() !!}
                   </td>

</tr>
@endforeach
</tbody>
                                            </table>

                </div></div>
                
            </div>
        
        </div>
    </div>
</div>
</section>
         
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
