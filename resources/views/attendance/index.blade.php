@inject('request', 'Illuminate\Http\Request')
@inject('Currency', '\App\Helpers\Currency')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')
<style>
    .form-control
    {
        float:left;
    }
    </style>
@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
	<div class="card">
		<div class="card-body">
			<div class="main-content-label mg-b-5">
            <h3 style = "float:left;">Syncronize Attendance</h3>
    <div class="box box-primary">
         <a  href="{{route('admin.courses.index')}}" style = "float:right;" class="btn btn-success pull-right">Back</a>
         <div id="show">
            <div id="box" class="box-body" style = "padding-top:40px;">
            {!! Form::open(['method' => 'POST', 'route' => ['fetch-data'], 'id' => 'validation-form']) !!}
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left">
                    {!! Form::label('name', 'Select Branch*', ['class' => 'control-label']) !!}
                        <select style="width: 100%" name="branch_id" class="form-control branch_id" required="required">

                          @foreach($Branches as  $branch)
                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                          @endforeach
                          
                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('subject_code')) has-error @endif"  style = "float:left">
                {!! Form::label('name', 'IP Address*', ['class' => 'control-label']) !!}
                        <select style="width: 100%" name="ip_address" class="form-control ip_address" id = "ip_address" required="required">

                    
                        </select>
                          </div>
             
            
           
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
            {!! Form::close() !!}
        </div>

    </div>
@stop
@section('javascript')
<script type = "text/javascript">
   $(document).on('change','.branch_id',function () 
        {
         
          
 
            var branch_id=$(this).val();
           


            var a=$('#ip_address').parent();
        
         
            var op="";
          
            $.ajax({
                type:'get',
                url:'{!!URL::to('getIpAddress')!!}',
                data:{'branch_id':branch_id},
                dataType:'json',//return data will be json
                async:false,
                success:function(data){
               
           

                    $html = '<option value="null">Select IP Address</option>';
                    data.forEach((dat) => {
            
                        $html += `<option value=${dat.ip_address}>${dat.ip_address}</option>`;
                    });

                    $("#ip_address").html($html);

                },
                error:function(){
                }
            });


        }); 
</script>
    <script type="text/javascript">
    
    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" style="height:120px"><hr style="border-top:1px solid #222d32"><div class="form-group col-md-3"><input required="required" type="text" name="name[]" placeholder="Enter Course Name" class="form-control" /></div><div class="form-group col-md-3"><input required="required" type="text" name="subject_code[]" placeholder="Enter Subject Code" class="form-control" /></div><div class="form-group col-md-3"><input required="required" type="number" name="credit_hours[]" placeholder="Enter Credit Hours" class="form-control" min="0" /></div><div class="form-group col-md-3"><input required="required" type="text" name="actual_fee[]" placeholder="Enter Actual Fee" class="form-control"/></div><div class="form-group col-md-3"><input required="required" type="text" name="subject_fee[]" placeholder="Enter Subject Fee" class="form-control"/></div><div class="form-group col-md-3"><input required="required" type="text" name="subject_optional_code[]" placeholder="Enter Subject Fee" class="form-control"/></div><div class="form-group col-md-6"><input type="text" required="required" name="description[]" placeholder="Enter Description" class="form-control" /></div><br> <div class="form-group col-md-2"><button style="margin-top: -27px;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    </script>
    <script src="{{ url('public/js/admin/companies/create_modify.js') }}" type="text/javascript"></script>
@endsection

