@extends('layouts.app')

@section('css')
@stop
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Academics Roles</h1>
    </section>
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
          <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}">
            <tr>
                 <th>id</th>
                 <th>name</th>
                 <th>display_name</th>
                 <th>description</th>
                 <th>action</th>
            </tr>

            @foreach($data['roles'] as $role)
            <tr>
                  <td>{{$role->id}}</td>
                  <td>{{$role->name}}</td>
                  <td>{{$role->display_name}}</td>
                  <td>{{$role->description}}</td>
                  <td>
                    <a href="{{ route('role.edit',[$role->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                    <a href="{{ route('role.delete', ['id' => $role->id]) }}" class="tooltip-error bootbox-confirm" data-rel="tooltip" title="Delete">
                                                    <span class="red ">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </span>
                                                    </a>
                  </td>
            </tr>
            @endforeach
          </table>
        </div>
    </div>
@stop


@section('js')


    
@stop