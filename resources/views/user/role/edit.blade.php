@extends('layouts.app')

@section('css')
@endsection
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Roles</h1>
    </section>
@stop
@section('content')
<div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Role</h3>
            <a href="{{ route('role') }}" class="btn btn-success pull-right">Back</a>
        </div>
{!! Form::model($data['row'], ['route' => [$base_route.'.update', $data['row']->id], 'method' => 'POST', 'class' => 'form-horizontal',
                    'id' => 'validation-form', "enctype" => "multipart/form-data"]) !!}

                    {!! Form::hidden('id', $data['row']->id) !!}
                    <div class="box-body">
                        @include($view_path.'.includes.form')
                    </div>
        <div class="box-footer">
            <button class="btn btn-info" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Update
            </button>
        </div>

                    {!! Form::close() !!}

</div>

@endsection


@section('js')

    <script>
        $(document).ready(function () {
            $('table th input:checkbox').on('click' , function(){
                var that = this;
                $(this).closest('table').find('input:checkbox')
                    .each(function(){
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

            });

            $('.group').on('click' , function(){
                var that = this;
                $(this).closest('tr').find('input:checkbox')
                    .each(function(){
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });

            });


        });
    </script>
@endsection
