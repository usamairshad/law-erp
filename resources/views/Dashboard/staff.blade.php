@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Staff</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>

        <div class="container-fluid">
            <form method="post" action="{{url("filter-staff")}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">
                <div class="col-md-4">
                    <label for="formGroupExampleInput">Branch:</label>
                    <select class="form-control" name="branch_id">
                        <option value="" disabled selected>--Select--</option>
                        {!! App\Models\Admin\Branches::branchList() !!}
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="formGroupExampleInput" >Staff Role:</label>
                    <select class="form-control"  name="staff_role_id">
                        <option value="" disabled selected>--Select--</option>
                        {!! App\Roles::role() !!}
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="formGroupExampleInput">Job Status:</label>
                    <select class="form-control" name="job_status_id">
                        <option value="" disabled selected>--Select--</option>
                        <option value="1">Active</option>
                        <option value="2">InActive</option>
                        <option value="3">Permanent</option>
                        <option value="4">Temporary</option>
                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
            </form>
            <br>
            <div class="row">
                @foreach($records as $record)
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                {{$record->first_name}}
                            </div>
                            <div class="panel-footer">
                                <i class="fa fa-eye modalTrigger" data-staff="{{$record}}" data-toggle="modal" data-target="#exampleModal{{$record->id}}" style="margin-left: 15px" ></i>
                                <i class="fa fa-building" style="margin-left: 60px"></i>
                            </div>
                        </div>
                    </div>
                    {{--modal--}}
                    <div class="modal fade" id="exampleModal{{$record->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div style="padding: 10px">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>Reg No</th>
                                            <th>Join date</th>
                                            <th>Blood Group</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['reg_no']}}</td>
                                            <td>{{$record['join_date']}}</td>
                                            <td>{{$record['blood_group']}}</td>
                                        </tr>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Middle Name</th>
                                            <th>Last Name</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['first_name']}}</td>
                                            <td>{{$record['middle_name']}}</td>
                                            <td>{{$record['last_name']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Father Name</th>
                                            <th>Date Of Birth</th>
                                            <th>Gender</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['father_name']}}</td>
                                            <td>{{$record['date_of_birth']}}</td>
                                            <td>{{$record['gender']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Mother Name</th>
                                            <th>Nationality</th>
                                            <th>Mother Tongue</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['mother_name']}}</td>
                                            <td>{{$record['nationality']}}</td>
                                            <td>{{$record['mother_tongue']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Country</th>
                                            <th>City</th>
                                            <th>Address</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['country']}}</td>
                                            <td>{{$record['city']}}</td>
                                            <td>{{$record['address']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Temp Address</th>
                                            <th>Home Phone</th>
                                            <th>Mobile 1</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['temp_address']}}</td>
                                            <td>{{$record['home_phone']}}</td>
                                            <td>{{$record['mobile_1']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Mobile_2</th>
                                            <th>Email</th>
                                            <th>Qualification</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['mobile_2']}}</td>
                                            <td>{{$record['email']}}</td>
                                            <td>{{$record['Qualification']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Experience</th>
                                            <th>Experience Info</th>
                                            <th>Other Info</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['experience']}}</td>
                                            <td>{{$record['experience_info']}}</td>
                                            <td>{{$record['other_info']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Skill</th>
                                            <th>Salary</th>
                                            <th>CNIC</th>
                                        </tr>
                                        <tr>
                                            <td>{{$record['skill']}}</td>
                                            <td>{{$record['salary']}}</td>
                                            <td>{{$record['cnic']}}</td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>

                            </div>
                        </div>
                    </div>
                    {{--modal end--}}
                @endforeach
            </div>

        </div>
    </div>

@stop
@section('javascript')
    <script>
        $('.modalTrigger').on('click',function(){
            var registration = $(this).data('staff').reg_no;
            var join_date = $(this).data('staff').join_date;
            var first_name = $(this).data('staff').first_name;
            var middle_name = $(this).data('staff').middle_name;
            var last_name = $(this).data('staff').last_name;
            var father_name = $(this).data('staff').father_name;
            var mother_name = $(this).data('staff').mother_name;
            var dateofbirth = $(this).data('staff').date_of_birth;
            var gender = $(this).data('staff').gender;
            var blood_group = $(this).data('staff').blood_group;
            var nationality = $(this).data('staff').nationality;
            var mother_tongue = $(this).data('staff').mother_tongue;
            var address = $(this).data('staff').address;
            var country = $(this).data('staff').country;
            var city = $(this).data('staff').city;
            var temp_address = $(this).data('staff').temp_address;
            var home_phone = $(this).data('staff').home_phone;
            var mobile_1 = $(this).data('staff').mobile_1;
            var mobile_2 = $(this).data('staff').mobile_2;
            var email = $(this).data('staff').email;
            var qualification = $(this).data('staff').qualification;
            var experience = $(this).data('staff').experience;
            var experience_info = $(this).data('staff').experience_info;
            var other_info = $(this).data('staff').other_info;
            var skill = $(this).data('staff').skill;
            var salary = $(this).data('staff').salary;
            var cnic = $(this).data('staff').cnic;






            $('#registration').val(registration);
            $('#join_date').val(join_date);
            $('#first_name').val(first_name);
            $('#middle_name').val(middle_name);
            $('#last_name').val(last_name);
            $('#father_name').val(father_name);
            $('#dateofbirth').val(dateofbirth);
            $('#gender').val(gender);
            $('#blood_group').val(blood_group);
            $('#nationality').val(nationality);
            $('#mother_tongue').val(mother_tongue);
            $('#address').val(address);
            $('#country').val(country);
            $('#city').val(city);
            $('#temporaryaddress').val(temp_address);
            $('#home_phone').val(home_phone);
            $('#mobile_1').val(mobile_1);
            $('#mobile_2').val(mobile_2);
            $('#email').val(email);
            $('#qualification').val(qualification);
            $('#experience').val(experience);
            $('#experience_info').val(experience_info);
            $('#other_info').val(other_info);
            $('#skill').val(skill);
            $('#salary').val(salary);
            $('#cnic').val(cnic);
            $('#mother_name').val(mother_name);

        });

    </script>
@endsection