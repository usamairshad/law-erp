@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Branches</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <div class="container-fluid">
            <div class="row">
                @foreach($records as $record)


                    <div class="col-md-4">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                {{$record->name ?? ''}}
                                {{$record['board']->name ?? ''}}
                            </div>
                            <div class="panel-footer">
                                <i class="fa fa-eye modalTrigger" data-branch = '{{$record}}' data-toggle="modal" data-target="#exampleModal" data-toggle="modal" data-target="#exampleModal"  data-target="#MymodalPreventHTML" data-toggle="modal" data-backdrop="static" data-keyboard="false" style="margin-left: 15px" ></i>
                               <a href="{{url('related-board',$record->id)}}"> <i class="fa fa-building" style="margin-left: 60px"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
    {{--modal--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                </div>
                <div class="panel-body table-responsive">

                    <table class="table table-bBanked table-striped dt-select">
                        <thead>
                        <tr>
                            <th>CITY NAME</th>
                            <th>BRANCH CODE</th>
                            <th>PHONE</th>
                            <th>FAX</th>
                            <th>CELL</th>
                            <th>ADDRESS</th>
                        </tr>
                        </thead>

                        <tbody id="append_row">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="removeDiv" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    {{--modal end--}}
@stop
@section('javascript')
    <script>
        $('.modalTrigger').on('click',function(){
            var city_name = $(this).data('branch').city_name;
            var branch_code = $(this).data('branch').branch_code;
            var phone = $(this).data('branch').phone;
            var fax = $(this).data('branch').fax;
            var cell = $(this).data('branch').cell;
            var address = $(this).data('branch').address;
            $('#append_row').
            append('"<tr id="append"><td>'+city_name+'</td>' +
                '<td>'+branch_code+'</td>' +
                '<td>'+phone+'</td>' +
                '<td>'+fax+'</td>' +
                '<td>'+cell+'</td>' +
                '<td>'+address+'</td>' +
                '</tr>"');
            $(document).ready(function(){
                $('#removeDiv').click(function(){
                    $('#append').remove();
                });
            });

        });
    </script>
    @endsection