
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

<link href="{{ url('public/theme') }}/assets/plugins/morris.js/morris.css" rel="stylesheet">
<link href="{{ url('public/theme') }}/assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="{{ url('public/theme') }}/assets/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>
<link href="{{ url('public/theme') }}/assets/plugins/multislider/multislider.css" rel="stylesheet">

<div class = "row">
<!-- <div class="col-lg-12 col-md-12">
						<div class="card custom-card">
							<div class="card-body ht-100p">
								<div>
									<h6 class="card-title mb-1">General Information</h6>
								</div>
								<div id="basicSlider">
									<div class="MS-content">
										<div class="item">
											<a href="#" target="_blank"> 
						<div class="card text-center" style = "background: linear-gradient( 45deg, #f54266, #3858f9) !important; color: white; ">
							<div class="card-body ">
								<div class="feature widget-2 text-center mt-0 mb-3">
									<i class="ti-bar-chart project bg-primary-transparent mx-auto text-primary "></i>
								</div>
								<h6 class="mb-1 text-muted" style = "color: white !important;">Companies</h6>
								<h3 class="font-weight-semibold">{{$companies ?? 0}} </h3>
							</div>
						</div>
				
                       </a>
										</div>

                    <div class="item">
											<a href="#" target="_blank"> 
						<div class="card text-center" style = "background: linear-gradient( 45deg, #f54266, #3858f9) !important; color: white; ">
							<div class="card-body ">
								<div class="feature widget-2 text-center mt-0 mb-3">
									<i class="ti-bar-chart project bg-primary-transparent mx-auto text-primary "></i>
								</div>
                <h6 class="mb-1 text-muted" style = "color: white !important;">Branches</h6>
								<h3 class="font-weight-semibold">{{$branch_count ?? 0}} </h3>
							</div>
						</div>
				
                       </a>
										</div>

                    <div class="item">
											<a href="#" target="_blank"> 
						<div class="card text-center" style = "background: linear-gradient( 45deg, #f54266, #3858f9) !important; color: white; ">
							<div class="card-body ">
								<div class="feature widget-2 text-center mt-0 mb-3">
									<i class="ti-bar-chart project bg-primary-transparent mx-auto text-primary "></i>
								</div>
								<h6 class="mb-1 text-muted" style = "color: white !important;">Programs</h6>
								<h3 class="font-weight-semibold">{{$programs ?? 0}} </h3>
							</div>
						</div>
				
                       </a>
										</div>

                    <div class="item">
											<a href="#" target="_blank"> 
						<div class="card text-center" style = "background: linear-gradient( 45deg, #f54266, #3858f9) !important; color: white; ">
							<div class="card-body ">
								<div class="feature widget-2 text-center mt-0 mb-3">
									<i class="ti-bar-chart project bg-primary-transparent mx-auto text-primary "></i>
								</div>
								<h6 class="mb-1 text-muted" style = "color: white !important;">Total Staff</h6>
								<h3 class="font-weight-semibold">{{$staff ?? 0}} </h3>
							</div>
						</div>
				
                       </a>
										</div>

                    <div class="item">
											<a href="#" target="_blank"> 
						<div class="card text-center" style = "background: linear-gradient( 45deg, #f54266, #3858f9) !important; color: white; ">
							<div class="card-body ">
								<div class="feature widget-2 text-center mt-0 mb-3">
                <i class="ti-bar-chart project bg-primary-transparent mx-auto text-primary "></i>
								</div>
								<h6 class="mb-1 text-muted" style = "color: white !important;">Total Students</h6>
								<h3 class="font-weight-semibold">{{$students ?? 0}} </h3>
							</div>
						</div>
				
                       </a>
                       </div>
                       <div class="item">
                       <a href="#" target="_blank"> 
						<div class="card text-center" style = "background: linear-gradient( 45deg, #f54266, #3858f9) !important; color: white; ">
							<div class="card-body ">
								<div class="feature widget-2 text-center mt-0 mb-3">
									<i class="ti-bar-chart project bg-primary-transparent mx-auto text-primary "></i>
								</div>
								<h6 class="mb-1 text-muted" style = "color: white !important;">Courses</h6>
                <h3 class="font-weight-semibold">{{$courses ?? 0}}  </h3>
							</div>
						</div>
				
                       </a>
                       </div>
                       <div class="item">
                       <a href="#" target="_blank"> 
						<div class="card text-center" style = "background: linear-gradient( 45deg, #f54266, #3858f9) !important; color: white; ">
							<div class="card-body ">
								<div class="feature widget-2 text-center mt-0 mb-3">
									<i class="ti-bar-chart project bg-primary-transparent mx-auto text-primary "></i>
								</div>
								<h6 class="mb-1 text-muted" style = "color: white !important;">Fee Defaulters</h6>
								<h3 class="font-weight-semibold">3</h3>
							</div>
						</div>
				
                       </a>
										</div>
					
								
									</div>
								</div>
							</div>
						</div>
					</div>
 -->

 <div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">SOS Staff</div>
										<div class="h3 mt-2 mb-2"><b>{{$sos_teacher ?? 0}}</b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall SOS Staff </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">SOS Students</div>
										<div class="h3 mt-2 mb-2"><b>{{$sos_student ?? 0}}</b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall SOS Students </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Fee Defaulters</div>
										<div class="h3 mt-2 mb-2"><b>{{$defaulter ?? 0}}</b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Fee Defaulters</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Extra Info</div>
										<div class="h3 mt-2 mb-2"><b>{{$defaulter ?? 0}}</b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Fee Defaulters</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>

          <div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">New Users</div>
										<div class="h3 mt-2 mb-2"><b>0</b><span class="text-success tx-13 ml-2">(+15%)</span></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview of Last month</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Monthly<span class="float-right text-muted">50%</span></small>
								</div>
							</div>
						</div>
					</div>


					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Revenue</div>
										<div class="h3 mt-2 mb-2"><b>0M</b><span class="text-success tx-13 ml-2">(pkr)</span></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="ti-bar-chart-alt project bg-success-transparent text-success "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview of Last month</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-success wd-25 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Monthly<span class="float-right text-muted">Amount in PKR</span></small>
								</div>
							</div>
						</div>
					</div>
          <div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Income</div>
										<div class="h3 mt-2 mb-2"><b>0K</b><span class="text-success tx-13 ml-2">(pkr)</span></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-eye project bg-primary-transparent text-primary "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview of Last month</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-primary wd-80 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Monthly<span class="float-right text-muted">Amount in pkr</span></small>
								</div>
							</div>
						</div>
					</div>
				
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Expense</div>
										<div class="h3 mt-2 mb-2"><b>0</b><span class="text-success tx-13 ml-2">pkr</span></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="ti-pulse project bg-warning-transparent text-warning "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview of Last month</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-danger wd-30 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Monthly<span class="float-right text-muted">Amount in Pkr</span></small>
								</div>
							</div>
						</div>
					</div>
</div>

<div class="row row-sm">
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="card-order">
									<h6 class="mb-2">Total Payables</h6>
									<h2 class="text-right "><i class="mdi mdi-account-multiple icon-size float-left text-primary text-primary-shadow"></i><span>0</span></h2>
									<p class="mb-0">Monthly Payables<span class="float-right">pkr</span></p>
								</div>
							</div>
						</div>
					</div><!-- COL END -->
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
						<div class="card ">
							<div class="card-body">
								<div class="card-widget">
									<h6 class="mb-2">Total Tax</h6>
									<h2 class="text-right"><i class="mdi mdi-cube icon-size float-left text-success text-success-shadow"></i><span>0</span></h2>
									<p class="mb-0">Monthly Income<span class="float-right">pkr</span></p>
								</div>
							</div>
						</div>
					</div><!-- COL END -->
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="card-widget">
									<h6 class="mb-2">Total Profit</h6>
									<h2 class="text-right"><i class="icon-size mdi mdi-poll-box   float-left text-warning text-warning-shadow"></i><span>0</span></h2>
									<p class="mb-0">Monthly Profit<span class="float-right">pkr</span></p>
								</div>
							</div>
						</div>
					</div><!-- COL END -->
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
						<div class="card ">
							<div class="card-body">
								<div class="card-widget">
									<h6 class="mb-2">Total Receivables</h6>
									<h2 class="text-right"><i class="fa fa-cart-plus icon-size float-left text-danger text-danger-shadow"></i><span>0</span></h2>
									<p class="mb-0">Monthly Sales<span class="float-right">pkr</span></p>
								</div>
							</div>
						</div>
					</div><!-- COL END -->
				</div>
        <div class="row row-sm">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
								IVY Education Group
								</div>
        
        <div aria-multiselectable="true" class="accordion" id="accordion" role="tablist">
									<div class="card">
										<div class="card-header" id="headingOne" role="tab">
											<a aria-controls="collapseOne" style = "background-color:#205220; height:55px; color:white;" aria-expanded="true" data-toggle="collapse" href="#collapseOne"> 
                      <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">Accounts Head</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;"> Roots IVY International Schools</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;"> Roots IVY College of manage Sciences</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;"> Roots IVY United Schools</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">Overall</div>
        
        </a>
										</div>
										
									</div>
									<div class="card">
										<div class="card-header" id="headingTwo" role="tab">
											<a aria-controls="collapseTwo" style = " height:55px;" aria-expanded="false" class="collapsed" data-toggle="collapse" href="#collapseTwo">
                      <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">Receivables</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">300</div>
                      </a>
										</div>
										<div aria-labelledby="headingTwo" class="collapse" data-parent="#accordion" id="collapseTwo" role="tabpanel">
											<div class="card-body">
                      <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">
        @foreach ($receivables as $receivable)
                                {{ $receivable->name }}
                                <hr/>
                                <br/>
                                @endforeach
        
        </div>
		
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">300</div>
                     

											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingThree" role="tab">
											<a aria-controls="collapseThree" aria-expanded="false" style = " height:55px;" class="collapsed" data-toggle="collapse" href="#collapseThree">  <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">Payables</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">300</div></a>
										</div>
										<div aria-labelledby="headingThree" class="collapse" data-parent="#accordion" id="collapseThree" role="tabpanel">
											<div class="card-body">
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore.
											</div>
										</div><!-- collapse -->
									</div>
                  <div class="card">
										<div class="card-header" id="headingfour" role="tab">
											<a aria-controls="collapsefour" aria-expanded="false" style = " height:55px;" class="collapsed" data-toggle="collapse" href="#collapsefour">  <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">Income</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">300</div></a>
										</div>
										<div aria-labelledby="headingfour" class="collapse" data-parent="#accordion" id="collapsefour" role="tabpanel">
											<div class="card-body">
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore.
											</div>
										</div><!-- collapse -->
									</div>
                  <div class="card">
										<div class="card-header" id="headingfive" role="tab">
											<a aria-controls="collapsefive" aria-expanded="false" style = " height:55px;" class="collapsed" data-toggle="collapse" href="#collapsefive">  <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">Expense</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">100</div>
        <div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">300</div></a>
										</div>
										<div aria-labelledby="headingfive" class="collapse" data-parent="#accordion" id="collapsefive" role="tabpanel">
											<div class="card-body">
											<div class="col-lg-3 col-md-3" style = "float:left; text-align:center;">
											<div class="col-lg-2 col-md-2" style = "float:left; text-align:center;">100</div>
        @foreach ($expenses as $receivable)
                                {{ $receivable->name }}
                                <hr/>
                                <br/>
                                @endforeach
        
        </div>	
										</div>
										</div><!-- collapse -->
									</div>
								</div><!-- accordion -->
        </table>
							</div>
						</div>
					</div>
          </div>
          <div class="row row-sm">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
								Income V/s Expense
								</div>
                
                <table  style = "width:100%; color:white;">
        <tr >
        <th>
        <table style ="width:100%; text-align:center;">
        <tr style = "width:100%;;background-color:#54a8c3;color:white;width:100%">
        <td></td>
        <td></td>
        <td></td>
        
        <td >Fixed Expense</td>
        <td></td>
        <td></td>
        <td></td>
        </tr>
        <tr style = "background-color:#54a8c3;color:white;">
        <td style = "padding: 19px;"> Salaries </td>
        <td style = "padding: 19px;"> Lower Staff </td>
        <td style = "padding: 19px;"> Security Staff </td>
        <td style = "padding: 19px;"> EOBI ESS and EPF </td>
        <td style = "padding: 19px;"> Mark Up </td>
        <td style = "padding: 19px;"> Rent </td>
        <td style = "padding: 19px;"> Total </td>

        </tr>
        <tr>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;">100 </td>
        <td style = "padding: 19px;"> 100 </td>

        </tr>
        </table></th>
       
        
        </tr>
        </table>
        <table style = "width:100%;">
        <tr style = "background-color:#0a960a;color:white;text-align:center;width:100%">
        <td></td>
        <td></td>
        <td></td>
        
        <td >Running Expense</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        </tr>
        <tr style = "background-color:#0a960a;color:white;text-align:center;width:100%">
        <td style = "padding: 19px;"> Petty Cash </td>
        <td style = "padding: 19px;"> Photo Copy </td>
        <td style = "padding: 19px;"> Utilities </td>
        <td style = "padding: 19px;"> Internet </td>
        <td style = "padding: 19px;"> Generator and POL </td>
        <td style = "padding: 19px;"> But Payment </td>
        <td style = "padding: 19px;"> Others </td>
        <td style = "padding: 19px;"> Total  Variable Expense</td>

        </tr>
        <tr>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;"> 100 </td>
        <td style = "padding: 19px;">100 </td>
        <td style = "padding: 19px;">100 </td>
        <td style = "padding: 19px;"> 100</td>
        <td style = "padding: 19px;"> 100</td>

        </tr>
        </table>
							</div>
						</div>
					</div>
          </div>
				<div class="row row-sm">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
								Income V/s Expense
								</div>
								<div id="echart1" class="ht-300"></div>
							</div>
						</div>
					</div>
          </div>
        
        
   </div>

@stop
<!-- Bar chart for Defaulter -->
