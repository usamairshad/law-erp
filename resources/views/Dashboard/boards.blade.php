@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Boards</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <div class="container-fluid">
            <div class="row">
                @foreach($records as $record)
                    <div class="col-md-4" >

                        <div class="panel panel-default">
                            <div class="panel-body">
                                {{$record->name ?? ''}}
                                {{$record['board']->name ?? ''}}
                            </div>
                            <div class="panel-footer">
                             <i class="fa fa-eye modalTrigger" data-board="{{$record}}"  data-toggle="modal" data-target="#exampleModal" data-target="#MymodalPreventHTML" data-toggle="modal" data-backdrop="static" data-keyboard="false" style="margin-left: 15px" ></i>
                                <a href="{{url('related-program',$record->id)}}"> <i class="fa fa-building" style="margin-left: 60px"></i></a>
                            </div>
                        </div>
                    </div>
                    @if ($loop->iteration % 3 == 0)
            </div>


        <div class="row">
            @endif
            @endforeach
        </div>
        </div>
    </div>
    {{--modal--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                </div>
                <div class="panel-body table-responsive">

                    <table class="table table-bBanked table-striped dt-select">
                        <thead>
                        <tr>

                            <th>STATUS</th>
                        </tr>
                        </thead>

                        <tbody class="append_row">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="removeDiv" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    {{--modal end--}}
@stop
@section('javascript')

    <script>
        $('.modalTrigger').on('click',function(){
            var status = $(this).data('board').status;

            if (status){
                $('.append_row').
                append('"<tr id="append"><td> '+status+'</td>' +
                    '</tr>"');
            }
      if($(this).data('board')['board']){
          var status1 = $(this).data('board')['board'].status;
          $('.append_row').
          append('"<tr id="append"><td>'+status1+'</td>' +
              '</tr>"');
      }




        });
        $(document).ready(function(){
            $('#removeDiv').click(function(){
                $('#append').remove();
            });
        });
    </script>
    @endsection