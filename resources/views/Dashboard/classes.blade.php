@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Classes</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <div class="container-fluid">
            <div class="row">
                @foreach($records as $record)
                    <div class="col-md-4">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                {{$record['classes']->name ?? ''}}
                            </div>
                            <div class="panel-footer">
                                <i class="fa fa-eye modalTrigger"  style="margin-left: 15px" ></i>
                                <a data-program="" data-toggle="modal" data-target="#exampleModal" data-target="#MymodalPreventHTML" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="studentCount({{$record['classes']->id}})">Student Count</a>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>

        </div>
    </div>
    {{--modal--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                </div>
                <div class="panel-body table-responsive">

                    <table class="table table-bBanked table-striped dt-select" id="countTable">
                        <thead>
                        <tr>
                            <th>Strength:</th>
                        </tr>
                        </thead>

                        <tbody>
                        <td id="td"></td>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-secondary" id="removeDiv" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    {{--modal end--}}
@stop
<script>
    function studentCount(id){
        $.ajax({
            url: '{{url('/student-count')}}/'+id,
            type: 'GET',
            success: function(data){
                $('#td').text(data);
            }
        });

    }
</script>