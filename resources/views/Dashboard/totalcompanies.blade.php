
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Details</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <div class="container-fluid">
            <div class="row">
                @foreach($records as $record)
                <div class="col-md-4">

                        <div class="panel panel-default">
                            <div class="panel-body">
                            {{$record->name ?? ''}}
                            </div>
                            <div class="panel-footer">
                                <i class="fa fa-eye" data-toggle="modal" data-target="#exampleModal" style="margin-left: 15px" ></i>
                                <i class="fa fa-building" style="margin-left: 60px"></i>
                            </div>
                        </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
@extends('Dashboard.modals')
@stop

