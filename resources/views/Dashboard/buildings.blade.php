@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Buildings</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <div class="container-fluid">
            <div class="row">
                @foreach($records as $record)
                    <div class="col-md-4">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                {{$record->name ?? ''}}
                            </div>
                            <div class="panel-footer">
                                <i class="fa fa-eye modalTrigger" data-building="{{$record}}" data-toggle="modal" data-target="#exampleModal" style="margin-left: 15px" ></i>
                                <i class="fa fa-building" style="margin-left: 60px"></i>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
    {{--modal--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Agreement Date:</label>
                                    <input name="description" class="form-control" value="" id="agreement_date" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Land Name:</label>
                                    <input type="text" name="payee_name" class="form-control" value="" id="land_name" required>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Contact Email:</label>
                                    <input type="text" name="payee_name" class="form-control" value="" id="contact_email" required>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">C.N.I.C:</label>
                                    <input type="text" name="description" class="form-control" value="" id="cnic" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Basic Rent:</label>
                                    <input type="text" name="payee_name" class="form-control" value="" id="Basic_rent" required>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Security:</label>
                                    <input type="text" name="payee_name" class="form-control" value="" id="sceurity" required>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">fax:</label>
                                    <input type="text" name="description" class="form-control" value="" id="fax" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Tenure:</label>
                                    <textarea  name="payee_name" class="form-control" value="" id="tenure" required></textarea>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Increment:</label>
                                    <input type="text" name="payee_name" class="form-control" value="" id="increment" required>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    {{--modal end--}}
@stop
@section('javascript')
    <script>
        $('.modalTrigger').on('click',function(){
            var land_name = $(this).data('building').land_name;
            var agreement_name = $(this).data('branch').agreement_name;
            var contact_email = $(this).data('branch').contact_email;
            var contact_number = $(this).data('branch').contact_number;
            var cnic = $(this).data('branch').cnic;
            var basic_rent = $(this).data('branch').basic_rent;
            var security = $(this).data('branch').security;
            var tenure = $(this).data('branch').tenure;
            var increment = $(this).data('branch').increment;
            $('#land_name').val(land_name);
            $('#agreement_name').val(agreement_name);
            $('#contact_email').val(contact_email);
            $('#contact_number').val(contact_number);
            $('#cnic').val(cnic);
            $('#basic_rent').val(basic_rent);
            $('#security').val(security);
            $('#tenure').val(tenure);
            $('#increment').val(increment);

        });
    </script>
@endsection