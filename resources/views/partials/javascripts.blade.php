<script src="{{ url('public/theme') }}/assets/plugins/jquery/jquery.min.js"></script>
<!--script src="https:cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<!--- Datepicker js -->
<script src="{{ url('public/theme') }}/assets/plugins/jquery-ui/ui/widgets/datepicker.js"></script>
<!-- JQueryValidation Plugin Starts -->
<script src="{{ url('public/adminlte/plugins/jqueryvalidation') }}/jquery.validate.min.js"></script>
<!-- JQueryValidation Additional Methods -->
<script src="{{ url('public/adminlte/plugins/jqueryvalidation') }}/additional-methods.min.js"></script>

<script src="{{ url('public/theme') }}/assets/plugins/jquery.flot/jquery.flot.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/jquery.flot/jquery.flot.pie.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/jquery.flot/jquery.flot.resize.js"></script>
<!--- Internal Chart flot js -->
<script src="{{ url('public/theme') }}/assets/js/chart.flot.js"></script>

<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/dataTables.dataTables.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/dataTables.responsive.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/responsive.dataTables.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/jquery.dataTables.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/dataTables.bootstrap4.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/dataTables.buttons.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/buttons.bootstrap4.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/jszip.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/pdfmake.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/vfs_fonts.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/buttons.html5.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/buttons.print.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/buttons.colVis.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/dataTables.responsive.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/datatable/js/responsive.bootstrap4.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/select2/js/select2.min.js"></script>
<!--- Owl Carousel js -->
<script src="{{ url('public/theme') }}/assets/plugins/multislider/multislider.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/owl-carousel/owl.carousel.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/jquery-ui/ui/widgets/datepicker.js"></script>
<!-- datatable js -->
<script src="{{ url('public/theme') }}/assets/js/table-data.js"></script>
<script src="{{ url('public/theme') }}/assets/js/carousel.js"></script>
<script src="{{ url('public/theme') }}/assets/js/custom.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/echart/echart.js"></script>
<script src="{{ url('public/theme') }}/assets/js/echarts.js"></script>


<script src="{{ url('public/theme') }}/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!--- Chart bundle min js -->
<script src="{{ url('public/theme') }}/assets/plugins/chart.js/Chart.bundle.min.js"></script>
<!--- Internal Sampledata js -->
<script src="{{ url('public/theme') }}/assets/js/chart.flot.sampledata.js"></script>
<!--- Index js -->
<script src="{{ url('public/theme') }}/assets/js/index.js"></script>


<!--- Moment js -->
<script src="{{ url('public/theme') }}/assets/plugins/moment/moment.js"></script>

<!--- JQuery sparkline js -->
<script src="{{ url('public/theme') }}/assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

<!--- Perfect-scrollbar js -->
<script src="{{ url('public/theme') }}/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/perfect-scrollbar/p-scroll.js"></script>


<!--- Rating js -->
<script src="{{ url('public/theme') }}/assets/plugins/rating/jquery.rating-stars.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/rating/jquery.barrating.js"></script>

<!--- Custom Scroll bar Js -->
<script src="{{ url('public/theme') }}/assets/plugins/mscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>


<!--- Sidebar js -->
<script src="{{ url('public/theme') }}/assets/plugins/side-menu/sidemenu.js"></script>


<!--- Right-sidebar js -->
<script src="{{ url('public/theme') }}/assets/plugins/sidebar/sidebar.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/sidebar/sidebar-custom.js"></script>

<!--- Eva-icons js -->
<script src="{{ url('public/theme') }}/assets/js/eva-icons.min.js"></script>

<!--- Scripts js -->
<script src="{{ url('public/theme') }}/assets/js/script.js"></script>

<!--- Custom js -->
<script src="{{ url('public/theme') }}/assets/js/custom.js"></script>

<!--- Switcher js -->
<script src="{{ url('public/theme') }}/assets/switcher/js/switcher.js"></script>
<script src="{{ url('public/theme') }}/assets/plugins/raphael/raphael.min.js"></script>
		<script src="{{ url('public/theme') }}/assets/plugins/morris.js/morris.min.js"></script>
		<script src="{{ url('public/theme') }}/assets/js/chart.morris.js"></script>
	

		
@yield('javascript')

<!-- Ziggy Routes Starts -->
@routes
<!-- Ziggy Routes Ends -->