@inject('request', 'Illuminate\Http\Request')

<?php $per = PermissionHelper::getUserPermissions();?>

<!-- Loader -->
<div id="global-loader">
            <img src="{{ url('public/theme') }}/assets/img/loaders/loader-4.svg" class="loader-img" alt="Loader">
        </div>
        <!-- /Loader -->

        <!-- main-sidebar opened -->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <aside class="main-sidebar app-sidebar sidebar-scroll">
            <div class="main-sidebar-header">
                <a class="desktop-logo logo-light active" href="#" style = "text-align: center;
    font-size: 17px;
    margin: 25px;
    font-weight: 500;" class="text-center mx-auto">Roots IVY ERP Network </a>
            </div><!-- /logo -->
            <div class="main-sidebar-loggedin">
                <div class="app-sidebar__user">
                    <div class="dropdown user-pro-body text-center">
                        <div class="user-pic">
                            <img src="{{ url('public/theme') }}/assets/img/faces/6.jpg" alt="user-img" class="rounded-circle mCS_img_loaded">
                        </div>
                        <div class="user-info">
                            <h6 class=" mb-0 text-dark">{{Auth::user()->name}}</h6>
                            <span class="text-muted app-sidebar__user-name text-sm">{{Auth::user()->name}}</span>
                        </div>
                    </div>
                </div>
            </div><!-- /user -->
            <div class="sidebar-navs">
                <ul class="nav  nav-pills-circle">
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings" aria-describedby="tooltip365540">
                        <a class="nav-link text-center m-2">
                            <i class="fe fe-settings"></i>
                        </a>
                    </li>
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Chat">
                        <a class="nav-link text-center m-2">
                            <i class="fe fe-mail"></i>
                        </a>
                    </li>
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Followers">
                        <a class="nav-link text-center m-2">
                            <i class="fe fe-user"></i>
                        </a>
                    </li>
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Logout">
                          
                        <a href="#logout" onclick="$('#logout').submit();"  class="nav-link text-center m-2">
                            <i class="fe fe-power"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="main-sidebar-body">
                <ul class="side-menu ">
                    
                    @if(!Auth::user()->hasRole('GM Finance'))
                    <li class="slide">
                        <a class="side-menu__item" href="{{ url('/dashboard') }}"><i class="side-menu__icon fe fe-airplay"></i><span class="side-menu__label">@lang('global.app_dashboard')</span></a>
                    </li>
                    @endif
                    <li class="slide">
                        <a class="side-menu__item" href="{{ url('/gmdashboard') }}"><i class="side-menu__icon fe fe-airplay"></i><span class="side-menu__label">GM Finance Dashboard</span></a>
                    </li>
             
                    <li class="slide">
                    <a href="#" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fe fe-airplay"></i>
                        <span class="side-menu__label">Tax Management</span>
                     
                    </a>
                        <ul class="slide-menu">
                            @can('erp_laws_manage',$per)
                            <li class="{{ ($request->segment(2) == 'groups' && $request->segment(3) != 'overview') ? 'active active-sub' : '' }}">
                                <a class="slide-item" href="{{ route('tax-law') }}">
                                    <span class="title">Define Laws</span>
                                </a>
                            </li>
                            @endcan
                            <li class="{{ $request->segment(2) == 'taxsettings' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.taxsettings.index') }}">
                                    <span class="title">Define Salary Tax Slabs</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(2) == 'taxsettings' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('rental-tax-settings') }}">
                                    <span class="title">Define Rent Tax Slabs</span>
                                </a>
                            </li>
                     
                            <li class="{{ ($request->segment(2) == 'groups' && $request->segment(3) != 'overview') ? 'active active-sub' : '' }}">
                                <a class="slide-item" href="{{ route('tax-law-rate') }}">
                                    <span class="title">Define Law Rates</span>
                                </a>
                            </li>
                        </ul>
                    </li>
               
                    <li class="slide">
                        <a class="side-menu__item" href="{{ route('vendor-list') }}"><i class="side-menu__icon fe fe-airplay"></i><span class="side-menu__label">Vendor or Payee</span></a>
                    </li>
                    <li class="slide">
                    <a href="{{ route('tax-law') }}" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fe fe-airplay"></i>
                        <span class="side-menu__label">Payment Sheets</span>
                     
                    </a>
                    <ul class="slide-menu">
    <li >
        <a class="slide-item" href="{{ route('bank-payment') }}">
            <span class="title">Payment List</span>
        </a>
    </li>
    <li>
        <a class="slide-item" href="{{ route('bank-payment-sheet-create') }}">
            <span class="title">Bank Payment Sheets</span>
        </a>
    </li>
    <li >
        <a class="slide-item" href="{{ route('cash-payment-sheet-create') }}">
            <span class="title">Cash Payment Sheets</span>
        </a>
    </li>
    <li >
        <a class="slide-item" href="{{ route('rent-payment-sheet-create') }}">
            <span class="title">Rental Payment Sheets</span>
        </a>
    </li>
    <li >
        <a class="slide-item" href="{{ route('payment-sheet-list') }}">
            <span class="title">Pending Payment List</span>
        </a>
    </li>
   
    </ul>

                    </li>




                    @if(in_array('erp_groups_manage',$per) || in_array('erp_ledgers_manage',$per) || in_array('erp_ledgers_manage',$per) || in_array('erp_entries_manage',$per))
                <li class="slide{{ ($request->segment(2) == 'groups' || $request->segment(2) == 'ledgers' || $request->segment(2)=='ledger_tree' || $request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? ' active' : '' }}">
                    <a href="#" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fe fe-airplay"></i>
                        <span class="side-menu__label">Accounts Management</span>
                     
                    </a>
              
                    <ul class="slide-menu">

                        @if(in_array('erp_groups_manage',$per))
                            <li class="{{ ($request->segment(2) == 'groups' && $request->segment(3) != 'overview') ? 'active active-sub' : '' }}">
                                <a class="slide-item" href="{{ route('admin.groups.index') }}">
                                    <span class="title">Groups</span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_ledgers_manage',$per))
                            <li class="{{ $request->segment(2) == 'ledgers' ? 'active active-sub' : '' }}">
                                <a class="slide-item" href="{{ route('admin.ledgers.index') }}">
                                    <span class="title">Ledgers</span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_ledgers_manage',$per))
                            <li class="{{ $request->segment(2) == 'ledger_tree' ? 'active active-sub' : '' }}">
                                <a class="slide-item" href="{{ route('admin.ledger_tree') }}">
                                    <span class="title">Ledger Tree</span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_entries_manage',$per))
                            <li class="{{ ($request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? 'active active-sub' : '' }}">
                                <a class="slide-item" href="{{ route('admin.entries.index') }}">
                                    <span class="title">Journal Entry</span>
                                </a>
                            </li>
                        @endif
               
                       {{-- @if(in_array('erp_entries_manage',$per))
                            <li class="{{ ($request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? 'active active-sub' : '' }}">
                                <a class="slide-item" href="{{ route('admin.duty_seg.index') }}">
                                    <span class="title">Segregate Quatities</span>
                                </a>
                            </li>
                        @endif--}}
                    </ul>
                </li>
                
            @endif
          
             @can('erp_group_report_manage',$per)
            <li class="slide{{ ($request->segment(1) == 'report-group' || $request->segment(1) == 'report-parameter') ? ' active' : '' }}">
                   <a href="#" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fe fe-airplay"></i>
                        <span class="side-menu__label">Group Reports</span>
                     
                    </a>
                    <ul  class="slide-menu">

                        
                        
                        <li class="{{ $request->segment(1) == 'report-group' ? 'active active-sub' : '' }}">
                        <a class="slide-item" href="{{ url('/report-group') }}">
                            <span class="title"> Group Report</span>
                        </a>
                    </li>
                    <li class="{{ $request->segment(1) == 'report-parameter' ? 'active active-sub' : '' }}">
                        <a class="slide-item" href="{{ url('/report-parameter') }}">
                            <span class="title"> Report Parameter</span>
                        </a>
                    </li>
                       {{-- @if(in_array('erp_entries_manage',$per))
                            <li class="{{ ($request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? 'active active-sub' : '' }}">
                                <a class="slide-item" href="{{ route('admin.duty_seg.index') }}">
                                    <span class="title">Segregate Quatities</span>
                                </a>
                            </li>
                        @endif--}}
                    </ul>
                </li>
            @endcan
            <li class="slide ">
                 
                 <a href="#" class="side-menu__item" data-toggle="slide">
                 <i class="side-menu__icon fe fe-grid"></i>
                     <span class="side-menu__label">Asset Management</span>
                 
                 </a>
                 <ul class="slide-menu">
                     <li class="sub-slide{{ $request->segment(3) == 'ledger' ? 'active active-sub' : '' }}">
                         <a class="sub-side-menu__item" href="{{ url('/admin/asset_type') }}">
                             <span class="title"> Create Asset Type</span>
                         </a>
                     </li>
                     <li class="sub-slide{{ $request->segment(3) == 'ledger' ? 'active active-sub' : '' }}">
                         <a class="sub-side-menu__item" href="{{ url('/admin/asset') }}">
                             <span class="title">Create Asset</span>
                         </a>
                     </li>
                     <li class="sub-slide{{ $request->segment(3) == 'ledger' ? 'active active-sub' : '' }}">
                         <a class="sub-side-menu__item" href="{{ url('/return-policy') }}">
                             <span class="title">Asset Return Policy</span>
                         </a>
                     </li>
                     <li class="sub-slide{{ $request->segment(3) == 'ledger' ? 'active active-sub' : '' }}">
                         <a class="sub-side-menu__item" href="{{ url('asset-transaction') }}">
                             <span class="title"> Asset Transaction</span>
                         </a>
                     </li>
                     <li class="sub-slide{{ $request->segment(3) == 'ledger' ? 'active active-sub' : '' }}">
                         <a class="sub-side-menu__item" href="{{ url('asset-assign') }}">
                             <span class="title"> Asset Assign</span>
                         </a>
                     </li>
                 </ul>
                 </li>



                 <li class="slide ">
                 <a href="#" class="side-menu__item" data-toggle="slide">
                 <i class="side-menu__icon fe fe-grid"></i>
                     <span class="side-menu__label">Land Management</span>
                                         </a>

                                 <ul class="slide-menu">
                                 <li class="sub-slide{{ $request->segment(3) == 'trial_balance' ? 'active active-sub' : '' }}">
                                         <a class="sub-side-menu__item" href="{{ url('/payee-list') }}">
                                             <span class="title"> LandLord Details</span>
                                         </a>
                                     </li>
                                      <li class="sub-slide{{ $request->segment(3) == 'trial_balance' ? 'active active-sub' : '' }}">
                                         <a class="sub-side-menu__item" href="{{ url('/building-list') }}">
                                             <span class="title"> Building List</span>
                                         </a>
                                     </li>
                                     <li class="sub-slide{{ $request->segment(3) == 'trial_balance' ? 'active active-sub' : '' }}">
                                         <a class="sub-side-menu__item" href="{{ url('utility-bills') }}">
                                             <span class="title">Utilities Bills</span>
                                         </a>
                                     </li>
                      
                                 </ul>
                    
             </li>

   <!-- HR Module Starts -->
   @include('hrm.partials.sidebar_hrm')
        <!-- HR Module Ends -->
            {{--@if(in_array('erp_groups_manage',$per))--}}
            {{--<li class="treeview{{ ($request->segment(2) == 'groups' || $request->segment(2) == 'ledgers' || $request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? ' active' : '' }}">--}}
            {{--<a href="#">--}}
            {{--<i class="fa fa-book"></i>--}}
            {{--<span class="title">HR Reports</span>--}}
            {{--<span class="pull-right-container">--}}
            {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
            {{--</a>--}}
            {{--<ul class="treeview-menu">--}}

            {{--@if(in_array('erp_groups_manage',$per))--}}
            {{--<li class="{{ ($request->segment(2) == 'groups' && $request->segment(3) != 'overview') ? 'active active-sub' : '' }}">--}}
            {{--<a href="{{ route('hrm.attendance.report') }}">--}}
            {{--<span class="title">Attandance Report</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--@endif--}}
            {{--@if(in_array('erp_ledgers_manage',$per))--}}
            {{--<li class="{{ $request->segment(2) == 'ledgers' ? 'active active-sub' : '' }}">--}}
            {{--<a href="{{ route('hrm.leave.report') }}">--}}
            {{--<span class="title">Leave Report</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--@endif--}}
            {{--@if(in_array('erp_ledgers_manage',$per))--}}
            {{--<li class="{{ $request->segment(2) == 'ledgers' ? 'active active-sub' : '' }}">--}}
            {{--<a href="{{ route('hrm.leave_performance.report') }}">--}}
            {{--<span class="title">Leave Performance Report</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--@endif--}}

            {{--</ul>--}}
            {{--</li>--}}
            {{--@endif--}}
            @can('erp_users_management_dfit',$per)
                <li class="slide {{ ($request->segment(2) == 'permissions' || $request->segment(2) == 'roles' || $request->segment(2) == 'users') ? 'active' : '' }}">
                      <a href="#" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">@lang('global.user-management.title')</span>
                     
                    </a>
                    
                 <ul class="slide-menu">
                        <li class="sub-slide{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                            <a class="sub-side-menu__item" href="{{ route('admin.permissions.index') }}">
                                {{--<i class="fa fa-briefcase"></i>--}}
                                <span class="title">Permissions</span>
                            </a>
                        </li>
                        <li class="sub-slide{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                            <a class="sub-side-menu__item" href="{{ route('admin.roles.index') }}">
                                {{--<i class="fa fa-briefcase"></i>--}}
                                <span class="title">Roles</span>
                            </a>
                        </li>
                        <li class="sub-slide{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                            <a class="sub-side-menu__item" href="{{ route('admin.users.index') }}">
                                {{--<i class="fa fa-user"></i>--}}
                                <span class="title">Users</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan

             @can('erp_school_setting_manage',$per)

            <li class="slide  {{ ($request->segment(2) == 'classes' || $request->segment(2) == 'boards' || $request->segment(2) == 'assign-board-program' || $request->segment(2) == 'assign-branch-program' || $request->segment(2) == 'assign-class-section' || $request->segment(2) == 'section' || $request->segment(2) == 'program-group' || $request->segment(2) == 'class-group' || $request->segment(2) == 'multiple-courses-groups' || $request->segment(2) == 'course-group' ||  $request->segment(2) == 'academics-groups' || $request->segment(2) == 'academics-groups-courses' ||  $request->segment(2) == 'boardprogramindex' ||  $request->segment(2) == 'courses' ||  $request->segment(2) == 'assign-board-program-classes') ? 'active' : '' }}"   >


            <li class="slide {{ ($request->segment(2) == 'classes' || $request->segment(2) == 'boards' || $request->segment(2) == 'assign-board-program' || $request->segment(2) == 'assign-branch-program' || $request->segment(2) == 'assign-class-section' || $request->segment(2) == 'section' || $request->segment(2) == 'program-group' || $request->segment(2) == 'class-group' || $request->segment(2) == 'multiple-courses-groups' || $request->segment(2) == 'course-group' ||  $request->segment(2) == 'academics-groups' || $request->segment(2) == 'academics-groups-courses' ||  $request->segment(2) == 'boardprogramindex' ||  $request->segment(2) == 'courses' ||  $request->segment(2) == 'assign-course-lab' ||  $request->segment(2) == 'assign-board-program-classes' ||  $request->segment(2) == 'labs') ? 'active' : '' }}"   >
                

                    <a href="#" class="side-menu__item" data-toggle="slide">
                        <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">School Management</span>
                    </a>
                 
      
                    <ul class="slide-menu">
                        <li class="{{ $request->segment(2) == 'boards' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.boards.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Programs</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'boardprogramindex' ? 'active' : '' }}">
                         
                                <a  class="sub-side-menu__item" href="{{  route('admin.boardprogramindex') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Board Programs</span>
                            </a>
                        </li>
                        {{--<li class="{{ $request->segment(2) == 'assign-branch-program' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.branch-program') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Assign Branch Programs</span>
                            </a>
                        </li>--}}
                        <li class="{{ $request->segment(2) == 'classes' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.classes.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Classes</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'assign-board-program-classes' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.boards-program-classes') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Assign Board Program Classes</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'section' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.section.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Sections</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'assign-class-section' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.classes-assign-class-section') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Assign Class Sections</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'courses' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.courses.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Courses</span>
                            </a>
                        </li>
                        
                        <!--------------------------------Assign Classes to teacher------------------------------->

                        {{--<li class="{{ $request->segment(2) == 'courses' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.courses.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Course</span>
                            </a>
                        </li>--}}
                        <li  class="{{ $request->segment(2) == 'labs' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.labs.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Labs</span>
                            </a>
                        </li>
                        <li  class="{{ $request->segment(2) == 'assign-course-lab' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.course-lab') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Assign Course Lab</span>
                            </a>
                        </li>



                      



                        <!--------------------------------Assign Classes to teacher end--------------------------->






                        {{--<li class="{{ $request->segment(2) == 'program-group' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.program-group.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Program Group</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'course-group' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.course-group.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Course Group</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'class-group' ? 'active' : '' }}">
                            <a class="sub-side-menu__item" href="{{  route('admin.class-group.index') }}">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Class Group</span>
                            </a>
                        </li>--}}
                        <li class="{{ ($request->segment(2) == 'academics-groups' || $request->segment(2) == 'academics-groups-courses' || $request->segment(3) == 'index' ||  $request->segment(2) == 'class-group-courses') ? 'active' : '' }}">

                          
                                    <a class="sub-side-menu__item" href="{{  route('admin.academics-groups.index') }}">
                                      
                                        <span class="title">Academic Group</span>
                                    </a>
                              
                        </li>

                    </ul>
                    </li>
           @endcan
           <!---------------------------training--------------------------->
            @can('erp_session_manage',$per)

                <li class="slide  {{ ($request->segment(2) == 'session' || $request->segment(1) == 'active-session-section'   || $request->segment(3) == 'session' ||$request->segment(2) == 'candidate-hire') ? 'active' : '' }}">
                    <a href="#" class="side-menu__item" data-toggle="slide">
                        <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Session Management</span>
                     
                    </a>

                    <ul class="slide-menu">
                        <li class="{{ $request->segment(2) == 'session' ? 'active active-sub' : '' }}">
                            <a class="sub-side-menu__item" href="{{ route('admin.session.index') }}">
                                
                                <span class="title">Session</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(3) == 'session' ? 'active active-sub' : '' }}">
                            <a class="sub-side-menu__item" href="{{ route('admin.active-session.index') }}">
                             
                                <span class="title">Active Session</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'active-session-section' ? 'active active-sub' : '' }}">
                            <a class="sub-side-menu__item" href="{{ url('active-session-section') }}">
                               
                                <span class="title">Active Session Section</span>
                            </a>
                        </li>
                        @can('erp_academic_years_manage',$per)
                            <li class="{{ $request->segment(2) == 'academic_years' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.academic_year.index') }}">
                   
                                    <span class="title">Academic Year</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(2) == 'academic_years' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.term.index') }}">
                             
                                    <span class="title">Term</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('erp_employee_management_new',$per)
                <li class="slide  {{ ($request->segment(2) == 'home' || $request->segment(1) == 'approve-card-request'   || $request->segment(2) == 'staff') ? 'active' : '' }}">
                    <a href="#" class="side-menu__item" data-toggle="slide">
                         <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Employee  Management</span>
                     
                    </a>

                    <ul class="slide-menu">
                         <li class="slide{{ $request->segment(2) == 'staff' ? 'active active-sub' : '' }}">
                                <a href="{{  route('admin.staff.index') }}"  class="sub-side-menu__item">
                                    <span class="title">Manage Staff</span>
                                 
                                </a>
                        </li>
                        <li class="slide{{ $request->segment(1) == 'approve-card-request' ? 'active active-sub' : '' }}">
                            <a href="{{url('approve-card-request')}}" class="sub-side-menu__item">
                                <span class="title">Id Card Requests</span>
                            </a>
                        </li>
                        <li class="slide{{ $request->segment(2) == 'bank-account-request' ? 'active active-sub' : '' }}">
                            <a href="#" class="sub-side-menu__item">
                                <span class="title">Bank Account Request</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan

            @can('erp_leave_management',$per)
                <li class="slide  {{ ($request->segment(2) == 'home' || $request->segment(1) == 'approve-card-request'   || $request->segment(2) == 'staff') ? 'active' : '' }}">
                    <a href="#" class="side-menu__item" data-toggle="slide">
                         <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Leave Management</span>
                     
                    </a>

                    <ul class="slide-menu">
                         <li class="slide{{ $request->segment(2) == 'staff' ? 'active active-sub' : '' }}">
                                <a href="{{  route('admin.leave_request.list') }}"  class="sub-side-menu__item">
                                    <span class="title">Leave Requests</span>
                                 
                                </a>
                        </li>
                        <li class="slide{{ $request->segment(1) == 'approve-card-request' ? 'active active-sub' : '' }}">
                            <a href="{{  route('admin.leave_management.index') }}" class="sub-side-menu__item">
                                <span class="title">Apply Leave</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan

            @can('erp_exit_management',$per)
                <li class="slide  {{ ($request->segment(2) == 'home' || $request->segment(1) == 'approve-card-request'   || $request->segment(2) == 'staff') ? 'active' : '' }}">
                    <a href="#" class="side-menu__item" data-toggle="slide">
                         <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Exit  Management</span>
                     
                    </a>

                    <ul class="slide-menu">
                         <li class="slide{{ $request->segment(2) == 'staff' ? 'active active-sub' : '' }}">
                                <a href="{{route('terminated_staff_list')}}"  class="sub-side-menu__item">
                                    <span class="title">Terminated Staff List</span>
                                 
                                </a>
                        </li>
                        <li class="slide{{ $request->segment(1) == 'approve-card-request' ? 'active active-sub' : '' }}">
                            <a href="{{route('staff_status_list')}}" class="sub-side-menu__item">
                                <span class="title">SOS Staff List</span>
                            </a>
                        </li>
                        <li class="slide{{ $request->segment(2) == 'bank-account-request' ? 'active active-sub' : '' }}">
                            <a href="{{route('my-resign-request-list')}}" class="sub-side-menu__item">
                                <span class="title">My Resign Request</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan

            @can('erp_agenda_manage',$per)
                <li class="slide  {{ ($request->segment(2) == 'home' || $request->segment(1) == 'approve-card-request'   || $request->segment(2) == 'staff') ? 'active' : '' }}">
                    <a href="#" class="side-menu__item" data-toggle="slide">
                         <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Agenda  Management</span>
                    </a>
                    <ul class="slide-menu">
                         <li class="slide{{ $request->segment(2) == 'staff' ? 'active active-sub' : '' }}">
                                <a href="{{ url('/agenda') }}"  class="sub-side-menu__item">
                                    <span class="title">Manage Agenda</span>
                                 
                                </a>
                        </li>
                        <li class="slide{{ $request->segment(1) == 'approve-card-request' ? 'active active-sub' : '' }}">
                            <a href="{{ url('agenda-list') }}" class="sub-side-menu__item">
                                <span class="title">Agenda List</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan
             @can('erp_training_manage',$per)
                <li class="slide  {{ ($request->segment(2) == 'home' || $request->segment(1) == 'approve-card-request'   || $request->segment(2) == 'staff') ? 'active' : '' }}">
                    <a href="#" class="side-menu__item" data-toggle="slide">
                         <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Training  Management</span>
                    </a>
                    <ul class="slide-menu">
                         <li class="slide{{ $request->segment(2) == 'staff' ? 'active active-sub' : '' }}">
                                <a href="{{route('admin.training.index')}}"  class="sub-side-menu__item">
                                    <span class="title">Training</span>
                                 
                                </a>
                        </li>
                        <li class="slide{{ $request->segment(1) == 'approve-card-request' ? 'active active-sub' : '' }}">
                            <a href="{{ url('/admin/training_request') }}" class="sub-side-menu__item">
                                <span class="title">My Trainig Requests</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan





        @if(Auth::user()->hasRole('teacher'))
         <li class="{{ $request->segment(2) == 'all-teachers' ? 'active' : '' }}">
               <a href="{{  route('admin.attendance_payroll.index') }}" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Teacher Attendance</span>
                     
                    </a>
       
        </li>
        @endif
            <!-- -------------TimeTable Start--------------------------->
        @can('erp_time_table',$per)
            <li class="slide {{ ($request->segment(2) == 'timetable' || $request->segment(1) == 'role' ||$request->segment(2) == 'timetable-hire') ? 'active' : '' }}">
                   <a href="{{  route('admin.leave_management.index') }}" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fa fa-calendar"></i>
                        <span class="side-menu__label">Timetable</span>
                     
                    </a>
                
                <ul class="slide-menu">
                    @can('erp_time_table_category_manage',$per)
                     <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                         <a class="sub-side-menu__item" href="{{  route('admin.timetable_category.index') }}">
                             
                                <span class="title">Manage Timetable Category</span>
                            </a>
                      
                       
                    </li>
                    @endcan

                    @can('erp_timetable_manage',$per)
                     <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                             <a class="sub-side-menu__item" href="{{  route('admin.timetable.index') }}">
                             
                                <span class="title">Manage Timetable</span>
                            </a>
                    </li>
                    @endcan

                    @can('erp_class_time_table_manage',$per)
                     <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                         
                           <a class="sub-side-menu__item" href="{{  route('admin.class_timetable.index') }}">
                             
                            <span class="title">Manage Class Timetable</span>
                        </a>
                    </li>
                    @endcan

                    @can('erp_class_time_table_manage',$per)
                     <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                        <a class="sub-side-menu__item" href="{{  route('admin.event_timetable.index') }}">
                     
                            <span class="title">Manage Event Timetable</span>
                        </a>
                    </li>
                    @endcan

                    @can('erp_class_time_table_manage',$per)
                     <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                        <a class="sub-side-menu__item" href="{{  route('admin.lab_timetable.index') }}">
                     
                            <span class="title">Manage Lab Timetable</span>
                        </a>
                    </li>
                    @endcan

                    @can('erp_class_time_table_manage',$per)
                    <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                        <a class="sub-side-menu__item" href="{{  route('admin.exam_timetable.index') }}">
                           
                            <span class="title">Manage Exam Timetable</span>
                        </a>
                    </li>
                    @endcan
                </ul>

            </li>
            <!-- -------------TimeTable END--------------------------->
        @endcan

            @can('erp_roles_manage',$per)

                <li class="slide {{ ($request->segment(2) == 'roles' || $request->segment(1) == 'role' ||$request->segment(2) == 'candidate-hire') ? 'active' : '' }}">
                  <a href="#" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fa fa-calendar"></i>
                        <span class="side-menu__label">Roles And Permission</span>
                        </a>
                        

                    <ul class="slide-menu">
                        <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.roles.index') }}">
                                <i class="fa fa-briefcase"></i>
                                <span class="title">ERP</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'role' ? 'active active-sub' : '' }}">
                            <a href="{{ route('role') }}">
                                <i class="fa fa-briefcase"></i>
                                <span class="title">Academics</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan


        @if( in_array('erp_settings_manage',$per) || in_array('erp_financial_years_manage',$per) ||
                in_array('erp_account_types_manage',$per) ||
                in_array('erp_branches_manage',$per)|| in_array('erp_regions_manage',$per) ||
                in_array('erp_departments_manage',$per) || in_array('erp_dealer_manage',$per) ||
                in_array('erp_employees_manage',$per) || in_array('erp_cities_manage',$per) ||
                in_array('erp_countries_manage',$per) || in_array('erp_fee_section_manage',$per) ||
                in_array('erp_fee_head_manage',$per) || in_array('erp_currencies_manage',$per) ||
                in_array('erp_customers_manage',$per) || in_array('erp_payment_methods_manage',$per) ||
                in_array('erp_banks_manage',$per ) || in_array('erp_companies_manage', $per) ||
                in_array('erp_suppliers_manage',$per) )
                <li class="slide {{ ($request->segment(2) == 'settings' || $request->segment(2) == 'financial_years' ||
                $request->segment(2) == 'account_types' || $request->segment(2) == 'companies' ||
                $request->segment(2) == 'branches' || $request->segment(2) == 'departments' ||
                $request->segment(2) == 'employees' || $request->segment(2) == 'fee_head' ||
                $request->segment(2) == 'customers' || $request->segment(2) == 'payment_methods' ||
                $request->segment(2) == 'banks' || $request->segment(2) == 'city' ||
                $request->segment(2) == 'fee_section' || $request->segment(2) == 'country' ||
                $request->segment(2) == 'currency' ||
                $request->segment(2) == 'suppliers') ? 'active' : '' }}">
                @can('erp_countries_manage',$per)
                    <a href="#" class="side-menu__item" data-toggle="slide">
                    <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Administrator</span>
                     
                    </a>
                @endcan
                    <ul class="slide-menu">
                        @can('erp_companies_manage',$per)
                            <li class="{{ $request->segment(2) == 'companies' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.companies.index') }}">
                                    <span class="title">Companies</span>
                                </a>
                            </li>
                        @endcan



{{--                        @if(in_array('erp_financial_years_manage',$per))--}}
{{--                            <li class="{{ $request->segment(2) == 'financial_years' ? 'active active-sub' : '' }}">--}}
{{--                                <a class="sub-side-menu__item" href="{{ route('admin.financial_years.index') }}">--}}
{{--                                    <span class="title">Financial Years</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endif--}}
                        @if(in_array('erp_academic_years_manage',$per))
                            <li class="{{ $request->segment(2) == 'academic_years' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.academic_year.index') }}">
                      
                                    <span class="title">Academic Year</span>
                                </a>
                            </li>
                        @endif
                   

                        @if(in_array('erp_departments_manage',$per))
                            <li class="{{ $request->segment(2) == 'departments' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.departments.index') }}">
                                    <span class="title">Departments</span>
                                </a>
                            </li>
                        @endif
                        {{-- @if(in_array('erp_countries_manage',$per))
                            <li class="{{ $request->segment(2) == 'country' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.country.index') }}">
                                    <span class="title">Country</span>
                                </a>
                            </li>
                        @endif --}}
                        @if(in_array('erp_regions_manage',$per))
                            <li class="{{ $request->segment(2) == 'regions' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.regions.index') }}">
                                    <span class="title">Regions</span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_cities_manage',$per))
                            <li class="{{ $request->segment(2) == 'city' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.city.index') }}">
                                    <span class="title">City</span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_branches_manage',$per))
                            {{--<li class="{{ $request->segment(2) == 'branches' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.branches.index') }}">
                                    <span class="title">Branches</span>
                                </a>
                            </li>--}}
                        @endif
                        @if(in_array('erp_fee_section_manage',$per))
                            <li  class="{{ $request->segment(2) == 'fee_section' ? 'active active-sub' : '' }}">
                                <a  class="sub-side-menu__item" href="{{ route('admin.fee_section.index') }}">
                                    <span class="title">Fee Section</span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_fee_head_manage',$per))
                            <li class="{{ $request->segment(2) == 'fee_head' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.fee_head.index') }}">
                                    <span class="title">Fee Head</span>
                                </a>
                            </li>
                        @endif
                        {{--@if(in_array('erp_currencies_manage',$per))
                            <li class="{{ $request->segment(2) == 'currency' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.currency.index') }}">
                                    <span class="title">Currencies</span>
                                </a>
                            </li>
                        @endif--}}

                        @if(in_array('erp_customers_manage',$per))
                            <li class="{{ $request->segment(2) == 'customers' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.customers.index') }}">
                                    <span class="title">Customers</span>
                                </a>
                            </li>
                        @endif


                        @if(in_array('erp_banks_manage',$per))
                            <li class="{{ $request->segment(2) == 'banks' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.banks.index') }}">
                                    <span class="title">Banks</span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_banksdeatil_manage',$per))
                            <li class="{{ $request->segment(2) == 'bank_detail' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.bank_detail.index') }}">
                                    <span class="title">Banks Detail</span>
                                </a>
                            </li>
                        @endif

                        @if(in_array('erp_suppliers_manage',$per))
                            <li class="{{ $request->segment(2) == 'suppliers' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('admin.suppliers.index') }}">
                                    <span class="title"> Suppliers </span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            <li class="slide {{ $request->segment(3) == 'ledger' ||
            $request->segment(3) == 'trial_balance' || $request->segment(3) == 'expense_summary' ||
              $request->segment(3) == 'profit_loss' || $request->segment(3) == 'balance_sheet' ||
               $request->segment(3)=='balance_sheet_tree' ? 'active' : '' }}">
               <a href="#" class="side-menu__item" data-toggle="slide">
                    <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Loan Management</span>
                     
                    </a>
         
               
                    <ul class="slide-menu">
                    <li class="{{ $request->segment(2) == 'taxsettings' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('loan-list') }}">
                                    <span class="title">Loan Types</span>
                                </a>
                            </li>
                            <li class="{{ $request->segment(2) == 'taxsettings' ? 'active active-sub' : '' }}">
                                <a class="sub-side-menu__item" href="{{ route('loan-installment-list') }}">
                                    <span class="title">Loan Installment Plans</span>
                                </a>
                            </li>
                        <li class="sub-slide {{ $request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : '' }}">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Employee Loans</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class="sub-slide-menu">
                             
                                <li class="{{ $request->segment(3) == 'expense_summary' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.expense_summary') }}">
                                        <span class="title">Request For Loan</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'profit_loss' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.profit_loss') }}">
                                        <span class="title">Approved Loan Requests</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'profit_loss' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.profit_loss_cmp') }}">
                                        <span class="title">Rejected Loan Requests</span>
                                    </a>
                                </li> 
                              

                                </ul>
                            </li>
                            <li class="sub-slide {{ $request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : '' }}">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Bank Loans</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class = "sub-slide-menu">
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('security-refund-register') }}">
                                        <span class="title">Request For Loan</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('recon-with-fee-department') }}">
                                        <span class="title">Approved Loan Requests</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('fee-recover-report') }}">
                                        <span class="title">Rejected Loan Request</span>
                                    </a>
                                </li>
                              
                                
                                </ul>


                                </li>
                        
                
                </ul>



            </li>







            @if(in_array('erp_report_manage',$per))
            <li class="slide {{ $request->segment(3) == 'ledger' ||
            $request->segment(3) == 'trial_balance' || $request->segment(3) == 'expense_summary' ||
              $request->segment(3) == 'profit_loss' || $request->segment(3) == 'balance_sheet' ||
               $request->segment(3)=='balance_sheet_tree' ? 'active' : '' }}">
               <a href="#" class="side-menu__item" data-toggle="slide">
                    <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Reports</span>
                     
                    </a>
                    @if(in_array('erp_account_reports_manage', $per))
                        
             
               
                    <ul class="slide-menu">
                      
                        <li class="sub-slide {{ $request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : '' }}">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">A/C Financial Reports</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class="sub-slide-menu">
                                <li class="{{ $request->segment(3) == 'ledger' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.ledger') }}">
                                        <span class="title"> Ledger Report</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'trial_balance' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.trial_balance') }}">
                                        <span class="title"> Trial Balance</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'expense_summary' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.expense_summary') }}">
                                        <span class="title"> Expense Summary</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'profit_loss' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.profit_loss') }}">
                                        <span class="title"> Profit & Loss Report</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'profit_loss' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.profit_loss_cmp') }}">
                                        <span class="title"> Comparative P&L Report</span>
                                    </a>
                                </li> 
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.balance_sheet') }}">
                                        <span class="title"> Balance Sheet Report </span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(1) == 'report_eobi' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="">
                                        <span class="title"> Eobi report</span>
                                    </a>
                                </li>

                                </ul>
                            </li>
                            <li class="sub-slide {{ $request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : '' }}">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Fee Mis Reconcilation</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class = "sub-slide-menu">
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('security-refund-register') }}">
                                        <span class="title">Fee - Security Refundable Register </span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('recon-with-fee-department') }}">
                                        <span class="title">Fee - 236i Recon Fee Department </span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('fee-recover-report') }}">
                                        <span class="title">Fee - Recovery Report</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('fee-daily-outstanding-report') }}">
                                        <span class="title">Fee - Daily Outstading Report</span>
                                    </a>
                                </li>
                                <li class="{{ $request->segment(3) == 'balance_sheet' ? 'active active-sub' : '' }}">
                                    <a class="sub-slide-item" href="{{ route('admin.account_reports.balance_sheet') }}">
                                        <span class="title">Fee - Monthly Report</span>
                                    </a>
                                </li>
                                
                                </ul>


                                </li>
                                @endif
                
                </ul>



            </li>
            @endif

            @can('erp_ceo_dashboard',$per)
            {{--<li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/dashboard') }}">
                    <i class="fa fa-bar-chart"></i>
                    <span class="title">CEO Dashboard</span>
                </a>
            </li>--}}
            @endcan
            @if(Auth::user()->hasRole('HR_head') || Auth::user()->hasRole('quality assurance'))
            <li class="slide {{ $request->segment(2) == 'staff_performance_detail' ? 'active active-sub' : '' }}">
                  <a href="{{ route('staff-performance.detail') }}"  class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Teacher Performance List</span>
                     
                    </a>
            </li>
            @endif
            @if(Auth::user()->hasRole('sr.coordinator'))
            <li class="{{ $request->segment(2) == 'staff_performance_detail' ? 'active active-sub' : '' }}">
                   <a href="{{ route('staff-performance.index') }}"  class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Manage Teacher Performance</span>
                     
                    </a>
             
            </li>
            @endif
            
            {{--@if(!Auth::user()->hasRole('GM Finance'))--}}
            {{--<li class="slide {{ $request->segment(2) == 'resign_request' ? 'active active-sub' : '' }}">
                 <a href="{{route('my-resign-request-list')}}"  class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">My Resign Request</span>
                     
                    </a>
            
            </li>--}}
            {{--@endif--}}

           

<script type="text/javascript">
    
    setInterval(function(){
        $.ajax({url: "http://rootsschool.erpnetroots.com/api/count_leave_request_notification", success: function(result){
           console.log(result);
           $("#leave_count").text(result)
        }});
    },9000)
</script>

            {{--@can('erp_training_manage',$per)--}}
            {{--<li class="slide {{ $request->segment(3) == 'ledger' ? 'active active-sub' : '' }}">
                   <a href="{{ url('/admin/training_request') }}"  class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">My Trainig Requests</span>
                     
                    </a>
                
            </li>
            <li class="slide{{ $request->segment(2) == 'training' ? 'active active-sub' : '' }}">
                            <a href="{{route('admin.training.index')}}" class="side-menu__item">
                                <i class="side-menu__icon fa fa-users"></i>
                                <span class="side-menu__label">Training</span>
                            </a>
            </li>--}}
            {{--@endcan--}}
            <!-- Inventory Section -->
            @if(in_array('erp_stockitems_manage',$per) || in_array('erp_stockcategory_manage',$per))
                <li class="treeview {{ ($request->segment(2) == 'stockitems' ||
                 $request->segment(2) == 'stockcategory'  || $request->segment(2) == 'inventorytransfer' ||
                 $request->segment(2) == 'goodsissue' ||
                  $request->segment(2) == 'fix_assets'
                 ? ' active' : '' ) }}">
                    <a href="#">
                        <i class="fa fa-tasks"></i>
                        <span class="title">Inventory Manager</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(in_array('erp_stockitems_manage',$per))
                            <li class="{{ $request->segment(2) == 'stockitems' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.stockitems.index') }}">
                                    <i class="fa fa-archive"></i>
                                    <span class="title">
                                        Stock Items
                                    </span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_inventorytransfer_manage',$per))
                            <li class="{{ $request->segment(2) == 'inventorytransfer' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.inventorytransfer.index') }}">
                                    <i class="fa fa-tags"></i>
                                    <span class="title">
                                Inventory Transfer
                                </span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_inventorytransfer_manage',$per))
                            <li class="{{ $request->segment(2) == 'inventorytransfer' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.stockitems.report') }}">
                                    <i class="fa fa-tags"></i>
                                    <span class="title">
                                Inventory Report
                                </span>
                                </a>
                            </li>
                        @endif
                        @if(in_array('erp_goodsissue_manage',$per))
                            <li class="{{ $request->segment(2) == 'goodsissue' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.goodsissue.index') }}">
                                    <i class="fa fa-tags"></i>
                                    <span class="title">
                                Goods Issue
                                </span>
                                </a>
                            </li>
                        @endif

                        {{--@if(in_array('erp_stockcategory_manage',$per))--}}
                        {{--<li class="{{ $request->segment(2) == 'goodsreceipt' ? 'active active-sub' : '' }}">--}}
                        {{--<a href="{{ route('admin.goodsreceipt.index') }}">--}}
                        {{--<i class="fa fa-tags"></i>--}}
                        {{--<span class="title">--}}
                        {{--Goods Receipt--}}
                        {{--</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--@endif--}}

                        @if(in_array('erp_fixedasset_manage',$per))
                            <li class="{{ $request->segment(2) == 'fix_assets' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.fix_assets.index') }}">
                                    <i class="fa fa-archive"></i>
                                    <span class="title">
                                    Fixed Assets
                                    </span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
                @can("erp_hr_manage")

            <li class="slide {{ $request->segment(2) == 'training' || $request->segment(2)== 'resign' ? 'active': ''}}">
                  <a href="#"  class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label"> HR</span>
                     
                    </a>

        </li>
                <li class="slide {{ $request->segment(2) == 'all-teachers' ? 'active' : '' }}">
                       <a href="{{  route('admin.all-teachers.index') }}"  class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                             <span class="side-menu__label">Teachers</span>
                     
                    </a>
                </li>
                
                        
                  

                   

                    <li class="slide {{ $request->segment(2) == 'resign' ? 'active active-sub' : '' }}">
                        <a href="{{route('admin.resign.index')}}" class="side-menu__item">
                            <i class="side-menu__icon fa fa-users"></i>
                            <span class="side-menu__label">Resign</span>
                        </a>
                    </li>
                        
                  
                
                    
            
           @endcan




           @can('erp_recruitment_manage',$per)

                <li class="slide {{ ($request->segment(3) == 'job-request' || $request->segment(2) == 'candidate-hired' || $request->segment(2) == 'schedule' || $request->segment(2) == 'candidate-hire' || $request->segment(3) == 'job-post' || $request->segment(1) == 'candidate-form') ? 'active' : '' }}">
                <a href="#" class="side-menu__item" data-toggle="slide">
                    <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Recuritment</span>
                     
                    </a>

                        <ul class="slide-menu">
                        <li class="sub-slide{{ $request->segment(3) == 'job-request' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.recruitment.job-request') }}" class="sub-side-menu__item">
                                {{--<i class="fa fa-briefcase"></i>--}}
                                <span class="sub-side-menu__label">Employee Require Request</span>
                            </a>
                        </li>

                        @can('erp_job_post_create',$per)


                    
                        <li class="sub-slide{{ $request->segment(3) == 'job-post' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.recruitment.job-post') }}" class="sub-side-menu__item">
                                {{--<i class="fa fa-briefcase"></i>--}}
                                <span class="sub-side-menu__label">Job Post</span>
                            </a>
                        </li>
                         
                    
                        <li class="sub-slide{{ $request->segment(1) == 'candidate-form' ? 'active active-sub' : '' }}">
                            <a href="{{ route('candidate-form.index') }}" class="sub-side-menu__item">
                                {{--<i class="fa fa-user"></i>--}}
                                <span class="sub-side-menu__label">Candidate Applications</span>
                            </a>
                        </li>
                        <li class="sub-slide{{ $request->segment(2) == 'schedule' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.schedule.index') }}" class="sub-side-menu__item">
                                {{--<i class="fa fa-user"></i>--}}
                                <span class="sub-side-menu__label">Scheduled Interviews</span>
                            </a>
                        </li>
                        <li class="sub-slide{{ $request->segment(2) == 'candidate-hired' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.candidate-hired') }}" class="sub-side-menu__item">
                                {{--<i class="fa fa-user"></i>--}}
                                <span class="sub-side-menu__label">Hire Candidates</span>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>

            @endcan

            

<!---Edited \By Amir-->

<!-- 
                <li class="treeview {{ ($request->segment(2) == 'all-teachers' || $request->segment(3) == 'session' ||$request->segment(2) == 'candidate-hire') ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span class="title">Payroll</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ $request->segment(2) == 'all-teachers' ? 'active' : '' }}">
                            <a href="{{  route('admin.attendance_payroll.index') }}">
                                <i class=""></i>
                                <span class="title">Teacher Attendance</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(3) == 'session' ? 'active' : '' }}">
                            <a href="{{  route('admin.leave_management.index') }}">
                                <i class=""></i>
                                <span class="title">Apply Leave</span>
                            </a>
                        </li>  

                    </ul>
                </li> -->
                
            @can('erp_payroll_manage',$per)
                 <li class="slide {{ ($request->segment(2) == 'all-teachers' || $request->segment(3) == 'session' ||$request->segment(2) == 'candidate-hire') ? 'active' : '' }}">
                    <a href="#" class="side-menu__item" data-toggle="slide">
                          <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Payroll</span>
                     
                    </a>
                
                        <ul class="slide-menu">
                       
                        <li class="sub-slide{{ $request->segment(2) == 'all-teachers' ? 'active' : '' }}">
                            <a href="{{  route('admin.salary_payroll.index') }}" class="sub-side-menu__item">
                              
                                <span class="sub-side-menu__label">Employee Salary</span>
                            </a>
                        </li> 

                    </ul>
                </li>
            @endcan
            @can('erp_salary_management',$per)
                <li class="slide{{ ($request->segment(1) == 'loan' || $request->segment(1) == 'eobi' || $request->segment(1) == 'incrementPf' || $request->segment(2) == 'taxsettings') ? ' active' : '' }}">
                   <a href="#" class="side-menu__item" data-toggle="slide">
                          <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Salary Management</span>
                     
                    </a>
                
                    <ul class="slide-menu">

                        
              
                        
                    <li class="sub-slide {{ $request->segment(1) == 'loan' ? 'active active-sub' : '' }}">
                        <a href="{{ url('/loan') }}" class="sub-side-menu__item">
                            <span class="title"><i class="fa fa-angle-double-right"></i>Loan</span>
                        </a>
                    </li>
                    <li class="sub-slide {{ $request->segment(1) == 'eobi' ? 'active active-sub' : '' }}">
                        <a href="{{ url('/eobi') }}" class="sub-side-menu__item">
                            <span class="title"><i class="fa fa-angle-double-right"></i> EOBI</span>
                        </a>
                    </li>
                    <li class="sub-slide {{ $request->segment(1) == 'incrementPf' ? 'active active-sub' : '' }}">
                        <a href="{{ url('/incrementPf') }}" class="sub-side-menu__item">
                            <span class="title"><i class="fa fa-angle-double-right"></i>Incremental Provident Fund</span>
                        </a>
                    </li>
                
                       {{-- @if(in_array('erp_entries_manage',$per))
                            <li class="sub-slide {{ ($request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.duty_seg.index') }}">
                                    <span class="title">Segregate Quatities</span>
                                </a>
                            </li>
                        @endif--}}
                    </ul>
                </li>
            @endcan
             @can('erp_staff_manage',$per)
             <li class="slide {{ $request->segment(2) == 'home' ? 'active' : '' }}">
                <a href="{{  route('admin.leave_request.list') }}" class="side-menu__item">
                    <i class="side-menu__label fa fa-user"></i>
                    <span  class="side-menu__label">Leave Requests</span> &nbsp;&nbsp; <span class="badge text-danger" id="leave_count">0</span>
                </a>
            </li>
            @endcan
    
         @can('erp_group_event_manage')


<li class="slide {{ $request->segment(2) == 'groupss' ? "active":'' }} "  >
        <a href="#" class="side-menu__item" data-toggle="slide">
                    <i class="side-menu__icon fa fa-user"></i>
                    <span  class="side-menu__label">Group Event</span>
                </a>

    <ul class="slide-menu">
        <li class=" {{ $request->segment(2) == 'groupss' ? "active active-sub":'' }}">
            <a href="{{route('admin.groupss.index')}}" class="sub-side-menu__item">
              
                <span class="sub-side-menu__label">Groups</span>
            </a>
        </li>


    </ul>
</li>

@endcan
            
            @if(!Auth::user()->hasRole('admin') || !Auth::user()->hasRole('Ceo'))
            <li class="slide{{ $request->segment(2) == 'candidate-profile' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.staff.profile') }}" class="side-menu__item">
                    <i class="side-menu__icon  fa fa-user"></i>
                    <span class="side-menu__label">Profile</span>
                </a>
            </li>
            @endif

      
            @if(Auth::user()->hasRole('admin'))
            <li class="slide{{ $request->segment(2) == 'candidate-hired' ? 'active active-sub' : '' }}">
                <a href="{{url('create-user')}}" class="side-menu__item" >
                    <i class="side-menu__icon fa fa-user"></i>
                    <span class="side-menu__label">Add CEO & ADMIN</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->hasRole('Ceo'))
            <li class="slide{{ $request->segment(2) == 'candidate-hired' ? 'active active-sub' : '' }}">
                <a href="{{url('/attendance_details')}}" class="side-menu__item">
                    <i class="side-menu__icon fa fa-user"></i>
                    <span class="side-menu__label">View Attendance Detail</span>
                </a>
            </li>
            @endif

            @can('erp_discount_type_manage',$per)
            <li class="slide{{ $request->segment(2) == 'candidate-profile' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.discount-type.index') }}" class="side-menu__item">
                    <i class="side-menu__icon fa fa-user"></i>
                    <span class="side-menu__label">Discount Type</span>
                </a>
            </li>
            @endcan
            @can('erp_document_manage',$per)
            <li class="slide{{ $request->segment(2) == 'candidate-hired' ? 'active active-sub' : '' }}">
                <a href="{{ route('admin.documents.index') }}" class="side-menu__item">
                    <i class="side-menu__icon fa fa-file"></i>
                    <span class="side-menu__label">Documents</span>
                </a>
            </li>
            @endcan
            @if(!Auth::user()->hasRole('GM Finance'))
                @can('erp_id_card_request_manage',$per)
                


                    <li class="slide{{ $request->segment(2) == 'candidate-hired' ? 'active active-sub' : '' }}">
                        <a href="{{url('request-idcard_gm')}}" class="side-menu__item">
                            <i class="side-menu__icon fa fa-file"></i>
                            <span class="side-menu__label">Approved ID Card Requests</span>
                        </a>
                    </li>

                    

                    
                @endcan 
            @endif
                @can('erp_daily_attendace_manage',$per)
                 
                
                <li class="slide{{ $request->segment(2) == 'candidate-hired' ? 'active active-sub' : '' }}">
                        <a href="{{url('admin\teacher_attendance_daywise')}}" class="side-menu__item">
                            <i class="side-menu__icon fa fa-file"></i>
                            <span class="side-menu__label">Daily Attendance Schedule</span>
                        </a>
                    </li> 
                @endcan
            
          

          <!--      
            @can('erp_hire_hr',$per)
            <li class="slide {{ $request->segment(2) == 'branchhr' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin.branchhr.index') }}"  class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Hire HR</span>
                     
                    </a>
            </li>
            @endcan -->

            @can('erp_staff_manage',$per)
             <li class="slide {{ $request->segment(2) == 'home' ? 'active' : '' }}">
                   <a href="{{  route('admin.teaacher.course.detail') }}"  class="side-menu__item">
                    <i class="side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Teacher Course Details</span>
                     
                    </a>
               
            </li>
            @endcan

    
                </ul>
            </div>
        </aside>