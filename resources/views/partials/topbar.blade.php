

<div class="main-content">
		<!-- main-header -->
			<div class="main-header  side-header">
				<div class="container-fluid" style = "display:block;">
					<div class="main-header-left">
					<h4 style ="color:white;margin-top:20px;float:left;">ROOTS IVY ACADEMICS NETWORK</h4>
						<div class="responsive-logo">
							<a href="index.html"><img src="{{ url('public/theme') }}/assets/img/brand/logo-white.png" class="logo-1"></a>
							<a href="index.html"><img src="{{ url('public/theme') }}/assets/img/brand/logo.png" class="logo-11"></a>
							<a href="index.html"><img src="{{ url('public/theme') }}/assets/img/brand/favicon-white.png" class="logo-2"></a>
							<a href="index.html"><img src="{{ url('public/theme') }}/assets/img/brand/favicon.png" class="logo-12"></a>
						</div>
					
					</div>
					<div class="main-header-right" style ="  margin-right: 16%;">
					
						<div class="nav nav-item  navbar-nav-right ml-auto">
							<div class="nav-item full-screen fullscreen-button">
								<a class="new nav-link full-screen-link" href="#"><i class="fe fe-maximize"></i></span></a>
							</div>
							<div class="dropdown  nav-item main-header-message ">
								<a class="new nav-link" href="#" ><i class="fe fe-mail"></i><span class=" pulse-danger"></span></a>
								<div class="dropdown-menu">
									<div class="menu-header-content bg-primary-gradient text-left d-flex">
										<div class="">
											<h6 class="menu-header-title text-white mb-0">5 new Messages</h6>
										</div>
										<div class="my-auto ml-auto">
											<a class="badge badge-pill badge-warning float-right" href="#">Mark All Read</a>
										</div>
									</div>
									<div class="main-message-list chat-scroll">
										<a href="#" class="p-3 d-flex border-bottom">
											<div class="  drop-img  cover-image  " data-image-src="assets/img/faces/3.jpg">
												<span class="avatar-status bg-teal"></span>
											</div>

											<div class="wd-90p">
												<div class="d-flex">
													<h5 class="mb-1 name">Paul Molive</h5>
													<p class="time mb-0 text-right ml-auto float-right">10 min ago</p>
												</div>
												<p class="mb-0 desc">I'm sorry but i'm not sure how...</p>
											</div>
										</a>
										<a href="#" class="p-3 d-flex border-bottom">
											<div class="drop-img cover-image" data-image-src="assets/img/faces/2.jpg">
												<span class="avatar-status bg-teal"></span>
											</div>
											<div class="wd-90p">
												<div class="d-flex">
													<h5 class="mb-1 name">Sahar Dary</h5>
													<p class="time mb-0 text-right ml-auto float-right">13 min ago</p>
												</div>
												<p class="mb-0 desc">All set ! Now, time to get to you now......</p>
											</div>
										</a>
										<a href="#" class="p-3 d-flex border-bottom">
											<div class="drop-img cover-image" data-image-src="assets/img/faces/9.jpg">
												<span class="avatar-status bg-teal"></span>
											</div>
											<div class="wd-90p">
												<div class="d-flex">
													<h5 class="mb-1 name">Khadija Mehr</h5>
													<p class="time mb-0 text-right ml-auto float-right">20 min ago</p>
												</div>
												<p class="mb-0 desc">Are you ready to pickup your Delivery...</p>
											</div>
										</a>
										<a href="#" class="p-3 d-flex border-bottom">
											<div class="drop-img cover-image" data-image-src="assets/img/faces/12.jpg">
												<span class="avatar-status bg-danger"></span>
											</div>
											<div class="wd-90p">
												<div class="d-flex">
													<h5 class="mb-1 name">Barney Cull</h5>
													<p class="time mb-0 text-right ml-auto float-right">30 min ago</p>
												</div>
												<p class="mb-0 desc">Here are some products ...</p>
											</div>
										</a>
										<a href="#" class="p-3 d-flex border-bottom">
											<div class="drop-img cover-image" data-image-src="assets/img/faces/5.jpg">
												<span class="avatar-status bg-teal"></span>
											</div>
											<div class="wd-90p">
												<div class="d-flex">
													<h5 class="mb-1 name">Petey Cruiser</h5>
													<p class="time mb-0 text-right ml-auto float-right">35 min ago</p>
												</div>
												<p class="mb-0 desc">I'm sorry but i'm not sure how...</p>
											</div>
										</a>
									</div>
									<div class="text-center dropdown-footer">
										<a href="#">VIEW ALL</a>
									</div>
								</div>
							</div>
							<div class="dropdown nav-item main-header-notification">
								<a class="new nav-link" href="#"><i class="fe fe-bell"></i><span class=" pulse"></span></a>
								<div class="dropdown-menu">
									<div class="menu-header-content bg-primary-gradient text-left d-flex">
										<div class="">
											<h6 class="menu-header-title text-white mb-0">7 new Notifications</h6>
										</div>
										<div class="my-auto ml-auto">
											<a class="badge badge-pill badge-warning float-right" href="#">Mark All Read</a>
										</div>
									</div>
									<div class="main-notification-list Notification-scroll">
										<a class="d-flex p-3 border-bottom" href="#">
											<div class="notifyimg bg-success-transparent">
												<i class="la la-shopping-basket text-success"></i>
											</div>
											<div class="ml-3">
												<h5 class="notification-label mb-1">New Order Received</h5>
												<div class="notification-subtext">1 hour ago</div>
											</div>
											<div class="ml-auto" >
												<i class="las la-angle-right text-right text-muted"></i>
											</div>
										</a>
										<a class="d-flex p-3 border-bottom" href="#">
											<div class="notifyimg bg-danger-transparent">
												<i class="la la-user-check text-danger"></i>
											</div>
											<div class="ml-3">
												<h5 class="notification-label mb-1">22 verified registrations</h5>
												<div class="notification-subtext">2 hour ago</div>
											</div>
											<div class="ml-auto" >
												<i class="las la-angle-right text-right text-muted"></i>
											</div>
										</a>
										<a class="d-flex p-3 border-bottom" href="#">
											<div class="notifyimg bg-primary-transparent">
												<i class="la la-check-circle text-primary"></i>
											</div>
											<div class="ml-3">
												<h5 class="notification-label mb-1">Project has been approved</h5>
												<div class="notification-subtext">4 hour ago</div>
											</div>
											<div class="ml-auto" >
												<i class="las la-angle-right text-right text-muted"></i>
											</div>
										</a>
										<a class="d-flex p-3 border-bottom" href="#">
											<div class="notifyimg bg-pink-transparent">
												<i class="la la-file-alt text-pink"></i>
											</div>
											<div class="ml-3">
												<h5 class="notification-label mb-1">New files available</h5>
												<div class="notification-subtext">10 hour ago</div>
											</div>
											<div class="ml-auto" >
												<i class="las la-angle-right text-right text-muted"></i>
											</div>
										</a>
										<a class="d-flex p-3 border-bottom" href="#">
											<div class="notifyimg bg-warning-transparent">
												<i class="la la-envelope-open text-warning"></i>
											</div>
											<div class="ml-3">
												<h5 class="notification-label mb-1">New review received</h5>
												<div class="notification-subtext">1 day ago</div>
											</div>
											<div class="ml-auto" >
												<i class="las la-angle-right text-right text-muted"></i>
											</div>
										</a>
										<a class="d-flex p-3" href="#">
											<div class="notifyimg bg-purple-transparent">
												<i class="la la-gem text-purple"></i>
											</div>
											<div class="ml-3">
												<h5 class="notification-label mb-1">Updates Available</h5>
												<div class="notification-subtext">2 days ago</div>
											</div>
											<div class="ml-auto" >
												<i class="las la-angle-right text-right text-muted"></i>
											</div>
										</a>
									</div>
									<div class="dropdown-footer">
										<a href="#">VIEW ALL</a>
									</div>
								</div>
							</div>
							</div>


					</div>
				</div>
			</div>
			<!-- /main-header -->

			<!-- container -->
			<div class="container-fluid">
									<!-- breadcrumb -->
				<div class="breadcrumb-header justify-content-between">
					<div>
						
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><h2>{{Auth::user()->name}}</h2></a></li>
							</ol>
						</nav>
					</div>
				
				</div>
			
			

		