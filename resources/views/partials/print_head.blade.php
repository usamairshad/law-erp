<table width="100%" style="font-family: sans-serif;line-height: 1.2">
    <tbody><tr>
        <td width="33.33%" style="text-align: left;"><h4 style="margin-bottom: 10px;margin-top: 5px;font-size: 20px;">ROOTS IVY INT SCHOOLS (PVT) LTD.</h4>
            <p style="margin-bottom: 2px;font-size: 12px;margin-top: 2px;">Building # 66, Street # 7, H-8/4 Islamabad</p>
            <p style="margin-bottom: 2px;font-size: 12px;margin-top: 2px;">Phone : +111-724-111</p>
            <p style="margin-bottom: 2px;font-size: 12px;margin-top: 2px;">Email : info@rootsinternational.edu.pk</p>
        </td>
        <td width="10%" style="text-align: right;">
            <img src="{{ URL::asset('/public/uploads/logo.png') }}" width="80">
        </td>
    </tr>
    </tbody>
</table>