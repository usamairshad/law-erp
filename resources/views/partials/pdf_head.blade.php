<link rel="stylesheet" href="{{ url('adminlte') }}/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
      page. However, you can choose any other skin. Make sure you
      apply the skin class to the body tag so the changes take effect. -->
<link rel="stylesheet" href="{{ url('adminlte') }}/dist/css/skins/skin-blue.min.css">
<link rel="stylesheet" href="{{ url('css') }}/custom.css">

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">