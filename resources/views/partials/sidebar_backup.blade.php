@inject('request', 'Illuminate\Http\Request')

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>
            <!-- Inventory Section -->
            @if(Gate::check('stockitems_manage') || Gate::check('stockcategory_manage'))
                <li class="treeview {{ ($request->segment(2) == 'stockitems' || $request->segment(2) == 'stockcategory') ? ' active' : '' }}">
                    <a href="#">
                        <i class="fa fa-tasks"></i>
                        <span class="title">Inventory Manager</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Gate::check('stockitems_manage'))
                            <li class="{{ $request->segment(2) == 'stockitems' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.stockitems.index') }}">
                                    <i class="fa fa-archive"></i>
                                    <span class="title">
                                        Stock Items
                                    </span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('stockcategory_manage'))
                            <li class="{{ $request->segment(2) == 'inventorytransfer' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.inventorytransfer.index') }}">
                                    <i class="fa fa-tags"></i>
                                    <span class="title">
                                Inventory Transfer
                                </span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('stockcategory_manage'))
                            <li class="{{ $request->segment(2) == 'goodsissue' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.goodsissue.index') }}">
                                    <i class="fa fa-tags"></i>
                                    <span class="title">
                                Goods Issue
                                </span>
                                </a>
                            </li>
                        @endif

                        {{--@if(Gate::check('stockcategory_manage'))--}}
                            {{--<li class="{{ $request->segment(2) == 'goodsreceipt' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.goodsreceipt.index') }}">--}}
                                    {{--<i class="fa fa-tags"></i>--}}
                                    {{--<span class="title">--}}
                                {{--Goods Receipt--}}
                                {{--</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}

                            @if(Gate::check('stockitems_manage'))
                                <li class="{{ $request->segment(2) == 'fix_assets' ? 'active active-sub' : '' }}">
                                    <a href="{{ route('admin.fix_assets.create') }}">
                                        <i class="fa fa-archive"></i>
                                        <span class="title">
                                    Fixed Assets
                                    </span>
                                    </a>
                                </li>
                            @endif
                    </ul>
                </li>
            @endif
        <!-- Sales Section -->
            @if(Gate::check('sales_manage'))
                <li class="treeview {{ ($request->segment(2) == 'sales') || $request->segment(2) == 'salesreturn' ? ' active' : '' }}">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span class="title">Sales Manager</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if( Gate::check('sales_manage'))
                            {{--<li class="treeview {{ ($request->segment(2) == 'sales') ? ' active' : '' }}">--}}
                            {{--<li class="{{ $request->segment(2) == 'sales' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.sales.index') }}">--}}
                                    {{--<span class="title">--}}
                                    {{--Sales--}}
                                    {{--</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--</li>--}}

                            <li class="{{ $request->segment(2) == 'salesinvoice' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.salesinvoice.index') }}">
                                    <span class="title">
                                        Sales Invoice
                                    </span>
                                </a>
                            </li>


                            <li class="{{ $request->segment(2) == 'sales' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.sales.index') }}">
                                    <span class="title">
                                        Sales Orders
                                    </span>
                                </a>
                            </li>

                            <li class="{{ $request->segment(2) == 'salesreturn' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.salesreturn.index') }}">
                                    <span class="title">
                                        Sales Return
                                    </span>
                                </a>
                            </li>

                        @endif
                    </ul>
                </li>
            @endif

            @if(Gate::check(Gate::check('orders_manage') || 'performastock_manage')  || Gate::check('products_manage'))
                <li class="treeview{{ ($request->segment(2) == 'orders' || $request->segment(2) == 'performastock' || $request->segment(2) == 'lcbank'  ) ? ' active' : '' }}">
                    <a href="#">
                        <i class="fa fa-folder-open"></i>
                        <span class="title">Purchase</span>
                        <span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
				</span>
                    </a>
                    <ul class="treeview-menu">

                            @if(Gate::check('orders_manage'))
                                <li class="{{ $request->segment(2) == 'orders' ? 'active active-sub' : '' }}">
                                    <a href="{{ route('admin.orders.index') }}">
                                        <span class="title"> Order </span>
                                    </a>
                                </li>
                            @endif
                        @if(Gate::check('lcbank_manage'))
                            <li class="{{ $request->segment(2) == 'lcbank' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.lcbank.index') }}">
                                    <span class="title">Transaction</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('lcbank_manage'))
                            <li class="{{ $request->segment(2) == 'lcbank' && $request->segment(3) == 'shipment' ? 'active active-sub' : '' }}">
                                 <a href="{{ route('admin.lcbank.shipment') }}">
                                    <span class="title">Shipment Schedule</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('lcbank_manage'))

                          <li class="{{ $request->segment(2) == 'lcbank' ? 'active active-sub' : '' }}">
                                 <a href="{{ route('admin.lcbank.sheetcost') }}">
                                    <span class="title">Costing</span>
                                </a>
                            </li>
                        @endif

                            <li class="{{ $request->segment(2) == 'grimport' ? 'active active-sub' : '' }}">
                                 <a href="{{ route('admin.grimport') }}">
                                    <span class="title">GR Import</span>
                                </a>
                            </li>
                    </ul>
                </li>
            @endif
            @if(Gate::check('units_manage') || Gate::check('groups_manage') || Gate::check('ledgers_manage') || Gate::check('entries_manage'))
                <li class="treeview{{ ($request->segment(2) == 'units' || $request->segment(2) == 'groups' || $request->segment(2) == 'ledgers' || $request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? ' active' : '' }}">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span class="title">Accounts Manager</span>
                        <span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
				</span>
                    </a>
                    <ul class="treeview-menu">

                        @if(Gate::check('groups_manage'))
                            <li class="{{ ($request->segment(2) == 'groups' && $request->segment(3) != 'overview') ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.groups.index') }}">
                                    <span class="title">Groups</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('ledgers_manage'))
                            <li class="{{ $request->segment(2) == 'ledgers' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.ledgers.index') }}">
                                    <span class="title">Ledgers</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('entries_manage'))
                            <li class="{{ ($request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.entries.index') }}">
                                    <span class="title">Journal Entry</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            {{--@if(--}}
                    {{--Gate::check('account_reports_manage') ||--}}
                    {{--Gate::check('account_reports_profit_loss') ||--}}
                    {{--Gate::check('account_reports_accounts_chart') ||--}}
                    {{--Gate::check('account_reports_trial_balance') ||--}}
                    {{--Gate::check('account_reports_ledger_statement') ||--}}
                    {{--Gate::check('account_reports_balance_sheet')--}}
                {{--)--}}
                {{--<li class="treeview{{ ($request->segment(2) == 'Reports') ? ' active' : '' }}">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-bar-chart"></i>--}}
                        {{--<span class="title">Account Reports</span>--}}
                        {{--<span class="pull-right-container">--}}
				{{--<i class="fa fa-angle-left pull-right"></i>--}}
				{{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--@if(Gate::check('account_reports_balance_sheet'))--}}
                            {{--<li class="{{ $request->segment(3) == 'balance-sheet' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.account_reports.balance_sheet') }}">--}}
                                    {{--<span class="title">--}}
                                        {{--Balance Sheet--}}
                                    {{--</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                        {{--@if(Gate::check('account_reports_profit_loss'))--}}
                            {{--<li class="{{ $request->segment(3) == 'profit-loss' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.account_reports.profit_loss') }}">--}}
                                    {{--<span class="title">--}}
                                        {{--Profit and Loss--}}
                                    {{--</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                        {{--@if(Gate::check('account_reports_trial_balance'))--}}
                            {{--<li class="{{ $request->segment(3) == 'trial-balance' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.account_reports.trial_balance') }}">--}}
                                    {{--<span class="title">--}}
                                        {{--Trial Balance--}}
                                    {{--</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                        {{--@if(Gate::check('account_reports_ledger_statement'))--}}
                            {{--<li class="{{ $request->segment(3) == 'ledger-statement' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.account_reports.ledger_statement') }}">--}}
                                    {{--<span class="title">--}}
                                        {{--Ledger Statement--}}
                                    {{--</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--@endif--}}

            <!-- HR Module Starts -->
            @include('hrm.partials.sidebar_hrm')
            <!-- HR Module Ends -->

            @include('marketing.partials.sidebar_marketing')
            @include('support.partials.sidebar_support')
            @can('users_manage')
                <li class="treeview {{ ($request->segment(2) == 'permissions' || $request->segment(2) == 'roles' || $request->segment(2) == 'users') ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span class="title">@lang('global.user-management.title')</span>
                        <span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
				</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.permissions.index') }}">
                                {{--<i class="fa fa-briefcase"></i>--}}
                                <span class="title">Permissions</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.roles.index') }}">
                                {{--<i class="fa fa-briefcase"></i>--}}
                                <span class="title">Roles</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.users.index') }}">
                                {{--<i class="fa fa-user"></i>--}}
                                <span class="title">Users</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan
            @if(Gate::check('settings_manage') || Gate::check('financial_years_manage') ||
             Gate::check('tax_settings_manage') || Gate::check('companies_manage') ||
             Gate::check('departments_manage') || Gate::check('regions_manage') ||
             Gate::check('branches_manage')|| Gate::check('territory_manage') ||
             Gate::check('dealer_manage') || Gate::check('customers_manage') ||
             Gate::check('employees_manage')|| Gate::check('banks_manage') ||
             Gate::check('banksdeatil_manage') || Gate::check('products_manage') ||
             Gate::check('suppliers_manage') ||  Gate::check('units_manage' )||
             Gate::check('duty_manage') ||  Gate::check('stockcategory_manage' )||
             Gate::check('suppliers_manage') ||  Gate::check('products_manage' )||
             Gate::check('suppliers_manage'))
                <li class="treeview {{ ($request->segment(2) == 'settings' ||
                $request->segment(2) == 'financial_years' || $request->segment(2) == 'taxsettings' ||
                 $request->segment(2) == 'companies' || $request->segment(2) == 'departments' ||
                 $request->segment(2) == 'regions' || $request->segment(2) == 'branches' ||
                 $request->segment(2) == 'territory' || $request->segment(2) == 'dealers' ||
                 $request->segment(2) == 'customers' || $request->segment(2) == 'employees' ||
                 $request->segment(2) == 'banks' || $request->segment(2) == 'bank_detail' ||
                 $request->segment(2) == 'products' || $request->segment(2) == 'suppliers' ||
                 $request->segment(2) == 'units' || $request->segment(2) == 'duty' ||
                 $request->segment(2) == 'stockcategory' || $request->segment(2) == 'suppliers') ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-gear"></i>
                        <span class="title">Admin Settings</span>
                        <span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
				</span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Gate::check('settings_manage'))
                            <li class="{{ $request->segment(2) == 'settings' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.settings.index') }}">
                                    <span class="title">Settings</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('financial_years_manage'))
                            <li class="{{ $request->segment(2) == 'financial_years' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.financial_years.index') }}">
                                    <span class="title">Financial Years</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('tax_settings_manage'))
                            <li class="{{ $request->segment(2) == 'taxsettings' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.taxsettings.index') }}">
                                    <span class="title">Tax Settings</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('companies_manage'))
                            <li class="{{ $request->segment(2) == 'companies' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.companies.index') }}">
                                    <span class="title">Companies</span>
                                </a>
                            </li>
                        @endif
                        {{--@if(Gate::check('account_types_manage'))--}}
                            {{--<li class="{{ $request->segment(2) == 'account_types' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.account_types.index') }}">--}}
                                    {{--<span class="title">Account Types</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                        {{--@if(Gate::check('payment_methods_manage'))--}}
                            {{--<li class="{{ $request->segment(2) == 'payment_methods' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.payment_methods.index') }}">--}}
                                    {{--<span class="title">Payment Methods</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                        @if(Gate::check('departments_manage'))
                            <li class="{{ $request->segment(2) == 'departments' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.departments.index') }}">
                                    <span class="title">Departments</span>
                                </a>
                            </li>
                        @endif
                        {{--@if(Gate::check('currencies_manage'))--}}
                            {{--<li class="{{ $request->segment(2) == 'currencies' ? 'active active-sub' : '' }}">--}}
                                {{--<a href="{{ route('admin.currencies.index') }}">--}}
                                    {{--<span class="title">Currencies</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                        @if(Gate::check('regions_manage'))
                            <li class="{{ $request->segment(2) == 'regions' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.regions.index') }}">
                                    <span class="title">Regions</span>
                                </a>
                            </li>
                        @endif
                            @if(Gate::check('branches_manage'))
                                <li class="{{ $request->segment(2) == 'branches' ? 'active active-sub' : '' }}">
                                    <a href="{{ route('admin.branches.index') }}">
                                        <span class="title">Branches</span>
                                    </a>
                                </li>
                            @endif
                        @if(Gate::check('territory_manage'))
                            <li class="{{ $request->segment(2) == 'territory' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.territory.index') }}">
                                    <span class="title">Territories</span>
                                </a>
                            </li>
                        @endif

                        @if(Gate::check('dealer_manage'))
                            <li class="{{ $request->segment(2) == 'dealers' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.dealer.index') }}">
                                    <span class="title">Dealers</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('customers_manage'))
                            <li class="{{ $request->segment(2) == 'customers' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.customers.index') }}">
                                    <span class="title">Customers</span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('employees_manage'))
                            <li class="{{ $request->segment(2) == 'employees' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.employees.index') }}">
                                    <span class="title">Employees</span>
                                </a>
                            </li>
                        @endif

                            @if(Gate::check('banks_manage'))
                                <li class="{{ $request->segment(2) == 'banks' ? 'active active-sub' : '' }}">
                                    <a href="{{ route('admin.banks.index') }}">
                                        <span class="title">Banks</span>
                                    </a>
                                </li>
                            @endif
                            @if(Gate::check('banksdeatil_manage'))
                                <li class="{{ $request->segment(2) == 'bank_detail' ? 'active active-sub' : '' }}">
                                    <a href="{{ route('admin.bank_detail.index') }}">
                                        <span class="title">Banks Detail</span>
                                    </a>
                                </li>
                            @endif
                            @if(Gate::check('products_manage'))
                                <li class="{{ $request->segment(2) == 'products' ? 'active active-sub' : '' }}">
                                    <a href="{{ route('admin.products.index') }}">
                                        <span class="title">
                                        Products
                                        </span>
                                    </a>
                                </li>
                            @endif
                        @if(Gate::check('suppliers_manage'))
                            <li class="{{ $request->segment(2) == 'suppliers' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.suppliers.index') }}">
                                    <span class="title"> Suppliers </span>
                                </a>
                            </li>
                        @endif
                        @if(Gate::check('units_manage'))
                            <li class="{{ $request->segment(2) == 'units' ? 'active active-sub' : '' }}">
                                <a href="{{ route('admin.units.index') }}">
                                    <span class="title">
                                    Units
                                    </span>
                                </a>
                            </li>
                        @endif
                            @if(Gate::check('duty_manage'))
                                <li class="{{ $request->segment(3) == 'duty' ? 'active active-sub' : '' }}">
                                    <a href="{{ route('admin.duty.index') }}">
                                        <span class="title"> Duty </span>
                                    </a>
                                </li>
                            @endif
                            @if(Gate::check('stockcategory_manage'))
                                <li class="{{ $request->segment(2) == 'stockcategory' ? 'active active-sub' : '' }}">
                                    <a href="{{ route('admin.stockcategory.index') }}">

                                        <span class="title">
                                    Stock Category
                                    </span>
                                    </a>
                                </li>
                            @endif
                    </ul>
                </li>
            @endif
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>