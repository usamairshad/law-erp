@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Expense Summary Report</h1>
    </section>
@stop
@section('stylesheet')
    <!-- Pace style -->
    {{--<link rel="stylesheet" href="{{ url('public/adminlte') }}/plugins/pace/pace.min.css">--}}
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('content')
    <div class="box box-primary">
        <br>
        <form id="form">
            {{ csrf_field()}}
            <div class="col-md-3">
                <label>Date</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input id="datepicker" class="form-control" name="date_range" type="text" autocomplete="off">
                </div>
            </div>
            <div class="col-md-2" style="margin-top: 25px;">
                <div class="form-group">
                    {{--<button type="button" class="btn btn-sm btn-primary" onclick="fetch_trial_balance()"><i class="fa fa-search"></i> </button>--}}
                    <button type="submit" class="btn btn-sm btn-default" formaction="print_expense_summary" formmethod="post" formtarget="_blank"><i class="fa fa-print"></i> </button>
                    <button type="submit" class="btn btn-sm btn-success" formaction="print_expense_summary?type=excel" formmethod="post" formtarget="_blank"><i class="fa fa-file-excel-o"></i> </button>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <!-- /.box-header -->
    </div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $( function() {
            $( "#datepicker" ).daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                }
            });
        });

    });
    function fetch_trial_balance() {
        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:route('admin.account_reports.get_trial_balance'),
            data:$("#form").serialize(),
            success:function (data) {


            }
        })
    }

</script>
@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection