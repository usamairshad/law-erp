@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Report Parameter</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title"></h3>

            <a href="" class="btn btn-success pull-right" data-toggle="modal" data-target="#reportparameterModal" onclick="resetform()">Add New Report Parameter</a>

        </div>



        <div class="panel-body table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Range</th>
                    <th>Group Report</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>

                    @foreach($report_parameter as $report)
                    <tr>
                        <td>{{$report->name}}</td>
                        <td>{{$report->description}}</td>
                        <td>{{$report->range}}</td>
                        <td>{{$report->reportgroup->name}}</td>
                            <td>
                                <form method="post" action="{{url('report-parameter', $report->id)}}">
                                    {{ method_field('delete') }}
                                    {!! csrf_field() !!}
                                    <a  class="btn btn-primary update btn-sm approve" data-toggle="modal" data-target="#reportparameterModal" data-group_parameter="{{$report}}">Update</a>

                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    
                                </form>
                            </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--add new modal popup--}}
    <div class="modal fade reportparameterModal" id="reportparameterModal" tabindex="-1" role="dialog" aria-labelledby="reportparameterModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="reportparameterModal">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('report-parameter')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Name:</label>
                                    <input type="text" class="form-control" value="" name="name" id="name" required>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-gruop">
                                    <label for="formGroupExampleInput">Group Report:</label>
                                    <select class="form-control" name="reportGroup_id" id="reportGroup_id" required>
                                    <option>Select Group Report</option>
                                    @foreach($report_group as $group)
                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Range:</label>
                                    <input type="text" class="form-control" value="" name="range" id="range" required>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-gruop">
                                    <label for="formGroupExampleInput">Description:</label>
                                    <input type="text" class="form-control" value="" name="description" id="description" required>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@stop

@section('javascript')
    <script>
        $(".update").on("click",function() {
            var id = $(this).data('group_parameter').id;
            var name = $(this).data('group_parameter').name;
            var description = $(this).data('group_parameter').description;
            var range = $(this).data('group_parameter').range;
            var group_report = $(this).data('group_parameter').reportGroup_id
            
            $('#name').val(name);
            $('#description').val(description);
            $('#id').val(id);
            $('#range').val(range);
            $('#reportGroup_id').val(group_report);
        });
        
         function resetform(){

            $("#add_new").trigger("reset");
        }
     
      
    </script>
@endsection