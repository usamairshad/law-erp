@inject('request', 'Illuminate\Http\Request')
@include('partials.head')
<style type="text/css">
body{
    background-color:white;
}
    @page {
        margin: 10px 20px;
    }
    table{
        text-align: center;
    }
    @media print {
        table {
            font-size: 12px;
        }
        .tr-root-group {
            background-color: #F3F3F3;
            color: rgba(0, 0, 0, 0.98);
            font-weight: bold;
        }
        .tr-group {
            font-weight: bold;
        }
        .bold-text {
            font-weight: bold;
        }
        .error-text {
            font-weight: bold;
            color: #FF0000;
        }
        .ok-text {
            color: #006400;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding: 2px !important;
        }

    }
    .table th{ text-align: center; }
    .last-th th{border-bottom: 1px solid}
</style>
<div class="panel-body pad table-responsive">
    @include('partials.print_head')
        <div class="panel-body pad table-responsive">
        <table align="center" style = "margin-bottom: 20px;">
        <tbody>
        <tr>
            <td><h3 align="center"><span style="border-bottom: double;">Student Address Report</span></h3>
             </td>
        </tr>
        <tr>
            <td align="center"><span></span>
            </td>
        </tr>
        </tbody>
    </table>
            <table class="table table-bordered">
                <thead>
                <tr>
                
                <th>Admission No</th>
                    <th>Branch Name </th>
                    <th>Program </th>
                    <th>Class</th>
                    <th>Section</th>
                    <th>Student Name</th>
                    <th>Date of Birth</th>
                    <th>Student Address</th>
                    <th>Date of Admission</th>
                </tr>
                </thead>
               
                <tbody id="getData">
                @foreach($student as $std)

                <tr id="fetch_ob">
                <td>{{$std->reg_no}}</td>
                <td>{{$std->branch_name}}</td>
                <td>{{$std->program_name}}</td>
                <td>{{$std->class_name}}</td>
                <td>{{$std->section_name}}</td>
                <td>{{$std->first_name}} {{$std->last_name}}</td>
                <td>{{$std->date_of_birth}}</td>
                <td>{{$std->address}}</td>
                <td>{{$std->reg_date}}</td>
                </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- On Fetching Data -->
<script>
 
    function fetch_ledger() {
        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : '{{ route( 'student-address-report-print' ) }}',
            data:$("#ledger-form").serialize(),
            success:function (data) {
                console.log(data);
                var htmlData="";
                var k=1;
                for(i in data.data){
                    if(k >1)
                    {
                    //console.log(data.data[i].vt);
                    htmlData+='<tr>';
                    htmlData+='<td>'+data.data[i].reg_no+'</td>';
                    htmlData+='<td>'+data.data[i].branch_name+'</td>';
                    htmlData+='<td>'+data.data[i].program_name+'</td>';
                    htmlData+='<td>'+data.data[i].class_name+'</td>';
                    htmlData+='<td>'+data.data[i].section_name+'</td>';
                    htmlData+='<td>'+data.data[i].first_name+" "+data.data[i].last_name+'</td>';
                    htmlData+='<td>'+data.data[i].date_of_birth+'</td>';
                     htmlData+='<td>'+data.data[i].address+'</td>';
                    htmlData+='<td>'+data.data[i].reg_date+'</td>';
                    htmlData+='</tr>';
                    }
                    k++;
                }
           
                $("#getData").html(htmlData);
                $('#fetch_ob').html(data.ob);
            }
        });
    }

</script>
<!-- Company And City Ajax Call  to Get Branch Record-->
<script>
function myFunction() {
  var getid = document.getElementById("mySelect").value;
  $.ajax({
      url: '{{url('security-company')}}',
      type: 'get',
      data: {
          "company_id": getid
      },
      success: function(data){
          console.log(data);
          $("#bID").empty();
          
          $("#bID").append("<option>---Select---</option>");
          for(i in data){
              $("#bID").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
          }
      }
  })
}
    </script>

@section('javascript')
    
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    

@endsection