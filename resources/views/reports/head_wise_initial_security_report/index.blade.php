@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}

</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Head Wise Initial Security Fee Report</h3>
    <div class="box box-primary">
        <br>
        <form id="ledger-form">
            {{ csrf_field()}}
         
            <div class="col-md-3" style = "float:left">
                <div class="form-group">
                    <label>Select Company</label>
                    <select name = "company_id" class="form-control input-sm select2" id="mySelect" onchange="myFunction()">
                        <option value="">---Select---</option>
                        {!! \App\Models\Admin\Company::CompanyList() !!}
                    </select>
                </div>
            </div>
            <div class="col-md-3"  style = "float:left">
                <div class="form-group">
                    <label>Select City</label>
                    <select  name='city_id' class="form-control input-sm select2" id="cID">
                        <option value="">---Select---</option>
                        {!! \App\Models\Admin\City::CityList() !!}
                    </select>
                </div>
            </div>
            <div class="col-md-3"  style = "float:left">
                <div class="form-group">
                    <label>Select Branch</label>
                    <select  name='branch_id' class="form-control input-sm select2 branches" id="bID">
                        <option value="">---Select---</option>
                        
                    </select>
                </div>
            </div>

            <div class="col-md-3"  style = "float:left">
                <div class="form-group">
                    <label>Select Board</label>
                    <select id="boards" name="boards" class="form-control
                SelectBoard"></select>
                </div>
            </div>
            <div class="col-md-3"  style = "float:left">
                <div class="form-group">
                    <label>Select Program</label>
                    <select id="programs" name="programs" class="form-control
                SelectProgram">      
                </select>
                </div>
            </div>
            <div class="col-md-3"  style = "float:left">
                <div class="form-group">
                    <label>Select Class</label>
                    <select id="classes" name="classes" class="form-control SelectClass">      
                </select>
                </div>
            </div>
            <div class="col-md-3"  style = "float:left">
                <div class="form-group">
                    <label>Select Section</label>
                    <select id="sections" name="sections" class="form-control SelectSection">      
                </select>
                </div>
            </div>
            <div class="col-md-3"  style = "float:left">
                <div class="form-group">
                    <label>Select Month</label>
                    <input type="month" id="month" name="month">
                </div>
            </div>

            <div class="col-md-2" style="margin-top: 25px;">
                <div class="form-group">
                    <button type="button" class="btn btn-sm btn-primary" onclick="fetch_ledger()"><i class="fa fa-search"></i> </button>
                    <button type="submit" class="btn btn-sm btn-default" formaction="{{ route( 'head-wise-initial-security-fee-report-print' ) }}?type=print" formmethod="post" formtarget="_blank"><i class="fa fa-print"></i> </button>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Admission No</th>
                    <th>Student Name</th>
                    <th>Branch Name </th>
                    <th>Board Name </th>
                    <th>Program </th>
                    <th>Class</th>
                    <th>Term</th>
                    <th>Installement</th>
                    <th>Initial Fee</th>
                    <th>Security Fee</th>
                    <th>Due Date</th>
                    <th>Paid Date</th>
                </tr>
                </thead>
                <tr id="fetch_ob"></tr>
                <tbody id="getData"></tbody>
            </table>
        </div>
    </div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- On Fetching Data -->
<script>
 
    function fetch_ledger() {
        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : '{{ route( 'head-wise-initial-security-fee-report-print' ) }}',
            data:$("#ledger-form").serialize(),
            success:function (data) {
                    console.log(data);

                var htmlData="";
                var k=1;
                for(i in data.data){
                    if(k >1)
                    {
                    //console.log(data.data[i].vt);
                    htmlData+='<tr>';
                    // htmlData+='<td>'+data.data[i].reg_no+'</td>';
                    // htmlData+='<td>'+data.data[i].branch_name+'</td>';
                    // htmlData+='<td>'+data.data[i].board_name+'</td>';
                    // htmlData+='<td>'+data.data[i].program_name+'</td>';
                    // htmlData+='<td>'+data.data[i].class_name+'</td>';
                    // htmlData+='<td>'+data.data[i].section_name+'</td>';
                    // htmlData+='<td>'+data.data[i].first_name+" "+data.data[i].last_name+'</td>';
                    // htmlData+='<td>'+data.data[i].date_of_birth+'</td>';
                    // htmlData+='<td>'+data.data[i].reg_date+'</td>';
                    htmlData+='</tr>';
                    }
                    k++;
                }
           
                $("#getData").html(htmlData);
                $('#fetch_ob').html(data.ob);
            }
        });
    }

</script>
<!-- Company And City Ajax Call  to Get Branch Record-->
<script>
function myFunction() {
  var getid = document.getElementById("mySelect").value;
  $.ajax({
      url: '{{url('security-company')}}',
      type: 'get',
      data: {
          "company_id": getid
      },
      success: function(data){
          console.log(data);
          $("#bID").empty();
          
          $("#bID").append("<option>---Select---</option>");
          for(i in data){
              $("#bID").append("<option value='"+data[i].id+"'>"+data[i].name+"</option>");
          }
      }
  })
}
    </script>

    <script type="text/javascript">
        //Board Selection
        $(document).on('change','.branches',function () 
        {
        

            var branch_id=$(this).val();
            
           
            
            console.log(branch_id);
            
            var op="";
            $.ajax({
                type:'get',
          
                url: '{{ route("find_student_board") }}',
                data:{'branch_id':branch_id},
                dataType:'json',//return data will be json
                success:function(data){
                    
                   console.log(data);
                    // here price is coloumn name in products table data.coln name
                   
                     $html = '<option value="null">Select Board</option>';
                     data.forEach((dat) => {


                         $html += `<option value=${dat.board.id}>${dat.board.name}</option>`;
                     });

                   $("#boards").html($html);

                },
                error:function(){
                }
            });


        });

 //Program Selection
        $(document).on('change','.SelectBoard',function () 
        {
            
            var board_id=$(this).val();
            console.log(board_id,"board");
            var branch_id=$('#bID').val();
         


            var a=$('#program').parent();
            
            console.log(branch_id,"branch");
            
            var op="";
            
            console.log("here");
            
            $.ajax({
                type:'get',
                url:'{!!URL::to('find_student_board_program')!!}',
                data:{'board_id':board_id,'branch_id':branch_id},
                dataType:'json',//return data will be json
                async:false,
                success:function(data){
                    //here price is coloumn name in products table data.coln name
                    console.log(data,"data");
                    $html = '<option value="null">Select Program</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.program.id}>${dat.program.name}</option>`;
                    });

                    $("#programs").html($html);

                },
                error:function(){
                }
            });


        });

//ClassSelection

 $(document).on('change','.SelectProgram',function () 
        {
            
            var program_id=$(this).val();
            
            var branch_id=$('#bID').val();
            var board_id=$('#boards').val();

          
            $.ajax({
                type:'get',
                url:'{!!URL::to('find_student_board_program_class')!!}',
                data:{'board_id':board_id,'branch_id':branch_id,'program_id':program_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    $html = '<option value="null">Select Class</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.get_class.id}>${dat.get_class.name}</option>`;
                    });

                    $("#classes").html($html);

                },
                error:function(){
                }
            });

        });



//ClassSelection

 $(document).on('change','.SelectClass',function () 
        {

            
            var class_id=$(this).val();
            
            var branch_id=$('#bID').val();
            var board_id=$('#boards').val();
             var program_id=$('#programs').val();

          
             $.ajax({
                type:'get',
                url:'{!!URL::to('find_student_board_program_class_section')!!}',
                data:{'board_id':board_id,'branch_id':branch_id,'program_id':program_id,'class_id':class_id},
                dataType:'json',//return data will be json
                success:function(data){
                    console.log(data)

                        $('#sections').append($('<option selected="selected" disabled="disabled">Select</option>'));
                        $.each( data, function(k, v) {
                            $('#sections').append($('<option value=' +  v.section.id + '>' + v.section.name + '</option>'));
                        });
                    // here price is coloumn name in products table data.coln name


                },
                error:function(){
                }
            });

        });
    </script>

@section('javascript')
    
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    

@endsection