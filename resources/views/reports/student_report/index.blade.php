@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Student Report</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title"></h3>

            <a href="" class="btn btn-success pull-right" data-toggle="modal" data-target="#studentModal" onclick="resetform()">Add New Student Report</a>

        </div>



        <div class="panel-body table-responsive">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('deleted'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                    <th>Student Name</th>
                    <th>Grade</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>

                    @foreach($stu_reports as $stu_report)
                    <tr>
                        <td>{{$stu_report->student->first_name}}</td>
                        <td>{{$stu_report->grade}}</td>
                            <td>
                                <form method="post" action="{{url('student-report', $stu_report->id)}}">
                                    {{ method_field('delete') }}
                                    {!! csrf_field() !!}
                                    <a  class="btn btn-primary update btn-sm approve" data-toggle="modal" data-target="#studentModal" data-stu_report="{{$stu_report}}">Update</a>

                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    
                                </form>
                            </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--add new modal popup--}}
    <div class="modal fade studentModal" id="studentModal" tabindex="-1" role="dialog" aria-labelledby="groupModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="groupModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="{{url('student-report')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-gruop">
                                    <label for="formGroupExampleInput">Student:</label>
                                    <select class="form-control" name="student_id" id="student_id" required>
                                    <option>Select Student</option>
                                    @foreach($students as $student)
                                    <option value="{{$student->id}}">{{$student->first_name}}</option>
                                    @endforeach
                                </select>
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-gruop">
                                    <label for="formGroupExampleInput">Grade:</label>
                                    <input type="text" class="form-control" value="" name="grade" id="grade" required>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@stop

@section('javascript')
    <script>
        $(".update").on("click",function() {
            var id = $(this).data('stu_report').id
            var stu_id = $(this).data('stu_report').student_id;
            var grade = $(this).data('stu_report').grade;

             $('#student_id').val(stu_id);
             $('#grade').val(grade);
             $('#id').val(id);
        });
        
         function resetform(){

            $("#add_new").trigger("reset");
        }
     
      
    </script>
@endsection