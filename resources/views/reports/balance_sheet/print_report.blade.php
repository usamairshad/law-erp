@inject('request', 'Illuminate\Http\Request')
@include('partials.head')
<div class="panel-body pad table-responsive">
    @include('partials.print_head')
    <table align="center" width="100%">
        <tbody>
        <tr>
            <td><h3 align="center"><span style="border-bottom: double;">Balance Sheet Report</span></h3>
            </td>
        </tr>
        <tr>
            <td align="center"><span>As on {{ $end_date }}</span>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="clear clearfix"></div>
    <!-- Liabilities and Assets -->

    <div class="col-md-12">
        <!-- Assets -->
        <table class="table">
            <tbody>
            {!! $data !!}
            </tbody>
        </table>
    </div>