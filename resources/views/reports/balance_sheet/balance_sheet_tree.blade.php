@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Balance Sheet Report <i class="fa  fa-spin"></i></h1>
    </section>
@stop
@section('stylesheet')
    <!-- Pace style -->
    {{--<link rel="stylesheet" href="{{ url('public/adminlte') }}/plugins/pace/pace.min.css">--}}
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('content')
    <div class="box box-primary">
        <br>
        <form id="form">
            {{ csrf_field()}}
            <div class="col-md-3">
                <label>Date</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input id="datepicker" class="form-control" name="date_range" type="text" autocomplete="off" style="height: 30px;">
                </div>
            </div>
            <div class="col-md-2" style="margin-top: 25px;">
                <div class="form-group">
                    {{--<button type="button" class="btn btn-sm btn-primary" onclick="fetch_trial_balance()"><i class="fa fa-search"></i> </button>--}}
                    <button type="submit" class="btn btn-sm btn-default" formaction="print_balance_sheet" formmethod="post" formtarget="_blank"><i class="fa fa-print"></i> </button>
                    <button onclick="printDiv()" type="button" class="btn btn-sm btn-default"><i class="fa fa-print"></i> print</button>
                    {{--<button type="submit" class="btn btn-sm btn-success" formaction="print_balance_sheet?type=excel" formmethod="post" formtarget="_blank"><i class="fa fa-file-excel-o"></i> </button>--}}
                </div>
            </div>
        </form>
        <div class="col-md-12" id="print">
            <div class="menu">
                <div class="accordion">
                    <!-- Main Account -->
                    @foreach($Groups as $group)
                        @if($group->parent_id==0)
                            <div class="accordion-group">
                                <!--Group-heading-->
                                <div class="accordion-heading area">

                                    <a class="accordion-toggle" data-toggle="collapse" href=
                                    "#{{$group->number}}" onclick="get_tree(this, {{$group->id}}, {{$group->level}});" val="{{$group->number}}">{{$group->number}}-{{$group->name}} <span style="float: right;"> {{ \App\Helpers\CoreAccounts::balance_by_group('', $group->account_type_id) }}</span></a>
                                </div><!-- /Área -->
                                <!--end-group-heading-->
                                <div class="accordion-body collapse" id="{{$group->number}}">

                                </div>
                            </div>
                    @endif
                @endforeach
                <!--end main account-->
                </div><!-- /accordion -->
            </div>
        </div>
        <div class="clearfix"></div>

        <!-- /.box-header -->
    </div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $( function() {
            $( "#datepicker" ).daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                }
            });
        });

    });

    function get_tree(obj, id, level) {
        var DID = $(obj).attr('val');
        var g = $(obj).parents('.accordion-group').find('.accordion-body');
       if($(obj).attr('aria-expanded')==undefined){

        $.ajax({
            url: '{{ url('admin/reports/get_balance_sheet_tree') }}?id=' + id,
            dataType: 'JSON',
            success: function (data) {
                g.css('margin-left', '30px');
                $("#"+DID).html(data.data);
                g.removeClass('collapse.in');
                color_level(DID, level);
            }
        });
       }
    }
    function color_level(id, level){
        if(level==1){
            $("#"+id).find('.accordion-group').css('border-left','4px solid #65c465');
            $("#"+id).find('.accordion-toggle').addClass('text-success');
        }
        if(level==2){
            $("#"+id).find('.accordion-group').css('border-left','4px solid #98b3fa');
            $("#"+id).find('.accordion-toggle').addClass('text-info');
        }
        if(level==3){
            $("#"+id).find('.accordion-group').css('border-left','4px solid #a83266');
            $("#"+id).find('.accordion-toggle').addClass('text-warning');
        }
        if(level==4){
            $("#"+id).find('.accordion-group').css('border-left','4px solid #e0d7d7');
            $("#"+id).find('.accordion-toggle').addClass('text-danger');
        }
        if(level==5){
            $("#"+id).find('.accordion-group').css('border-left','4px solid #141706');
            $("#"+id).find('.accordion-toggle').addClass('text-secondary');
        }
    }
    function printDiv() {
        var divContents = document.getElementById("print").innerHTML;
        var a = window.open('', '', 'height=1000, width=1000');
        a.document.write('<html>');
        a.document.write('<body > ');
        a.document.write('<head><link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">' +
            '<link rel="stylesheet" href="https://localhost/rootsivy_erp/public/adminlte/dist/css/skins/skin-blue.min.css">' +
            '<link rel="stylesheet" href="https://localhost/rootsivy_erp/public/css/custom.css">' +
            '<link rel="stylesheet" href="https://localhost/rootsivy_erp/public/adminlte/bower_components/select2/dist/css/select2.min.css">' +
            '<style>#id { font-size: 12px;}</style>' +
            '</head>');
        a.document.write(divContents);
        a.document.write('</body></html>');
        a.document.close();
        a.focus();
        a.print();
        a.close();
    }


</script>
@section('javascript')
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection