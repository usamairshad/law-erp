@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
.daterangepicker
{
	width:40% !important;
} 
.left
{
	float:left;
	width:47% !important;
	margin-left:2%;
	margin: 5px;

}
.right
{
	float:left;
	width:49% !important;
	margin: 5px;

}
.range_inputs
{
    padding:10px;
}
.applyBtn
{
    width:50%;
}
.cancelBtn
{
    background: lightgrey;
    width:49%;
}
.daterangepicker.show-calendar .ranges {
    margin-top: 8px;
    width: 100%;
}
.fa
{
	display:none !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Ledger Report</h3>
                        
    <div class="box box-primary">
        <br>
        <form id="ledger-form" style = "margin-top:30px;">
            {{ csrf_field()}}
            <div class="col-md-4" style = "float:left">

                <label>Date</label>
                <div class="input-group" >
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                  
                    <input id="datepicker" class="form-control" name="date_range" type="text" autocomplete="off" >
                </div>
            </div>
            <div class="col-md-4" style = "float:left">
                <div class="form-group">
                    <label>Select Branch</label>
                    <select  name='branch_id' class="form-control input-sm select2" id="bID">
                        <option value="">---Select---</option>
                        {!! \App\Models\Admin\Branches::branchList() !!}
                    </select>
                </div>
            </div>
            <div class="col-md-4" style = "float:left">
                <div class="form-group">
                    <label>Select Search Type</label>
                    <select  name='search_type' class="form-control input-sm"  id="search_type" required>
                        <option value="Staff">Staff</option>
                        <option value="Student">Student</option>
                        <option value="Ledger">Ledger</option>

                    </select>
                </div>
            </div> 
             <div class="col-md-9" style = "float:left">
                <div class="form-group">
                    <label>Select Ledger</label>
                    <select  name='leadger_idd' class="form-control input-sm" id="search_ledger_id">
                        <option value="">---Select---</option>

                    </select>
                </div>
            </div> 
          
            <div class="col-md-3" style="float:left;">
                <div class="form-group" style = "float: right;
    margin-top: 10px;">
                    <button type="button" class="btn btn-sm btn-primary" onclick="fetch_ledger()" style ="width:100px; padding:10px;"><i class="fa fa-search"style="display:block !important;"></i> search </button>
                    <button type="submit" class="btn btn-sm btn-default" formaction="ledger_print?type=print" formmethod="post"  style ="width:100px; background: lightgray; padding: 10px;" formtarget="_blank"><i class="fa fa-print" style="display:block !important;"></i>Print </button>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style = "font-size: 14px;color: black;">Date</th>
                    <th style = "font-size: 14px;color: black;">VN</th>
                    <th style = "font-size: 14px;color: black;">Voucher Type</th>
                    <th style="font-size: 14px;color: black;">Description</th>
                    <th style="text-align: right;font-size: 14px;color: black;">Dr</th>
                    <th style="text-align: right;font-size: 14px;color: black;">Cr</th>
                    <th style="font-size: 14px;color: black;">Balance</th>
                </tr>
                </thead>
                <tr id="fetch_ob"></tr>
                <tbody id="getData"></tbody>
            </table>
        </div>
    </div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    
    $(document).ready(function() {
    
        $('.select2').select2({
            placeholder: 'Select an option',
            allowClear: true,
        });
            $('#search_ledger_id').select2({
            placeholder: 'Select an option',
            allowClear: true,
            ajax: {
                url: route('admin.voucher.teacher_search'),
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    return {
                        
                        item: params.term, branch_id:$("#bID").val(),type:$("#search_type").val()
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
            }
            });
        


        $( function() {
            $( "#datepicker" ).daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                }
            });
        });

    });
    function fetch_ledger() {
        $.ajax({
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:route('admin.account_reports.ledger_print'),
            data:$("#ledger-form").serialize(),
            success:function (data) {
                var htmlData="";
                for(i in data.data){
                    //console.log(data.data[i].vt);
                    htmlData+='<tr>';
                    htmlData+='<td>'+data.data[i].voucher_date+'</td>';
                    htmlData+='<td>'+data.data[i].number+'</td>';
                    htmlData+='<td>'+data.data[i].vt+'</td>';
                    htmlData+='<td>'+data.data[i].narration+'</td>';
                    htmlData+='<td>'+data.data[i].dr_amount+'</td>';
                    htmlData+='<td>'+data.data[i].cr_amount+'</td>';
                    htmlData+='<td style="text-align: right;">'+data.data[i].balance+'</td>';
                    htmlData+='</tr>';
                }
                htmlData+='<tr>';
                    htmlData+='<td colspan="4"></td>';
                    htmlData+='<th>'+data.total_dr+'</th>';
                    htmlData+='<th>'+data.total_cr+'</th>';
                    htmlData+='<th style="text-align: right;">'+data.balance+'</th>';
                htmlData+='</tr>';
                $("#getData").html(htmlData);
                $('#fetch_ob').html(data.ob);
            }
        })
    }

</script>
@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/PACE/pace.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

@endsection