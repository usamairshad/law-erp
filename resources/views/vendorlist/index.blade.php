@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Landlord or Payee List</h3>

                
                <a href="{{ route('vendor-create') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Add New Landlord or Payee</a>
               

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($asset_types) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th> Name</th>
                        <th>Cnic</th>
                        <th>NTN</th>
                        <th>Type</th>
                        <th>Category</th>
                        <th>Sales Tax No</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Address</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($asset_types) > 0)
                        @foreach ($asset_types as $asset_type)
                            <tr data-entry-id="{{ $asset_type->id }}">
                                <td>{{ $asset_type->vendor_name }}</td>
                                <td>{{ $asset_type->cnic }}</td>
                                <td>{{ $asset_type->ntn }}</td>
                                <td>{{ $asset_type->type }}</td>
                                <td>{{ $asset_type->category }}</td>

                                <td>{{ $asset_type->salestaxno }}</td>
                                <td>{{ $asset_type->email }}</td>
                                <td>{{ $asset_type->contact }}</td>
                                <td>{{ $asset_type->addresss }}</td>
                                <td style = "float:left;">
                                        <a href="{{ route('vendor-shows', [$asset_type->vendor_id]) }}"
                                            class="btn btn-xs btn-info">View</a>

                                        <a href="{{ route('vendor-edit', [$asset_type->vendor_id]) }}"
                                            class="btn btn-xs btn-info">Edit</a>
                                   
                                            <a  class="btn btn-xs btn-danger view" href="{{ route('vendor-delete', [$asset_type->vendor_id]) }}">Delete</a>
                             


                                  
                                </td> 
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
