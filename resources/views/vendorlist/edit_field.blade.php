<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    <input type="text" name="name" value="{{$asset_types->vendor_name}}" class="form-control" required maxlength="30">
    <input type="hidden" name="id" value="{{$asset_types->vendor_id}}" class="form-control" required maxlength="30">
</div>
<div class="form-group col-md-3 @if($errors->has('cnic')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'CNIC*', ['class' => 'control-label']) !!}
    <input type="text" name="cnic" value="{{$asset_types->cnic}}"  class="form-control" required >
</div>
<div class="form-group col-md-3 @if($errors->has('NTN')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'NTN Number*', ['class' => 'control-label']) !!}
    <input type="text" name="ntn" value="{{$asset_types->ntn}}" class="form-control"  maxlength="10">
</div>
<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Select Filer Type*', ['class' => 'control-label']) !!}
                    <select name="filer"  required="required"  value="{{$asset_types->type}}"  class="form-control" >
                    <option value = "">Select Filer</option>
                    @if($asset_types->type =="Filer")
                            <option value = 'Filer' selected>Filer</option>
                            <option value = 'Non-Filer'>Non-Filer</option>
                            @else
                            <option value = 'Filer' >Filer</option>
                            <option value = 'Non-Filer' selected>Non-Filer</option>
                            @endif
                        
                    </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Select Category*', ['class' => 'control-label']) !!}
                    <select name="category"  required="required" value="{{$asset_types->category}}"  class="form-control" >
                    <option value = "">Select Filer</option>
               @if($asset_types->category =="Employee")
                            <option value = 'Employee' selected>Employee</option>
                            <option value = 'Director'>Director</option>
                            <option value = 'Individual / AOP'>Individual / AOP</option>
                            <option value = 'Company'>Company</option>
                            <option value = 'Others'>Others</option>
                            @elseif($asset_types->category =="Director")
                            <option value = 'Employee' >Employee</option>
                            <option value = 'Director' selected>Director</option>
                            <option value = 'Individual / AOP'>Individual / AOP</option>
                            <option value = 'Company'>Company</option>
                            <option value = 'Others'>Others</option>
                            @elseif($asset_types->category =="Individual / AOP")
                            <option value = 'Employee' >Employee</option>
                            <option value = 'Director'>Director</option>
                            <option value = 'Individual / AOP' selected>Individual / AOP</option>
                            <option value = 'Company'>Company</option>
                            <option value = 'Others'>Others</option>
                            @elseif($asset_types->category =="Company")
                            <option value = 'Employee' >Employee</option>
                            <option value = 'Director'>Director</option>
                            <option value = 'Individual / AOP'>Individual / AOP</option>
                            <option value = 'Company' selected>Company</option>
                            <option value = 'Others'>Others</option>
                            @elseif($asset_types->category =="Others")
                            <option value = 'Employee' >Employee</option>
                            <option value = 'Director'>Director</option>
                            <option value = 'Individual / AOP'>Individual / AOP</option>
                            <option value = 'Company'>Company</option>
                            <option value = 'Others' selected>Others</option>
                            
                            
                        @endif
                    </select>
                </div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Sales Tax No*', ['class' => 'control-label']) !!}
    <input type="text" name="salestax" value="{{$asset_types->salestaxno}}" class="form-control" maxlength="10">
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Email*', ['class' => 'control-label']) !!}
    <input type="email" name="email" value="{{$asset_types->email}}" class="form-control" required >
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Contact*', ['class' => 'control-label']) !!}
    <input type="tel" name="contact" value="{{$asset_types->contact}}" data-mask="000-00000000" placeholder="0XX-XXXXXXX" class="form-control home_phone" required >
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Address*', ['class' => 'control-label']) !!}
    <textarea type="text" name="address" value="{{$asset_types->addresss}}" class="form-control"></textarea>
</div>