@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Vendor Details</h3>

                
            <a href="{{ route('vendor-list') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Name</th>
                    <td>{{ $asset_type->vendor_name}}</td>
                </tr>
                <tr>
                    <th>CNIC</th>
                    <td>{{ $asset_type->cnic}}</td>
                </tr>
                <tr>
                    <th>NTN Number</th>
                    <td>{{ $asset_type->ntn }}</td>
                    
                </tr>
                <tr>
                    <th>Sales Tax No</th>
                    <td>{{ $asset_type->salestaxno }}</td>
                    
                </tr>
                <tr>
                    <th>Type</th>
                    <td>{{ $asset_type->type }}</td>
                    
                </tr>
                <tr>
                    <th>Category</th>
                    <td>{{ $asset_type->category }}</td>
                    
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $asset_type->email }}</td>
                    
                </tr>
                <tr>
                    <th>Contact</th>
                    <td>{{ $asset_type->contact }}</td>
                    
                </tr>
                <tr>
                    <th>Address</th>
                    <td>{{ $asset_type->addresss }}</td>
                    
                </tr>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
