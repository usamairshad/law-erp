<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    <input type="text" name="name" class="form-control" required >
</div>
<div class="form-group col-md-3 @if($errors->has('cnic')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'CNIC*', ['class' => 'control-label']) !!}
    <input type="text" name="cnic"  class="form-control" required >
</div>
<div class="form-group col-md-3 @if($errors->has('NTN')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'NTN Number*', ['class' => 'control-label']) !!}
    <input type="text" name="ntn" class="form-control"  >
</div>
<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Select Filer Type*', ['class' => 'control-label']) !!}
                    <select name="filer"  required="required"  class="form-control" >
                    <option value = "">Select Filer</option>
               
                            <option value = 'Filer'>Filer</option>
                            <option value = 'Non-Filer'>Non-Filer</option>
                        
                    </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
                    {!! Form::label('name', 'Select Category*', ['class' => 'control-label']) !!}
                    <select name="category"  required="required"  class="form-control" >
                    <option value = "">Select Filer</option>
               
                            <option value = 'Employee'>Employee</option>
                            <option value = 'Director'>Director</option>
                            <option value = 'AOP / Individual '>AOP / Individual</option>
                            <option value = 'Company'>Company</option>
                            <option value = 'Others'>Others</option>
                        
                    </select>
                </div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Sales Tax No*', ['class' => 'control-label']) !!}
    <input type="text" name="salestax" class="form-control" maxlength="10">
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Email*', ['class' => 'control-label']) !!}
    <input type="email" name="email" class="form-control" required >
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Contact*', ['class' => 'control-label']) !!}
    <input type="tel" name="contact" data-mask="000-00000000" placeholder="0XX-XXXXXXX" class="form-control home_phone" required >
</div>
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Address*', ['class' => 'control-label']) !!}
    <textarea type="text" name="address" class="form-control"></textarea>
</div>