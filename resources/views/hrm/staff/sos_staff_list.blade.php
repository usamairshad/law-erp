@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">SOS Staff</h3>
                
          
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($staffs) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Staff Reg No</th>
                        <th>Staff Name</th>  
                        <th>City</th>
                        <th>Staff Type</th>
                        <th>Job Status</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>



                <tbody>
                    @if (count($staffs) > 0)
                        @foreach ($staffs as $staff)
                            <tr>
                                <td>{{ \App\Helpers\Helper::getStaffIDToRegNo($staff->id) }}</td>
                                <td>{{ \App\Helpers\Helper::getStaffIDToFirstName($staff->id) }}</td>
                                 <td>{{ $staff->city}}</td>
                                  <td>{{ $staff->staff_type}}</td>
                                   <td>{{ $staff->job_status}}</td>
                                <td>{{ $staff->status}}</td>
                           
                            <td>
                                @if($staff->status == 'InActive')
                                <a href="{{ route('staff_status_active', [$staff->id]) }}"
                                        class="btn btn-xs btn-success">Active</a>
                                @else($staff->status == 'Active')
                                <a href="{{ route('staff_status_inactive', [$staff->id]) }}"
                                        class="btn btn-xs btn-info">InActive</a>
                                @endif
                            </td> 

                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>













@stop

@section('javascript')

<script>
    $(document).ready(function() {
        $('.datatable').DataTable()
    });
</script>

@endsection
