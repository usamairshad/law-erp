@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Salary Approval Sheet </h3>
           

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}">
                <thead>

                    <tr>
                        <th>Branch Name</th>
                        <th>Staff Name</th>
                        <th>Gross Salary</th>
                        <th>Basic Salary</th>
                        <th>Modical</th>
                        <th>House Rent</th>
                        <th>Conveyance</th>
                        <th>Epf Amount</th>
                        <th>Eobi Amount</th>
                        <th>Tax Amount</th>

                        <th>Training</th>
                        <th>Loan</th>

                        <th>Net Salary</th>
                        
                    </tr>
                </thead>

                <tbody>
                    @if (count($data) > 0)
                        @foreach ($data as $Employee)
                            <tr data-entry-id="{{ $Employee->id }}">
                                <td>{{ \App\Helpers\Helper::branchIdToName($Employee->branch_id) }}</td>
                                <td>{{ \App\Helpers\Helper::getStaffIDToFirstName($Employee->staff_id) }}</td>
                                <td>{{ $Employee->gross_salary}}</td>

                                <td>{{ $Employee->basic_salary }}</td>
                                <td>{{ $Employee->modical_allowance }}</td>
                                <td>{{ $Employee->house_rent_allowance }}</td>

                                <td>{{ $Employee->conveyance_allowance }}</td>

                                <td>{{ $Employee->epf_amount }}</td>
                                <td>{{ $Employee->eobi_amount }}</td>
                                <td>{{ $Employee->tax_amount }}</td>

                                <td>{{ $Employee->training }}</td>
                                <td>{{ $Employee->loan_amount }}</td>
                                <td>{{ $Employee->net_salary }}</td>
                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>











    <!--------------------------------------modal for Warning------------------------------------>

    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> EOBI Government ID! </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">

                            <form method="POST" action="{{ url('eobi_notification/govt_reg_id') }}" style="width: 100%;" >
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Govt. Registration ID*</label>
                                    <input type="text" name="eobi_govt_id" class="form-control col-lg-12">
                                </div>

                                <div class="form-group">

                                    <label>Date*</label>


                                    <input type="date" name="date" class="form-control col-lg-12" placeholder="select Date*">

                                    <input type="hidden" name="user_id" id="warning_user_input">

                                </div>



                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!--------------------------------------modal for Warning------------------------------------>




@stop

@section('javascript')

    <script>


        $(document).ready(function() {
            $('.datatable').DataTable()
        });



        $(".warning").on("click", function() {

            $user_id = $(this).data("user-id");
        
            $('#warning_user_input').val($user_id);

        });

    </script>
@endsection
