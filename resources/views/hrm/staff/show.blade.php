@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Staff Details</h3>
             <a href="{{ route('admin.staff.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <center>
                <img src="{{asset('public/').'/'.$image}}" height="200" width="200">
            </center>
            <table class="table table-bordered table-striped">
                <tr>
                    <th>First Name</th>
                    <td>{{ $users->first_name ?? '' }}</td> 
                    <th>Last Name</th>
                    <td>{{ $users->last_name ?? '' }}</td>             
                </tr>
                <tr>
                    <th>Father Name</th>
                    <td>{{ $users->father_name  ?? ''}}</td>
                    <th>DOB</th>
                    <td>{{ $users->date_of_birth }}</td>
                   
                </tr>
                <tr>
                    <th>Registration No</th>
                    <td>{{ $users->reg_no }}</td>
                    <th>CNIC</th>
                    <td>{{ $users->cnic }}</td>
                    
                </tr>
                <tr>
                    <th>Gender</th>
                    <td>{{ $users->gender }}</td>
                    <th>Blood Group</th>
                    <td>{{ $users->blood_group }}</td>
                   
                </tr>
                <tr>
                    <th>Nationality</th>
                    <td>{{ $users->nationality }}</td>
                    <th>Mother Tongue</th>
                    <td>{{ $users->mother_tongue }}</td>
                    
                </tr>
                <tr>
                    <th>Address</th>
                    <td>{{ $users->address }}</td>
                    <th>Country</th>
                    <td>{{ $users->country }}</td>
                    
                </tr>
                <tr>
                    <th>City</th>
                    <td>{{ $users->city }}</td>
                    <th>Temp Address</th>
                    <td>{{ $users->temp_address }}</td>
                   
                </tr>
                <tr>
                    <th>Phone No</th>
                    <td>{{ $users->home_phone }}</td>
                    <th>Mobile No1</th>
                    <td>{{ $users->mobile_1 }}</td>
                    
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $users->email }}</td>
                  
                    <th>Role</th>
                    <td>{{ $role }}</td>
              
                </tr>
                    
                </tr>                
                <tr>
                    <th>Qualification</th>
                    <td>{{ $users->qualification }}</td>
                    <th>Salary</th>
                    <td>{{ $users->salary }}</td>
                    
                </tr>
                <tr>
                    <th>Primary Branch</th>
                    <td>{{ \App\Helpers\Helper::branchIdToName($users->branch_id) }}</td>
                    <th>Experience</th>
                    <td>{{ $users->experience }}</td>
                </tr>
                <tr>
                    <th>Other Branch</th>
                    

                    @foreach ($branches as $key => $value)

                        <td>{{ \App\Helpers\Helper::branchIdToName($value['branch_id']) }}</td>
            
            
                    @endforeach

                    
                

                </tr>
<!--

Removed after discussion with sir intikhar

                 <tr>
                    <th>Skill</th>
                    <td>{{ $users->skill }}</td>
                    <th>Probation period</th>
                    <td>{{ $users->probation }} days</td>
                </tr>
                <tr>
                    <th>Start Day of Contract</th>
                    <td>{{ $users->start_day_contract }}</td>
                    <th>End Day of Contract</th>
                    <td>{{ $users->end_day_contract }}</td>
                    
                </tr>
                <tr>
                    <th>Provident Fund</th>
                    <td>
                    @if($users->provident_fund==1)
                    YES
                    @else
                    NO
                    @endif</td>
                    <th>Provident Fund Deduction Per Month</th>
                    <td>{{ $users->provident_amount }} /-PKR</td>
                </tr>
                <tr>
                    <th>EOBI</th>
                    <td>
                    @if($users->EOBI==1)
                    YES
                    @else
                    NO
                    @endif</td>
                    <th>EOBI Deduction Per Month</th>
                    <td>{{ $users->eobi_amount }}</td>
                    
                </tr>
                <tr>
                    <th>Medical Allowances</th>
                    <td>{{$users->medical_allowances}}
                    
                    </td>
                    <th>House Rent</th>
                    <td>{{ $users->house_rent }} /-PKR</td>
                </tr>
                <tr>
                    <th>FBR Register</th>
                    <td>{{ $users->filer_type }}</td>
                    <th>NTN</th>
                    <td>{{ $users->ntn }}</td>
                </tr> -->
               
            </table>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });
    </script>
@endsection
