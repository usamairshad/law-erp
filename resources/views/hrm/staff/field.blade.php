 <style>
.accordion {
  background-color:#4a128cbf;
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #44034ac2; 
}

.panel {
  padding: 0 18px;
  display: none;
  background-color: white;
  overflow: hidden;


}
.form-group
{
    float:left !important;
}
</style>


<p class="accordion"><b>Personal Information</b></p>
<div class="panel">
<input type="hidden" name="branch_id" value="{{$branch_id}}">



<div class="form-group col-md-3">


        <label for="formGroupExampleInput">Staff Type*</label>
        <select required="" class="form-control" name="role_type"  id="role_type">
            <option disabled>--Select--</option>
            <option value="admission manager" <?php if (old('role_type')=="admission manager"): ?>
              selected
            <?php endif ?> >Admission Manager</option>
            <option value="library" <?php if (old('role_type')=="library"): ?>
              selected
            <?php endif ?> >Librarian</option>
            <option value="account" <?php if (old('role_type')=="Accountant"): ?>
              selected
            <?php endif ?> >Accountant</option>
            <option value="sr.coordinator" <?php if (old('role_type')=="sr.coordinator"): ?>
              selected
            <?php endif ?> >Coordinator</option>
            <option value="teacher" <?php if (old('role_type')=="teacher"): ?>
              selected
            <?php endif ?>> Teacher</option>
            <option value="Principal" <?php if (old('role_type')=="Principal"): ?>
              selected
            <?php endif ?>>Principal</option>
            <option value="fee manager" <?php if (old('role_type')=="fee manager"): ?>
              selected
            <?php endif ?> >Fee Manager</option>
            <option value="quality assurance" <?php if (old('role_type')=="quality assurance "): ?>
              selected
            <?php endif ?>>Quality Assurance</option>
            <option value="HR_head" <?php if (old('role_type')=="HR_head"): ?>
              selected
            <?php endif ?>>HR Head</option>
            <option value="HR" <?php if (old('role_type')=="HR"): ?>
              selected
            <?php endif ?>>Principle </option>
            <option value="Candidate" <?php if (old('role_type')=="Candidate"): ?>
              selected
            <?php endif ?>>Candidate</option>
            <option value="GM Finance" <?php if (old('role_type')=="GM Finance"): ?>
              selected
            <?php endif ?>>GM Finance</option>
            <option value="Supporting Staff" <?php if (old('role_type')=="Supporting Staff"): ?>
              selected
            <?php endif ?>> Supporting Staff</option>
            <option value="Tax Manager" <?php if (old('role_type')=="Tax Manager"): ?>
              selected
            <?php endif ?>> Tax Manager</option>
            <option value="Deputy Manager" <?php if (old('role_type')=="DGM Finance"): ?>
              selected
            <?php endif ?>>DGM Finance</option>
            <option value="Assistant Accounts" <?php if (old('role_type')=="Assistant Accounts"): ?>
              selected
            <?php endif ?>>Assistant Accounts</option>
            
        </select>
   </div>

<div class="form-group col-md-3 @if($errors->has('reg_no')) has-error @endif" style ="display:none;">
    {!! Form::label('reg_no', 'Registration No*', ['class' => 'control-label']) !!}
    <input type="text" name="reg_no" placeholder="XX-XXXXX" class="form-control reg_no" value="0" required unique maxlength="10" readonly>
</div>
<div class="form-group col-md-3 @if($errors->has('join_date')) has-error @endif">
    {!! Form::label('join_date', 'Join Date*', ['class' => 'control-label']) !!}
    <input type="date" name="join_date" value="{{ old('join_date') }}" class="form-control" required maxlength="10">
</div>
<div class="form-group col-md-3 @if($errors->has('first_name')) has-error @endif">
    {!! Form::label('first_name', 'First Name*', ['class' => 'control-label']) !!}
    <input type="text" name="first_name" class="form-control" value="{{ old('first_name') }}" required maxlength="50">
</div>

<div class="form-group col-md-3 @if($errors->has('middle_name')) has-error @endif" style ="display:none;">
    {!! Form::label('middle_name', 'Middle Name*', ['class' => 'control-label']) !!}
    <input type="text" name="middle_name" class="form-control" value="{{ old('middle_name') }}"  required maxlength="50">

</div>
<div class="form-group col-md-3 @if($errors->has('first_name')) has-error @endif">
    {!! Form::label('last_name', 'Last Name*', ['class' => 'control-label']) !!}
    <input type="text" name="last_name" class="form-control" value="{{ old('last_name') }}" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('father_name')) has-error @endif">
    {!! Form::label('father_name', 'Father Name*', ['class' => 'control-label']) !!}
    <input type="text" name="father_name" class="form-control" value="{{ old('father_name') }}" required maxlength="50">
</div>

<div class="form-group col-md-3 @if($errors->has('mother_name')) has-error @endif" style ="display:none;">
    {!! Form::label('mother_name', 'Mother Name*', ['class' => 'control-label']) !!}

    <input type="text" name="mother_name" class="form-control" value="{{ old('mother_name') }}" maxlength="50">
</div>


<div class="form-group col-md-3 @if($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
    <input type="email" id="email"  name="email" size="50" value="{{ old('email') }}" class="form-control" required>
</div>
<div class="form-group col-md-3 @if($errors->has('date_of_birth')) has-error @endif">
    {!! Form::label('date_of_birth', 'Date of Birth*', ['class' => 'control-label']) !!}
    <input type="date" name="date_of_birth" class="form-control" value="{{ old('date_of_birth') }}" required maxlength="10">
</div>
<div class="form-group col-md-3 @if($errors->has('gender')) has-error @endif">
    {!! Form::label('gender', 'Gender*', ['class' => 'control-label']) !!}
    <select name="gender" id="gender"  class="form-control">
      <option>Select Gender</option>
      <option value="male" <?php if (old('gender')=="male"): ?>
        selected
      <?php endif ?>>Male</option>
      <option value="female" <?php if (old('gender')=="female"): ?>
        selected
      <?php endif ?>>Female</option>
    </select>
</div>

<div style="none" class="form-group col-md-3 @if($errors->has('blood_group')) has-error @endif">
    {!! Form::label('blood_group', 'Blood Group', ['class' => 'control-label']) !!}
    <input type="text" name="blood_group" value="{{ old('blood_group') }}" class="form-control">
</div>
<div class="form-group col-md-3 @if($errors->has('mother_tongue')) has-error @endif" style ="display:none;">
    {!! Form::label('mother_tongue', 'Mother Tongue*', ['class' => 'control-label']) !!}
    <input type="text" name="mother_tongue" class="form-control" >

</div>
</div>

<p class="accordion"><b>Contact Information</b></p>
<div class="panel">

<div class="form-group col-md-3 @if($errors->has('nationality')) has-error @endif">
    {!! Form::label('nationality', 'Nationality*', ['class' => 'control-label']) !!}
    <input type="text" name="nationality" value="{{ old('nationality') }}"  class="form-control" required>
</div>

<div class="form-group  col-md-3 @if($errors->has('city')) has-error @endif">
    {!! Form::label('city', 'City*', ['class' => 'control-label']) !!}
    <input type="text" name="city" value="{{ old('city') }}"  class="form-control" required >
</div>
<div class="form-group  col-md-3 @if($errors->has('address')) has-error @endif">
    {!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
    <input type="text" name="address" value="{{ old('address') }}" class="form-control" required >
</div>

<div class="form-group col-md-3 @if($errors->has('temp_address')) has-error @endif">
    {!! Form::label('temp_address', 'Temporary Address', ['class' => 'control-label']) !!}

    <input type="text" name="temp_address" value="{{ old('temp_address') }}" class="form-control">



</div>
<div class="form-group  col-md-3 @if($errors->has('home_phone')) has-error @endif">
    {!! Form::label('home_phone', 'Home Phone', ['class' => 'control-label']) !!}
    <input type="tel" name="home_phone" class="form-control home_phone" data-mask="000-00000000" placeholder="0XX-XXXXXXX" value="{{ old('home_phone') }}"  maxlength="100">
</div>
<div class="form-group  col-md-3 @if($errors->has('mobile_1')) has-error @endif">
    {!! Form::label('mobile_1', 'Mobile 1*', ['class' => 'control-label']) !!}
    <input type="tel" id="mobile_1" value="{{ old('mobile_1') }}" data-mask="0000-0000000" name="mobile_1" class="form-control phone" placeholder="03XX-XXXXXXX" required>
</div>
<div class="form-group  col-md-3 @if($errors->has('mobile_2')) has-error @endif" style ="display:none;">
    {!! Form::label('mobile_2', 'Mobile 2', ['class' => 'control-label']) !!}
    <input type="tel" id="mobile_2" data-mask="0000-0000000" name="mobile_2" class="form-control phone" value="{{ old('mobile_2') }}" placeholder="03XX-XXXXXXX" >
</div>

<div class="form-group  col-md-3 @if($errors->has('cnics')) has-error @endif">
    {!! Form::label('cnic', 'CNIC*', ['class' => 'control-label']) !!}
    <!-- <input type="text" name="cnic" placeholder="XXXXX-XXXXXXX-X" data-mask="00000-0000000-0" required="required" class="form-control cnic" /> -->
    <input type="text" id="cnics" name="cnic" data-mask="00000-0000000-0" placeholder="00000-0000000-0" value="{{ old('cnic') }}"  required="required" class="form-control cnics" />
</div>
</div>

<p class="accordion"><b>Job Description</b></p>
<div class="panel">
<div class="form-group col-md-3 @if($errors->has('job_status')) has-error @endif">
    {!! Form::label('job_status', 'Job Status*', ['class' => 'control-label']) !!}
    <select name="job_status" class="form-control" required>
      <option>Select Job Status</option>
      <option value="Full Time" <?php if (old('job_status')=="Full Time"): ?>
        selected
      <?php endif ?> >Full Time</option>
      <option value="Lecture base" <?php if (old('job_status')=="Lecture base"): ?>
        selected
      <?php endif ?> >Lecture Base</option>
    </select>
</div>

<div class="form-group col-md-3 @if($errors->has('filertype')) has-error @endif">
                {!! Form::label('filertype', 'Filer Type*', ['class' => 'control-label']) !!}
                <select name="filertype" class="form-control" required>
                  <option>Select Type</option>
                  <option value="Filer" <?php if (old('filertype')=="Filer"): ?>
                    selected
                  <?php endif ?> >Filer</option>
                  <option value="Non-FIler" <?php if (old('filertype')=="Non-FIler"): ?>
                    
                  <?php endif ?> >Non-Filer</option>
                  
                </select>
            
        </div>
        <div class="form-group  col-md-3 @if($errors->has('ntn')) has-error @endif">
    <label>NTN*</label>
    <input type="text" required="required" value="{{ old('ntn') }}"  name="ntn" min="0"  class="form-control"  />
</div>
<div class="form-group  col-md-3 @if($errors->has('salary')) has-error @endif">
    <label>Gross Salary*</label>
    <input type="number"  name="salary" id="salary" value="{{ old('salary') }}"   min="0" required="required" class="form-control salary" max="1000000" />
</div>
<div class="form-group  col-md-3 @if($errors->has('salary')) has-error @endif">
    <label>Basic Salary*</label>
    <input type="number" readonly name="basic_salary" value="{{ old('basic_salary') }}" id="basic_salary"  min="0" required="required" class="form-control salary" max="1000000" />
</div>

<div class="form-group col-md-3 @if($errors->has('working_hours')) has-error @endif">
    {!! Form::label('working_hours', 'Working hours*', ['class' => 'control-label']) !!}
     <input type="number" name="working_hours" value="{{ old('working_hours') }}"  class="form-control" min="1" required maxlength="8">
</div>

<div class="form-group col-md-3 @if($errors->has('qualification')) has-error @endif">
    {!! Form::label('qualification', 'Qualification*', ['class' => 'control-label']) !!}
    <input type="text" name="qualification" value="{{ old('qualification') }}" class="form-control" required maxlength="60">
</div>
<div class="form-group col-md-3 @if($errors->has('experience')) has-error @endif">
    {!! Form::label('experience', 'Year of Experience*', ['class' => 'control-label']) !!}
     <input type="number" name="experience" value="{{ old('experience') }}"  class="form-control" min="1" required maxlength="100">
</div>
<div class="form-group col-md-3 @if($errors->has('other_info')) has-error @endif">
    {!! Form::label('other_info', 'Other Info', ['class' => 'control-label']) !!}
    <input type="text" name="other_info" value="{{ old('other_info') }}" class="form-control">
</div>
<div class="form-group col-md-3 @if($errors->has('staff_image')) has-error @endif">
    {!! Form::label('staff_image', 'Staff Image*', ['class' => 'control-label', ]) !!}
    <!-- {!! Form::file('images[]',array('class'=>'send-btn')) !!} -->
     <input type="file" name="staff_image" value="{{ old('staff_image') }}" required class="form-control" />
</div>
<div class="form-group col-md-3 @if($errors->has('skill')) has-error @endif">
    {!! Form::label('skill', 'Skill*', ['class' => 'control-label']) !!}
    <input type="text" name="skill" value="{{ old('skill') }}" class="form-control" required maxlength="100">
</div>

<div class="form-group  col-md-3 @if($errors->has('status')) has-error @endif">
    {!! Form::label('status', 'Status*', ['class' => 'control-label']) !!}
    {!! Form::select('status',[ ''=>'Select  Status','Active' => 'Active','InActive' => 'InActive','Pending' => 'Pending'],old('status') , ['class' => 'form-control', 'required']) !!}
    @if($errors->has('status'))
        <span class="help-block">
            {{ $errors->first('status') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('staff_type')) has-error @endif">
    {{ Form::label('Primary Branch Name*') }}
    <!-- {{ Form::select('branch_id', $branchList, null, array( 'class'=>'form-control js-example-basic-single', 'placeholder'=>'Please select ...','required', 'multiple')) }}
 -->
    <select style="width: 100%" name="branch_id"  class="form-control" required="required">

      @foreach($branchList as $key => $branch)
        <option value="{{$branch['id']}}">{{$branch['name']}}</option>
      @endforeach
      
    </select>
</div>




<div class="form-group col-md-12">
  {!! Form::label('branch_id', 'Other Branches', ['class' => 'control-label']) !!}
  <select style="width: 100%;" class="js-example-basic-single" name="branches[]" multiple="multiple">

        @foreach($branchList as $key => $branch)

      <option  value="{{$branch['id']}}">{{$branch['name']}}</opton>
      @endforeach
  </select>
</div>





 </div>
 <p class="accordion"><b>Probation Period</b></p>

<div class="panel">
<div class="row">
    <div class="col-md-12">
            <div class="form-group col-md-4 @if($errors->has('start_day')) has-error @endif" >
                {!! Form::label('start_day', 'Start Day of Contract', ['class' => 'control-label']) !!}

                <input type="date" name="start_day_contract" value="{{ old('start_day_contract') }}" class="form-control"  maxlength="10">
            </div>
            <div class="form-group col-md-4 @if($errors->has('join_date')) has-error @endif" >
                {!! Form::label('end_day', 'End Day of Contract', ['class' => 'control-label']) !!}
                <input type="date" name="end_day_contract" value="{{ old('end_day_contract') }}" class="form-control" required maxlength="10">

            </div>

           {{-- <div class="form-group @if($errors->has('probation_period')) has-error @endif" style ="margin-left:20px;">
                {!! Form::label('probation_period', 'Probation Period*', ['class' => 'control-label']) !!}
                <select name="probation" required="required" id="probation" class="form-control" required>
                  <option>Select Probation</option>
                  <option value="0">No Probation</option>
                  <option value="30">One Month</option>
                  <option value="90">Three Months</option>
                  <option value="180">Six Months</option>
                   <option value="365">One Year</option>
                  
                </select>
            
        </div> --}}
        <div class="form-group col-md-4 @if($errors->has('probation_period')) has-error @endif">
                {!! Form::label('probation_period', 'Probation Period*', ['class' => 'control-label']) !!}
                <select name="probation" class="form-control" required>
                  <option>Select Probation</option>
                  <option value="No Probation">No Probation</option>
                  <option value="One Month">One Month</option>
                  <option value="Three Months">Three Months</option>
                  <option value="Six Months">Six Months</option>
                   <option value="One Year">One Year</option>
                  
                </select>
            
        </div> 







    </div>
</div>
</div>

<p class="accordion"><b>Allowances/Deduction</b></p>

<div class="panel">
        <div class="form-group col-md-4 @if($errors->has('medical')) has-error @endif">
            {!! Form::label('medical', 'Medical Allowances*', ['class' => 'control-label']) !!}
            <input type="text" readonly name="medical" value="{{ old('medical') }}" id="medical" class="form-control" required maxlength="100">
        </div>
        <div class="form-group col-md-4 @if($errors->has('conveyance')) has-error @endif">
            {!! Form::label('conveyance', 'Conveyance*', ['class' => 'control-label']) !!}
            <input type="text" readonly name="conveyance" value="{{ old('conveyance') }}" id="conveyance" class="form-control" required maxlength="100">
        </div>
        <div class="form-group col-md-4 @if($errors->has('HouseRent')) has-error @endif">
            {!! Form::label('HouseRent', 'House Rent*', ['class' => 'control-label']) !!}
            <input type="text" readonly name="HouseRent" value="{{ old('HouseRent') }}" id="house_rent" class="form-control" required maxlength="100">
        </div>

<!--         <div class="form-group col-md-3 @if($errors->has('providentFund')) has-error @endif">
                {!! Form::label('providentFund', 'Provident Fund*', ['class' => 'control-label']) !!}
                <select name="providentFund" class="form-control" required>
                  <option>Select Provident Fund</option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                  
                </select>
            
        </div>


        <div class="form-group col-md-3 @if($errors->has('eobi')) has-error @endif">
                {!! Form::label('eobi', 'EOBI*', ['class' => 'control-label']) !!}
                <select name="eobi" class="form-control" required>
                  <option>Select EOBI</option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                  
                </select>
            
        </div> -->
        
</div>












<p class="accordion"><b>HBL Account Details</b></p>

<div class="panel">


  <div class="row" style = "text-align:center;">

      <label style="width: 100px;" ><input style="float: left;"  checked=""  type="radio" class="radio" name="bank" value="yes"> Yes</label>
      <label style="width: 100px;" ><input style="float: left;"  type="radio" class="radio" name="bank" value="no"> No</label> 
    
  </div>




  <div class="row">
    <div id="bankexist" class = "col-md-12">
          <div class="form-group col-md-4 @if($errors->has('account_title')) has-error @endif" style = "float:left;">
              {!! Form::label('account_title', 'Account Title*', ['class' => 'control-label']) !!}
              <input type="text" name="account_title" class="form-control" >
          </div>
          <div class="form-group col-md-4 @if($errors->has('account_number')) has-error @endif" style = "float:left;">
              {!! Form::label('account_number', 'Account Number*', ['class' => 'control-label']) !!}
              <input type="text" name="account_number" class="form-control" >
          </div>
          <div class="form-group col-md-4 @if($errors->has('bank_branch')) has-error @endif" style = "float:left;">
              {!! Form::label('bank_branch', 'Bank Branch*', ['class' => 'control-label']) !!}
              <input type="text" name="bank_branch" class="form-control" >
          </div>    
    </div>
  </div>


  <div class="row">
      <div id="banknotexist" style="display: none; text-align:center;">

        <div class="form-group col-md-12 ">
           <h3>Need To Create HBL Account</h3>
        </div>
        
      </div>  
    
  </div>
  


        
</div>

















<!-- <div class="form-group col-md-3 @if($errors->has('staff_type')) has-error @endif">
    {!! Form::label('staff_type', 'Staff Types*', ['class' => 'control-label']) !!}
    <select required="required" name="staff_type" class="form-control">
      <option disabled="disabled" selected="selected">Select Type</option>
      <option value="teacher">Teacher</option>
      <option value="coordinator">Coordinator</option>
      <option value="Principal">Principal</option>
    </select>
</div>
 -->


<script type="text/javascript">



$("#seeAnotherField").change(function() {
  if ($(this).val() != "Teacher") {
    $('#otherFieldDiv').show();
    $('#otherField').attr('required', '');
    $('#otherField').attr('data-error', 'This field is required.');
  } else {
    $('#otherFieldDiv').hide();
    $('#otherField').removeAttr('required');
    $('#otherField').removeAttr('data-error');
  }
});
$("#seeAnotherField").trigger("change");
        $(function(){
            $('#role_type').change(function(){
               $("#roles option").remove();;
               var id = $(this).val();
               console.log(id)
               $.ajax({
                  url : '{{ route( 'admin.load-roles-staff' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'get',
                  dataType: 'json',
                  success: function( result )
                  {     
                        console.log(result)
                        $('#roles').append($('<option disabled selected>--Select--</option>'));
                        $.each( result, function(k, v) {
                            $('#roles').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
</script>





