<div class="box-body" style = "margin-top:40px;">
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Select Branch</label>
                    <select name="select_branch" id="select_branch" class="form-control select_branch">
                        <option>Select Branch</option>
                          @foreach($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->name}}</option>                                  
                          @endforeach    
                    </select>
                </div>
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Month</label>
                    <select name="select_month" id="select_month" class="form-control select_month">
                        <option>Select Month</option>
                          <option value="januaray">january</option>
                          <option value="february">february</option>
                          <option value="march">march</option>
                          <option value="april">april</option>
                          <option value="may">may</option>
                          <option value="june">june</option>
                          <option value="july">july</option>
                          <option value="august">august</option>
                          <option value="september">september</option>
                          <option value="october">october</option>
                          <option value="november">november</option>
                          <option value="december">december</option>


                          
                    </select>
                </div>
                <div class="col-xs-3 col-md-3 form-group" style="margin-bottom: 0px !important;float:left;">
                            {!! Form::label('narration', 'Bank A/C*', ['class' => 'control-label']) !!}
                            {!! Form::select('bank_ledger_id', array(), null, ['id' => 'entry_item-ledger_id-1', 'style' => 'width: 100%;', 'class' => 'form-control base-data-ajax']) !!}
                        </div>

                {{--
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Total Salary</label>
                    <input type="text" class="form-control" name="staff_total_salary" id="staff_total_salary" value="">

                </div>
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Total EOBI</label>
                    
                </div> 
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Total EPF</label>
                    <input type="text" class="form-control" name="staff_total_epf" id="staff_total_epf" value="">

                </div> --}}   



                <!-- /.box-body -->


<div class="box-footer" style="padding-top:30px;">
                    {!! Form::submit(trans('Pay'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
                </div>
                
        
            </div>



    <div class="panel-body pad table-responsive col-md-12">
            <table class="table table-bordered table-striped datatable" >
                <thead>
                    <tr>
                        

                        <th>First name</th>
                        <th>Gross Salary</th>
                        <th>Basic Salary</th>
                        <th>Conveyance</th>
                        <th>House Rent</th>
                        <th>Medical Allowance</th>
                        <th>Total Leaves</th>
                        <th>Leave Deduction</th>
                        <th>Gross Salary Before Adj.</th>
                        <th>EPF</th>
                        <th>EOBI</th>
                        <th>Tax Amount</th>
                        <th>Training</th>
                        <th>Loan</th>
                        <th style="font-weight: bold; color:blue;">Net Salary</th>
                        

                        
                    </tr>
                </thead>

                <tbody id="stafflist">
                    
                   

                </tbody>


            </table>
        </div>







@section('javascript')
<script src="{{ url('public/js/admin/entries/voucher/bank_voucher/bank_payment_sheet/create_modify.js') }}" type="text/javascript"></script>


<script type = "text/javascript">

$(document).on('change','.select_month',function () 
{

            var branch_id=$('#select_branch').val();
            var month_id=$('#select_month').val();                
       
             
              

            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_branch_staff')!!}',
                data:{'branch_id':branch_id,'month_id':month_id},
                dataType:'json',//return data will be json
                success:function(stafflistdat){
                    // here price is coloumn name in products table data.coln name

                    
                    console.log('staff aa gey');
                     console.log(stafflistdat);
                     console.log(stafflistdat.total_salary);



                    //$html = '<option value="null">select course</option>';
                    $list='';
                    var i=0;
                    stafflistdat.Staffs.forEach((dataa) => {

                        $list += `
                        
                        <tr>                            
                            
                                 <input type="hidden" id="staff-${dataa.id}" name="staff[]" data-row-id="${dataa.id}" class="staff" readonly="true"  value="${dataa.id}">    
                             
                            <td>
                                <input type="text" id="name-${dataa.id}" name="name[]" data-row-id="${dataa.id}" class="data_id" readonly="true"  value="${dataa.first_name}">    
                            </td>
                            <td>
                                <input type="number" id="salary-${dataa.id}" name="salary[]" data-row-id="${dataa.id}" class="salary" readonly="true"  value="${dataa.salary}">
                                    </td>


                            <td>
                                <input type="number" id="basic_salary-${dataa.id}" name="basic_salary[]" data-row-id="${dataa.id}" class="basic_salary" readonly="true"  value="${dataa.basic_salary}">
                                    
                            </td>
                            <td>
                                <input type="text" id="conveyance-${dataa.id}" name="conveyance[]" data-row-id="${dataa.id}" class="conveyance" readonly="true"  value="${dataa.conveyance}">
                            </td>
                            <td>
                                <input type="text" id="house_rent-${dataa.id}" name="house_rent[]" data-row-id="${dataa.id}" class="house_rent" readonly="true"  value="${dataa.house_rent}">
                            </td>
                            <td>
                                <input type="text" id="medical_allowances-${dataa.id}" name="medical_allowances[]" data-row-id="${dataa.id}" class="medical_allowances" readonly="true"  value="${dataa.medical_allowances}">

                            </td>
                            <td>
                                <input type="number" id="leaves-${dataa.id}" name="leaves[]" data-row-id="${dataa.id}" class="leaves" value="">
                            </td>
                            <td>
                                <input type="number" id="leave_deduction-${dataa.id}" name="leave_deduction[]" data-row-id="${dataa.id}" class="leave_deduction" readonly="true"  value="${dataa.missed_lectures_deduction}">   
                            </td>
                            <td>
                                <input type="number" id="gross_before_adj-${dataa.id}" name="gross_before_adj[]" data-row-id="${dataa.id}" class="gross_before_adj" readonly="true"  value="">   
                            </td>
                            <td>
                                <input type="number" id="epf_amount-${dataa.id}" name="epf_amount[]" data-row-id="${dataa.id}" class="epf_amount" readonly="true"  value="${dataa.epf_amount}">
                            </td>

                            <td>
                                <input type="number" id="eobi_amount-${dataa.id}" name="eobi_amount[]" data-row-id="${dataa.id}" class="eobi_amount" readonly="true"  value="${dataa.eobi_amount}">
                            </td>

                            <td>
                                <input type="number" id="tax_amount-${dataa.id}" name="tax_amount[]" data-row-id="${dataa.id}" class="tax_amount"  value="${dataa.tax_amount}">

                                
                            </td>
                                


                            <td>
                                <input type="number" id="miscellaneous-${dataa.id}" name="training[]" data-row-id="${dataa.id}" class="miscellaneous"  value="">
                            </td>
                            <td>
                                <input type="number" id="loan-${dataa.id}" name="loan[]" data-row-id="${dataa.id}" class="loan"  value="">
                            </td>

                            <td>
                                <input type="number" id="net_salary-${dataa.id}" name="net_salary[]" data-row-id="${dataa.id}" class="net_salary" readonly="true"  value="${dataa.net_salary}">
                            </td>

                            


                        </tr>
                        
                        
                            
                        
                        `;

                        i++;
                    });


                    $list += `
                        
                        
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>


                            <td>
                                
                            </td>
                            

                            <td>
                                <input type="number" readonly class="form-control" name="staff_total_epf" id="staff_total_epf" value="${stafflistdat.total_epf}">
                            </td>
                            <td>
                                <input type="number" readonly class="form-control" name="staff_total_eobi" id="staff_total_eobi" value="${stafflistdat.total_eobi}">
                            </td>
                                
                            <td>
                                <input type="number" readonly class="form-control" name="staff_total_tax" id="staff_total_tax" value="${stafflistdat.total_tax}">
                                
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="number" readonly class="form-control" name="staff_total_salary" id="staff_total_salary" value="${stafflistdat.total_salary}">

                            </td>
                        </tr>
                        
                            
                        
                        `;

                                        
                    $("#staff_total_salary").val(stafflistdat.total_salary);
                    $("#staff_total_eobi").val(stafflistdat.total_eobi);
                    $("#staff_total_epf").val(stafflistdat.total_epf);
                    $("#stafflist").html($list);
                    

                   
                    

                },
                error:function(){
                }
            });

});
//Courses


$(document).on('change','.data_id',function () 
{
    var rowId = $(this).attr('data-row-id');
    var value = $('#staff-'+rowId).val();    
    alert(value);
});

$(document).on('change','.leaves',function () 
{
    var rowId = $(this).attr('data-row-id');
    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    


    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);

    var gross_before_adj=salary-deduction;


    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous;
 

    $('#leave_deduction-'+rowId).val(deduction);
    $('#gross_before_adj-'+rowId).val(gross_before_adj);

    $('#net_salary-'+rowId).val(net_salary);


});


$(document).on('change','.miscellaneous',function () 
{
    var rowId = $(this).attr('data-row-id');
    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var loan_amount = $('#loan-'+rowId).val();
 

    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);
    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous;
     
    var total_salary= $('#staff_total_salary').val();
    total_salary= total_salary - miscellaneous;



    $('#leave_deduction-'+rowId).val(deduction);
    $('#net_salary-'+rowId).val(net_salary);
    $('#staff_total_salary').val(total_salary);


});



$(document).on('change','.loan',function () 
{
    var rowId = $(this).attr('data-row-id');
    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var loan_amount = $('#loan-'+rowId).val(); 

    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);
    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous - loan_amount;
    var total_salary= $('#staff_total_salary').val();
    total_salary= total_salary - loan_amount;

    $('#leave_deduction-'+rowId).val(deduction);
    $('#net_salary-'+rowId).val(net_salary);
    $('#staff_total_salary').val(total_salary);

});










</script>



@endsection

