 
 <style>
.accordion {
  background-color: #bce1ec;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #3898ea; 
}

.panel {
  padding: 0 18px;
  display: none;
  background-color: white;
  overflow: hidden;
}
.form-group
{
    float:left !important;
}
</style>

<p class="accordion"><b>Personal Information</b></p>
<div class="panel">
<input type="hidden" name="branch_id" value="{{$branch_id}}">
<div class="form-group col-md-3 @if($errors->has('reg_no')) has-error @endif">
    {!! Form::label('reg_no', 'Registration No*', ['class' => 'control-label']) !!}
    <input type="text" name="reg_no" value="{{$Staff->reg_no}}" class="form-control reg_no" placeholder="XX-XXXXX" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('join_date')) has-error @endif">
    {!! Form::label('join_date', 'Join Date*', ['class' => 'control-label']) !!}
    <input type="date" name="join_date"value="{{$Staff->join_date}}" class="form-control" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('first_name')) has-error @endif">
    {!! Form::label('first_name', 'First Name*', ['class' => 'control-label']) !!}
    <input type="text" name="first_name"  value="{{$Staff->first_name}}"class="form-control" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('middle_name')) has-error @endif">
    {!! Form::label('middle_name', 'Middle Name*', ['class' => 'control-label']) !!}
    <input type="text" name="middle_name" value="{{$Staff->middle_name}}"class="form-control" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('first_name')) has-error @endif">
    {!! Form::label('last_name', 'Last Name*', ['class' => 'control-label']) !!}
    <input type="text" name="last_name"  value="{{$Staff->last_name}}" class="form-control" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('father_name')) has-error @endif">
    {!! Form::label('father_name', 'Father Name*', ['class' => 'control-label']) !!}
    <input type="text" name="father_name"  value="{{$Staff->father_name}}" class="form-control" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('mother_name')) has-error @endif">
    {!! Form::label('mother_name', 'Mother Name*', ['class' => 'control-label']) !!}
    <input type="text" name="mother_name"  value="{{$Staff->mother_name}}" class="form-control" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
    <input type="email" id="email"  value="{{$Staff->email}}"  name="email" size="50" class="form-control" required>
</div>
<div class="form-group col-md-3 @if($errors->has('date_of_birth')) has-error @endif">
    {!! Form::label('date_of_birth', 'Date of Birth*', ['class' => 'control-label']) !!}
    <input type="date" name="date_of_birth"  value="{{$Staff->date_of_birth}}" class="form-control" required maxlength="50">
</div>
<div class="form-group col-md-3 @if($errors->has('gender')) has-error @endif">
    {!! Form::label('gender', 'Gender*', ['class' => 'control-label']) !!}
    <select name="gender" id="gender" value="{{$Staff->gender}}" class="form-control" required >
      <option>Select Gender</option>
       @if($gender = 'male')
      <option value="male"  selected="male">Male</option>
       <option value="female">Female</option>
      @elseif($gender = 'female')
      <option value="male">Male</option>
      <option value="female"selected="female">Female</option>
      @endif
    </select>
</div>
<div class="form-group  col-md-3 @if($errors->has('cnic')) has-error @endif">
    {!! Form::label('cnic', 'CNIC*', ['class' => 'control-label']) !!}
    <input type="text" name="cnic"  value="{{$Staff->cnic}}" placeholder="XXXXX-XXXXXXX-X" data-mask="00000-0000000-0" required="required" class="form-control cnic" />
</div>
<div class="form-group col-md-3 @if($errors->has('blood_group')) has-error @endif">
    {!! Form::label('blood_group', 'Blood Group*', ['class' => 'control-label']) !!}
    <input type="text" name="blood_group"  value="{{$Staff->blood_group}}" class="form-control" required>
</div>
</div>
<p class="accordion"><b>Contact Information</b></p>
<div class="panel">

<div class="form-group col-md-3 @if($errors->has('nationality')) has-error @endif">
    {!! Form::label('nationality', 'Nationality*', ['class' => 'control-label']) !!}
    <input type="text" name="nationality"  value="{{$Staff->nationality}}" class="form-control" required>
</div>
<div class="form-group  col-md-3 @if($errors->has('address')) has-error @endif">
    {!! Form::label('address', 'Address*', ['class' => 'control-label']) !!}
    <input type="text" name="address"  value="{{$Staff->address}}" class="form-control" required="required" >
</div>

<div class="form-group col-md-3 @if($errors->has('nationality')) has-error @endif">
    {!! Form::label('nationality', 'Nationality*', ['class' => 'control-label']) !!}
    <select name="nationality" value="{{$Staff->nationality}}" class="form-control" required >
      <option>Select Nationality</option>
      <option value="PAKISTANI"  selected="PAKISTANI">Pakistan</option>
    </select>
</div>
<div class="form-group col-md-3 @if($errors->has('country')) has-error @endif">
    {{ Form::label('Country Name*') }}
    <input type="text" name="country"  value="{{$Staff->country}}" class="form-control" required="required" >
</div>
<div class="form-group col-md-3 @if($errors->has('city')) has-error @endif">
    {{ Form::label('City Name*') }}
    <input type="text" name="city"  value="{{$Staff->city}}" class="form-control" required="required" >
</div>

<div class="form-group col-md-3 @if($errors->has('temp_address')) has-error @endif">
    {!! Form::label('temp_address', 'Temporary Address*', ['class' => 'control-label']) !!}
    <input type="text" name="temp_address"  value="{{$Staff->temp_address}}" class="form-control" required>
</div>
<div class="form-group  col-md-3 @if($errors->has('home_phone')) has-error @endif">
    {!! Form::label('home_phone', 'Home Phone*', ['class' => 'control-label']) !!}
    <input type="tel" name="home_phone"  value="{{$Staff->home_phone}}" class="form-control home_phone" data-mask="000-00000000" placeholder="0XX-XXXXXXX" required maxlength="100">
</div>
<div class="form-group  col-md-3 @if($errors->has('mobile_1')) has-error @endif">
    {!! Form::label('mobile_1', 'Mobile 1*', ['class' => 'control-label']) !!}
    <input type="tel" id="mobile_1" data-mask="0000-0000000" name="mobile_1" value="{{$Staff->mobile_1}}" class="form-control phone" placeholder="03XX-XXXXXXX" required>
</div>
<div class="form-group  col-md-3 @if($errors->has('mobile_2')) has-error @endif">
    {!! Form::label('mobile_2', 'Mobile 2', ['class' => 'control-label']) !!}
    <input type="tel" id="mobile_2" data-mask="0000-0000000" name="mobile_2"  value="{{$Staff->mobile_2}}" class="form-control phone" placeholder="03XX-XXXXXXX" >
</div>


</div>
<div class="form-group col-md-3 @if($errors->has('job_status')) has-error @endif">
    {!! Form::label('job_status', 'Job Status*', ['class' => 'control-label']) !!}
    <select name="job_status" class="form-control" value="{{$Staff->job_status}}" required>
      <option>Select Job Status</option>

      @if($job_status = 'Lecture base')
      <option value="Lecture base" selected="Lecture base">Lecture base</option>
      <option value="Full Time">Full Time</option>
       @elseif($Staff->job_status == 'Full Time')
      <option value="Lecture base">Lecture base</option>
      <option value="Full Time" selected="Full Time">Full Time</option>
      @endif
    </select>
</div>
<div class="form-group col-md-3 @if($errors->has('working_hours')) has-error @endif">
    {!! Form::label('working_hours', 'Working hours*', ['class' => 'control-label']) !!}
     <input type="number" name="working_hours" value="{{$Staff->working_hours}}" class="form-control" min="1" required maxlength="8">
</div>
<div class="form-group col-md-3 @if($errors->has('qualification')) has-error @endif">
    {!! Form::label('qualification', 'Qualification*', ['class' => 'control-label']) !!}
    <input type="text" name="qualification"  value="{{$Staff->qualification}}" class="form-control" required maxlength="60">
</div>
<div class="form-group col-md-3 @if($errors->has('experience')) has-error @endif">
    {!! Form::label('experience', 'Year of Experience*', ['class' => 'control-label']) !!}
     <input type="number" name="experience" class="form-control"  value="{{$Staff->experience}}" min="1" required maxlength="100">
</div>
<div class="form-group col-md-3 @if($errors->has('other_info')) has-error @endif">
    {!! Form::label('other_info', 'Other Info', ['class' => 'control-label']) !!}
    <input type="text" name="other_info" value="{{$Staff->other_info}}" class="form-control">
</div>
<div class="form-group col-md-3 @if($errors->has('staff_image')) has-error @endif">
    {!! Form::label('staff_image', 'Staff Image', ['class' => 'control-label', ]) !!}
     {!! Form::file('images[]',array('class'=>'send-btn')) !!}

     {{--<input type="file" name="staff_image" value="{{$Staff->staff_image}}" required="required" class="form-control" /> --}}
</div>
<div class="form-group col-md-3 @if($errors->has('skill')) has-error @endif">
    {!! Form::label('skill', 'Skill*', ['class' => 'control-label']) !!}
    <input type="text" name="skill" class="form-control"  value="{{$Staff->skill}}"required maxlength="100">
</div>


<div class="form-group  col-md-3 @if($errors->has('status')) has-error @endif">
    {!! Form::label('status', 'Status*', ['class' => 'control-label']) !!}
     <select name="status" class="form-control" required> 
    @if($status = 'Active')
    <option value="Active"  selected="Active">Active</option>
    <option value="InActive">InActive</option>
    <option value="Pending">Pending</option>
    @elseif($status = 'InActive')
    <option value="Active">Active</option>
    <option value="Pending">Pending</option>
    <option value="InActive" selected="InActive">InActive</option>
    @elseif($status = 'Pending')
    <option value="Active">Active</option>
    <option value="Pending"selected="Pending">Pending</option>
    <option value="InActive">InActive</option>
    @endif
    </select>
</div>
<div class="form-group  col-md-3 @if($errors->has('salary')) has-error @endif">
    {!! Form::label('salary', 'Salary*', ['class' => 'control-label']) !!}
    <input type="number" name="salary" value="{{$Staff->salary}}" min="0" required="required" class="form-control" />
</div>
<div class="form-group col-md-3 @if($errors->has('staff_type')) has-error @endif">
    {{ Form::label('Branch Name*') }}
    {!! Form::select('branch_id', $branchList,old('branch_name'), ['class' => 'form-control select2']) !!}
</div>

<div class="row">
    <div class="col-md-12">
      
            <div class="form-group @if($errors->has('start_day')) has-error @endif">
                {!! Form::label('start_day', 'Start Day of Contract*', ['class' => 'control-label']) !!}
                <input type="date" name="start_day_contract" value="{{$Staff->start_day_contract}}" class="form-control" required maxlength="10">
            </div>
            <div class="form-group @if($errors->has('join_date')) has-error @endif">
                {!! Form::label('end_day', 'End Day of Contract*', ['class' => 'control-label']) !!}
                <input type="date" name="end_day_contract" value="{{$Staff->end_day_contract}}" class="form-control" required maxlength="10">
            </div>
          {{--  <div class="form-group @if($errors->has('probation_period')) has-error @endif">
                {!! Form::label('probation_period', 'Probation Period*', ['class' => 'control-label']) !!}
                <select name="probation" class="form-control" value="{{$Staff->probation}}" required>
                  <option>Select Probation Days</option>
                  @if($Staff->probation == '0')
                  <option value="0" selected="0">0</option>
                  <option value="30">30</option>
                  <option value="90">90</option>
                  @elseif($Staff->probation == '30')
                  <option value="0">0</option>
                  <option value="30" selected="0">30</option>
                  <option value="90">90</option>
                  @elseif($Staff->probation == '90')
                  <option value="0">0</option>
                  <option value="30">30</option>
                  <option value="90" selected="90">90</option>
                  @endif
                </select>
                

        </div> --}}

         <div class="form-group @if($errors->has('probation_period')) has-error @endif">
                {!! Form::label('probation_period', 'Probation Period*', ['class' => 'control-label']) !!}
                <select name="probation" class="form-control" value="{{$Staff->probation}}" required>
                  <option>Select Probation </option>
                  @if($Staff->probation == 'No Probation')
                  <option value="No Probation" selected="No Probation">No Probation</option>
                  <option value="One Month">One Month</option>
                  <option value="Three Months">Three Months</option>
                  <option value="Six Months">Six Months</option>
                  <option value="One Year">One Year</option>
                  @elseif($Staff->probation == 'One Month')
                  <option value="No Probation" >No Probation</option>
                  <option value="One Month" selected="One Month">One Month</option>
                  <option value="Three Months">Three Months</option>
                  <option value="Six Months">Six Months</option>
                   <option value="One Year">One Year</option>
                  @elseif($Staff->probation == 'Three Months')
                  <option value="No Probation" >No Probation</option>
                  <option value="One Month" >One Month</option>
                  <option value="Three Months" selected="Three Months">Three Months</option>
                  <option value="Six Months">Six Months</option>
                  <option value="One Year">One Year</option>
                  @elseif($Staff->probation == 'Six Months')
                  <option value="No Probation" >No Probation</option>
                  <option value="One Month" >One Month</option>
                  <option value="Three Months" >Three Months</option>
                  <option value="Six Months" selected="Six Months">Six Months</option>
                  <option value="One Year">One Year</option>
                  @elseif($Staff->probation == 'One Year')
                  <option value="No Probation" >No Probation</option>
                  <option value="One Month" >One Month</option>
                  <option value="Three Months" >Three Months</option>
                  <option value="Six Months">Six Months</option>
                  <option value="One Year" selected="One Year">One Year</option>
                  @endif
                </select>
                

        </div>
    </div>
    <div class="form-group col-md-3 @if($errors->has('medical')) has-error @endif">
    {!! Form::label('medical', 'Medical Allowances*', ['class' => 'control-label']) !!}
    <input type="text" name="medical" value="{{$Staff->medical_allowances}}" class="form-control" required maxlength="100">
</div>
<div class="form-group col-md-3 @if($errors->has('conveyance')) has-error @endif">
    {!! Form::label('conveyance', 'Conveyance*', ['class' => 'control-label']) !!}
    <input type="text" name="conveyance" value="{{$Staff->conveyance}}" class="form-control" required maxlength="100">
</div>
<div class="form-group col-md-3 @if($errors->has('HouseRent')) has-error @endif">
    {!! Form::label('HouseRent', 'House Rent*', ['class' => 'control-label']) !!}
    <input type="text" name="HouseRent" value="{{$Staff->house_rent}}" class="form-control" required maxlength="100">
</div>

<div class="form-group col-md-3 @if($errors->has('providentFund')) has-error @endif">
                {!! Form::label('providentFund', 'Provident Fund*', ['class' => 'control-label']) !!}
                <select name="providentFund" class="form-control" value="{{$Staff->provident_fund}}" required>
                <option>Select Provident Fund</option>
                @if($Staff->provident_fund == 0)
                  <option value="0" selected>No</option>
                  <option value="1">Yes</option>
                  @else
                  <option value="1" selected>Yes</option>
                  <option value="0">No</option>
                  @endif
                </select>
            
        </div>
        <div class="form-group col-md-3 @if($errors->has('pf_per_month')) has-error @endif">
    {!! Form::label('pf_per_month', 'Providen Fund Deduct Per Month*', ['class' => 'control-label']) !!}
    <input type="text" name="pf_per_month" value="{{$Staff->provident_amount}}" class="form-control" required maxlength="100">
</div>

        <div class="form-group col-md-3 @if($errors->has('eobi')) has-error @endif">
                {!! Form::label('eobi', 'EOBI*', ['class' => 'control-label']) !!}
                <select name="eobi" class="form-control" required>
                  <option>Select EOBI</option>
                  @if($Staff->EOBI == 0)
                  <option value="0" selected>No</option>
                  <option value="1">Yes</option>
                  @else
                  <option value="1" selected>Yes</option>
                  <option value="0">No</option>
                  @endif
                  
                </select>
            
        </div>
        <div class="form-group col-md-3 @if($errors->has('eobi_per_month')) has-error @endif">
    {!! Form::label('EOBI_per_month', 'EOBI Deduct Per Month*', ['class' => 'control-label']) !!}
    <input type="text" name="eobi_per_month" value="{{$Staff->eobi_amount}}" class="form-control" required maxlength="100">
</div>
<div class="form-group col-md-3 @if($errors->has('filertype')) has-error @endif">
                {!! Form::label('filertype', 'Filer Type*', ['class' => 'control-label']) !!}
                <select name="filertype" class="form-control" value="{{$Staff->filer_type}}" required>
                @if($Staff->filer_type == 'Filer')
                  <option value="Filer" selected>Filer</option>
                  <option value="Non-Filer">Non Filer</option>
                  @else
                  <option value="Filer">Filer</option>
                  <option value="Non-Filer" selected>Non Filer</option>
                  @endif
                  
                </select>
            
        </div>
        <div class="form-group  col-md-3 @if($errors->has('ntn')) has-error @endif">
    <label>NTN*</label>
    <input type="text"  name="ntn" min="0" value="{{$Staff->ntn}}" class="form-control"  />
</div>
</div>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>