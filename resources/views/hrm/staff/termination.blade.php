@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Termination</h3>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($users) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        
                        <th>Staff Reg No</th>
                        <th>Staff Name</th>
                        <th>Email</th>
                        <th>Branch</th>   
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($users) > 0)
                        @foreach ($users as $user)
                            <tr>
                                
                                <td>{{ $user->reg_no }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ \App\Helpers\Helper::branchIdToName($user->branch_id) }}</td>
                                <td>{{ \App\Helpers\Helper::rolesIdToName($user->role_id) }}</td> 
                                <td>
                                    <a href="{{ url('admin/re-activate/user/') . '/' . $user->id }}"
                                        onclick="return confirm('Are you sure ');"
                                        data-user-id="{{ $user->id }}"
                                        class="btn btn-xs btn-success">Re-Activate
                                    </a>
                                </td>        
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
 <script>
    $(document).ready(function() {
        $('.datatable').DataTable()
    });
</script>
@endsection
