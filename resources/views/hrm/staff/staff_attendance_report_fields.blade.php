<div class="box-body" style = "margin-top:40px;">
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Select Branch</label>
                    <select name="select_branch" id="select_branch" class="form-control select_branch">
                        <option>Select Branch</option>
                          @foreach($data as $branch)
                            <option value="{{$branch->id}}">{{$branch->name}}</option>                                  
                          @endforeach    
                    </select>
                </div>
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Month</label>
                    <select name="select_month" id="select_month" class="form-control select_month">
                        <option>Select Month</option>
                          <option value="januaray">january</option>
                          <option value="february">february</option>
                          <option value="march">march</option>
                          <option value="april">april</option>
                          <option value="may">may</option>
                          <option value="june">june</option>
                          <option value="july">july</option>
                          <option value="august">august</option>
                          <option value="september">september</option>
                          <option value="october">october</option>
                          <option value="november">november</option>
                          <option value="december">december</option>


                          
                    </select>
                </div>
                <div class="col-lg-3 form-group" style="float: left">
                    <label>Year</label>
                    <input type="number" class="form-control select_year" id="select_year" name="select_year" value="2021">
                </div>




                {{--
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Total Salary</label>
                    <input type="text" class="form-control" name="staff_total_salary" id="staff_total_salary" value="">

                </div>
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Total EOBI</label>
                    
                </div> 
                <div class="col-lg-3 form-group"  style = "float:left;">
                    <label>Total EPF</label>
                    <input type="text" class="form-control" name="staff_total_epf" id="staff_total_epf" value="">

                </div> --}}   



                <!-- /.box-body -->
                <div class="box-footer" style="padding-top:30px;">
                    

                    <a href="#" class="btn btn-danger" name="search" id="search">
                        Search
                    </a>

                </div>


                
        
            </div>



    <div class="panel-body pad table-responsive col-md-12">
            <table class="table table-bordered table-striped datatable" >
                <thead>
                    <tr>
                        

                        <th>Staff Id</th>
                        <th>Staff Name</th>
                        <th>Month</th>
                        <th>Year</th>
                        <th>Branch Id</th>
                        <th>Total</th>
                        

                        
                    </tr>
                </thead>

                <tbody id="stafflist">
                    
                   

                </tbody>


            </table>
        </div>







@section('javascript')

<script type = "text/javascript">

//$(document).on('click','.search',function () 
$("#search").click(function()
{

    
            var branch_id=$('#select_branch').val();
            var month_id=$('#select_month').val();
            var year_id=$('#select_year').val();


       
             
              

            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_branch_staff_attendance_report')!!}',
                data:{'branch_id':branch_id,'month_id':month_id,'year_id':year_id},
                dataType:'json',//return data will be json
                success:function(stafflistdat){
                    // here price is coloumn name in products table data.coln name

                    
                    console.log('staff aa gey');
                     console.log(stafflistdat);
                     



                    //$html = '<option value="null">select course</option>';
                    $list='';
                    var i=0;
                    stafflistdat.attendance_report.forEach((dataa) => {

                        console.log(dataa);



                        $list += `
                        
                        <tr>                            
                            <td>
                                 <input type="text" id="staff-${dataa.staff_id}" name="staff[]" data-row-id="${dataa.staff_id}" class="staff" readonly="true"  value="${dataa.staff_id}">    
                            </td>                                                
                            <td>
                                 <input type="text" id="staff-${dataa.staff_id}" name="staff[]" data-row-id="${dataa.staff_id}" class="staff" readonly="true"  value="${dataa.staff_name}">    
                            </td>
                            <td>
                                <input type="text" id="month-${dataa.staff_id}" name="month[]" data-row-id="${dataa.staff_id}" class="month_id" readonly="true"  value="${dataa.month}">    
                            </td>
                            <td>
                                <input type="number" id="year-${dataa.staff_id}" name="year[]" data-row-id="${dataa.staff_id}" class="year" readonly="true"  value="${dataa.year}">
                                    </td>


                            <td>
                                <input type="number" id="branch_id-${dataa.staff_id}" name="branch_id[]" data-row-id="${dataa.staff_id}" class="branch_id" readonly="true"  value="${dataa.branch_id}">
                                    
                            </td>
                            <td>
                                <input type="text" id="total-${dataa.staff_id}" name="total[]" data-row-id="${dataa.staff_id}" class="total" readonly="true"  value="${dataa.total}">
                            </td>
                            

                            


                        </tr>
                        
                        
                            
                        
                        `;

                        i++;
                    });


                    

                                        
                    $("#staff_total_salary").val(stafflistdat.total_salary);
                    $("#staff_total_eobi").val(stafflistdat.total_eobi);
                    $("#staff_total_epf").val(stafflistdat.total_epf);
                    $("#stafflist").html($list);
                    

                   
                    

                },
                error:function(){
                }
            });

});
//Courses


$(document).on('change','.data_id',function () 
{
    var rowId = $(this).attr('data-row-id');
    var value = $('#staff-'+rowId).val();    
    alert(value);
});

$(document).on('change','.leaves',function () 
{
    var rowId = $(this).attr('data-row-id');
    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    


    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);

    var gross_before_adj=salary-deduction;


    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous;
 

    $('#leave_deduction-'+rowId).val(deduction);
    $('#gross_before_adj-'+rowId).val(gross_before_adj);

    $('#net_salary-'+rowId).val(net_salary);


});


$(document).on('change','.miscellaneous',function () 
{
    var rowId = $(this).attr('data-row-id');
    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var loan_amount = $('#loan-'+rowId).val();
 

    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);
    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous;
     
    var total_salary= $('#staff_total_salary').val();
    total_salary= total_salary - miscellaneous;



    $('#leave_deduction-'+rowId).val(deduction);
    $('#net_salary-'+rowId).val(net_salary);
    $('#staff_total_salary').val(total_salary);


});



$(document).on('change','.loan',function () 
{
    var rowId = $(this).attr('data-row-id');
    var miscellaneous = $('#miscellaneous-'+rowId).val();
    var leaves = $('#leaves-'+rowId).val();
    var salary = $('#salary-'+rowId).val();
    var epf_amount = $('#epf_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var eobi_amount = $('#eobi_amount-'+rowId).val();
    var loan_amount = $('#loan-'+rowId).val(); 

    var one_day_salary=salary/30; 
    var deduction=leaves*one_day_salary;
    deduction=deduction.toFixed(2);
    var net_salary=salary - deduction - epf_amount - eobi_amount - miscellaneous - loan_amount;
    var total_salary= $('#staff_total_salary').val();
    total_salary= total_salary - loan_amount;

    $('#leave_deduction-'+rowId).val(deduction);
    $('#net_salary-'+rowId).val(net_salary);
    $('#staff_total_salary').val(total_salary);

});










</script>



@endsection

