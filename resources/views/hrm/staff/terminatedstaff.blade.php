@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Terminated Users</h3>
           {{--  @if (Gate::check('erp_class_time_table_create'))
                <a href="{{ route('admin.class_timetable.create') }}" class="btn btn-success pull-right">Add New Class TimeTable</a>
            @endif
            --}}
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>User Name</th>
                        <th>User Email</th>
                        <th>Branch Name</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($data) > 0)
                        @foreach ($data as $teacher)
                            <tr data-entry-id="{{ $teacher->id }}">
                 
                                <td>{{ $teacher->id }}</td>
                                <td>{{ $teacher->name }}</td>
                                <td>{{ $teacher->email }}</td>
                                <td>{{ \App\Helpers\Helper::userIdToBranchName($teacher->id) }}</td>
                                <td>
                                    <a href="{{ url('admin/re-activate/user/') . '/' . $teacher->id }}"
                                            onclick="return confirm('Are you sure ');"
                                            data-user-id="{{ $teacher->id }}"
                                            class="btn btn-xs btn-success">Re-Activate
                                    </a>
                                   
                                 
                                </td>

                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
            
    </script>

@endsection
