@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Employee Laon</h3>
                
          
                                </div>

        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($staffs) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Staff ID</th>
                        <th>Loan Type</th>  
                        <th>Amount</th>
                        <th>Installments</th>
                        <th>Monthly Installlment</th>
                        <th>Reason</th>
                        <th>Approved By CEO</th>
                        <th>Approved By HR</th>
                    </tr>
                </thead>



                <tbody>
                    @if (count($staffs) > 0)
                        @foreach ($staffs as $staff)
                            <tr>
                                <td>{{ \App\Helpers\Helper::getStaffIDToFirstName($staff->staff_id) }}</td>


                                <td>{{ $staff->type}}</td>
                                 <td>{{ $staff->amount}}</td>
                                  <td>{{ $staff->installments}}</td>
                                   <td>{{ $staff->monthly_installment}}</td>
                                <td>{{ $staff->reason}}</td>
                           
                            <td>
                                @if($staff->approved_by_ceo == 0)
                                    <a href="{{ url('loan_request_approve_ceo', [$staff->id]) }}"
                                            class="btn btn-xs btn-primary">Approve</a>
                                    <a href="{{ url('loan_request_reject_ceo', [$staff->id]) }}"
                                            class="btn btn-xs btn-primary">Regect</a>        
                                @elseif($staff->approved_by_ceo == 1)
                                    <a href="#" class="btn btn-xs btn-success">Approved</a>
                                @elseif($staff->approved_by_ceo == 2)
                                    <a href="#" class="btn btn-xs btn-danger">Rejected</a>

                                @endif 



                            </td> 
                            <td>
                                @if($staff->approved_by_hr == 0)
                                <a href="{{ url('loan_request_approve_hr', [$staff->id]) }}"
                                        class="btn btn-xs btn-primary">Approve</a>
                                <a href="{{ url('loan_request_reject_hr', [$staff->id]) }}"
                                        class="btn btn-xs btn-primary">Regect</a>        
                                @elseif($staff->approved_by_hr == 1)
                                    <a href="#" class="btn btn-xs btn-success">Approved</a>
                                @elseif($staff->approved_by_hr == 2)
                                    <a href="#" class="btn btn-xs btn-danger">Rejected</a>

                                @endif 
                            </td>

                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>













@stop

@section('javascript')

<script>
    $(document).ready(function() {
        $('.datatable').DataTable()
    });
</script>

@endsection
