@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Warning List</h3>   
            @if(Gate::check('skills_create'))
                <a href="{{ route('hrm.skills.create') }}" class="btn btn-success pull-right">Warning</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>

                    <th>Sender</th>
                    <th>Reciever</th>
                    <th>Title</th>
                    <th>Reason</th>

                    <th>Justification</th>
                    @if($role != "HR_head")
                    <th>Actions</th>
                    @endif
                </tr>
                </thead>

                <tbody>
                @if (count($data) > 0)
                    @foreach ($data as $Skill)
                        <tr data-entry-id="{{ $Skill->id }}">
                            
                            <td>{{ \App\Helpers\Helper::userIdToName($Skill->sid ) }}</td>
                            <td>{{ \App\Helpers\Helper::userIdToName($Skill->rid ) }}</td>
                            <td>{{ $Skill->title }}</td>
                            <td>{{ $Skill->reason }}</td>
                            <td>{{ $Skill->justification }}</td>
                            @if($role != "HR_head")
                            <td>

                                @if( $Skill->justification ==null)
                                 <a href="javascript:void(0)" data-user-id="{{ $Skill->id }}"
                                            data-target="#warningModal" data-toggle="modal"
                                            class="btn btn-xs btn-warning warning">Justification

                                </a>
                                @else
                                <button data-user-id="{{ $Skill->id }}"
                                          disabled  style = "pointer-event:none;"
                                            class="btn btn-xs btn-success success">Justification</button

                                </a>
                                @endif


                            </td>
                            @endif
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>










   <!--------------------------------------modal for Warning------------------------------------>

    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Give Justification!</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">

                            <form method="POST" action="{{ url('admin/justification') }}" style="width: 100%;" >
                                {{ csrf_field() }}


                                <div class="form-group">

                                    <label for="exampleInputEmail1">Justification</label>
                                    <br>

                                    <textarea  class="from-control col-lg-12" name="reason" id="reason" rows="5"></textarea>

                                    <input type="hidden" name="user_id" id="warning_user_input">

                                </div>



                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>


@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });



        $(".warning").on("click", function() {

            $user_id = $(this).data("user-id");

            

            $('#warning_user_input').val($user_id);

        });



    </script>


@endsection