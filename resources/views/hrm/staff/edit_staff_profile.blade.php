@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Staff Edit Form</h1>
    </section>
@stop

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Edit Staff Details</h3>
            <a href="{{ route('admin.staff.index') }}" style = "float:right" class="btn btn-success pull-right" >Back</a>
        </div>

        <div class="box-body" style ="margin-top: 40px;">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <form method="post" action="{{url('update-staff-profile',$employee->id)}}" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{csrf_token()}}">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="something">
                            Role Type:</label>

                        <select name="staff_type" class="form-control " id="role"  disabled>
                            <option selected   >--Select--</option>
                            @foreach($roles as $role)

                                <option value="{{$role->name ?? ''}}" {{$employee->staff_type == $role->name  ? 'selected' : ''}}>{{$role->name}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Registration No*</label>
                        <input type="text" name="reg_no" value="{{$employee->reg_no}}" class="form-control" id="month" disabled >
{{--                        <input type="hidden" name="hidden_id" value="{{$employee->id}}">--}}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Join Date*</label>
                        <input type="date" class="form-control" value="{{$employee->join_date}}" name="join_date" id="meter_no" disabled required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">First Name*</label>
                        <input type="text" class="form-control" value="{{$employee->first_name}}" name="first_name" id="meter_no"  >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3" style ="display:none;">
                    <div class="form-group">
                        <label for="something">
                            Middle Name*:</label>
                       <input type="text" value="{{$employee->middle_name}}" name="middle_name" class="form-control"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Last Name*</label>
                        <input type="text" value="{{$employee->last_name}}" name="last_name" class="form-control" id="month"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Father Name*</label>
                        <input type="text" class="form-control" value="{{$employee->father_name}}" name="father_name" id="meter_no" required>
                    </div>
                </div>
                <div class="col-md-3" style ="display:none;">
                    <div class="form-group" >
                        <label for="formGroupExampleInput">Mother Name*</label>
                        <input type="text" class="form-control" value="{{$employee->mother_name}}" name="mother_name" id="meter_no" >
                    </div>
                </div>
            
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="something">
                            Email*</label>
                       <input type="text" value="{{$employee->email}}" name="email" class="form-control" disabled>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Date of Birth*</label>
                        <input type="date" value="{{$employee->date_of_birth}}" name="date_of_birth" class="form-control" id="month"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Gender*</label>

                        <select class="form-control" name="gender"  >

                        
                        @if($employee->gender == 'male')
                            <option value="Selcet Gender" >Select Gender</option>
                            <option value="male" selected>Male</option>
                            <option value="female" >Female</option>
                        @elseif($employee->gender == 'female')
                            <option value="Selcet Gender" >Select Gender</option>
                            <option value="male">Male</option>
                            <option value="female" selected="" >Female</option>
                        @else
                            <option value="Selcet Gender" selected>Select Gender</option>
                            <option value="male" >Male</option>
                            <option value="female">Female</option>    
                        @endif
                            
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Blood Group*</label>
                        <input type="text" class="form-control" value="{{$employee->blood_group}}" name="blood_group" id="meter_no"  >
                    </div>
                </div>
                <div class="col-md-3" style ="display:none;">
                    <div class="form-group">
                        <label for="something">
                            Mother Tongue*</label>
                       <input type="text" name="mother_tongue" value="{{$employee->mother_tongue}}" class="form-control"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Nationality*</label>
                        <input type="text" name="nationality" value="{{$employee->nationality}}" class="form-control" id="month"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Address*</label>
                        <input type="text" class="form-control" value="{{$employee->address}}" name="address" id="meter_no"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="something">
                            Qualification*</label>
                     <input type="text" name="qualification" value="{{$employee->qualification}}" class="form-control"  >
                    </div>
                </div>
<!--                 <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Country*</label>
                        <input type="text" class="form-control" value="{{$employee->country}}" name="country" id="meter_no" disabled>
                    </div>
                </div> -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="something">
                            City*:</label>
                   <input type="text" name="city" value="{{$employee->city}}" class="form-control" disabled>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Temporary Address</label>
                        <input type="text" name="temp_address" value="{{$employee->temp_address}}" class="form-control" id="month"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Home Phone</label>
                        <input type="text" class="form-control" value="{{$employee->home_phone}}" name="home_phone" id="meter_no"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Mobile 1*</label>
                        <input type="text" class="form-control" value="{{$employee->mobile_1}}" name="mobile_1" id="meter_no"  >
                    </div>
                </div>
                <div class="col-md-3" style ="display:none;">
                    <div class="form-group">
                        <label for="something">
                            Mobile 2</label>
                <input type="text" name="mobile_2" value="{{$employee->mobile_2}}" class="form-control"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">CNIC*:</label>
                        <input type="text" name="cnic" value="{{$employee->cnic}}" class="form-control" id="month"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Job Status*</label>
                        <select name="job_status" class="form-control" >
                            @if($employee->job_status == 'Full Time')
                                <option value="Full Time" selected>Full Time</option>
                            @elseif($employee->job_status == 'Lecture Base')
                                <option value="Lecture Base" selected>Lecture Base</option>
                                @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Filer Type*</label>
                        <select name="filer_type" class="form-control">
                            @if($employee->filer_type == 'Filer')
                                <option value="Filer" selected>Filer</option>
                                @elseif($employee->filer_type == 'Non-Filer')
                                <option value="Non-Filer" selected>Non-Filer</option>
                                @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="something">
                            NTN</label>
                       <input type="number" name="ntn" value="{{$employee->ntn}}" class="form-control" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Basic Salary*</label>
                        <input type="text" name="salary" value="{{$employee->salary}}" class="form-control" id="month" disabled>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Branch Name*</label>
                        <select name="branch_id" class="form-control" id="branch" disabled>
                            <option selected   >--Select--</option>
                            @foreach($branches as $branch)
                                <option value="{{$branch->id ?? ''}}" {{$employee->branch_id == $branch->id  ? 'selected' : ''}}>{{$branch->name ?? ''}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Working hours*</label>
                        <input type="text" class="form-control" value="{{$employee->working_hours}}" name="working_hours" id="meter_no" disabled >
                    </div>
                </div>
            </div>
            <div class="row">
                
                <!-- <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Year of Experience*</label>
                        <input type="text" name="experience" value="{{$employee->experience}}" class="form-control" id="month"  >
                    </div>
                </div> -->
<!--                 <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Other Info</label>
                        <input type="text" class="form-control" value="{{$employee->other_info}}" name="other_info" id="meter_no"  >
                    </div>
                </div> -->
<!--                 <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Staff Image</label>
                        <input type="file" class="form-control" name="staff_image"  >
                    </div>
                </div>
 -->            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="something">
                            Skill*:</label>
                    <input type="text" class="form-control" name="skill" value="{{$employee->skill}}"  >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">

                        <label for="formGroupExampleInput">Status*</label>
                       <select name="status" class="form-control" disabled>

                               <option value="Active"  @if($employee->status == 'Active') selected @endif >Active</option>

                               <option value="In Active" @if($employee->status == 'In Active') selected @endif >In Active</option>

                               <option value="Pending" @if($employee->status == 'Pending') selected @endif>Pending</option>


                       </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Start Day of Contract*</label>
                        <input type="date" class="form-control" value="{{$employee->start_day_contract}}" name="start_day_contract" id="meter_no" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="formGroupExampleInput">End Day of Contract*</label>
                        <input type="date" class="form-control" value="{{$employee->end_day_contract}}" name="end_day_contract" id="meter_no">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="something">
                            Probation Period*</label>
                        <select name="probation" class="form-control" id="branch">

                                <option value="No Probation" @if($employee->probation == 'No Probation')  selected   @endif>No Probation</option>

                                <option value="One Month" @if($employee->probation == 'One Month')  selected   @endif>One Month</option>

                                <option value="Three Months" @if($employee->probation == 'Three Months')  selected   @endif>Three Months</option>
                                <option value="Six Months" @if($employee->probation == 'Six Months')  selected   @endif>Six Months</option>
                                <option value="One Year" @if($employee->probation == 'One Year')  selected   @endif>One Year</option>


                        </select>
                    </div>
                </div>



                
            </div>
            
        </div>


        <div class="box-footer">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>

            </form>
    </div>
@stop
