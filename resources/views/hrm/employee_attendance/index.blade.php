@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Attendance</h1>
    </section>
   {{csrf_field()}}
    <input type="hidden" name="name" value="abc">
    <div class="form-group col-md-3 @if($errors->has('from_date')) has-error @endif" id="from_date_div">
        {!! Form::label('from_date', 'From Date', ['class' => 'control-label']) !!}
        {!! Form::text('from_date', old('from_date'),
         ['class' => 'form-control datepicker' ,'id'=>'from_date']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('to_date')) has-error @endif" id="to_date_div">
        {!! Form::label('to_date', 'To Date', ['class' => 'control-label']) !!}
        {!! Form::text('to_date', old('to_date'),
         ['class' => 'form-control datepicker' ,'id'=>'to_date']) !!}
    </div>
    <div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif" id="user_div">
        {!! Form::label('user_id', 'User ', ['class' => 'control-label']) !!}
        {!! Form::select('user_id', $Employees , old('user_id'),
         ['class' => 'form-control select2'  ,'id' => 'user_id']) !!}
    </div>

    <button id="search_button" onclick="FormControls.fetchFilterRecord();" type="button" style="margin-top: 25px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search </b> </button>
    <a  href="{{route('hrm.attendance.sync')}}" type="button" style="margin-top: 25px;" class="btn  btn-sm btn-flat btn-success"><b>&nbsp;Sync Attendance</b> </a>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('attendance_create'))
                {{--<a href="{{ route('hrm.attendance.requests') }}" class="btn btn-success pull-right">Attendance Request</a>--}}
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($EmployeeAttendance) > 0 ? 'datatable' : '' }}" id="users-table" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>User</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>

    <div class="box box-primary" style="display: none;">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">Exceptional Cases</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($EmployeeAttendance) > 0 ? 'datatable' : '' }}" id="users-table-f" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>User</th>
                    <th>Job Title</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Type</th>
                    <th>Date Created</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script src="{{ url('public/js/hrm/employee_attendance/list.js') }}" type="text/javascript"></script>
@endsection