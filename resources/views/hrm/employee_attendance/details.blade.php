@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Contacts</h1>
    </section>
@stop

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body basic_info">

                    <div class="box-header with-border">
                        <h3 class="box-title">General Information</h3>
                        @if(Gate::check('contacts_edit'))
                            <a href="{{ route('marketing.contacts.edit',[$Contact->id]) }}" class="btn btn-success pull-right">Edit</a>
                        @endif
                    </div>

                    <div class="panel-body pad table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Contact Name</th>
                                    <td colspan="3">@if($Contact->first_name){{ $Contact->first_name }} {{ $Contact->last_name  }} @else{{ 'N/A' }}@endif</td>
                                </tr>

                                <tr>
                                    <th>Office Phone </th>
                                    <td>@if($Contact->office_phone){{ $Contact->office_phone }}@else{{ 'N/A' }}@endif</td>
                                    <th>Mobile Phone </th>
                                    <td>@if($Contact->mobile_phone){{ $Contact->mobile_phone }}@else{{ 'N/A' }}@endif</td>

                                </tr>
                                <tr>
                                    <th>Title </th>
                                    <td>@if($Contact->title){{ $Contact->title  }}@else{{ 'N/A' }}@endif</td>
                                    <th>Department </th>
                                    <td>@if($Contact->department){{ $Contact->department  }}@else{{ 'N/A' }}@endif</td>
                                </tr>


                                <tr>
                                    <th>Primary Email</th>
                                    <td>@if($Contact->primary_email){{ $Contact->primary_email }}@else{{ 'N/A' }}@endif</td>
                                    <th>Secondary Email</th>
                                    <td>@if($Contact->secondary_email){{ $Contact->secondary_email }}@else{{ 'N/A' }}@endif</td>
                                </tr>

                                <tr>
                                    <th>Assigned To</th>
                                    <td>@if($Contact->assigned_to){{ $Contact->emp_name }}@else{{ 'N/A' }}@endif</td>
                                    <th>Data bank</th>
                                    <td>@if($Contact->databank_id){{ $Contact->databank_name }}@else{{ 'N/A' }}@endif</td>
                                </tr>

                                <tr>
                                    <th width="15%">Street </th>
                                    <td width="35%">@if($Contact->street){{ $Contact->street }}@else{{ 'N/A' }}@endif</td>
                                    <th width="15%">City</th>
                                    <td width="35%">@if($Contact->city){{ $Contact->city }}@else{{ 'N/A' }}@endif</td>
                                </tr>
                                <tr>
                                    <th width="15%">State </th>
                                    <td width="35%">@if($Contact->state){{ $Contact->state }}@else{{ 'N/A' }}@endif</td>
                                    <th width="15%">Postal Code</th>
                                    <td width="35%">@if($Contact->postal_code){{ $Contact->postal_code }}@else{{ 'N/A' }}@endif</td>
                                </tr>
                                <tr>
                                    <th width="15%">Country </th>
                                    <td width="35%">@if($Contact->country){{ $Contact->country }}@else{{ 'N/A' }}@endif</td>
                                    <th width="15%">Fax #</th>
                                    <td width="35%">@if($Contact->fax){{ $Contact->fax }}@else{{ 'N/A' }}@endif</td>

                                </tr>
                                <tr>
                                    <th width="15%">Description </th>
                                    <td colspan="3">@if($Contact->description){{ $Contact->description }}@else{{ 'N/A' }}@endif</td>


                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>



            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/marketing/contacts/create_modify.js') }}" type="text/javascript"></script>
@endsection