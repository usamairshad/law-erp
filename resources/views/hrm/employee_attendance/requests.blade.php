@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Attendance Requests</h1>
    </section>

@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('attendance_create'))
                <a href="{{ route('hrm.attendance.create') }}" class="btn btn-success pull-right">Create Request Attendance</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped datatable" id="users-table" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>User</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th>Approved By</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                @if(count($EmployeeAttendance) > 0)
                    @foreach($EmployeeAttendance as $key => $val)
                        <tr>
                            <td>{{$val->id}}</td>
                            <td>{{$val->first_name}}</td>
                            {{--<td>{{ date('d-m-Y h:m a', strtotime($val->date_time))}}</td>--}}
                            <td> {{ $val->date_time }}</td>
                            <td>{{Config::get('hrm.attendance_request_array.'.$val->status) }}</td>

                            <td>@if($val->approved_by){{ $val->approved_by_name->first_name }}@else{{ 'N/A' }}@endif</td>

                            <td>{{date('d-m-Y h:m a', strtotime($val->created_at))}}</td>
                            <td>
                                @if( ( Gate::allows('employee_attendance_approve') || Gate::allows('employee_attendance_final_approve') ) && $val->user_id != Auth::user()->id )
                                    {{--@if(Gate::allows('attendance_manage') || Gate::allows('employee_attendance_final_approve') )--}}
                                    <form method="get" action="active" >
                                          {{csrf_field()}}
                                         <input type="hidden" value='{{$val->id}}' name="id" >
                                         <input   value="Approve"  class="btn btn-xs btn-primary" type="submit"  onclick="return confirm('Are you sure you want to approve this? ')">
                                    </form>
                                @else
                                    {{'No Action'}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">No Record Found</td>

                    </tr>
                @endif

                </tbody>

            </table>
        </div>
    </div>


@stop

@section('javascript') 
    <script src="{{ url('public/js/hrm/employee_attendance/list.js') }}" type="text/javascript"></script>
@endsection