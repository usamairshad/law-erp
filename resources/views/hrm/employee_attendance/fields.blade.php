@if(old('first_name'))
        <div class="form-group col-md-3 @if($errors->has('first_name')) has-error @endif">
            {!! Form::label('first_name', 'User Name', ['class' => 'control-label']) !!}
            {!! Form::text('first_name', old('first_name'), ['class' => 'form-control' , 'readonly' => 'true']) !!}
            @if($errors->has('first_name'))
                <span class="help-block">
                    {{ $errors->first('first_name') }}
                </span>
            @endif
        </div>
@else
    @if( count($Employees) > 0)
            <div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif">
                    {!! Form::label('user_id', 'Employee Name*', ['class' => 'control-label']) !!}
                    {!! Form::select('user_id', $Employees, old('user_id'), ['id' => 'user_id', 'class' => 'form-control select2']) !!}
                    <span id="user_id_handler"></span>
                    @if($errors->has('user_id'))
                            <span class="help-block">
                                    {{ $errors->first('user_id') }}
                            </span>
                    @endif
            </div>
        @endif
@endif

<div class="form-group col-md-3 @if($errors->has('att_date')) has-error @endif">
        {!! Form::label('att_date', 'Attendance Date *', ['class' => 'control-label']) !!}
        {!! Form::text('att_date', old('att_date'), ['class' => 'form-control datepicker']) !!}
        @if($errors->has('att_date'))
                <span class="help-block">
            {{ $errors->first('att_date') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('checkin_time')) has-error @endif">
    {!! Form::label('checkin_time', 'Time *', ['class' => 'control-label']) !!}
    {!! Form::time('checkin_time', old('checkin_time'), ['class' => 'form-control']) !!}
    @if($errors->has('checkin_time'))
        <span class="help-block">
            {{ $errors->first('checkin_time') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('type')) has-error @endif">
        {!! Form::label('type', 'Status *', ['class' => 'control-label']) !!}
        {!! Form::select('type', array('' => 'Select a Status') + Config::get('hrm.attendance_status_array'), old('type'), ['class' => 'form-control']) !!}
        @if($errors->has('type'))
                <span class="help-block">
            {{ $errors->first('type') }}
        </span>
        @endif
</div>

