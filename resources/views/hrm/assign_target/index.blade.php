@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Assign Targets</h1>
    </section>
    {{csrf_field()}}
    <input type="hidden" name="name" value="abc">

@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('contacts_create'))
                <a href="{{ route('hrm.assign_target.create') }}" class="btn btn-success pull-right">Assign New Target</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($AssignTarget) > 0 ? 'datatable' : '' }}" id="users-table" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Employee</th>
                    <th>Target </th>
                    <th>Type </th>
                    <th>Value</th>
                    <th>Weight</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                    <th>Actions</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/assign_target/list.js') }}" type="text/javascript"></script>
@endsection