<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif">
    {!! Form::label('user_id', 'Employee *', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $Employees, old('user_id'), ['id' => 'user_id', 'class' => 'form-control select2']) !!}
    <span id="user_id_handler"></span>
    @if($errors->has('user_id'))
        <span class="help-block">
                        {{ $errors->first('user_id') }}
                </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('target_id')) has-error @endif" >
    {!! Form::label('target_id', 'Target', ['class' => 'control-label']) !!}
    {!! Form::select('target_id', $EmployeesTarget, old('target_id'), ['id' => 'target_id', 'class' => 'form-control select2']) !!}
    @if($errors->has('target_id'))
        <span class="help-block">
            {{ $errors->first('target_id') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('value')) has-error @endif">
    {!! Form::label('value', 'Value *', ['class' => 'control-label']) !!}
    {!! Form::number('value', old('value'), [ 'id' => 'value', 'min' => 1, 'class' => 'form-control']) !!}
    @if($errors->has('value'))
        <span class="help-block">
            {{ $errors->first('value') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('weight')) has-error @endif">
    {!! Form::label('weight', 'Weightage * (in  %)', ['class' => 'control-label']) !!}
    {!! Form::number('weight', old('weight'), [ 'id' => 'weight', 'min' => 1, 'class' => 'form-control']) !!}
    @if($errors->has('weight'))
        <span class="help-block">
            {{ $errors->first('weight') }}
        </span>
    @endif
</div>
