<div class="container">
 <h4>Staff Name : <b>{{ \App\Helpers\Helper::userIdToName($user_id) }}</b></h4>
 <input type="hidden" name="user_id" value="{{$user_id}}">
  <table class="table">
    <thead>
      <tr>
        <th>Performance</th>
        <th>Excellent</th>
        <th>Good</th>
        <th>Satisfactory</th>
        <th>Bad</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Lecture Deliver</td>
        <td><input type="radio" name="lecture_deliver" value="Excellent" required></td>
        <td><input type="radio" name="lecture_deliver" value="Good" required></td>
        <td><input type="radio" name="lecture_deliver" value="Satisfactory" required></td>
        <td><input type="radio" name="lecture_deliver" value="Bad" required></td>
      </tr> 
      <tr>
        <td>Confidence</td>
        <td><input type="radio" name="confidence" value="Excellent" required></td>
        <td><input type="radio" name="confidence" value="Good" required></td>
        <td><input type="radio" name="confidence" value="Satisfactory" required></td>
        <td><input type="radio" name="confidence" value="Bad" required></td>
      </tr> 
      <tr>
        <td>Punctuality</td>
        <td><input type="radio" name="punctuality" value="Excellent" required></td>
        <td><input type="radio" name="punctuality" value="Good" required></td>
        <td><input type="radio" name="punctuality" value="Satisfactory" required></td>
        <td><input type="radio" name="punctuality" value="Bad" required></td>
      </tr> 
      <tr>
        <td>Moral Values</td>
        <td><input type="radio" name="moral_values" value="Excellent" required></td>
        <td><input type="radio" name="moral_values" value="Good" required></td>
        <td><input type="radio" name="moral_values" value="Satisfactory" required></td>
        <td><input type="radio" name="moral_values" value="Bad"required></td>
      </tr>     
      <tr>
        <td>Student Feedback</td>
        <td><input type="radio" name="student_feedback" value="Excellent" required></td>
        <td><input type="radio" name="student_feedback" value="Good" required></td>
        <td><input type="radio" name="student_feedback" value="Satisfactory" required></td>
        <td><input type="radio" name="student_feedback" value="Bad" required></td>
      </tr> 
    </tbody>

  </table>
  <div class="row">
    <div class="col-md-12">
      <label>Remarks</label>
      <input type="text" class="form-control" name="remarks">
    </div>
  </div>
</div>