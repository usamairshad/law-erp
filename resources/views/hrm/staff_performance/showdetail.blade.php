
    <div class="box box-primary">
        <div class="box-header with-border">
      
          
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($get_data) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Branch Name</th>
                        <th>Remarks Given By</th>
                        <th>Given By</th>
                        <th>Teacher Reg No</th>
                        <th>Teacher Name</th>
                        <th>Lecture Deliver</th>
                        <th>Confidence</th>
                        <th>Punctuality</th>
                        <th>Moral Values</th>
                        <th>Student Performance</th>
                        <th>Remarks</th>
                        <th>Date</th>   
                        <th>Month</th>
                     
                    </tr>
                </thead>



                <tbody>
                    @if (count($get_data) > 0)
                        @foreach ($get_data as $performance)
                            <tr>    
                                <td>{{ \App\Helpers\Helper::userIdToBranchName($performance->user_id) }}</td>
                                <td>{{ \App\Helpers\Helper::userIdToName($performance->created_by) }}</td>
                                <td>{{ \App\Helpers\Helper::userIdToRole($performance->created_by) }}</td>
                                <td>{{ \App\Helpers\Helper::userIdToStaffRegNo($performance->user_id) }}</td>
                                <td>{{ \App\Helpers\Helper::userIdToName($performance->user_id) }}</td>
                                <td>{{ $performance->lecture_deliver }}</td>
                                <td>{{ $performance->confidence}}</td>
                                <td>{{ $performance->punctuality}}</td>
                                <td>{{ $performance->moral_values}}</td>
                                <td>{{ $performance->student_feedback}}</td>
                                <td>{{ $performance->remarks}}</td>
                                <td>{{ $performance->date}}</td>
                                <td>{{ $performance->month}}</td>
                           
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>













@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>
@endsection
