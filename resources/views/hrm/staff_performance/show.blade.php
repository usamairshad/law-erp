@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Performance Details</h3>
    <div class="box box-primary">
        <div class="box-header with-border">
          <a href="{{ route('staff-performance.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
          
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($get_data) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        
                        <th>Teacher Reg No</th>
                        <th>Teacher Name</th>
                        <th>Lecture Deliver</th>
                        <th>Confidence</th>
                        <th>Punctuality</th>
                        <th>Moral Values</th>
                        <th>Student Performance</th>
                        <th>Remarks</th>
                        <th>Date</th>   
                        <th>Month</th>
                     
                    </tr>
                </thead>



                <tbody>
                    @if (count($get_data) > 0)
                        @foreach ($get_data as $performance)
                            <tr>
                                <td>{{ \App\Helpers\Helper::userIdToStaffRegNo($performance->user_id) }}</td>
                                <td>{{ \App\Helpers\Helper::userIdToName($performance->user_id) }}</td>
                                <td>{{ $performance->lecture_deliver }}</td>
                                <td>{{ $performance->confidence}}</td>
                                <td>{{ $performance->punctuality}}</td>
                                <td>{{ $performance->moral_values}}</td>
                                <td>{{ $performance->student_feedback}}</td>
                                <td>{{ $performance->remarks}}</td>
                                <td>{{ $performance->date}}</td>
                                <td>{{ $performance->month}}</td>
                           
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>













@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>
@endsection
