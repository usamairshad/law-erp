@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Resign List</h3>


        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($resigns) > 0 ? 'datatable' : '' }}"
                id="users-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Branch</th>
                        <th>Staff Role</th>
                        <th>Valid</th>
                        <th>Notice Period</th>
                        <th>Reason</th>
                        <th>Approved by CEO</th>
                        <th>Approved by HR Head</th>
                       <th>Actions</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($resigns as $key => $resign)


                        <tr>

                            <td>{{ ++$key }}</td>

                            <td>{{ $resign->user->name }}</td>
                            <td>{{ $resign->user->email }}</td>
                            <td>{{ \App\Helpers\Helper::userIdToBranchName($resign->user->id) }}</td>
                             <td>{{ \App\Helpers\Helper::userIdToRole($resign->user->id) }}</td>
                            <td>{{ $resign->valid }}</td>
                            <td>{{ $resign->notice_period }}</td>

                            <td>{{ $resign->reason }}</td>
                            <td>{{ $resign->approved_by_ceo }}</td>
                            <td>{{ $resign->approved_by_hr_head }}</td>
                            <td>
                        

                                  {{--  <form method="POST" action="{{ url("admin/resign/$resign->id") }}" accept-charset="UTF-8"
                                        style="display: inline-block;" onsubmit="return confirm('Are you sure?');">

                                        {{ method_field('delete') }}
                                        {{ csrf_field() }}

                                        <input class="btn btn-xs btn-danger" type="submit" value="Delete">
                                    </form> --}}
                                @if(Auth::user()->hasRole('admin'))
                                    @if($resign->approved_by_ceo == 'Pending')
                                            <a href="{{ route('resignrequestceo.approve', [$resign->id]) }}"
                                                    class="btn btn-xs btn-success">Approve</a>
                                                <a href="{{ route('resignrequestceo.reject', [$resign->id]) }}"
                                                    class="btn btn-xs btn-danger">Reject</a>
                                        @elseif($resign->status == 'Approved')
                                        <p style="color:green"><b> Approved </b> </p>
                                        @elseif($resign->status == 'Reject')
                                         <p style="color:red"><b> Reject </b> </p>
                                    @endif
                                @endif

                                @if(Auth::user()->hasRole('HR_head'))
                                    @if($resign->approved_by_hr_head == 'Pending')
                                            <a href="{{ route('resignrequesthr.approve', [$resign->id]) }}"
                                                    class="btn btn-xs btn-success">Approve</a>
                                                <a href="{{ route('resignrequesthr.reject', [$resign->id]) }}"
                                                    class="btn btn-xs btn-danger">Reject</a>
                                        @elseif($resign->status == 'Approved')
                                        <p style="color:green"><b> Approved </b> </p>
                                        @elseif($resign->status == 'Reject')
                                         <p style="color:red"><b> Reject </b> </p>
                                    @endif
                                
                                 @if($resign->approved_by_hr_head == 'Approved' && $resign->approved_by_ceo == 'Approved')      
                                <a href="{{ route('experience_letter_send', [$resign->id]) }}"
                                                class="btn btn-xs btn-success">View Details</a>

                                <a href="{{ route('upload_document', [$resign->id]) }}"
                                                class="btn btn-xs btn-info">Attach Experience Letter</a> 

                                <a href="{{ route('upload_clearance_form', [$resign->id]) }}"
                                                class="btn btn-xs btn-primary">Upload Clearance Form</a>    
                                @endif
                                @endif
                            </td>


                        </tr>

                    @endforeach





                </tbody>




            </table>
        </div>
    </div>

@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>


@endsection
