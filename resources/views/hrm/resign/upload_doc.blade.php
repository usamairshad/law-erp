@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Upload Experience Letter</h1>
    </section>
@stop

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                    <h3 style = "float:left;">Attach File</h3>          
          <a href="{{ route('admin.resign.index') }}" style="float: right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->


<br>
<br>

        {!! Form::open(['method' => 'POST', 'route' => ['upload_document_experience'],'enctype' => 'multipart/form-data', 'id' => 'validation-form']) !!}
        <div class="box-body">
           {{csrf_field()}}

            <input type="hidden" name="resign_id" value="{{$resign_id}}">
            <div class="form-group col-md-6">
                {!! Form::label('Full Name', 'Upload Document', ['class' => 'control-label']) !!}
                <input type="file" name="file" class="form-control" required>
            </div>
        </div>
        <!-- /.box-body -->


        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}

    </div>
@stop

@section('javascript')

@endsection

