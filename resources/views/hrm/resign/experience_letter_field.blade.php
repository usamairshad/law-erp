


{{csrf_field()}}


<input type="hidden" name="user_id" value="{{$data->user_id}}">

<div class="row" style="margin-top: 40px;">


    <div class="form-group col-md-3" style="float: left;">

    <label>{{$data['status'] }}</label>
    {!! Form::label('Full Name', 'Full Name', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="name" value="{{ \App\Helpers\Helper::getStaffIDToFirstName($staff_id) }}" readonly>  
</div>
<div class="form-group col-md-3">
    {!! Form::label('Employee ID', 'Employee ID', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="reg_no" value="{{$data->reg_no}}" readonly>
</div>

<div class="form-group col-md-3">
    {!! Form::label('Branch', 'Branch/Department', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="branch_id" value="{{$data->branch_id}}" readonly>
</div>
<div class="form-group col-md-3">
    {!! Form::label('Class', 'Class/Grade', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="class" value="{{$data->class}}" readonly>
</div>
<div class="form-group col-md-3">
    {!! Form::label('Subject', 'Subject', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="subject" value="{{$data->subject}}" readonly>
</div>
<div class="form-group col-md-3">
    {!! Form::label('Home Address', 'Home Address', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="home_address" value="{{$data->address}}" readonly>
</div>
<div class="form-group col-md-3">
    {!! Form::label('Contact', 'Contact', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="contact" value="{{$data->mobile_1}}" readonly>
</div>
<div class="form-group col-md-3">
    {!! Form::label('Date of Joining', 'Date of Joining', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="date_of_joining" value="{{$data->join_date}}" readonly>
</div>
<div class="form-group col-md-3">
    {!! Form::label('Last Day of Work', 'Last Day of Work', ['class' => 'control-label']) !!}
    <input type="text" class="form-control" name="last_day_of_work" value="{{$data->last_day_of_work}}" readonly>
</div>


</div>



 @if($staff_status == 'Active')
 <div class="form-group col-md-6">
    <div class="row">
        <div class="col-md-4">
           {!! Form::label('Move to SOS', 'Move to SOS', ['class' => 'control-label']) !!} 
        </div>
        <div class="col-md-6">
           <a href="{{url('move-to-sos',$staff_id)}}" class="btn btn-xs btn-danger" >Move to SOS</a>
        </div>
    </div>
     
</div>  
 @else
 <div class="form-group col-md-6">
    <div class="row">
        <div class="col-md-4">
           {!! Form::label('Remove to SOS', 'Remove to SOS', ['class' => 'control-label']) !!} 
        </div>
        <div class="col-md-6">
            <a href="{{url('remove-to-sos',$staff_id)}}" class="btn btn-xs btn-success" >Remove from SOS</a>
        </div>
    </div>
     
</div>
@endif
@if ($user_status == 1)



 <div class="form-group col-md-6">
    <div class="row">
        <div class="col-md-4">
           {!! Form::label('Terminate', 'Terminate', ['class' => 'control-label']) !!} 
        </div>
        <div class="col-md-6">
            <a href="{{ url('admin/terminate/user') . '/' . $data->user_id }}"
                onclick="return confirm('Are you sure ');"
                data-user-id="{{ $data->id }}"
                class="btn btn-xs btn-danger terminate">Terminate

            </a>
        </div>
    </div>
</div>
@else

 <div class="form-group col-md-6">
    <div class="row">
        <div class="col-md-4">
           {!! Form::label('Re-Activate', 'Re-Activate', ['class' => 'control-label']) !!} 
        </div>
        <div class="col-md-6">
            <a href="{{ url('admin/re-activate/user/') . '/' . $data->user_id }}"
                onclick="return confirm('Are you sure ');"
                data-user-id="{{ $data->user_id }}"
                class="btn btn-xs btn-success">Re-Activate
            </a>
        </div>
    </div>
</div>

@endif

    <div class="modal fade" id="terminateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Terminate ?</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row ">

                            <form method="POST" action="{{ route('admin.termination.store') }}">
                                {{ csrf_field() }}
                                <div class="form-group">

                                    <label for="exampleInputEmail1">Reason</label>
                                    <br>

                                    <textarea style="width: 100%" class="from-control" name="reason" id="" cols="30"
                                        rows="10"></textarea>

                                    <input type="hidden" name="user_id" id="terminate_user_input">

                                </div>


                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>  



    



