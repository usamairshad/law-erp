@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Staff Details</h1>
    </section>
@stop



@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                    <h3 style = "float:left;">Info</h3>          
                    <a href="{{ route('admin.resign.index') }}" style="float: right" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->





        {!! Form::open(['method' => 'POST', 'route' => ['experience_letter_store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('hrm.resign.experience_letter_field')
        </div>
        <!-- /.box-body -->
        

        <div class="box-footer">
            
        </div>
        {!! Form::close() !!}

    </div>
@stop

@section('javascript')

    <script>
        $(".terminate").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#terminate_user_input').val($user_id);

        });


        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });



    </script>

    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>

@endsection