@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Resign List</h3>


        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}"
                id="users-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Branch</th>
                        <th>Staff Role</th>
                        <th>Notice Period</th>
                        <th>Reason</th>
                        <th>Want Experience Letter</th>
                        <th>Approved By CEO</th>
                        <th>Approved By HR Head</th>
                       <th>Actions</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($data as $key => $resign)


                        <tr>

                            <td>{{ ++$key }}</td>                          
                            <td>{{ \App\Helpers\Helper::userIdToBranchName($resign->user->id) }}</td>
                             <td>{{ \App\Helpers\Helper::userIdToRole($resign->user->id) }}</td>
                            <td>{{ $resign->notice_period }}</td>
                            <td>{{ $resign->reason }}</td>
                            <td>{{ $resign->experience_letter }}</td>
                            <td>{{ $resign->approved_by_ceo }}</td>
                            <td>{{ $resign->approved_by_hr_head }}</td>
                            <td>
                                    @if($resign->approved_by_ceo == 'Pending' || $resign->approved_by_hr_head == 'Pending')
                                   <form method="POST" action="{{ url("admin/resign/$resign->id") }}" accept-charset="UTF-8"
                                        style="display: inline-block;" onsubmit="return confirm('Are you sure?');">

                                        {{ method_field('delete') }}
                                        {{ csrf_field() }}

                                        <input class="btn btn-xs btn-danger" type="submit" value="Delete">
                                    </form> 

                                   @elseif($resign->approved_by_ceo == 'Approved' || $resign->approved_by_hr_head == 'Approved')
                                    <p style="color:green"><b> Approved </b> </p>

                                    @elseif($resign->approved_by_ceo == 'Rejected' || $resign->approved_by_hr_head == 'Rejected')
                                    
                                     <p style="color:red"><b> Rejected </b> </p>
                                    @endif
                            </td>


                        </tr>

                    @endforeach





                </tbody>




            </table>
        </div>
    </div>










@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>





@endsection
