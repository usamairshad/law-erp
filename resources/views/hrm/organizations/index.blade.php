@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Organizations</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">General Information</h3>
            @if(Gate::check('organizations_edit'))
                <a href="{{ route('hrm.organizations.edit',[$Organization->id]) }}" class="btn btn-success pull-right">Edit</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>Organization Name</th>
                        <td colspan="3">@if($Organization->name){{ $Organization->name }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                    <tr>
                        <th width="15%">GST #</th>
                        <td width="35%">@if($Organization->gst){{ $Organization->gst }}@else{{ 'N/A' }}@endif</td>
                        <th width="15%">NTN #</th>
                        <td width="35%">@if($Organization->ntn){{ $Organization->ntn }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                    <tr>
                        <th>Phone #</th>
                        <td>@if($Organization->phone){{ $Organization->phone }}@else{{ 'N/A' }}@endif</td>
                        <th>Fax #</th>
                        <td>@if($Organization->fax){{ $Organization->fax }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                    <tr>
                        <th align="right">Email</th>
                        <td colspan="3">@if($Organization->email){{ $Organization->email }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Location Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th align="right">Location Name</th>
                        <td colspan="3">@if($OrganizationLocation->name){{ $OrganizationLocation->name }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                    <tr>
                        <th width="15%">Phone #</th>
                        <td width="35%">@if($OrganizationLocation->phone){{ $OrganizationLocation->phone }}@else{{ 'N/A' }}@endif</td>
                        <th width="15%">Fax #</th>
                        <td width="35%">@if($OrganizationLocation->fax){{ $OrganizationLocation->fax }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                    <tr>
                        <th>Address Line 1</th>
                        <td>@if($OrganizationLocation->street_1){{ $OrganizationLocation->street_1 }}@else{{ 'N/A' }}@endif</td>
                        <th>Address Line 2</th>
                        <td>@if($OrganizationLocation->street_2){{ $OrganizationLocation->street_2 }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td>@if($OrganizationLocation->city){{ $OrganizationLocation->city }}@else{{ 'N/A' }}@endif</td>
                        <th>Zip Code</th>
                        <td>@if($OrganizationLocation->zip_code){{ $OrganizationLocation->zip_code }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                    <tr>
                        <th>Province</th>
                        <td>@if($OrganizationLocation->province){{ $OrganizationLocation->province }}@else{{ 'N/A' }}@endif</td>
                        <th>Country</th>
                        <td>@if($OrganizationLocation->country){{ Config::get('hrm.country_array')[$OrganizationLocation->country] }}@else{{ 'N/A' }}@endif</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection