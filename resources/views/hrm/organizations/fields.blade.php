<div class="form-group col-md-4 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 @if($errors->has('gst')) has-error @endif">
    {!! Form::label('gst', 'GST #', ['class' => 'control-label']) !!}
    {!! Form::text('gst', old('gst'), ['class' => 'form-control']) !!}
    @if($errors->has('gst'))
        <span class="help-block">
            {{ $errors->first('gst') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 @if($errors->has('ntn')) has-error @endif">
    {!! Form::label('ntn', 'NTN #', ['class' => 'control-label']) !!}
    {!! Form::text('ntn', old('ntn'), ['class' => 'form-control']) !!}
    @if($errors->has('ntn'))
        <span class="help-block">
            {{ $errors->first('ntn') }}
        </span>
    @endif
</div>

<div class="clearfix"></div>

<div class="form-group col-md-4 @if($errors->has('phone')) has-error @endif">
    {!! Form::label('phone', 'Phone #', ['class' => 'control-label']) !!}
    {!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}
    @if($errors->has('phone'))
        <span class="help-block">
            {{ $errors->first('phone') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 @if($errors->has('fax')) has-error @endif">
    {!! Form::label('fax', 'Fax #', ['class' => 'control-label']) !!}
    {!! Form::text('fax', old('fax'), ['class' => 'form-control']) !!}
    @if($errors->has('fax'))
        <span class="help-block">
            {{ $errors->first('fax') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 @if($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
    @if($errors->has('email'))
        <span class="help-block">
            {{ $errors->first('email') }}
        </span>
    @endif
</div>