{{csrf_field()}}
<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif" id="user_div">
        {!! Form::label('user_id', 'User ', ['class' => 'control-label']) !!}
        {!! Form::select('user_id', $Employees , old('user_id'),
         ['class' => 'form-control select2'  ,'id' => 'user_id']) !!}
</div>

<div class="form-group col-md-3 @if($errors->has('document_type')) has-error @endif" id="user_div">
        {!! Form::label('document_type', 'Document Type ', ['class' => 'control-label']) !!}
        {!! Form::select('document_type', $EmployeeDocumentTypes , old('document_type'),
         ['class' => 'form-control select2'  ,'id' => 'document_type']) !!}
        @if($errors->has('document_type'))
        <span class="help-block">
            {{ $errors->first('document_type') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('file_location')) has-error @endif">
    {!! Form::label('file_location', 'Upload File', ['class' => 'control-label']) !!}
    {!! Form::file('file_location', ['class' => 'form-control', 'id' => 'file_location']) !!}
    @if($errors->has('file_location'))
        <span class="help-block">
            {{ $errors->first('file_location') }}
        </span>
    @endif
</div>

