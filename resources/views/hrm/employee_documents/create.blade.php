@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Documents</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add Employee Document</h3>
            <a href="{{ route('hrm.employee_documents.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->


        {!! Form::open(['method' => 'POST', 'route' => ['hrm.employee_documents.store'], 'enctype' => 'multipart/form-data' , 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('hrm.employee_documents.fields')
        </div>
        <!-- /.box-body -->


        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}

    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/employee_documents/create_modify.js') }}" type="text/javascript"></script>

@endsection

