@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Documents</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Employee Document</h3>
            <a href="{{ route('hrm.employees_overtime.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($EmployeeDocument, ['method' => 'PUT', 'enctype' => 'multipart/form-data' , 'id' => 'validation-form', 'route' => ['hrm.employee_documents.update', $EmployeeDocument->id]]) !!}
        <div class="box-body">
            @include('hrm.employee_documents.fields')
        </div>
        <!-- /.box-body -->
        {!! Form::hidden('id',  $EmployeeDocument->id  , ['id' => 'id'] ) !!}

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/employees_overtime/create_modify.js') }}" type="text/javascript"></script>
@endsection