@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Provident Fund & EOBI</h3>
           

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Staffs) > 0 ? 'datatable' : '' }}">
                <thead>

                    <tr>
                        <th>Branch Name</th>
                        <th>Name</th>
                        <th>Staff Reg no</th>

                        <th>Staff Type</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($Staffs) > 0)
                        @foreach ($Staffs as $Employee)
                            <tr data-entry-id="{{ $Employee->id }}">
                                <td>{{ \App\Helpers\Helper::branchIdToName($Employee->branch_id) }}</td>
                                <td>{{ $Employee->first_name . ' ' . $Employee->middle_name . ' ' . $Employee->last_name}}</td>
                                <td>{{ $Employee->reg_no }}</td>

                                <td>{{ $Employee->staff_type }}</td>
                                <td>{{ $Employee->email }}</td>
                                <td>

                                    @if($Employee->epf=="No")
                                        <a href="{{url('give_epf',$Employee->reg_no)}}" class="btn btn-primary"> Apply EPF</a>

                                    @endif

                                    @if($Employee->eobi=="No")

                                    <a href="javascript:void(0)" data-user-id="{{ $Employee->user_id }}" data-target="#warningModal" data-toggle="modal" class="btn btn-xs btn-warning warning">Apply EOBI 
                                        </a>
                                    @endif    


                                    
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>











    <!--------------------------------------modal for Warning------------------------------------>

    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> EOBI Government ID! </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">

                            <form method="POST" action="{{ url('eobi_notification/govt_reg_id') }}" style="width: 100%;" >
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Govt. Registration ID*</label>
                                    <input type="text" name="eobi_govt_id" class="form-control col-lg-12">
                                </div>

                                <div class="form-group">

                                    <label>Date*</label>


                                    <input type="date" name="date" class="form-control col-lg-12" placeholder="select Date*">

                                    <input type="hidden" name="user_id" id="warning_user_input">

                                </div>



                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!--------------------------------------modal for Warning------------------------------------>




@stop

@section('javascript')

    <script>


        $(document).ready(function() {
            $('.datatable').DataTable()
        });



        $(".warning").on("click", function() {

            $user_id = $(this).data("user-id");
        
            $('#warning_user_input').val($user_id);

        });

    </script>
@endsection
