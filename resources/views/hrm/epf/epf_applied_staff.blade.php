@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Employee Provident Fund Register</h3>
           

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Staffs) > 0 ? 'datatable' : '' }}">
                <thead>

                    <tr>
                        <th>Branch Name</th>
                        <th>Name</th>
                        <th>Staff Reg no</th>

                        <th>Staff Type</th>
                        <th>Email</th>
                        <th>Total EPF</th>
                        <th>Paid EPF</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($Staffs) > 0)
                        @foreach ($Staffs as $Employee)
                            <tr data-entry-id="{{ $Employee->id }}">
                                <td>{{ \App\Helpers\Helper::branchIdToName($Employee->branch_id) }}</td>
                                <td>{{ $Employee->first_name . ' ' . $Employee->middle_name . ' ' . $Employee->last_name}}</td>
                                <td>{{ $Employee->reg_no }}</td>

                                <td>{{ $Employee->staff_type }}</td>
                                <td>{{ $Employee->email }}</td>
                                <td>{{ $Employee->epf_amount}}</td>
                                <td>{{ $Employee->epf_paid}}</td>
                                <td>

                                        
                                        <a href="#" class="btn btn-success">Epf Applied</a>

                                    
                                    
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>






@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>
@endsection
