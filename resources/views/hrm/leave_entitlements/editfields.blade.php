<div class="form-group col-md-4 employee_id @if($errors->has('employee_id')) has-error @endif">
    {!! Form::label('employee_id', 'Employee Name*', ['class' => 'control-label']) !!}
    {!! Form::text(null, $Employee->first_name . ' ' . $Employee->last_name, ['readonly' => true, 'class' => 'form-control']) !!}
    {!! Form::hidden('employee_id', encrypt($LeaveEntitlement->employee_id), ['id' => 'employee_id']) !!}
    @if($errors->has('employee_id'))
        <span class="help-block">
            {{ $errors->first('employee_id') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 employee_id @if($errors->has('leave_type_id')) has-error @endif">
    {!! Form::label('employee_id', 'Leave Type*', ['class' => 'control-label']) !!}
    {!! Form::text('', $LeaveType->name, ['readonly' => true, 'class' => 'form-control']) !!}
    {!! Form::hidden('leave_type_id', encrypt($LeaveEntitlement->leave_type_id), ['id' => 'leave_type_id']) !!}
    @if($errors->has('leave_type_id'))
        <span class="help-block">
            {{ $errors->first('leave_type_id') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 employee_id @if($errors->has('entitlement_dates')) has-error @endif">
    {!! Form::label('employee_id', 'Leave Period*', ['class' => 'control-label']) !!}
    {!! Form::text('', $LeaveEntitlement->start_date . ' - ' . $LeaveEntitlement->end_date, ['readonly' => true, 'class' => 'form-control']) !!}
    {!! Form::hidden('entitlement_dates', $LeaveEntitlement->start_date . '::'. $LeaveEntitlement->end_date, ['id' => 'leave_type_id']) !!}
    <span id="employee_id_handler"></span>
    @if($errors->has('entitlement_dates'))
        <span class="help-block">
            {{ $errors->first('entitlement_dates') }}
        </span>
    @endif
</div>

<div class="clearfix"></div>

<div class="form-group col-md-4 @if($errors->has('no_of_days')) has-error @endif">
    {!! Form::label('no_of_days', 'Entitlements*', ['class' => 'control-label']) !!}
    {!! Form::number('no_of_days', old('no_of_days'), ['id' => 'no_of_days', 'class' => 'form-control']) !!}
    @if($errors->has('no_of_days'))
        <span class="help-block">
            {{ $errors->first('no_of_days') }}
        </span>
    @endif
</div>

{!! Form::hidden('start_date', old('start_date'), ['id' => 'start_date']) !!}
{!! Form::hidden('end_date', old('end_date'), ['id' => 'end_date']) !!}