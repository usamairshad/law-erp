@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Leave Entitlements</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('leave_entitlements_create'))
                <a href="{{ route('hrm.leave_entitlements.create') }}" class="btn btn-success pull-right">Add Entitlements</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($LeaveEntitlements) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Employee</th>
                    <th>Leave Type</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Entitlements</th>
                    <th>Days Used</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($LeaveEntitlements) > 0)
                    @foreach ($LeaveEntitlements as $LeaveEntitlement)
                        <tr data-entry-id="{{ $LeaveEntitlement->id }}">
                            <td>{{ $LeaveEntitlement->id }}</td>
                            <td>{{ $Employees[$LeaveEntitlement->employee_id]->first_name . ' ' . $Employees[$LeaveEntitlement->employee_id]->father_name }}</td>
                            <td>{{ $LeaveTypes[$LeaveEntitlement->leave_type_id]->name }}</td>
                            <td>{{ $LeaveEntitlement->start_date }}</td>
                            <td>{{ $LeaveEntitlement->end_date }}</td>
                            <td>{{ $LeaveEntitlement->no_of_days }}</td>
                            <td>{{ $LeaveEntitlement->days_used }}</td>
                            <td>{{ $LeaveEntitlement->created_at }}</td>
                            <td>
                                @if(Gate::check('leave_entitlements_edit'))
                                    <a href="{{ route('hrm.leave_entitlements.edit',[$LeaveEntitlement->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                @if(Gate::check('leave_entitlements_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['hrm.leave_entitlements.destroy', $LeaveEntitlement->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection