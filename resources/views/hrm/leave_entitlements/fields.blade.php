<div class="form-group col-md-4 @if($errors->has('condition')) has-error @endif">
    {!! Form::label('condition', 'Add to Multiple Employees', ['id' => 'condition', 'class' => 'control-label']) !!}<br/>
    {{ Form::checkbox('condition', 1, old('condition')) }}&nbsp;Yes
    @if($errors->has('condition'))
        <span class="help-block">
            {{ $errors->first('condition') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 employee_id @if($errors->has('employee_id')) has-error @endif">
    <?php
    $employee = ((array)\DB::select("SELECT * FROM `hrm_employees` WHERE STATUS = 1"));

    ?>
    {!! Form::label('employee_id', 'Employee Name*', ['class' => 'control-label']) !!}
    {{--{!! Form::select('employee_id', array(), old('employee_id'), ['id' => 'employee_id', 'class' => 'form-control employee_id' ,'multiple' => true]) !!}--}}
        <select id="employee_id" name="employee_id" class="form-control select2">
            @if (count($employee) >=1)
                <option value="">Select employee for reoprt</option>
                @foreach($employee as $employee)
                    <option value="{{ $employee->id }}" >{{ $employee->first_name .' '.$employee->father_name }}</option>
                @endforeach
            @endif
        </select>
        <span id="employee_id_handler"></span>
    @if($errors->has('employee_id'))
        <span class="help-block">
            {{ $errors->first('employee_id') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 employee @if($errors->has('employee_id')) has-error @endif">
    {!! Form::label('employee', 'Employees', ['class' => 'control-label']) !!}<br/>
    (Matches {{ $EmployeeCount }} epmloyees)
</div>

<div class="form-group col-md-4 @if($errors->has('leave_type_id')) has-error @endif">
    {!! Form::label('leave_type_id', 'Leave Type*', ['class' => 'control-label']) !!}
    {!! Form::select('leave_type_id', $LeaveTypes, old('leave_type_id'), ['id' => 'leave_type_id', 'class' => 'form-control']) !!}
    <span id="leave_type_id_handler"></span>
    @if($errors->has('leave_type_id'))
        <span class="help-block">
            {{ $errors->first('leave_type_id') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 @if($errors->has('entitlement_dates')) has-error @endif">
    {!! Form::label('entitlement_dates', 'Leave Period*', ['class' => 'control-label']) !!}
    {!! Form::select('entitlement_dates', array('' => 'Select a Leave Period') + array(
    $entitlement_dates['current_year_start']->format('Y-m-d') . '::' . $entitlement_dates['current_year_end']->format('Y-m-d') => $entitlement_dates['current_year_start']->format('Y-m-d') . ' - ' . $entitlement_dates['current_year_end']->format('Y-m-d'),
    $entitlement_dates['next_year_start']->format('Y-m-d') . '::' . $entitlement_dates['next_year_end']->format('Y-m-d') => $entitlement_dates['next_year_start']->format('Y-m-d') . ' - ' . $entitlement_dates['next_year_end']->format('Y-m-d'),
    ), old('start_date') . '::' . old('end_date'), ['id' => 'entitlement_dates', 'class' => 'form-control']) !!}
    {!! Form::hidden('start_date', old('start_date'), ['id' => 'start_date']) !!}
    {!! Form::hidden('end_date', old('end_date'), ['id' => 'end_date']) !!}
    @if($errors->has('entitlement_dates'))
        <span class="help-block">
            {{ $errors->first('entitlement_dates') }}
        </span>
    @endif
</div>

<div class="form-group col-md-4 @if($errors->has('no_of_days')) has-error @endif">
    {!! Form::label('no_of_days', 'Entitlement*', ['class' => 'control-label']) !!}
    {!! Form::number('no_of_days', old('no_of_days'), ['id' => 'no_of_days', 'class' => 'form-control']) !!}
    @if($errors->has('no_of_days'))
        <span class="help-block">
            {{ $errors->first('no_of_days') }}
        </span>
    @endif
</div>