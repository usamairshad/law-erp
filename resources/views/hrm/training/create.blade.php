@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">

                            
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Add Training</h3>
        <div class="panel-body pad table-responsive">
            <form method="POST" action="{{route('admin.training.store') }}">
                {{ csrf_field() }}
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Venue:</label>
                                <input type="text" class="form-control" name="venue" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Department:</label>
                                <input type="text" class="form-control" name="department" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Attendee:</label>
                                <input type="text" class="form-control" name="designation" id="formGroupExampleInput">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Description:</label>
                                <input type="text" class="form-control" name="description" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Date:</label>
                                <input type="date" name="date" class="form-control" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Start Time:</label>
                                <input type="time" id="appt" class="form-control" name="time" min="09:00" max="18:00"
                                    required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Training Type:</label>
                                <input type="text" class="form-control" name="training_type" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">No.Of.Days:</label>
                                <input type="number" name="no_of_days" class="form-control" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Trainer:</label>
                                <input type="text" class="form-control" name="training_by" id="formGroupExampleInput">
                            </div>
                        </div>
                       <!--  <div class="col-md-4">
                            <div class="form-group">
                                <label>Branch:</label>
                                <select class="form-control" name="branch_id">
                                    <option selected disabled>--Select--</option>
                                    @foreach ($branches as $branch)

                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>

                                    @endforeach

                                </select> 

                            </div>
                        </div> -->

                    


                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Training Charges:</label>
                                <br> 
                                <input type="number" name="training_charges" class="form-control" min="0">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Category:</label>
                                <br>
                                <input type="radio" name="category" value="local" id="formGroupExampleInput">
                                <label for="">Local</label>
                                <input type="radio" name="category" value="international" id="formGroupExampleInput">
                                <label for="">International</label>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('branch_id', 'Branch*', ['class' => 'control-label']) !!}
                            <select class="js-example-basic-multiple" name="branch_id[]" multiple="multiple" required="required">
                                @foreach($branches as $key => $branch)
                                <option value="{{$branch['id']}}">{{$branch['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4" style="margin-top: 25px">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>
@stop

@section('javascript')


    <script type="text/javascript">

        $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
    </script>



@endsection