@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Training Request</h3>
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($trainings) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Trainer Given By</th>
                        <th>Avalibility</th>
                        <th>Venue</th>
                        <th>Category</th>
                        <th>Department</th>
                        <th>Designation</th>
                        <th>Branch</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>No of Days</th>
                        <th>Training Charges</th>
                        <th>Training Type</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($trainings) > 0)
                        @foreach ($trainings as $training)
                            <tr data-entry-id="{{ $training->id }}">
                                 <td>{{ $training->training_by }}</td>
                                 @if($training->avalibility == 0)
                                 <td>No Available</td>
                                 @elseif($training->avalibility == 1)
                                 <td>Available</td>
                                 @endif
                                 <td>{{ $training->venue }}</td>

                                 <td>{{ $training->category }}</td>
                                 <td>{{ $training->department }}</td>
                                 <td>{{ $training->designation }}</td>
                                <td> {{ \App\Helpers\Helper::branchIdToName($training->branch_id) }}</td>

                                <td>{{ $training->date }}</td>
                                <td>{{ $training->time }}</td>
                                <td>{{ $training->no_of_days }}</td>
                                <td>{{ $training->training_charges }}</td>
                                <td>{{ $training->training_type }}</td>
                                <td>
                                    
                                @if($training->acknowledge==0)
                                
                                <a class="btn btn-xs btn-primary assign-modal-anchor" href = "training/acceptance/form/{{$training->id}}">Accept
                            Training </a>
                            @else
                            <a class="btn btn-xs btn-success assign-modal-anchor" href = "#">Accepted </a>
                            @endif
                              
                            </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
            
    </script>

@endsection
