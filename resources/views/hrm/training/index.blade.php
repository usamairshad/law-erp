@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Training</h3>

        @can('erp_training_create')

        <a href="{{ route('admin.training.create') }}" style = "float:right;" class="btn btn-success pull-right">Add training </a>

        @endcan

    </div>
    <!-- /.box-header -->
    <div class="panel-body pad table-responsive">
        <table class="table table-bordered table-striped {{ count($trainings) > 0 ? 'datatable' : '' }}"
            id="users-table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Venue</th>
                    <th>Category</th>
                    <th>Department</th>
                    <th>Branch</th>
                    <th>Date</th>
                    <th>Start Time</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>

                @foreach ($trainings as $key => $training)


                <tr>

                    <td>{{ ++$key }}</td>
                    <td>{{ $training->venue }}</td>
                    <td>{{ $training->category }}</td>
                    <td>{{ $training->department }}</td>
                    <td>{{ $training->branch->name ?? '' }}</td>
                    <td>{{ $training->date }}</td>
                    <td>{{ $training->time }}</td>
                    <td>


                        @can('erp_training_edit')



                        <a href="{{ route('admin.training.edit', ['id' => $training->id]) }}"
                            class="btn btn-xs btn-info">Edit</a>
                        @endcan

                        @can('erp_training_destroy')



                        <form method="POST" action="{{ route('admin.training.destroy', ['id' => $training->id]) }}"
                            accept-charset="UTF-8" style="display: inline-block;"
                            onsubmit="return confirm('Are you sure?');">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}
                            <input class="btn btn-xs btn-danger" type="submit" value="Delete">
                        </form>

                        @endcan

                        <a class="btn btn-xs btn-primary assign-modal-anchor" href = "assign-training\{{$training->id}}\{{$training->branch_id}}">Assign
                            Training </a>


                        <a href='{{url("admin/training/assigned/$training->id/users")}}'
                            class="btn btn-xs btn-primary assign-modal-anchor">See Users
                        </a>



                    </td>


                </tr>

                @endforeach





            </tbody>




        </table>
    </div>
</div>

<!-------------------------------------------------modal------------------------------------>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Assign Training to staff</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <div class="row ">


                        <form method="GET" action="{{url('admin/assign/training/to-users')}}">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Role Type:</label>
                                    <select style="width: 100%" class="form-control" name="role_type" id="role_type">
                                        <option disabled selected>--Select--</option>
                                
                                        <option value="erp">Management/ Adminstration</option>
                                        <option value="academics">Academcis/Teaching</option>
                                        
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Role:</label>
                                    <select style="width: 100%" class="form-control" name="role_id" id="role_select">
                                    
                                    </select>
                                </div>
                             <input type="hidden" name="training_id" id="training_id_input">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Staff</label>
                                <select style="width: 100%" class=" form-control js-example-basic-multiple"
                                    name="users[]" id="users_select" multiple="multiple">




                                </select>
                            </div>






                    </div>

                </div>




            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>





@stop

@section('javascript')

<script>
    $(document).ready(function () {
        $('.datatable').DataTable()
    });


    $("#role_select").on("change", function () {
        $role_id = $(this).find(":selected").val();

        getUsersByRole($role_id);

    });


    function getUsersByRole($role_id) {

        $.ajax({

            url: "{{url('admin/training/users-by/role')}}/" + $role_id,
            success: function (res) {
                $html = "";
                $('#users_select').html("");

                res.forEach(data => {
                    $html += ` <option value="${data.id}">${data.name}</option>         `;


                });


                $('#users_select').append($html);


            }
        });

    }


    $('.assign-modal-anchor').on("click", function () {

        $training_id = $(this).data("training-id");

        $("#training_id_input").val($training_id);

    });



    $(document).ready(function () {
        $('.js-example-basic-multiple').select2();
    });

        $(function(){
            $('#role_type').change(function(){
               $("#role_select option").remove();;
               var id = $(this).val();
               console.log(id)
               $.ajax({
                  url : '{{ route( 'admin.load-roles-staff' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'get',
                  dataType: 'json',
                  success: function( result )
                  {     
                        console.log(result)
                        $('#role_select').append($('<option disabled selected>--Select--</option>'));
                        $.each( result, function(k, v) {
                            $('#role_select').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
</script>





@endsection