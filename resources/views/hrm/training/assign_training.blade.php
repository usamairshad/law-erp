@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Teacher</h3>
        <div class="panel-body pad table-responsive">
        <form method="GET" action="{{url('admin/assign/training/to-users')}}">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Role Type:</label>
                                    <select style="width: 100%" class="form-control" name="role_type" id="role_type">
                                        <option disabled selected>--Select--</option>
                                
                                        <option value="erp">Management/ Adminstration</option>
                                        <option value="academics">Academcis/Teaching</option>
                                        
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Role:</label>
                                    <select style="width: 100%" class="form-control" name="role_id" id="role_select">
                                    
                                    </select>
                                </div>
                             <input type="hidden" name="training_id" value ="{{$trainings}}" id="training_id_input">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Staff</label>
                                <select style="width: 100%" class=" form-control js-example-basic-multiple"
                                    name="users[]" id="users_select" multiple="multiple">




                                </select>
                            </div>






                    </div>

                </div>




            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
    </div>
@stop

@section('javascript')


    <script type="text/javascript">

        $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
    </script>
<script>
    $(document).ready(function () {
        $('.datatable').DataTable()
    });


    $("#role_select").on("change", function () {
        $role_id = $(this).find(":selected").val();

        getUsersByRole($role_id);

    });


    function getUsersByRole($role_id) {

        $.ajax({

            url: "{{url('admin/training/users-by/role')}}/" + $role_id,
            success: function (res) {
                $html = "";
                $('#users_select').html("");

                res.forEach(data => {
                    $html += ` <option value="${data.id}">${data.name}</option>         `;


                });


                $('#users_select').append($html);


            }
        });

    }


    $('.assign-modal-anchor').on("click", function () {

        $training_id = $(this).data("training-id");

        $("#training_id_input").val($training_id);

    });



    $(document).ready(function () {
        $('.js-example-basic-multiple').select2();
    });

        $(function(){
            $('#role_type').change(function(){
               $("#role_select option").remove();;
               var id = $(this).val();
               console.log(id)
               $.ajax({
                  url : '{{ route( 'admin.load-roles-staff' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id,
                    "name":"{{$branch_id}}"
                    },
                  type: 'get',
                  dataType: 'json',
                  success: function( result )
                  {     
                        console.log(result)
                        $('#role_select').append($('<option disabled selected>--Select--</option>'));
                        $.each( result, function(k, v) {
                            $('#role_select').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
</script>


@endsection