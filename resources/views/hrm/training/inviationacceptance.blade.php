@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Training</h3>

    </div>
    <!-- /.box-header -->
    <div class="panel-body pad table-responsive">
                            <div style=" padding-top:30px;" class="row">

                                <div class="col-md-2" style ="font-weight:700;">
                                    Venue :
</div>

<div class="col-md-2" >
                                        {{ $training->training->venue }}
                               


                                </div>

                                <div class="col-md-2" style ="font-weight:700;">
                                   Category :</div>

<div class="col-md-2"> {{ $training->training->category }}


                                </div>
                                <div class="col-md-2" style ="font-weight:700;">

                                    Department </div>

<div class="col-md-2"> {{ $training->training->department }}

                                </div>

                            </div>

                            <div style="margin-top: 15px;" class="row">

                                <div class="col-md-2" style ="font-weight:700;">
                                    Attendee : </div>

<div class="col-md-2" >{{ $training->training->designation }}
                                </div>
                                <div class="col-md-2" style ="font-weight:700;">
                                    Branch :
                                    </div>

<div class="col-md-2"> {{ $training->training->branch->name }}</p>

                                </div>
                                <div class="col-md-2" style ="font-weight:700;">
                                Description:    </div>

<div class="col-md-2">  {{ $training->training->description }}

                                </div>

</div>
                            <div style="margin-bottom:10px" class="row">

                                <div class="col-md-2" style ="font-weight:700;">
                                    Date : 
                                    </div>

<div class="col-md-2"> {{ $training->training->date }}



                                </div>
                                <div class="col-md-2" style ="font-weight:700;">
                                    Start Time :
                                    </div>

<div class="col-md-2">  {{ $training->training->time }}
                                </div>
                                <div class="col-md-2" style ="font-weight:700;">
                                   Trainer :
                                   </div>

<div class="col-md-2">  {{ $training->training->training_by }}
                                </div>

                            
</div>

                            <div style="margin-bottom:10px" class="row">

                                <div class="col-md-2" style ="font-weight:700;">
                                    No of days :
                                    </div>

<div class="col-md-2">
                                    {{ $training->training->no_of_days }}
</div>

                                <div class="col-md-2" style ="font-weight:700;">
                                    Training Charges :
                                    </div>

<div class="col-md-2">
     {{ $training->training->training_charges }}
                                </div>
                                <div class="col-md-2" style ="font-weight:700;">
                                    Training Type :           </div>

<div class="col-md-2"> {{ $training->training->training_type }}
                                </div>

                            </div>




                        </div>

                    </div>



                    <div class="col-md-12">


                        <div style = "text-align:center;">

                            <h1 style="padding-left:20px;margin-top:0;margin-bottom:30px" style = "text-align:center;" >Acceptance Form</h1>

                        </div>


                        <form method="get" action="{{ url('admin/training/acceptance/form') }}">
                            {{ csrf_field() }}
                            
                            <div class="container-fluid">

                                <div class="row">

                                    <input type="hidden" value="{{ Auth::id() }}" name="staff_id" id="">
                                    <input type="hidden" value="{{ $training->training_id }}" name="training_id" id="">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">Are you coming ?</label>
                                            <br>
                                            <input type="radio" value="1" name="avalibility" id="formGroupExampleInput">
                                            <label for="">Yes</label>
                                            <input type="radio" value="0" name="avalibility" id="formGroupExampleInput">
                                            <label for="">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="formGroupExampleInput">Reasons:</label>
                                            <br>
                                            <textarea style = "width: 100%;" name="remarks" id="" cols="70" rows="4"></textarea>

                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

<script>
    import Index from "../../../public/adminlte/bower_components/Flot/examples/zooming/index.html";
    export default {
        components: {
            Index
        }
    }

</script>
