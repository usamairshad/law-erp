@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Add Training</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">Training</h3>

        </div>
        <div class="panel-body pad table-responsive">
            <form method="POST" action="{{ route('admin.training.update', ['id' => "$training->id"]) }}">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Venue:</label>
                                <input type="text" class="form-control" value="{{ $training->venue }}" name="venue"
                                    id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Department:</label>
                                <input type="text" value="{{ $training->department }}" class="form-control"
                                    name="department" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Attendee:</label>
                                <input type="text" value="{{ $training->designation }}" class="form-control"
                                    name="designation" id="formGroupExampleInput">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Description:</label>
                                <input type="text" value="{{ $training->description }}" class="form-control"
                                    name="description" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Date:</label>
                                <input type="date" name="date" value="{{ $training->date }}" class="form-control"
                                    id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Start Time:</label>
                                <input type="time" id="appt" class="form-control" value="{{ $training->time }}"
                                    name="time" min="09:00" max="18:00" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Training Type:</label>
                                <input type="text" class="form-control" value="{{ $training->training_type }}"
                                    name="training_type" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">No.Of.Days:</label>
                                <input type="number" name="no_of_days" value="{{ $training->no_of_days }}"
                                    class="form-control" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Branch:</label>
                                <input value="{{ $training->branch_id }}" id="something" name="branch_id"
                                    class="form-control" list="somethingelse">
                                <datalist id="somethingelse">

                                    @foreach ($branches as $branch)

                                        <option value="{{ $branch->id }}">
                                            {{ $branch->name }}
                                        </option>

                                    @endforeach

                                </datalist>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Trainer:</label>
                                <input type="text" class="form-control" value="{{ $training->training_by }}"
                                    name="training_by" id="formGroupExampleInput">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Training Chargies:</label>
                                <br>
                                <input type="number" value="{{ $training->training_charges }}" name="training_charges" class="form-control" min="0">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Category:</label>
                                <br>
                                <input type="radio" name="category" {{ $training->category == 'local' ? 'checked' : '' }}
                                    value="local" id="formGroupExampleInput">
                                <label for="">Local</label>
                                <input type="radio" name="category"
                                    {{ $training->category == 'international' ? 'checked' : '' }} value="international"
                                    id="formGroupExampleInput">
                                <label for="">International</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4" style="margin-top: 25px">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>
@stop

<script>
    import Index from "../../../public/adminlte/bower_components/Flot/examples/zooming/index.html";
    export default {
        components: {
            Index
        }
    }

</script>
