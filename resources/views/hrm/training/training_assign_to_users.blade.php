@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Training Assigned To Users</h1>
    </section>
    {{ csrf_field() }}
    <input type="hidden" name="name" value="abc">

@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>

          

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($trainings) > 0 ? 'datatable' : '' }}"
                id="users-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Venue</th>
                        <th>Category</th>
                        <th>Date</th>
                        <th>Start Time</th>
                        <th>Attending</th>
                        <th>Acknowledge</th>
                        <th>Remarks</th>
                       {{--  <th>Actions</th> --}}
                    </tr>
                </thead>

                <tbody>

                    @foreach ($trainings as $key => $training)


                        <tr>

                            <td>{{ ++$key }}</td>
                            <td>{{ $training->user->name }}</td>
                            <td>{{ $training->user->email }}</td>
                            <td>{{ $training->training->venue }}</td>
                            <td>{{ $training->training->category }}</td>
                            <td>{{ $training->training->date }}</td>
                            <td>{{ $training->training->time }}</td>
                            <td>{{ $training->avalibility == 1 ? 'Yes' : 'No' }}</td>
                            <td>{{ $training->acknowledge == 0 ? 'Pending' : 'Acknowledged' }}</td>
                            <td>{{ $training->remarks }}</td>
                            

                        </tr>

                    @endforeach





                </tbody>




            </table>
        </div>
    </div>

   










@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });


        $("#role_select").on("change", function() {
            $role_id = $(this).find(":selected").val();

            getUsersByRole($role_id);

        });


        function getUsersByRole($role_id) {

            $.ajax({

                url: "{{ url('admin/training/users-by/role') }}/" + $role_id,
                success: function(res) {
                    $html = "";
                    $('#users_select').html("");

                    res.forEach(data => {

                        $html += ` <option value="${data.id}">${data.name}</option>         `;


                    });


                    $('#users_select').append($html);


                }
            });

        }


        $('.assign-modal-anchor').on("click", function() {

            $training_id = $(this).data("training-id");

            $("#training_id_input").val($training_id);

        });



        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });

    </script>





@endsection
