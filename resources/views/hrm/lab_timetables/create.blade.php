@extends('layouts.app')
@inject('request', 'Illuminate\Http\Request')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
@stop
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Lab Timetable</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Lab Timetable</h3>
            <a href="{{ route('admin.lab_timetable.index') }}" class="btn btn-success pull-right">Back</a>
        </div>

       {!! Form::open(['method' => 'POST', 'route' => ['admin.lab_timetable.store'], 'id' => 'validation-form' , 'enctype' => 'multipart/form-data']) !!} 
       {!! Form::token(); !!}
        <div class="box-body">
            @include('hrm.lab_timetables.field')
        </div>

        <div class="box-footer">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>

@endsection

