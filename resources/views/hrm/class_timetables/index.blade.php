@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Class Timetable</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
             @if (Gate::check('erp_class_time_table_create'))
                <a href="{{ route('admin.class_timetable.create') }}" class="btn btn-success pull-right">Add New Class TimeTable</a>
            @endif

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($class_timetables) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Time Table Name</th>
                        <th>Subject</th>
                        <th>Day</th>
                        <th>Action</th>
                        <th>Slot</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($class_timetables) > 0)
                        @foreach ($class_timetables as $class_timetable)
                            <tr data-entry-id="{{ $class_timetable->id }}">
                                 <td>{{ \App\Helpers\Helper::timetableIdToTitle($class_timetable->time_table_id) }}</td>
                               <td>{{ \App\Helpers\Helper::subjectIdToName($class_timetable->subject_id) }}</td>
                                <td>{{ $class_timetable->day }}</td>
                                <td>
                                    @if (Gate::check('erp_class_time_table_destroy'))
                                        {!! Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['admin.class_timetable.destroy', $class_timetable->id],
                                        ]) !!}
                                        {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger'])
                                        !!}
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                                <td>{{$class_timetable->timetable->start_time." - ".$class_timetable->timetable->end_time}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
            
    </script>

@endsection
