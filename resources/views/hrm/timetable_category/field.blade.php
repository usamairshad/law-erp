<div class="form-group col-md-4 @if($errors->has('name')) has-error @endif" style = "float:left;">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    <input type="text" name="name" class="form-control" required maxlength="30">
</div>
<div class="form-group col-md-6 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
     <input type="texarea" name="description" class="form-control" required maxlength="100">
</div>