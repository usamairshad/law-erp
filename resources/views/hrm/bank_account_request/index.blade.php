@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Bank Account Request</h3>      
     
        <!-- /.box-header -->

        @if(count($branches) > 0 )

        @foreach($branches as $branch)

        <div class="panel-body pad table-responsive">

            <h4>{{$branch['name']}}</h4>
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Branch Name</th>
                    <th>Satff Reg. No.</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Father Name</th>
                    <th>CNIC</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($data) > 0)
                    @foreach ($data as $key)
                        @if($key->branch==$branch['id'])
                        <tr data-entry-id="{{ $key->id }}">
                            <td>{{\App\Helpers\Helper::branchIdToName($key->branch)}}</td>
                            <td>{{ $key->staff_id }}</td>
                            <td>{{ $key->first_name }}</td> 
                            <td>{{ $key->last_name }}</td>
                            <td>{{ $key->father_name }}</td>                      
                            <td>{{ $key->cnic }}</td>
                            <td>{{$key->joining_date}}</td>
                            
                            
                            <td>
                                  <a href="{{ url('update_staff_account_details').'/'.$key->staff_id }}" class="btn btn-xs btn-info">Update Account Details </a>
                                        <a href="{{ url('print_bank_account_request-list').'/'.$key->id }}" class="btn btn-xs btn-danger">PDF</a>

                                

                            </td>
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

        @endforeach
        @endif

    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });
    </script>
@endsection