@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Staff Bank Account Form</h1>
    </section>
@stop

<style type="text/css">
  .form-group{
    float: left;
  }
</style>

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                    <h3 style = "float:left;">Add Account Details </h3>
            <a href="{{ url()->previous() }}" style="float: right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->


        {!! Form::open(['method' => 'POST', 'url' => 'update_staff_account_details' , 'enctype' => 'multipart/form-data' , 'id' => 'validation-form']) !!}


<br>
<br>

        <div class="box-body">
            {{csrf_field()}}
            
            <div style="float: left" class="form-group col-md-4 @if($errors->has('staff_reg_no')) has-error @endif">
                {!! Form::label('staff_reg_no', 'Branch Name', ['class' => 'control-label']) !!}
                <input type="text" name="staff_reg_no" readonly id="staff_reg_no" value="{{$staff_data->reg_no}}"  class="form-control" >
            </div>
            <div style="float: left" class="form-group col-md-4 @if($errors->has('staff_first_name')) has-error @endif">
                {!! Form::label('staff_first_name', 'First Name', ['class' => 'control-label']) !!}
                <input type="text" name="staff_first_name" readonly id="staff_first_name" value="{{$staff_data->first_name}}"  class="form-control" >
            </div>
            <div class="form-group col-md-4 @if($errors->has('staff_last_name')) has-error @endif">
                {!! Form::label('staff_last_name', 'Last Name', ['class' => 'control-label']) !!}
                <input type="text" name="staff_last_name" readonly id="staff_last_name" value="{{$staff_data->last_name}}"  class="form-control" >
            </div>




  
          <div class="form-group col-md-4 @if($errors->has('account_title')) has-error @endif">
              {!! Form::label('account_title', 'Account Title*', ['class' => 'control-label']) !!}
              <input type="text" name="account_title" class="form-control" >
          </div>
          <div class="form-group col-md-4 @if($errors->has('account_number')) has-error @endif">
              {!! Form::label('account_number', 'Account Number*', ['class' => 'control-label']) !!}
              <input type="text" name="account_number" class="form-control" >
          </div>
          <div class="form-group col-md-4 @if($errors->has('bank_branch')) has-error @endif">
              {!! Form::label('bank_branch', 'Bank Branch*', ['class' => 'control-label']) !!}
              <input type="text" name="bank_branch" class="form-control" >
          </div>    
  
        </div>


        <!-- /.box-body -->


        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}

    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/advance_payroll/create_modify.js') }}" type="text/javascript"></script>

@endsection

