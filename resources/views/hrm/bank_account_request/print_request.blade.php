<!DOCTYPE html>
<html>
<head>
	<title>Print PDF </title>

	<style type="text/css">

		

	</style>
</head>
<body>

	<div style="padding-left: 15%;padding-right: 15%; padding-top: 15%; width: 100%;">
		<p style="font-weight: bold;">Date: {{date("d F, Y", strtotime(date("Y-m-d")))}}</p>
	</div>
	<div style="padding-left: 15%;padding-right: 15%; padding-top: 5%; width: 100%;">
		<h1 style="text-align: center;font-size: 26px;"><u>Account Opening Request</u></h1>
	</div>

	<div style="padding-left: 15%;padding-right: 15%; padding-top: 5%; width: 100%;">
		<p style="text-align:justify; font-size: 18px;">
			It is stated that {{$data->first_name}} {{$data->last_name}} <b> CNIC # {{$data->cnic}} </b> S/O: {{$data->father_name}} joined our prestigious institutions on {{date("d M, Y", strtotime($data->joining_date))}}. He is appointed as {{$data->designation}}, in {{\App\Helpers\Helper::branchIdToName($data->branch)}} of Roots IVY international School System. His gross salary for the given post according to PAK Rupees is {{$data->salary}}/-. We request you to please open his salary account at your ealiest. You kind and quick response shall be appreciated.
		</p> 	
	</div>

	<div style="padding-left: 15%;padding-right: 15%; padding-top: 5%; width: 100%;">
		<h1 style="font-size: 22px;">Maria Nouman</h1>
		<p style="text-align:justify; font-size: 18px;line-height: 5px;">Regional HR Manager</p>
		<p style="text-align:justify; font-size: 18px;line-height: 5px;">Department of Human Resource Management</p>
		<p style="text-align:justify; font-size: 18px;line-height: 5px;">Roots IVY International School System</p>
		<p style="text-align:justify; font-size: 18px;line-height: 5px;">Maria.nouman@rootsivyintschools.edu.pk</p> 	
	</div>


</body>
</html>