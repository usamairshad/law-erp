<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif">
    {!! Form::label('user_id', 'Employee Name*', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $Employees, old('user_id'), ['id' => 'user_id', 'class' => 'form-control select2','required']) !!}
    <span id="user_id_handler"></span>
    @if($errors->has('user_id'))
        <span class="help-block">
                        {{ $errors->first('user_id') }}
                </span>
    @endif
</div>
<div class="form-group  col-md-3 @if($errors->has('bank_id')) has-error @endif">
    <?php
        $Bankname =\App\Models\Admin\BanksModel::pluck('bank_name','id');
    ?>
        {!! Form::label('bank_id', 'Bank Name', ['class' => 'control-label']) !!}
        {!! Form::select('bank_id', $Bankname->prepend('Select Bank Name', ''),old('bank_id'), ['class' => 'form-control select2','required']) !!}
        @if($errors->has('bank_id'))
                <span class="help-block">
                        {{ $errors->first('bank_id') }}
                </span>
        @endif
</div>



<div class="form-group col-md-3 @if($errors->has('branch')) has-error @endif" >
        {!! Form::label('branch', 'Branch Name', ['class' => 'control-label']) !!}
        {!! Form::text('branch',  old('branch'), ['class' => 'form-control' ,'maxlength'=>'100']) !!}
        @if($errors->has('branch'))
                <span class="help-block">
            {{ $errors->first('branch') }}
        </span>
        @endif
</div>
<div class="form-group col-md-3 @if($errors->has('branch_code')) has-error @endif">
        {!! Form::label('branch_code', 'Branch Code', ['class' => 'control-label']) !!}
        {!! Form::text('branch_code', old('branch_code'), ['class' => 'form-control' ,'maxlength'=>'200']) !!}
        @if($errors->has('branch_code'))
                <span class="help-block">
            {{ $errors->first('branch_code') }}
        </span>
        @endif
</div>



<div class="form-group col-md-3 @if($errors->has('title')) has-error @endif" >
    {!! Form::label('title', 'Account Title', ['class' => 'control-label']) !!}
    {!! Form::text('title',  old('title'), ['class' => 'form-control','required']) !!}
    @if($errors->has('title'))
        <span class="help-block">
            {{ $errors->first('title') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('account_no')) has-error @endif">
    {!! Form::label('account_no', 'Account No', ['class' => 'control-label']) !!}
    {!! Form::text('account_no', old('account_no'), ['class' => 'form-control']) !!}
    @if($errors->has('account_no'))
        <span class="help-block">
            {{ $errors->first('account_no') }}
        </span>
    @endif
</div>



<div class="form-group col-md-3 @if($errors->has('iban')) has-error @endif">
    {!! Form::label('iban', 'IBAN', ['class' => 'control-label']) !!}
    {!! Form::text('iban', old('iban'), ['class' => 'form-control']) !!}
    @if($errors->has('iban'))
        <span class="help-block">
            {{ $errors->first('iban') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('type')) has-error @endif">
    {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
    {!! Form::select('type', array() + Config::get('hrm.account_type_array'), old('type'), ['class' => 'form-control']) !!}
    @if($errors->has('type'))
        <span class="help-block">
            {{ $errors->first('type') }}
        </span>
    @endif
</div>
