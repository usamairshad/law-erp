@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Banks</h1>
    </section>
   {{csrf_field()}}
    {{--<input type="hidden" name="name" value="abc">--}}

    {{--<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif" id="user_div">--}}
        {{--{!! Form::label('user_id', 'User ', ['class' => 'control-label']) !!}--}}
        {{--{!! Form::select('user_id', $Employees , old('user_id'),--}}
         {{--['class' => 'form-control select2'  ,'id' => 'user_id']) !!}--}}
    {{--</div>--}}

    {{--<button id="search_button" onclick="FormControls.fetchFilterRecord();" type="button" style="margin-top: 25px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search </b> </button>--}}
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('contacts_create'))
                <a href="{{ route('hrm.employee_banks.create') }}" class="btn btn-success pull-right">Add Employee Bank</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($EmployeeBanks) > 0 ? 'datatable' : '' }}" id="users-table" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>User</th>
                    <th>Account Number </th>
                    <th>IBAN</th>
                    <th>Bank</th>
                    <th>Branch</th>
                    <th>Branch Code</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        {{--$(document).ready(function(){--}}

            {{--$('#users-table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{!! route('hrm.employee_banks.datatables') !!}',--}}
                {{--columns: [--}}
                    {{--{ data: 'id', name: 'id' },--}}
                    {{--{ data: 'user_name', name: 'user_name' },--}}
                    {{--{ data: 'branch', name: 'branch' },--}}
                    {{--{ data: 'checkin_time', name: 'checkin_time' },--}}
                    {{--{ data: 'checkout_time', name: 'checkout_time' },--}}

                    {{--{ data: 'created_at', name: 'created_at' },--}}
                    {{--{ data: 'action', name: 'action' , orderable: false, searchable: false},--}}

                {{--]--}}
            {{--});--}}
        {{--});--}}
    </script>
    <script src="{{ url('public/js/hrm/employee_banks/list.js') }}" type="text/javascript"></script>
@endsection