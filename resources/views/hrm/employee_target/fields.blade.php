
<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" >
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name',  old('name'), ['class' => 'form-control' , 'maxlength'=>'200', 'required']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('type')) has-error @endif">
    {!! Form::label('type', 'Target Type *', ['class' => 'control-label']) !!}
    {!! Form::select('type', array('' => 'Select type') + Config::get('hrm.target_type_array'), old('type'), ['class' => 'form-control', 'required']) !!}
    @if($errors->has('type'))
        <span class="help-block">
            {{ $errors->first('type') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('year')) has-error @endif">
    {!! Form::label('year', 'Year *', ['class' => 'control-label']) !!}
    {!! Form::select('year', array('' => 'Select an Year') + Config::get('hrm.year_array'), old('year'), ['class' => 'form-control']) !!}
    @if($errors->has('year'))
        <span class="help-block">
            {{ $errors->first('year') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('brand')) has-error @endif">
        {!! Form::label('brand', 'Brand', ['class' => 'control-label']) !!}
        {!! Form::select('brand', $SuppliersModel, old('brand'), ['class' => 'form-control']) !!}
        @if($errors->has('brand'))
                <span class="help-block">
            {{ $errors->first('brand') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('category')) has-error @endif" id="category_div">
        {!! Form::label('category', 'Product Category', ['class' => 'control-label']) !!}
        {!! Form::select('category',  $StockCategoryModel , old('category'),
         ['class' => 'form-control'  ,'id' => 'category']) !!}
        @if($errors->has('category'))
                <span class="help-block">
            {{ $errors->first('category') }}
        </span>
        @endif
</div>


<div class="form-group col-md-3 @if($errors->has('class_type')) has-error @endif" id="class_type_div">
    {!! Form::label('class_type', 'Product Class', ['class' => 'control-label']) !!}
    {!! Form::select('class_type', array('' => 'Select Product Class')+ Config::get('admin.product_class_array') , old('class_type'),
     ['class' => 'form-control'  ,'id' => 'class_type']) !!}
    @if($errors->has('class_type'))
        <span class="help-block">
            {{ $errors->first('class_type') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('product_type')) has-error @endif" id="product_type_div">
        {!! Form::label('product_type', 'Product Function', ['class' => 'control-label']) !!}
        {!! Form::select('product_type',  array('' => 'Select Product Type')+ Config::get('admin.product_function_array') , old('product_type'),
         ['class' => 'form-control'  ,'id' => 'product_type']) !!}
        @if($errors->has('product_type'))
                <span class="help-block">
            {{ $errors->first('product_type') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('product_id')) has-error @endif">
        {!! Form::label('product_id', 'Product', ['class' => 'control-label ']) !!}
        {!! Form::select('product_id', $Products, old('product_id'), ['class' => 'form-control select2']) !!}
        @if($errors->has('product_id'))
                <span class="help-block">
            {{ $errors->first('product_id') }}
        </span>
        @endif
</div>




