@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Target</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Employee Target</h3>
            <a href="{{ route('hrm.employee_target.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($EmployeeTarget, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.employee_target.update', $EmployeeTarget->id]]) !!}
        <div class="box-body">
            @include('hrm.employee_target.fields')
        </div>
        <!-- /.box-body -->
        {!! Form::hidden('id',  $EmployeeTarget->id  , ['id' => 'id'] ) !!}

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/employee_target/create_modify.js') }}" type="text/javascript"></script>
@endsection