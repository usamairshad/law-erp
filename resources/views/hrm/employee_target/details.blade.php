@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Target Configuration Detail</h1>
    </section>
@stop

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body basic_info">

                    <div class="box-header with-border">
                        <h3 class="box-title">General Information</h3>

                        @if(Gate::allows('employee_target_edit'))
                            {{--<a href="{{ route('employee_target/'.$EmployeesTarget->id .'/edit') }}" class="btn btn-success pull-right">Edit</a>--}}
                        @endif
                    </div>

                    <div class="panel-body pad table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Title</th>
                                    <td colspan="3">@if($EmployeesTarget->name){{ $EmployeesTarget->name }}  @else{{ 'N/A' }}@endif</td>
                                </tr>

                                <tr>
                                    <th>Target Type </th>
                                    <td>@if($EmployeesTarget->type){{ Config::get('hrm.target_type_array.'.$EmployeesTarget->type)  }}@else{{ 'N/A' }}@endif</td>
                                    <th>Target Year </th>
                                    <td>@if($EmployeesTarget->year){{ $EmployeesTarget->year }}@else{{ 'N/A' }}@endif</td>

                                </tr>
                                <tr>
                                    <th>Brand </th>
                                    <td>@if($EmployeesTarget->brand){{ $EmployeesTarget->brand_name->sup_name  }}@else{{ 'N/A' }}@endif</td>
                                    <th>Product </th>
                                    <td>@if($EmployeesTarget->product_id){{ $EmployeesTarget->product_name->products_name  }}@else{{ 'N/A' }}@endif</td>
                                </tr>


                                <tr>
                                    <th>Product Category </th>
                                    <td>@if($EmployeesTarget->category){{ $EmployeesTarget->categ_name->cat_name }}@else{{ 'N/A' }}@endif</td>
                                    <th>Product Class</th>
                                    <td>@if($EmployeesTarget->product_class){{ Config::get('admin.product_class_array.'.$EmployeesTarget->type) }}@else{{ 'N/A' }}@endif</td>
                                </tr>

                                <tr>
                                    <th>Product Function </th>
                                    <td colspan="3">@if($EmployeesTarget->product_type){{ Config::get('admin.product_type_array.'.$EmployeesTarget->product_type)}}@else{{ 'N/A' }}@endif</td>

                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>



            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/marketing/contacts/create_modify.js') }}" type="text/javascript"></script>
@endsection