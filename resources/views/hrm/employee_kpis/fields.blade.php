<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif">
    {!! Form::label('user_id', 'Employee *', ['class' => 'control-label']) !!}
    {!! Form::select('user_id',  $Employees, old('user_id'), ['id' => 'user_id', 'class' => 'form-control select2']) !!}
    <span id="user_id_handler"></span>
    @if($errors->has('user_id'))
        <span class="help-block">
                        {{ $errors->first('user_id') }}
                </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Title *', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), [ 'id' => 'name', 'min' => 1, 'class' => 'form-control', 'required']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('year')) has-error @endif">
    {!! Form::label('year', 'Year *', ['class' => 'control-label']) !!}
    {!! Form::select('year', array('' => 'Select an Year') + Config::get('hrm.year_array'), old('year'), ['class' => 'form-control']) !!}
    @if($errors->has('year'))
        <span class="help-block">
            {{ $errors->first('year') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('value')) has-error @endif">
    {!! Form::label('value', 'Value *', ['class' => 'control-label']) !!}
    {!! Form::number('value', old('value'), [ 'id' => 'value', 'min' => 1, 'class' => 'form-control']) !!}
    @if($errors->has('value'))
        <span class="help-block">
            {{ $errors->first('value') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('weight')) has-error @endif">
    {!! Form::label('weight', 'Weightage * (in  %)', ['class' => 'control-label']) !!}
    {!! Form::number('weight', old('weight'), [ 'id' => 'weight', 'min' => 1, 'class' => 'form-control']) !!}
    @if($errors->has('weight'))
        <span class="help-block">
            {{ $errors->first('weight') }}
        </span>
    @endif
</div>



@if($request->segment(4) == 'edit')
<div class="form-group col-md-3 @if($errors->has('acheived')) has-error @endif">
    {!! Form::label('acheived', 'Acheived ', ['class' => 'control-label']) !!}
    {!! Form::number('acheived', old('acheived'), [ 'id' => 'acheived', 'min' => 1, 'class' => 'form-control']) !!}
    @if($errors->has('acheived'))
        <span class="help-block">
            {{ $errors->first('acheived') }}
        </span>
    @endif
</div>
@endif