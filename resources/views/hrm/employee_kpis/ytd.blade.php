@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>YTD</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">YTD</h3>
            <a href="{{ route('hrm.assign_target.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->



        <div class="box-body">
            {!! Form::open(['method' => 'POST', 'route' => ['hrm.assign_target.store'], 'id' => 'validation-form']) !!}
            <div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif">
                {!! Form::label('user_id', 'Employee *', ['class' => 'control-label']) !!}
                {!! Form::select('user_id', $Employees, old('user_id'), ['id' => 'user_id', 'class' => 'form-control select2']) !!}
                <span id="user_id_handler"></span>
                @if($errors->has('user_id'))
                    <span class="help-block">
                        {{ $errors->first('user_id') }}
                </span>
                @endif
            </div>
            {!! Form::button('Search', ['class' => 'btn btn-info',  'onclick'=>"FormControls.fetchYTD();", "style"=> "margin-top:25px"]) !!}
            {{--{!! Form::submit(trans('Search'), ['class' => 'btn btn-info']) !!}--}}
            <div class="col-md-12" >
            <div class="panel-body pad table-responsive">
                <table class="table table-bordered table-striped  datatable" id="ytd-table" >

                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Employee</th>
                        <th>Target</th>
                        <th>Value</th>
                        <th>YTD Target</th>
                        <th>YTD Acheived</th>
                        <th>Acheived %</th>
                        <th>Weight</th>
                        <th>Weight %</th>
                        {{--<th>Actions</th>--}}
                    </tr>
                    </thead>

                    <tbody id="ytd-body">

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->


        <div class="box-footer">

        </div>
        {!! Form::close() !!}

    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/assign_target/create_modify.js') }}" type="text/javascript"></script>

@endsection

