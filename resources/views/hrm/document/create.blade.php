@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Upload Documents </h3>
            <a href="{{ route('admin.documents.index') }}" style ="float:right;" class="btn btn-success pull-right">Back</a>
        </div>

       {!! Form::open(['method' => 'POST', 'route' => ['admin.documents.store'] ,'enctype' => 'multipart/form-data']) !!}

        <div class="box-body" style = "margin-top:40px;">
            @include('hrm.document.field')
        </div>

        <div class="box-footer" >
           <button class="btn btn-primary" style = "margin-top:30px;"  type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(".select2").select2({

        });
    </script>
@endsection

