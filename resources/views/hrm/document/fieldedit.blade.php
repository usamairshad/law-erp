<?php 
$branch_id = Auth::user()->branch_id;
$user_id = Auth::user()->id;
$role_id = DB::table('erp_model_has_roles')->where('model_id',$user_id)->value('role_id');
$role = \Spatie\Permission\Models\Role::where('id',$role_id)->value('name');
  ?>

  <style type="text/css">
        .form-group{
            float:left  !important;
        }
    </style>



<input type="hidden" name="member_type" value="{{$role}}">
<input type="hidden" name="branch_id" value="{{$branch_id}}">
<input type="hidden" name="member_id" value="{{$user_id}}">

<div class="row">
    <div class="form-group col-md-3 @if($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Title*', ['class' => 'control-label']) !!}
    <input type="text" name="title" value="{{$document->title}}" placeholder="Title" class="form-control" required>
</div>
<!-- <div class="form-group col-md-3 @if($errors->has('file')) has-error @endif">
    {!! Form::label('file', 'File*', ['class' => 'control-label']) !!}
    <input type="file" name="file" value="{{$document->file}}" class="form-control" required>
</div> -->
<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif">
    {!! Form::label('description', 'Description*', ['class' => 'control-label']) !!}
    <input type="text" name="description" value="{{$document->description}}" class="form-control" required>
</div>
<div class="form-group col-md-4 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Branch Name', ['class' => 'control-label']) !!}
    <input class="form-control" type="text" readonly value="{{\App\Helpers\Helper::branchIdToName($branch_id)}}">
</div>
  
</div>



