@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">

    

@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Edit Documents </h3>
            <a href="{{ route('admin.staff.profile') }}" style ="float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($document, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.documents.update', $document->id]]) !!}
        <div class="box-body">
            @include('hrm.document.fieldedit')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
           <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/staff/create_modify.js') }}" type="text/javascript"></script>
@endsection