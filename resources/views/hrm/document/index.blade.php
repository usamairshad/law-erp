@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Documents Record </h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($documents) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Uploaded By</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>File</th>
                    <th>Date & Time</th>
                </tr>
                </thead>

                <tbody>
                @if (count($documents) > 0)
                    @foreach ($documents as $key => $document)
                        <tr data-entry-id="{{ $document->id }}">
                            <td>{{\App\Helpers\Helper::userIdToName($document->created_by)}}</td>

                            <td>{{ $document->title }}</td>
                            <td>{{ $document->description }}</td>
                           <td> <a href="{{asset($document->file)}}"> Document {{$key+1}} </a></td>
                            <td>{{ $document->created_at }}</td>
                            
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });
    </script>
@endsection