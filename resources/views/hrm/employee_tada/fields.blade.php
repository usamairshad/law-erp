<div class="form-group col-md-3 @if($errors->has('job_title_id')) has-error @endif">
    {!! Form::label('job_title_id', 'Job Title *', ['class' => 'control-label']) !!}
    {!! Form::select('job_title_id', $JobTitles, old('job_title_id'), ['id' => 'job_title_id', 'class' => 'form-control select2']) !!}
    <span id="job_title_id_handler"></span>
    @if($errors->has('job_title_id'))
        <span class="help-block">
                        {{ $errors->first('job_title_id') }}
                </span>
    @endif
</div>




<div class="form-group col-md-3 @if($errors->has('tada_amount')) has-error @endif" >
    {!! Form::label('tada_amount', 'Tada Amount', ['class' => 'control-label']) !!}
    {!! Form::text('tada_amount',  old('tada_amount'), ['class' => 'form-control']) !!}
    @if($errors->has('tada_amount'))
        <span class="help-block">
            {{ $errors->first('tada_amount') }}
        </span>
    @endif
</div>





<div class="form-group col-md-3 @if($errors->has('year')) has-error @endif">
    {!! Form::label('year', 'Year *', ['class' => 'control-label']) !!}
    {!! Form::select('year', array('' => 'Select an Year') + Config::get('hrm.year_array'), old('year'), ['class' => 'form-control']) !!}
    @if($errors->has('year'))
        <span class="help-block">
            {{ $errors->first('year') }}
        </span>
    @endif
</div>
