@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Attendance Settings</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Attendance Settings</h3>
            <a href="{{ route('hrm.attendance_settings.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($AttendanceSetting, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.attendance_settings.update', $AttendanceSetting->id]]) !!}
        <div class="box-body">
            @include('hrm.attendance_settings.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/attendance_settings/create_modify.js') }}" type="text/javascript"></script>
@endsection