@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Attendance Settings</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('attendance_settings_create'))
                <a href="{{ route('hrm.attendance_settings.create') }}" class="btn btn-success pull-right">Add New Settings</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($AttendanceSettings) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Shift</th>
                    <th>In Start</th>
                    <th>In End</th>
                    <th>Out Start</th>
                    <th>Out End</th>
                    <th>Type</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($AttendanceSettings) > 0)
                    @foreach ($AttendanceSettings as $settings)
                        <tr data-entry-id="{{ $settings->id }}">
                            <td>{{ $settings->id }}</td>
                            <td>{{ $settings->shift_name->name }}</td>
                            <td>{{ $settings->in_start }}</td>
                            <td>{{ $settings->in_end }}</td>
                            <td>{{ $settings->out_start }}</td>
                            <td>{{ $settings->out_end }}</td>

                            <td>@if($settings->attendance_type){{ Config::get('hrm.attendance_type_array')[$settings->attendance_type] }}@else{{'N/A'}}@endif</td>
                            <td>{{date('d-m-Y  h:m a', strtotime( $settings->created_at ))}}</td>
                            <td>


                                @if(Gate::check('attendance_settings_edit'))
                                    <a href="{{ route('hrm.attendance_settings.edit',[$settings->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                @if(Gate::check('attendance_settings_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['hrm.attendance_settings.destroy', $settings->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection