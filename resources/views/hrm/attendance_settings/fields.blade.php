<div class="form-group col-md-4 @if($errors->has('shift_id')) has-error @endif">
    {!! Form::label('shift_id', 'Work Shifts*', ['class' => 'control-label']) !!}
    {!! Form::select('shift_id', $WorkShifts, old('shift_id'), [ 'id' => 'shift_id', 'class' => 'form-control']) !!}
    <span id="shift_id_handler"></span>
    @if($errors->has('shift_id'))
        <span class="help-block">
                {{ $errors->first('shift_id') }}
        </span>
    @endif
</div>


<div class="form-group col-md-2 @if($errors->has('in_start')) has-error @endif">
    {!! Form::label('in_start', 'In Start *', ['class' => 'control-label']) !!}
    {!! Form::number('in_start', old('in_start'),
    ['onblur' => 'FormControls.applyMaxValue();',
    'onkeyup' => 'FormControls.applyMaxValue();', 'id' => 'in_start', 'min' => -525, 'class' => 'form-control', 'required']) !!}
    @if($errors->has('in_start'))
        <span class="help-block">
            {{ $errors->first('in_start') }}
        </span>
    @endif
</div>

<div class="form-group col-md-2 @if($errors->has('in_end')) has-error @endif">
    {!! Form::label('in_end', 'In End *', ['class' => 'control-label']) !!}
    {!! Form::number('in_end', old('in_end'), ['onblur' => 'FormControls.applyMaxValue();', 'onkeyup' => 'FormControls.applyMaxValue();', 'id' => 'in_end', 'class' => 'form-control', 'required']) !!}
    @if($errors->has('in_end'))
        <span class="help-block">
            {{ $errors->first('in_end') }}
        </span>
    @endif
</div>


<div class="form-group col-md-2 @if($errors->has('out_start')) has-error @endif">
    {!! Form::label('out_start', 'Out Start *', ['class' => 'control-label']) !!}
    {!! Form::number('out_start', old('out_start'), ['onblur' => 'FormControls.applyMaxValue();', 'onkeyup' => 'FormControls.applyMaxValue();', 'id' => 'out_start', 'class' => 'form-control', 'required']) !!}
    @if($errors->has('out_start'))
        <span class="help-block">
            {{ $errors->first('out_start') }}
        </span>
    @endif
</div>

<div class="form-group col-md-2 @if($errors->has('out_end')) has-error @endif">
    {!! Form::label('out_end', 'Out End *', ['class' => 'control-label']) !!}
    {!! Form::number('out_end', old('out_end'), ['onblur' => 'FormControls.applyMaxValue();', 'onkeyup' => 'FormControls.applyMaxValue();', 'id' => 'out_end', 'class' => 'form-control', 'required']) !!}
    @if($errors->has('out_end'))
        <span class="help-block">
            {{ $errors->first('out_end') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 allowed @if($errors->has('attendance_type')) has-error @endif">
    {!! Form::label('attendance_type', 'Type*', ['class' => 'control-label']) !!}
    {!! Form::select('attendance_type', Config::get('hrm.attendance_type_array'), old('attendance_type'), ['class' => 'form-control']) !!}
    @if($errors->has('attendance_type'))
        <span class="help-block">
                        {{ $errors->first('attendance_type') }}
                </span>
    @endif
</div>


