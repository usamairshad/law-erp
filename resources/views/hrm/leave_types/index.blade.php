@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Leave Types</h3>
            @if(Gate::check('leave_types_create'))
            @endif
                <a href="{{ route('hrm.leave_types.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Leave Type</a>
           
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($LeaveTypes) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Permitted Days</th>
                    <th>Apply Condition?</th>
                    <th>Allowed Number</th>
                    <th>Allowed Type</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($LeaveTypes) > 0)
                    @foreach ($LeaveTypes as $LeaveType)
                        <tr data-entry-id="{{ $LeaveType->id }}">
                            <td>{{ $LeaveType->id }}</td>
                            <td>{{ $LeaveType->name }}</td>
                            <td>{{ $LeaveType->permitted_days }}</td>
                            <td>@if($LeaveType->condition){{ 'Yes' }}@else{{'No'}}@endif</td>
                            <td>@if($LeaveType->condition){{ $LeaveType->allowed_number }}@else{{'N/A'}}@endif</td>
                            <td>@if($LeaveType->condition){{ Config::get('hrm.allowed_type_array')[$LeaveType->allowed_type] }}@else{{'N/A'}}@endif</td>
                            <td>{{date('d-m-Y h:m a ', strtotime( $LeaveType->created_at)) }}</td>
                            <td>
                                @if($LeaveType->status && Gate::check('leave_types_inactive'))
                                @elseif(!$LeaveType->status && Gate::check('leave_types_active'))
                                @endif
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['hrm.leave_types.inactive', $LeaveType->id])) !!}
                                    {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}
                            
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['hrm.leave_types.active', $LeaveType->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                              

                                @if(Gate::check('leave_types_edit'))
                                @endif
                                    <a href="{{ route('hrm.leave_types.edit',[$LeaveType->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                               

                                @if(Gate::check('leave_types_destroy'))
                                @endif
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['hrm.leave_types.destroy', $LeaveType->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection