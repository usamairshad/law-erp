<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style = "float:left;">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>

<div class="form-group col-md-2 @if($errors->has('permitted_days')) has-error @endif" style = "float:left;">
    {!! Form::label('permitted_days', 'Permitted Days*', ['class' => 'control-label']) !!}
    {!! Form::number('permitted_days', old('permitted_days'), ['onblur' => 'FormControls.applyMaxValue();', 'onkeyup' => 'FormControls.applyMaxValue();', 'id' => 'permitted_days', 'min' => 0, 'class' => 'form-control']) !!}
    @if($errors->has('permitted_days'))
        <span class="help-block">
            {{ $errors->first('permitted_days') }}
        </span>
    @endif
</div>

<div class="form-group col-md-2 @if($errors->has('condition')) has-error @endif" style = "float:left;">
        {!! Form::label('condition', 'Apply Conditions?', ['id' => 'condition', 'class' => 'control-label']) !!}<br/>
        {{ Form::checkbox('condition', 1, old('condition')) }}&nbsp;Yes
        @if($errors->has('condition'))
                <span class="help-block">
                        {{ $errors->first('condition') }}
                </span>
        @endif
</div>

<div class="form-group col-md-2 allowed @if($errors->has('allowed_number')) has-error @endif" style = "float:left;">
        {!! Form::label('allowed_number', 'Allowed', ['class' => 'control-label']) !!}
        {!! Form::number('allowed_number', old('allowed_number'), ['onblur' => 'FormControls.applyMaxValue();', 'onkeyup' => 'FormControls.applyMaxValue();', 'id' => 'allowed_number', 'min' => 0, 'class' => 'form-control']) !!}
        @if($errors->has('allowed_number'))
                <span class="help-block">
                        {{ $errors->first('allowed_number') }}
                </span>
        @endif
</div>

<div class="form-group col-md-3 allowed @if($errors->has('allowed_type')) has-error @endif" style = "float:left;">
        {!! Form::label('allowed_type', 'Allowed Type*', ['class' => 'control-label']) !!}
        {!! Form::select('allowed_type', Config::get('hrm.allowed_type_array'), old('allowed_type'), ['class' => 'form-control']) !!}
        @if($errors->has('allowed_type'))
                <span class="help-block">
                        {{ $errors->first('allowed_type') }}
                </span>
        @endif
</div>
