@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
{{csrf_field()}}
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Working Days</h1>
    </section>

    <div class="form-group col-md-3 @if($errors->has('month')) has-error @endif" id="month_div">
        {!! Form::label('month', 'Month *', ['class' => 'control-label']) !!}
        {!! Form::select('month', array('Choose a Month') + Config::get('hrm.month_array') , old('month'),
         ['class' => 'form-control select2'  ,'id' => 'month']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('year')) has-error @endif" id="year_div">
        {!! Form::label('year', 'Year *', ['class' => 'control-label']) !!}
        {!! Form::select('year', array('Choose an Year') + Config::get('hrm.year_array') , old('year'),
         ['class' => 'form-control select2'  ,'id' => 'year']) !!}
    </div>

    <button id="search_button" style="margin-top: 25px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search </b> </button>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($EmployeeWorkingDays) > 0 ? 'datatable' : '' }}" id="users-table" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>User</th>
                    <th>Job Title</th>
                    <th>Date</th>
                    <th>Office In</th>
                    <th>Office Out</th>
                    <th>Working Hours</th>
                    <th>Day Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- The info modal starts here -->
    <div class="modal fade" id="favoritesModal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="favoritesModalLabel"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="user_id" name="user_id">
                    <input type="hidden" id="date" name="date">

                        <table class="table table-bordered table-striped" id="in-out-datables" >
                            <thead>
                                <tr>
                                    <th>Date Time</th>
                                    <th>Attendance Type</th>
                                </tr>
                            </thead>
                        </table>

                    <div id='edit-panel' class="@if($errors->has('attendance_type')) has-error @endif">
                        {!! Form::label('attendance_type', 'Attendace Type * ', ['class' => 'control-label']) !!}
                        {!! Form::select('attendance_type', array('' => 'Set Manual Status') + Config::get('hrm.attendance_type_array'), old('attendance_type'), ['id' => 'attendance_type', 'class' => 'form-control']) !!}
                        <span id="attendance_type_handler"></span>
                        @if($errors->has('attendance_type'))
                                <span class="help-block">
                                        {{ $errors->first('attendance_type') }}
                                </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="update">Update</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        var token = $("input[name=_token]").val();

        $('#users-table').DataTable();
        $('#search_button').on('click', function(Event){

            var month = $('#month').val();
            var year = $('#year').val();
            var today = new Date();
            var date = new Date(month+'-01-'+year);

            if(date < today){
                if(month && year){
                    //fetching data of given month and year
                    $('#users-table').DataTable().destroy();
                    $('#users-table').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: {
                                url :'employee_working_days/fetch_filter_records',
                                method: 'POST',
                                data:  {
                                    month : month,
                                    year : year,
                                    mode : 'given_month',
                                    _token: token
                                }
                            },
                        columns: [
                            { data: 'id', name: 'id' },
                            { data: 'user_name', name: 'user_name' },
                            { data: 'job_title', name: 'job_title' },
                            { data: 'date', name: 'date' },
                            { data: 'office_in', name: 'office_in' },
                            { data: 'office_out', name: 'office_out' },
                            { data: 'working_hours', name: 'working_hours' },
                            { data: 'day_status', name: 'day_status' },
                            { data: 'action', name: 'action' , orderable: false, searchable: false},
                        ]
                    });
                }
                else{
                    alert('Please fill the required fields');
                    Event.preventDefault();
                }
            }
            else{
                alert('Please Enter a Valid Month and Year');
                Event.preventDefault();
            }
        });

        $(function() {
            $('#favoritesModal').on("show.bs.modal", function (e) {
                $("#favoritesModalLabel").html($(e.relatedTarget).data('title'));
                $("#fav-title").html($(e.relatedTarget).data('title'));
                $("#user_id").val($(e.relatedTarget).data('id'));
                $("#date").val($(e.relatedTarget).data('date'));

                if($(e.relatedTarget).data('mode') == 'view'){
                    $('#in-out-datables').show();
                    $('#in-out-datables').addClass('datatable');
                    $('#update').hide();
                    $('#edit-panel').hide();
                    $('#in-out-datables').DataTable().destroy();
                    $('#in-out-datables').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url :'employee_working_days/employee_in_outs',
                            method: 'POST',
                            data:  {
                                'id' : $(e.relatedTarget).data('id'),
                                'date' : $(e.relatedTarget).data('date'),
                                _token : token
                            }
                        },
                        columns: [
                                    { data: 'date_time', name: 'date_time' },
                                    { data: 'attendance_type', name: 'attendance_type' },
                                ],        
                    });
                }
                else{
                    $('#in-out-datables').DataTable().destroy();
                    $('#in-out-datables').hide();
                    $('#in-out-datables').removeClass('datatable');
                    $('#edit-panel').show();
                    $('#update').show();
                }
            });
        });

        $(function(){
            $('#update').on('click', function(){
                if($('#attendance_type').val() == ''){
                    alert('Please Choose a Type');
                }
                else{
                    $.ajax({
                        url: 'employee_working_days/change_attendance_type',
                        type: "POST",
                        data: {
                            'user_id' : $("#user_id").val(),
                            'date' : $("#date").val(),
                            'type_id' : $('#attendance_type').val(),
                            _token : token
                        }, 
                        success:function(data) {
                            $('#users-table').DataTable().ajax.reload();
                        }
                    });
                }
            });
        });

        function getDay(date) {
            var d = new Date(date),
                day = '' + d.getDate();
            if (day.length < 2) day = '0' + day;
            return day;
        }
    </script>
    <!-- <script src="{{ url('public/js/hrm/employee_working_days/list.js') }}" type="text/javascript"></script> -->
@endsection