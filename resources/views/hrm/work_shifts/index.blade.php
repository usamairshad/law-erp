@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Work Shifts</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('work_shifts_create'))
                <a href="{{ route('hrm.work_shifts.create') }}" class="btn btn-success pull-right">Add New Work Shift</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($WorkShifts) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Shift Hours</th>
                    <th>Shift Start</th>
                    <th>Shift End</th>
                    <th>Working Hours</th>
                    <th>Break Start</th>
                    <th>Break End</th>
                    <th>Break Hours</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($WorkShifts) > 0)
                    @foreach ($WorkShifts as $WorkShift)
                        <tr data-entry-id="{{ $WorkShift->id }}">
                            <td>{{ $WorkShift->id }}</td>
                            <td>{{ $WorkShift->name }}</td>
                            <td>{{ $WorkShift->hours_per_day }}&nbsp;Hours</td>
                            <td>@if($WorkShift->shift_start_time){{ Config::get('hrm.time_array')[$WorkShift->shift_start_time] }}@else{{ 'N/A' }}@endif</td>
                            <td>@if($WorkShift->shift_end_time){{ Config::get('hrm.time_array')[$WorkShift->shift_end_time] }}@else{{ 'N/A' }}@endif</td>
                            <td>{{ $WorkShift->working_hours_per_day }}&nbsp;Hours</td>
                            <td>@if($WorkShift->break_start_time){{ Config::get('hrm.time_array')[$WorkShift->break_start_time] }}@else{{ 'N/A' }}@endif</td>
                            <td>@if($WorkShift->break_end_time){{ Config::get('hrm.time_array')[$WorkShift->break_end_time] }}@else{{ 'N/A' }}@endif</td>
                            <td>{{ $WorkShift->break_hours_per_day }}&nbsp;Hours</td>
                            <td>
                                @if($WorkShift->status && Gate::check('work_shifts_inactive'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['hrm.work_shifts.inactive', $WorkShift->id])) !!}
                                    {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}
                                @elseif(!$WorkShift->status && Gate::check('work_shifts_active'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['hrm.work_shifts.active', $WorkShift->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                                @endif

                                @if(Gate::check('work_shifts_edit'))
                                    <a href="{{ route('hrm.work_shifts.edit',[$WorkShift->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                @if(Gate::check('work_shifts_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['hrm.work_shifts.destroy', $WorkShift->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection