<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('shift_start_time')) has-error @endif">
    {!! Form::label('shift_start_time', 'Shift Start Time*', ['class' => 'control-label']) !!}
    {!! Form::select('shift_start_time', array('' => 'Select a Start Time') + Config::get('hrm.time_array'), old('shift_start_time'), ['id' => 'shift_start_time', 'class' => 'form-control']) !!}
    @if($errors->has('shift_start_time'))
        <span class="help-block">
            {{ $errors->first('shift_start_time') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('shift_end_time')) has-error @endif">
    {!! Form::label('shift_end_time', 'Shift End Time*', ['class' => 'control-label']) !!}
        {!! Form::select('shift_end_time', array('' => 'Select an End Time') + Config::get('hrm.time_array'), old('shift_end_time'), ['id' => 'shift_end_time', 'class' => 'form-control']) !!}
    @if($errors->has('shift_end_time'))
        <span class="help-block">
            {{ $errors->first('shift_end_time') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('working_hours_per_day')) has-error @endif">
    {!! Form::label('working_hours_per_day', 'Working Duration*', ['class' => 'control-label']) !!}
    {!! Form::text('working_hours_per_day', old('hours_per_day'), ['readonly' => 'true', 'id' => 'working_hours_per_day', 'class' => 'form-control']) !!}
    @if($errors->has('working_hours_per_day'))
        <span class="help-block">
            {{ $errors->first('working_hours_per_day') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('break_start_time')) has-error @endif">
    {!! Form::label('break_start_time', 'Break Start Time*', ['class' => 'control-label']) !!}
    {!! Form::select('break_start_time', array('' => 'Select Break Start Time') + Config::get('hrm.time_array'), old('break_start_time'), ['id' => 'break_start_time', 'class' => 'form-control']) !!}
    @if($errors->has('break_start_time'))
        <span class="help-block">
            {{ $errors->first('break_start_time') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('break_end_time')) has-error @endif">
    {!! Form::label('end_time', 'Break End Time*', ['class' => 'control-label']) !!}
        {!! Form::select('break_end_time', array('' => 'Select Break End Time') + Config::get('hrm.time_array'), old('break_end_time'), ['id' => 'break_end_time', 'class' => 'form-control']) !!}
    @if($errors->has('break_end_time'))
        <span class="help-block">
            {{ $errors->first('break_end_time') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('break_hours_per_day')) has-error @endif">
    {!! Form::label('break_hours_per_day', 'Break Duration*', ['class' => 'control-label']) !!}
    {!! Form::text('break_hours_per_day', old('hours_per_day'), ['readonly' => 'true', 'id' => 'break_hours_per_day', 'class' => 'form-control']) !!}
    @if($errors->has('break_hours_per_day'))
        <span class="help-block">
            {{ $errors->first('break_hours_per_day') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('hours_per_day')) has-error @endif">
    {!! Form::label('hours_per_day', 'Duration*', ['class' => 'control-label']) !!}
    {!! Form::text('hours_per_day', old('hours_per_day'), ['readonly' => 'true', 'id' => 'hours_per_day', 'class' => 'form-control']) !!}
    @if($errors->has('hours_per_day'))
        <span class="help-block">
            {{ $errors->first('hours_per_day') }}
        </span>
    @endif
</div>