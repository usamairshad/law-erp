@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Staff</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Staff</h3>
            <a href="{{ route('admin.attendance_payroll.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::open(['method' => 'PUT','route' => ['admin.attendance_payroll.update',$attendance_values->id]]) !!}

        {!! Form::token(); !!}
        <div class="box-body">
            @include('hrm.attendance_payroll.editfield')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
              <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/staff/create_modify.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function(){
            $('.phone').mask('0000-0000000');
            $('.cnic').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');  
        })
    </script>

@endsection