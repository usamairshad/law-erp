@extends('layouts.app')
@inject('request', 'Illuminate\Http\Request')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
@stop
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Attendance Sheet Details of Student </h1>
    </section>
@stop

@section('content')
<div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ 'datatable'}}">
                <thead>

                    <tr>
                        <th>Total Presents</th>
                        <th>Total Absents</th>
                        <th>Total Leaves</th>
                        <th>Total Short Leaves</th>

                    </tr>
                </thead>

                <tbody>
                   
                    
                       
                            <tr>
                                <td>{{$Attendance_Data['present_attendance']}}</td>
                                <td>{{$Attendance_Data['absent_attendance']}}</td>
                                <td>{{$Attendance_Data['leave_attendance']}}</td>
                                <td>{{$Attendance_Data['short_leave_attendance']}}</td>

                                
                            </tr>

                </tbody>
            </table>    

</div>                            
@stop

@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>

    <script type="text/javascript">




     







        $(document).ready(function(){
            $('.phone').mask('0000-0000000');
            $('.cnic').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');

        })
        
    </script>
@endsection










<script type="text/javascript">
    $(document).ready(function(){

        

        $(document).on('change','.SelectBoard',function () 
        {

            alert("Good");
            // var prod_id=$(this).val();

            // var a=$('#program').parent();
            

            // console.log(prod_id);
            // var op="";
            // $.ajax({
            //     type:'get',
            //     url:'{!!URL::to('find_board_program')!!}',
            //     data:{'id':prod_id},
            //     dataType:'json',//return data will be json
            //     success:function(data){


            //         // here price is coloumn name in products table data.coln name

            //         a.find('#program').val(data.program);
                    
            //     },
            //     error:function(){
            //     }
            // });


        });
</script>
