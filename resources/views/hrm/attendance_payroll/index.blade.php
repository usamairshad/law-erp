@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Lecture Wise Attendance Details</h1>
    </section>
@stop

@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">List</h3>
            
               <a style="float: right;" href="{{ route('admin.attendance_payroll.create') }}" class="btn btn-success pull-right">Mark Attendace</a>
            

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($attendance_payrolls) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        
                        <th>Staff Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Attendance</th>
                        <th>Attendance Type</th>
                        
                        <th>Couse Name</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($attendance_payrolls) > 0)
                        @foreach ($attendance_payrolls as $att_rolls)
                            <tr>

                                <td>{{ \App\Helpers\Helper::getStaffIDToFirstName($att_rolls->staff_id) }}</td>
                               
                               
                                <td>{{ $att_rolls->class_Date }}</td>
                                <td>{{ $att_rolls->class_time}}</td>
                                <td>{{ $att_rolls->attendance}}</td>
                                <td>{{ $att_rolls->attendance_type}}</td>
                                <td>{{ \App\Helpers\Helper::subjectIdToName($att_rolls->course_id) }}</td>
                
                                </td>
                                
                                
                              {{-- <td>

                                    <a href="{{ route('admin.attendance_payroll.edit', [$att_rolls->id]) }}"
                                            class="btn btn-xs btn-info">Edit</a>
                                    
                                    
                                        <a href="{{ route('admin.attendance_payroll.show', [$att_rolls->id]) }}"
                                            class="btn btn-xs btn-info">View</a>
                                </td>--}}

                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>













@stop

@section('javascript')

    <script>



        $(document).ready(function()
        {
            var course_id=$('#course_id').val();
            
            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_course_name')!!}',
                data:{'course_id':course_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                   // alert(data.name);
                    //$html = '<option value="null">select class</option>';
                    // data.forEach((dat) => {
                    //     $html += `<option value=${dat.get_class.id}>${dat.get_class.name}</option>`;
                    $html = `<td>${data.name}</td>`;
                    // });
                    document.getElementById("coursee").innerHTML = data.name;
                     

                },
                error:function(){
                }
            });

        });


        $(document).ready(function() {
            $('.datatable').DataTable()
        });

        $(".warning").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#warning_user_input').val($user_id);

        });


        $(".terminate").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#terminate_user_input').val($user_id);

        });


        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });


        $(".transferBranchButton").on("click", function() {

            $user_id = $(this).data('user-id');
            $branch_name = $(this).data('branch-name');
            $name = $(this).data('name');
            $status = $(this).data('status');



            console.log($user_id, $branch_name, $name, $status);



            $('#transfer_staff_id').val();
            $('#branch_name_transfer').text('');
            $('#staff_name_transfer').text('');
            $('#status_name_transfer').text('');



            $('#transfer_staff_id').val($user_id);
            $('#branch_name_transfer').text($branch_name);
            $('#staff_name_transfer').text($name);
            $('#status_name_transfer').text($status);

            $('#action_name_transfer').attr("href", `{{ url('admin/delete/staff/branch') }}/${$user_id}`);



        });

        $('.promotionDemotion').on("click", function() {

            $name = $(this).data('user-name');
            $user_role_id = $(this).data('user-role-id');
            $user_id = $(this).data('user-id');
            $branch_id = $(this).data('branch-id');

            $('#user_name_promotion').val($name);

            $('#promotion_user_id').val($user_id);
            $('#staff_user_id').val($user_id);
            $("#active_teacher_id").val($user_id);
            console.log($user_id);
            $(".roles").select2().val($user_role_id).trigger('change');


            loadBorad($branch_id);

        });

        function loadBorad($branch_id) {
            $.ajax({
                url: `{{ url('admin/getBoard/') }}/${$branch_id}`,

                success: function(response) {
                    $html = '<option value="null">Select board</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.board_id}>${res.board.name}</option>`;

                    });

                    $("#boards").data("branch-id", $branch_id);
                    $("#boards").html($html);
                }
            });
        }
        ////////////////on board change get program/////////////// 
        $("#boards").on("change", function() {

            $board_id = $(this).find(":selected").val();
            $branch_id = $(this).data('branch-id');

            loadProgram($branch_id, $board_id);
        });

        function loadProgram($branch, $board) {
            $.ajax({
                url: `{{ url('admin/getProgram/') }}/${$branch}/${$board}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select program</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.program_id}>${res.program.name}</option>`;

                    });

                    $("#programs").data('branch-id', $branch);
                    $("#programs").data('board-id', $board);
                    $("#programs").html($html);
                }
            });
        }

        ////////////////on program change get class/////////////// 
        $("#programs").on("change", function() {

            $board_id = $(this).data("board-id");
            $branch_id = $(this).data('branch-id');
            $program_id = $(this).find(':selected').val();

            console.log($board_id, $branch_id, $program_id);

            loadClasses($branch_id, $board_id, $program_id);
        });

        function loadClasses($branch, $board, $program) {
            $.ajax({
                url: `{{ url('admin/getClasses/') }}/${$branch}/${$board}/${$program}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select class</option>';

                    response.forEach((res) => {

                        $html +=
                            `<option data-active-s-id=${res.id}  value=${res.get_class.id}>${res.get_class.name}</option>`;

                    });

                    $("#classes").html($html);
                }
            });
        }
        $("#classes").change(function() {
            $activeSessionID= $(this).find(":selected").data("active-s-id");
            $("#active_session_id").val($activeSessionID);
            console.log($activeSessionID);
        });


        $("#classes").change(function() {

            $activeSessionID= $(this).find(":selected").data("active-s-id");

            $("#active_session_id").val($activeSessionID);
            console.log($activeSessionID);
        });
        

    </script>
@endsection
