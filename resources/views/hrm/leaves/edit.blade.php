@extends('layouts.app')

@section('stylesheet')

@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Leave Requests</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Leave Request</h3>
            <a href="{{ route('hrm.leave_requests.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($LeaveRequest, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.leave_requests.update', $LeaveRequest->id]]) !!}
        <div class="box-body">
            @include('hrm.leave_requests.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/js/hrm/leave_requests/create_modify.js') }}" type="text/javascript"></script>
@endsection