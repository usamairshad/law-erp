@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Leaves Approved</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Leaves) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Employee</th>
                    <th>Leave Type</th>
                    <th>Work Shift</th>
                    <th>Leave Status</th>
                    <th>Leave Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                </tr>
                </thead>

                <tbody>
                @if (count($Leaves) > 0)
                    @foreach ($Leaves as $Leave)
                        <tr data-entry-id="{{ $Leave->id }}">
                            <td>{{ $Leave->id }}</td>
                            <td>{{ $Leave->first_name }}</td>
                            <td>{{ $Leave->leave_type->name }}</td>
                            <td>{{ $Leave->work_shift->name }}</td>
                            <td>{{ $Leave->leave_status->leave_name }}</td>
                            <td>{{  date('d-m-Y', strtotime($Leave->leave_date)) }}</td>
                            <td>{{ $Leave->start_time }}</td>
                            <td>{{ $Leave->end_time }}</td>
                            
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
        function leaveAction(leave_id , val) {
            var token = $("input[name=_token]").val();
            var status = confirm('Are you sure to change status');
            if(status){

                $.ajax({
                    url: route('hrm.leaves.change_status'),
                    type: 'post',
                    data: {
                        leave_id: leave_id,
                        leave_status_id: val,
                        _token: token,
                    },
                    success: function( data, textStatus, jQxhr ){
                        //$('#loadEmployeeEntitlements').html(data);
                        location.reload();
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        //$('#loadEmployeeEntitlements').html('<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-warning"></i> Alert!</h4>Something went worng, please try again later.</div>');
                        alert('failed');
                    }
                });

            }
            else{
                alert('abortted');
            }

        }
    </script>
@endsection