@extends('layouts.app')

@section('stylesheet')

@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Available</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Employee Available</h3>

        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['hrm.leaves.available_result'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="form-group col-md-3 @if($errors->has('employee_id')) has-error @endif">
                {!! Form::label('employee_id', 'Employee Name*', ['class' => 'control-label']) !!}
                {!! Form::select('employee_id', $Employees, old('employee_id'), ['id' => 'employee_id', 'class' => 'form-control']) !!}
                <span id="employee_id_handler"></span>
                @if($errors->has('employee_id'))
                    <span class="help-block">
                            {{ $errors->first('employee_id') }}
                    </span>
                @endif
            </div>


            <div class="form-group col-md-3 @if($errors->has('start_date')) has-error @endif">
                {!! Form::label('start_date', 'Start Date*', ['class' => 'control-label']) !!}
                {!! Form::text('start_date', old('start_date'), ['id' => 'start_date', 'class' => 'form-control']) !!}
                @if($errors->has('start_date'))
                    <span class="help-block">
                            {{ $errors->first('start_date') }}
                    </span>
                @endif
            </div>

            <div class="form-group col-md-3 @if($errors->has('end_date')) has-error @endif">
                {!! Form::label('end_date', 'End Date*', ['class' => 'control-label']) !!}
                {!! Form::text('end_date', old('end_date'), ['id' => 'end_date', 'class' => 'form-control']) !!}
                @if($errors->has('end_date'))
                    <span class="help-block">
                            {{ $errors->first('end_date') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 " style="margin-top: 25px;">
                {!! Form::submit(trans('Search'), ['class' => 'btn btn-info']) !!}
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">

        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <!-- bootstrap datepicker -->

    <script src="{{ url('public/js/hrm/leave_requests/create_modify.js') }}" type="text/javascript"></script>
@endsection

