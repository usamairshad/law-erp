<table class="table table-bordered">
        <tbody>
                <tr>
                        <th width="30%">Employee</th>
                        <td>{{ $Employee->first_name . ' ' . $Employee->last_name }}</td>
                </tr>
                <tr>
                        <th>Leave Type</th>
                        <td>{{ $LeaveType->name }}</td>
                </tr>
                <tr>
                        <th>As of Date</th>
                        <td>{{ \Carbon\Carbon::now()->format('Y-m-d') }}</td>
                </tr>
        </tbody>
</table>

<table class="table table-striped col-md-6">
        <tbody>
        <tr>
                <th width="70%">Entitled</th>
                <td>{{ $LeaveEntitlement->no_of_days }}</td>
        </tr>
        <tr>
                <th>Taken</th>
                <td>{{ $LeaveEntitlement->days_used }}</td>
        </tr>
        <tr>
                <th>Scheduled</th>
                <td>0</td>
        </tr>
        <tr>
                <th>Pending Approval</th>
                <td>0</td>
        </tr>
        <tr>
                <th>Balance</th>
                <th>{{ $LeaveEntitlement->no_of_days - $LeaveEntitlement->days_used }}</th>
        </tr>
        </tbody>
</table>