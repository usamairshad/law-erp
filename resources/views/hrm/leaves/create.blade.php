@extends('layouts.app')

@section('stylesheet')

@stop

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Leave Requests</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Leave Request</h3>
            <a href="{{ route('hrm.leave_requests.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['hrm.leave_requests.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('hrm.leave_requests.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <!-- bootstrap datepicker -->

    <script src="{{ url('public/js/hrm/leave_requests/create_modify.js') }}" type="text/javascript"></script>
@endsection

