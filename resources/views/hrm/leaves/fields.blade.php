<table class="table table-condensed">
        <tbody>
                <tr>
                        <td width="70%">
                                <div class="form-group col-md-4 @if($errors->has('employee_id')) has-error @endif">
                                        {!! Form::label('employee_id', 'Employee Name*', ['class' => 'control-label']) !!}
                                        {!! Form::select('employee_id', $Employees, old('employee_id'), ['id' => 'employee_id', 'class' => 'form-control']) !!}
                                        <span id="employee_id_handler"></span>
                                        @if($errors->has('employee_id'))
                                                <span class="help-block">
                                                        {{ $errors->first('employee_id') }}
                                                </span>
                                        @endif
                                </div>

                                <div class="form-group col-md-4 @if($errors->has('leave_type_id')) has-error @endif">
                                        {!! Form::label('leave_type_id', 'Leave Type*', ['class' => 'control-label']) !!}
                                        {!! Form::select('leave_type_id', $LeaveTypes, old('leave_type_id'), ['onchange' => 'FormControls.loadEmployeeEntitlements();', 'id' => 'leave_type_id', 'class' => 'form-control']) !!}
                                        <span id="leave_type_id_handler"></span>
                                        @if($errors->has('leave_type_id'))
                                                <span class="help-block">
                                                        {{ $errors->first('leave_type_id') }}
                                                </span>
                                        @endif
                                </div>

                                <div class="form-group col-md-4 @if($errors->has('work_shift_id')) has-error @endif">
                                        {!! Form::label('work_shift_id', 'Work Shifts*', ['class' => 'control-label']) !!}
                                        {!! Form::select('work_shift_id', $WorkShifts, old('work_shift_id'), ['onchange' => 'FormControls.loadEmployeeEntitlements();', 'id' => 'work_shift_id', 'class' => 'form-control']) !!}
                                        <span id="work_shift_id_handler"></span>
                                        @if($errors->has('work_shift_id'))
                                                <span class="help-block">
                                                        {{ $errors->first('work_shift_id') }}
                                                </span>
                                        @endif
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group col-md-4 @if($errors->has('start_date')) has-error @endif">
                                        {!! Form::label('start_date', 'Start Date*', ['class' => 'control-label']) !!}
                                        {!! Form::text('start_date', old('start_date'), ['id' => 'start_date', 'class' => 'form-control select2']) !!}
                                        @if($errors->has('start_date'))
                                                <span class="help-block">
                                                        {{ $errors->first('start_date') }}
                                                </span>
                                        @endif
                                </div>

                                <div class="form-group col-md-4 @if($errors->has('end_date')) has-error @endif">
                                        {!! Form::label('end_date', 'End Date*', ['class' => 'control-label']) !!}
                                        {!! Form::text('end_date', old('end_date'), ['id' => 'end_date', 'class' => 'form-control select2']) !!}
                                        @if($errors->has('end_date'))
                                                <span class="help-block">
                                                        {{ $errors->first('end_date') }}
                                                </span>
                                        @endif
                                </div>

                                <div class="form-group col-md-4 @if($errors->has('total_days')) has-error @endif">
                                        {!! Form::label('end_date', 'Days*', ['class' => 'control-label']) !!}
                                        {!! Form::number('total_days', old('total_days'), ['min' => 0, 'readonly' => true, 'id' => 'total_days', 'class' => 'form-control select2']) !!}
                                        @if($errors->has('total_days'))
                                                <span class="help-block">
                                                        {{ $errors->first('total_days') }}
                                                </span>
                                        @endif
                                </div>

                                <div id="single">
                                        <div class="form-group col-md-3 single_duration @if($errors->has('single_duration')) has-error @endif">
                                                {!! Form::label('single_duration', 'Duration*', ['class' => 'control-label']) !!}
                                                {!! Form::select('single_duration', array('full_day' => 'Full Day', 'half_day' => 'Half Day', 'specify_time' => 'Specify'), old('single_duration'), ['onchange' => 'FormControls.singleProcess();', 'id' => 'single_duration', 'class' => 'form-control']) !!}
                                                @if($errors->has('single_duration'))
                                                        <span class="help-block">
                                                                {{ $errors->first('single_duration') }}
                                                        </span>
                                                @endif
                                        </div>
                                        <div class="form-group col-md-3 single_shift @if($errors->has('single_shift')) has-error @endif">
                                                {!! Form::label('single_shift', 'Shift*', ['class' => 'control-label']) !!}
                                                {!! Form::select('single_shift', array('first_half' => 'First Half', 'second_half' => 'Second Half'), old('single_half'), ['id' => 'single_half', 'class' => 'form-control']) !!}
                                                @if($errors->has('single_shift'))
                                                        <span class="help-block">
                                                                {{ $errors->first('single_shift') }}
                                                        </span>
                                                @endif
                                        </div>
                                        <div class="form-group col-md-3 single_hours_start @if($errors->has('single_hours_start')) has-error @endif">
                                                {!! Form::label('single_hours_start', 'Start*', ['class' => 'control-label']) !!}
                                                {!! Form::select('single_hours_start', Config::get('hrm.time_array'), (old('single_hours_start')) ? old('single_hours_start') : $WorkShift->shift_start_time, ['onchange' => 'FormControls.hoursProcess("single");', 'id' => 'single_hours_start', 'class' => 'form-control']) !!}
                                                @if($errors->has('single_hours_start'))
                                                        <span class="help-block">
                                                                {{ $errors->first('single_hours_start') }}
                                                        </span>
                                                @endif
                                        </div>
                                        <div class="form-group col-md-3 single_hours_end @if($errors->has('single_hours_end')) has-error @endif">
                                                {!! Form::label('single_hours_end', 'End*', ['class' => 'control-label']) !!}
                                                {!! Form::select('single_hours_end', Config::get('hrm.time_array'), (old('single_hours_end')) ? old('signle_hours_end') : $WorkShift->shift_end_time, ['onchange' => 'FormControls.hoursProcess("single");', 'id' => 'single_hours_end', 'class' => 'form-control']) !!}
                                                @if($errors->has('single_hours_end'))
                                                        <span class="help-block">
                                                                {{ $errors->first('single_hours_end') }}
                                                        </span>
                                                @endif
                                        </div>
                                        <div class="form-group col-md-3 single_hours_duration @if($errors->has('single_hours_duration')) has-error @endif">
                                                {!! Form::label('single_hours_duration', 'Duration*', ['class' => 'control-label']) !!}
                                                {!! Form::number('single_hours_duration', old('single_hours_duration'), ['readonly' => true, 'id' => 'single_hours_duration', 'class' => 'form-control']) !!}
                                                @if($errors->has('single_hours_duration'))
                                                        <span class="help-block">
                                                                {{ $errors->first('single_hours_duration') }}
                                                        </span>
                                                @endif
                                        </div>
                                        <div class="clearfix"></div>
                                </div>

                                <div id="partial">
                                        <div class="form-group col-md-4 @if($errors->has('partial_days')) has-error @endif">
                                                {!! Form::label('partial_days', 'Leave Type*', ['class' => 'control-label']) !!}
                                                {!! Form::select('partial_days', array('none' => 'None', 'all' => 'All Days', 'start' => 'Start Day Only', 'end' => 'End Day Only', 'start_end' => 'Start and  End Day'), old('partial_days'), ['onchange' => 'FormControls.partialProcess();', 'id' => 'partial_days', 'class' => 'form-control']) !!}
                                                @if($errors->has('partial_days'))
                                                        <span class="help-block">
                                                                {{ $errors->first('partial_days') }}
                                                        </span>
                                                @endif
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="all_days">
                                                <div class="form-group col-md-3 all_days_duration @if($errors->has('all_days_duration')) has-error @endif">
                                                        {!! Form::label('all_days_duration', 'Duration*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('all_days_duration', array('half_day' => 'Half Day', 'specify_time' => 'Specify'), old('all_days_duration'), ['onchange' => 'FormControls.allDaysProcess();', 'id' => 'all_days_duration', 'class' => 'form-control']) !!}
                                                        @if($errors->has('all_days_duration'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('all_days_duration') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 all_days_shift @if($errors->has('all_days_shift')) has-error @endif">
                                                        {!! Form::label('all_days_shift', 'Shift*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('all_days_shift', array('first_half' => 'First Half', 'second_half' => 'Second Half'), old('all_days_half'), ['id' => 'all_days_half', 'class' => 'form-control']) !!}
                                                        @if($errors->has('all_days_shift'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('all_days_shift') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 all_days_hours_start @if($errors->has('all_days_hours_start')) has-error @endif">
                                                        {!! Form::label('all_days_hours_start', 'Start*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('all_days_hours_start', Config::get('hrm.time_array'), (old('all_days_hours_start')) ? old('all_days_hours_start') : $WorkShift->shift_start_time, ['onchange' => 'FormControls.hoursProcess("all_days");', 'id' => 'all_days_hours_start', 'class' => 'form-control']) !!}
                                                        @if($errors->has('all_days_hours_start'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('all_days_hours_start') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 all_days_hours_end @if($errors->has('all_days_hours_end')) has-error @endif">
                                                        {!! Form::label('all_days_hours_end', 'End*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('all_days_hours_end', Config::get('hrm.time_array'), (old('all_days_hours_end')) ? old('all_days_hours_end') : $WorkShift->shift_end_time, ['onchange' => 'FormControls.hoursProcess("all_days");', 'id' => 'all_days_hours_end', 'class' => 'form-control']) !!}
                                                        @if($errors->has('all_days_hours_end'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('all_days_hours_end') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 all_days_hours_duration @if($errors->has('all_days_hours_duration')) has-error @endif">
                                                        {!! Form::label('all_days_hours_duration', 'Duration*', ['class' => 'control-label']) !!}
                                                        {!! Form::number('all_days_hours_duration', old('all_days_hours_duration'), ['readonly' => true, 'id' => 'all_days_hours_duration', 'class' => 'form-control']) !!}
                                                        @if($errors->has('all_days_hours_duration'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('all_days_hours_duration') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="starting">
                                                <div class="form-group col-md-3 starting_duration @if($errors->has('starting_duration')) has-error @endif">
                                                        {!! Form::label('starting_duration', 'Start Day*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('starting_duration', array('half_day' => 'Half Day', 'specify_time' => 'Specify'), old('starting_duration'), ['onchange' => 'FormControls.startDayProcess();', 'id' => 'starting_duration', 'class' => 'form-control']) !!}
                                                        @if($errors->has('starting_duration'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('starting_duration') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 starting_shift @if($errors->has('starting_shift')) has-error @endif">
                                                        {!! Form::label('starting_shift', 'Shift*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('starting_shift', array('first_half' => 'First Half', 'second_half' => 'Second Half'), old('starting_half'), ['id' => 'starting_half', 'class' => 'form-control']) !!}
                                                        @if($errors->has('starting_shift'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('starting_shift') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 starting_hours_start @if($errors->has('starting_hours_start')) has-error @endif">
                                                        {!! Form::label('starting_hours_start', 'Start*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('starting_hours_start', Config::get('hrm.time_array'), (old('starting_hours_start')) ? old('starting_hours_start') : $WorkShift->shift_start_time, ['onchange' => 'FormControls.hoursProcess("starting");', 'id' => 'starting_hours_start', 'class' => 'form-control']) !!}
                                                        @if($errors->has('starting_hours_start'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('starting_hours_start') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 starting_hours_end @if($errors->has('starting_hours_end')) has-error @endif">
                                                        {!! Form::label('starting_hours_end', 'End*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('starting_hours_end', Config::get('hrm.time_array'), (old('starting_hours_end')) ? old('starting_hours_end') : $WorkShift->shift_end_time, ['onchange' => 'FormControls.hoursProcess("starting");', 'id' => 'starting_hours_end', 'class' => 'form-control']) !!}
                                                        @if($errors->has('starting_hours_end'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('starting_hours_end') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 starting_hours_duration @if($errors->has('starting_hours_duration')) has-error @endif">
                                                        {!! Form::label('starting_hours_duration', 'Duration*', ['class' => 'control-label']) !!}
                                                        {!! Form::number('starting_hours_duration', old('starting_hours_duration'), ['readonly' => true, 'id' => 'starting_hours_duration', 'class' => 'form-control']) !!}
                                                        @if($errors->has('starting_hours_duration'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('starting_hours_duration') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="ending">
                                                <div class="form-group col-md-3 ending_duration @if($errors->has('ending_duration')) has-error @endif">
                                                        {!! Form::label('ending_duration', 'End Day*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('ending_duration', array('half_day' => 'Half Day', 'specify_time' => 'Specify'), old('ending_duration'), ['onchange' => 'FormControls.endDayProcess();', 'id' => 'ending_duration', 'class' => 'form-control']) !!}
                                                        @if($errors->has('ending_duration'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('ending_duration') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 ending_shift @if($errors->has('ending_shift')) has-error @endif">
                                                        {!! Form::label('ending_shift', 'Shift*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('ending_shift', array('first_half' => 'First Half', 'second_half' => 'Second Half'), old('ending_half'), ['id' => 'ending_half', 'class' => 'form-control']) !!}
                                                        @if($errors->has('ending_shift'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('ending_shift') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 ending_hours_start @if($errors->has('ending_hours_start')) has-error @endif">
                                                        {!! Form::label('ending_hours_start', 'Start*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('ending_hours_start', Config::get('hrm.time_array'), (old('ending_hours_start')) ? old('ending_hours_start') : $WorkShift->shift_start_time, ['onchange' => 'FormControls.hoursProcess("ending");', 'id' => 'ending_hours_start', 'class' => 'form-control']) !!}
                                                        @if($errors->has('ending_hours_start'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('ending_hours_start') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 ending_hours_end @if($errors->has('ending_hours_end')) has-error @endif">
                                                        {!! Form::label('ending_hours_end', 'End*', ['class' => 'control-label']) !!}
                                                        {!! Form::select('ending_hours_end', Config::get('hrm.time_array'), (old('ending_hours_end')) ? old('ending_hours_end') : $WorkShift->shift_end_time, ['onchange' => 'FormControls.hoursProcess("ending");', 'id' => 'ending_hours_end', 'class' => 'form-control']) !!}
                                                        @if($errors->has('ending_hours_end'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('ending_hours_end') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="form-group col-md-3 ending_hours_duration @if($errors->has('ending_hours_duration')) has-error @endif">
                                                        {!! Form::label('ending_hours_duration', 'Duration*', ['class' => 'control-label']) !!}
                                                        {!! Form::number('ending_hours_duration', old('ending_hours_duration'), ['readonly' => true, 'id' => 'ending_hours_duration', 'class' => 'form-control']) !!}
                                                        @if($errors->has('ending_hours_duration'))
                                                                <span class="help-block">
                                                                        {{ $errors->first('ending_hours_duration') }}
                                                                </span>
                                                        @endif
                                                </div>
                                                <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-12 @if($errors->has('comments')) has-error @endif">
                                {!! Form::label('comments', 'Comments', ['class' => 'control-label']) !!}
                                {!! Form::textarea('comments', old('comments'), ['rows' => 3, 'id' => 'comments', 'class' => 'form-control']) !!}
                                <span id="comments_handler"></span>
                                @if($errors->has('comments'))
                                    <span class="help-block">
                                        {{ $errors->first('comments') }}
                                    </span>
                                @endif
                            </div>
                        </td>
                        <td><p align="center"><b>Leave Balance Details</b></p><span id="loadEmployeeEntitlements"></span></td>
                </tr>
        </tbody>
</table>