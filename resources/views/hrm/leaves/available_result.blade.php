@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Availability Result</h1>
        {!! Form::open(['method' => 'POST', 'route' => ['hrm.leaves.available_result'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <div class="form-group col-md-3 @if($errors->has('employee_id')) has-error @endif">
                {!! Form::label('employee_id', 'Employee Name*', ['class' => 'control-label']) !!}
                {!! Form::select('employee_id', $Employees, old('employee_id'), ['id' => 'employee_id', 'class' => 'form-control']) !!}
                <span id="employee_id_handler"></span>
                @if($errors->has('employee_id'))
                    <span class="help-block">
                            {{ $errors->first('employee_id') }}
                    </span>
                @endif
            </div>


            <div class="form-group col-md-3 @if($errors->has('start_date')) has-error @endif">
                {!! Form::label('start_date', 'Start Date*', ['class' => 'control-label']) !!}
                {!! Form::text('start_date', old('start_date'), ['id' => 'start_date', 'class' => 'form-control']) !!}
                @if($errors->has('start_date'))
                    <span class="help-block">
                            {{ $errors->first('start_date') }}
                    </span>
                @endif
            </div>

            <div class="form-group col-md-3 @if($errors->has('end_date')) has-error @endif">
                {!! Form::label('end_date', 'End Date*', ['class' => 'control-label']) !!}
                {!! Form::text('end_date', old('end_date'), ['id' => 'end_date', 'class' => 'form-control']) !!}
                @if($errors->has('end_date'))
                    <span class="help-block">
                            {{ $errors->first('end_date') }}
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 " style="margin-top: 25px;">
                {!! Form::submit(trans('Search'), ['class' => 'btn btn-info']) !!}
                <a href="{{ route('hrm.leaves.employee_available') }}" class="btn btn-success pull-right">Back</a>
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">

        </div>
        {!! Form::close() !!}
    </section>
@stop
{{csrf_field()}}
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            {{--@if(Gate::check('leaves_create'))--}}
                {{--<a href="{{ route('hrm.leaves.create') }}" class="btn btn-success pull-right">Create Leave</a>--}}
            {{--@endif--}}
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Leaves) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Employee</th>
                    <th>Leave Type</th>
                    <th>Work Shift</th>
                    <th>Leave Status</th>
                    <th>Leave Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>


                </tr>
                </thead>

                <tbody>
                @if (count($Leaves) > 0)
                    @foreach ($Leaves as $Leave)
                        <tr data-entry-id="{{ $Leave->id }}">
                            <td>{{ $Leave->id }}</td>
                            <td>{{ $Leave->employee->full_name }}</td>
                            <td>{{ $Leave->leave_type->name }}</td>
                            <td>{{ $Leave->work_shift->name }}</td>
                            <td>{{ $Leave->leave_status->leave_name }}</td>
                            <td>{{ $Leave->leave_date }}</td>
                            <td>{{ $Leave->start_time }}</td>
                            <td>{{ $Leave->end_time }}</td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/leave_requests/create_modify.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable();
            $(".datepicker").datepicker({ format: 'yyyy-mm-dd' });
            //$('.select2').select2();
            $('#employee_id').select2()
        });

    </script>
@endsection