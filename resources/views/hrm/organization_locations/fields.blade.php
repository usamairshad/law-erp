<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('phone')) has-error @endif">
    {!! Form::label('phone', 'Phone*', ['class' => 'control-label']) !!}
    {!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}
    @if($errors->has('phone'))
        <span class="help-block">
            {{ $errors->first('phone') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('fax')) has-error @endif">
    {!! Form::label('fax', 'Fax', ['class' => 'control-label']) !!}
    {!! Form::text('fax', old('phone'), ['class' => 'form-control']) !!}
    @if($errors->has('fax'))
        <span class="help-block">
            {{ $errors->first('fax') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('street_1')) has-error @endif">
    {!! Form::label('street_1', 'Address Line 1', ['class' => 'control-label']) !!}
    {!! Form::text('street_1', old('street_1'), ['class' => 'form-control']) !!}
    @if($errors->has('street_1'))
        <span class="help-block">
            {{ $errors->first('street_1') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('street_2')) has-error @endif">
    {!! Form::label('street_2', 'Address Line 2', ['class' => 'control-label']) !!}
    {!! Form::text('street_2', old('street_1'), ['class' => 'form-control']) !!}
    @if($errors->has('street_2'))
        <span class="help-block">
            {{ $errors->first('street_2') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('city')) has-error @endif">
    {!! Form::label('city', 'City', ['class' => 'control-label']) !!}
    {!! Form::text('city', old('city'), ['class' => 'form-control']) !!}
    @if($errors->has('city'))
        <span class="help-block">
            {{ $errors->first('city') }}
        </span>
    @endif
</div>

<div class="clearfix"></div>

<div class="form-group col-md-3 @if($errors->has('zip_code')) has-error @endif">
    {!! Form::label('zip_code', 'Zip Code', ['class' => 'control-label']) !!}
    {!! Form::text('zip_code', old('city'), ['class' => 'form-control']) !!}
    @if($errors->has('zip_code'))
        <span class="help-block">
            {{ $errors->first('zip_code') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('province')) has-error @endif">
    {!! Form::label('province', 'Province', ['class' => 'control-label']) !!}
    {!! Form::text('province', old('province'), ['class' => 'form-control']) !!}
    @if($errors->has('province'))
        <span class="help-block">
            {{ $errors->first('province') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('country')) has-error @endif">
    {!! Form::label('country', 'Country', ['class' => 'control-label']) !!}
    {!! Form::select('country', Config::get('hrm.country_array'), old('country'), ['class' => 'form-control']) !!}
    @if($errors->has('country'))
        <span class="help-block">
            {{ $errors->first('country') }}
        </span>
    @endif
</div>

<div class="form-group col-md-12 @if($errors->has('notes')) has-error @endif">
    {!! Form::label('notes', 'Notes', ['class' => 'control-label']) !!}
    {!! Form::textarea('notes', old('notes'), ['rows' => '3', 'class' => 'form-control']) !!}
    @if($errors->has('notes'))
        <span class="help-block">
            {{ $errors->first('notes') }}
        </span>
    @endif
</div>

{!! Form::hidden('is_default', (old('is_default')) ? encrypt(old('is_default')) : encrypt(0)) !!}
{!! Form::hidden('organization_id', encrypt(1)) !!}
