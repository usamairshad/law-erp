@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Organization Locations</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Organization Location</h3>
            <a href="{{ route('hrm.organization_locations.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($OrganizationLocation, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.organization_locations.update', $OrganizationLocation->id]]) !!}
        <div class="box-body">
            @include('hrm.organization_locations.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/organization_locations/create_modify.js') }}" type="text/javascript"></script>
@endsection