@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Organization Locations</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('organization_locations_create'))
                <a href="{{ route('hrm.organization_locations.create') }}" class="btn btn-success pull-right">Add New Organization Location</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($OrganizationLocations) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Address Line 1</th>
                    <th>City</th>
                    <th>Default</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($OrganizationLocations) > 0)
                    @foreach ($OrganizationLocations as $OrganizationLocation)
                        <tr data-entry-id="{{ $OrganizationLocation->id }}">
                            <td>{{ $OrganizationLocation->id }}</td>
                            <td>{{ $OrganizationLocation->name }}</td>
                            <td>@if($OrganizationLocation->phone){{ $OrganizationLocation->phone }}@else{{ 'N/A' }}@endif</td>
                            <td>@if($OrganizationLocation->street_1){{ $OrganizationLocation->street_1 }}@else{{ 'N/A' }}@endif</td>
                            <td>@if($OrganizationLocation->city){{ $OrganizationLocation->city }}@else{{ 'N/A' }}@endif</td>
                            <td>@if($OrganizationLocation->is_default) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                            <td>{{date('d-m-Y h:m a', strtotime( $OrganizationLocation->created_at ))}}</td>
                            <td>
                                @if(!$OrganizationLocation->is_default)
                                    @if($OrganizationLocation->status && Gate::check('organization_locations_inactive'))
                                        {!! Form::open(array(
                                                'style' => 'display: inline-block;',
                                                'method' => 'PATCH',
                                                'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                                'route' => ['hrm.organization_locations.inactive', $OrganizationLocation->id])) !!}
                                        {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                        {!! Form::close() !!}
                                    @elseif(!$OrganizationLocation->status && Gate::check('organization_locations_active'))
                                        {!! Form::open(array(
                                                'style' => 'display: inline-block;',
                                                'method' => 'PATCH',
                                                'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                                'route' => ['hrm.organization_locations.active', $OrganizationLocation->id])) !!}
                                        {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                        {!! Form::close() !!}
                                    @endif
                                @endif

                                @if(Gate::check('organization_locations_edit'))
                                    <a href="{{ route('hrm.organization_locations.edit',[$OrganizationLocation->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                @if(!$OrganizationLocation->is_default)
                                    @if(Gate::check('organization_locations_destroy'))
                                        {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'DELETE',
                                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                            'route' => ['hrm.organization_locations.destroy', $OrganizationLocation->id])) !!}
                                        {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                        {!! Form::close() !!}
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection