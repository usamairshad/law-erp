@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="row row-sm">
					<!-- Col -->
					<div class="col-lg-4">
						<div class="card mg-b-20">
							<div class="card-body">
								<div class="pl-0">
									<div class="main-profile-overview">
										<div class="main-img-user profile-user"><img alt="" src="{{asset('public/').'/'.$image}}">{{--<a href="JavaScript:void(0);" class="fas fa-camera profile-edit"></a>--}}</div>
										<div class="d-flex justify-content-between mg-b-20">
											<div>
												<h5 class="main-profile-name">{{ $users->first_name ?? '' }} {{ $users->last_name ?? '' }}</h5>
												<p class="main-profile-name-text">{{ $role ?? '' }}</p>
											</div>
										</div>
										<h6>Branch Name</h6>
										<div class="main-profile-bio">
										{{ \App\Helpers\Helper::branchIdToName($users->branch_id) }}	</div>

										<!-- main-profile-bio -->
										<div class="main-profile-work-list">
											<div class="media">
												<div class="media-logo bg-success-transparent text-success">	
													<i class="fas fa-briefcase"></i>
												</div> 
												<div class="media-body">
													<h6>Experience</span>
													<p>{{ $users->experience }} Years</p>
												</div>
											</div>
											<div class="media">
												<div class="media-logo bg-primary-transparent text-primary">
													<i class="icon ion-logo-buffer"></i>
												</div>
												<div class="media-body">
													<h6>Qualification</h6>
													<p>{{ $users->qualification }}</p>
												</div>
											</div>
										</div>
										<!-- main-profile-work-list -->

										<hr class="mg-y-30">
										<label class="main-content-label tx-13 mg-b-20">Company Details</label>
										<div class="main-profile-social-list">
											<div class="media">
												<div class="media-icon bg-primary-transparent text-primary">
													<i class="far fa-registered"></i>
												</div>
												<div class="media-body">
													<span>Registration No:</span> <a href="#">{{ $users->reg_no }}</a>
												</div>
											</div>
											<div class="media">
												<div class="media-icon bg-success-transparent text-success">
													<i class="far fa-flag"></i>
												</div>
												<div class="media-body">
													<span>Nationality:</span> <a href="#">{{ $users->nationality }}</a>
												</div>
											</div>
										
										</div><!-- main-profile-social-list -->
									</div><!-- main-profile-overview -->
								</div>
							</div>
						</div>
						<div class="card mg-b-20">
							<div class="card-body">
								<div class="main-content-label tx-13 mg-b-25">
									Conatct
								</div>
								<div class="main-profile-contact-list">
									<div class="media">
										<div class="media-icon bg-primary-transparent text-primary">
											<i class="icon ion-md-phone-portrait"></i>
										</div>
										<div class="media-body">
											<span>Contact No:</span>
											<div>
											{{ $users->mobile_1 }}
											</div>
										</div>
									</div>
									<div class="media">
									<div class="media-icon bg-success-transparent text-success">
											<i class="icon ion-md-phone-portrait"></i>
										</div>
										<div class="media-body">
											<span>Phone No:</span>
											<div>
											{{ $users->mobile_2 }}
											</div>
										</div>
									</div>
									<div class="media">
										<div class="media-icon bg-info-transparent text-info">
											<i class="icon ion-md-locate"></i>
										</div>
										<div class="media-body">
											<span>Temporary Address</span>
											<div>
											{{ $users->temp_address }}
											</div>
										</div>
									</div>
								</div><!-- main-profile-contact-list -->
							</div>
						</div>
					</div>
					<!-- /Col -->

					<!-- Col -->
					<div class="col-lg-8">
						<div class="card">
							<div class="card-body">
								<div class="mb-4 main-content-label">Personal Information</div>
						
									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">First Name</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled value = "{{ $users->first_name ?? '' }}"  placeholder="User Name" value="Redashna">
											</div>
										</div>
									</div>

									{{--

									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Middle Name</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled value = "{{ $users->middle_name   ?? '' }}" placeholder="First Name" value="Redashna">
											</div>
										</div>
									</div>	

										--}}
									
									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">last Name</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled value = "{{ $users->last_name ?? '' }}"  placeholder="Last Name" value="Redashna">
											</div>
										</div>
									</div>
									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Father's Name</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled value="{{ $users->father_name  ?? ''}}" value="Redash">
											</div>
										</div>
									</div>
                                  
									{{--

									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Mother's Name</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled value="{{ $users->mother_name  ?? ''}}" value="Redash">
											</div>
										</div>
									</div>		


										--}}
                                    
									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Date Of Birth</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled  value ="{{ $users->date_of_birth }}"  placeholder="Designation" value="Web Designer">
											</div>
										</div>
									</div>
                                    <div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">CNIC</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled  value ="{{ $users->cnic }}"  placeholder="Designation" value="Web Designer">
											</div>
										</div>
									</div>
                                    <div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Gender</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled  value ="{{ $users->gender }}"  placeholder="Designation" value="Web Designer">
											</div>
										</div>
									</div>
                                    <div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Blood Group</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled  value ="{{ $users->blood_group }}"  placeholder="Designation" value="Web Designer">
											</div>
										</div>
									</div>
									<div class="mb-4 main-content-label">Contact Info</div>
									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Email<i></i></label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled value ="{{ $users->email }}"  placeholder="Email" value="">
											</div>
										</div>
									</div>
									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Mother Tongue</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control" disabled value="{{ $users->mother_tongue }}"  placeholder="Mohter Tongue" value="">
											</div>
										</div>
									</div>
								
									<div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Address</label>
											</div>
											<div class="col-md-9">
												<textarea class="form-control" name="example-textarea-input" disabled  rows="2"  placeholder="Address">{{ $users->address }}</textarea>
											</div>
										</div>
									</div>
                                    
								
                                    <div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">City</label>
											</div>
											<div class="col-md-9">
												<textarea class="form-control" name="example-textarea-input" disabled  rows="2"  placeholder="Address">{{ $users->city }}</textarea>
											</div>
										</div>
									</div>
                                    <div class="form-group ">
										<div class="row">
											<div class="col-md-3">
												<label class="form-label">Country</label>
											</div>
											<div class="col-md-9">
												<textarea class="form-control" name="example-textarea-input" disabled   rows="2"  placeholder="Address">{{ $users->country }}</textarea>
											</div>
										</div>
									</div>
									
						
								
								
							
									
								</form>
							</div>
							<div class="card-footer">
							<a href="{{ route('admin.documents.create') }}" class="btn btn-success">Upload Document</a>     
							<button data-target="#resignModal" data-toggle="modal" class="btn btn-xl btn-info" style = "margin-right:20px;">Want to Resign
                            </button>
							<form method="get" style = "float: left;margin-left: 100px;margin-right: 3px;" id="card-request" action="{{url('request-idcard')}}">
                            <input type="hidden" name="link_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                            <input type="hidden" name="link_type" value="2">

                            <button type="submit" onclick = 'return confirm("Are you sure you want to delete this entry?");' class="btn btn-xl btn-info">Request Id Card
                               </button>

                                <button type="button" class="btn btn-xl btn-primary" data-toggle="modal" data-target="#myModal">Id Card Status
                               </button>
                        </form>
							</div>
						</div>
					</div>
					<!-- /Col -->
				</div>
				<!-- row closed -->
			</div>
			<!-- Container closed -->
		</div>

		<div class="card mg-b-20">
							<div class="card-body">
								<div class="main-content-label tx-13 mg-b-25">
        <h3><b>Uploaded Document</b></h3>
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($documents) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>File</th>
                        <th>Time and Time</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($documents) > 0)
                        @foreach ($documents as $key => $document)
                            <tr data-entry-id="{{ $document->id }}">
                                <td>{{ $document->title }}</td>
                                <td>{{ $document->description }}</td>
                                 <td> <a href="{{asset($document->file)}}"> Document {{$key+1}} </a></td>
                                <td>{{ $document->created_at }}</td>
                                <td>
                                  

                                  {{--

                                  	<a href="{{ route('admin.documents.edit', [$document->id]) }}"
                                        class="btn btn-xs btn-info">@lang('global.app_edit')</a>

                                  	--}}  



                                    {!! Form::open([
                                    'style' => 'display: inline-block;',
                                    'method' => 'DELETE',
                                    'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                    'route' => ['admin.documents.destroy', $document->id],
                                    ]) !!}
                                    {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>







    <!-------------------------------------------------modal------------------------------------>

    <div class="modal fade" id="resignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Resign  Form</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row ">


                            <form method="POST" action="{{ url('admin/resign') }}">
                                {{ csrf_field() }}
<!--                                 <div class="form-group">
                                    <label for="exampleInputEmail1">Valid From:</label>
                                    <br>
                                    <input class="form-control" name="valid" id="datepicker" type="text">

                                </div> -->


                                <div class="form-group " style = "text-align:left;">
                                    <label for="exampleInputEmail1" style = "text-align:left;font-size:18px;font-weight:600;">Notice Period</label>
								</div>
                                
								<div class="row" style="margin-top: 5px;">
									<div class="col-md-4">
										<label for="exampleInputEmail1">From* : </label>
									</div>
									<div class="col-md-8">
										<input type="date" name="from" required="required" style="width: 100%;">
									</div>
								</div>

								<div class="row" style="margin-top: 5px;">
									<div class="col-md-4">
										<label for="exampleInputEmail1">To* : </label>
									</div>
									<div class="col-md-8">
										<input type="date" required="required" name="to" style="width: 100%;">
									</div>
								</div>


								<div class="row" style="margin-top: 5px;">
									<div class="col-md-4">
										<label for="exampleInputEmail1">Last Day of Work*:</label>
									</div>
									<div class="col-md-8">
										<input type="date" name="last_day_of_work" style = "width: 100%;" required>
									</div>
								</div>

								<div class="row" style="margin-top: 5px;">
									<div class="col-md-4">
										<label for="exampleInputEmail1">Class/Grade:</label>
									</div>
									<div class="col-md-8">
										<input type="text" name="class"  style = "    width: 100%;">
									</div>
								</div>

								<div class="row" style="margin-top: 5px;">
									<div class="col-md-4">
										<label for="exampleInputEmail1">Subject:</label>
									</div>
									<div class="col-md-8">
										<input type="text" name="subject"  style ="width: 100%;">
									</div>
								</div>

								<div class="row" style="margin-top: 5px;">
									<div class="col-md-4">
										<label for="exampleInputEmail1">Experience Letter:</label>
									</div>
									<div class="col-md-8">
										<label for="exampleInputEmail1">Yes</label>
                                    	<input type="radio" id="yes" name="experience_letter" checked value="Yes">
                                    
	                                    <label for="exampleInputEmail1">No</label>
	                                    <input type="radio" id="no" name="experience_letter" value="No">
									</div>
								</div>


									

                          

                          {{--


                          	<div class="row">
								<div class="form-group col-md-12" style = "float:left;" >
                                    <label for="exampleInputEmail1" style = "float:left;">Class/Grade:</label>
									<input type="text" name="class"  style = "    width: 78%;
    margin-left: 2px;
    float: left;">
									</div>
									<div class="form-group col-md-12" style = "float:left;" >
                                    <label for="exampleInputEmail1" style = "float:left;">Subject:</label>
									<input type="text" name="subject"  style = "    width: 78%;
    margin-left: 33px;
    float: left;">
									</div>
                                   
									<div class="form-group col-md-12" style = "float:left;" >
                                    <label for="exampleInputEmail1" style = "float:left;">Last Day of Work:</label>
									<input type="date" name="last_day_of_work" style = "    width: 72%;
    margin-left: 2px;
    float: left;">
									</div>
                                 
                                </div>


                                <div class="form-group " style = "text-align:left;">
                                    <label for="exampleInputEmail1" style = "text-align:left;font-weight:600;font-size:20px;">Want Experience Letter</label>
                                    <br>
                                    <label for="exampleInputEmail1">Yes</label>
                                    <input type="radio" id="yes" name="experience_letter" value="Yes">
                                    
                                    <label for="exampleInputEmail1">No</label>
                                    <input type="radio" id="no" name="experience_letter" value="No">

                                </div>

 


                          	--}}      
                                
                                


                                



                                
                                
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Reason* : </label><br>
                                    <textarea  class="form-control" style="width: 100%" name="reason" id="reason" cols="50" rows="5" required="required">






                                        </textarea>
                                </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>




<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Id Card Status</h4>
        </div>
        <div class="modal-body">


          <h4>Status:</h4>
          @foreach($users->user->IdCards as $status)
            <p>{{$status->status}}</p>
            @endforeach

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  











@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });

        $(function() {
            $("#datepicker").datepicker();
        });


        $(function() {
            var dateFormat = "mm/dd/yy",
                from = $("#from")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on("change", function() {
                    to.datepicker("option", "minDate", getDate(this));
                }),
                to = $("#to").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on("change", function() {
                    from.datepicker("option", "maxDate", getDate(this));
                });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }
        });
        
    </script>
@endsection
