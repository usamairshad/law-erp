
<div class="form-group col-md-12 @if($errors->has('heading')) has-error @endif" >
    {!! Form::label('heading', 'Heading', ['class' => 'control-label']) !!}
    {!! Form::text('heading',  old('heading'), ['class' => 'form-control']) !!}
    @if($errors->has('heading'))
        <span class="help-block">
            {{ $errors->first('heading') }}
        </span>
    @endif
</div>

<div class="form-group col-md-12 @if($errors->has('suggestion')) has-error @endif" >
    {!! Form::label('suggestion', 'Suggestion ', ['class' => 'control-label']) !!}
    {!! Form::textarea('suggestion',  old('suggestion'), ['class' => 'form-control']) !!}
    @if($errors->has('suggestion'))
        <span class="help-block">
            {{ $errors->first('suggestion') }}
        </span>
    @endif
</div>

<div class="form-group col-md-12 @if($errors->has('comments')) has-error @endif" >
    {!! Form::label('comments', 'Comments', ['class' => 'control-label']) !!}
    {!! Form::textarea('comments',  old('comments'), ['class' => 'form-control']) !!}
    @if($errors->has('comments'))
        <span class="help-block">
            {{ $errors->first('comments') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('ratings')) has-error @endif">
    {!! Form::label('ratings', 'Ratings ', ['class' => 'control-label']) !!}
    {!! Form::select('ratings', array('' => 'Select a Ratings') + Config::get('hrm.ratings_array'), old('ratings'), ['class' => 'form-control']) !!}
    @if($errors->has('ratings'))
        <span class="help-block">
            {{ $errors->first('ratings') }}
        </span>
    @endif
</div>
