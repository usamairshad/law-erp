<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" >
    {!! Form::label('name', 'Type Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name',  old('name'), ['id' => 'name' ,'class' => 'form-control']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>

