@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Document Types</h1>
    </section>
    {{csrf_field()}}
    <input type="hidden" name="name" value="abc">

@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('employee_document_types_create'))
                <a href="{{ route('hrm.employee_document_types.create') }}" class="btn btn-success pull-right">Add Employee Document Type </a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($EmployeeDocumentTypes) > 0 ? 'datatable' : '' }}" id="users-table" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Type Name</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/employee_document_types/list.js') }}" type="text/javascript"></script>
@endsection