@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update EOBI Notification {{$data->id}}</h3>

            <a href="{{ route('eobi_notification.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::model($data, [
    'method' => 'PATCH',
    'route' => ['eobi_notification.update',$data->id], 'id' => 'validation-form'
]) !!}
        <div class="box-body" style = "margin-top:40px;">
            @include('hrm.eobi.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn','style'=>'margin-top: 27px;']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/employees/create_modify.js') }}" type="text/javascript"></script>
@endsection