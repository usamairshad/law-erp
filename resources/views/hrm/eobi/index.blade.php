@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Add Eobi Notifications</h3>
            @if (Gate::check('erp_staff_create'))
                <a href="{{ route('eobi_notification.create') }}" style ="float:right;" class="btn btn-success pull-right">Add New Notification</a>
            @endif

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}">
                <thead>

                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Company Share</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($data) > 0)
                        @foreach ($data as $eobi)
                            <tr data-entry-id="{{ $eobi->id }}">
                                
                                <td>{{ $eobi->name}}</td>
                                <td>{{ $eobi->description }}</td>
                                <td>{{ $eobi->date }}</td>
                                <td>{{ $eobi->amount }}</td>
                                <td>{{ $eobi->company_share }}</td>
                                <td>    
                                    
                                    <a href="{{ route('eobi_notification.edit',$eobi->id ) }}" class="btn btn-primary">Edit</a>
                                    {!! Form::open([
                                    'method' => 'DELETE',
                                     'route' => ['eobi_notification.destroy', $eobi->id]
                                    ]) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}

                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


   




@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });


    </script>
@endsection
