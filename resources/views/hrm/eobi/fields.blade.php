<div class="form-group col-md-3 @if($errors->has('first_name')) has-error @endif" style = "float:left;">
    {!! Form::label('Name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control','required','placeholder'=>'Enter Name']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('description')) has-error @endif" style = "float:left;">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::text('description', old('description'), ['class' => 'form-control','required','placeholder'=>'Enter Description']) !!}
    @if($errors->has('description'))
        <span class="help-block">
            {{ $errors->first('description') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('date')) has-error @endif" style = "float:left;">
        {!! Form::label('date', 'Date*', ['class' => 'control-label']) !!}
        {!! Form::date('date', old('date'), ['class' => 'form-control' ,'required','placeholder'=>'Select Date']) !!}
        @if($errors->has('date'))
                <span class="help-block">
            {{ $errors->first('date') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('amount')) has-error @endif" style = "float:left;">
        {!! Form::label('amount', 'Amount*', ['class' => 'control-label']) !!}
        {!! Form::number('amount', old('amount'), ['class' => 'form-control','required', 'placeholder'=>'Enter Amount']) !!}
        @if($errors->has('amount'))
                <span class="help-block">
            {{ $errors->first('amount') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('company_share')) has-error @endif" style = "float:left;">
        {!! Form::label('company_share', 'Company Share*', ['class' => 'control-label']) !!}
        {!! Form::number('company_share', old('company_share'), ['class' => 'form-control','required', 'placeholder'=>'Enter Company Share']) !!}
        @if($errors->has('company_share'))
                <span class="help-block">
            {{ $errors->first('company_share') }}
        </span>
        @endif
</div>