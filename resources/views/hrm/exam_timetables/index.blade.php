@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Exam Timetable</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
             @if (Gate::check('erp_class_time_table_create'))
                <a href="{{ route('admin.exam_timetable.create') }}" class="btn btn-success pull-right">Add New Exam TimeTable</a>
            @endif

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($exam_timetables) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Time Table Name</th>
                        <th>Exam</th>
                        <th>Action</th>
                        
                    </tr>
                </thead>

                <tbody>
                    @if (count($exam_timetables) > 0)
                        @foreach ($exam_timetables as $exam_timetable)
                            <tr data-entry-id="{{ $exam_timetable->id }}">
                                 <td>{{ \App\Helpers\Helper::timetableIdToTitle($exam_timetable->time_table_id) }}</td>
                               <td>{{ \App\Helpers\Helper::examIdToName($exam_timetable->exam_id) }}</td>
                                <td>
                                    @if (Gate::check('erp_class_time_table_create'))
                                        {!! Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['admin.exam_timetable.destroy', $exam_timetable->id],
                                        ]) !!}
                                        {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger'])
                                        !!}
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
            
    </script>

@endsection
