@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Teacher Course Details</h3>
 
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Timetable Title</th>
                        <th>Branch Name</th> 
                        <th>Staff Name</th>
                        <th>Course</th>
                        <th>Rates</th>
                        <th>Lectures Per Month</th>
                        <th>Day</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($data) > 0)
                        @foreach ($data as $Employee)
                            <tr data-entry-id="{{ $Employee->id }}">
                                <td>{{ $Employee->title }}</td>
                                <td>{{ \App\Helpers\Helper::branchIdToName($Employee->branch_id) }}</td>
                                <td>{{ \App\Helpers\Helper::getStaffIDToFirstName($Employee->staff_id) }}</td>
                                 <td>{{ \App\Helpers\Helper::subjectIdToName($Employee->subject_id) }}</td>
                                <td>{{ $Employee->rates }}</td>
                                <td>{{ $Employee->days_in_month }}</td>
                                <td>{{ $Employee->day }}</td>
                                <td>{{ $Employee->start_time }}</td>
                                <td>{{ $Employee->end_time }}</td>
                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>




@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
