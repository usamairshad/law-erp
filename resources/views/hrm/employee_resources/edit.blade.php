@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Resources</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Employee Resources</h3>
            <a href="{{ route('hrm.employee_resources.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($EmployeeResources, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.employee_resources.update', $EmployeeResources->id]]) !!}
        <div class="box-body">
            @include('hrm.employee_resources.fields')
        </div>
        <!-- /.box-body -->
        {!! Form::hidden('id',  $EmployeeResources->id  , ['id' => 'id'] ) !!}

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/employee_resources/create_modify.js') }}" type="text/javascript"></script>
@endsection