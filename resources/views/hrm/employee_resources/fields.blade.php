<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif">
    {!! Form::label('user_id', 'Employee Name*', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $Employees, old('user_id'), ['id' => 'user_id', 'class' => 'form-control select2','required']) !!}
    <span id="user_id_handler"></span>
    @if($errors->has('user_id'))
        <span class="help-block">
                        {{ $errors->first('user_id') }}
                </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('product_id')) has-error @endif">
    {!! Form::label('product_id', 'Product *', ['class' => 'control-label']) !!}
    {!! Form::select('product_id', $Products, old('product_id'), [ 'id'=>'product_id', 'class' => 'form-control select2', 'required']) !!}
    @if($errors->has('product_id'))
        <span class="help-block">
            {{ $errors->first('product_id') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('serial_no')) has-error @endif" >
    {!! Form::label('serial_no', 'Serial no', ['class' => 'control-label']) !!}
    {!! Form::text('serial_no',  old('serial_no'), ['class' => 'form-control']) !!}
    @if($errors->has('serial_no'))
        <span class="help-block">
            {{ $errors->first('serial_no') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('grant_date')) has-error @endif">
    {!! Form::label('grant_date', 'Grant Date', ['class' => 'control-label']) !!}
    {!! Form::text('grant_date', old('grant_date'), ['class' => 'form-control datepicker']) !!}
    @if($errors->has('grant_date'))
        <span class="help-block">
            {{ $errors->first('grant_date') }}
        </span>
    @endif
</div>



<div class="form-group col-md-3 @if($errors->has('return_date')) has-error @endif">
    {!! Form::label('return_date', 'Return Date', ['class' => 'control-label']) !!}
    {!! Form::text('return_date', old('return_date'), ['class' => 'form-control datepicker']) !!}
    @if($errors->has('return_date'))
        <span class="help-block">
            {{ $errors->first('return_date') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('condition')) has-error @endif">
    {!! Form::label('condition', 'Status *', ['class' => 'control-label']) !!}
    {!! Form::select('condition', array('' => 'Select a Status') + Config::get('hrm.resource_status_array'), old('condition'), ['class' => 'form-control']) !!}
    @if($errors->has('condition'))
        <span class="help-block">
            {{ $errors->first('condition') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('worth')) has-error @endif" >
    {!! Form::label('worth', 'Worth', ['class' => 'control-label']) !!}
    {!! Form::number('worth',  old('worth'), ['class' => 'form-control' ,'maxlength'=>'11']) !!}
    @if($errors->has('worth'))
        <span class="help-block">
            {{ $errors->first('worth') }}
        </span>
    @endif
</div>
<div class="form-group col-md-12 @if($errors->has('remarks')) has-error @endif">
    {!! Form::label('remarks', 'Remarks', ['class' => 'control-label']) !!}
    {!! Form::textarea('remarks', old('remarks'), ['size' => '10x5','class' => 'form-control' ,'maxlength'=>'200']) !!}
    @if($errors->has('remarks'))
        <span class="help-block">
            {{ $errors->first('remarks') }}
        </span>
    @endif
</div>
