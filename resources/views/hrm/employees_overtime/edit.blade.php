@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employees Overtime</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Employee Overtime</h3>
            <a href="{{ route('hrm.employees_overtime.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($EmployeeOvertime, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.employees_overtime.update', $EmployeeOvertime->id]]) !!}
        <div class="box-body">
            @include('hrm.employees_overtime.fields')
        </div>
        <!-- /.box-body -->
        {!! Form::hidden('id',  $EmployeeOvertime->id  , ['id' => 'id'] ) !!}

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/employees_overtime/create_modify.js') }}" type="text/javascript"></script>
@endsection