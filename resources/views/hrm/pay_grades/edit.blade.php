@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Pay Grades</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Pay Grade</h3>
            <a href="{{ route('hrm.pay_grades.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($PayGrade, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.pay_grades.update', $PayGrade->id]]) !!}
        <div class="box-body">
            @include('hrm.pay_grades.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/pay_grades/create_modify.js') }}" type="text/javascript"></script>
@endsection