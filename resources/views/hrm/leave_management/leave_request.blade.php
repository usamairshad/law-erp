@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Leave Request Approval</h1>
    </section>
@stop


@section('content')
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">List</h3>
              </div>
        </div>
        
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($leaves) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Staff Reg No</th>
                        <th>Staff Name</th>
                        <th>Course</th>
                        <th>Leave Date</th>   
                        <th>Leave Type</th> 
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>



                <tbody>
                    @if (count($leaves) > 0)
                        @foreach ($leaves as $leave)
                       <tr>
                                <td>{{ \App\Helpers\Helper::getStaffIDToRegNo($leave->staff_id) }}</td>
                                <td>{{ \App\Helpers\Helper::getStaffIDToFirstName($leave->staff_id) }}</td>
                                <td>{{ \App\Helpers\Helper::subjectIdToName($leave->course_id) }}</td>
                                <td>{{ $leave->leave_date }}</td>
                               
                
                                  
                                <td>



                                    {{ $leave->leave_type }}</td>
                                    
                               
                                <td>

                                    {{ $leave->status}}</td>
                                <td>

                                    @if($leave->status == 'Pending')
                                        <a href="{{ route('admin.leave_request.approve', [$leave->id]) }}"
                                                class="btn btn-xs btn-success">Allowed</a>
                                            <a href="{{ route('admin.leave_request.unapprove', [$leave->id]) }}"
                                                class="btn btn-xs btn-info">Not Allowed</a>
                                    @elseif($leave->status == 'Allowed')
                                    <p style="color:green"><b> Allowed </b> </p>
                                    @elseif($leave->status == 'Not Allowed')
                                     <p style="color:blue"><b> Not Allowed </b> </p>
                                    @endif
                                </td>

                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>













@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable(
                {
    "order": ['status','desc']
})
        });

        $(".warning").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#warning_user_input').val($user_id);

        });


        $(".terminate").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#terminate_user_input').val($user_id);

        });


        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });


        $(".transferBranchButton").on("click", function() {

            $user_id = $(this).data('user-id');
            $branch_name = $(this).data('branch-name');
            $name = $(this).data('name');
            $status = $(this).data('status');



            console.log($user_id, $branch_name, $name, $status);



            $('#transfer_staff_id').val();
            $('#branch_name_transfer').text('');
            $('#staff_name_transfer').text('');
            $('#status_name_transfer').text('');



            $('#transfer_staff_id').val($user_id);
            $('#branch_name_transfer').text($branch_name);
            $('#staff_name_transfer').text($name);
            $('#status_name_transfer').text($status);

            $('#action_name_transfer').attr("href", `{{ url('admin/delete/staff/branch') }}/${$user_id}`);



        });

        $('.promotionDemotion').on("click", function() {

            $name = $(this).data('user-name');
            $user_role_id = $(this).data('user-role-id');
            $user_id = $(this).data('user-id');
            $branch_id = $(this).data('branch-id');

            $('#user_name_promotion').val($name);

            $('#promotion_user_id').val($user_id);
            $('#staff_user_id').val($user_id);
            $("#active_teacher_id").val($user_id);
            console.log($user_id);
            $(".roles").select2().val($user_role_id).trigger('change');


            loadBorad($branch_id);

        });

        function loadBorad($branch_id) {
            $.ajax({
                url: `{{ url('admin/getBoard/') }}/${$branch_id}`,

                success: function(response) {
                    $html = '<option value="null">Select board</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.board_id}>${res.board.name}</option>`;

                    });

                    $("#boards").data("branch-id", $branch_id);
                    $("#boards").html($html);
                }
            });
        }
        ////////////////on board change get program/////////////// 
        $("#boards").on("change", function() {

            $board_id = $(this).find(":selected").val();
            $branch_id = $(this).data('branch-id');

            loadProgram($branch_id, $board_id);
        });

        function loadProgram($branch, $board) {
            $.ajax({
                url: `{{ url('admin/getProgram/') }}/${$branch}/${$board}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select program</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.program_id}>${res.program.name}</option>`;

                    });

                    $("#programs").data('branch-id', $branch);
                    $("#programs").data('board-id', $board);
                    $("#programs").html($html);
                }
            });
        }

        ////////////////on program change get class/////////////// 
        $("#programs").on("change", function() {

            $board_id = $(this).data("board-id");
            $branch_id = $(this).data('branch-id');
            $program_id = $(this).find(':selected').val();

            console.log($board_id, $branch_id, $program_id);

            loadClasses($branch_id, $board_id, $program_id);
        });

        function loadClasses($branch, $board, $program) {
            $.ajax({
                url: `{{ url('admin/getClasses/') }}/${$branch}/${$board}/${$program}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select class</option>';

                    response.forEach((res) => {

                        $html +=
                            `<option data-active-s-id=${res.id}  value=${res.get_class.id}>${res.get_class.name}</option>`;

                    });

                    $("#classes").html($html);
                }
            });
        }
        $("#classes").change(function() {
            $activeSessionID= $(this).find(":selected").data("active-s-id");
            $("#active_session_id").val($activeSessionID);
            console.log($activeSessionID);
        });


        $("#classes").change(function() {

            $activeSessionID= $(this).find(":selected").data("active-s-id");

            $("#active_session_id").val($activeSessionID);
            console.log($activeSessionID);
        });

    </script>
@endsection
