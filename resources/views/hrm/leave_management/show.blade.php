@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Profile</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
             <a href="{{ route('admin.leave_management.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">


            <table class="table table-bordered table-striped">
                <tr>
                    <th>Teacher ID</th>
                    <td>{{ $leaves->teacher_id ?? '' }}</td>
                    <th>Subject ID</th>
                    <td>{{ $leaves->subject_id ?? '' }}</td>
                </tr>
                <tr>
                    <th>Class ID</th>
                    <td>{{ $leaves->class_id  ?? ''}}</td>
                    <th>Board</th>
                    <td>{{ $leaves->board ?? '' }}</td>
                </tr>
                <tr>
                    <th>Program</th>
                    <td>{{ $leaves->program }}</td>
                    <th>Start Date</th>
                    <td>{{ $leaves->start_date }}</td>
                </tr>
                <tr>
                    <th>End Date</th>
                    <td>{{ $leaves->end_date }}</td>
                    <th>leave Type</th>
                    <td>{{ $leaves->leave_type }}</td>
                </tr>
               

               
            </table>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });
    </script>
@endsection
