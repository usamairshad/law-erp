



 


<div class="form-group col-md-3 @if($errors->has('teacher_id')) has-error @endif">
    {!! Form::label('techer_id', 'Teacher ID*', ['class' => 'control-label']) !!}
    <input type="text" name="teacher_id" value="{{$leaves->teacher_id}}" placeholder="Enter Teacher ID" class="form-control" required>
</div>

<div class="form-group col-md-3 @if($errors->has('subject_id')) has-error @endif">
    {!! Form::label('Subject_id', 'Subject ID', ['class' => 'control-label']) !!}
    <input type="text" name="subject_id" value="{{$leaves->subject_id}}" class="form-control" >
</div>
<div class="form-group col-md-3 @if($errors->has('class_id')) has-error @endif">
    {!! Form::label('Class_id', 'Class Id', ['class' => 'control-label']) !!}
    <input type="text" name="class_id" value="{{$leaves->class_id}}" class="form-control" >
</div>
<div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
    {!! Form::label('Board', 'Board', ['class' => 'control-label']) !!}
    <input type="text" name="board" class="form-control" value="{{$leaves->board}}" >
</div>
<div class="form-group col-md-3 @if($errors->has('program')) has-error @endif">
    {!! Form::label('Program', 'Program', ['class' => 'control-label']) !!}
    <input type="text" name="program" class="form-control" value="{{$leaves->program}}" >
</div>

<div class="form-group col-md-3 @if($errors->has('start_date')) has-error @endif">
    {!! Form::label('start_date', 'Start Date*', ['class' => 'control-label']) !!}
    <input type="date" name="start_date" class="form-control" value="{{$leaves->start_date}}" required >
</div>

<div class="form-group col-md-3 @if($errors->has('end_date')) has-error @endif">
    {!! Form::label('Class_date', 'Class Time*', ['class' => 'control-label']) !!}
    <input type="date" name="end_date" class="form-control" value="{{$leaves->end_date}}" required>
</div>
<div class="form-group col-md-3 @if($errors->has('leave_type')) has-error @endif">
    {!! Form::label('leave_type', 'Leave Type*', ['class' => 'control-label']) !!}
    <select name="leave_type" id="leave_type"  value="{{$leaves->attendance_type}}" class="form-control" required >
        
            <option value="Full Day"  @if($leaves->leave_type == 'Full Day') selected @endif>Full Day</option>
            <option value="Hald Day" @if($leaves->leave_type == 'Half Day') selected @endif   >Half Day</option>
            <option value="lecture" @if($leaves->leave_type == 'Lecture') selected @endif   >Lecture</option>
            <option value="Half Lecture" @if($leaves->leave_type == 'Half Lecture') selected @endif   >Half Lecture</option>
       
    </select>

</div>









<script type="text/javascript">
$("#seeAnotherField").change(function() {
  if ($(this).val() != "Teacher") {
    $('#otherFieldDiv').show();
    $('#otherField').attr('required', '');
    $('#otherField').attr('data-error', 'This field is required.');
  } else {
    $('#otherFieldDiv').hide();
    $('#otherField').removeAttr('required');
    $('#otherField').removeAttr('data-error');
  }
});
$("#seeAnotherField").trigger("change");

</script>