@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Gratuity</h1>
    </section>
   {{csrf_field()}}
    <input type="hidden" name="name" value="abc">

@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('gratuity_create'))
                <a href="{{ route('hrm.gratuity.create') }}" class="btn btn-success pull-right">Calculate Gratuity</a>
            @endif
        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Gratuity) > 0 ? 'datatable' : '' }}" id="users-table" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Employee</th>
                    <th>Amount</th>
                    <th>Years</th>
                    <th>Basic</th>
                    <th>Paid On</th>

                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        {{--$(document).ready(function(){--}}

            {{--$('#users-table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{!! route('hrm.gratuity.datatables') !!}',--}}
                {{--columns: [--}}
                    {{--{ data: 'id', name: 'id' },--}}
                    {{--{ data: 'user_name', name: 'user_name' },--}}
                    {{--{ data: 'branch', name: 'branch' },--}}
                    {{--{ data: 'checkin_time', name: 'checkin_time' },--}}
                    {{--{ data: 'checkout_time', name: 'checkout_time' },--}}

                    {{--{ data: 'created_at', name: 'created_at' },--}}
                    {{--{ data: 'action', name: 'action' , orderable: false, searchable: false},--}}

                {{--]--}}
            {{--});--}}
        {{--});--}}
    </script>
    <script src="{{ url('public/js/hrm/gratuity/list.js') }}" type="text/javascript"></script>
@endsection