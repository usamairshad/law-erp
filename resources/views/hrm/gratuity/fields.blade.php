<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif">
    {!! Form::label('user_id', 'Employee Name*', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $Employees, old('user_id'), ['id' => 'user_id', 'class' => 'form-control select2']) !!}
    <span id="user_id_handler"></span>
    @if($errors->has('user_id'))
        <span class="help-block">
                        {{ $errors->first('user_id') }}
                </span>
    @endif
</div>

<div class="form-group col-md-9">

    <button id="search_button" onclick="FormControls.fetchEmployeeRecord();" type="button" style="margin-top: 25px; border-radius: 10px;" class="btn  btn-md btn-flat btn-primary"><b>&nbsp;Search </b> </button>

</div>


<div class="form-group col-md-3 @if($errors->has('perm_start_date')) has-error @endif">
    {!! Form::label('perm_start_date', 'Permanent Start Date', ['class' => 'control-label']) !!}
    {!! Form::text('perm_start_date', old('perm_start_date'), ['class' => 'form-control', 'id' => 'perm_start_date','readonly']) !!}
    @if($errors->has('perm_start_date'))
        <span class="help-block">
            {{ $errors->first('perm_start_date') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('gratuity_amount')) has-error @endif" >
    {!! Form::label('gratuity_amount', 'Amount', ['class' => 'control-label']) !!}
    {!! Form::number('gratuity_amount',  old('gratuity_amount'), ['class' => 'form-control' ,'maxlength'=>'11', 'id'=>'amount','readonly']) !!}
    @if($errors->has('gratuity_amount'))
        <span class="help-block">
            {{ $errors->first('gratuity_amount') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('years')) has-error @endif" >
    {!! Form::label('years', 'Years', ['class' => 'control-label']) !!}
    {!! Form::number('years',  old('years'), ['class' => 'form-control' ,'maxlength'=>'11' ,'id'=>'years','readonly']) !!}
    @if($errors->has('years'))
        <span class="help-block">
            {{ $errors->first('years') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('basic')) has-error @endif" >
    {!! Form::label('basic', 'Basic', ['class' => 'control-label']) !!}
    {!! Form::number('basic',  old('basic'), ['class' => 'form-control' ,'maxlength'=>'11', 'id'=>'basic','readonly']) !!}
    @if($errors->has('basic'))
        <span class="help-block">
            {{ $errors->first('basic') }}
        </span>
    @endif
</div>

<div class="form-group col-md-12 @if($errors->has('remarks')) has-error @endif">
    {!! Form::label('remarks', 'Remarks', ['class' => 'control-label']) !!}
    {!! Form::textarea('remarks', old('remarks'), ['size' => '10x5','class' => 'form-control' ,'maxlength'=>'200']) !!}
    @if($errors->has('remarks'))
        <span class="help-block">
            {{ $errors->first('remarks') }}
        </span>
    @endif
</div>
