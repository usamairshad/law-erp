@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Timetable</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
                @if (Gate::check('erp_time_table_category_create'))
                <a href="{{ route('admin.timetable.create') }}" class="btn btn-success pull-right">Add New TimeTable</a>
                @endif

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($timetables) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Branch Name</th>
                        <th>Title</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                    {{--<th>Actions</th> --}}
                    </tr>
                </thead>

                <tbody>
                    @if (count($timetables) > 0)
                        @foreach ($timetables as $timetable)
                            <tr data-entry-id="{{ $timetable->id }}">
                                <td>{{ \App\Helpers\Helper::branchIdToName($timetable->branch_id) }}</td>
                                <td>{{ $timetable->title }}</td>
                                <td>{{ $timetable->start_time }}</td>
                                <td>{{ $timetable->end_time }}</td>
                              {{-- <td>
                                    @if (Gate::check('erp_staff_destroy'))
                                        {!! Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['admin.timetable.destroy', $timetable->id],
                                        ]) !!}
                                        {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger'])
                                        !!}
                                        {!! Form::close() !!}
                                    @endif
                                </td> --}}  
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>


@stop

@section('javascript')

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
