@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Report</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Leave Performance Report</h3>
            {{-- <a href="{{ route('support.complaints.index') }}" class="btn btn-success pull-right">Back</a> --}}
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['hrm.leave_performance.post'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('hrm.hrm_reports.leaveperformance_report.index_fields')
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {!! Form::submit(trans('Load report'), ['class' => 'btn btn-primary pull-right']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
<!-- date-range-picker -->
{{-- <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script> --}}
<script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/js/hrm/report/report.js') }}" type="text/javascript"></script>
@endsection

