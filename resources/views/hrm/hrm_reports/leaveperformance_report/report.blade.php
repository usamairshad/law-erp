@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Report</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Custom Report</h3>
            {{-- <a href="{{ route('support.complaints.index') }}" class="btn btn-success pull-right">Back</a> --}}
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['hrm.leave_performance.post'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('hrm.hrm_reports.leaveperformance_report.report_fields')
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="load_report" class="btn btn-primary pull-right">Load Report</button>
                   
        </div>
        {!! Form::close() !!}
    </div>

    @include('hrm.hrm_reports.leaveperformance_report.reporttable')
    
        {!! Form::open(['method' => 'POST', 'target' => '_blank', 'route' => ['hrm.leave_performance.report.pdf'], 'id' => 'form-pdf']) !!}
        {!! Form::hidden('employee', null, ['id' => 'employee']) !!}
        {!! Form::hidden('status', null, ['id' => 'status']) !!}
        {!! Form::hidden('leave_type_id', null, ['id' => 'leave_type_id']) !!}
        {!! Form::close() !!} 
    
@stop

@section('javascript')
<!-- date-range-picker -->
<script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
<script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{ url('public/js/hrm/report/report.js') }}" type="text/javascript"></script>
@endsection

