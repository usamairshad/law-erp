
<style>

   
    .table{
        width: 100%;
        float: left;
        margin-top: -50px;
    }
    .footer { width:100%; position: fixed; bottom: 0px;}

    .pagenum:after {
        content: counter(page);  right: 0px }
    tr{
        page-break-before: always;
    }
    .footer{
        border-top: 1px solid #000;
        padding-top: 3px;
    }
    .terms{
        width: 100%;
    }
    .terms p{
        margin: 0;
        padding:0;
    }
 
   table th{
        font-size: 14px;
       font-family: 'Source Sans Pro', sans-serif;
       font-weight: 600;
    }
   table{
       font-family: 'Source Sans Pro', sans-serif;
       font-weight: 400;
   }
    tr td , th{
        border: 1px solid;
        font-size: 13px;
    }

</style>



<!DOCTYPE>
<!DOCTYPE html>
<html>
<head>
    <title>Leave Performance Report</title>
</head>
<body>
<div class="po-header">
    {{-- <img src="{{url('public/image/Screenshot_2018-10-25.png')}}"> --}}
    <h2 style="font-family: 'Source Sans Pro', sans-serif; font-weight: 600; text-align: center"><span>Leave Performance Report</span></h2>
</div>

<div class="col-md-12" style="position:relative;">

        <table style="border:1px solid #000000;  margin:0 0 20px 0;line-height: 1 " width="100%" cellspacing="0" >
                <thead>
                        <tr>
                            <th>Employee Name</th>
                            <td>{{App\User::emp_name($employee_id)}}</td>
                        </tr>
                        <tr>
                            <th>Report Date</th>
                            <td>{{$mytime = Carbon\Carbon::now()}}</td>
                        </tr>
                    </thead>
            </table>
        </div>

<div class="col-md-12" style="position:relative;">

    <table style="border:1px solid #000000;  margin:0 0 20px 0;text-align: center;line-height: 1 " width="100%" cellspacing="0" >
            <thead>
                <tr>
                    <th>Leave Type</th>
                    <th>Rejected</th>
                    <th>Cancelled</th>
                    <th>Pending_Approval</th>
                    <th>Schedule</th>
                    <th>Taken</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Sick</td>
                    <td>{{$sick_rejected}}</td>
                    <td>{{$sick_cancelled}}</td>
                    <td>{{$sick_pending_approval}}</td>
                    <td>{{$sick_schedule}}</td>
                    <td>{{$sick_taken}}</td>
                </tr>
                <tr>
                    <td>Vacation</td>
                    <td>{{$vacation_rejected}}</td>
                    <td>{{$vacation_cancelled}}</td>
                    <td>{{$vacation_pending_approval}}</td>
                    <td>{{$vacation_schedule}}</td>
                    <td>{{$vacation_taken}}</td>
                </tr>
                <tr>
                    <td>Casual</td>
                    <td>{{$casual_rejected}}</td>
                    <td>{{$casual_cancelled}}</td>
                    <td>{{$casual_pending_approval}}</td>
                    <td>{{$casual_schedule}}</td>
                    <td>{{$casual_taken}}</td>
                </tr>
            </tbody>
        </table>
    </div>
<div class="col-md-12" style="position:relative;">

    <table style="border:1px solid #000000;  margin:0 0 20px 0;text-align: center;line-height: 1 " width="100%" cellspacing="0" >
        <thead>
                <tr>
                    <th>Sr #</th>
                    <th>Leave Type</th>
                    <th>Leave Status</th>
                    <th>Leave Date</th>
                </tr>
            </thead>
            <tbody>
                    @php
                    $i = 1;
                @endphp
                @foreach ($leave as $leaves)
                    <tr>
                        <td>{{ $i++}}</td>
                        <td>{{ $leaves->leave_type['name'] }}</td>
                        <td>{{ $leaves->leave_status['name'] }}</td>
                        <td>{{ $leaves->leave_date }}</td>
                    </tr>
                @endforeach
            </tbody>
    </table>
</div>
<div class="footer">  <span class="pagenum" style="float: right;"></span></div>