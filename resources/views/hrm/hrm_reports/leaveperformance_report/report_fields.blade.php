
    <div class="form-group col-md-3 @if($errors->has('employee')) has-error @endif">
            {!! Form::label('employee', 'Employee *', ['class' => 'control-label']) !!}
            {!! Form::select('employee', $Employees, old('employee'), [ 'id' => 'employee' , 'class' => 'form-control select2', 'required' => 'required']) !!}
            @if($errors->has('employee'))
                    <span class="help-block">
                {{ $errors->first('employee') }}
            </span>
            @endif
    </div>
    <div class="form-group col-md-3 @if($errors->has('status')) has-error @endif">
                {!! Form::label('status', 'Leave Status', ['class' => 'control-label']) !!}
                {!! Form::select('status', $Leave_status, old('status'), [ 'id' => 'status' , 'class' => 'form-control select2']) !!}
                <span id="status"></span>
                @if($errors->has('status'))
                        <span class="help-block">
                                {{ $errors->first('status') }}
                        </span>
                @endif
        </div>

        <div class="form-group col-md-3 @if($errors->has('leave_type_id')) has-error @endif">
                        {!! Form::label('leave_type_id', 'Leave Type*', ['class' => 'control-label']) !!}
                        {!! Form::select('leave_type_id', $LeaveTypes, old('leave_type_id'), ['onchange' => 'FormControls.loadEmployeeEntitlements();', 'id' => 'leave_type_id', 'class' => 'form-control']) !!}
                        <span id="leave_type_id_handler"></span>
                        @if($errors->has('leave_type_id'))
                                <span class="help-block">
                                        {{ $errors->first('leave_type_id') }}
                                </span>
                        @endif
                </div>
    
    
    
    
    