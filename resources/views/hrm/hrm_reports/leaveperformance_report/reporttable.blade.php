<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        <button  target="_blank" class="btn btn-warning pull-right" id="pdf"><i class="fa fa-file-excel-o"></i>&nbsp;PDF</button>
        {{-- <button class="btn btn-success pull-right" id="excel"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</button> --}}
    </div>
    <!-- /.box-header -->
    <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped " id="complaints-table">
                    <thead>
                        <tr>
                            <th>Employee Name</th>
                            <td>{{App\User::emp_name($employee_id)}}</td>
                        </tr>
                        <tr>
                            <th>Report Date</th>
                            <td>{{date('d-m-Y h:m a', strtotime($mytime = Carbon\Carbon::now()))}}</td>
                        </tr>
                    </thead>
                   
                </table>
            <table class="table table-bordered table-striped " id="complaints-table">
                <thead>
                    <tr>
                        <th>Leave Type</th>
                        <th>Rejected</th>
                        <th>Cancelled</th>
                        <th>Pending_Approval</th>
                        <th>Schedule</th>
                        <th>Taken</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Sick</td>
                        <td>{{$sick_rejected}}</td>
                        <td>{{$sick_cancelled}}</td>
                        <td>{{$sick_pending_approval}}</td>
                        <td>{{$sick_schedule}}</td>
                        <td>{{$sick_taken}}</td>
                    </tr>
                    <tr>
                        <td>Vacation</td>
                        <td>{{$vacation_rejected}}</td>
                        <td>{{$vacation_cancelled}}</td>
                        <td>{{$vacation_pending_approval}}</td>
                        <td>{{$vacation_schedule}}</td>
                        <td>{{$vacation_taken}}</td>
                    </tr>
                    <tr>
                        <td>Casual</td>
                        <td>{{$casual_rejected}}</td>
                        <td>{{$casual_cancelled}}</td>
                        <td>{{$casual_pending_approval}}</td>
                        <td>{{$casual_schedule}}</td>
                        <td>{{$casual_taken}}</td>
                    </tr>
                </tbody>
            </table>


        <table class="table table-bordered table-striped " id="complaints-table">
            <thead>
                <tr>
                    <th>Sr #</th>
                    <th>Leave Type</th>
                    <th>Leave Status</th>
                    <th>Leave Date</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($leave as $leaves)
                    <tr>
                        <td>{{ $i++}}</td>
                        <td>{{ $leaves->leave_type['name'] }}</td>
                        <td>{{ $leaves->leave_status['name'] }}</td>
                        <td>{{date('d-m-Y', strtotime( $leaves->leave_date)) }}</td>
                    </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>