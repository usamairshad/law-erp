@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Report</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Custom Report</h3>
            {{-- <a href="{{ route('support.complaints.index') }}" class="btn btn-success pull-right">Back</a> --}}
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['hrm.attendance.post'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('hrm.hrm_reports.attendance_reports.report_fields')
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="load_report" class="btn btn-primary pull-right">Load Report</button>
                   
        </div>
        {!! Form::close() !!}
    </div>

    
    
    {{-- {!! Form::open(['method' => 'POST', 'target' => '_blank', 'route' => ['support.report.excell'], 'id' => 'form-report']) !!}
        {!! Form::hidden('sdate', null, ['id' => 'sdate']) !!}
        {!! Form::hidden('edate', null, ['id' => 'edate']) !!}
        {!! Form::hidden('databank_id', null, ['id' => 'databank_id']) !!}
        {!! Form::hidden('assigned_to', null, ['id' => 'assigned_to']) !!}
        {!! Form::hidden('product_id', null, ['id' => 'product_id']) !!}
        {!! Form::hidden('product_type', null, ['id' => 'product_type']) !!}
        {!! Form::hidden('category', null, ['id' => 'category']) !!}
        {!! Form::hidden('priority', null, ['id' => 'priority']) !!}
        {!! Form::hidden('state', null, ['id' => 'state']) !!}
        {!! Form::hidden('complaint_status', null, ['id' => 'complaint_status']) !!}
        {!! Form::close() !!} --}}

        {{-- {!! Form::open(['method' => 'POST', 'target' => '_blank', 'route' => ['hrm.attendance_report.pdf'], 'id' => 'form-pdf']) !!}
        {!! Form::hidden('sdate', null, ['id' => 'sdate']) !!}
        {!! Form::hidden('edate', null, ['id' => 'edate']) !!}
        {!! Form::hidden('employee', null, ['id' => 'databank_id']) !!}
        {!! Form::hidden('status', null, ['id' => 'assigned_to']) !!}
        {!! Form::close() !!} --}}
    @include('hrm.hrm_reports.attendance_reports.reporttable')
    
@stop

@section('javascript')
<!-- date-range-picker -->
<script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
<script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{ url('public/js/hrm/report/report.js') }}" type="text/javascript"></script>
@endsection

