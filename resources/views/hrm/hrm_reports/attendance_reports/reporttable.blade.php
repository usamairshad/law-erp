
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        {{-- <button  target="_blank" class="btn btn-warning pull-right" id="pdf"><i class="fa fa-file-excel-o"></i>&nbsp;PDF</button>
        <button class="btn btn-success pull-right" id="excel"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</button> --}}
    </div>
    <!-- /.box-header -->

    
    <div class="panel-body pad table-responsive">
        <table class="table table-bordered table-striped " id="complaints-table">
            <thead>
                <tr>
                    <th>Employee</th>
                    <th>IN Requests</th>
                    <th>OUT Requests</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {{-- @php
                    dd($attendance);
                @endphp --}}
            
                    <tr>
                        <td>{{ App\Models\HRM\Employees::engineerName($data['employee']) }}</td>
                        <td>{{  $attendance1}}</td>
                        <td>{{ $attendance2 }}</td>
                        <td>
                        <a href="{{route('hrm.attendance.report.detail',$data['employee'] )}}" class=""><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </td>
                    </tr>

                    {{-- <tr>
                            <td>{{ $data['id']->id }}</td>
                            <td>{{ $data->employee['first_name'] }}</td>
                            <td>{{ Config::get('hrm.attendance_status_array.'. $data['type']) }}</td>
                            <td>{{ $attendance[$key]->date_time }}</td>
                        </tr> --}}
            </tbody>

        </table>
    </div>
</div>