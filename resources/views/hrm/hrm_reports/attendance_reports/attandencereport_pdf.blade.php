
<style>

   
    .table{
        width: 100%;
        float: left;
        margin-top: -50px;
    }
    .footer { width:100%; position: fixed; bottom: 0px;}

    .pagenum:after {
        content: counter(page);  right: 0px }
    tr{
        page-break-before: always;
    }
    .footer{
        border-top: 1px solid #000;
        padding-top: 3px;
    }
    .terms{
        width: 100%;
    }
    .terms p{
        margin: 0;
        padding:0;
    }
 
   table th{
        font-size: 14px;
       font-family: 'Source Sans Pro', sans-serif;
       font-weight: 600;
    }
   table{
       font-family: 'Source Sans Pro', sans-serif;
       font-weight: 400;
   }
    tr td , th{
        border: 1px solid;
        font-size: 13px;
    }

</style>



<!DOCTYPE>
<!DOCTYPE html>
<html>
<head>
    <title>Complaints</title>
</head>
<body>
<div class="po-header">
    {{-- <img src="{{url('public/image/Screenshot_2018-10-25.png')}}"> --}}
    <h2 style="font-family: 'Source Sans Pro', sans-serif; font-weight: 600; text-align: center"><span>Complaints</span></h2>
</div>


<div class="col-md-12" style="position:relative;">

    <table style="border:1px solid #000000;  margin:0 0 20px 0;text-align: center;line-height: 1 " width="100%" cellspacing="0" >
            <thead>
                    <tr>
                        <th>ID</th>
                        <th>Employee</th>
                        <th>Type</th>
                        <th>Date Time</th>
                      
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detail as $details)
                        <tr>
                            <td>{{ $details->id }}</td>
                            <td>{{ $details->employee['first_name'] }}</td>
                            <td>{{ Config::get('hrm.attendance_status_array.'. $details->type) }}</td>
                            <td>{{ $details->date_time }}</td>
                        </tr>
                     @endforeach       
                </tbody>

    </table>
</div>
<div class="footer">  <span class="pagenum" style="float: right;"></span></div>