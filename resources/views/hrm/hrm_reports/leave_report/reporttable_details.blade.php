@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Report</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Custom Report</h3>
            {{-- <a href="{{ route('support.complaints.index') }}" class="btn btn-success pull-right">Back</a> --}}
        <a href="{{route('hrm.leave_report.detail.pdf',$id)}}" target="_blank" class="btn btn-warning pull-right" id="pdf"><i class="fa fa-file-excel-o"></i>&nbsp;PDF</a>
        </div>
    <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped " id="complaints-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Employee</th>
                        <th>Leave Type</th>
                        <th>Applied Date</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                      
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detail as $details)
                        <tr>
                            <td>{{ $details->id }}</td>
                            <td>{{ $details->employee['first_name'] }}</td>
                            <td>{{ $details->leave_type['name'] }}</td>
                            <td>{{ $details->applied_date }}</td>
                            <td>{{ $details->start_date }}</td>
                            <td>{{ $details->end_date }}</td>
                        </tr>
                     @endforeach       
                </tbody>
            </table>
        </div>
@stop

@section('javascript')
<!-- date-range-picker -->
<script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
<script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/js/support/complaints/create_modify.js') }}" type="text/javascript"></script>
@endsection
