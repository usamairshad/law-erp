<div class="form-group col-md-3 @if($errors->has('sdate')) has-error @endif">
        {!! Form::label('sdate', 'Start Date *', ['class' => 'control-label']) !!}
        {!! Form::text('sdate' ,old('att_date'), ['class' => 'form-control datepicker']) !!}
        @if($errors->has('sdate'))
                <span class="help-block">
            {{ $errors->first('sdate') }}
        </span>
        @endif
    </div>
    
    <div class="form-group col-md-3 @if($errors->has('edate')) has-error @endif">
        {!! Form::label('edate', 'End Date *', ['class' => 'control-label']) !!}
        {!! Form::text('edate', old('att_date'), ['class' => 'form-control datepicker']) !!}
        @if($errors->has('edate'))
                <span class="help-block">
            {{ $errors->first('edate') }}
        </span>
        @endif
    </div>
    
    
    
    
    <div class="form-group col-md-3 @if($errors->has('employee')) has-error @endif">
            {!! Form::label('employee', 'Employee *', ['class' => 'control-label']) !!}
            {!! Form::select('employee', $Employees, old('employee'), [ 'id' => 'employee' , 'class' => 'form-control select2', 'required' => 'required']) !!}
            @if($errors->has('employee'))
                    <span class="help-block">
                {{ $errors->first('employee') }}
            </span>
            @endif
    </div>
    <div class="form-group col-md-3 @if($errors->has('leave_type_id')) has-error @endif">
            {!! Form::label('leave_type_id', 'Leave Type*', ['class' => 'control-label']) !!}
            {!! Form::select('leave_type_id', $LeaveTypes, old('leave_type_id'), ['onchange' => 'FormControls.loadEmployeeEntitlements();', 'id' => 'leave_type_id', 'class' => 'form-control']) !!}
            <span id="leave_type_id_handler"></span>
            @if($errors->has('leave_type_id'))
                    <span class="help-block">
                            {{ $errors->first('leave_type_id') }}
                    </span>
            @endif
    </div>
    
    
    
    
    