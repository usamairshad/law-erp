<div class="form-group col-md-3 @if($errors->has('time_table_id')) has-error @endif">
    {{ Form::label('Select Time Table*') }}
    {{ Form::select('time_table_id', $time_tables, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required')) }}
</div>
<div class="form-group col-md-3 @if($errors->has('board_id')) has-error @endif">
    {{ Form::label('Select Event*') }}
    {{ Form::select('event_id', $events, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required')) }}
</div>
<div class="form-group col-md-3 @if($errors->has('start_date')) has-error @endif">
    {!! Form::label('date', 'Date*', ['class' => 'control-label']) !!}
    <input type="date" name="date" class="form-control" required maxlength="10">
</div>