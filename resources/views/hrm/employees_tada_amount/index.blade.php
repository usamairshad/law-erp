@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employees Tada Amount</h1>
    </section>
    {{csrf_field()}}
    <input type="hidden" name="name" value="abc">

@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('contacts_create'))
                <a href="{{ route('hrm.employees_tada_amount.create') }}" class="btn btn-success pull-right">Add Employee TA/DA Amount</a>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($EmployeesTadaAmount) > 0 ? 'datatable' : '' }}" id="users-table" >
                <thead>
                <tr>
                    <th>Id</th>
                    <th>User</th>
                    <th>Tada Amount</th>
                    <th>Allowed By</th>
                    <th>Approved By</th>
                    <th>For Date</th>

                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/employees_tada_amount/list.js') }}" type="text/javascript"></script>
@endsection