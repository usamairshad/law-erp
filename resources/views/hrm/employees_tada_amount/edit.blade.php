@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Tada Amount</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Employee Tada Amount</h3>
            <a href="{{ route('hrm.employees_tada_amount.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($EmployeeTadaAmount, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.employees_tada_amount.update', $EmployeeTadaAmount->id]]) !!}
        <div class="box-body">
            @include('hrm.employees_tada_amount.fields')
        </div>
        <!-- /.box-body -->
        {!! Form::hidden('id',  $EmployeeTadaAmount->id  , ['id' => 'id'] ) !!}

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/employees_tada_amount/create_modify.js') }}" type="text/javascript"></script>
@endsection