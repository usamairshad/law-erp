<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif">
    {!! Form::label('user_id', 'Employee *', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $Employees, old('user_id'), ['id' => 'user_id', 'class' => 'form-control select2',
    'onchange' => 'FormControls.getEmployeeDetail(this.value)']) !!}
    <span id="user_id_handler"></span>
    @if($errors->has('user_id'))
        <span class="help-block">
                        {{ $errors->first('user_id') }}
                </span>
    @endif
</div>

<input name="job_title_id" id="job_title_id" type="hidden" value="0">

<div class="form-group col-md-3 @if($errors->has('job_title')) has-error @endif" >
    {!! Form::label('job_title', 'Job Title', ['class' => 'control-label']) !!}
    @if(isset($EmployeeTadaAmount->id))
    {!! Form::text('job_title',  $EmployeeTadaAmount->job_name->name, ['id' => 'job_title', 'class' => 'form-control', 'disabled' => 'true']) !!}
    @else
        {!! Form::text('job_title',  old('job_title'), ['id' => 'job_title', 'class' => 'form-control', 'disabled' => 'true']) !!}
        @endif
    @if($errors->has('job_title'))
        <span class="help-block">
            {{ $errors->first('job_title') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('tada_amount')) has-error @endif" >
    {!! Form::label('tada_amount', 'Tada Amount', ['class' => 'control-label']) !!}
    {!! Form::text('tada_amount',  old('tada_amount'), ['id' => 'tada_amount' ,'class' => 'form-control', 'readonly' => 'true']) !!}
    @if($errors->has('tada_amount'))
        <span class="help-block">
            {{ $errors->first('tada_amount') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('for_date')) has-error @endif" >
    {!! Form::label('for_date', 'For Date', ['class' => 'control-label']) !!}
    {!! Form::text('for_date',  old('for_date'), ['class' => 'form-control datepicker']) !!}
    @if($errors->has('for_date'))
        <span class="help-block">
            {{ $errors->first('for_date') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('allowed_by')) has-error @endif">
    {!! Form::label('allowed_by', 'Allowed By*', ['class' => 'control-label']) !!}
    {!! Form::select('allowed_by', $Employees, old('allowed_by'), ['id' => 'allowed_by', 'class' => 'form-control select2']) !!}
    <span id="allowed_by_handler"></span>
    @if($errors->has('allowed_by'))
        <span class="help-block">
                        {{ $errors->first('allowed_by') }}
                </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('approved_by')) has-error @endif">
    {!! Form::label('approved_by', 'Approved By*', ['class' => 'control-label']) !!}
    {!! Form::select('approved_by', $Employees, old('approved_by'), ['id' => 'approved_by', 'class' => 'form-control select2']) !!}
    <span id="approved_by_handler"></span>
    @if($errors->has('approved_by'))
        <span class="help-block">
                        {{ $errors->first('approved_by') }}
                </span>
    @endif
</div>

