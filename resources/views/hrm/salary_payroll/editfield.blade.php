
<div class="form-group col-md-3 @if($errors->has('teacher_id')) has-error @endif">
    {!! Form::label('techer_id', 'Teacher ID*', ['class' => 'control-label']) !!}
    <input type="text" name="teacher_id" value="{{$attendance_values->teacher_id}}" placeholder="Enter Teacher ID" class="form-control" required>
</div>

<div class="form-group col-md-3 @if($errors->has('subject_id')) has-error @endif">
    {!! Form::label('Subject_id', 'Subject ID', ['class' => 'control-label']) !!}
    <input type="text" name="subject_id" value="{{$attendance_values->subject_id}}" class="form-control" >
</div>
<div class="form-group col-md-3 @if($errors->has('class_id')) has-error @endif">
    {!! Form::label('Class_id', 'Class Id', ['class' => 'control-label']) !!}
    <input type="text" name="class_id" value="{{$attendance_values->class_id}}" class="form-control" >
</div>
<div class="form-group col-md-3 @if($errors->has('first_name')) has-error @endif">
    {!! Form::label('Board', 'Board', ['class' => 'control-label']) !!}
    <input type="text" name="board" class="form-control" value="{{$attendance_values->class_id}}" >
</div>
<div class="form-group col-md-3 @if($errors->has('program')) has-error @endif">
    {!! Form::label('Program', 'Program', ['class' => 'control-label']) !!}
    <input type="text" name="program" class="form-control" value="{{$attendance_values->program}}" >
</div>

<div class="form-group col-md-3 @if($errors->has('class_date')) has-error @endif">
    {!! Form::label('Class_date', 'Class Date*', ['class' => 'control-label']) !!}
    <input type="date" name="class_date" class="form-control" value="{{$attendance_values->class_Date}}" required >
</div>

<div class="form-group col-md-3 @if($errors->has('class_time')) has-error @endif">
    {!! Form::label('Class_date', 'Class Time*', ['class' => 'control-label']) !!}
    <input type="time" name="class_time" class="form-control" value="{{$attendance_values->class_time}}" required>
</div>
<div class="form-group col-md-3 @if($errors->has('attendance_type')) has-error @endif">
    {!! Form::label('attendance_type', 'Attendance Type*', ['class' => 'control-label']) !!}
    <select name="attendance_type" id="attendance_type"  value="{{$attendance_values->attendance_type}}" class="form-control" required >
        
            <option value="Full Day"  @if($attendance_values->attendance_type == 'Full Day') selected @endif>Full Day</option>
            <option value="Hald Day" @if($attendance_values->attendance_type == 'Half Day') selected @endif   >Half Day</option>
            <option value="lecture" @if($attendance_values->attendance_type == 'Lecture') selected @endif   >Lecture</option>
            <option value="Half Lecture" @if($attendance_values->attendance_type == 'Half Lecture') selected @endif   >Half Lecture</option>
       
    </select>

</div>









<script type="text/javascript">
$("#seeAnotherField").change(function() {
  if ($(this).val() != "Teacher") {
    $('#otherFieldDiv').show();
    $('#otherField').attr('required', '');
    $('#otherField').attr('data-error', 'This field is required.');
  } else {
    $('#otherFieldDiv').hide();
    $('#otherField').removeAttr('required');
    $('#otherField').removeAttr('data-error');
  }
});
$("#seeAnotherField").trigger("change");

</script>