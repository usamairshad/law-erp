@inject('request', 'Illuminate\Http\Request')

@include('partials.print_head')
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

</style>

    <div class="box box-primary">
        <!-- /.box-header --> 
        <div class="panel-body pad table-responsive">
        <div class = "col-md-12" style = "font-size:20px;text-align:center; background:grey; padding:10px">Salary Slip</div>
        <table class="table table-bordered" style="width:100%;">
        <tr>
        <td><b>Name</b><td>
        <td>{{ $get_data->first_name }} {{ $get_data->middle_name }} {{ $get_data->last_name }}  </td>
        <td><b>CNIC</b><td>
        <td>{{ $get_data->cnic }} </td>
        <td><b>Provident Fund No.</b><td>
        <td>{{ $get_data->reg_no }} </td>
        </tr>

        <tr>
        <td><b>Designation</b><td>
        <td>{{ $get_data->staff_type }}</td>
        <td><b>NTN</b><td>
        <td>{{ $get_data->ntn }} </td>
        <td><b>Provident Fund Balance</b><td>
        <td>000.00</td>
    </tr>
        
    <tr>
        <td><b>Employee No:</b><td>
        <td>{{ $get_data->reg_no }}</td>
        <td><b>DOB:</b><td>
        <td>{{ $get_data->date_of_birth }}</td>
        <td><b>Loan Balance (if any):</b><td>
        <td>000.00</td>
    </tr>
    <tr>
        <td><b>Date of Joining:</b><td>
        <td>{{ $get_data->join_date }}</td>
        <td><b>Email:</b><td>
        <td>{{ $get_data->email}}</td>
        <td><b>Any Other Info</b><td>
        <td>000.00</td>
        </tr>
        </table>
            <table class="table table-bordered" style="width:100%;">

                <thead>
                <tr>
                    <th>Month</th>
                    <th>Basic Salary</th>
                    <th>After (Deduct)</th>
                    <th>Medical</th>
                    <th style="">Conveyance</th>
                    <th style="">House Rent</th>
                    <th style="text-align: right;">Total Allowances</th>
                    <th style="text-align: right;">Gross Salary</th>
                    <th style="text-align: right;">Advance Loan</th>
                    <th style="text-align: right;">EOBI</th>
                    <th style="text-align: right;">Training</th>
                    <th style="text-align: right;">Income Tax</th>
                    <th style="text-align: right;">EPF</th>
                    <th style="text-align: right;">Total</th>
                    <th style="text-align: right;">Remarks</th>
                </tr>
                <tr>
                    <td>{{ $get_data->month_id }} - {{ $get_data->year_id }}</td>
                    <td>{{ $get_data->salary }}</td>
                    <td>{{ $sum = $get_data->net_salary- $get_data->conveyance_allowance- $get_data->house_rent_allowance -$get_data->medical_allowance +$get_data->eobi+$get_data->pay_loan+$get_data->training_all + $get_data->provident_fund+$get_data->tax_amount}}</td>
                    <td>{{ $get_data->medical_allowance }}</td>
                    <td>{{ $get_data->conveyance_allowance }}</td>
                    <td>{{ $get_data->house_rent_allowance }}</td>
                    <td>{{ $sum = $get_data->medical_allowance + $get_data->conveyance_allowance+ $get_data->house_rent_allowance}}</td>
                    <td>{{ $sum = $get_data->net_salary- $get_data->conveyance_allowance+ $get_data->house_rent_allowance +$get_data->medical_allowance}}</td>
                    <td>{{$get_data->pay_loan}}</td>
                    <td>{{$get_data->eobi}}</td>
                    <td>{{$get_data->training_all}}</td>
                    <td>{{$get_data->tax_amount}}</td> 
                     <td>{{$get_data->provident_fund}}</td>
                       <td>{{$get_data->net_salary}}</td>
                       <td>{{$get_data->description}}</td>
                    
                </tr>
                </thead>
            </table> 
        </div>
    </div>
</div>

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });
    </script>
@endsection
