@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Full Time Employee Salary</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Full Time Employee Salary</h3>
            <a href="{{ route('admin.attendance_payroll.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
<?php
$zero=0;
?>
        {!! Form::open(['method' => 'POST','route' => ['admin.pay.employee_salary']]) !!}

        {!! Form::token(); !!}
        <div class="box-body">
            <div class="form-group col-md-4 ">
                {!! Form::label('staff_id', 'Staff_id*', ['class' => 'control-label']) !!}
               

                <input type="text" name="reg_no" id="reg_no" class="form-control" readonly value="{{$staff_data->id}}" required >



            </div>
          
            <div class="form-group col-md-4 @if($errors->has('staff_salary')) has-error @endif">
                {!! Form::label('Start_date', 'Staff Salary*', ['class' => 'control-label']) !!}
                <input type="text" id="salary" name="salary" readonly value="{{$staff_data->salary}}" class="form-control" required> 
                
            </div>
            <div class="form-group col-md-4 @if($errors->has('month')) has-error @endif">
                {!! Form::label('Month', 'Select Month*', ['class' => 'control-label']) !!}
                <select name= "month_name" id = "month_name" class="form-control" required>
                <option value = "January">January</option>
                <option value = "February">February</option>
                <option value = "March">March</option>
                <option value = "April">April</option>
                <option value = "May">May</option>
                <option value = "June">June</option>
                <option value = "July">July</option>
                <option value = "August">August</option>
                <option value = "September">September</option>
                <option value = "October">October</option>
                <option value = "November">November</option>
                <option value = "December">December</option>
                </select>

            </div>
            <div class="form-group col-md-4 @if($errors->has('month')) has-error @endif">
                {!! Form::label('Year', 'Select Year*', ['class' => 'control-label']) !!}
                <select name= "year_name" id = "year_name" class="form-control" required>
                <option value = "2021">2021</option>
                <option value = "2022">2022</option>
                <option value = "2023">2023</option>
                <option value = "2024">2024</option>
                <option value = "2025">2025</option>
                
                <option value = "2026">2026</option>
                
                <option value = "2027">2027</option>
                </select>

            </div>
            <div class="form-group col-md-4 @if($errors->has('staff_salary')) has-error @endif">
                {!! Form::label('Start_date', 'Staff Salary*', ['class' => 'control-label']) !!}
                <input type="text" id="salary" name="salary" readonly value="{{$staff_data->salary}}" class="form-control" required> 

            </div>
            <div class="form-group col-md-4 @if($errors->has('med_all')) has-error @endif">
                {!! Form::label('med_all', 'Medical Allowance*', ['class' => 'control-label']) !!}
                <input type="text" id="med_all" name="med_all"  value="{{$staff_data->medical_allowances}}" class="form-control" required> 

            </div>
            <div class="form-group col-md-4 @if($errors->has('rent_all')) has-error @endif">
                {!! Form::label('rent_all', 'Rent Allowance*', ['class' => 'control-label']) !!}
                <input type="text" id="rent_all" name="rent_all"  value="{{$staff_data->house_rent}}" class="form-control" required> 

            </div>
            <div class="form-group col-md-4 @if($errors->has('con_all')) has-error @endif">
                {!! Form::label('con_all', 'Conveyance Allowance*', ['class' => 'control-label']) !!}
                <input type="text" id="con_all" name="con_all"  value="{{$staff_data->conveyance}}" class="form-control" required> 

            </div>

            <div class="form-group col-md-4 @if($errors->has('training_all')) has-error @endif">
                {!! Form::label('training_all', 'Training Allowance*', ['class' => 'control-label']) !!}
                <input type="number" min="0"   id="training_all" name="training_all" value="0" class="form-control"> 

            </div>

            




            <div class="form-group col-md-4 @if($errors->has('start_date')) has-error @endif">
                {!! Form::label('Start_date', 'Start Date*', ['class' => 'control-label']) !!}
                <!--<input type="date" id="start_date" name="start_date" class="form-control"  required >
                --><input type="date" id="dateInput1" name="dateInput1" class="form-control" required> 

            </div>

            <div class="form-group col-md-4 @if($errors->has('end_date')) has-error @endif">
                {!! Form::label('End_date', 'End Date*', ['class' => 'control-label']) !!}
              <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
                --><input type="date" id="dateInput2" name="dateInput2" class="form-control" required> 
            </div> 
            <div class="form-group col-md-4 @if($errors->has('days')) has-error @endif">
                {!! Form::label('days', 'Number of Days', ['class' => 'control-label']) !!}
              <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
                --><input type="integer" id="days" name="days" class="form-control" readonly required> 
            </div> 
            <div class="form-group col-md-4 @if($errors->has('leaves')) has-error @endif">
                {!! Form::label('leaves', 'Leaves', ['class' => 'control-label']) !!}
              <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
                --><input type="integer" id="leaves" name="leaves" class="form-control" required> 
            </div>
            <div class="form-group col-md-4 @if($errors->has('deduction')) has-error @endif">
                {!! Form::label('deduction', 'Deduction', ['class' => 'control-label']) !!}
              <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
                --><input type="integer" id="deduction" name="deduction" class="form-control" readonly required> 
            </div>


            <div class="form-group col-md-4 @if($errors->has('eobi')) has-error @endif">
                {!! Form::label('eobi', 'EOBI*', ['class' => 'control-label']) !!}
              <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
                --><input type="integer" id="eobi" readonly name="eobi" value="<?php if (isset($eobi_data->deduction)): ?>{{$eobi_data->deduction}}<?php else:?>{{$zero}}<?php endif ?>" class="form-control"  required> 
            </div>

            <div class="form-group col-md-4 @if($errors->has('provident_fund')) has-error @endif">
                {!! Form::label('provident_fund', 'Provident Fund*', ['class' => 'control-label']) !!}
              <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
                --><input type="integer" id="provident_fund" readonly name="provident_fund" value="<?php if(isset($staff_data->provident_amount)): ?>{{$staff_data->provident_amount}}<?php else:?>{{$zero}}<?php endif ?>" class="form-control"  required> 
            </div>

            <div class="form-group col-md-4 @if($errors->has('tax_amount')) has-error @endif">
                {!! Form::label('tax_amount', 'Tax Amount*', ['class' => 'control-label']) !!}
              <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
                --><input type="number" readonly  id="tax_amount" name="tax_amount" value="{{$tax_amount}}" class="form-control"> 
            </div>



            <div class="form-group col-md-4 @if($errors->has('loan_amount')) has-error @endif">
                {!! Form::label('loan_amount', 'Loan Amount*', ['class' => 'control-label']) !!}
                <input type="number" readonly  id="loan_amount" name="loan_amount" value="<?php if (isset($loan_data->amount)): ?>{{$loan_data->amount}}<?php else:?>{{$zero}}<?php endif ?>" class="form-control"> 

            </div>
            <div class="form-group col-md-4 @if($errors->has('pay_loan')) has-error @endif">
                {!! Form::label('pay_loan', 'Pay Loan', ['class' => 'control-label']) !!}
                <input type="number" required id="pay_loan" name="pay_loan" value="" class="form-control"> 

            </div>


            <div class="form-group col-md-4 @if($errors->has('remaining_amount')) has-error @endif">
                {!! Form::label('remaining_amount', 'Remaining Amount*', ['class' => 'control-label']) !!}
                <input type="number" readonly   id="remaining_amount" name="remaining_amount" 
                value="<?php if (isset($loan_data->remaining)): ?>{{$loan_data->remaining}}<?php else:?>{{$zero}}<?php endif ?>" class="form-control">


                <input type="hidden"   id="remaining_amount1" name="remaining_amount1" value="<?php if (isset($loan_data->remaining)): ?>{{$loan_data->remaining}}<?php else:?>{{$zero}}<?php endif ?>"  class="form-control">  

            </div>



            <div class="form-group col-md-4 @if($errors->has('net_salary')) has-error @endif">
                {!! Form::label('net_salary', 'Net Salary', ['class' => 'control-label']) !!}
              <!--  <input type="date" id="end_date" name="end_date" class="form-control"  required >
                --><input type="integer" id="net_salary" name="net_salary" class="form-control" readonly required> 
            </div>   

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
              <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/staff/create_modify.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function(){
            $('.phone').mask('0000-0000000');
            $('.cnic').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');  
        })
    
    // JavaScript program to illustrate  
    // calculation of no. of days between two date  
  
    $(document).on('change','#dateInput2',function () 
    {
        //define two variables and fetch the input from HTML form
         var dateI1 = $("#dateInput1").val();
         var dateI2 = $("#dateInput2").val();

        //define two date object variables to store the date values
         var date1 = new Date(dateI1);
         var date2 = new Date(dateI2);
 
        //calculate time difference
         var time_difference = date2.getTime() - date1.getTime();

         //calculate days difference by dividing total milliseconds in a day
         var result = time_difference / (1000 * 60 * 60 * 24);
         
         result=result+1;
        $('#days').val(result);

        
})

    $(document).on('change','#leaves',function () 
    {
        // body...

        var days=$('#days').val();
        var leaves=$('#leaves').val();
        var med=$('#med_all').val();
        var rent=$('#rent_all').val();
        var con=$('#con_all').val();
        var training_all=$('#training_all').val();
        var staff_salary=$('#salary').val();
       
        var rate_per_day=staff_salary/days;

       
        var deduction=leaves*rate_per_day;

        deduction=parseInt(deduction);
        


       
        

        $("#deduction").val(deduction);
        
        
        
       

    })


    $(document).on('change','#pay_loan',function() {
        // body...

        var pay_loan=$('#pay_loan').val();  
        var remaining_amount=$('#remaining_amount1').val();
        remaining_amount=remaining_amount- pay_loan;
        


        var staff_salary=$('#salary').val();
        var deduction=$('#deduction').val();
        var med=$('#med_all').val();
        var rent=$('#rent_all').val();
        var eobi=$('#eobi').val();
        var provident_fund=$('#provident_fund').val();
        var tax_amount=$('#tax_amount').val();

        
        var con=$('#con_all').val();
        var training_all=$('#training_all').val();




        deduction=parseInt(deduction);
        rent=parseInt(rent);
        con=parseInt(con);
        med=parseInt(med);
        training_all=parseInt(training_all); 
       
       


        var net_salary=staff_salary - deduction;    
        net_salary=parseInt(net_salary);
        
        net_salary=net_salary+med+rent+con+training_all;

        net_salary=net_salary- pay_loan;
        net_salary=net_salary - eobi;
        net_salary=net_salary- provident_fund;
        net_salary=net_salary - tax_amount;
        
        
        $("#remaining_amount").val(remaining_amount);
        $("#net_salary").val(net_salary);
    })
  
</script> 
@endsection