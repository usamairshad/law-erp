@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Lecture Based Monthly Salary</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Lecture Based Monthly Salary</h3>
            <a href="{{ route('admin.attendance_payroll.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        {!! Form::open(['method' => 'POST','route' => ['admin.Monthly.lecture_based_salary',$staff_data->id]]) !!}

        {!! Form::token(); !!}
        <div class="box-body">
            <div class="form-group col-md-3 @if($errors->has('staff_id')) has-error @endif">
                {!! Form::label('staff_id', 'Staff_id*', ['class' => 'control-label']) !!}
                <input type="text" readonly name="staff_id" class="form-control" value="{{$staff_data->id}}" required >
            </div>
          
            <div class="form-group col-md-4 @if($errors->has('start_date')) has-error @endif">
                {!! Form::label('Start_date', 'Start Date*', ['class' => 'control-label']) !!}
                <input type="date" name="start_date" class="form-control" value="" required >
            </div>
            <div class="form-group col-md-4 @if($errors->has('end_date')) has-error @endif">
                {!! Form::label('End_date', 'End Date*', ['class' => 'control-label']) !!}
                <input type="date" name="end_date" class="form-control" value="" required >
            </div> 
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
              <button class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/staff/create_modify.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function(){
            $('.phone').mask('0000-0000000');
            $('.cnic').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');  
        })
    </script>

@endsection