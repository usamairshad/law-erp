

 



<div class="form-group col-md-3 @if($errors->has('teacher_id')) has-error @endif">
    {!! Form::label('techer_id', 'Teacher ID*', ['class' => 'control-label']) !!}
    <input type="text" name="teacher_id" id="teacher_id" value="{{$staff_data->reg_no}}" placeholder="Enter Teacher ID" class="form-control" required>
</div>
<div class="form-group col-md-3 @if($errors->has('first_name')) has-error @endif">
    {!! Form::label('first_name', 'Teacher Name*', ['class' => 'control-label']) !!}
    <input type="text" name="first_name" value="{{$staff_data->first_name}}" placeholder="Enter Teacher ID" class="form-control" required>
</div>

<div class="form-group col-md-3 @if($errors->has('branch_name')) has-error @endif">
    {!! Form::label('branch_name', 'Branch Name', ['class' => 'control-label']) !!}
    <input type="text" name="branch_name" id="branch_name" value="{{$branch_data->name}}"  class="form-control" >
</div>
<!-- <div class="form-group col-md-3 @if($errors->has('branch_id')) has-error @endif">
    {!! Form::label('branch_id', 'Branch Id', ['class' => 'control-label']) !!}
 -->    <input type="hidden" name="branch_id" id="branch_id" value="{{$branch_data->id}}"  class="form-control" >
<!-- </div>
 -->
<div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
    {!! Form::label('Board', 'Board', ['class' => 'control-label']) !!}

     <select name="SelectBoard" id="SelectBoard" class="form-control SelectBoard">
            <option>Select</option>
              @foreach($boards_data as $board)
                <option value="{{$board->id}}">{{$board->name}}</option>
                                                    
              @endforeach    
      </select>
</div>





<div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif">
   {!! Form::label('Programs', 'Programs', ['class' => 'control-label']) !!}
    <select id="programs" name="programs" class="form-control SelectProgram">      
    </select>
</div>


<div class="form-group col-md-3 @if($errors->has('classes')) has-error @endif">
    {!! Form::label('Class_id', 'Class ', ['class' => 'control-label']) !!}
    <select id="classes" name="classes" class="form-control SelectClass">      
    </select>
    
</div>
<div class="form-group col-md-3 @if($errors->has('sections')) has-error @endif">
    {!! Form::label('Sections_id', 'Section', ['class' => 'control-label']) !!}
    <select id="sections" name="sections" class="form-control SelectSection">      
    </select>
</div>


<div class="form-group col-md-3 @if($errors->has('courses')) has-error @endif">
    {!! Form::label('Subject_id', 'Course', ['class' => 'control-label']) !!}
    <select id="courses" name="courses" class="form-control SelectCourse">      
    </select>
</div>




<div class="form-group col-md-3 @if($errors->has('class_date')) has-error @endif">
    {!! Form::label('Class_date', 'Class Date*', ['class' => 'control-label']) !!}
    <input type="date" name="class_date" class="form-control" required >
</div>

<div class="form-group col-md-3 @if($errors->has('class_time')) has-error @endif">
    {!! Form::label('Class_time', 'Class Time*', ['class' => 'control-label']) !!}
    <input type="time" name="class_time" class="form-control" required>
</div>
<div class="form-group col-md-3 @if($errors->has('attendance_type')) has-error @endif">
    {!! Form::label('attendance_type', 'Attendace Type*', ['class' => 'control-label']) !!}
    <select name="attendance_type" id="attendance_type" class="form-control">
          <option>Select Attendace</option>
          <option value="Full Day">Full Day</option>
          <option value="Half Day">Half Day</option>
          <option value="Lecture">Lecture</option>
          <option value="Half Lecture">Half Lecture</option>
    </select>
</div>









<script type="text/javascript">


//Program Selection
        $(document).on('change','.SelectBoard',function () 
        {
          
            var board_id=$(this).val();
            var branch_id=$('#branch_id').val();
            var a=$('#program').parent();
            console.log(board_id);
            console.log(branch_id);
            var op="";
            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program')!!}',
                data:{'board_id':board_id,'branch_id':branch_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    $html = '<option value="null">select program</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.program.id}>${dat.program.name}</option>`;
                    });

                    $("#programs").html($html);

                },
                error:function(){
                }
            });


        });


//ClassSelection

 $(document).on('change','.SelectProgram',function () 
        {
          
            var program_id=$(this).val();
            console.log(program_id);
            var branch_id=$('#branch_id').val();
            var board_id=$('#SelectBoard').val();
            
            console.log(board_id);
            console.log(branch_id);
            
            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program_class')!!}',
                data:{'board_id':board_id,'branch_id':branch_id,'program_id':program_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    $html = '<option value="null">select class</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.get_class.id}>${dat.get_class.name}</option>`;
                    });

                    $("#classes").html($html);

                },
                error:function(){
                }
            });


        });
//Classes Selection



//Sections

 $(document).on('change','.SelectClass',function () 
        {
          
            var class_id=$(this).val();
            
            var branch_id=$('#branch_id').val();
            var board_id=$('#SelectBoard').val();
            var program_id=$('.SelectProgram').val();

            console.log(class_id);
            console.log(board_id);
            console.log(branch_id);
            console.log(program_id);
            
            


            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program_class_section')!!}',
                data:{'board_id':board_id,'branch_id':branch_id,'program_id':program_id,'class_id':class_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    $html = '<option value="null">select class</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.section.id}>${dat.section.name}</option>`;
                    });

                    $("#sections").html($html);

                },
                error:function(){
                }
            });


        });
//Sections








//Courses

 $(document).on('change','.SelectSection',function () 
        {
          
            var teacher_id=$('#teacher_id').val();
            console.log(teacher_id);

            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program_class_section_courses')!!}',
                data:{'teacher_id':teacher_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);

                    $html = '<option value="null">select course</option>';
                    data.forEach((dat) => {
                        $html += `<option value=${dat.id}>${dat.name}</option>`;
                    });

                    $("#courses").html($html);
                    

                },
                error:function(){
                }
            });


        });
//Courses




$("#seeAnotherField").change(function() {
  if ($(this).val() != "Teacher") { 
    $('#otherFieldDiv').show();
    $('#otherField').attr('required', '');
    $('#otherField').attr('data-error', 'This field is required.');
  } else {
    $('#otherFieldDiv').hide();
    $('#otherField').removeAttr('required');
    $('#otherField').removeAttr('data-error');
  }
});
$("#seeAnotherField").trigger("change");

</script>





