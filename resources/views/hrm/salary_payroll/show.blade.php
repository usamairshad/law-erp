@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Profile</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>

            
            <h3 class="box-title">List</h3>
             <a href="{{ route('admin.leave_management.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Teacher ID</th>
                    <td>{{ $attendance->teacher_id ?? '' }}</td>
                    <th>Subject ID</th>
                    <td>{{ $attendance->subject_id ?? '' }}</td>
                </tr>
                <tr>
                    <th>Class ID</th>
                    <td>{{ $attendance->class_id  ?? ''}}</td>
                    <th>Board</th>
                    <td>{{ $attendance->board ?? '' }}</td>
                </tr>
                <tr>
                    <th>Program</th>
                    <td>{{ $attendance->program }}</td>
                    <th>Class Date</th>
                    <td>{{ $attendance->class_Date }}</td>
                </tr>
                <tr>
                    <th>Class Time</th>
                    <td>{{ $attendance->class_time }}</td>
                    <th>Attendance Type</th>
                    <td>{{ $attendance->attendance_type }}</td>
                </tr>
               

               
            </table>

    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });
    </script>
@endsection
