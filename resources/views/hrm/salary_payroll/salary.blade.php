@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Lecture Based Employee Salary</h1>
    </section>
@stop
<?php
$zero = 0;
 ?>
@section('content')

<?php
$zero=0;
?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>

            
            <h3 class="box-title">List</h3>
             <a href="{{ route('admin.salary_payroll.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->





        {!! Form::open(['method' => 'POST','route' => ['admin.pay.employee_salary']]) !!}


         {!! Form::token(); !!}
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Employee Registration#</th>
                    <td>
                        <input type="text" readonly  name="reg_no" id="reg_no" value="{{$staff_data->id}}" class="form-control reg_no" >


                        </td>
                    <th>Staff Type</th>
                    <td>
                        <input type="text" readonly name="staff_type" value="{{ $staff_data->staff_type ?? '' }}" class="form-control reg_no" >
                    </td>
                </tr>
                <tr>
                    <th>First Name</th>
                    <td>
                        <input type="text" readonly name="first_name" value="{{ $staff_data->first_name  ?? ''}}" class="form-control" >
                    </td>
                    <th>Father Name</th>
                    <td>
                     <input type="text" readonly name="father_name" value="{{ $staff_data->father_name ?? '' }}" class="form-control" >

                        </td>
                </tr>
                <tr>
                    <th>Working Hours</th>
                    <td>
                        <input type="text" readonly name="working_hours" value="{{ $staff_data->working_hours }}" class="form-control" >
                        </td>
                    <th>Skill</th>
                    <td>
                        <input type="text" readonly name="skill" value="{{ $staff_data->skill }}" class="form-control" >

                    </td>
                    
                        
                </tr>

                <tr>

                    <th >Actual Salary</th>
                    <td >

                        <input type="text" readonly name="salary" id="salary" value="{{ $staff_data->salary }}" class="form-control" >

                        </td>
                    <th>Deduction</th>
                    <td>
                        <input type="integer" id="deduction" name="deduction" value="{{$staff_data->deduction}}" class="form-control" readonly required>
                    </td>
                    
                </tr>
                <tr>

                    <th >Medical Allowance*</th>
                    <td >

                         <input type="text" id="med_all" name="med_all"  value="{{$staff_data->medical_allowances}}" class="form-control" required> 

                        </td>
                    <th>Rent Allowance*</th>
                    <td>
                        <input type="text" id="rent_all" name="rent_all"  value="{{$staff_data->house_rent}}" class="form-control" required> 
                    </td>
                    
                </tr>
                <tr>

                    
                    <th>Conveyance Allowance*</th>
                    <td>
                        <input type="text" id="con_all" name="con_all" value="{{$staff_data->conveyance}}" class="form-control" required> 

                    </td>

                    <th >Training Allowance*</th>
                    <td>

                         <input type="number" min="0"  id="training_all" name="training_all" value="0" class="form-control"> 
                    </td>
                </tr>
                <tr>

                <th>EOBI*</th>
               
                    <td>
                        @if($eobi_data == 0)
                        <input type="integer" id="eobi" readonly name="eobi" value="0" class="form-control" readonly required>
                        @else
                        <input type="integer" id="eobi" readonly name="eobi" value="{{$eobi_data->deduction}}" class="form-control" readonly required> 
                        @endif
                    </td>
            
                
               
                    <th>Provident Fund*</th>
                    <td>
                        <input type="integer" id="provident_fund" readonly name="provident_fund" value="{{$staff_data->provident_amount}}" class="form-control"  required> 
                    </td>
                    
                </tr>
                <tr>

                    <th >Loan Amount*</th>
                    <td >

                         <input type="number" readonly   id="loan_amount" name="loan_amount" value="<?php if (isset($loan_data->amount)): ?>{{$loan_data->amount}}<?php else:?>{{$zero}}<?php endif ?>" class="form-control">  

                        </td>
                    <th>Pay Loan*</th>
                    <td>
                        <input type="number" required id="pay_loan" name="pay_loan" value="" class="form-control"> 
                    </td>
                    
                </tr>

              

                <tr>

                    <th >Remaining Amount*</th>
                    <td >

                         <input type="number" readonly   id="remaining_amount" name="remaining_amount" value="{{$loan_data->remaining}}" class="form-control">
                <input type="hidden"   id="remaining_amount1" name="remaining_amount1" value="<?php if (isset($loan_data->remaining)): ?>{{$loan_data->remaining}}<?php else:?>{{$zero}}<?php endif ?>" class="form-control"> 

                        </td>

                        <th >Tax Amount*</th>
                    <td >

                         <input type="number" readonly   id="tax_amount" name="tax_amount" value="{{$tax_amount}}" class="form-control">
                 

                        </td>
                    
                    
                </tr>














                <tr>
                    
                </tr>
                <th style="font-size: 24px;">Net Salary</th>
                        <td>
                        <input type="integer" id="net_salary" name="net_salary" value="{{$staff_data->net_salary}}" class="form-control" readonly required>    

                        </td>
                        
                    
                <tr>
    <td>
    
    <button class="btn btn-primary" type="submit">Save</button>
</td>
              
        
        
            </tr>
               

               
            </table>

            {!! Form::close() !!}

    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });







    $(document).on('change','#pay_loan',function() {
        // body...

        var pay_loan=$('#pay_loan').val(); 

        


        var remaining_amount=$('#remaining_amount1').val();




        remaining_amount=remaining_amount - pay_loan;


        


        var staff_salary=$('#salary').val();


        var deduction=$('#deduction').val();
        var med=$('#med_all').val();
        var rent=$('#rent_all').val();
        var eobi=$('#eobi').val();
        var provident_fund=$('#provident_fund').val();
        var tax_amount=$('#tax_amount').val();

        
        var con=$('#con_all').val();
        var training_all=$('#training_all').val();




        deduction=parseInt(deduction);
        rent=parseInt(rent);
        con=parseInt(con);
        med=parseInt(med);
        training_all=parseInt(training_all); 
       
       


        var net_salary=staff_salary - deduction;  



        net_salary=parseInt(net_salary);
        
        net_salary=net_salary+med+rent+con+training_all;

        
        net_salary=net_salary- pay_loan;
        net_salary=net_salary - eobi;
        net_salary=net_salary- provident_fund;
        net_salary=net_salary - tax_amount;
        
        $("#remaining_amount").val(remaining_amount);
        $("#net_salary").val(net_salary);
    })


    </script>



@endsection
