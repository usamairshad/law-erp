@extends('layouts.app')
@inject('request', 'Illuminate\Http\Request')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
@stop
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Teacher Attendance</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Mark Attendance</h3>
            <a href="{{ route('admin.attendance_payroll.index') }}" class="btn btn-success pull-right">Back</a>
        </div>


       {!! Form::open(['method' => 'POST', 'route' => ['admin.attendance_payroll.store'], 'id' => 'validation-form' , 'enctype' => 'multipart/form-data']) !!} 
       {!! Form::token(); !!}
        <div class="box-body">
            @include('hrm.attendance_payroll.field')
        </div>

        <div class="box-footer">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>

    <script type="text/javascript">




//Load Students

         $(document).on('change','.SelectCourse',function () 
        {
          
            var teacher_id=$('#teacher_id').val();
            console.log(teacher_id);

            


            var branch_id=$('#branch_id').val();
            var board_id=$('#SelectBoard').val();
            var program_id=$('.SelectProgram').val();
            var class_id=$('.SelectClass').val();   
            var section_id=$('.SelectSection').val();


            


            $.ajax({
                type:'get',
                url:'{!!URL::to('admin/find_board_program_class_section_students')!!}',
                data:{'teacher_id':teacher_id,'board_id':board_id,'branch_id':branch_id,'program_id':program_id,'class_id':class_id,'section_id':section_id},
                dataType:'json',//return data will be json
                success:function(data){
                    // here price is coloumn name in products table data.coln name
                    console.log(data);
                    // $html = '<option value="null">select class</option>';
                    // data.forEach((dat) => {
                    //     $html += `<option value=${dat.section.id}>${dat.section.name}</option>`;
                    // });

                    // $("#students").html($html);

                },
                error:function(){
                }
            });

            


        });



//Load Students         







        $(document).ready(function(){
            $('.phone').mask('0000-0000000');
            $('.cnic').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');

        })
        
    </script>
@endsection










<script type="text/javascript">
    $(document).ready(function(){

        

        $(document).on('change','.SelectBoard',function () 
        {

            alert("Good");
            // var prod_id=$(this).val();

            // var a=$('#program').parent();
            

            // console.log(prod_id);
            // var op="";
            // $.ajax({
            //     type:'get',
            //     url:'{!!URL::to('find_board_program')!!}',
            //     data:{'id':prod_id},
            //     dataType:'json',//return data will be json
            //     success:function(data){


            //         // here price is coloumn name in products table data.coln name

            //         a.find('#program').val(data.program);
                    
            //     },
            //     error:function(){
            //     }
            // });


        });
</script>
