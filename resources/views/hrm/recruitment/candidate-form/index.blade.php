@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')


    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Job Application Request</h3>
    <div class="box box-primary">
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }} }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Education</th>
                    <th>Gender</th>
                    <th>Experience</th>
                    <th>Expected Salary</th>
                    <th>CV</th>

                    <th>Phone</th>
                    <th>Email</th>
                    <th>Expected joining</th>
                    <th>Status</th>
                </tr>
                </thead>

                <tbody>                  
                  
                @if (count($data) > 0)
                    @foreach ($data as $Employee)

                        <tr data-entry-id="{{ $Employee->id }}">
                            <td>{{ $Employee->id }}</td>
                            <td>{{ $Employee->name }}</td>
                            <td>{{ $Employee->education }}</td>
                            <td>{{ $Employee->gender }}</td>
                            <td>{{ $Employee->experience }}</td>
                            <td>{{ $Employee->expected_salary }}</td>
    
                            <td>
                              <a href="{{asset($Employee->cv)}}">{{ $Employee->name }}'s-CV</a>
                            </td>
                            <td>{{ $Employee->phone }}</td>
                            <td>{{ $Employee->email }}</td>
                            <td>{{ $Employee->expected_joining }}</td>




                            <td>




                               @if($Employee->status == 'Pending')
                                   <a class="btn btn-xs btn-primary" style="color: white;" href="{{url('admin/candidate_shortlist',$Employee->id)}}" ><span>Short List</span></a>
                                    <a class="btn btn-xs btn-primary" style="color: white;" data-toggle="modal" data-target="#exampleModal1{{$Employee->id}}"><span>Reject</span></a>  
                                @elseif($Employee->status == 'Rejected')
                                    <a class="btn btn-xs btn-primary" style="color: white;"><span>Rejected</span></a>
                                

                                <!--@elseif($Employee->status == 'Accepted' || $Employee->status == 'Interview-Taken' || $Employee->status == 'Interview-Pending' || $Employee->status == 'Acknowledged' ||  $Employee->status == 'Selected')
                                    <a class="btn btn-xs btn-info"><span>Accepted</span></a> -->

                                @elseif($Employee->status == 'Short Listed')
                                    <a class="btn btn-xs btn-primary" style="color: white;"><span>Short Listed</span></a>
                                @elseif($Employee->status == 'Test_Passed')
                                    <a class="btn btn-xs btn-primary" style="color: white;"><span>Test Passed</span></a> 
                                @elseif($Employee->status == 'Interview_Passed')
                                    <a class="btn btn-xs btn-primary" style="color: white;"><span>Interview Passed</span></a>
                                @elseif($Employee->status == 'Hired')
                                    <a class="btn btn-xs btn-success" style="color: white;"><span>Hired</span></a>                                      
                                @endif  




                              </td> 

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

                                              