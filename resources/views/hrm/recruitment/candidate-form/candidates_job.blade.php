@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Candidates </h1>
    </section>
@stop

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">List</h3>
    <div class="box box-primary">
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }} }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Education</th>
                    <th>Gender</th>
                    <th>Experience</th>
                    <th>Expected Salary</th>
                    <th>CV</th> 
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Expected joining</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>                  
                  
                @if (count($data) > 0)
                    @foreach ($data as $Employee)

                        <tr data-entry-id="{{ $Employee->id }}">
                            <td>{{ $Employee->id }}</td>
                            <td>{{ $Employee->name }}</td>
                            <td>{{ $Employee->education }}</td>
                            <td>{{ $Employee->gender }}</td>
                            <td>{{ $Employee->experience }}</td>
                            <td>{{ $Employee->expected_salary }}</td>
    
                            <td>
                              <a href="{{asset($Employee->cv)}}">{{ $Employee->name }}'s-CV</a>
                            </td>
                            <td>{{ $Employee->phone }} {{$Employee->status }}</td>
                            <td>{{ $Employee->email }}</td>
                            <td>{{ $Employee->expected_joining }}</td>




                            <td>


@if($Employee->status == 'Short Listed')                              

<a class="btn btn-xs btn-primary" data-toggle="modal" data-target="#exampleModal{{$Employee->id}}"><span>Test Schedule</span></a>





@elseif($Employee->status == 'Test_Pending')

<a class="btn btn-xs btn-primary" data-toggle="modal" data-target="#testfeedbackModal{{$Employee->id}}"><span>Test Feedback</span></a>


@elseif($Employee->status == 'Test_Passed')
<a class="btn btn-xs btn-primary" data-toggle="modal" data-target="#interviewscheduleModal{{$Employee->id}}"><span>Interview Schedule</span></a>

@elseif($Employee->status == 'Test_Failed')
<a class="btn btn-xs btn-primary" data-toggle="modal" data-target="#interviewModal{{$Employee->id}}"><span>Interview</span></a>


@elseif($Employee->status == 'Interview_Pending')

<a class="btn btn-xs btn-primary" data-toggle="modal" data-target="#interviewfeedbackModal{{$Employee->id}}"><span>Interview Feedback</span></a>

@endif


























<div class="modal fade" id="exampleModal{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">

                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Schedule Candidate  Test</h2>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      {!! Form::open(['route' => 'admin.schedule.store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      <input type="hidden" name="candidate_form_id" value="{{$Employee->id}}">

                                      <input type="hidden" name="category" value="test">

                                        <div class="form-group">
                                          <label for="test_time" class="col-form-label">Test Time*</label>
                                          <input type="datetime-local" name="test_time" class="form-control" required="required">
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                          <div class="form-group">
                                          <label for="test_venue" class="col-form-label">Test Venue*</label>
                                          <input type="text" name="test_venue" class="form-control" required="required">
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
                                          <label for="test_doc" class="col-form-label">Important Notes</label>
                                          <textarea class="form-control" placeholder="Please Bring these Documents on Test Day" name="test_doc"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Schedule</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
</div>                                
                                 






<div class="modal fade" id="interviewscheduleModal{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">

                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Schedule Candidate  Interview</h2>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      {!! Form::open(['route' => 'admin.schedule.store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      <input type="hidden" name="candidate_form_id" value="{{$Employee->id}}">

                                      <input type="hidden" name="category" value="interview">
                                        <div class="form-group">
                                          <label for="interview_time" class="col-form-label">Interview Time*</label>
                                          <input type="datetime-local" name="interview_time" class="form-control" required="required">
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>

                                        <div class="form-group">
                                          <label for="interview_venue" class="col-form-label">Interview Venue*</label>
                                          <input type="text" name="interview_venue" class="form-control" required="required">
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
                                          <label for="interview_doc" class="col-form-label">Important</label>
                                          <textarea class="form-control" placeholder="Please Bring these Documents on Interview Day" name="interview_doc"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Schedule</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                              </div>


















<div class="modal fade" id="testfeedbackModal{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">

                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Candidate Test Feedback </h2>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">





                                      {!! Form::open(['route' => 'admin.schedule.store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      <input type="hidden" name="candidate_form_id" value="{{$Employee->id}}">

                                      <input type="hidden" name="category" value="testfeedback">
                                        <div class="form-group">
                                          <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Result*</p>
      <label class="radio-inline">
        <input type="radio" name="result" checked> Test Passed
      </label>
      <label class="radio-inline">
        <input type="radio" name="result"> Test Failed
      </label>

                                          

                                        </div>



      

                                        <div style="margin-top: 20px">
                                          
                                        </div>

                                        <div class="form-group">
                                          <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Confidence*</p>
        <label class="radio-inline">
          <input type="radio" name="confidence" value="Excellent" checked> Excellent
        </label>
        <label class="radio-inline">
          <input type="radio" name="confidence" value="Good" > Good
        </label>
        <label class="radio-inline">
          <input type="radio" name="confidence" value="Satisfactory" > Satisfactory
        </label>
        <label class="radio-inline">
          <input type="radio" name="confidence" value="Bad"> Bad
        </label>
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
        <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Communication*</p>
        <label class="radio-inline">
          <input type="radio" name="communication" value="Excellent" checked> Excellent
        </label>
        <label class="radio-inline">
          <input type="radio" name="communication" value="Good" > Good
        </label>
        <label class="radio-inline">
          <input type="radio" name="communication" value="Satisfactory" > Satisfactory
        </label>
        <label class="radio-inline">
          <input type="radio" name="communication" value="Bad"> Bad
        </label>          



                                        </div>


                                        <div style="margin-top: 20px">
                                          
                                        </div>
                  <div class="form-group">
                        <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Knowledge*</p>
                        <label class="radio-inline">
                          <input type="radio" name="knowledge" value="Excellent" checked> Excellent
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="knowledge" value="Good" > Good
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="knowledge" value="Satisfactory" > Satisfactory
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="knowledge" value="Bad"> Bad
                        </label>
                  </div>

                  <div class="form-group">
                    <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Remarks</p>
                    <textarea name="remarks" class="form-control"></textarea>
                    
                  </div>

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                              </div>




<div class="modal fade" id="interviewfeedbackModal{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">

                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Candidate  Interview Feedback</h2>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                                  {!! Form::open(['route' => 'admin.schedule.store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      <input type="hidden" name="candidate_form_id" value="{{$Employee->id}}">

                                      <input type="hidden" name="category" value="interviewfeedback">
                                        <div class="form-group">
                                          <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Result*</p>
      <label class="radio-inline">
        <input type="radio" name="result" checked> Test Passed
      </label>
      <label class="radio-inline">
        <input type="radio" name="result"> Test Failed
      </label>

                                          

                                        </div>



      

                                        <div style="margin-top: 20px">
                                          
                                        </div>

                                        <div class="form-group">
                                          <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Confidence*</p>
        <label class="radio-inline">
          <input type="radio" name="confidence" value="Excellent" checked> Excellent
        </label>
        <label class="radio-inline">
          <input type="radio" name="confidence" value="Good" > Good
        </label>
        <label class="radio-inline">
          <input type="radio" name="confidence" value="Satisfactory" > Satisfactory
        </label>
        <label class="radio-inline">
          <input type="radio" name="confidence" value="Bad"> Bad
        </label>
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
        <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Communication*</p>
        <label class="radio-inline">
          <input type="radio" name="communication" value="Excellent" checked> Excellent
        </label>
        <label class="radio-inline">
          <input type="radio" name="communication" value="Good" > Good
        </label>
        <label class="radio-inline">
          <input type="radio" name="communication" value="Satisfactory" > Satisfactory
        </label>
        <label class="radio-inline">
          <input type="radio" name="communication" value="Bad"> Bad
        </label>          



                                        </div>


                                        <div style="margin-top: 20px">
                                          
                                        </div>
  <div class="form-group">
        <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Knowledge*</p>
        <label class="radio-inline">
          <input type="radio" name="knowledge" value="Excellent" checked> Excellent
        </label>
        <label class="radio-inline">
          <input type="radio" name="knowledge" value="Good" > Good
        </label>
        <label class="radio-inline">
          <input type="radio" name="knowledge" value="Satisfactory" > Satisfactory
        </label>
        <label class="radio-inline">
          <input type="radio" name="knowledge" value="Bad"> Bad
        </label>
                  



  </div>


                    <div class="form-group">
                    <p for="interview_venue" style="font-size: 12px; font-weight: bold;" class="col-form-label">Remarks</p>
                    <textarea name="remarks" class="form-control"></textarea>
                    
                  </div>


                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                              </div>












<!--Rejected -->



                                 

                              <div class="modal fade" id="exampleModal1{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-body">
                                      {!! Form::open(['route' => ['admin.reject',$Employee->id],'method' => 'GET', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      {{csrf_field()}}
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-danger">Reject</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                              </div>




                              <div class="modal fade" id="exampleModal1{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-body">
                                      {!! Form::open(['route' => ['admin.reject',$Employee->id],'method' => 'GET', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      {{csrf_field()}}
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-danger">Reject</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                              </div>























                               @if($Employee->status == 'Pending')



                                
                               <a class="btn btn-xs btn-primary" style="color: white;" href="{{url('admin/candidate_shortlist',$Employee->id)}}" ><span>Short List</span></a>

<!--                               <a class="btn btn-xs btn-danger" data-toggle="modal"            data-target="#exampleModal1{{$Employee->id}}"><span>Reject</span></a>
                                 -->
                                <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal1{{$Employee->id}}"><span>Reject</span></a>
                            




                            
                          
                              
                                @elseif($Employee->status == 'Rejected')
                                <a class="btn btn-xs btn-info"><span>Rejected</span></a>
                                @elseif($Employee->status == 'Accepted' || $Employee->status == 'Interview-Taken' || $Employee->status == 'Interview-Pending' || $Employee->status == 'Acknowledged' ||  $Employee->status == 'Selected')
                                <a class="btn btn-xs btn-info"><span>Accepted</span></a>
                                 
                                @endif



                              </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
























@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

                                              















