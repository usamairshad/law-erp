<div class="modal fade" id="exampleModal{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Schedule Candidate Interview</h2>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      {!! Form::open(['route' => 'admin.schedule.store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      <input type="hidden" name="candidate_form_id" value="{{$Employee->id}}">
                                        <div class="form-group">
                                          <label for="scheduled_time" class="col-form-label">Scheduled Time:</label>
                                          <input type="datetime-local" name="scheduled_time" class="form-control" required="required">
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
                                          <label for="required_doc" class="col-form-label">Required Documents</label>
                                          <textarea class="form-control" name="required_doc"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Schedule</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                              </div>                                
                                 

                              <div class="modal fade" id="exampleModal1{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-body">
                                      {!! Form::open(['route' => ['admin.reject',$Employee->id],'method' => 'GET', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      {{csrf_field()}}
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-danger">Reject</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                              </div>











                              <a class="btn btn-xs btn-primary" data-toggle="modal" data-target="#exampleModal{{$Employee->id}}"><span>Accept</span></a>

