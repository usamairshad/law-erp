@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Recruitment</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Job Request</h3>
            <a href="{{ route('admin.recruitment.job-request') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.recruitment.job-request-update'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <input type="hidden" name="id" value="{{$data->id}}">
<!--             <div class="form-group col-md-3">
                <label>Select Branch</label>
                <select name="branch_id" class="form-control" required="required">
                    <option  disabled="disabled" selected="selected"> {{$data->branch_id}}</option>
                    <option value="1">1</option>
                </select>

            </div> -->
            <div class="form-group col-md-3">
                <label>Select Section</label>
                <select name="section" class="form-control" required="required">
                    @foreach($sections as $section)
                        @if($section == $data->section)
                            <option value="{{$section}}" selected>{{$section}}</option>
                        @else
                            <option value="{{$section}}">{{$section}}</option>
                        @endif
                    @endforeach
                </select>

            </div>
            <div class="form-group col-md-3">
                <label>Select Subject</label>
                <select name="subject_id" class="form-control subjects select2"  required="required">
                     @foreach($subjects as $subject)
                        @if($subject->id == $data->subject_id)
                            <option value="{{$subject->id}}" selected>{{$subject->name}}</option>
                        @else
                            <option value="{{$subject->id}}">{{$subject->name}}</option>
                        @endif
                    @endforeach
                </select>

            </div>
            
            <div class="form-group col-md-3">
                <label>Select Program</label>
                 <select name="program_id" class="form-control subjects select2"  required="required">
                     @foreach($programs as $program)
                        @if($program->id == $data->program_id)
                            <option value="{{$program->id}}"  selected>{{$program->faculty}}</option>
                        @else
                            <option value="{{$program->id}}">{{$program->faculty}}</option>
                        @endif
                    @endforeach
                </select>

            </div>
            <div class="form-group col-md-3">
                <div style="margin-top: 30px">
                    <label class="radio-inline">
                      <input <?php echo ($data->gender=='male')?'checked':'' ?> type="radio" value="male" name="gender">Male
                    </label>
                    <label class="radio-inline">
                      <input <?php echo ($data->gender=='female')?'checked':'' ?> type="radio" value="female" name="gender">Female
                    </label>
                </div>  
            </div>            
            {{--<div class="form-group col-md-3">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'100', 'required']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>--}}
            
        </div>
        <div class="box-body">

            <div class="form-group col-md-3">
                <label>Experience</label>
                <input type="text" value="{{$data->experience}}" name="experience" class="form-control" required="required">

            </div>
            <div class="form-group col-md-3">
                {!! Form::label('number', 'Expected Joining', ['class' => 'control-label']) !!}
               <!--  {!! Form::date('expected_joining', old('expected_joining'), ['class' => 'form-control']) !!} -->
                <input type="date" name="expected_joining" class="form-control" value="{{$data->expected_joining}}">

            </div>
            <div class="form-group col-md-3">
                <label>Education</label>
                <input type="text" value="{{$data->education}}" name="education" class="form-control" required="required">
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_edit'), ['class' => 'btn btn-danger']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

@endsection

