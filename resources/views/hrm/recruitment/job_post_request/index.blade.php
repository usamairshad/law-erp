@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Job Post Request</h3>
            

            <a href="{{ route('admin.recruitment.job-request-create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Job Request</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            {{--<table class="table table-bordered table-striped {{ count($Employees) > 0 ? 'datatable' : '' }}">--}}
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }} }}">
                <thead>
                <tr>
                    
                    <th>Job Title</th>
                    <th>Job Category</th>
                    <th>Requested Branch</th>
                    
                    <th>Subject</th>
                    <th>Program</th>
                    <th>Gender</th>
                    <th>Education</th>
                    <th>Experience</th>
                    <th>Expected Joining</th>
                    <th>Created At</th>
                    
                    <th>Actions</th>
                    
                </tr>
                </thead>

                <tbody>                  
                @if (count($data) > 0)
                    @foreach ($data as $Employee)
                        <tr data-entry-id="{{ $Employee->id }}">
                    
                            <td>{{$Employee->job_title}}</td>
                            <td>{{$Employee->job_category}}</td>

                            <td>{{ App\Helpers\Helper::branchIdToName( $Employee->branch_id) }}</td>
                    
                            <td>{{App\Helpers\Helper::subjectIdToName( $Employee->subject_id) }}</td>
                            <td>{{App\Helpers\Helper::programIdToName( $Employee->program_id)  }}</td>
                            <td>{{ $Employee->gender }}</td>
                            <td>{{ $Employee->education }}</td>
                            <td>{{ $Employee->experience }}</td>
                            <td>{{ $Employee->expected_joining }}</td>
                            <td>{{ $Employee->created_at }}</td>
                            <td>
                                {{--<a href="{{ route('admin.recruitment.job-request-edit',[$Employee->id]) }}" class="btn btn-xs btn-info">Edit</a>--}}
                                


                                @if($staff_data['staff_type'] == "HR_head" and $Employee->status==1)
                                <a href="{{ route('admin.recruitment.job-post-create',[$Employee->id]) }}" class="btn btn-xs btn-success">
                                Post Job</a>




                                

                                

                             


                                @elseif($Employee->status==2 )

                                <a href="#" class="btn btn-warning" style="color: white;">
                                <span>Job Posted</span>
                                    
                                </a>

                                <a href="{{url('admin/candidate_list_job',$Employee->id)}}" class="btn btn-xs btn-primary">Candidates</a>

                                @else
                                    

                                <a href="#" class="btn btn-warning" style="color: white;">
                                <span>Job Posting Pending</span>



                                @endif

                                </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection