@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Job Request</h3>
            <a href="{{ route('admin.recruitment.job-request') }}" style = "float:right;" class="btn btn-success text-left">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.recruitment.job-request-store'], 'id' => 'validation-form']) !!}
        <div class="box-body" style = "margin-top:40px;">


            <div class="row">
                <div class="form-group col-md-3">
                    {!! Form::label('job_title', 'Job Title*', ['class' => 'control-label']) !!}
                    <input type="text" name="job_title" required="required" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('job_category', 'Job Category*', ['class' => 'control-label']) !!}
                    <select name="job_category" id="job_category" class="form-control">
                        <option value="teaching" > Teaching Staff</option>
                        <option value="nonteaching" > Non Teaching Staff</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <div style="margin-top: 30px">
                        <label class="radio-inline">
                          <input type="radio" value="male" name="gender" checked>Male
                        </label>
                        <label class="radio-inline">
                          <input type="radio" value="female" name="gender">Female
                        </label>                    
                    </div>

                </div>                
                
            </div>  







            <div class="row" id="teachingstaffsection" >
                <div class="form-group col-md-3">
                    {!! Form::label('section', 'Section*', ['class' => 'control-label']) !!}
                    <select name="section" class="form-control">
                      <option>Select Section</option>
                      <option value="junior">Junior</option>
                      <option value="senior">Senior</option>
                    </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('board')) has-error @endif">
                    {!! Form::label('board_id', 'Boards*', ['class' => 'control-label']) !!}
                        <select id="board" class="form-control select2 boards" name="board_id" required="required">
                            <option selected="selected" disabled="disabled">Select Board</option>
                            @foreach($boards as $key => $board)
                            <option value="{{$board['id']}}">{{$board['name']}}</option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group col-md-3 @if($errors->has('programs')) has-error @endif">
                    {!! Form::label('programs', 'Programs*', ['class' => 'control-label']) !!}
                    <select class="form-control select2 programs" name="program_id" id="programs">
                        
                    </select>
                </div>




                <div class="form-group col-md-3">
                    <label>Select Course</label>
                    
                        <select name="subject_id" class="form-control">
                            <option selected="selected" disabled="disabled">Select Course</option>
                            @foreach($subjects as $subject)
                                    <option value="{{$subject->id}}">{{$subject->name}}</option>
                            @endforeach
                        </select>
                </div>
                


            </div>

            
            {{--<div class="form-group col-md-3">
                {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '','minlength'=>'2','maxlength'=>'100', 'required']) !!}
                @if($errors->has('name'))
                    <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>--}}


            <div class="row">

                <div class="form-group col-md-3">
                    <label>Experience Required*</label>
                    <input type="text" name="experience" class="form-control" required="required">

                </div>

                            <div class="form-group col-md-3">
                <label>Education*</label>
                <input type="text" name="education"  class="form-control" required="required">
            </div>

             <div class="form-group col-md-3">
                {!! Form::label('skill', 'Skill*', ['class' => 'control-label']) !!}
                <input type="text" name="skill" required="required"   class="form-control">
            </div>
                             <div class="form-group col-md-3">
                    {!! Form::label('location', 'Location*', ['class' => 'control-label']) !!}
                    <input type="text" name="location" required="required"  class="form-control">
                </div>




            </div>

            <div class="row">



                <div class="form-group col-md-3">
                    {!! Form::label('number', 'Expected Joining*', ['class' => 'control-label']) !!}
<!--                     {!! Form::date('expected_joining', date('Y-m-d'), ['class' => 'form-control']) !!} -->
<input type="date" name="expected_joining" required="required"  class="form-control">
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('description', 'Job Description*', ['class' => 'control-label']) !!}
                    <input type="text" name="description" required="required"  class="form-control">
                    


                </div>

                
            </div>

                        
            
        </div>
        <div class="box-body">
            

            


        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop


@section('javascript')
<script type="text/javascript">


    $("#job_category").change(function(){

        var Category= $("#job_category").val();
    
        if(Category=="teaching")
        {
$("#teachingstaffsection").show();

        }
        else
        {
            $("#teachingstaffsection").hide();

        }
        
    });



        function selectRefresh() {
            $(".programs").select2({

            });
        }
        function selectRefresh() {
            $(".classes").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
 <script>
        $(function(){
            $('#board').change(function(){
               $("#programs option").remove();;
               var id = $(this).val();
               $.ajax({
                  url : '{{ route( 'admin.board-loadPrograms' ) }}',
                  data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {
                        $.each( result, function(k, v) {
                            $('#programs').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
@endsection

