<!DOCTYPE html>
<html>
<head>


<style>
.center {
  margin: auto;
  width: 60%;
  padding: 10px;
}
.watermark {
    position: absolute;
    z-index: -1;
    width: 700px;
    opacity: 0.1;
    left: 28%;
    top: -50px;
}
</style>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<img src="{{asset('public/images/roots-logo.jpg')}}" class="img-rounded" width="200px" height="175x" style="position: absolute; left: 50px">
<div class="container">
  <div class="row">
    <div class="col-md-12">
		<div style="margin-top: 260px" class=" text-center">
		  <h1 class="display-3" style="font-size: 100px">Thank You!</h1>
		  <p class="lead">Your reqeust for <strong>{{$job_post}}</strong> has been submitted successfully</p>
		  <hr>
		</div>      	
    </div>
  </div>
</div>
<div class="center">
  <img class="watermark" src="{{asset('public/images/roots-logo.jpg')}}">
    <center>
      
    </center>
</div>

</body>
</html>
