@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Schedule Interview</h3>

    <div class="box box-primary">
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }} }}">
                <thead>
                <tr>
                    <th>Branch</th>
                    <th>Candidate</th>
                    <th>Scheduled Time</th>
                    <th>Required Documents</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>                  
                @if (count($data) > 0)
                    @foreach ($data as $Employee)
                        <tr data-entry-id="{{ $Employee->id }}">
                            <td>{{ App\Helpers\Helper::branchIdToName($Employee->branch_id) }}</td>
                            <td>{{ App\Models\Schedule::where('candidate_form_id',$Employee->candidate_form_id)->first()->candidate->name }}</td>
                            <td>{{ $Employee->scheduled_time }}</td>
                            <td>{{ $Employee->required_doc }}</td>

                            @if($Employee->status == 'Interview-Pending' && $Employee->status != 'Interview-Taken' && $Employee->status != 'Selected' && $Employee->status != 'Rejected' &&  $Employee->scheduled_time > $now)
                                <td><a class="btn btn-xs btn-primary" data-toggle="modal" data-target="#exampleModal{{$Employee->id}}"><span>Mark Performance</span></a></td>
                            @elseif($Employee->status == 'Interview-Taken')
                                <td><a  data-id="{{$Employee->id}}" class="btn btn-xs btn-primary candidate-score" data-toggle="modal" data-target="#exampleModal1l{{$Employee->id}}"><span>View Eligiblity Score</span></a></td>


                             @elseif($Employee->status == 'Interview_Passed')
                                <td><a class="btn btn-xs btn-success"><span>Hired</span></a></td> 
                            @elseif($Employee->status == 'Rejected')
                                <td><a class="btn btn-xs btn-danger"><span>Rejected</span></a></td> 
                            @elseif($Employee->scheduled_time < $now )
                                <td><a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal2{{$Employee->candidate_form_id}}"><span>Reschedule</span></a></td>        
                            @endif
                            <div class="modal fade" id="exampleModal2{{$Employee->candidate_form_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Re-schedule Candidate Interview</h2>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      {!! Form::open(['route' => 'admin.schedule.store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                      <input type="hidden" name="reschedule" value="reschedule">
                                      <input type="hidden" name="schedule_id" value="{{$Employee->id}}">
                                      <input type="hidden" name="candidate_form_id" value="{{$Employee->candidate_form_id}}">
                                        <div class="form-group">
                                          <label for="scheduled_time" class="col-form-label">Scheduled Time:</label>
                                          <input type="datetime-local" value="{{ date('Y-m-d\TH:i', strtotime($Employee->scheduled_time)) }}" name="scheduled_time" class="form-control" required="required">
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
                                          <label for="required_doc" class="col-form-label">Required Documents</label>
    
                                          {!! Form::textarea('required_doc', $Employee->required_doc, array('placeholder' => 'Required Document ','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Re-schedule</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                              </div> 
                            <div class="modal fade" id="exampleModal1l{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Schedule Candidate Interview</h2>
                                      
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                          <label for="relevant_knowledge" class="col-form-label">Relevant Knowledge</label>
                                          <input class="form-control" type="text" readonly="readonly" name="relevant_knowledge" id="relevant_knowledge_{{$Employee->id}}">
                           
                                        </div>
                                        <div class="form-group">
                                          <label for="relevant_experience" class="col-form-label">Relevant Experience</label>
                                          <input class="form-control" type="text" readonly="readonly" name="relevant_experience" id="relevant_experience_{{$Employee->id}}">
                           
                                        </div>
                                        <div class="form-group">
                                          <label for="confidence" class="col-form-label">Confidence</label>
                                          <input class="form-control" type="text" readonly="readonly" name="confidence" id="confidence_{{$Employee->id}}">
                           
                                        </div>
                                        <div class="form-group">
                                          <label for="communication" class="col-form-label">Communication</label>
                                          <input class="form-control" type="text" readonly="readonly" name="communication" id="communication_{{$Employee->id}}">
                           
                                        </div>
                                        <div class="form-group">
                                          <label for="skills" class="col-form-label">Skills</label>
                                          <input class="form-control" type="text" readonly="readonly" name="skills" id="skills_{{$Employee->id}}">
                           
                                        </div>
                                        <div class="form-group">

                                           <a href="{{route ('admin.schedule.hire',$Employee->id) }}">
                                              <button type="button" class="btn btn-success">Hire</button>
                                          </a>
                                        
                                          <a href="{{route ('admin.schedule.reject',$Employee->id)}}">
                                              <button type="button" class="btn btn-danger">Reject</button>
                                          </a>
                           
                                        </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        
                                    </div>
                                  </div>
                                   
                                </div>
                            </div>
                            <div class="modal fade" id="exampleModal{{$Employee->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Mark Candidate Interview Performance</h2>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      {!! Form::open(['route' => 'admin.schedule.score','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]) !!}
                                        <input type="hidden" name="candidate_form_id" value="{{$Employee->candidate_form_id}}">
                                        <input type="hidden" name="schedule_id" value="{{$Employee->id}}">
                                        <div class="form-group">
                                          <label for="relevant_knowledge" class="col-form-label">Relevant Knowledge</label>
                                          <textarea class="form-control" name="relevant_knowledge"></textarea>
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
                                          <label for="skills" class="col-form-label">Skills</label>
                                          <textarea class="form-control" name="skills"></textarea>
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
                                          <label for="communication" class="col-form-label">Communication</label>
                                          <textarea class="form-control" name="communication"></textarea>
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                        <div class="form-group">
                                          <label for="relevant_experience" class="col-form-label">Relevant Experience</label>
                                          <textarea class="form-control" name="relevant_experience"></textarea>
                                        </div>
                                        <div class="form-group">
                                          <label for="confidence" class="col-form-label">Confidence</label>
                                          <textarea class="form-control" name="confidence"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                    {!! Form::close() !!}
                                  </div>
                                   
                                </div>
                            </div>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
        $(function() {
           $(".candidate-score").on('click', function () {   
                   var id=  $(this).data('id');
                   console.log(id)
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('admin.schedule.details') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            employee_id: id
                        },
                        success: function (response) {
                          console.log(response)
                           $("#relevant_knowledge_"+response.schedule_id).val(response.relevant_knowledge);
                           $("#relevant_experience_"+response.schedule_id).val(response.relevant_experience);
                           $("#communication_"+response.schedule_id).val(response.communication);
                           $("#skills_"+response.schedule_id).val(response.skills);
                           $("#confidence_"+response.schedule_id).val(response.confidence);
                        }
                    });
            })
        });
    </script>
@endsection

                                      