<!DOCTYPE html>
<html>
<head>


<style>
.center {
  margin: auto;
  width: 60%;
  padding: 10px;
}
.watermark {
    position: absolute;
    z-index: -1;
    width: 700px;
    opacity: 0.1;
    left: 28%;
    top: -50px;
}

</style>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<img src="{{asset('public/images/roots-logo.jpg')}}" class="img-rounded" width="200px" height="175x" style="position: absolute; left: 50px">
<h1 style="font-weight: bold; text-align: center; color: purple">Apply For {{$data->job_title}}</h1>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary" style="margin-top: 120px;border:2px solid purple; height:250px;border-radius: 27px" >
          {!! Form::open(['method' => 'POST', 'route' => ['candidate-form.store'], 'id' => 'validation-form', "enctype" => "multipart/form-data"]) !!}
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
         <input type="hidden" name="job_post_id" value="{{$data->id}}">
        <div class="box-body">
            <div class="form-group col-md-3">
                <label>Name</label>
                <input type="text" placeholder="Enter Your Name" name="name" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Education</label>
                <input type="text" name="education" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Experience</label>
                <input type="text" name="experience" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Expected Salary</label>
                <input type="number" name="expected_salary" class="form-control" required="required">
            </div>
            <!-- <div class="form-group col-md-3">
                <label>CV</label>
                <input type="file" name="cv" class="form-control" required="required">
            </div> -->
            <div class="form-group col-md-3">
                <label>CV</label>
                <input type="file" name="cv" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Phone</label>
                <input type="number" name="phone" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Email</label>
                <input type="email" name="email" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Expected Joining</label>
                <input type="date" name="expected_joining" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Cover Letter</label>
                <textarea type="text" name="cover_letter" class="form-control" required="required"></textarea>
            </div>            
            <div class="form-group col-md-3">
                <label>Password</label>
                <input type="password" name="password" class="form-control" required="required">
            </div>
            <div class="form-group col-md-2">
                <div style="margin-top: 30px">
                    <label class="radio-inline">
                      <input type="radio" value="male" name="gender" checked>Male
                    </label>
                    <label class="radio-inline">
                      <input type="radio" value="female" name="gender">Female
                    </label>                    
                </div>

            </div>
            <div class="form-group col-md-4">
              <button style="margin-top: 22px" type="submit" class="btn btn-success" style="width: 100%">Apply</button>
            </div>
        </div>
          <!-- /.box-body -->

           
          {!! Form::close() !!}
      </div>
      
    </div>
  </div>
</div>
<div class="center">
  <img class="watermark" src="{{asset('public/images/roots-logo.jpg')}}">
    <center>
      
    </center>
</div>

</body>
</html>
