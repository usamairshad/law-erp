@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Waiting List of  Candidate</h3>

    <div class="box box-primary">
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }} }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Education</th>
                    <th>Experience</th>
                    <th>Expected Salary</th>
                    <th>CV</th>
                    <th>Cover Letter</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Expected joining</th>
                    <th>Actioin</th>
                </tr>
                </thead>

                <tbody>                  
                @if (count($data) > 0)
                    @foreach ($data as $Employee)
                        <tr data-entry-id="{{ $Employee->id }}">
                            <td>{{ $Employee->name }}</td>
                            <td>{{ $Employee->education }}</td>
                            <td>{{ $Employee->experience }}</td>
                            <td>{{ $Employee->expected_salary }}</td>
                            <td>
                              <a href="{{asset($Employee->cv)}}">{{ $Employee->name }}'s-CV</a>
                            </td>
                            <td>{{ $Employee->cover_letter }}</td>
                            <td>{{ $Employee->phone }}</td>
                            <td>{{ $Employee->email }}</td>
                            <td>{{ $Employee->expected_joining }}</td>
                            <td><a class="btn btn-xs btn-success" style="color: white;" ><span>Waiting</span></a> </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

                                              