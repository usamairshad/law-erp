@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('stylesheet')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Recommended Candidates</h1>
    </section>
@stop

@section('content')



<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">List</h3>
    <div class="box box-primary">
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }} }}">
                <thead>
                <tr>

                    <th>Name</th>
                    <th>Education</th>
                    <th>Gender</th>
                    <th>Experience</th>
                    <th>Expected Salary</th>
                    <th>CV</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Expected joining</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>                  
                  
                @if (count($data) > 0)
                    @foreach ($data as $Employee)

                        <tr data-entry-id="{{ $Employee->id }}">
                            
                            <td>{{ $Employee->name }}</td>
                            <td>{{ $Employee->education }}</td>
                            <td>{{ $Employee->gender }}</td>
                            <td>{{ $Employee->experience }}</td>
                            <td>{{ $Employee->expected_salary }}</td>
    
                            <td>
                              <a href="{{asset($Employee->cv)}}">{{ $Employee->name }}'s-CV</a>
                            </td>
                            <td>{{ $Employee->phone }}</td>
                            <td>{{ $Employee->email }}</td>
                            <td>{{ $Employee->expected_joining }}</td>




                            <td>


                                  @if($Employee->status=="Interview_Passed")
                                    <a href="{{url('admin/candidate_feedback_job',$Employee->id)}}" class="btn btn-xs btn-warning" style="color: white;"><span>Feedback</span></a>


                                    <a class="btn btn-xs btn-primary" style="color: white;" href="{{url('admin/candidate_hired_job',$Employee->id)}}" ><span>Hire</span></a>
                                    <a class="btn btn-xs btn-primary" style="color: white;" href="{{url('admin/candidate_rejected_job',$Employee->id)}}" ><span>Reject</span></a>
                                  @endif  
                                  @if($Employee->status=="Hired")     
                                  <a class="btn btn-xs btn-success" style="color: white;" ><span>Hired</span></a>
                                  @endif
                                  @if($Employee->status=="Rejected") 
                                          <a class="btn btn-xs btn-primary" style="color: white;" ><span>Rejected</span></a>
                                  @endif 
                                  

                               


                              </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

                                              