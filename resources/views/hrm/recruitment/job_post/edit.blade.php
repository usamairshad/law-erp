@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Recruitment</h1>
    </section>
@stop

<style type="text/css">
    .form-group{
        float: left;
    }
</style>

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">            <h3 class="box-title" style="float: left;">Edit Job Post</h3>
            <a href="{{ route('admin.recruitment.job-request') }}" style="float: right;" class="btn btn-success pull-right">Back</a>
        </div>
        <br>
        <br>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.recruitment.job-post-update'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <input type="hidden" name="branch_id" value="{{$branch_id}}">
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="job_post_request_id" value="{{$data->job_post_request_id}}">
            <div class="form-group col-md-3">
                <label>Job Title</label>
                <input type="text" name="job_title" value="{{$data->job_title}}" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Select Section</label>
                <select name="section" class="form-control" required="required">
                    @foreach($sections as $section)
                        @if($section == $data->section)
                            <option value="{{$section}}" selected>{{$section}}</option>
                        @else
                            <option value="{{$section}}">{{$section}}</option>
                        @endif
                    @endforeach
                </select>

            </div>
            <div class="form-group col-md-3">
                <label>Experience</label>
                <input type="text" value="{{$data->experience}}" name="experience" class="form-control" required="required">

            </div>
            <div class="form-group col-md-3">
                {!! Form::label('number', 'Expected Joining', ['class' => 'control-label']) !!}
               <!--  {!! Form::date('expected_joining', old('expected_joining'), ['class' => 'form-control']) !!} -->
                <input type="date" name="expected_joining" class="form-control" value="{{$data->expected_joining}}">

            </div>
            <div class="form-group col-md-3">
                <label>Education</label>
                <input type="text" value="{{$data->education}}" name="education" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Skill</label>
                <input type="text"   value="{{$data->skill}}" name="skill" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Location</label>
                <input type="text" name="location" value="{{$data->location}}" class="form-control" required="required">
            </div>
            <div class="form-group col-md-3">
                <label>Job Description</label>
                <input type="text" name="location" value="{{$data->description}}" class="form-control">
            </div>
            
        </div>

        <div class="box-footer">
            {!! Form::submit(trans('global.app_edit'), ['class' => 'btn btn-danger']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

@endsection

