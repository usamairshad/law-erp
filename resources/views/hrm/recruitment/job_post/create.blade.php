@extends('layouts.app')
@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Recruitment</h1>
    </section>
@stop

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">            <h3 class="box-title" style="float: left;">Post Job</h3>
            <a href="{{ route('admin.recruitment.job-request') }}" style="float: right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method' => 'POST', 'route' => ['admin.recruitment.job-post-store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            <input type="hidden" name="job_post_request_id" value="{{$data->id}}">

<br>
<br>            
            <div class="row">
                <div class="form-group col-md-3">
                    <label>Job Title</label>
                    <input type="text" name="job_title" value="{{$data->job_title}}" class="form-control" required="required">
                </div>


            @if($data->job_category=="teaching")

                <div class="form-group col-md-3">
                    <label>Select Section</label>
                    <select name="section" class="form-control" required="required">
                        <option value="" disabled="disabled" selected="selected"> Select Section</option>

                        <option value="Junior" <?php if ($data->section=="junior"): ?>
                            selected
                        <?php endif ?>> Junior</option>
                        <option value="Senior" <?php if ($data->section=="senior"): ?>
                            selected
                        <?php endif ?> >Senior</option>
                    </select>

                </div>

            @endif    
                <div class="form-group col-md-3">
                    <label>Experience</label>
                    <input type="text" value="{{$data->experience}}" name="experience" class="form-control" required="required">

                </div>
                <div class="form-group col-md-3">
                    {!! Form::label('number', 'Expected Joining', ['class' => 'control-label']) !!}
    <!--                 {!! Form::date('expected_joining', date('Y-m-d'), ['class' => 'form-control']) !!}
     -->                <input type="date" name="expected_joining"  class="form-control" value="{{$data->expected_joining}}">

                </div>
                
            </div>


            <div class="row">
            
                <div class="form-group col-md-3">
                    <label>Education</label>
                    <input type="text" name="education" value="{{$data->education}}" class="form-control" required="required">
                </div>
                 <div class="form-group col-md-3">
                    <label>Skill</label>
                    <input type="text" name="skill" value="{{$data->skill}}" class="form-control" required="required">
                </div>
                <div class="form-group col-md-3">
                    <label>Location</label>
                    <input type="text" name="location" value="{{$data->location}}" class="form-control" required="required">
                </div>
                <div class="form-group col-md-3">
                    <label>Job Description</label>
                    <input  type="text" required name="description" value="{{$data->description}}" class="form-control">
                    
                </div>    

            </div>            
            
            
            
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

@endsection

