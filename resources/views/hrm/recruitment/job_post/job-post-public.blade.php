<!DOCTYPE html>
<html>
<head>


<style>
.center {
  margin: auto;
  width: 60%;
  padding: 10px;
}
.watermark {
	position: absolute;
    z-index: -1;
    width: 700px;
    opacity: 0.1;
    left: 28%;
    top: -50px;
}
</style>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<img src="{{asset('public/images/roots-logo.jpg')}}" class="img-rounded" width="200px" height="175x" style="position: absolute; left: 50px">
<h2 style="text-align: center;">Job Post</h2>

<div class="center">
  <img class="watermark" src="{{asset('public/images/roots-logo.jpg')}}">
  <table class="table table-bordered table-stripped">
  	<tr>
  		<th>Title</th>
  		<td>{{$job_post->job_title}}</td>
  	</tr>

  	<tr>
  		<th>Education</th>
  		<td>{{$job_post->education}}</td>
  	</tr>
  	<tr>
  		<th>Skills</th>
  		<td>{{$job_post->skill}}</td>
  	</tr>
  	<tr>
  		<th>Years of Experience</th>
  		<td>{{$job_post->experience}}</td>
  	</tr>
  	<tr>
  		<th>Location</th>
  		<td>{{$job_post->location}}</td>
  	</tr>
  	<tr>
  		<th width="30%">Expected joining Date</th>
  		<td>{{$job_post->expected_joining}}</td>
  	</tr>
  	<tr>
  		<th>Description</th>
  		<td>{{$job_post->description}}</td>
  	</tr>
  	<tr>
  		<th>Status</th>
  		<td>
  			@if($job_post->status == 1)
  			<button class="badge btn-success" style="background-color: green">Active</button>
  			@endif

  			@if($job_post->status == 0)
  			<button class="badge btn-danger" style="background-color: red">Expired</button>
  			@endif
  		</td>
  	</tr>
  	</table>
  	<center>
      @if($job_post->status == 0)
      <button class="btn btn-success" style="width: 50%" disabled="disabled">Apply</button>
      @else
      <a href="{{ route('candidate-form-create',$job_post->id) }}">
        <button class="btn btn-success" style="width: 50%">Apply</button>
      </a>
      @endif
      
	</center>
</div>

</body>
</html>
