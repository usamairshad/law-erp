@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Job Post</h3>
            {{--@if(Gate::check('employees_create'))
                <a href="{{ route('hrm.employees.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Job Request</a>
            @endif--}}
            

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            {{--<table class="table table-bordered table-striped {{ count($Employees) > 0 ? 'datatable' : '' }}">--}}
            <table class="table table-bordered table-striped  {{ count($data) > 0 ? 'datatable' : '' }} }}">
                <thead>
                <tr>
                    <th>Branch</th>
                     <th>Job Title</th>
                    <th>Section</th>
                    <th>Education</th>
                    <th>Experience</th>
                    <th>Skill</th>
                    <th>Link</th>
                    <th>Expected Joining</th>
                    <th>Created At</th>
                    <th>Actions</th>
                    <th>Status</th>
                    <th>Recommended Candidates</th>
                </tr>
                </thead>

                <tbody>                  
                @if (count($data) > 0)
                    @foreach ($data as $Employee)
                        <tr data-entry-id="{{ $Employee->id }}">
                        
                            <td>{{ App\Helpers\Helper::branchIdToName($Employee->branch_id) }}</td>
                            <td>{{ $Employee->job_title }}</td>
                            <td>{{ $Employee->section }}</td>
                            <td>{{ $Employee->education }}</td>
                            <td>{{ $Employee->experience }}</td>
                            <td>{{ $Employee->skill }}</td>
                            <td><a class="copy_text"  data-toggle="tooltip" title="Copy to Clipboard" href="{{ $Employee->public_link }}">Job Link</a></td>
                            <td>{{ $Employee->expected_joining }}</td>
                            <td>{{ $Employee->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.recruitment.job-post-edit',[$Employee->id]) }}" class="btn btn-xs btn-info">Edit</a>
                            </td>
                            <td>             
                                <input  id="status" type="checkbox" checked data-toggle="toggle">
                            </td>
                            <td>

                                <a href="{{ url('admin/recommended_candidates',$Employee->id) }}" class="btn btn-success btn-small">Recommended Candidates</a>
                                

                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>    
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    @if (count($data) > 0)
    <script>

        $('.copy_text').click(function (e) {
           e.preventDefault();
           var copyText = $(this).attr('href');

           document.addEventListener('copy', function(e) {
              e.clipboardData.setData('text/plain', copyText);
              e.preventDefault();
           }, true);

           document.execCommand('copy');  
           // console.log('copied text : ', copyText);
           alert('copied text: ' + copyText); 
         });

        $(document).ready(function(){
            $('.datatable').DataTable()
        });

        $("#status").change(function(){
            
            if($("#status").is(':checked'))
            {
               var base_url = "{{url()->current()}}";
               var url = base_url+"/"+"job-status-update-active/"+"{{$Employee->id}}";
               $.ajax({
                  url: url,
                });
               var notification = alertify.notify('Job Post Activated Successfully', 'success', 5, function(){  console.log('dismissed'); });
            }
            else
            {
               var base_url = "{{url()->current()}}";
               var url = base_url+"/"+"job-status-update-inactive/"+"{{$Employee->id}}";
               $.ajax({
                  url: url,
                });               
               var notification = alertify.notify('Job Post Deactivated Successfully', 'success', 5, function(){  console.log('dismissed'); });   
            }
        }); 
    </script>
    @endif

@endsection