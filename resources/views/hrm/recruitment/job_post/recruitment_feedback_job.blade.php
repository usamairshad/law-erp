@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Candidate Feedback</h1>


        
    </section>
@stop

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
                        <!--div-->
                        <div class="card">
                            <div class="card-body">
                                <div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Feedback Detail</h3>
            <a href="{{ url()->previous() }}"style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>


        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($data) > 0 ? 'datatable' : '' }} }}">
                <thead>
                <tr>


                    <th>Category</th>
                    <th>Confidence</th>
                    <th>Communication</th>
                    <th>Knowledge</th>
                    <th>Remarks</th>

                </tr>
                </thead>

                <tbody>                  
                  
                @if (count($data) > 0)
                    @foreach ($data as $Employee)

                        <tr data-entry-id="{{ $Employee->id }}">
                            

                            <td>
                                @if($Employee->category=="interviewfeedback")

                                    {{ "Interview Feedback" }}

                                @else
                                        {{ "Test Feedback" }}
                                @endif            
                                </td>
                            <td>{{ $Employee->confidence }}</td>
                            <td>{{ $Employee->communication }}</td>
                            <td>{{ $Employee->knowledge }}</td>
                            <td>{{ $Employee->remarks }}</td>
    
                            




                          

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection

                                              