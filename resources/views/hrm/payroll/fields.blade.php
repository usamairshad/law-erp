{{csrf_field()}}
<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif" id="user_div">
        {!! Form::label('user_id', 'User ', ['class' => 'control-label']) !!}
        {!! Form::select('user_id', $Employees , old('user_id'),
         ['class' => 'form-control select2'  ,'id' => 'user_id']) !!}
</div>


<div class="form-group col-md-3 @if($errors->has('year')) has-error @endif">
        {!! Form::label('year', 'Year', ['class' => 'control-label']) !!}
        {!! Form::select('year', array('' => 'Select Year') + Config::get('hrm.year_array') , old('year'), ['class' => 'form-control', 'id'=> 'year']) !!}
        @if($errors->has('year'))
                <span class="help-block">
            {{ $errors->first('year') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('month')) has-error @endif">
        {!! Form::label('month', 'Month', ['class' => 'control-label']) !!}
        {!! Form::select('month',array('' => 'Select Month') + Config::get('hrm.month_array'), old('month'), ['class' => 'form-control', 'id'=> 'month']) !!}
        @if($errors->has('month'))
                <span class="help-block">
            {{ $errors->first('month') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3">
        <button id="search_button" onclick="FormControls.fetchEmployeeDetail();" type="button" style="margin-top: 29px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search Employee Detail </b> </button>
</div>

    {!! Form::hidden('bank_id', old('bank_id'), ['class' => 'form-control', 'id'=> 'bank_id']) !!}


<div class="form-group col-md-12 @if($errors->has('emp_balance')) has-error @endif">
    {!! Form::label('emp_balance', 'Balance', ['class' => 'control-label']) !!}
    {!! Form::text('emp_balance', old('emp_balance'), ['class' => 'form-control', 'readonly' => 'true', 'id'=> 'emp_balance']) !!}
    @if($errors->has('emp_balance'))
        <span class="help-block">
            {{ $errors->first('emp_balance') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('bank_name')) has-error @endif">
    {!! Form::label('bank_name', 'Bank', ['class' => 'control-label']) !!}
    {!! Form::text('bank_name', old('bank_name'), ['class' => 'form-control', 'readonly' => 'true', 'id'=> 'bank_name']) !!}
    @if($errors->has('bank_name'))
        <span class="help-block">
            {{ $errors->first('bank_name') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('account_number')) has-error @endif">
        {!! Form::label('account_number', 'Account Number', ['class' => 'control-label']) !!}
        {!! Form::text('account_number', old('account_number'), ['class' => 'form-control','readonly' => 'true','id'=> 'account_number']) !!}
        @if($errors->has('account_number'))
                <span class="help-block">
            {{ $errors->first('account_number') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('basic_salary')) has-error @endif">
    {!! Form::label('basic_salary', 'Basic Salary', ['class' => 'control-label']) !!}
    {!! Form::text('basic_salary', old('basic_salary'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'basic_salary']) !!}
    @if($errors->has('basic_salary'))
        <span class="help-block">
            {{ $errors->first('basic_salary') }}
        </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('sc_amount')) has-error @endif">
        {!! Form::label('sc_amount', 'SC Amount', ['class' => 'control-label']) !!}
        {!! Form::text('sc_amount', old('sc_amount'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'sc_amount']) !!}
        @if($errors->has('sc_amount'))
                <span class="help-block">
            {{ $errors->first('sc_amount') }}
        </span>
        @endif
</div>
<div class="form-group col-md-3 @if($errors->has('house_rent')) has-error @endif">
        {!! Form::label('house_rent', 'House Rent', ['class' => 'control-label']) !!}
        {!! Form::text('house_rent', old('house_rent'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'house_rent']) !!}
        @if($errors->has('house_rent'))
                <span class="help-block">
            {{ $errors->first('house_rent') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('utilities')) has-error @endif">
        {!! Form::label('utilities', 'Utilities', ['class' => 'control-label']) !!}
        {!! Form::text('utilities', old('utilities'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'utilities']) !!}
        @if($errors->has('utilities'))
                <span class="help-block">
            {{ $errors->first('utilities') }}
        </span>
        @endif
</div>

<div class="form-group col-md-3 @if($errors->has('overtime')) has-error @endif">
        {!! Form::label('overtime', 'Overtime', ['class' => 'control-label']) !!}
        {!! Form::text('overtime', old('overtime'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'overtime']) !!}
        @if($errors->has('overtime'))
                <span class="help-block">
            {{ $errors->first('overtime') }}
        </span>
        @endif
</div>


<div class="form-group col-md-3 @if($errors->has('total_pf')) has-error @endif">
    {!! Form::label('total_pf', 'Total PF', ['class' => 'control-label']) !!}
    {!! Form::text('total_pf', old('total_pf'), ['class' => 'form-control','readonly' => 'false' , 'id'=> 'total_pf']) !!}
    @if($errors->has('total_pf'))
        <span class="help-block">
            {{ $errors->first('total_pf') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('leaves')) has-error @endif">
    {!! Form::label('leaves', 'Leaves', ['class' => 'control-label']) !!}
    {!! Form::text('leaves', old('leaves'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'leaves']) !!}
    @if($errors->has('leaves'))
        <span class="help-block">
            {{ $errors->first('leaves') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('short_leaves')) has-error @endif">
    {!! Form::label('short_leaves', 'Short Leaves', ['class' => 'control-label']) !!}
    {!! Form::text('short_leaves', old('short_leaves'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'short_leaves']) !!}
    @if($errors->has('short_leaves'))
        <span class="help-block">
            {{ $errors->first('short_leaves') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('half_leaves')) has-error @endif">
    {!! Form::label('half_leaves', 'Half Leaves', ['class' => 'control-label']) !!}
    {!! Form::text('half_leaves', old('half_leaves'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'half_leaves']) !!}
    @if($errors->has('half_leaves'))
        <span class="help-block">
            {{ $errors->first('half_leaves') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('sandwich_leaves')) has-error @endif">
    {!! Form::label('sandwich_leaves', 'Sandwich Leaves', ['class' => 'control-label']) !!}
    {!! Form::text('sandwich_leaves', old('sandwitch_leaves'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'sandwitch_leaves']) !!}
    @if($errors->has('sandwich_leaves'))
        <span class="help-block">
            {{ $errors->first('sandwich_leaves') }}
        </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('presents')) has-error @endif">
    {!! Form::label('presents', 'Presents', ['class' => 'control-label']) !!}
    {!! Form::text('presents', old('presents'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'presents']) !!}
    @if($errors->has('presents'))
        <span class="help-block">
            {{ $errors->first('presents') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('allowed_leaves')) has-error @endif">
    {!! Form::label('allowed_leaves', 'Allowed Leaves', ['class' => 'control-label']) !!}
    {!! Form::number('allowed_leaves', old('allowed_leaves'), ['class' => 'form-control' , 'id'=> 'allowed_leaves' ,'readonly' => 'true']) !!}
    @if($errors->has('allowed_leaves'))
        <span class="help-block">
            {{ $errors->first('allowed_leaves') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('wages')) has-error @endif">
    {!! Form::label('wages', 'Wages', ['class' => 'control-label']) !!}
    {!! Form::number('wages', old('wages'), ['class' => 'form-control' , 'id'=> 'wages' ,'readonly' => 'true']) !!}
    @if($errors->has('wages'))
        <span class="help-block">
            {{ $errors->first('wages') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('deducted_amount')) has-error @endif">
    {!! Form::label('deducted_amount', 'Deducted Amount', ['class' => 'control-label']) !!}
    {!! Form::text('deducted_amount', old('deducted_amount'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'deducted_amount']) !!}
    @if($errors->has('deducted_amount'))
        <span class="help-block">
            {{ $errors->first('deducted_amount') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('total_bonus')) has-error @endif">
    {!! Form::label('total_bonus', 'Total Bonus', ['class' => 'control-label']) !!}
    {!! Form::text('total_bonus', old('total_bonus'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'total_bonus']) !!}
    @if($errors->has('total_bonus'))
        <span class="help-block">
            {{ $errors->first('total_bonus') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('total_commission')) has-error @endif">
    {!! Form::label('total_commission', 'Total Commission', ['class' => 'control-label']) !!}
    {!! Form::text('total_commission', old('total_commission'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'total_commission']) !!}
    @if($errors->has('total_commission'))
        <span class="help-block">
            {{ $errors->first('total_commission') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('gross_total')) has-error @endif">
    {!! Form::label('gross_total', 'Gross Total', ['class' => 'control-label']) !!}
    {!! Form::text('gross_total', old('gross_total'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'gross_total']) !!}
    @if($errors->has('gross_total'))
        <span class="help-block">
            {{ $errors->first('gross_total') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('tax_percentage')) has-error @endif">
    {!! Form::label('tax_percentage', 'Tax %age', ['class' => 'control-label']) !!}
    {!! Form::text('tax_percentage', old('tax_percentage'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'tax_percentage']) !!}
    @if($errors->has('tax_percentage'))
        <span class="help-block">
            {{ $errors->first('tax_percentage') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('tax_amount')) has-error @endif">
    {!! Form::label('tax_amount', 'Tax Amount', ['class' => 'control-label']) !!}
    {!! Form::text('tax_amount', old('tax_amount'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'tax_amount']) !!}
    @if($errors->has('tax_amount'))
        <span class="help-block">
            {{ $errors->first('tax_amount') }}
        </span>
    @endif
</div>


<div class="form-group col-md-3 @if($errors->has('total_after_tax')) has-error @endif">
    {!! Form::label('total_after_tax', 'Total After Tax', ['class' => 'control-label']) !!}
    {!! Form::text('total_after_tax', old('total_after_tax'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'total_after_tax']) !!}
    @if($errors->has('total_after_tax'))
        <span class="help-block">
            {{ $errors->first('total_after_tax') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('paid_date')) has-error @endif" id="paid_date_div" style="display: none;">
    {!! Form::label('paid_date', 'Paid Date', ['class' => 'control-label']) !!}
    {!! Form::text('paid_date', old('paid_date'), ['class' => 'form-control datepicker' ,'id'=>'paid_date']) !!}
    @if($errors->has('paid_date'))
        <span class="help-block">
            {{ $errors->first('paid_date') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('cash_date')) has-error @endif" id="cash_date_div" style="display: none;">
    {!! Form::label('cash_date', 'Cash Date', ['class' => 'control-label']) !!}
    {!! Form::text('cash_date', old('cash_date'), ['class' => 'form-control datepicker' ,'id'=>'cash_date']) !!}
    @if($errors->has('cash_date'))
        <span class="help-block">
            {{ $errors->first('cash_date') }}
        </span>
    @endif
</div>





<div class="form-group col-md-3 @if($errors->has('salary_type')) has-error @endif" id="salary_type_div" style="display: none;">
        {!! Form::label('salary_type', 'Salary Type', ['class' => 'control-label']) !!}
        {!! Form::select('salary_type', array('' => 'Select Salary Type') + Config::get('hrm.salary_type_array') , old('salary_type'),
         ['class' => 'form-control select2'  ,'id' => 'salary_type']) !!}
    @if($errors->has('salary_type'))
        <span class="help-block">
            {{ $errors->first('salary_type') }}
        </span>
    @endif         
</div>


<div class="form-group col-md-3 @if($errors->has('paid_amount')) has-error @endif">
    {!! Form::label('paid_amount', 'Paid Amount', ['class' => 'control-label']) !!}
    {!! Form::number('paid_amount', old('paid_amount'), ['class' => 'form-control', 'id'=> 'paid_amount',
    'onchange' => 'FormControls.calculatePending();',
    'onkeydown' => 'FormControls.calculatePending();',
    'onkeyup' => 'FormControls.calculatePending();',
    'onblur' => 'FormControls.calculatePending();']) !!}
    @if($errors->has('paid_amount'))
        <span class="help-block">
            {{ $errors->first('paid_amount') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('pending_amount')) has-error @endif">
    {!! Form::label('pending_amount', 'Pending Amount', ['class' => 'control-label']) !!}
    {!! Form::text('pending_amount', old('pending_amount'), ['class' => 'form-control','readonly' => 'true' , 'id'=> 'pending_amount']) !!}
    @if($errors->has('pending_amount'))
        <span class="help-block">
            {{ $errors->first('pending_amount') }}
        </span>
    @endif
</div>



<div class="form-group col-md-3 @if($errors->has('payment_method')) has-error @endif" id="payment_method_div" style="display: none;">
        {!! Form::label('payment_method', 'Payment Method', ['class' => 'control-label']) !!}
        {!! Form::select('payment_method', array('' => 'Select Payment Method') + Config::get('hrm.payment_method_array') , old('payment_method'),
         ['class' => 'form-control select2'  ,'id' => 'payment_method']) !!}
    @if($errors->has('payment_method'))
        <span class="help-block">
            {{ $errors->first('payment_method') }}
        </span>
    @endif         
</div>

