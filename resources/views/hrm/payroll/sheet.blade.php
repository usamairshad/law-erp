@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Salary Sheet</h1>
    </section>

    {!! Form::open(['method' => 'POST', 'route' => ['hrm.payroll.payroll_report'], 'id' => 'validation-form']) !!}

   {{csrf_field()}}
    <input type="hidden" name="name" value="abc">
    <div class="form-group col-md-3 @if($errors->has('year')) has-error @endif">
        {!! Form::label('year', 'Year', ['class' => 'control-label']) !!}
        {!! Form::select('year', array('' => 'Select Year') + Config::get('hrm.year_array') , old('year'), ['class' => 'form-control', 'id'=> 'year']) !!}
        @if($errors->has('year'))
            <span class="help-block">
            {{ $errors->first('year') }}
        </span>
        @endif
    </div>

    <div class="form-group col-md-3 @if($errors->has('month')) has-error @endif">
        {!! Form::label('month', 'Month', ['class' => 'control-label']) !!}
        {!! Form::select('month',array('' => 'Select Month') + Config::get('hrm.month_array'), old('month'), ['class' => 'form-control', 'id'=> 'month']) !!}
        @if($errors->has('month'))
            <span class="help-block">
            {{ $errors->first('month') }}
        </span>
        @endif
    </div>

    {{--<button id="search_button" onclick="FormControls.fetchFilterRecord();" type="button" style="margin-top: 25px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search </b> </button>--}}
    <button id="search_button" style="margin-top: 25px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search </b> </button>
    {!! Form::close() !!}
@stop




@section('content')
    <div class="box box-primary">
        @if(isset($Reports))
            <div class="box-header">
                <i class="fa fa-user-circle"></i> <h3 class="box-title">Salary Sheet</h3>
            </div>
            <!-- /.box-header -->
            <div class="panel-body pad table-responsive">
                <table class="table table-striped" id="tablewithextensions" >
                    <thead>
                    <tr style="background: #606060; color: white;">
                        <th>Sr No</th>
                        <th>Name of Employee</th>
                        <th>Designation</th>
                        <th>Basic Salary</th>
                        <th>House Rent Allowance</th>
                        <th>Utilities</th>
                        <th>Gross Salary</th>
                        <th>Conveyance Allowance</th>
                        <th>Accommodation Allowance</th>
                        <th>Total</th>
                        <th>Arrears</th>
                        <th>Bonus/Incentives</th>
                        <th>Late Deductions</th>
                        <th>Taxable Salary</th>
                        <th>EOBI</th>
                        <th>Advance</th>
                        <th>I.Tax</th>
                        <th>Amount Payable</th>
                        <th>Comments</th>
                        <th>Bank Name</th>
                        <th>Bank Branch</th>
                        <th>A/C No</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($Reports)
                        <?php $counter = 0; $departments = array_keys($Reports); for($i=1; $i<17; $i++){unset($departments[count($Reports)-$i]);} ?>
                        @foreach($departments as $department)
                            <tr>
                                <td style='font-weight: 600;'>{{ $department }}</td>
                                @for($i=0; $i < 21; $i++)
                                    <td>&nbsp;</td>
                                @endfor
                            </tr>
                            @if(isset($Reports[$department]['data']))
                                @foreach($Reports[$department]['data'] as $employee)
                                    <tr>
                                        <td>{{ ++$counter }}</td>
                                        <td>{{ App\Models\Admin\Employees::getEmployeeNameByUserId($employee->user_id) }}</td>
                                        <td>{{ App\Models\Admin\Employees::getJobTitleByUserId($employee->user_id) }}</td>
                                        <td>{{ $employee->basic_salary }}</td>
                                        <td>{{ $employee->house_rent_allowance }}</td>
                                        <td>{{ $employee->utilities_allowance }}</td>
                                        <td>{{ \App\Models\Admin\Employees::getEmployeeByUserId($employee->user_id)->gross_salary }}</td>
                                        <td>{{ $employee->conveyance_allowance }}</td>
                                        <td>{{ $employee->accommodation_allowance }}</td>
                                        <td>{{ $employee->gross_salary + $employee->conveyance_allowance + $employee->accommodation_allowance }}</td>
                                        <td>{{ \App\Models\HRM\Payroll::getArearsByPayrollId($employee->id) }}</td>
                                        <td>{{ $employee->overtime }}</td>
                                        <td>{{ $employee->late_arrivals }}</td>
                                        <td>{{ \App\Models\HRM\Payroll::getTaxableSalaryByPayrollId($employee->id) }}</td>
                                        <td>{{ $employee->eobi }}</td>
                                        <td>{{ $employee->advance }}</td>
                                        <td>{{ $employee->income_tax }}</td>
                                        <td>{{ $employee->net_payable }}</td>
                                        <td>{{ $employee->comments }}</td>
                                        <td>{{ \App\Models\Admin\Employees::getEmployeeByUserId($employee->user_id)->bank_name }}</td>
                                        <td>{{ \App\Models\Admin\Employees::getEmployeeByUserId($employee->user_id)->branch_name }}</td>
                                        <td>{{ \App\Models\Admin\Employees::getEmployeeByUserId($employee->user_id)->account_number }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            <tr>
                                <td>Sub Total :</td>
                                <td style='font-weight: bold;'>{{ $Reports[$department]['extras']['sub_total'] }}</td>
                                @for($i=0; $i < 20; $i++)
                                    <td>&nbsp;</td>
                                @endfor
                                @endforeach
                            </tr>
                            <td>Total :</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['total'] }}</td>
                            <td></td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['basic_salary_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['house_rent_allowance_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['utilities_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['gross_salary_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['conveyance_allowance_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['accomodation_allowance_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['total_renumeration_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['arrears_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['bonus_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['late_deductions_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['taxable_salary_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['eobi_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['advance_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['income_tax_total'] }}</td>
                            <td style='font-weight: bold;'>Rs. {{ $Reports['amount_total'] }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            </tr>
                            @else
                                <div class="box-header text-center">
                                    No Record Found
                                </div>
                            @endif
                    </tbody>
                </table>
            </div>
        @else
            <div class="box-header text-center">
                Nothing to Show
            </div>
        @endif
    </div>
@stop



@section('javascript')
    @include('partials.datatables_extensions')
    <script src="{{ url('public/js/hrm/payroll/list.js') }}" type="text/javascript"></script>
@endsection