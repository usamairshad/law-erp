@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
<style>
.DTFC_LeftBodyLiner{
    top: -12px !important;
}
</style>

        {!! Form::open(['method' => 'POST', 'route' => ['hrm.payroll.payroll_report'], 'id' => 'validation-form']) !!}
        {{csrf_field()}}
        <section class="content-header" style="padding: 10px 15px !important;">
            <h1>Payroll Report</h1>
        </section>

        <div class="form-group col-md-3 @if($errors->has('month')) has-error @endif" id="month_div">
            {!! Form::label('month', 'Month *', ['class' => 'control-label']) !!}
            {!! Form::select('month', array('' => 'Choose a Month') + Config::get('hrm.month_array') , old('month'),
            ['class' => 'form-control select2'  ,'id' => 'month']) !!}
        </div>

        <div class="form-group col-md-3 @if($errors->has('year')) has-error @endif" id="year_div">
            {!! Form::label('year', 'Year *', ['class' => 'control-label']) !!}
            {!! Form::select('year', array('' => 'Choose an Year') + Config::get('hrm.year_array') , old('year'),
            ['class' => 'form-control select2'  ,'id' => 'year']) !!}
        </div>


        <div style="margin-right: 20px;">
            <button id="search_button" style="margin-top: 25px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search </b> </button>
        </div>
    {!! Form::close() !!}
@stop

@section('content')
    <div class="box box-primary">
            @if(isset($Reports))
                        <div class="box-header">
                            <i class="fa fa-user-circle"></i> <h3 class="box-title">Salary Sheet</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="panel-body pad table-responsive">
                            <table class="table table-striped" id="tablewithextensions" >
                                <thead>
                                    <tr style="background: #606060; color: white;">
                                        <th>Sr No</th>
                                        <th>Name of Employee</th>
                                        <th>Designation</th>
                                        <th>Basic Salary</th>
                                        <th>House Rent Allowance</th>
                                        <th>Utilities</th>
                                        <th>Gross Salary</th>
                                        {{--<th>Conveyance Allowance</th>--}}
                                        {{--<th>Accommodation Allowance</th>--}}
                                        {{--<th>Total</th>--}}
                                        {{--<th>Arrears</th>--}}
                                        {{--<th>Bonus/Incentives</th>--}}
                                        <th> Deductions</th>
                                        <th>Taxable Salary</th>
                                        <th>EOBI</th>
                                        <th>Advance</th>
                                        <th>I.Tax</th>
                                        <th>Amount Payable</th>
                                        {{--<th>Comments</th>--}}
                                        {{--<th>Bank Name</th>--}}
                                        {{--<th>Bank Branch</th>--}}
                                        {{--<th>A/C No</th>--}}
                                    </tr>
                                </thead>
                                <tbody>

                                    @if(isset($Reports))
                                        <?php $total_payable=0; ?>
                                        @foreach($Reports as $report)
                                            <tr>
                                                <td>{{$report->id}}</td>

                                                <td>{{$report->emp_name->first_name}}</td>
                                                <td>{{$report->emp_name->job_title}}</td>
                                                <td>{{$report->basic_salary}}</td>
                                                <td>{{$report->house_rent}}</td>
                                                <td>{{$report->utilities}}</td>
                                                <td>{{$report->gross_total + $report->deducted_amount }}</td>
                                                {{--<td>{{$report->id}}</td>--}}
                                                {{--<td>{{$report->id}}</td>--}}
                                                {{--<td>{{$report->id}}</td>--}}
                                                {{--<td>{{$report->id}}</td>--}}
                                                {{--<td>{{$report->id}}</td>--}}
                                                <td>{{$report->deducted_amount}}</td>
                                                <td>{{$report->gross_total}}</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>{{$report->tax_amount}}</td>
                                                <td>{{$report->total_after_tax}}</td>
                                                <?php $total_payable += $report->total_after_tax;?>
                                                {{--<td>{{$report->id}}</td>--}}
                                                {{--<td>{{$report->id}}</td>--}}
                                                {{--<td>{{$report->id}}</td>--}}
                                                {{--<td>{{$report->id}}</td>--}}

                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td> Total Payable </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td > {{$total_payable}} </td>
                                        </tr>

                                    @else
                                        <div class="box-header text-center">
                                            No Record Found
                                        </div>
                                    @endif
                                </tbody>
                            </table>
                        </div>
            @else
                <div class="box-header text-center">
                    Nothing to Show
                </div>
            @endif


    </div>

@stop

@section('javascript')
    @include('partials.datatables_extensions')
    <script>
        var FormControls = function(){
            //== Private functions
            $("#user_id").select2({
                placeholder: "Select Employee(s)"
            });

           

            var baseFunction = function () {
                $( "#validation-form" ).validate({
                    // define validation rules
                    errorElement: 'span',
                    errorClass: 'help-block',
                    rules: {
                        month: {
                            required: true
                        },
                        year: {
                            required: true
                        },
                    },
                    highlight: function (element) { // hightlight error inputs
                        $(element)
                            .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label.closest('.form-group').removeClass('has-error');
                        label.remove();
                    },
                });
            }

            return {
                // public functions
                init: function() {
                    baseFunction();
                }
            };

        }();

        $(document).ready(function(){
            FormControls.init();
        });
    </script>
@endsection