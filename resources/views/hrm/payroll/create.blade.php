@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Payroll</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Payroll</h3>
            <a href="{{ route('hrm.payroll.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->


        {!! Form::open(['method' => 'POST', 'route' => ['hrm.payroll.store'], 'id' => 'validation-form']) !!}
        <div class="box-body">
            @include('hrm.payroll.fields')
        </div>
        <!-- /.box-body -->


        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}

    </div>
@stop

@section('javascript')

    <script>
        function calculatePendingAmount(key){
            try {
                var charCode = (key.which) ? key.which : key.keyCode;
                if (charCode >= 48 && charCode <= 57) {
                    var pending_amount = Number($('#gross_total').val()) - Number($('#paid_amount').val());
                    console.log(pending_amount);
                    $('#pending_amount').val(pending_amount);
                }
            }
            catch(err) {
                console.log(err);
            }
        }
    </script>
    <script src="{{ url('public/js/hrm/payroll/create_modify.js') }}" type="text/javascript"></script>

@endsection

