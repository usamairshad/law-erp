@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Payroll</h1>
    </section>
   {{csrf_field()}}
    <input type="hidden" name="name" value="abc">
    <div class="form-group col-md-3 @if($errors->has('from_date')) has-error @endif" id="from_date_div">
        {!! Form::label('from_date', 'From Date', ['class' => 'control-label']) !!}
        {!! Form::text('from_date', old('from_date'),
         ['class' => 'form-control datepicker' ,'id'=>'from_date']) !!}
    </div>

    <div class="form-group col-md-3 @if($errors->has('to_date')) has-error @endif" id="to_date_div">
        {!! Form::label('to_date', 'To Date', ['class' => 'control-label']) !!}
        {!! Form::text('to_date', old('to_date'),
         ['class' => 'form-control datepicker' ,'id'=>'to_date']) !!}
    </div>
    <div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif" id="user_div">
        {!! Form::label('user_id', 'User ', ['class' => 'control-label']) !!}
        {!! Form::select('user_id', $Employees , old('user_id'),
         ['class' => 'form-control select2'  ,'id' => 'user_id']) !!}
    </div>

    <button id="search_button" onclick="FormControls.fetchFilterRecord();" type="button" style="margin-top: 25px;" class="btn  btn-sm btn-flat btn-primary"><b>&nbsp;Search </b> </button>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            @if(Gate::check('contacts_create'))
                <a href="{{ route('hrm.payroll.create') }}" class="btn btn-success pull-right">Add Salary</a>
                <button class="btn btn-info pull-right" id="myBtn">Calculate Payroll</button>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Payroll) > 0 ? 'datatable' : '' }}" id="users-table" >

                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Month</th>
                    <th>Year</th>
                    <th>Gross Total</th>
                    <th>Paid Date</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding:35px 50px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><span class="glyphicon glyphicon-lock"></span> Calculate Payroll</h4>
                </div>
                <div class="modal-body" style="padding:40px 50px;">
                    {!! Form::open(['method' => 'POST', 'route' => ['hrm.payroll.calculate_payroll'], 'id' => 'validation-form']) !!}


                    <div class="form-group col-md-6 @if($errors->has('salary_month')) has-error @endif" id="salary_month_div">
                        {!! Form::label('salary_month', 'Month', ['class' => 'control-label']) !!}
                        {!! Form::select('salary_month', array('' => 'Select a Month') + config('hrm.month_array'), old('salary_month'), ['class' => 'form-control'  ,'id' => 'salary_month']) !!}
                    </div>

                    <div class="form-group col-md-6 @if($errors->has('salary_year')) has-error @endif" id="salary_year_div">
                        {!! Form::label('salary_year', 'Year', ['class' => 'control-label']) !!}
                        {!! Form::select('salary_year', array('' => 'Select an Year') + config('hrm.year_array'), old('salary_year'), ['class' => 'form-control'  ,'id' => 'salary_year']) !!}
                    </div>

                    <button type="submit" class="btn btn-success btn-block"><span > <i class="fa fa-calculator"></i></span> Calculate</button>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>

                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@stop

@section('javascript') 

    <script src="{{ url('public/js/hrm/payroll/list.js') }}" type="text/javascript"></script>
@endsection