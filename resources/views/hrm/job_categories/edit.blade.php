@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Job Categories</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Job Category</h3>
            <a href="{{ route('hrm.job_categories.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($JobCategorie, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.job_categories.update', $JobCategorie->id]]) !!}
        <div class="box-body">
            @include('hrm.job_categories.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/js/hrm/job_categories/create_modify.js') }}" type="text/javascript"></script>
@endsection