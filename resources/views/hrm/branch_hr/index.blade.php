@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">HR Details</h3>
              
                <a href="{{ route('admin.branchhr.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New HR</a>  
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($hr) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>Branch Name</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>TimeStamp</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($hr) > 0)
                    @foreach ($hr as $Employee)
                        <tr data-entry-id="{{ $Employee->id }}">
                            <td>{{\App\Helpers\Helper::branchIdToName($Employee->branch_id)}}</td>
                            <td>{{ $Employee->name }}</td>
                          
                            <td>{{ $Employee->email }}</td>
                            <td>{{ $Employee->created_at }}</td>
                            <td>
                                  
                    
              
                                   <a href="{{ route('admin.branchhr.edit',[$Employee->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                        
                        
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.branchhr.destroy', $Employee->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                            
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
            $("#branch").append('<option selected disabled> Select </option>');
        });
    </script>
@endsection