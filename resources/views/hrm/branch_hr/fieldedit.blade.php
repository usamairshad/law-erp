<style>
    .form-group
    {
        float:left  !important;
    }
    </style>
<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
    <?php
    $id = Auth::user()->id;
    $branch_id =\App\User::where('id',$id)->pluck('branch_id');
    $branch_name = \App\Models\Admin\Branches::where('id',$branch_id)->value('name');
    $Branchname =\App\Models\Admin\Branches::pluck('name','id');
    ?>
    {!! Form::label('name', 'Branch Name', ['class' => 'control-label']) !!}
    {!! Form::select('branch_id', $Branchname,old('branch_name'), ['class' => 'form-control select2']) !!}
    @if($errors->has('name'))
        <span class="help-block">
        {{ $errors->first('name') }}
        </span>
    @endif
</div>
<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    <input type="text" name="name" value="{{$hr->name}}" class="form-control" required>  
</div>
<div class="form-group col-md-3 @if($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
     <input type="email" id="email"  name="email" value="{{$hr->email}}" class="form-control" required>
</div>
