<div class="form-group col-md-3 @if($errors->has('name')) has-error @endif" style ="float:left;">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('holiday_date')) has-error @endif" style ="float:left;">
        {!! Form::label('holiday_date', 'Date*', ['class' => 'control-label']) !!}
        {!! Form::text('holiday_date', old('holiday_date'), ['class' => 'form-control']) !!}
        @if($errors->has('holiday_date'))
                <span class="help-block">
                        {{ $errors->first('holiday_date') }}
                </span>
        @endif
</div>


<div class="form-group col-md-3 @if($errors->has('holiday_length')) has-error @endif" style ="float:left;">
        {!! Form::label('holiday_length', 'Holiday Length*', ['class' => 'control-label']) !!}
        {!! Form::select('holiday_length', array('' => 'Select a Length') + Config::get('hrm.working_full_half_array'), old('holiday_length'), ['class' => 'form-control']) !!}
        @if($errors->has('holiday_length'))
                <span class="help-block">
                        {{ $errors->first('holiday_length') }}
                </span>
        @endif
</div>
<div class="form-group col-md-3 @if($errors->has('is_recurring')) has-error @endif" style ="float:left;">
        {!! Form::label('is_recurring', 'Repeats Annually', ['class' => 'control-label']) !!}<br/>
        {{ Form::checkbox('is_recurring', 1, old('is_recurring')) }}
        @if($errors->has('is_recurring'))
                <span class="help-block">
                        {{ $errors->first('is_recurring') }}
                </span>
        @endif
</div>