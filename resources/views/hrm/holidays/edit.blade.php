@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Holiday</h3>
            <a href="{{ route('hrm.holidays.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($Holiday, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.holidays.update', $Holiday->id]]) !!}
        <div class="box-body" style = "margin-top:40px;">
            @include('hrm.holidays.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer" style ="padding-top:85px;">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <!-- bootstrap datepicker -->
    <script src="{{ url('adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/js/hrm/holidays/create_modify.js') }}" type="text/javascript"></script>
@endsection