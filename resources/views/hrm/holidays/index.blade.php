@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Holiday</h3>
            @if(Gate::check('holidays_create'))
            @endif
                <a href="{{ route('hrm.holidays.create') }}" style = "float:right;" class="btn btn-success pull-right">Add New Holiday</a>
           
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($Holidays) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Recurring?</th>
                    <th>Holiday Length</th>
                    <th width="15%">Create At</th>
                    <th width="15%">Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($Holidays) > 0)
                    @foreach ($Holidays as $Holiday)
                        <tr data-entry-id="{{ $Holiday->id }}">
                            <td>{{ $Holiday->id }}</td>
                            <td>{{ $Holiday->name }}</td>
                            <td>{{date('d-m-Y ', strtotime( $Holiday->holiday_date ))}}</td>
                            <td>@if($Holiday->is_recurring){{ 'Yes' }}@else{{ 'No' }}@endif</td>
                            <td>{{ Config::get('hrm.working_full_half_array')[$Holiday->holiday_length] }}</td>
                            <td>{{ date('d-m-Y h:m a ', strtotime($Holiday->created_at ))}}</td>
                            <td>
                                @if($Holiday->status && Gate::check('holidays_inactive'))
                                @elseif(!$Holiday->status && Gate::check('holidays_active'))
                                @endif
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['hrm.holidays.inactive', $Holiday->id])) !!}
                                    {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}
                          
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['hrm.holidays.active', $Holiday->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                              

                                @if(Gate::check('holidays_edit'))
                                @endif
                                    <a href="{{ route('hrm.holidays.edit',[$Holiday->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                              

                                @if(Gate::check('holidays_destroy'))
                                @endif
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['hrm.holidays.destroy', $Holiday->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                               
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection