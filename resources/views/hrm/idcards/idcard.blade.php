@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">ID Card Request</h3>


        <div class="panel-body table-responsive">

            <table class="table table-bBanked table-striped {{ count($data) > 0 ? 'datatable' : '' }} ">
                <thead>
                <tr>
                    <th>Branch Name</th>
                    <th>Employee Name</th>
                    <th>Employee Role</th>
                    <th>Status</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                    @if(count($data) > 0)        

                    @foreach($data as $record)
                    <tr>

                        <td>{{$record->branch_name}}</td>
                        <td>{{$record->name}}</td>
                        <td>{{$record->role}}</td>
                        <td>{{$record->status}}</td>
                        <td>{{$record->created_at}}</td>
                        <td> 
                            @if($record->status == 'approved')
                            <a href="{{url('approve-request',$record->id)}}" id="not-active" class="btn btn-success">
Approved
</a>
@else
<a href="{{url('approve-request',$record->id)}}" id="not-active" class="btn btn-primary">
Approve
</a>
                          @endif



                        

                    </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop







@section('javascript')


 <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>

@endsection