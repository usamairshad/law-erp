@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">ID Card Request for GM Finanace</h3>


        <div class="panel-body table-responsive">

            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                    <th>Branch Name</th>
                    <th>Employee Name</th>
                    <th>Employee Role</th>
                    <th>Status</th>
                    <th>Timestamp</th>
                </tr>
                </thead>
                <tbody>

                    @foreach($data as $record)
                    <tr>

                        <td>{{$record->branch_name}}</td>
                        <td>{{$record->name}}</td>
                        <td>{{$record->role}}</td>
                        <td>{{$record->status}}</td>
                        <td>{{$record->created_at}}</td>
                       


                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop







@section('javascript')


@endsection