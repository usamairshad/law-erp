@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Working Days</h3>
            @if(Gate::check('leave_periods_edit'))
            @endif
                <a href="{{ route('hrm.work_weeks.edit',[$WorkWeek->id]) }}" 
                    style = "float:right;" class="btn btn-success pull-right">Edit</a>
          
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tbody>
                <tr>
                    <th width="15%">Monday</th>
                    <td>@if(isset($WorkWeek->mon)){{ Config::get('hrm.working_full_array')[$WorkWeek->mon] }}@else{{'N/A'}}@endif</td>
                </tr>
                <tr>
                    <th width="15%">Tuesday</th>
                    <td>@if(isset($WorkWeek->tue)){{ Config::get('hrm.working_full_array')[$WorkWeek->tue] }}@else{{'N/A'}}@endif</td>
                </tr>
                <tr>
                    <th width="15%">Wednesday</th>
                    <td>@if(isset($WorkWeek->wed)){{ Config::get('hrm.working_full_array')[$WorkWeek->wed] }}@else{{'N/A'}}@endif</td>
                </tr>
                <tr>
                    <th width="15%">Thursday</th>
                    <td>@if(isset($WorkWeek->thu)){{ Config::get('hrm.working_full_array')[$WorkWeek->thu] }}@else{{'N/A'}}@endif</td>
                </tr>
                <tr>
                    <th width="15%">Friday</th>
                    <td>@if(isset($WorkWeek->fri)){{ Config::get('hrm.working_full_array')[$WorkWeek->fri] }}@else{{'N/A'}}@endif</td>
                </tr>
                <tr>
                    <th width="15%">Saturday</th>
                    <td>@if(isset($WorkWeek->sat)){{ Config::get('hrm.working_full_array')[$WorkWeek->sat] }}@else{{'N/A'}}@endif</td>
                </tr>
                <tr>
                    <th width="15%">Sunday</th>
                    <td>@if(isset($WorkWeek->sun)){{ Config::get('hrm.working_full_array')[$WorkWeek->sun] }}@else{{'N/A'}}@endif</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection