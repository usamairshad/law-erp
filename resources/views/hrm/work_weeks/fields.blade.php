<table class="table table-bordered table-striped">
        <tbody>
        <tr>
           <th width="15%">Monday</th>
           <td>{!! Form::select('mon', Config::get('hrm.working_full_array'), old('mon'), ['class' => 'form-control']) !!}</td>
           <td width="50%">&nbsp;</td>
        </tr>
        <tr>
           <th width="15%">Tuesday</th>
                <td>{!! Form::select('tue', Config::get('hrm.working_full_array'), old('tue'), ['class' => 'form-control']) !!}</td>
           <td width="50%">&nbsp;</td>
        </tr>
        <tr>
           <th width="15%">Wednesday</th>
                <td>{!! Form::select('wed', Config::get('hrm.working_full_array'), old('wed'), ['class' => 'form-control']) !!}</td>
           <td width="50%">&nbsp;</td>
        </tr>
        <tr>
           <th width="15%">Thursday</th>
                <td>{!! Form::select('thu', Config::get('hrm.working_full_array'), old('thu'), ['class' => 'form-control']) !!}</td>
           <td width="50%">&nbsp;</td>
        </tr>
        <tr>
           <th width="15%">Friday</th>
                <td>{!! Form::select('fri', Config::get('hrm.working_full_array'), old('fri'), ['class' => 'form-control']) !!}</td>
           <td width="50%">&nbsp;</td>
        </tr>
        <tr>
           <th width="15%">Saturday</th>
                <td>{!! Form::select('sat', Config::get('hrm.working_full_array'), old('sat'), ['class' => 'form-control']) !!}</td>
           <td width="50%">&nbsp;</td>
        </tr>
        <tr>
           <th width="15%">Sunday</th>
                <td>{!! Form::select('sun', Config::get('hrm.working_full_array'), old('sun'), ['class' => 'form-control']) !!}</td>
           <td width="50%">&nbsp;</td>
        </tr>
        </tbody>
</table>