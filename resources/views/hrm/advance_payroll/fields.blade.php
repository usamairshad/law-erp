{{csrf_field()}}
<div class="form-group col-md-3 @if($errors->has('user_id')) has-error @endif" id="user_div">
        {!! Form::label('user_id', 'User *', ['class' => 'control-label']) !!}
        {!! Form::select('user_id', $Employees , old('user_id'),
         ['class' => 'form-control select2'  ,'id' => 'user_id']) !!}
    @if($errors->has('user_id'))
        <span class="help-block">
            {{ $errors->first('user_id') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('month')) has-error @endif" id="month_div">
        {!! Form::label('month', 'Month *', ['class' => 'control-label']) !!}
        {!! Form::select('month', array('' => 'Select a Month') + Config::get('hrm.month_array') , old('month'),
         ['class' => 'form-control select2'  ,'id' => 'month']) !!}
    @if($errors->has('month'))
        <span class="help-block">
            {{ $errors->first('month') }}
        </span>
    @endif         
</div>

<div class="form-group col-md-3 @if($errors->has('year')) has-error @endif" id="year_div">
        {!! Form::label('year', 'Year *', ['class' => 'control-label']) !!}
        {!! Form::select('year', array('' => 'Select an Year') + Config::get('hrm.year_array') , old('year'),
         ['class' => 'form-control select2'  ,'id' => 'year']) !!}
    @if($errors->has('year'))
        <span class="help-block">
            {{ $errors->first('year') }}
        </span>
    @endif         
</div>

<div class="form-group col-md-3 @if($errors->has('salary_type')) has-error @endif" id="salary_div">
        {!! Form::label('salary_type', 'Salary Type *', ['class' => 'control-label']) !!}
        {!! Form::select('salary_type', array('' => 'Select a Type') + Config::get('hrm.salary_type_array') , old('year'),
         ['class' => 'form-control select2'  ,'id' => 'salary_type']) !!}
    @if($errors->has('salary_type'))
        <span class="help-block">
            {{ $errors->first('salary_type') }}
        </span>
    @endif         
</div>

<div class="form-group @if($errors->has('comments')) has-error @endif" id="comments_div">
    {!! Form::label('comments', 'Comments *', ['class' => 'control-label']) !!}
    {!! Form::textarea('comments',  old('name'), ['id' => 'comments', 'rows' => 6 , 'cols' => 30 ,'class' => 'form-control' , 'resize:none']) !!}
    
    @if($errors->has('comments'))
        <span class="help-block">
            {{ $errors->first('comments') }}
        </span>
    @endif
</div>



