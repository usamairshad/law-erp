@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Employee Advance Payroll</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Advance Payroll</h3>
            <a href="{{ route('hrm.employees_overtime.index') }}" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($AdvancePayroll, ['method' => 'PUT', 'enctype' => 'multipart/form-data' , 'id' => 'validation-form', 'route' => ['hrm.advance_payroll.update', $AdvancePayroll->id]]) !!}
        <div class="box-body">
            @include('hrm.advance_payroll.fields')
        </div>
        <!-- /.box-body -->
        {!! Form::hidden('id',  $AdvancePayroll->id  , ['id' => 'id'] ) !!}

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')

    <script src="{{ url('public/js/hrm/advance_payroll/create_modify.js') }}" type="text/javascript"></script>
@endsection