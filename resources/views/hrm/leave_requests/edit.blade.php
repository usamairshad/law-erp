@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Leave Request</h3>
            <a href="{{ route('hrm.leave_requests.index') }}" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($LeaveRequest, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['hrm.leave_requests.update', $LeaveRequest->id]]) !!}
        <div class="box-body">
            @include('hrm.leave_requests.fields')
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/js/hrm/leave_requests/create_modify.js') }}" type="text/javascript"></script>
@endsection