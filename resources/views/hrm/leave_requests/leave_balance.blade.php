<table class="table table-bordered">
        <tbody>
                <tr>
                        <th width="30%">Employee</th>
                        <td>{{ $Employee->first_name . ' ' . $Employee->last_name }}</td>
                </tr>
                <tr>
                        <th>Leave Type</th>
                        <td>{{ $LeaveType->name }}</td>
                </tr>
                <tr>
                        <th>As of Date</th>
                        <td>{{ \Carbon\Carbon::now()->format('Y-m-d') }}</td>
                </tr>
        </tbody>
</table>

<table class="table table-striped col-md-6">
        <tbody>
        <tr>
                <th width="70%">Entitled</th>
                <td>{{ $LeaveEntitlement->no_of_days }}</td>
        </tr>
        <tr>
                <th>Taken</th>
                <td>{{ $LeaveEntitlement->taken_count }}</td>
        </tr>
        <tr>
                <th>Scheduled</th>
                <td>{{$LeaveEntitlement->scheduled }}</td>
        </tr>
        <tr>
                <th>Pending Approval</th>
                <td>{{$LeaveEntitlement->pending_approval }}</td>
        </tr>
        <tr>
                <th>Balance</th>
                <th>{{ $LeaveType->permitted_days - $LeaveEntitlement->scheduled - $LeaveEntitlement->taken_count}}</th>
        </tr>
        </tbody>
</table>