@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Leaves Request</h3>
            @if(Gate::check('leave_requests_create'))
            @endif
                <a href="{{ route('hrm.leave_requests.create') }}" style ="float:right;" class="btn btn-success pull-right">Create Leave Request</a>
         
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($LeaveRequests) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Employee</th>
                    <th>Leave Type</th>
                    <th>Work Shift</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Total Days</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @if (count($LeaveRequests) > 0)
                    @foreach ($LeaveRequests as $LeaveRequest)
                        <tr data-entry-id="{{ $LeaveRequest->id }}">
                            <td>{{ $LeaveRequest->id }}</td>
                            <td>{{ $LeaveRequest->first_name }}</td>
                            <td>{{ $LeaveRequest->leave_type->name }}</td>
                            <td>{{ $LeaveRequest->work_shift->name }}</td>
                            <td>{{ date('d-m-Y  ', strtotime($LeaveRequest->start_date ))}}</td>
                            <td>{{ date('d-m-Y  ', strtotime($LeaveRequest->end_date ))}}</td>
                            <td>{{ $LeaveRequest->total_days }}</td>

                            <td>{{ date('d-m-Y  h:m a', strtotime($LeaveRequest->created_at ))}}</td>
                            <td>
                           

                                {{--@if(Gate::check('leave_requests_edit'))--}}
                                    <a href="{{ route('hrm.leave_requests.edit',[$LeaveRequest->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>

                                {{--@endif--}}

                                {{--@if(Gate::check('leave_requests_destroy'))--}}
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['hrm.leave_requests.destroy', $LeaveRequest->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                {{--@endif--}}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection