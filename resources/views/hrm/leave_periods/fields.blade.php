<div class="form-group col-md-4 @if($errors->has('start_date')) has-error @endif" style = "float:left;">
        {!! Form::label('start_date', 'Start Date*', ['class' => 'control-label']) !!}
        {!! Form::text('start_date', old('start_date'), ['id' => 'start_date', 'class' => 'form-control']) !!}
        @if($errors->has('start_date'))
                <span class="help-block">
                        {{ $errors->first('start_date') }}
                </span>
        @endif
</div>
<div class="form-group col-md-4 @if($errors->has('end_date')) has-error @endif" style = "float:left;">
        {!! Form::label('end_date', 'End Date*', ['class' => 'control-label']) !!}
        {!! Form::text('end_date', old('end_date'), ['readonly' => 'true', 'class' => 'form-control datepicker']) !!}
        @if($errors->has('end_date'))
                <span class="help-block">
                        {{ $errors->first('end_date') }}
                </span>
        @endif
</div>