@inject('request', 'Illuminate\Http\Request')
@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Leave Periods</h3>
            @if(Gate::check('leave_periods_edit'))
             @endif
                <a href="{{ route('hrm.leave_periods.edit',[$LeavePeriod->id]) }}" style = "float:right;" class="btn btn-success pull-right">Edit</a>
           
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th width="15%">Start Date</th>
                        <td width="35%">{{ $date_start->format('F j') }}</td>
                        <th width="15%">End Date</th>
                        <td width="35%">{{ $date_end->format('F j') }}</td>
                    </tr>
                    <tr>
                        <th>Current Leave Period</th>
                        <td colspan="3">{{ $date_start->format('F j, Y') }} to {{ $date_end->format('F j, Y') }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection