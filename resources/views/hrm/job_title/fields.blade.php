<div class="form-group @if($errors->has('department_id')) has-error @endif">
    <?php
    $DepartmentName =\App\Models\Admin\Departments::pluck('name','id');
    ?>
        {!! Form::label('department_id', 'Department Name', ['class' => 'control-label']) !!}
        {!! Form::select('department_id', $DepartmentName->prepend('Select Department', ''),old('department_id'), ['class' => 'form-control']) !!}
        @if($errors->has('department_id'))
                <span class="help-block">
                        {{ $errors->first('department_id') }}
                    </span>
        @endif
</div>
<div class="form-group @if($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
    @if($errors->has('name'))
        <span class="help-block">
            {{ $errors->first('name') }}
        </span>
    @endif
</div>