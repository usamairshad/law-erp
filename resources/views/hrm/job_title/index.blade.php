@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('breadcrumbs')
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Job Title</h1>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            {{--@if(Gate::check('job_title_create'))--}}
                <a href="{{ route('hrm.job_title.create') }}" class="btn btn-success pull-right">Add New Job Title</a>
            {{--@endif--}}
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped {{ count($JobTitle) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Create At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                <?php $Departments = \App\Models\Admin\Departments::get()->getDictionary();?>
                @if (count($JobTitle) > 0)
                    @foreach ($JobTitle as $JobTitle)
                        <tr data-entry-id="{{ $JobTitle->id }}">
                            <td>{{ $JobTitle->id }}</td>
                            <td>{{ $JobTitle->name }}</td>
                            <td>{{ $Departments[$JobTitle->department_id]->name }}</td>
                            <td>{{date('d-m-Y h:m a', strtotime( $JobTitle->created_at)) }}</td>
                            <td>
                                @if($JobTitle->id >=26)
                                @if($JobTitle->status && Gate::check('job_title_inactive'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['hrm.job_title.inactive', $JobTitle->id])) !!}
                                    {!! Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')) !!}
                                    {!! Form::close() !!}
                                @elseif(!$JobTitle->status && Gate::check('job_title_active'))
                                    {!! Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['hrm.job_title.active', $JobTitle->id])) !!}
                                    {!! Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')) !!}
                                    {!! Form::close() !!}
                                @endif

                                @if(Gate::check('job_title_edit'))
                                    <a href="{{ route('hrm.job_title.edit',[$JobTitle->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                                @endif

                                @if(Gate::check('job_title_destroy'))
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['hrm.job_title.destroy', $JobTitle->id])) !!}
                                    {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endif
                                @else
                                    No Action
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
@endsection