{{csrf_field()}}
<div class="form-group col-md-3 @if($errors->has('title')) has-error @endif" id="title_div">
    {!! Form::label('title', 'Title *', ['class' => 'control-label']) !!}
    {!! Form::text('title',  old('title'), ['id' => 'title' ,'class' => 'form-control']) !!}
    @if($errors->has('title'))
        <span class="help-block">
            {{ $errors->first('title') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('date')) has-error @endif" id="date_div">
    {!! Form::label('date', 'Date *', ['class' => 'control-label']) !!}
    {!! Form::text('date', old('date'), ['class' => 'form-control datepicker' ,'id'=>'date']) !!}
    @if($errors->has('date'))
        <span class="help-block">
            {{ $errors->first('date') }}
        </span>
    @endif
</div>

<div class="form-group col-md-3 @if($errors->has('workday_length')) has-error @endif" id="workday_div">
        {!! Form::label('workday_length', 'Workday Length *', ['class' => 'control-label']) !!}
        {!! Form::select('workday_length', array('' => 'Select Day Length') + Config::get('hrm.working_full_half_array') , old('workday_length'),
         ['class' => 'form-control select2'  ,'id' => 'workday_length']) !!}
    @if($errors->has('workday_length'))
        <span class="help-block">
            {{ $errors->first('workday_length') }}
        </span>
    @endif         
</div>

<div class="form-group col-md-3 @if($errors->has('applicable_to')) has-error @endif" id="applicable_div">
        {!! Form::label('applicable_to', 'Applicable To', ['class' => 'control-label']) !!}
        {!! Form::select('applicable_to', array('' => 'Everyone') + $Departments , old('applicable_to'),
         ['class' => 'form-control select2'  ,'id' => 'applicable_to']) !!}
    @if($errors->has('applicable_to'))
        <span class="help-block">
            {{ $errors->first('applicable_to') }}
        </span>
    @endif         
</div>