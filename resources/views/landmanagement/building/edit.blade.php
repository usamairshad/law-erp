
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Edit New Building</h3>

                
            <a href="{{ route('admin.asset_type.index') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Back</a>
        </div>


       {!! Form::open(['method' => 'POST', 'route' => ['update_building_list'], 'id' => 'validation-form' , 'enctype' => 'multipart/form-data']) !!} 
    
        <div class="box-body" style = margin-top:40px;>
        <div  id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  
            <form id="add_data">
                <div class="modal-body">
                    <div class="row">
                   
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Campus List</label>
                                <select class="form-control" name="branch_id" required>
                                    <option value="" disabled selected>--Select Campus --</option>

                                   <!--  {!! App\Models\Admin\Branches::branchList() !!} -->

                                    @foreach($Branches as $key => $branch)

                                        @if($branch->id==$data->branch_id)
                                        <option value="{{$branch->id}}" selected>{{$branch->name}}</option>

                                        @endif

                                        <option value="{{$branch->id}}">{{$branch->name}}</option>

                                    @endforeach


                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Agreement Starting Date</label>

                                <input type="hidden" name="building_id" value="{{$data->building_id}}">
                                <input class="form-control" type ="date" value="{{$data->aggreement_date}}"  name="agreement_start_date" id="message-text" required>  
                          
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Agreement Ending Date</label>
                                <input class="form-control" type ="date" value="{{$data->agreement_end_date}}"  name="agreement_end_date" id="message-text" required>
                          
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Increment%</label>
                                <input class="form-control" type ="number" value="{{$data->increment}}" placeholder ="Increment%" name="increment" id="message-text" required>
                          
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Tenure</label>
                                <input class="form-control" type ="number" value="{{$data->tenure}}" placeholder="Tenure" name="tenure" id="message-text" required>
                          
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Security Rent</label>
                                <input class="form-control" type ="number" value="{{$data->security}}" Placeholder = "Security Rent" name="security_rent" id="message-text" required>
                          
                            </div>
                        </div>
                    <div class="col-md-3">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Campus Rent</label>
                                <input class="form-control" type ="number" value="{{$data->basic_rent}}" Placeholder ="Campus Rent" name="campus_rent" id="message-text" required>
                          
                            </div>
                        </div>
                       
                     
                   
                    </div>
                    
                </div>
        

           

        <div class="box-footer" style ="padding-top:26px;">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>

@endsection

