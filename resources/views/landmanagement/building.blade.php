@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Building Agreement Details</h3>

                <a href="{{ route('add-building-list') }}" class="btn btn-success pull-right " style = "float:right; margin-bottom:20px;">Add New Building</a>
        </div>
{{--        <div class="col-md-12">--}}
{{--            @if(Session::has('message'))--}}
{{--                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>--}}
{{--            @endif--}}
{{--            @if(Session::has('alert-danger'))--}}
{{--                <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('alert-danger') }}</p>--}}
{{--            @endif--}}
{{--            <div class="messages"></div>--}}
{{--        </div>--}}
        <div class="panel-body table-responsive">
            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                <th>S: No</th>
                    <th>LandLord Name</th>
                    <th>Cnic</th>
                    <th>Contact</th>
                    <th>Campus Name</th>
                    <th>Agreement Start</th>
                    <th>Agreement End</th>
                    <th>Increment%</th>
                    <th>Tenure</th>
                    <th>Security Rent</th>
                    <th>Campus Rent</th>
                 
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="payee_table">
                @if (count($data) > 0)
                        @foreach ($data as $asset_type)
                            <tr data-entry-id="{{ $asset_type->id }}">
                                <td>{{ $asset_type->building_id }}</td>
                                <td>{{ $asset_type->vendor_name }}</td>
                                <td>{{ $asset_type->cnic }}</td>
                                <td>{{ $asset_type->phone }}</td>
                                <td>{{ $asset_type->name }}</td>
                                <td>{{ $asset_type->aggreement_date }}</td>
                                <td>{{ $asset_type->agreement_end_date }}</td>
                                
                                <td>{{ $asset_type->increment }}</td>
                                <td>{{ $asset_type->tenure }}</td>
                                <td>{{ $asset_type->security }}</td>
                                <td>{{ $asset_type->basic_rent }}</td>
                                <td style = "float:left;">

                                        <a href="{{ url('edit_building_list',$asset_type->id) }}"
                                            class="btn btn-xs btn-info">Edit</a>
                                   

                                        {!! Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['landlord-delete', $asset_type->id],
                                        ]) !!}
                                        {!! Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger'])
                                        !!}
                                        {!! Form::close() !!}  


                               
                                </td> 
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="12">@lang('global.app_no_entries_in_table')</td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>
    </div>
@stop
<div id="popup">

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

    $( document ).ready(function() {

        get_data();

    });
    function add_payee(){

        $.ajax({

            url: '{{url('/payee-list')}}',
            type: 'POST',
            data: $('#add_data').serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data){

                get_data();


            }

        })

    }
    function resetform(){

        $("#add_data").trigger("reset");
    }

    function get_data(){
        var serial_no = 1;
        $.ajax({

            url: '{{url('/fetch-payee-list')}}',
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data){
                console.log(data);
                var htmlData='';
                for(i in data){
                    htmlData+='<tr class="view" id="'+data[i].vendor_id+'">';
                    htmlData+='<td>'+serial_no+++'</td>';
                    htmlData+='<td class="text-center">'+data[i].vendor_name+'</td>';
                    htmlData+='<td>'+data[i].branches['name']+'</td>';
                    htmlData+='<td>'+data[i].phone+'</td>';
                    htmlData+='<td>'+data[i].email+'</td>';
                    htmlData+='<td>'+data[i].address+'</td>';
                    htmlData+='<td class="text-center">'+
                        '<a  data-toggle="modal" data-target="#exampleModal" onclick="edit_record('+data[i].id+')" class="action-links"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>'+
                        '<a   class="action-links"><i class="fa fa-trash-o" onclick="delete_record('+data[i].id+')" aria-hidden="true"></i></a>'+
                        '</td>';
                    htmlData+='</tr>';

                }
                $('#payee_table').html(htmlData);

            }

        })

    }
        function edit_record($id){

            $.ajax({
                url: '{{url('/payee-list')}}/'+$id+'/edit',
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data){

                    $("#add_data input[name~='payee_name']").val(data[0].payee_name);
                    $("#add_data input[name~='campus']").val(data[0].campus);
                    $("#add_data input[name~='phone']").val(data[0].phone);
                    $("#add_data input[name~='email']").val(data[0].email);
                    $("#add_data input[name~='id']").val(data[0].id);
                    $("#add_data textarea[name~='address']").val(data[0].address);
                    $("#add_data select[name~='branch_id']").val(data[0].branch_id);


                }


            })
        }



   


</script>

{{--            ajax request end--}}




@section('javascript')

@endsection