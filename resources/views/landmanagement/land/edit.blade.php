
@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Edit New Landlord</h3>

                
            <a href="{{ route('landlord-details') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Back</a>
        </div>


       {!! Form::open(['method' => 'POST', 'route' => ['landlord-update'], 'id' => 'validation-form' , 'enctype' => 'multipart/form-data']) !!} 
       @foreach ($data as $asset_types)
        <div class="box-body" style = margin-top:40px;>
        <div  id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  
            <form id="add_data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Payee Name</label>
                                <select class="form-control" name="payee_name" required>
                                    <option value="" disabled selected>--Select--</option>
                                    @foreach($vendorlist as $vendor)
                                    <option value = "{{$vendor->vendor_id}}">{{$vendor->vendor_name}}</option>
                                    @endforeach
                                    </select>
                               
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Campus List</label>
                                <select class="form-control" value = "{{$asset_types->branch_id}}" name="branch_id" required>
                                    <option value="" disabled selected>--Select--</option>

                                    {!! App\Models\Admin\Branches::branchList() !!}

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Email</label>
                                <input type="hidden" name="id"  value="{{$asset_types->id}}" class="form-control" id="recipient-name" required>
                            
                                <input type="text" name="email"  value="{{$asset_types->email}}" class="form-control" id="recipient-name" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Phone</label>
                                <input class="form-control" name="phone"  value="{{$asset_types->phone}}" id="message-text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Address</label>
                                <textarea name="address" class="form-control"  value="{{$asset_types->address}}" id="message-text" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
        

           

        <div class="box-footer" style ="padding-top:26px;">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>
        @endforeach
        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('public/adminlte') }}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ url('public/adminlte') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ url('public/js/admin/employees/create_modify.js') }}" type="text/javascript"></script>

@endsection

