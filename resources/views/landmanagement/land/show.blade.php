@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')
@section('stylesheet')
    <link rel="stylesheet" href="{{ url('public/adminlte') }}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
@stop
@section('breadcrumbs')

@section('content')
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">LandLord Details</h3>

                
            <a href="{{ route('admin.asset_type.index') }}" style = "float:right; margin-bottom:20px;"  class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Name</th>
                    <td>{{ $asset_type->name}}</td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td>{{ $asset_type->description}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{ $asset_type->status }}</td>
                    
                </tr>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
@endsection
