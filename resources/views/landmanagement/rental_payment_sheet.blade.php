@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">Rental Payment Sheet</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>


        <div class="panel-body table-responsive">

            <table class="table table-bBanked table-striped {{--{{ count($Country) > 0 ? 'datatable' : '' }}--}} dt-select">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Campus Name</th>
                    <th>Code</th>
                    <th>Payee</th>
                    <th>Campus Rent</th>
                    <th>Gross Rent PRE Adjust</th>
                    <th>Adjustments</th>
                    <th>Gross Rent AFT Adjust</th>
                    <th>Tax</th>
                    <th>Net Amount</th>
                    <th>Remarks</th>

                </tr>
                </thead>

                <tbody id="payee_table">
                <tr>
                    <td>1</td>
                    <td>Net Roots</td>
                    <td>1002</td>
                    <td>haider</td>
                    <td>21000</td>
                    <td>2100</td>
                    <td>5000</td>
                    <td>10000</td>
                    <td>1500</td>
                    <td>21000</td>
                    <td>Good Amount</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Net Roots</td>
                    <td>1002</td>
                    <td>haider</td>
                    <td>21000</td>
                    <td>2100</td>
                    <td>5000</td>
                    <td>10000</td>
                    <td>1500</td>
                    <td>21000</td>
                    <td>Good Amount</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Net Roots</td>
                    <td>1002</td>
                    <td>haider</td>
                    <td>21000</td>
                    <td>2100</td>
                    <td>5000</td>
                    <td>10000</td>
                    <td>1500</td>
                    <td>21000</td>
                    <td>Good Amount</td>
                </tr>


{{--                <tr>--}}
{{--                    <td colspan="3">@lang('global.app_no_entries_in_table')</td>--}}
{{--                </tr>--}}

                </tbody>
            </table>
        </div>
    </div>
@stop







@section('javascript')

@endsection