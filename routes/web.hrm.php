<?php
/**
 * Created by PhpStorm.
 * User: mustafa.mughal
 * Date: 2/9/2018
 * Time: 11:38 AM
 */

/*
 * ***************************************************
 * **********     HR MOdule Start       **************
 * ***************************************************
 */
Route::group(['middleware' => ['auth'], 'prefix' => 'hrm', 'as' => 'hrm.'], function () {

    // Leave Performance Reports starts here
    Route::get('leave_performance_report', ['uses' => 'HRM\Reports\HrReportsController@leavePerformanceIndex', 'as' => 'leave_performance.report']);
    Route::post('leave_performance_report', ['uses' => 'HRM\Reports\HrReportsController@getLeavePerformanceReport', 'as' => 'leave_performance.post']);
    Route::post('leave_performance_report_pdf', ['uses' => 'HRM\Reports\HrReportsController@getLeavePerformanceReportPDF', 'as' => 'leave_performance.report.pdf']);


    // Attendance Reports starts here
    Route::get('attendance_report', ['uses' => 'HRM\Reports\HrReportsController@index', 'as' => 'attendance.report']);
    Route::post('attendance_report', ['uses' => 'HRM\Reports\HrReportsController@getSimpleReport', 'as' => 'attendance.post']);
    Route::get('attendance_report_detail/{id}', ['uses' => 'HRM\Reports\HrReportsController@detail', 'as' => 'attendance.report.detail']);
    Route::get('attendance_reportpdf/{id}', ['uses' => 'HRM\Reports\HrReportsController@pdfReport', 'as' => 'attendance_report.detail.pdf']);

    //Leave Reports starts here
    Route::get('leave_report', ['uses' => 'HRM\Reports\HrReportsController@leaveindex', 'as' => 'leave.report']);
    Route::post('leave_report', ['uses' => 'HRM\Reports\HrReportsController@getSimpleLeaveReport', 'as' => 'leave.post']);
    Route::get('leave_report_detail/{id}', ['uses' => 'HRM\Reports\HrReportsController@leavedetail', 'as' => 'leave.report.detail']);
    Route::get('leave_reportpdf/{id}', ['uses' => 'HRM\Reports\HrReportsController@leavePdfReport', 'as' => 'leave_report.detail.pdf']);

    // Pay Grades Resrouce Starts Here
    Route::patch('pay_grades/active/{id}', ['uses' => 'HRM\PayGradesController@active', 'as' => 'pay_grades.active']);
    Route::patch('pay_grades/inactive/{id}', ['uses' => 'HRM\PayGradesController@inactive', 'as' => 'pay_grades.inactive']);
    Route::resource('pay_grades', 'HRM\PayGradesController');
    // Pay Grades Resrouce Ends Here
    // Pay Roll Resrouce Starts Here
    Route::patch('pay_roll/active/{id}', ['uses' => 'HRM\PayRollController@active', 'as' => 'pay_roll.active']);
    Route::patch('pay_roll/inactive/{id}', ['uses' => 'HRM\PayRollController@inactive', 'as' => 'pay_roll.inactive']);
    Route::resource('pay_roll', 'HRM\PayRollController');
    // Pay Grades Resrouce Ends Here

    // Employment Statuses Resrouce Starts Here
    Route::patch('employment_statuses/active/{id}', ['uses' => 'HRM\EmploymentStatusesController@active', 'as' => 'employment_statuses.active']);
    Route::patch('employment_statuses/inactive/{id}', ['uses' => 'HRM\EmploymentStatusesController@inactive', 'as' => 'employment_statuses.inactive']);
    Route::resource('employment_statuses', 'HRM\EmploymentStatusesController');
    // Employment Statuses Resrouce Ends Here

    // Job Categories Resrouce Starts Here
    Route::patch('job_categories/active/{id}', ['uses' => 'HRM\JobCategoriesController@active', 'as' => 'job_categories.active']);
    Route::patch('job_categories/inactive/{id}', ['uses' => 'HRM\JobCategoriesController@inactive', 'as' => 'job_categories.inactive']);
    Route::resource('job_categories', 'HRM\JobCategoriesController');
    // Job Categories Resrouce Ends Here

    // Job Categories Resrouce Starts Here
    Route::patch('job_title/active/{id}', ['uses' => 'HRM\JobTitleController@active', 'as' => 'job_title.active']);
    Route::patch('job_title/inactive/{id}', ['uses' => 'HRM\JobTitleController@inactive', 'as' => 'job_title.inactive']);
    Route::resource('job_title', 'HRM\JobTitleController');
    // Job Categories Resrouce Ends Here

    // Work Shifts Resrouce Starts Here
    Route::patch('work_shifts/active/{id}', ['uses' => 'HRM\WorkShiftsController@active', 'as' => 'work_shifts.active']);
    Route::patch('work_shifts/inactive/{id}', ['uses' => 'HRM\WorkShiftsController@inactive', 'as' => 'work_shifts.inactive']);
    Route::resource('work_shifts', 'HRM\WorkShiftsController');
    // Work Shifts Resrouce Ends Here

    // Organizations Resrouce Starts Here
    Route::patch('organizations/active/{id}', ['uses' => 'HRM\OrganizationsController@active', 'as' => 'organizations.active']);
    Route::patch('organizations/inactive/{id}', ['uses' => 'HRM\OrganizationsController@inactive', 'as' => 'organizations.inactive']);
    Route::resource('organizations', 'HRM\OrganizationsController');
    // Organizations Resrouce Ends Here

    // Organization Locations Resrouce Starts Here
    Route::patch('organization_locations/active/{id}', ['uses' => 'HRM\OrganizationLocationsController@active', 'as' => 'organization_locations.active']);
    Route::patch('organization_locations/inactive/{id}', ['uses' => 'HRM\OrganizationLocationsController@inactive', 'as' => 'organization_locations.inactive']);
    Route::resource('organization_locations', 'HRM\OrganizationLocationsController');
    // Organization Locations Resrouce Ends Here

    // Skills Resrouce Starts Here
    Route::patch('skills/active/{id}', ['uses' => 'HRM\SkillsController@active', 'as' => 'skills.active']);
    Route::patch('skills/inactive/{id}', ['uses' => 'HRM\SkillsController@inactive', 'as' => 'skills.inactive']);
    Route::resource('skills', 'HRM\SkillsController');
    // Skills Resrouce Ends Here

    // Educations Resrouce Starts Here
    Route::patch('educations/active/{id}', ['uses' => 'HRM\EducationsController@active', 'as' => 'educations.active']);
    Route::patch('educations/inactive/{id}', ['uses' => 'HRM\EducationsController@inactive', 'as' => 'educations.inactive']);
    Route::resource('educations', 'HRM\EducationsController');
    // Educations Resrouce Ends Here

    // Licenses Resrouce Starts Here
    Route::patch('licenses/active/{id}', ['uses' => 'HRM\LicensesController@active', 'as' => 'licenses.active']);
    Route::patch('licenses/inactive/{id}', ['uses' => 'HRM\LicensesController@inactive', 'as' => 'licenses.inactive']);
    Route::resource('licenses', 'HRM\LicensesController');
    // Licenses Resrouce Ends Here

    // Languages Resrouce Starts Here
    Route::patch('languages/active/{id}', ['uses' => 'HRM\LanguagesController@active', 'as' => 'languages.active']);
    Route::patch('languages/inactive/{id}', ['uses' => 'HRM\LanguagesController@inactive', 'as' => 'languages.inactive']);
    Route::resource('languages', 'HRM\LanguagesController');
    // Languages Resrouce Ends Here

    // Memberships Resrouce Starts Here
    Route::patch('memberships/active/{id}', ['uses' => 'HRM\MembershipsController@active', 'as' => 'memberships.active']);
    Route::patch('memberships/inactive/{id}', ['uses' => 'HRM\MembershipsController@inactive', 'as' => 'memberships.inactive']);
    Route::resource('memberships', 'HRM\MembershipsController');
    // Memberships Resrouce Ends Here

    // Leave Periods Resrouce Starts Here
    Route::resource('leave_periods', 'HRM\LeavePeriodsController');
    // Leave Periods Resrouce Ends Here

    // Leave Types Resrouce Starts Here
    Route::patch('leave_types/active/{id}', ['uses' => 'HRM\LeaveTypesController@active', 'as' => 'leave_types.active']);
    Route::patch('leave_types/inactive/{id}', ['uses' => 'HRM\LeaveTypesController@inactive', 'as' => 'leave_types.inactive']);
    Route::resource('leave_types', 'HRM\LeaveTypesController');
    // Leave Types Resrouce Ends Here

    // Work Weeks Resrouce Starts Here
    Route::resource('work_weeks', 'HRM\WorkWeeksController');
    // Work Weeks Resrouce Ends Here

    // Holidays Resrouce Starts Here
    Route::patch('holidays/active/{id}', ['uses' => 'HRM\HolidaysController@active', 'as' => 'holidays.active']);
    Route::patch('holidays/inactive/{id}', ['uses' => 'HRM\HolidaysController@inactive', 'as' => 'holidays.inactive']);
    Route::resource('holidays', 'HRM\HolidaysController');
    // Holidays Resrouce Ends Here

    // Leave Entitlements Resrouce Starts Here
    Route::get('leave_entitlements/employee_search', ['uses' => 'HRM\LeaveEntitlementsController@employee_search', 'as' => 'leave_entitlements.employee_search']);
    Route::resource('leave_entitlements', 'HRM\LeaveEntitlementsController');
    // Leave Entitlements Resrouce Ends Here

    // Leave Requests Resrouce Starts Here
    Route::get('leave_requests/employee_search', ['uses' => 'HRM\LeaveRequestsController@employee_search', 'as' => 'leave_requests.employee_search']);
    Route::get('leave_requests/leave_balance', ['uses' => 'HRM\LeaveRequestsController@leave_balance', 'as' => 'leave_requests.leave_balance']);
    Route::patch('leave_requests/active/{id}', ['uses' => 'HRM\LeaveRequestsController@active', 'as' => 'leave_requests.active']);
    Route::patch('leave_requests/inactive/{id}', ['uses' => 'HRM\LeaveRequestsController@inactive', 'as' => 'leave_requests.inactive']);
    Route::resource('leave_requests', 'HRM\LeaveRequestsController');
    // Leave Requests Resrouce Ends Here

    // Leaves  Resrouce Starts Here
    Route::get('leaves/employee_available', ['uses' => 'HRM\LeavesController@employee_available', 'as' => 'leaves.employee_available']);
    Route::post('leaves/available_result', ['uses' => 'HRM\LeavesController@available_result', 'as' => 'leaves.available_result']);
    Route::get('leaves/employee_search', ['uses' => 'HRM\LeavesController@employee_search', 'as' => 'leaves.employee_search']);
    Route::get('leaves/leave_balance', ['uses' => 'HRM\LeavesController@leave_balance', 'as' => 'leaves.leave_balance']);
    Route::patch('leaves/active/{id}', ['uses' => 'HRM\LeavesController@active', 'as' => 'leaves.active']);
    Route::patch('leaves/inactive/{id}', ['uses' => 'HRM\LeavesController@inactive', 'as' => 'leaves.inactive']);
    Route::post('leaves/change_status', ['uses' => 'HRM\LeavesController@change_status', 'as' => 'leaves.change_status']);
    Route::get('leaves/leave_cron/{date}', ['uses' => 'HRM\LeavesController@leave_cron', 'as' => 'leaves.leave_cron']);

    Route::get('leaves/approved', ['uses' => 'HRM\LeavesController@approved', 'as' => 'leaves.approved']);

    Route::resource('leaves', 'HRM\LeavesController');
    // Leaves  Resrouce Ends Here



    // Employees Resrouce Starts Here
    Route::patch('employees/active/{id}', ['uses' => 'HRM\EmployeesController@active', 'as' => 'employees.active']);

    Route::patch('employees/inactive/{id}', ['uses' => 'HRM\EmployeesController@inactive', 'as' => 'employees.inactive']);
    Route::resource('employees', 'HRM\EmployeesController');
    // Employees Resrouce Ends Here

    Route::get('attendance/sync_attendance', ['uses' => 'HRM\EmployeeAttendanceController@syncAttendance', 'as' => 'attendance.sync']);

    Route::get('attendance/requests', ['uses' => 'HRM\EmployeeAttendanceController@requests', 'as' => 'attendance.requests']);
    Route::post('attendance/delete', ['uses' => 'HRM\EmployeeAttendanceController@delete', 'as' => 'attendance.delete']);
    Route::get('attendance/active', ['uses' => 'HRM\EmployeeAttendanceController@active', 'as' => 'attendance.active']);
    Route::post('attendance/inactive', ['uses' => 'HRM\EmployeeAttendanceController@inactive', 'as' => 'attendance.inactive']);
    Route::post('attendance/datatables', ['uses' => 'HRM\EmployeeAttendanceController@datatables', 'as' => 'attendance.datatables']);
    Route::post('attendance/datatables1', ['uses' => 'HRM\EmployeeAttendanceController@datatables1', 'as' => 'attendance.datatables1']);
    Route::get('attendance/details/{id}', ['uses' => 'HRM\EmployeeAttendanceController@details', 'as' => 'attendance.details']);
    Route::get('attendance/transfer', ['uses' => 'HRM\EmployeeAttendanceController@transfer', 'as' => 'attendance.transfer']);
    Route::post('attendance/transfer_datatables', ['uses' => 'HRM\EmployeeAttendanceController@transfer_datatables', 'as' => 'attendance.transfer_datatables']);
    Route::post('attendance/transfer_attendance', ['uses' => 'HRM\EmployeeAttendanceController@transfer_attendance', 'as' => 'attendance.transfer_attendance']);
    Route::resource('attendance', 'HRM\EmployeeAttendanceController');

   // Route::resource('attendance', 'HRM\EmployeeAttendanceController');

    Route::post('payroll/delete', ['uses' => 'HRM\PayrollController@destroy', 'as' => 'payroll.delete']);
    Route::post('payroll/active', ['uses' => 'HRM\PayrollController@active', 'as' => 'payroll.active']);
    Route::post('payroll/inactive', ['uses' => 'HRM\PayrollController@inactive', 'as' => 'payroll.inactive']);
    Route::post('payroll/datatables', ['uses' => 'HRM\PayrollController@datatables', 'as' => 'payroll.datatables']);
    Route::get('payroll/details/{id}', ['uses' => 'HRM\PayrollController@details', 'as' => 'payroll.details']);
    Route::post('payroll/getEmployeeDetail', ['uses' => 'HRM\PayrollController@getEmployeeDetail', 'as' => 'payroll.getEmployeeDetail']);
    Route::post('payroll/transfer_datatables', ['uses' => 'HRM\PayrollController@transfer_datatables', 'as' => 'payroll.transfer_datatables']);
    Route::post('payroll/transfer_payroll', ['uses' => 'HRM\PayrollController@transfer_payroll', 'as' => 'payroll.transfer_payroll']);
    Route::get('salary_sheet', ['uses' => 'HRM\PayrollController@salary_sheet', 'as' => 'payroll.salary_sheet']);
    Route::post('payroll_report', ['uses' => 'HRM\PayrollController@payroll_report', 'as' => 'payroll.payroll_report']);
    Route::post('calculate_payroll', ['uses' => 'HRM\PayrollController@calculate_payroll', 'as' => 'payroll.calculate_payroll']);

    Route::resource('payroll', 'HRM\PayrollController');

    Route::post('payroll/get_bank_name', ['uses' => 'HRM\PayrollController@getBankName', 'as' => 'payroll.get_bank_name']);


    // Leave Types Resrouce Starts Here
    Route::patch('attendance_settings/active/{id}', ['uses' => 'HRM\HrmAttendanceSettingsController@active', 'as' => 'attendance_settings.active']);
    Route::patch('attendance_settings/inactive/{id}', ['uses' => 'HRM\HrmAttendanceSettingsController@inactive', 'as' => 'attendance_settings.inactive']);
    Route::resource('attendance_settings', 'HRM\HrmAttendanceSettingsController');
    // Leave Types Resrouce Ends Here

    // Employee Resrouce Starts Here
    Route::post('employee_resources/delete', ['uses' => 'HRM\EmployeeResourcesController@delete', 'as' => 'employee_resources.delete']);
    Route::post('employee_resources/active', ['uses' => 'HRM\EmployeeResourcesController@active', 'as' => 'employee_resources.active']);
    Route::post('employee_resources/inactive', ['uses' => 'HRM\EmployeeResourcesController@inactive', 'as' => 'employee_resources.inactive']);
    Route::post('employee_resources/datatables', ['uses' => 'HRM\EmployeeResourcesController@datatables', 'as' => 'employee_resources.datatables']);
    Route::get('employee_resources/details/{id}', ['uses' => 'HRM\EmployeeResourcesController@details', 'as' => 'employee_resources.details']);
    Route::post('employee_resources/getEmployeeDetail', ['uses' => 'HRM\EmployeeResourcesController@getEmployeeDetail', 'as' => 'employee_resources.getEmployeeDetail']);
    Route::post('employee_resources/transfer_datatables', ['uses' => 'HRM\EmployeeResourcesController@transfer_datatables', 'as' => 'employee_resources.transfer_datatables']);
    Route::post('employee_resources/transfer_employee_resources', ['uses' => 'HRM\EmployeeResourcesController@transfer_employee_resources', 'as' => 'employee_resources.transfer_employee_resources']);
    Route::resource('employee_resources', 'HRM\EmployeeResourcesController');


    // Employee Banks Starts Here
    Route::post('employee_banks/delete', ['uses' => 'HRM\EmployeeBanksController@delete', 'as' => 'employee_banks.delete']);
    Route::post('employee_banks/active', ['uses' => 'HRM\EmployeeBanksController@active', 'as' => 'employee_banks.active']);
    Route::post('employee_banks/inactive', ['uses' => 'HRM\EmployeeBanksController@inactive', 'as' => 'employee_banks.inactive']);
    Route::post('employee_banks/datatables', ['uses' => 'HRM\EmployeeBanksController@datatables', 'as' => 'employee_banks.datatables']);
    Route::get('employee_banks/details/{id}', ['uses' => 'HRM\EmployeeBanksController@details', 'as' => 'employee_banks.details']);
    Route::post('employee_banks/getEmployeeDetail', ['uses' => 'HRM\EmployeeBanksController@getEmployeeDetail', 'as' => 'employee_banks.getEmployeeDetail']);
    Route::post('employee_banks/transfer_datatables', ['uses' => 'HRM\EmployeeBanksController@transfer_datatables', 'as' => 'employee_banks.transfer_datatables']);
    Route::post('employee_banks/transfer_employee_banks', ['uses' => 'HRM\EmployeeBanksController@transfer_employee_banks', 'as' => 'employee_banks.transfer_employee_banks']);
    Route::resource('employee_banks', 'HRM\EmployeeBanksController');

    // Employee Resrouce Starts Here
    Route::post('gratuity/delete', ['uses' => 'HRM\GratuityController@delete', 'as' => 'gratuity.delete']);
    Route::post('gratuity/active', ['uses' => 'HRM\GratuityController@active', 'as' => 'gratuity.active']);
    Route::post('gratuity/inactive', ['uses' => 'HRM\GratuityController@inactive', 'as' => 'gratuity.inactive']);
    Route::post('gratuity/datatables', ['uses' => 'HRM\GratuityController@datatables', 'as' => 'gratuity.datatables']);
    Route::get('gratuity/details/{id}', ['uses' => 'HRM\GratuityController@details', 'as' => 'gratuity.details']);
    Route::post('gratuity/getGratuityDetail', ['uses' => 'HRM\GratuityController@getGratuityDetail', 'as' => 'gratuity.getGratuityDetail']);
    Route::post('gratuity/transfer_datatables', ['uses' => 'HRM\GratuityController@transfer_datatables', 'as' => 'gratuity.transfer_datatables']);
    Route::post('gratuity/transfer_gratuity', ['uses' => 'HRM\GratuityController@transfer_gratuity', 'as' => 'gratuity.transfer_gratuity']);
    Route::resource('gratuity', 'HRM\GratuityController');



    // Employee Resrouce Starts Here
    Route::post('employee_target/delete', ['uses' => 'HRM\EmployeeTargetController@delete', 'as' => 'employee_target.delete']);
    Route::post('employee_target/active', ['uses' => 'HRM\EmployeeTargetController@active', 'as' => 'employee_target.active']);
    Route::post('employee_target/inactive', ['uses' => 'HRM\EmployeeTargetController@inactive', 'as' => 'employee_target.inactive']);
    Route::post('employee_target/datatables', ['uses' => 'HRM\EmployeeTargetController@datatables', 'as' => 'employee_target.datatables']);
    Route::get('employee_target/details/{id}', ['uses' => 'HRM\EmployeeTargetController@details', 'as' => 'employee_target.details']);
    Route::post('employee_target/getEmployeeDetail', ['uses' => 'HRM\EmployeeTargetController@getEmployeeDetail', 'as' => 'employee_target.getEmployeeDetail']);
    Route::post('employee_target/transfer_datatables', ['uses' => 'HRM\EmployeeTargetController@transfer_datatables', 'as' => 'employee_target.transfer_datatables']);
    Route::post('employee_target/transfer_employee_target', ['uses' => 'HRM\EmployeeTargetController@transfer_employee_target', 'as' => 'employee_target.transfer_employee_target']);
    Route::resource('employee_target', 'HRM\EmployeeTargetController');



    // Employee Resrouce Starts Here

    Route::post('assign_target/ytdValue', ['uses' => 'HRM\AssignTargetController@ytdValue', 'as' => 'assign_target.ytdValue']);
    Route::get('assign_target/ytd', ['uses' => 'HRM\AssignTargetController@ytd', 'as' => 'assign_target.ytd']);
    Route::post('assign_target/delete', ['uses' => 'HRM\AssignTargetController@delete', 'as' => 'assign_target.delete']);
    Route::post('assign_target/active', ['uses' => 'HRM\AssignTargetController@active', 'as' => 'assign_target.active']);
    Route::post('assign_target/inactive', ['uses' => 'HRM\AssignTargetController@inactive', 'as' => 'assign_target.inactive']);
    Route::post('assign_target/datatables', ['uses' => 'HRM\AssignTargetController@datatables', 'as' => 'assign_target.datatables']);
    Route::get('assign_target/details/{id}', ['uses' => 'HRM\AssignTargetController@details', 'as' => 'assign_target.details']);
    Route::post('assign_target/getEmployeeDetail', ['uses' => 'HRM\AssignTargetController@getEmployeeDetail', 'as' => 'assign_target.getEmployeeDetail']);
    Route::post('assign_target/transfer_datatables', ['uses' => 'HRM\AssignTargetController@transfer_datatables', 'as' => 'assign_target.transfer_datatables']);
    Route::post('assign_target/transfer_assign_target', ['uses' => 'HRM\AssignTargetController@transfer_assign_target', 'as' => 'assign_target.transfer_assign_target']);
    Route::resource('assign_target', 'HRM\AssignTargetController');


    // Employee Resrouce Starts Here
    Route::post('employee_tada/delete', ['uses' => 'HRM\EmployeeTadaController@delete', 'as' => 'employee_tada.delete']);
    Route::post('employee_tada/active', ['uses' => 'HRM\EmployeeTadaController@active', 'as' => 'employee_tada.active']);
    Route::post('employee_tada/inactive', ['uses' => 'HRM\EmployeeTadaController@inactive', 'as' => 'employee_tada.inactive']);
    Route::post('employee_tada/datatables', ['uses' => 'HRM\EmployeeTadaController@datatables', 'as' => 'employee_tada.datatables']);
    Route::get('employee_tada/details/{id}', ['uses' => 'HRM\EmployeeTadaController@details', 'as' => 'employee_tada.details']);
    Route::post('employee_tada/getEmployeeDetail', ['uses' => 'HRM\EmployeeTadaController@getEmployeeDetail', 'as' => 'employee_tada.getEmployeeDetail']);
    Route::post('employee_tada/transfer_datatables', ['uses' => 'HRM\EmployeeTadaController@transfer_datatables', 'as' => 'employee_tada.transfer_datatables']);
    Route::post('employee_tada/transfer_employee_tada', ['uses' => 'HRM\EmployeeTadaController@transfer_employee_tada', 'as' => 'employee_tada.transfer_employee_tada']);
    Route::resource('employee_tada', 'HRM\EmployeeTadaController');

    // Employee Resrouce Starts Here
    Route::post('employees_tada_amount/delete', ['uses' => 'HRM\EmployeesTadaAmountController@delete', 'as' => 'employee_tada.delete']);
    Route::post('employees_tada_amount/active', ['uses' => 'HRM\EmployeesTadaAmountController@active', 'as' => 'employees_tada_amount.active']);
    Route::post('employees_tada_amount/inactive', ['uses' => 'HRM\EmployeesTadaAmountController@inactive', 'as' => 'employees_tada_amount.inactive']);
    Route::post('employees_tada_amount/datatables', ['uses' => 'HRM\EmployeesTadaAmountController@datatables', 'as' => 'employees_tada_amount.datatables']);
    Route::get('employees_tada_amount/details/{id}', ['uses' => 'HRM\EmployeesTadaAmountController@details', 'as' => 'employees_tada_amount.details']);
    Route::post('employees_tada_amount/getEmployeeDetail', ['uses' => 'HRM\EmployeesTadaAmountController@getEmployeeDetail', 'as' => 'employees_tada_amount.getEmployeeDetail']);
    Route::post('employees_tada_amount/transfer_datatables', ['uses' => 'HRM\EmployeesTadaAmountController@transfer_datatables', 'as' => 'employees_tada_amount.transfer_datatables']);
    Route::post('employees_tada_amount/transfer_employees_tada_amount', ['uses' => 'HRM\EmployeesTadaAmountController@transfer_employees_tada_amount', 'as' => 'employees_tada_amount.transfer_employees_tada_amount']);
    Route::resource('employees_tada_amount', 'HRM\EmployeesTadaAmountController');

    // Employee Resrouce Starts Here
    Route::post('employees_suggestion/delete', ['uses' => 'HRM\EmployeesSuggestionController@delete', 'as' => 'employee_tada.delete']);
    Route::post('employees_suggestion/active', ['uses' => 'HRM\EmployeesSuggestionController@active', 'as' => 'employees_suggestion.active']);
    Route::post('employees_suggestion/inactive', ['uses' => 'HRM\EmployeesSuggestionController@inactive', 'as' => 'employees_suggestion.inactive']);
    Route::post('employees_suggestion/datatables', ['uses' => 'HRM\EmployeesSuggestionController@datatables', 'as' => 'employees_suggestion.datatables']);
    Route::get('employees_suggestion/details/{id}', ['uses' => 'HRM\EmployeesSuggestionController@details', 'as' => 'employees_suggestion.details']);
    Route::post('employees_suggestion/getEmployeeDetail', ['uses' => 'HRM\EmployeesSuggestionController@getEmployeeDetail', 'as' => 'employees_suggestion.getEmployeeDetail']);
    Route::post('employees_suggestion/transfer_datatables', ['uses' => 'HRM\EmployeesSuggestionController@transfer_datatables', 'as' => 'employees_suggestion.transfer_datatables']);
    Route::post('employees_suggestion/transfer_employees_suggestion', ['uses' => 'HRM\EmployeesSuggestionController@transfer_employees_suggestion', 'as' => 'employees_suggestion.transfer_employees_suggestion']);
    Route::resource('employees_suggestion', 'HRM\EmployeesSuggestionController');

    //Employee Overtime Resource Starts Here
    Route::resource('employees_overtime','HRM\EmployeesOvertimeController');
    Route::post('employees_overtime/datatables', ['uses' => 'HRM\EmployeesOvertimeController@datatables', 'as' => 'employees_overtime.datatables']);
    Route::post('employees_overtime/getEmployeeDetail', ['uses' => 'HRM\EmployeesOvertimeController@getEmployeeDetail', 'as' => 'employees_overtime.getEmployeeDetail']);
    Route::post('employees_overtime/{id}/delete', ['uses' => 'HRM\EmployeesOvertimeController@destroy', 'as' => 'employees_overtime.destroy']);

    //Employee Document Types Resource Starts Here
    Route::post('employee_document_types/datatables', ['uses' => 'HRM\EmployeeDocumentTypesController@datatables', 'as' => 'employee_document_types.datatables']);
    Route::resource('employee_document_types', 'HRM\EmployeeDocumentTypesController');
    Route::post('employee_document_types/{id}/delete', ['uses' => 'HRM\EmployeeDocumentTypesController@destroy', 'as' => 'employee_document_types.destroy']);
    //Employee Document Types Resource Ends Here

    //Employee Documents Resource Starts Here
    Route::post('employee_documents/datatables', ['uses' => 'HRM\EmployeeDocumentsController@datatables', 'as' => 'employee_documents.datatables']);
    Route::resource('employee_documents', 'HRM\EmployeeDocumentsController');
    Route::post('employee_documents/{id}/delete', ['uses' => 'HRM\EmployeeDocumentsController@destroy', 'as' => 'employee_documents.destroy']);
    Route::get('employee_documents/{id}/view', ['uses' => 'HRM\EmployeeDocumentsController@view', 'as' => 'employee_documents.view']);
    //Employee Documents Resource Ends Here

    //Advance Payroll Resource Starts Here
    Route::post('advance_payroll/datatables', ['uses' => 'HRM\AdvancePayrollController@datatables', 'as' => 'advance_payroll.datatables']);
    Route::resource('advance_payroll', 'HRM\AdvancePayrollController');
    Route::post('advance_payroll/{id}/delete', ['uses' => 'HRM\AdvancePayrollController@destroy', 'as' => 'advance_payroll.destroy']);
    //Advance Payroll Resource Ends Here

    //Workdays Resrouce Starts Here
    Route::post('workdays/{id}/delete', ['uses' => 'HRM\WorkdaysController@destroy', 'as' => 'workdays.destroy']);
    Route::post('workdays/datatables', ['uses' => 'HRM\WorkdaysController@datatables', 'as' => 'workdays.datatables']);
    Route::post('workdays/active', ['uses' => 'HRM\WorkdaysController@active', 'as' => 'workdays.active']);
    Route::post('workdays/inactive', ['uses' => 'HRM\WorkdaysController@inactive', 'as' => 'workdays.inactive']);
    Route::resource('workdays', 'HRM\WorkdaysController');
    //Workdays Resrouce Ends Here

    //Employee Working Days Starts Here

    Route::get('employee_working_days', ['uses' => 'HRM\EmployeeWorkingDaysController@index', 'as' => 'employee_working_days']);
    Route::post('employee_working_days/employee_in_outs', ['uses' => 'HRM\EmployeeWorkingDaysController@getEmployeeInOuts', 'as' => 'employee_working_days.employee_in_outs']);
    Route::post('employee_working_days/fetch_filter_records', ['uses' => 'HRM\EmployeeWorkingDaysController@fetchFilterRecords', 'as' => 'employee_working_days.fetch_filter_records']);
    Route::post('employee_working_days/change_attendance_type', ['uses' => 'HRM\EmployeeWorkingDaysController@changeAttendanceType', 'as' => 'employee_working_days.change_attendance_type']);

    //Employee Working Days Ends Here

    //Employee Kpis starts here
    Route::post('employee_kpis/delete', ['uses' => 'HRM\EmployeeKpisController@delete', 'as' => 'employee_kpis.delete']);
    Route::post('employee_kpis/active', ['uses' => 'HRM\EmployeeKpisController@active', 'as' => 'employee_kpis.active']);
    Route::post('employee_kpis/inactive', ['uses' => 'HRM\EmployeeKpisController@inactive', 'as' => 'employee_kpis.inactive']);
    Route::get('employee_kpis/details/{id}', ['uses' => 'HRM\EmployeeKpisController@details', 'as' => 'employee_kpis.details']);
    Route::post('employee_kpis/datatables', ['uses' => 'HRM\EmployeeKpisController@datatables', 'as' => 'employee_kpis.datatables']);
    Route::resource('employee_kpis', 'HRM\EmployeeKpisController');

    //Employee KPIs Ends Here

});

/*
 * ***************************************************
 * **********     HR MOdule End       **************
 * ***************************************************
 */