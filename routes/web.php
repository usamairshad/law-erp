<?php

use App\Models\Admin\Groups;
//use App\Http\Middleware\profileActive;
//Route::get('/admin/script', function () {
//
//    $onlySoftDeleted = Groups::onlyTrashed()->get();
//    foreach ($onlySoftDeleted as $groupp) {
//        $groupp->forceDelete();
//    }
//    $onlySoftDeleted->forceDelete();
//    dd($onlySoftDeleted);
//    Groups::whereNotNull('deleted_at')->forceDelete();
//
//    $groups = Groups::all();
//    //dd($groups);
//    $str = "";

//    $i = 1;

//    foreach ($groups as $group) {
//        $str .= $i . "=>
//           [
//           " . "'id'=>" . "'" . $group->id . "',
//           " . "'name'=>" . "'" . $group->name . "',
//           " . "'number'=>" . "'" . $group->number . "',
//           " . "'code'=>" . "'" . $group->code . "',
//           " . "'level'=>" . "'" . $group->level . "',
//           " . "'parent_id'=>" . "'" . $group->parent_id . "',
//           " . "'account_type_id'=>" . "'" . $group->account_type_id . "',
//           " . "'status'=>" . "'" . $group->status . "',
//           " . "'created_by'=>" . "'" . $group->created_by . "',
//           " . "'updated_by'=>" . "'" . $group->updated_by . "',
//           " . "'deleted_by'=>" . "'" . $group->deleted_by . "',
//           " . "'created_at'=>" . "'" . $group->created_at . "',
//           " . "'updated_at'=>" . "'" . $group->updated_at . "',
//           " . "'deleted_at'=>" . "'" . $group->deleted_at . "',
//           " . "'parent_type'=>" . "'" . $group->parent_type . "',
//           ],
//           ";
//        $i++;
//    }
//    print_r("<pre>" . $str . "</pre>");
//
//
//    die;
//    // array_push($mainArray, $groupArray);
//
//});


Route::resource('report-group','Reports\ReportGroupController');
Route::resource('report-parameter', 'Reports\ReportParameterController');
Route::resource('student-report','Reports\StudentReportController');


Route::get('/leave_request_notification','Notifications\Notification_Controller@leave_request_count_notification');

//Utility Bills
Route::resource('utility-bills','Utilitybills\UtilityBillsController');
// Staff Profile Edit
Route::get('/edit-staff/{id}', 'HRM\StaffController@editStaffView');
Route::post('/update-staff-profile/{id}', 'HRM\StaffController@updateProfile');

Route::get('/move-to-sos/{id}', 'HRM\StaffController@moveToSos');
Route::get('/remove-to-sos/{id}', 'HRM\StaffController@reMoveToSos');   

// GM Finance Dashboard
Route::get('/gm-finance-dashboard', 'gmfinancedashboard\GmFinanceDashboardController@index');


//child benefit config
Route::resource('child/benefit-config','Admin\ChildBenefitConfigController');

//child benefit student
Route::resource('child/benefit-student','Admin\ChildBenefitStudentController');
//salry deduction
Route::resource('salary-deduction','Admin\SalaryDeductionController');
//loan
Route::resource('loan','Admin\LoanController');
Route::get('/loan-role-related-user', 'Admin\LoanController@RoleRelatedUser');
//active session section
Route::resource('active-session-section', 'Admin\ActiveSessionSectionController');

//EOBI Ghauri CRUD
// Route::resource('eobi','Admin\EobiController');
// Route::get('/eobi-role-related-user', 'Admin\EobiController@RoleRelatedUser');
// Route::get('/report_eobi','Admin\EobiController@report_eobi');
// Route::get('/eobi-company','Admin\EobiController@eobi_company');
// Route::get('/eobi-branch','Admin\EobiController@eobi_branch');
// Route::get('/eobi-staff','Admin\EobiController@eobi_staff');
//PF
Route::resource('incrementPf','Admin\IncrementPfController');
Route::get('/pf-role-related-user', 'Admin\IncrementPfController@RoleRelatedUser');
Route::get('/pf-user', 'Admin\IncrementPfController@staff');
// Land Management
Route::resource('/payee-list', 'LandManagement\LandManagementController');
Route::get('landlord-details', 'LandManagement\LandManagementController@index')->name('landlord-details');
Route::get('details/{id}', 'LandManagement\LandManagementController@show')->name('details');

Route::get('add-land-lord', 'LandManagement\LandManagementController@create')->name('add-land-lord');
Route::post('landlord-store', 'LandManagement\LandManagementController@store')->name('landlord-store');
Route::get('update_landlord/{id}', 'LandManagement\LandManagementController@edit')->name('update-landlord');
Route::post('landlord-update}', 'LandManagement\LandManagementController@update')->name('landlord-update');

Route::delete('landlord-delete/{id}', 'LandManagement\LandManagementController@destroy')->name('landlord-delete');

Route::get('rental-tax-settings', 'Admin\RentSettingsController@index')->name('rental-tax-settings');
Route::get('RentalTaxSettings/create', 'Admin\RentSettingsController@create')->name('rental-tax-settings-create');
Route::post('RentalTaxSettings/store', 'Admin\RentSettingsController@store')->name('rental-tax-settings-store');






// Building Management
Route::get('building-list', 'LandManagement\BuildingManagementController@index')->name('building-list');
Route::get('add-building-list', 'LandManagement\BuildingManagementController@create')->name('add-building-list');
Route::post('store-building-list', 'LandManagement\BuildingManagementController@store')->name('store-building-list');
Route::get('/fetch-building-list', 'LandManagement\BuildingManagementController@fetch');

Route::get('edit_building_list/{id}','LandManagement\BuildingManagementController@edit_building_list');

Route::post('update_building_list','LandManagement\BuildingManagementController@update_building_list')->name('update_building_list');

//Route::get('define-building', 'LandManagement\BuildingManagementController@create')->name('');



//For Remove payee_id
Route::get('/remove-payee-id', 'LandManagement\BuildingManagementController@delete_payee');

// Rental Payment Sheet
Route::get('/rental_payment_sheet',function (){return view('landmanagement.rental_payment_sheet');});





//Attendace Details Form



Route::get('/student_attendance_form', 'Payroll\AttendanceController@student_attendance_form');



//Attendace Details Form





	

Route::post('/attendance_details_list', 'Payroll\AttendanceController@get_student_attendance_detail_list')->name('attendance_details_list');
Route::get('admin/staff_status_list', 'Payroll\AttendanceController@get_staff_list')->name('staff_status_list');

Route::get('/staff_sos-active/{id}', 'Payroll\AttendanceController@staff_status_active')->name('staff_status_active');
Route::get('/staff_sos-inactive/{id}', 'Payroll\AttendanceController@staff_status_inactive')->name('staff_status_inactive');

Route::get('/terminated_staff_list', 'Payroll\AttendanceController@terminatedStaffList')->name('terminated_staff_list');


//Category Policy
Route::resource('/category-policy', 'RulesAndRegulations\CategoryPolicyController');

//Code of Conduct
Route::resource('/code-of-conduct', 'RulesAndRegulations\CodeOfConductController');

//Policy
Route::resource('/policy', 'RulesAndRegulations\PolicyController');

//Complete Rules and Policy
Route::get('/complete-rule-policy', 'RulesAndRegulations\PolicyController@completerolespolicy');
Route::get('/related-code-of-conduct/{id}', 'RulesAndRegulations\PolicyController@relatedCodeofConduct');


//Return Policy
Route::resource('/return-policy', 'Asset\ReturnPolicyController');
Route::get('/status-inactive/{id}', 'Asset\ReturnPolicyController@status_inactive');
Route::get('/status-active/{id}', 'Asset\ReturnPolicyController@status_active');

//Admin Companies
Route::get('companies-list/{id}', 'Admin\CompanyController@companies_detail');



//Dashboard
Route::get('/dashboard', 'Dashboard\DashboardController@index');
Route::get('/hr-dashboard', 'Dashboard\DashboardController@hr_dashboard');
Route::get('/gmdashboard', 'Dashboard\DashboardController@gm')->name('gmdashboard');
Route::get('/branch-id', 'Dashboard\DashboardController@board_branches');
Route::get('/board-id', 'Dashboard\DashboardController@boards');
Route::get('/program-id', 'Dashboard\DashboardController@programs');
Route::get('/total-companies', 'Dashboard\DashboardController@total_companies');
Route::get('/total-branches', 'Dashboard\DashboardController@total_branches');
Route::get('/total-boards', 'Dashboard\DashboardController@total_boards');
Route::get('/total-buildings', 'Dashboard\DashboardController@total_buildings');
Route::get('/total-programs', 'Dashboard\DashboardController@total_programs');
Route::get('/total-staffs', 'Dashboard\DashboardController@total_staffs');
Route::get('/total-students', 'Dashboard\DashboardController@total_students');
Route::get('/total-courses', 'Dashboard\DashboardController@total_courses');
Route::get('/related-branch/{id}', 'Dashboard\DashboardController@related_branch');
Route::get('/related-program/{id}', 'Dashboard\DashboardController@related_program');
Route::get('/related-board/{id}', 'Dashboard\DashboardController@related_board');
Route::get('/relate-programs/{id}', 'Dashboard\DashboardController@relate_programs');
Route::get('/student-count/{id}', 'Dashboard\DashboardController@studentCount');

Route::get('/get_all_staff', 'Dashboard\DashboardController@getAllStaff');

//Request ID Card
Route::get('/request-idcard', 'HRM\IdCards\IdCardController@idCard');
Route::get('/request-idcard_gm', 'HRM\IdCards\IdCardController@id_card_requests_gm');
Route::get('/approve-card-request', 'HRM\IdCards\IdCardController@approveCard');
Route::get('/approve-request/{id}', 'HRM\IdCards\IdCardController@approvRequest');

//Staff Filter
Route::post('/filter-staff', 'Dashboard\DashboardController@filter_staff');


//Agenda
Route::resource('/agenda', 'Agenda\AgendaController');
Route::get('/agenda-list', 'Agenda\AgendaController@agendaList');
Route::post('/agenda-approve', 'Agenda\AgendaController@agendaapprove');



//EPF By Amir

Route::get('/epf_eobi_staff','HRM\EpfController@index');
Route::get('/epf_applied_staff','HRM\EpfController@epf_applied_staff');
Route::get('/give_epf/{id}','HRM\EpfController@give_epf');

//EOBI By Amir

Route::resource('eobi_notification','HRM\EobiController');
Route::post('eobi_notification/govt_reg_id','HRM\EobiController@govt_reg_id');
Route::get('/eobi_applied_staff','HRM\EobiController@eobi_applied_staff');
	
//Salary Sheet

Route::get('salary_sheet_create','HRM\StaffController@salary_sheet_create');
Route::get('salary_sheet_approval','HRM\StaffController@salary_sheet_approval_index');

Route::get('/salary_sheet_approval_view/{sheet_code}','HRM\StaffController@salary_sheet_approval_view');



Route::get('/salary_sheet_approval_accountant/{sheet_code}','HRM\StaffController@salary_sheet_approval_accountant');
Route::get('/salary_sheet_approval_dgm/{sheet_code}','HRM\StaffController@salary_sheet_approval_dgm');
Route::get('/salary_sheet_approval_gm/{sheet_code}','HRM\StaffController@salary_sheet_approval_gm');
Route::get('/salary_sheet_approval_ceo/{sheet_code}','HRM\StaffController@salary_sheet_approval_ceo');





Route::get('request_loan_employee_view','HRM\StaffController@request_loan_employee_view');

Route::get('eobi_epf_register','HRM\StaffController@eobi_epf_register_view');
Route::get('staff_attendance_report','HRM\StaffController@staff_attendance_report');



Route::get('loan_request_approve_hr/{id}','HRM\StaffController@loan_request_approve_hr');
Route::get('index','Admin\AttendanceController@index');
Route::get('attendance-report-monthly','Admin\AttendanceController@monthly_report');
Route::get('attendance-report-monthly-details','Admin\AttendanceController@get_monthly_report')->name('attendance-report-monthly-details');
Route::get('attendance-report-summary','Admin\AttendanceController@summary_report');
Route::get('loan_request_reject_hr/{id}','HRM\StaffController@loan_request_reject_hr');
Route::get('loan_request_approve_ceo/{id}','HRM\StaffController@loan_request_approve_ceo');
Route::get('loan_request_reject_ceo/{id}','HRM\StaffController@loan_request_reject_ceo');


Route::post('/agenda-reject/{id}', 'Agenda\AgendaController@agendaReject');
Route::post('/schedule-date-time/{id}', 'Agenda\AgendaController@schedule_date_time');

//Asset Transaction
Route::resource('/asset-transaction', 'Asset\AssetTransactionController');

//Assign Asset
Route::resource('/asset-assign', 'Asset\AssignAssetController');
// Admin create user
Route::resource('/create-user', 'Admin\UserCreateController');
Route::get('security_refund_register', 'Admin\AccountReportsController@security_refund_register')->name('security-refund-register');

Route::get('income_tax/{id}', 'Admin\AccountReportsController@income_tax');
Route::get('Salary_Slip', 'Admin\AccountReportsController@SalarySlip');
Route::post('security_refund_register_report', 'Admin\AccountReportsController@security_refund_register_report')->name('security-refund-register-report-save');
Route::get('recon_with_fee_department', 'Admin\AccountReportsController@recon_with_fee_department')->name('recon-with-fee-department');
//236i Tax Calculation
Route::get('tax_calculation_236', 'Admin\AccountReportsController@tax_calculation_236')->name('tax-calculation-236');
Route::get('tax_calculation_236_report', 'Admin\AccountReportsController@tax_calculation_236_report')->name('tax-calculation-236-report');


// Tax Define
Route::get('tax_law', 'Admin\LawController@index')->name('tax-law');
Route::get('tax_law_add', 'Admin\LawController@create')->name('tax-law-add');
Route::post('tax_law_store', 'Admin\LawController@store')->name('tax-law-store');
Route::get('tax_law_edit/{id}', 'Admin\LawController@edit')->name('tax-law-edit');
Route::post('tax_law_update', 'Admin\LawController@update')->name('tax-law-update');
Route::delete('destroy_law/{id}', 'Admin\LawController@destroy')->name('destroy-law');

// Tax Law Rates
Route::get('tax_law_rate', 'Admin\LawRateController@index')->name('tax-law-rate');
Route::get('tax_law_rate_add', 'Admin\LawRateController@create')->name('tax-law-rate-add');
Route::post('tax_law_rate_store', 'Admin\LawRateController@store')->name('tax-law-rate-store');

Route::get('tax_law_rate_edit/{id}', 'Admin\LawRateController@edit')->name('tax-law-rate-edit');
Route::post('tax_law_rate_update', 'Admin\LawRateController@update')->name('tax-law-rate-update');
Route::delete('destroy_rate/{id}', 'Admin\LawRateController@destroy')->name('destroy-rate');


Route::get('vendor-list', 'Admin\VendorController@index')->name('vendor-list');
Route::get('vendor_create', 'Admin\VendorController@create')->name('vendor-create');
Route::get('show/{id}', 'Admin\VendorController@show')->name('vendor-shows');
Route::get('edit/{id}', 'Admin\VendorController@edit')->name('vendor-edit');
Route::post('vendor_update', 'Admin\VendorController@update')->name('vendor-update');
Route::post('vendor_store', 'Admin\VendorController@store')->name('vendor-store');
Route::get('vendor_delete/{id}', 'Admin\VendorController@destroy')->name('vendor-delete');
Route::get('vendor_fetch', 'Admin\VendorController@fetch')->name('vendor-fetch');

Route::get('loan-list', 'Admin\LoanTypeController@index')->name('loan-list');
Route::get('loan_create', 'Admin\LoanTypeController@create')->name('loan-create');
Route::get('loan_edit/{id}', 'Admin\LoanTypeController@edit')->name('loan-edit');
Route::post('loan_update', 'Admin\LoanTypeController@update')->name('loan-update');
Route::post('loan_store', 'Admin\LoanTypeController@store')->name('loan-store');
Route::get('loan_delete/{id}', 'Admin\LoanTypeController@destroy')->name('loan-delete');

Route::get('loan-installment-list', 'Admin\LoanInstallmentController@index')->name('loan-installment-list');
Route::get('loan_installment_create', 'Admin\LoanInstallmentController@create')->name('loan-installment-create');
Route::get('loan_installment_edit/{id}', 'Admin\LoanInstallmentController@edit')->name('loan-installment-edit');
Route::post('loan_installment_update', 'Admin\LoanInstallmentController@update')->name('loan-installment-update');
Route::post('loan_installment_store', 'Admin\LoanInstallmentController@store')->name('loan-installment-store');
Route::get('loan_installment_delete/{id}', 'Admin\LoanInstallmentController@destroy')->name('loan-installment-delete');

// nominal Reports Links
Route::get('nominal-report', 'Academics\ReportsController@nominal_report')->name('nominal-report');
Route::post('nominal-report-print', 'Academics\ReportsController@nominal_report_print')->name('nominal-report-print');

// student contact Reports Links
Route::get('student-contact', 'Academics\ReportsController@student_contact')->name('student-contact');
Route::post('student-contact-print', 'Academics\ReportsController@student_contact_report_print')->name('student-contact-report-print');

// student Address Reports Links
Route::get('student-address', 'Academics\ReportsController@student_address')->name('student-address');
Route::post('student-address-print', 'Academics\ReportsController@student_address_report_print')->name('student-address-report-print');

// Defaulter Student Reports Links
Route::get('default-student-report', 'Academics\ReportsController@default_student_report')->name('default-student-report');

Route::post('default-student-report-print', 'Academics\ReportsController@default_student_report_print')->name('default-student-report-print');



// Defaulter Amt Reports Links
Route::get('defaulter-amt-report', 'Academics\ReportsController@defaulter_amt_report')->name('defaulter-amt-report');

Route::post('defaulter-amt-report-print', 'Academics\ReportsController@default_amt_report_print')->name('defaulter-amt-report-print');


// Summary Reports Links
Route::get('summary-report', 'Academics\ReportsController@summary_report')->name('summary-report');

Route::post('summary-report-print', 'Academics\ReportsController@summary_report_print')->name('summary-report-print');


// Concession Reports Links
Route::get('concession-report', 'Academics\ReportsController@concession_report')->name('concession-report');

Route::post('concession-report-print', 'Academics\ReportsController@concession_report_print')->name('concession-report-print');

// Head Wise Fee Reports Links
Route::get('head-wise-fee-report', 'Academics\ReportsController@head_wise_fee_report')->name('head-wise-fee-report');

Route::post('head-wise-fee-report-print', 'Academics\ReportsController@head_wise_fee_report_print')->name('head-wise-fee-report-print');


// Head Wise Summary Reports Links
Route::get('head-wise-summary-report', 'Academics\ReportsController@head_wise_summary_report')->name('head-wise-summary-report');

Route::post('head-wise-summary-report-print', 'Academics\ReportsController@head_wise_summary_report_print')->name('head-wise-summary-report-print');


// Head Wise Revenue Reports Links
Route::get('head-wise-revenue-report', 'Academics\ReportsController@head_wise_revenue_report')->name('head-wise-revenue-report');

Route::post('head-wise-revenue-report-print', 'Academics\ReportsController@head_wise_revenue_report_print')->name('head-wise-revenue-report-print');



// Head Wise Initial Security Reports Links
Route::get('head-wise-inintial-security-fee-report', 'Academics\ReportsController@head_wise_initial_security_fee_report')->name('head-wise-initial-security-fee-report');

Route::post('head-wise-initial-security-fee-report-print', 'Academics\ReportsController@head_wise_initial_security_fee_report_print')->name('head-wise-initial-security-fee-report-print');

// Head Wise LMS Reports Links
Route::get('head-wise-annual-report', 'Academics\ReportsController@head_wise_annual_report')->name('head-wise-annual-report');

Route::post('head-wise-annual-report-print', 'Academics\ReportsController@head_wise_annual_report_print')->name('head-wise-annual-report-print');



// Head Wise LMS Reports Links
Route::get('head-wise-lms-report', 'Academics\ReportsController@head_wise_lms_report')->name('head-wise-lms-report');

Route::post('head-wise-lms-report-print', 'Academics\ReportsController@head_wise_lms_report_print')->name('head-wise-lms-report-print');


// Head Wise Revenue Reports Links
Route::get('head-wise-partyfund-report', 'Academics\ReportsController@head_wise_partyfund_report')->name('head-wise-partyfund-report');

Route::post('head-wise-partyfund-report-print', 'Academics\ReportsController@head_wise_partyfund_report_print')->name('head-wise-partyfund-report-print');

// Head Wise Revenue Reports Links
Route::get('revenue-receivable-received-report', 'Academics\ReportsController@revenue_receivable_received_report')->name('revenue-receivable-received-report');

Route::post('revenue-receivable-received-report-print', 'Academics\ReportsController@revenue_receivable_received_report_print')->name('revenue-receivable-received-report-print');

// Head Wise Revenue Reports Links
Route::get('monthlywise-revenue-receivable-received-report', 'Academics\ReportsController@monthlywise_revenue_receivable_received_report')->name('monthlywise-revenue-receivable-received-report');

Route::post('monthlywise-revenue-receivable-received-report-print', 'Academics\ReportsController@monthlywise_revenue_receivable_received_report_print')->name('monthwise-revenue-receivable-received-report-print');


// Yearly Projection Reports Links
Route::get('yearly-projection-report', 'Academics\ReportsController@yearly_projection_report')->name('yearly-projection-report');

Route::post('yearly-projection-report-print', 'Academics\ReportsController@yearly_projection_report_print')->name('yearly-projection-report-print');

// Admin Reports Detail Links
Route::get('admin-detail-report', 'Academics\ReportsController@admin_detail_report')->name('admin-dtl-report');

Route::post('admin-detail-report-print', 'Academics\ReportsController@admin_detail_report_print')->name('admin-detail-report-print');


// Admin Reports Dtl Links
Route::get('strength-comprison-report', 'Academics\ReportsController@strength_comprision_report')->name('strength-comprison-report');

Route::post('strength-comprison-report-print', 'Academics\ReportsController@strength_comprision_report_print')->name('strength-comprision-report-print');


// ClassWise Strength Report
Route::get('classwise-strength-report', 'Academics\ReportsController@classwise_strength_report')->name('classwise-strength-report');

Route::post('classwise-strength-report-print', 'Academics\ReportsController@classwise_strength_report_print')->name('classwise-strength-report-print');


// ClassWise Revenue Report
Route::get('classwise-revenue-report', 'Academics\ReportsController@classwise_revenue_report')->name('classwise-revenue-report');

Route::post('classwise-revenue-report-print', 'Academics\ReportsController@classwise_revenue_report_print')->name('classwise-revenue-report-print');


// ClassWise Revenue Report
Route::get('comparision-classwise-strength-report', 'Academics\ReportsController@comparision_classwise_strength_report')->name('comparision-classwise-strength-report');

Route::post('comparision-classwise-strength-report-print', 'Academics\ReportsController@comparision_classwise_strength_report_print')->name('comparision-classwise-strength-report-print');

Route::get('bank-payment', 'Admin\BankPaymentSheetController@index')->name('bank-payment');
Route::get('bank-payment-sheet-create', 'Admin\BankPaymentSheetController@bpv_create')->name('bank-payment-sheet-create');
Route::post('bank-payment-sheet-store', 'Admin\BankPaymentSheetController@bpv_store')->name('bank-payment-sheet-store');

Route::post('salary_sheet_store', 'HRM\StaffController@salary_sheet_store')->name('salary_sheet_store');





Route::post('bank_payment_sheet_update', 'Admin\BankPaymentSheetController@bank_payment_sheet_update')->name('bank_payment_sheet_update');

Route::post('bank-payment-sheet-dgm-approval/{id}', 'Admin\BankPaymentSheetController@dgm_approval')->name('bank-payment-sheet-dgm-approval');
Route::post('bank-payment-sheet-gm-approval/{id}', 'Admin\BankPaymentSheetController@gm_approval')->name('bank-payment-sheet-gm-approval');
Route::post('bank-payment-sheet-accountant-approval/{id}', 'Admin\BankPaymentSheetController@accountant_approval')->name('bank-payment-sheet-accountant-approval');
Route::get('payment-sheet-list', 'Admin\BankPaymentSheetController@get_payment_list')->name('payment-sheet-list');
Route::post('generate-voucher-payment/{id}', 'Admin\BankPaymentSheetController@generate_voucher')->name('generate-voucher-payment');
Route::get('payment-sheet-edit/{id}', 'Admin\BankPaymentSheetController@edit_sheet')->name('payment-sheet-edit');

Route::get('payment-sheet-print/{id}', 'Admin\BankPaymentSheetController@print_sheet')->name('payment-sheet-print');
Route::get('cash-payment-sheet-print/{id}', 'Admin\BankPaymentSheetController@cash_print_sheet')->name('cash-payment-sheet-print');



Route::get('cash-payment-sheet-create', 'Admin\BankPaymentSheetController@cpv_create')->name('cash-payment-sheet-create');
Route::post('cash-payment-sheet-store', 'Admin\BankPaymentSheetController@cpv_store')->name('cash-payment-sheet-store');

Route::get('rent-payment-sheet-create', 'Admin\BankPaymentSheetController@rpv_create')->name('rent-payment-sheet-create');
Route::post('rent-payment-sheet-store', 'Admin\BankPaymentSheetController@rpv_store')->name('rent-payment-sheet-store');
Route::get('rent-sheet-details/{id}', 'Admin\BankPaymentSheetController@rent_sheet_details')->name('rent-sheet-details');
Route::get('rent-sheet-print/{id}', 'Admin\BankPaymentSheetController@rent_print_sheet')->name('rent-sheet-print');
Route::get('sheet_details/{id}', 'Admin\BankPaymentSheetController@sheet_details')->name('payment-sheet-details');



Route::get('fee_recover_report', 'Admin\AccountReportsController@fee_recover_report')->name('fee-recover-report');
Route::get('fee_monthly_report', 'Admin\AccountReportsController@fee_monthly_report')->name('fee-monthly-report');
Route::post('fee_recover_report_print', 'Admin\AccountReportsController@fee_recover_report_print')->name('fee-recovery-report-save');
Route::post('fee_monthly_report_print', 'Admin\AccountReportsController@fee_monthly_report_print')->name('fee-monthly-report-save');


Route::get('fee_daily_outstanding_report', 'Admin\AccountReportsController@fee_daily_outstanding_report')->name('fee-daily-outstanding-report');
Route::post('fee_daily_outstanding_report_save', 'Admin\AccountReportsController@fee_daily_outstanding_report_save')->name('fee-daily-outstanding-report-save');
Route::get('/getIpAddress','Admin\AttendanceController@getIpAddress');
Route::get('/getAttendanceUsers','Admin\AttendanceController@getAttendanceUsers');
Route::post('fetch_data','Admin\AttendanceController@fetch_data')->name('fetch-data');

Route::get('security-company','Admin\AccountReportsController@company');
Route::post('recon_with_fee_department_report', 'Admin\AccountReportsController@recon_with_fee_department_report')->name('recon-with-fee-department-report-save');
//Student Detail Report
Route::get('/student-report-branch-id', 'Reports\StudentReportDetailController@board_branches');
Route::get('/student-report-board-id', 'Reports\StudentReportDetailController@boards');
Route::get('/student-report-program-id', 'Reports\StudentReportDetailController@programs');
Route::get('/student-report', 'Reports\StudentReportDetailController@index');
Route::get('/student-report-class-id', 'Reports\StudentReportDetailController@classes');
Route::post('/store-active-session-section', 'Reports\StudentReportDetailController@active_session')->name('store-active-session-section');

// Role related user
Route::get('/role-related-user', 'Asset\AssignAssetController@RoleRelatedUser');


Route::get('/', function () {
    return redirect('/admin/home');
});


Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');

Route::get('change-password', function () {
    return view('profile.change-password');
});
Route::post('change_password', 'Auth\ChangePasswordController@changePassword')->name('change_password');


// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::post('admin/schedule/details',  'HRM\ScheduleController@getCandidateScore')->name('admin.schedule.details');

/* Roles Routes */
Route::get('role', ['as' => 'role',  'uses' => 'RoleController@index']);
Route::get('role/add',                ['as' => 'role.add', 'uses' => 'RoleController@create']);
Route::post('role/store',             ['as' => 'role.store','uses' => 'RoleController@store']);
Route::get('role/{id}/edit',          ['as' => 'role.edit','uses' => 'RoleController@edit']);
Route::post('role/{id}/update',       ['as' => 'role.update', 'uses' => 'RoleController@update']);
Route::get('role/{id}/view',          ['as' => 'role.view','uses' => 'RoleController@show']);
Route::get('role/{id}/delete',        ['as' => 'role.delete','uses' => 'RoleController@destroy']);


Route::group(['middleware' => ['auth', "ProfileActive"], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('get-roles', ['uses' => 'Admin\RolesController@getRoleByRoleType', 'as' => 'load-roles']);
    Route::get('get-roles-staff', ['uses' => 'Admin\RolesController@getRoleByRoleTypeStaff', 'as' => 'load-roles-staff']);

    ////////////////////////////////////////TRAINING MODULE START////////////////////////

    Route::resource("training", "HRM\TrainingController");
    Route::get("training/users-by/role/{id}", "HRM\TrainingController@getUsersByRole");
    Route::get("assign-training/{id}/{name}", "HRM\TrainingController@assign_training")->name('assign-training');;
    Route::get("assign/training/to-users", "HRM\TrainingController@assignTraining");
    Route::get("training/acceptance/form", "HRM\TrainingController@trainingAcceptanceFormPost");
    Route::get("training/acceptance/form/{id}", "HRM\TrainingController@trainingAcceptanceForm");
    Route::get("training/assigned/{id}/users", "HRM\TrainingController@getAssignedUsers");

     Route::get("training_request", "HRM\TrainingController@getTrainingRequest");
    /////////////////////////////TRAINING MODULE END///////////////////////////////////////
    //-------------------------------------Resign Routes ---------------------------------//

    Route::resource("resign", "HRM\ResignController");

    //-------------------------------------Resign Routes  end---------------------------------//

    //-------------------------------------Termination----------------------------------------//

    Route::resource("termination", "HRM\TerminationController");

    Route::get("warning_list","HRM\TerminationController@warning_list");
    Route::post("/justification","HRM\TerminationController@justification");
    Route::get("/acknowledge", "HRM\TerminationController@acknowledge");
    Route::get("/terminate/user/{id}", "HRM\TerminationController@terminateEmploye");
    Route::get("/re-activate/user/{id}", "HRM\TerminationController@reActivate");
    Route::get("/terminatedstaff", "HRM\TerminationController@terminatedstaff");





    //-------------------------------------Termination end---------------------------------//
    //------------------------------staff branch trasnfer controller----------------------//

    Route::resource("branch-transfer", "HRM\StaffBranchTransferController");
    Route::get("/delete/staff/branch/{id}", "HRM\StaffBranchTransferController@delete");
    Route::get("transfer/PDF_add_table_cell(pdfdoc, table, column, row, text, optlist)ptance/{id}", "HRM\StaffBranchTransferController@acceptingOrNot");
    //------------------------------staff branch trasnfer controller end----------------------//

    //------------------------------promotion/demotion controller----------------------//

    Route::resource("PD", "HRM\PDController");
    Route::get("getBoard/{branch_id}", "HRM\PDController@getBoard");
    Route::get("getProgram/{branch_id}/{board_id}", "HRM\PDController@getProgram");
    Route::get("getClasses/{branch_id}/{board_id}/{program_id}", "HRM\PDController@getClasses");
    Route::get("assign/class", "HRM\PDController@assginClass");

    //-----------------------------------promotion/demotion controller end----------------------------//


    Route::resource("all-teachers", "AssignCourcesController");
    Route::get('/teacher-course-detail', 'AssignCourcesController@teacherCourseDetail');

    Route::get('/teacher-course-detail', ['uses' => 'AssignCourcesController@teacherCourseDetail', 'as' => 'teaacher.course.detail']);

    Route::get("get/active-session-section/{branch_id}/{board_id}/{program_id}/{class_id}", "AssignCourcesController@getActiveSessionSection");

   Route::get("get/active-session-section-courses/{course_id}/{branch_id}/{board_id}/{program_id}/{class_id}/{section_id}", "AssignCourcesController@getActiveSessionSectionCourses");


   
    Route::get("get/active-session-section-timetable/{activeSessionSectionID}", "AssignCourcesController@getActiveSessionSectionTimetable");

    Route::get("get/course-by-staff/{id}", "AssignCourcesController@getCoursesByStaff");

    Route::get("delete-session-teacher/{ASSTID}/{staff_id}", "AssignCourcesController@deleteActiveSessionTeacher");



    //------------------------------------Groups -------------------------------//

    Route::resource("groupss", "QA\GroupController");

    Route::get("getStudents/{branch_id}/{board_id}/{program_id}/{class_id}", "QA\GroupController@getStudents");


    Route::resource("labs", "Admin\LabsController");

    Route::get("group/archive/view", "QA\GroupController@archiveGroupView");

    Route::get("group/archive/{id}", "QA\GroupController@archiveGroup");
    
    Route::get("group/unarchive/{id}", "QA\GroupController@unarchiveGroup");
    
    Route::get("group/document/download/{id}", "QA\GroupController@downloadDocument");
    
    Route::get("group/send/notification/{id}", "QA\GroupController@sendNotification");
    

    




    // Regions Resrouce Starts Here
    Route::resource('term', 'Admin\TermController');
    Route::patch('term/active/{id}', ['uses' => 'Admin\TermController@active', 'as' => 'term.active']);
    Route::patch('term/inactive/{id}', ['uses' => 'Admin\TermController@inactive', 'as' => 'term.inactive']);

    // Regions Resrouce Ends Here

    Route::get("board/loadPrograms/{id}", 'Admin\BoardController@loadBoardPrograms');



    Route::post('load-branch-board-programs', ['uses' => 'Admin\BranchesController@loadBranchBoardPrograms', 'as' => 'load-branch-board-programs']);

    Route::post('branch-board-program-store', ['uses' => 'Admin\BranchesController@branchBoardProgramStore', 'as' => 'branch-board-program-store']);
    Route::post('load-program-compulsory-courses', ['uses' => 'Admin\ProgramController@loadProgramCompulsoryCourses', 'as' => 'load-program-compulsory-courses']);
    Route::post('compulsory-courses-store', ['uses' => 'Admin\ProgramController@compulsoryCoursesStore', 'as' => 'compulsory-courses-store']);
    Route::post('load-academics-group-courses', ['uses' => 'Admin\AcademicsGroupController@loadAcademicsGroupCourses', 'as' => 'load-academics-group-courses']);
    Route::post('academics-groups-courses-store', ['uses' => 'Admin\AcademicsGroupController@academicsGroupsCoursesStore', 'as' => 'academics-groups-courses-store']);
    Route::get('academics-groups-courses-create', ['uses' => 'Admin\AcademicsGroupController@academicsGroupsCoursesCreate', 'as' => 'academics-groups-courses-create']);
    Route::get('academics-groups-courses', ['uses' => 'Admin\AcademicsGroupController@academicsGroupsCourses', 'as' => 'academics-groups-courses']);
    Route::get('boardprogramindex', ['uses' => 'Admin\BoardController@boardProgramshow', 'as' => 'boardprogramindex']);
    Route::get('assign-class-section', ['uses' => 'Admin\SectionController@assignClassSection', 'as' => 'classes-assign-class-section']);
    Route::post('existing-add-board-program', ['uses' => 'Admin\BoardController@addExistingBoardProgram', 'as' => 'existing-board-program']);
    Route::post('assign-class-section-store', ['uses' => 'Admin\SectionController@assignClassSectionStore', 'as' => 'section-assign-class-section-store']);


    Route::post('/get/board-programs', 'Admin\BoardController@loadBoardPrograms')->name('board-loadPrograms');
    Route::post('/get/board-program-class', 'Admin\BoardController@loadBoardProgramClass')->name('load-board-program-class');
    Route::post('/get/board-program-program-group', 'Admin\CourseGroupController@loadBoardProgramProgramgroup')->name('load-board-program-program-group');

    Route::post('/get/load-course-group-class-group', 'Admin\ClassGroupController@loadCourseGroupClassGroup')->name('load-course-group-class-group');
    Route::post('/get/program-group-course-group', 'Admin\CourseGroupController@loadProgramGroupCourseGroup')->name('load-program-group-courses-group');
    Route::get('view/board-programs-classes', ['uses' => 'Admin\BoardController@viewBoardProgramsClasses', 'as' => 'view-board-program-class']);
    Route::get('view/board-programs/{id}', ['uses' => 'Admin\BoardController@viewBoardPrograms', 'as' => 'view-board-program']);

    Route::get('view-board-program-class-section', ['uses' => 'Admin\SectionController@viewBoardProgramClassSection', 'as' => 'view-board-program-class-section']);
    Route::post('add-board-program-class-store', ['uses' => 'Admin\ClassController@boardProgramClassStore', 'as' => 'board-program-class-store']);
    Route::get('class-group-courses', ['uses' => 'Admin\MultipleCoursesGroupController@classGroupCourseCreate', 'as' => 'class-group-course-create']);
    Route::get('course-group-course-create', ['uses' => 'Admin\MultipleCoursesGroupController@courseGroupCourseCreate', 'as' => 'course-group-course-create']);
    Route::resource('academics-groups', 'Admin\AcademicsGroupController');
    Route::resource('multiple-courses-groups', 'Admin\MultipleCoursesGroupController');
    Route::resource('class-group', 'Admin\ClassGroupController');
    Route::resource('course-group', 'Admin\CourseGroupController');
    Route::resource('program-group', 'Admin\ProgramController');
    Route::resource('bpc-term', 'Admin\BoardProgramClassTermController');
    Route::resource('courses', 'Admin\CourseController');
    Route::resource('section', 'Admin\SectionController');
    Route::resource('classes', 'Admin\ClassController');
    Route::resource('boards', 'Admin\BoardController');
    Route::get('compulsory-course/{id}', 'Admin\BoardController@compulsory')->name('compulsory-course');

    Route::get('Academic-group-courses/{id}', 'Admin\AcademicsGroupController@academic_courses')->name('Academic-group-courses');
    Route::get('assign-board-program-classes', ['uses' => 'Admin\ClassController@assignBoardProgramClass', 'as' => 'boards-program-classes']);
    Route::get('assign-course-labs', ['uses' => 'Admin\LabsController@assignCourseLab', 'as' => 'course-lab']);
    Route::post('assign-course-labs-store', ['uses' => 'Admin\LabsController@assignCourseLabStore', 'as' => 'assign-course-lab-store']);
    Route::get('assign-course-labs-view', ['uses' => 'Admin\LabsController@assignCourseLabView', 'as' => 'assign-course-lab-view']);
    Route::get('assign-branch-program', ['uses' => 'Admin\BranchesController@assignBranchProgram', 'as' => 'branch-program']);
    Route::get('assign-board-program', ['uses' => 'Admin\BoardController@assignBoardProgram', 'as' => 'board-program']);
    Route::post('add-board-program', ['uses' => 'Admin\BoardController@addBoardProgram', 'as' => 'boards-board-program-store']);
    Route::get('create-company-branch/{id}', ['uses' => 'Admin\BranchesController@create', 'as' => 'branch-create']);
    Route::get('company-branch/{id}', ['uses' => 'Admin\BranchesController@viewCompanyBranches', 'as' => 'view-company-branch']);

Route::get('assign_board_programs/{id}', ['uses' => 'Admin\BoardController@assignBoardPrograms', 'as' => 'assign_board_programs']);

    //Routes By Amir Muddassar

    Route::resource('schedule', 'HRM\ScheduleController');
    Route::get('candidate_shortlist/{id}','HRM\CandidateFormController@short_list_candidate');
	Route::get('candidate_list_job/{id}','HRM\CandidateFormController@candidate_list_job');
	Route::get('recommended_candidates/{id}','HRM\CandidateFormController@recommended_candidate_list_job');
	Route::get('candidate_hired_job/{id}','HRM\CandidateFormController@candidate_hired_job');
	Route::get('candidate_feedback_job/{id}','HRM\CandidateFormController@candidate_feedback_job');
	Route::get('candidate_rejected_job/{id}','HRM\CandidateFormController@candidate_rejected_job');




	

    Route::post('/schedule/score', ['uses' => 'HRM\ScheduleController@score', 'as' => 'schedule.score']);
   
    #Hired Candidates
    Route::get('/candidate-hired', ['uses' => 'HRM\CandidateFormController@candidateHired', 'as' => 'candidate-hired']);

    #Waiting List of Candidates
    Route::get('/candidate_waiting_list','HRM\CandidateFormController@candidate_waiting_list');


    Route::get('/candidate-hire/{id}', ['uses' => 'HRM\ScheduleController@hire', 'as' => 'schedule.hire']);
    Route::get('/candidate-reject/{id}', ['uses' => 'HRM\ScheduleController@notHired', 'as' => 'schedule.reject']);
    // Route::get('/reject/{id}', 'HRM\ScheduleController@reject')->name('admin.reject');s
    // Route::get('admin/get-candidate-score/{id}/details','HRM\ScheduleController@getCandidateScore');

    Route::get('/reject/{id}', ['uses' => 'HRM\ScheduleController@reject', 'as' => 'reject']);
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    // Financial Years Starts Here
    Route::patch('financial_years/active/{id}', ['uses' => 'Admin\FinancialYearsController@active', 'as' => 'financial_years.active']);
    Route::patch('financial_years/inactive/{id}', ['uses' => 'Admin\FinancialYearsController@inactive', 'as' => 'financial_years.inactive']);
    Route::resource('financial_years', 'Admin\FinancialYearsController');
    // Financial Years Ends Here

    // AccountTypes Resrouce Starts Here
    Route::patch('account_types/active/{id}', ['uses' => 'Admin\AccountTypesController@active', 'as' => 'account_types.active']);
    Route::patch('account_types/inactive/{id}', ['uses' => 'Admin\AccountTypesController@inactive', 'as' => 'account_types.inactive']);
    Route::resource('account_types', 'Admin\AccountTypesController');
    // AccountTypes Resrouce Ends Here

    // Payment Methods Resrouce Starts Here
    Route::patch('payment_methods/active/{id}', ['uses' => 'Admin\PaymentMethodsController@active', 'as' => 'payment_methods.active']);
    Route::patch('payment_methods/inactive/{id}', ['uses' => 'Admin\PaymentMethodsController@inactive', 'as' => 'payment_methods.inactive']);
    Route::resource('payment_methods', 'Admin\PaymentMethodsController');
    // Payment Methods Resrouce Ends Here

    // Department Resrouce Starts Here
    Route::resource('departments', 'Admin\DepartmentsController');
    // Department Resrouce Ends Here

    // Discount Type Resrouce Starts Here
    Route::resource('discount-type', 'Admin\DiscountTypeController');
    // Discount Type Resrouce Ends Here

    // Active Session Resrouce Starts Here
    Route::resource('active-session', 'Admin\ActiveSessionController');
    // Session Resrouce Ends Here

    // Session Resrouce Starts Here
    Route::resource('session', 'Admin\SessionController');
    // Session Resrouce Ends Here

    // Regions Resrouce Starts Here
    Route::resource('regions', 'Admin\RegionController');
    // Regions Resrouce Ends Here

    // Regions Resrouce Starts Here
    Route::resource('territory', 'Admin\TerritoryController');
    // Regions Resrouce Ends Here

    // Regions Resrouce Starts Here
    Route::resource('term', 'Admin\TermController');
    Route::patch('term/active/{id}', ['uses' => 'Admin\TermController@active', 'as' => 'term.active']);
    Route::patch('term/inactive/{id}', ['uses' => 'Admin\TermController@inactive', 'as' => 'term.inactive']);
    // Regions Resrouce Ends Here

    // Companies Resrouce Starts Here
    Route::resource('companies', 'Admin\CompanyController');

    // Companies Resrouce Ends Here

    // Branches Resrouce Starts Here
    Route::resource('branches', 'Admin\BranchesController');
    // Branches Resrouce Ends Here

    // Customers Resrouce Starts Here
    Route::patch('customers/active/{id}', ['uses' => 'Admin\CustomersController@active', 'as' => 'customers.active']);
    Route::patch('customers/inactive/{id}', ['uses' => 'Admin\CustomersController@inactive', 'as' => 'customers.inactive']);
    Route::resource('customers', 'Admin\CustomersController');
    // Customers Resrouce Ends Here

    // Local Purchase start Here
    Route::get('duty/segregate/index', ['uses' => 'Admin\DutySegController@indexDuties', 'as' => 'duty_seg.index']);
    Route::get('duty/segregate/datatables', ['uses' => 'Admin\DutySegController@datatables', 'as' => 'duty_seg.datatables']);
    Route::get('duty/segregate/detail/{id}', ['uses' => 'Admin\DutySegController@detailDuty', 'as' => 'duty_seg.detail']);
    Route::get('duty/segregate/duty', 'Admin\DutySegController@duty')->name('duty_seg.duty');
    Route::get('duty/segregate/invoices', 'Admin\DutySegController@Invoices')->name('duty_seg.invoices');
    Route::get('duty/segregate', 'Admin\DutySegController@viewDutySegrigation')->name('duty_seg.view');
    Route::post('duty/segregate', 'Admin\DutySegController@storeDutySegrigation')->name('duty_seg.store');
    // Local Purchase Ends Here

    // Local Purchase start Here
    Route::get('localpurchase/productlist/{order_id}', 'Admin\LocalPurchaseController@productList')->name('localpurchase.product_list');
    Route::resource('localpurchase', 'Admin\LocalPurchaseController');
    // Local Purchase Ends Here


    // Employees Resrouce Starts Here
    //Route::get('employees/show/{id}', ['uses' => 'Admin\EmployeesController@show', 'as' => 'employees.show']);

    //commented by asad
    // Route::patch('employees/active/{id}', ['uses' => 'Admin\EmployeesController@active', 'as' => 'employees.active']);
    // Route::patch('employees/inactive/{id}', ['uses' => 'Admin\EmployeesController@inactive', 'as' => 'employees.inactive']);
    // Route::get('employees/job/{id}', ['uses' => 'Admin\EmployeesController@update_employee_job_detail', 'as' => 'employees.job']);
    // Route::post('employees/job/{id}', ['uses' => 'Admin\EmployeesController@insert_employee_job_data', 'as' => 'employees.insertjobdata']);
    // Route::get('employees/salary/{id}', ['uses' => 'Admin\EmployeesController@update_employee_salary_detail', 'as' => 'employees.salary']);
    // Route::post('employees/salary/{id}', ['uses' => 'Admin\EmployeesController@update_employee_salary_data', 'as' => 'employees.salarydata']);
    // Route::get('employees/experience/{id}', ['uses' => 'Admin\EmployeesController@update_employee_experience_detail', 'as' => 'employees.experience']);
    // Route::post('employees/experience/{id}', ['uses' => 'Admin\EmployeesController@update_employee_experience_data', 'as' => 'employees.experiencedata']);
    // Route::get('employees/education/{id}', ['uses' => 'Admin\EmployeesController@update_employee_education_detail', 'as' => 'employees.education']);
    // Route::post('employees/education/{id}', ['uses' => 'Admin\EmployeesController@update_employee_education_data', 'as' => 'employees.educationdata']);
    // Route::get('employees/resignation/{id}', ['uses' => 'Admin\EmployeesController@update_employee_resignation_detail', 'as' => 'employees.resignation']);
    // Route::post('employees/resignation/{id}', ['uses' => 'Admin\EmployeesController@update_employee_resignation_data', 'as' => 'employees.resignationdata']);
    // Route::post('employees/get_branches_ajax', ['uses' => 'Admin\EmployeesController@get_branches_ajax', 'as' => 'employees.get_branches_ajax']);
    // Route::post('employees/get_territory_ajax', ['uses' => 'Admin\EmployeesController@get_territory_ajax', 'as' => 'employees.get_territory_ajax']);
    // Route::post('employees/get_job_title_by_departmen_id_ajax', ['uses' => 'Admin\EmployeesController@get_job_title_by_departmen_id_ajax', 'as' => 'employees.get_job_title_by_departmen_id_ajax']);

    // Route::get('employees/getBranches', ['uses' => 'Admin\EmployeesController@getBranches', 'as' => 'employees.getBranches']);
    // Route::get('EmpAvailable', ['uses' => 'Admin\EmployeesController@EmpAvailable', 'as' => 'employees.EmpAvailable']);
    // Route::post('employees/availableStatus', ['uses' => 'Admin\EmployeesController@availableStatus', 'as' => 'employees.availableStatus']);

    // Route::resource('employess', 'HRM\EmployeesController');
    Route::resource('documents', 'HRM\DocumentController');

    Route::resource('staff', 'HRM\StaffController');


    Route::resource('bank_loan', 'Admin\BankLoanController');

     Route::resource('device', 'Admin\DeviceController');

    Route::resource('holiday', 'Admin\HolidayController');


    //Asset Type Resource Controller Start

    Route::resource('asset_type', 'Asset\AssetTypeController');

    Route::patch('asset_type/active/{id}', ['uses' => 'Asset\AssetTypeController@active', 'as' => 'asset_type.active']);
    Route::patch('asset_type/inactive/{id}', ['uses' => 'Asset\AssetTypeController@inactive', 'as' => 'asset_type.inactive']);
    //Asset Type Resource Controller End

    //Asset Resource Controller Start


    Route::resource('asset', 'Asset\AssetController');
    
    Route::patch('asset/active/{id}', ['uses' => 'Asset\AssetController@active', 'as' => 'asset.active']);
    Route::patch('asset/inactive/{id}', ['uses' => 'Asset\AssetController@inactive', 'as' => 'asset.inactive']);
    
    //Asset Resource Controller End

    Route::resource('timetable', 'HRM\TimetableController');

    Route::resource('class_timetable', 'HRM\ClassTimetableController');

    Route::resource('event_timetable', 'HRM\EventTimetableController');

    Route::resource('lab_timetable', 'HRM\LabTimetableController');

    Route::resource('exam_timetable', 'HRM\ExamTimetableController');

    Route::resource('timetable_category', 'HRM\TimetableCategoryController');
    Route::get('staff-profile', 'HRM\StaffController@profile')->name('staff.profile');

    
    Route::patch('staff/active/{id}', ['uses' => 'HRM\StaffController@active', 'as' => 'staff.active']);
    Route::patch('staff/inactive/{id}', ['uses' => 'HRM\StaffController@inactive', 'as' => 'staff.inactive']);

    Route::resource('branchhr', 'HRM\BranchHrController');
    Route::patch('branchhr/active/{id}', ['uses' => 'HRM\BranchHrController@active', 'as' => 'branchhr.active']);
    Route::patch('branchhr/inactive/{id}', ['uses' => 'HRM\BranchHrController@inactive', 'as' => 'branchhr.inactive']);




//Lecture based Attendance    

    Route::resource('attendance_payroll', 'Payroll\AttendanceController');
    Route::resource('salary_payroll', 'Payroll\SalaryController');
    Route::get('salary_slip_print/{id}', 'Payroll\SalaryController@salaryprint');



// Amir Routes start


//Lecture Based Employee Salary


   






//Full Time Based Employee Salary
    Route::post('salary_employee_permanent', ['uses' => 'Payroll\SalaryController@salary', 'as' => 'Monthly.fulltime_based_salary']);


     Route::post('salary_employee_permanent_lecture', ['uses' => 'Payroll\SalaryController@salary', 'as' => 'Monthly.lecture_based_salary']);

    
    Route::get('salary_employee_permanent_only/{id}','Payroll\SalaryController@salary_permanent');
    Route::get('salary_employee_full_time_view/{id}','Payroll\SalaryController@full_time_view');

    Route::get('salary_employee_lecture_based_view/{id}','Payroll\SalaryController@lecture_based_salary');

    Route::get('salaries_details/{id}','Payroll\SalaryController@salary_details');
    Route::get('salaries_print/{id}','Payroll\SalaryController@salary_print');
    Route::get('payrol-company','Payroll\SalaryController@company');
    Route::get('payrol-branch','Payroll\SalaryController@branch');
    Route::get('payrol-staff','Payroll\SalaryController@staff');


//    Route::get('salary_employee_lecture/{id}', 'Payroll\SalaryController@lecture_based_salary');

    Route::post('pay_salary', ['uses' => 'Payroll\SalaryController@pay_salary', 'as' => 'pay.employee_salary']);
    

// Amir Routes end

    Route::get('find_timetable_branch','Payroll\AttendanceController@get_timetable_branch');

    Route::get('find_vendor_id','Admin\VendorController@get_vendor_id');
    
    Route::get('find_tax_precentage','Admin\VendorController@get_tax_precentage');
    Route::get('find_tax_amount_per','Admin\VendorController@get_tax_amount_per');


    Route::get('find_branch_staff','HRM\StaffController@find_branch_staff');
    
    Route::get('find_branch_staff_attendance_report','HRM\StaffController@find_branch_staff_attendance_report');
    
    


    Route::get('find_company_wise_eobiepf','HRM\StaffController@find_company_wise_eobiepf');
    Route::get('find_company_branch_wise_eobiepf','HRM\StaffController@find_company_branch_wise_eobiepf');








    Route::get('find_board','Payroll\AttendanceController@get_board');
    Route::get('find_board_program','Payroll\AttendanceController@get_program');
    Route::get('find_board_program_class','Payroll\AttendanceController@get_program_class');
    Route::get('find_board_program_class_section','Payroll\AttendanceController@get_program_class_section');
    Route::get('find_board_program_class_section_courses','Payroll\AttendanceController@get_program_class_section_course');

    Route::get('teacher_attendance_daywise','Payroll\AttendanceController@store_teacher_attendance_daywise');

    Route::get('find_board_program_class_section_students','Payroll\AttendanceController@get_program_class_section_students');


    Route::get('find_course_name','Payroll\AttendanceController@get_course_name');

    Route::get('find_board_program_class_section_courses_students','Payroll\AttendanceController@get_program_class_section_students');


Route::get('find_program_courses_timetable','Payroll\AttendanceController@get_program_courses_timetable');

    


    Route::resource('leave_management', 'Payroll\LeaveManagementController');

    Route::get('/leaverequestlist', ['uses' => 'Payroll\LeaveManagementController@leaveRequestList', 'as' => 'leave_request.list']);

    Route::get('/leaverequest/approve/{id}', ['uses' => 'Payroll\LeaveManagementController@leaveRequestApprove', 'as' => 'leave_request.approve']);

    Route::get('/leaverequest/unapprove/{id}', ['uses' => 'Payroll\LeaveManagementController@leaveRequestUnapprove', 'as' => 'leave_request.unapprove']);
    Route::get('/staff.create', ['uses' => 'HRM\StaffController@create', 'as' => 'staff.create']);
    // Employees Resrouce Ends Here
    //Banks Resrouce Starts Here
    Route::resource('banks', 'Admin\BanksController');
    Route::post('banks_mass_destroy', ['uses' => 'Admin\BanksController@massDestroy', 'as' => 'banks.mass_destroy']);
    // Banks Resrouce Ends Here
    //Banksdetail Resrouce Starts Here
    Route::resource('bank_detail', 'Admin\BankDetailController');
    Route::post('bank_detail_mass_destroy', ['uses' => 'Admin\BankDetailController@massDestroy', 'as' => 'bank_detail.mass_destroy']);
    // Banksdetail Resrouce Ends Here

    // Suppliers Resrouce Starts Here
    Route::post('suppliers/delete', ['uses' => 'Admin\SuppliersController@delete', 'as' => 'suppliers.delete']);
    Route::get('supliers/datatables', ['uses' => 'Admin\SuppliersController@datatables', 'as' => 'supliers.datatables']);
    Route::resource('suppliers', 'Admin\SuppliersController');
    Route::patch('suppliers/active/{id}', ['uses' => 'Admin\SuppliersController@active', 'as' => 'suppliers.active']);
    Route::patch('suppliers/inactive/{id}', ['uses' => 'Admin\SuppliersController@inactive', 'as' => 'suppliers.inactive']);
    Route::post('suppliers_mass_destroy', ['uses' => 'Admin\SuppliersController@massDestroy', 'as' => 'suppliers.mass_destroy']);
    // Suppliers Resrouce Ends Here

    // Sales Invoices Starts Here
    Route::get('/salesinvoice', ['uses' => 'Admin\SalesInvoiceController@index', 'as' => 'salesinvoice.index']);
    Route::get('/salesinvoice/create', ['uses' => 'Admin\SalesInvoiceController@create', 'as' => 'salesinvoice.create']);
    Route::get('/salesinvoice/view/{id}', ['uses' => 'Admin\SalesInvoiceController@view', 'as' => 'salesinvoice.view']);
    Route::get('/salesinvoice/view/downloadPDF/{id}', ['uses' => 'Admin\SalesInvoiceController@downloadPDF', 'as' => 'salesinvoice.pdf']);
    Route::post('/salesinvoice/store', ['uses' => 'Admin\SalesInvoiceController@store', 'as' => 'salesinvoice.store']);
    Route::post('salesinvoice/update/{id}', ['uses' => 'Admin\SalesInvoiceController@update', 'as' => 'salesinvoice.update']);
    Route::post('/salesinvoice/getOrderDetail', ['uses' => 'Admin\SalesInvoiceController@getOrderDetail', 'as' => 'salesinvoice.getOrderDetail']);
    Route::get('salesinvoice/datatables', ['uses' => 'Admin\SalesInvoiceController@datatables', 'as' => 'salesinvoice.datatables']);
    Route::post('salesinvoice/confirm', ['uses' => 'Admin\SalesInvoiceController@confirm', 'as' => 'salesinvoice.confirm']);
    Route::post('salesinvoice/update_invoice/{id}', ['uses' => 'Admin\SalesInvoiceController@update_invoice', 'as' => 'salesinvoice.update_invoice']);

    Route::get('/salesinvoice/{id}/edit', ['uses' => 'Admin\SalesInvoiceController@edit', 'as' => 'salesinvoice.edit']);
    //    Route::resource('salesinvoice', 'Admin\SalesInvoiceController');
    //Orders Resrouce Starts Here
    Route::post('orders/delete', ['uses' => 'Admin\OrdersController@delete', 'as' => 'orders.delete']);
    Route::get('importindex', ['uses' => 'Admin\OrdersController@importindex', 'as' => 'orders.importindex']);
    Route::get('orders/datatables', ['uses' => 'Admin\OrdersController@datatables', 'as' => 'orders.datatables']);
    Route::get('orders/importdatatables', ['uses' => 'Admin\OrdersController@importdatatables', 'as' => 'orders.importdatatables']);
    Route::get('orders/productlist', 'Admin\OrdersController@productlist')->name('orders.productlist');
    Route::resource('orders', 'Admin\OrdersController');
    Route::post('orders_mass_destroy', ['uses' => 'Admin\OrdersController@massDestroy', 'as' => 'orders.mass_destroy']);
    Route::post('stockitems/delete', ['uses' => 'Admin\StockItemsController@delete', 'as' => 'stockitems.delete']);
    Route::post('stockitems/datatables', ['uses' => 'Admin\StockItemsController@datatables', 'as' => 'stockitems.datatables']);
    Route::get('stockitems/models/{model_no}', 'Admin\StockItemsController@modelno')->name('stockitems.models');
    Route::get('stockitems/performa/{lc_no}/{sup_id}', 'Admin\StockItemsController@performa')->name('stockitems.performa');
    Route::get('stockitems/inventoryproduct', 'Admin\StockItemsController@inventoryproduct')->name('stockitems.inventoryproduct');
    //Orders Routeesrouce End Here
    Route::get('cinvoice/datatables', ['uses' => 'Admin\ComercialInvoiceController@datatables', 'as' => 'cinvoice.datatables']);
    Route::resource('cinvoice', 'Admin\ComercialInvoiceController');
    Route::get('cinvoice/productlist/{listvalue}', 'Admin\ComercialInvoiceController@getprpforma')->name('cinvoice.productlist');
    Route::get('cinvoice/costing/{ComercialInvoiceModel}', 'Admin\ComercialInvoiceController@lccosting')->name('cinvoice.costing');
    Route::post('cinvoice/cost', 'Admin\ComercialInvoiceController@costcalculation')->name('cinvoice.cost');
    Route::post('grimport/upd/{id}', ['uses' => 'Admin\ComercialInvoiceController@update', 'as' => 'comercialinvoiceupd.update']);
    Route::get('cinvoice/viewduty/{ComercialInvoiceModel}', 'Admin\ComercialInvoiceController@viewduty')->name('cinvoice.viewduty');
    Route::patch('cinvoice/viewduty/active/{id}', ['uses' => 'Admin\ComercialInvoiceController@active', 'as' => 'comercialinvoice.active']);
    Route::patch('cinvoice/viewduty/inactive/{id}', ['uses' => 'Admin\ComercialInvoiceController@inactive', 'as' => 'comercialinvoice.inactive']);

    //Sales Resrouce Starts Here
    Route::get('sales/produtsdetail/{id}', 'Admin\SalesController@produtsdetail')->name('sales.produtsdetail');
    Route::get('sales/produtscol/{id}', 'Admin\SalesController@produtscol')->name('sales.produtscol');
    Route::resource('sales', 'Admin\SalesController');
    Route::patch('sales/active/{id}', ['uses' => 'Admin\SalesController@active', 'as' => 'sales.active']);
    Route::patch('sales/inactive/{id}', ['uses' => 'Admin\SalesController@inactive', 'as' => 'sales.inactive']);
    Route::post('sales_mass_destroy', ['uses' => 'Admin\SalesController@massDestroy', 'as' => 'sales.mass_destroy']);
    //Sales Resrouce Ends Here
    // goods receipt Resrouce Starts Here
    // Route::post('products/delete', ['uses' => 'Admin\GoodsIssueController@delete', 'as' => 'products.delete']);
    Route::get('inventorytransfer/stocklist', 'Admin\InventoryTransferController@stocklist')->name('inventorytransfer.stocklist');
    Route::get('inventorytransfer/datatables', ['uses' => 'Admin\InventoryTransferController@datatables', 'as' => 'inventorytransfer.datatables']);
    Route::get('inventorytransfer/items/{id}', 'Admin\InventoryTransferController@stockrecord')->name('inventorytransfer.items');
    Route::resource('inventorytransfer', 'Admin\InventoryTransferController');
    // goods receipt Resrouce Ends Here

    // goods receipt Resrouce Starts Here
    // Route::post('products/delete', ['uses' => 'Admin\GoodsIssueController@delete', 'as' => 'products.delete']);
    Route::get('goodsreceipt/datatables', ['uses' => 'Admin\GoodsReceiptController@datatables', 'as' => 'goodsreceipt.datatables']);
    Route::get('goodsreceipt/product/{lc_no}/{sup_id}', 'Admin\GoodsReceiptController@getproduct')->name('goodsreceipt.product');
    Route::resource('goodsreceipt', 'Admin\GoodsReceiptController');
    // goods receipt Resrouce Ends Here

    // goods issue Resrouce Starts Here
    // Route::post('products/delete', ['uses' => 'Admin\GoodsIssueController@delete', 'as' => 'products.delete']);
    Route::get('goodsissue/datatables', ['uses' => 'Admin\GoodsIssueController@datatables', 'as' => 'goodsissue.datatables']);
    Route::get('goodsissue/items/{id}', 'Admin\GoodsIssueController@stockrecord')->name('goodsissue.items');
    Route::resource('goodsissue', 'Admin\GoodsIssueController');
    // goods issue Resrouce Ends Here

    //Stockitems Resrouce Starts Here

    Route::get('stockitems/performa/{lc_no}', 'Admin\StockItemsController@performa')->name('stockitems.performa');
    Route::get('stockitems/report', 'Admin\StockItemsController@StockReport')->name('stockitems.report');
    Route::post('stockitems/report_filter', 'Admin\StockItemsController@FilterReport')->name('stockitems.filterreport');
    Route::resource('stockitems', 'Admin\StockItemsController');
    Route::patch('stockitems/active/{id}', ['uses' => 'Admin\StockItemsController@active', 'as' => 'stockitems.active']);
    Route::patch('stockitems/inactive/{id}', ['uses' => 'Admin\StockItemsController@inactive', 'as' => 'stockitems.inactive']);

    Route::post('stockitems_mass_destroy', ['uses' => 'Admin\StockItemsController@massDestroy', 'as' => 'stockitems.mass_destroy']);
    Route::post('add/stockItem', ['uses' => 'Admin\PerformaStockController@addItem', 'as' => 'performastock.add_item']);

    //Stockitems Resrouce Ends Here

    //fix_assets Resrouce Starts Here

    Route::resource('fix_assets', 'Admin\FixAssetsController');

    //fix_assets Resrouce Starts Here

    //purchase order Resrouce Starts Here
    Route::post('pono/delete', ['uses' => 'Admin\PoNoController@delete', 'as' => 'pono.delete']);
    Route::get('pono/datatables', ['uses' => 'Admin\PoNoController@datatables', 'as' => 'pono.datatables']);
    Route::resource('pono', 'Admin\PoNoController');
    Route::post('pono_mass_destroy', ['uses' => 'Admin\PoNoController@massDestroy', 'as' => 'pono.mass_destroy']);
    //purchase order Resrouce Ends Here


	

    // custome rout performa //

    Route::post('performastock/costing/{PerformaStockModel}', 'Admin\PerformaStockController@submitcosting')->name('performastock.costing');

    //GRImport Starts Here
    Route::get('grimport', ['uses' => 'Admin\LcBankController@indexGrImport', 'as' => 'grimport']);
    Route::get('grimport/create', ['uses' => 'Admin\LcBankController@createGrImport', 'as' => 'grimport.create']);
    Route::post('grimport/datatables', ['uses' => 'Admin\LcBankController@getGrImportDatatables', 'as' => 'grimport.datatables']);
    Route::get('grimport/view/{id}', ['uses' => 'Admin\LcBankController@viewGrImport', 'as' => 'grimport.view']);
    Route::get('grimport/viewbuffer/{id}', ['uses' => 'Admin\LcBankController@viewBuffer', 'as' => 'grimport.buffer.view']);
    Route::post('grimport/transfer_inventory', ['uses' => 'Admin\LcBankController@transferInventory', 'as' => 'grimport.transfer.inventory']);

    //Performa Stock Starts Here//
    Route::resource('performastock', 'Admin\PerformaStockController');
    Route::post('performastock_mass_destroy', ['uses' => 'Admin\PerformaStockController@massDestroy', 'as' => 'performastock.mass_destroy']);
    Route::get('performastock/costing/{performaStockModel}', 'Admin\PerformaStockController@lccosting')->name('performastock.costing');
    Route::get('performastock/sale_voucher/create', 'Admin\PerformaStockController@performaInvoice')->name('performastock.sale_voucher.create');
    Route::get('performastock/sale_voucher/get', 'Admin\PerformaStockController@performaInvoice')->name('performastock.sale_voucher.create');
    //Performa Stock Starts Here/

    //Lc Bank Starts Here//
    Route::get('lcbank/productlist/{listvalue}', 'Admin\PerformaStockController@productImport')->name('lcbank.productlist');
    Route::post('lcbank/payment', 'Admin\LcBankController@marginCalculate')->name('lcbank.payment');
    Route::post('lcbank/fluctuation', 'Admin\LcBankController@rateFluctuation')->name('lcbank.fluctuation');
    Route::post('lcbank/delete', ['uses' => 'Admin\LcBankController@delete', 'as' => 'lcbank.delete']);
    Route::get('lcbank/sheetcost', ['uses' => 'Admin\LcBankController@sheetCsoting', 'as' => 'lcbank.sheetcost']);
    Route::get('lcbank/datatablesSheet', ['uses' => 'Admin\LcBankController@sheet_datatables', 'as' => 'lcbank.datatablesSheet']);
    Route::get('lcbank/datatables', ['uses' => 'Admin\LcBankController@datatables', 'as' => 'lcbank.datatables']);
    Route::get('lcbank/shipment', 'Admin\LcBankController@shipmentSchedule')->name('lcbank.shipment');
    Route::get('lcbank/create/{model_no}', 'Admin\LcBankController@lcamount')->name('lcbank.create');
    Route::resource('lcbank', 'Admin\LcBankController');
    Route::post('lcbank_mass_destroy', ['uses' => 'Admin\LcBankController@massDestroy', 'as' => 'lcbank.mass_destroy']);
    //Lc Bank Starts Here/

    // Release stock routes starts here
    Route::get('lcbank/invoicelist/{invoice_id}', 'Admin\LcBankController@invoiceImport')->name('lcbank.invoice_list');
    Route::get('lcbank/lctype/{invoice_id}', 'Admin\LcBankController@lctype')->name('lcbank.lctype');
    Route::post('releaseStock', 'Admin\LcBankController@storeReleaseStock')->name('releasestock.store');
    // Release stock routes ends 

    //Groups Resrouce Starts Here
    Route::get('groups/getData', ['uses' => 'Admin\GroupsController@getData', 'as' => 'groups.getData']);
    Route::post('groups_mass_destroy', ['uses' => 'Admin\GroupsController@massDestroy', 'as' => 'groups.mass_destroy']);
    Route::post('groups/destroy/{id}', ['uses' => 'Admin\GroupsController@destroy', 'as' => 'groups.destroy']);
    Route::post('groups/edit/{id}', ['uses' => 'Admin\GroupsController@edit', 'as' => 'groups.edit']);

    Route::resource('groups', 'Admin\GroupsController');
    //Groups Resrouce eNDS Here

    //Ledgers Resrouce Starts Here
    Route::resource('ledgers', 'Admin\LedgersController');
    Route::get('ledger_tree', ['uses' => 'Admin\LedgersController@ledger_tree', 'as' => 'ledger_tree']);
    Route::get('get_ledger_tree/{PID}', ['uses' => 'Admin\LedgersController@get_ledger_tree', 'as' => 'get_ledger_tree']);
    Route::post('ledgers_child_detail', ['uses' => 'Admin\LedgersController@ledgerChild', 'as' => 'ledgers.child.detail']);
    Route::post('ledgers_mass_destroy', ['uses' => 'Admin\LedgersController@massDestroy', 'as' => 'ledgers.mass_destroy']);
    //Ledgers Resrouce eNDS Here

    //Orders Resrouce Starts Here
    Route::resource('orders', 'Admin\OrdersController');
    Route::post('orders_mass_destroy', ['uses' => 'Admin\OrdersController@massDestroy', 'as' => 'orders.mass_destroy']);
    //Orders Resrouce Starts Here

        Route::patch('permissions/active/{id}', ['uses' => 'Admin\PermissionsController@active', 'as' => 'permissions.active']);
    Route::patch('permissions/inactive/{id}', ['uses' => 'Admin\PermissionsController@inactive', 'as' => 'permissions.inactive']);
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);

    Route::patch('roles/active/{id}', ['uses' => 'Admin\RolesController@active', 'as' => 'roles.active']);
    Route::patch('roles/inactive/{id}', ['uses' => 'Admin\RolesController@inactive', 'as' => 'roles.inactive']);
    Route::resource('roles', 'Admin\RolesController');

    Route::resource('users', 'Admin\UsersController');
    Route::post('user-store', ['uses' => 'Admin\UsersController@createUser', 'as' => 'user-create-store']);
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);

    Route::post('taxsettings/delete', ['uses' => 'Admin\TaxSettingsController@delete', 'as' => 'taxsettings.delete']);
    Route::post('taxsettings/active', ['uses' => 'Admin\TaxSettingsController@active', 'as' => 'taxsettings.active']);
    Route::post('taxsettings/inactive', ['uses' => 'Admin\TaxSettingsController@inactive', 'as' => 'taxsettings.inactive']);
    Route::post('taxsettings/datatables', ['uses' => 'Admin\TaxSettingsController@datatables', 'as' => 'taxsettings.datatables']);
    Route::get('taxsettings/details/{id}', ['uses' => 'Admin\TaxSettingsController@details', 'as' => 'taxsettings.details']);
    Route::post('taxsettings/getEmployeeDetail', ['uses' => 'Admin\TaxSettingsController@getEmployeeDetail', 'as' => 'taxsettings.getEmployeeDetail']);
    Route::post('taxsettings/transfer_datatables', ['uses' => 'Admin\TaxSettingsController@transfer_datatables', 'as' => 'taxsettings.transfer_datatables']);
    Route::post('taxsettings/transfer_taxsettings', ['uses' => 'Admin\TaxSettingsController@transfer_taxsettings', 'as' => 'taxsettings.transfer_taxsettings']);
    Route::resource('taxsettings', 'Admin\TaxSettingsController');

    //Settings Resrouce Starts Here
    Route::resource('settings', 'Admin\SettingsController');
    //Settings Resrouce Ends Here
    //Entries Resrouce Starts Here
    Route::get('entries/{entry}/entry', ['uses' => 'Admin\EntriesController@entry', 'as' => 'entries.entry']);
    Route::post('entries/activate/{entry}', ['uses' => 'Admin\EntriesController@active', 'as' => 'entries.active']);
    Route::post('entries/inactive/{entry}', ['uses' => 'Admin\EntriesController@inactive', 'as' => 'entries.inactive']);
    Route::post('entries/edit/{entry}', ['uses' => 'Admin\EntriesController@edit', 'as' => 'entries.edit']);
    Route::post('entries/destroy/{entry}', ['uses' => 'Admin\EntriesController@destroy', 'as' => 'entries.destroy']);
    Route::get('entries/getData', ['uses' => 'Admin\EntriesController@getData', 'as' => 'entries.getData']);
    Route::get('entries/downloadPDF/{id}', ['uses' => 'Admin\EntriesController@downloadPDF', 'as' => 'entries.downloadPDF']);
    Route::resource('entries', 'Admin\EntriesController'); //Entries Resrouce eNDS Here
    Route::get('entries1', 'Admin\EntriesController@expense');
 //  Route::group(['prefix' => 'reports', 'as' => 'account_reports.'], function () {
  //  Route::get('entries1', ['uses' => 'Admin\AccountReportsController@trial_balance', 'as' => 'trial_balance']);
    Route::group(['prefix' => 'reports', 'as' => 'account_reports.'], function () {
    //    Route::post('trial-balance-report', ['uses' => 'Admin\AccountReportsController@trial_balance_report', 'as' => 'trial_balance_report']);
      
});

    // Entries Group Started
    Route::group(['prefix' => 'voucher', 'as' => 'voucher.'], function () {
        // Cash Search Voucher Routes
        Route::get('cash/search', ['uses' => 'Admin\EntriesController@cash_search', 'as' => 'cash_search']);
        // Bank Search Voucher Routes
        Route::get('bank/search', ['uses' => 'Admin\EntriesController@bank_search', 'as' => 'bank_search']);
        // Cash & Bank Search Voucher Routes
        Route::get('cashbank/search', ['uses' => 'Admin\EntriesController@cashbank_search', 'as' => 'cashbank_search']);

        // Journal Voucher Routes
        Route::get('gjv/create', ['uses' => 'Admin\EntriesController@gjv_create', 'as' => 'gjv_create']);
        Route::post('gjv/store', ['uses' => 'Admin\EntriesController@gjv_store', 'as' => 'gjv_store']);
        Route::get('gjv/search', ['uses' => 'Admin\EntriesController@gjv_search', 'as' => 'gjv_search']);
        Route::get('student/search', ['uses' => 'Admin\EntriesController@student_search', 'as' => 'student_search']);
        Route::get('teacher/search', ['uses' => 'Admin\EntriesController@teacher_search', 'as' => 'teacher_search']);
        // Cash Receipt Voucher Routes
        Route::get('crv/create', ['uses' => 'Admin\EntriesController@crv_create', 'as' => 'crv_create']);
        Route::post('crv/store', ['uses' => 'Admin\EntriesController@crv_store', 'as' => 'crv_store']);
        Route::get('crv/search', ['uses' => 'Admin\EntriesController@crv_search', 'as' => 'crv_search']);
        // Cash Payment Voucher Routes
        Route::get('cpv/create', ['uses' => 'Admin\EntriesController@cpv_create', 'as' => 'cpv_create']);
        Route::post('cpv/store', ['uses' => 'Admin\EntriesController@cpv_store', 'as' => 'cpv_store']);
        Route::get('cpv/search', ['uses' => 'Admin\EntriesController@cpv_search', 'as' => 'cpv_search']);
        // Bank Receipt Voucher Routes
        Route::get('brv/create', ['uses' => 'Admin\EntriesController@brv_create', 'as' => 'brv_create']);
        Route::post('brv/store', ['uses' => 'Admin\EntriesController@brv_store', 'as' => 'brv_store']);
        Route::get('brv/search', ['uses' => 'Admin\EntriesController@brv_search', 'as' => 'brv_search']);
        // Bank Payment Voucher Routes
        Route::get('bpv/create', ['uses' => 'Admin\EntriesController@bpv_create', 'as' => 'bpv_create']);
        Route::post('bpv/store', ['uses' => 'Admin\EntriesController@bpv_store', 'as' => 'bpv_store']);
        Route::get('bpv/search', ['uses' => 'Admin\EntriesController@bpv_search', 'as' => 'bpv_search']);
        // LC Payment Voucher Routes
        Route::get('lcpv/create', ['uses' => 'Admin\EntriesController@lcpv_create', 'as' => 'lcpv_create']);
        Route::post('lcpv/store', ['uses' => 'Admin\EntriesController@lcpv_store', 'as' => 'lcpv_store']);
        Route::get('lcpv/search', ['uses' => 'Admin\EntriesController@lcpv_search', 'as' => 'lcpv_search']);
        // LC Product Voucher Routes
        Route::get('lrp/create', ['uses' => 'Admin\EntriesController@lrp_create', 'as' => 'lrp_create']);
        Route::post('lrp/store', ['uses' => 'Admin\EntriesController@lrp_store', 'as' => 'lrp_store']);
        Route::get('lrp_search/{listvalue}', ['uses' => 'Admin\EntriesController@lrp_search', 'as' => 'lrp_search']);
    });

    Route::group(['prefix' => 'recruitment', 'as' => 'recruitment.'], function () {

        Route::get('job-request', ['uses' => 'HRM\JobPostRequestController@index', 'as' => 'job-request']);
        Route::get('job-request-create', ['uses' => 'HRM\JobPostRequestController@create', 'as' => 'job-request-create']);
        Route::post('job-request-store', ['uses' => 'HRM\JobPostRequestController@store', 'as' => 'job-request-store']);
        Route::get('job-request-edit/{id}', ['uses' => 'HRM\JobPostRequestController@edit', 'as' => 'job-request-edit']);
        Route::post('job-request-update', ['uses' => 'HRM\JobPostRequestController@update', 'as' => 'job-request-update']);

        Route::get('job-post', ['uses' => 'HRM\JobPostController@index', 'as' => 'job-post']);
        Route::get('job-post-create/{id}', ['uses' => 'HRM\JobPostController@create', 'as' => 'job-post-create']);
        Route::post('job-post-store', ['uses' => 'HRM\JobPostController@store', 'as' => 'job-post-store']);
        Route::get('job-post-edit/{id}', ['uses' => 'HRM\JobPostController@edit', 'as' => 'job-post-edit']);
        Route::post('job-post-update', ['uses' => 'HRM\JobPostController@update', 'as' => 'job-post-update']);
        Route::get('job-post/job-status-update-active/{id}', ['uses' => 'HRM\JobPostController@updateStatusActive', 'as' => 'job-status-update-active']);
        Route::get('job-post/job-status-update-inactive/{id}', ['uses' => 'HRM\JobPostController@updateStatusInactive', 'as' => 'job-status-update-inactive']);
    });


    // Accounts Reports Started
    Route::group(['prefix' => 'reports', 'as' => 'account_reports.'], function () {
        // Duty Calculation Report
        Route::get('duty-calculation', ['uses' => 'Admin\DutyCalculationController@addDuty', 'as' => 'duty_create']);
        Route::post('duty-calculation', ['uses' => 'Admin\DutyCalculationController@storeDuty', 'as' => 'duty_store']);
        Route::get('duty-report', ['uses' => 'Admin\DutyCalculationController@getDutyReport', 'as' => 'duty_report']);
        Route::delete('duty_delete/{id}', ['uses' => 'Admin\DutyCalculationController@destroyy', 'as' => 'duty_delete']);
        Route::get('duty_report_calculation', ['uses' => 'Admin\DutyCalculationController@calculateDuty', 'as' => 'duty_calculation_report']);
        // route for ajax
        Route::get('duty_report_all_products', ['uses' => 'Admin\DutyCalculationController@getProducts', 'as' => 'duty_calculation_allproducts']);
        Route::get('duty_report_all_prices', ['uses' => 'Admin\DutyCalculationController@getProductPrices', 'as' => 'duty_calculation_allprices']);

        // demand Calculation Report
        Route::get('dutydemand_report', ['uses' => 'Admin\DutyCalculationController@DutyDemand', 'as' => 'dutydemand_report']);
        Route::post('dutydemand_report', ['uses' => 'Admin\DutyCalculationController@getProductlist', 'as' => 'duty_demand']);

        // Profit & Lost Report
        Route::post('load-groups', ['uses' => 'Admin\AccountReportsController@load_groups', 'as' => 'load_groups']);
        Route::get('profit_loss', ['uses' => 'Admin\AccountReportsController@profit_loss', 'as' => 'profit_loss']);
        Route::post('profit_loss_report', ['uses' => 'Admin\AccountReportsController@profit_loss_report', 'as' => 'profit_loss_report']);



        //Comparative Profit & Loss Statement    
        Route::get('profit_loss_cmp', ['uses' => 'Admin\AccountReportsController@profit_loss_comparative', 'as' => 'profit_loss_cmp']);
        Route::post('profit_loss_report_cmp', ['uses' => 'Admin\AccountReportsController@profit_loss_report_comparative', 'as' => 'profit_loss_report_cmp']);


        // Trial Balance Report
        Route::get('trial-balance', ['uses' => 'Admin\AccountReportsController@trial_balance', 'as' => 'trial_balance']);
        Route::post('trial-balance-report', ['uses' => 'Admin\AccountReportsController@trial_balance_report', 'as' => 'trial_balance_report']);
        // Ledger Statement Report
        Route::post('load-ledgers', ['uses' => 'Admin\AccountReportsController@load_ledgers', 'as' => 'load_ledgers']);
        Route::get('ledger-statement', ['uses' => 'Admin\AccountReportsController@ledger_statement', 'as' => 'ledger_statement']);
        Route::post('ledger-statement-report', ['uses' => 'Admin\AccountReportsController@ledger_statement_report', 'as' => 'ledger_statement_report']);
        // Balance Sheet Report
   
        Route::get('ledger', ['uses' => 'Reports\LedgerController@index', 'as' => 'ledger']);
        Route::post('ledger_print', ['uses' => 'Reports\LedgerController@ledger_print', 'as' => 'ledger_print']);
        Route::post('income', ['uses' => 'Dashboard\DashboardController@get_income', 'as' => 'income']);
        Route::post('payable', ['uses' => 'Dashboard\DashboardController@get_payable', 'as' => 'payable']);
        
        Route::post('receivable', ['uses' => 'Dashboard\DashboardController@get_receivable', 'as' => 'receivable']);
        Route::get('trial_balance', ['uses' => 'Reports\TrialBalanceController@index', 'as' => 'trial_balance']);
        Route::post('print_trial_balance', ['uses' => 'Reports\TrialBalanceController@get_trial_balance', 'as' => 'print_trial_balance']);
        Route::get('expense_summary', ['uses' => 'Reports\ExpenseSummaryController@index', 'as' => 'expense_summary']);
        Route::post('print_expense_summary', ['uses' => 'Reports\ExpenseSummaryController@print_report', 'as' => 'print_expense_summary']);
        Route::get('balance-sheet', ['uses' => 'Admin\AccountReportsController@balance_sheet', 'as' => 'balance_sheet']);
        Route::post('balance-sheet-report', ['uses' => 'Admin\AccountReportsController@balance_sheet_report', 'as' => 'balance_sheet_report']);





        Route::get('balance_sheet_tree', ['uses' => 'Reports\BalanceSheetController@balance_sheet_tree', 'as' => 'balance_sheet_tree']);
        Route::get('get_balance_sheet_tree', ['uses' => 'Reports\BalanceSheetController@get_balance_sheet_tree', 'as' => 'get_balance_sheet_tree']);

    });


    // Financial Years Starts Here
    Route::patch('financial_years/active/{id}', ['uses' => 'Admin\FinancialYearsController@active', 'as' => 'financial_years.active']);
    Route::patch('financial_years/inactive/{id}', ['uses' => 'Admin\FinancialYearsController@inactive', 'as' => 'financial_years.inactive']);
    Route::resource('financial_years', 'Admin\FinancialYearsController');


    // Sales Invoices Starts Here
    Route::get('/salesreturn', ['uses' => 'Admin\SalesReturnController@index', 'as' => 'salesreturn.index']);
    Route::get('/salesreturn/create', ['uses' => 'Admin\SalesReturnController@create', 'as' => 'salesreturn.create']);
    Route::get('/salesreturn/view/{id}', ['uses' => 'Admin\SalesReturnController@view', 'as' => 'salesreturn.view']);
    Route::get('/salesreturn/view/downloadPDF/{id}', ['uses' => 'Admin\SalesReturnController@downloadPDF', 'as' => 'salesreturn.pdf']);
    Route::post('/salesreturn/store', ['uses' => 'Admin\SalesReturnController@store', 'as' => 'salesreturn.store']);
    Route::post('/salesreturn/getOrderDetail', ['uses' => 'Admin\SalesReturnController@getOrderDetail', 'as' => 'salesreturn.getOrderDetail']);
    Route::get('salesreturn/datatables', ['uses' => 'Admin\SalesReturnController@datatables', 'as' => 'salesreturn.datatables']);
    Route::post('salesreturn/confirm', ['uses' => 'Admin\SalesReturnController@confirm', 'as' => 'salesreturn.confirm']);
    Route::resource('salesreturn', 'Admin\SalesReturnController');
    // Financial Years Ends Here

    // Sales Order Starts Here
    Route::get('saleorder_create/{id}', ['uses' => 'Admin\SaleOrderController@createSaleOrder', 'as' => 'saleorder.createe']);

    Route::get('saleorder/qoutes', ['uses' => 'Admin\SaleOrderController@approvedQoutes', 'as' => 'saleorder.qoutes']);
    Route::get('saleorder/getProductsByName', ['uses' => 'Admin\SaleOrderController@getProductsByName', 'as' => 'saleorder.getProductsByName']);
    Route::get('saleorder/pdf/{id}', ['uses' => 'Admin\SaleOrderController@generateInvoice', 'as' => 'saleorder.generatePDF']);
    Route::resource('saleorder', 'Admin\SaleOrderController');

    Route::resource('country', 'Admin\CountryController');

    Route::resource('city', 'Admin\CityController');
    Route::resource('fee_section', 'Admin\FeeSectionController');
    Route::resource('fee_head', 'Admin\FeeHeadController');
    Route::resource('currency', 'Admin\CurrencyController');

    Route::resource('academic_year', 'Admin\AcademicYearController');
    Route::patch('academic_year/active/{id}', ['uses' => 'Admin\AcademicYearController@active', 'as' => 'academic_year.active']);
    Route::patch('academic_year/inactive/{id}', ['uses' => 'Admin\AcademicYearController@inactive', 'as' => 'academic_year.inactive']);

    //    Route::resource('program', 'Admin\ProgramController');
    //    Route::post('program_mass_destroy', ['uses' => 'Admin\ProgramController@massDestroy', 'as' => 'program.mass_destroy']);
});

//Public route for job posts

Route::get('job-post/{id}', 'HRM\JobPostController@showPublicPost');
Route::resource('candidate-form', 'HRM\CandidateFormController');
Route::get('candidate-form-create/{id}', 'HRM\CandidateFormController@create')->name('candidate-form-create');


//Performance Management



//Bank Account Requests Started Here

Route::get('/bank_account_request-list', 'HRM\StaffController@bank_account_requests');
Route::get('/print_bank_account_request-list/{id}', 'HRM\StaffController@print_account_request');
Route::get('/update_staff_account_details/{id}', 'HRM\StaffController@update_staff_account_details_view');
Route::post('/update_staff_account_details','HRM\StaffController@update_staff_account_details_submit');


//Bank Account Requests Started Here



Route::resource('staff-performance', 'HRM\StaffPerformanceController');
Route::get('staff-performance/feedback/{id}', 'HRM\StaffPerformanceController@feedback')->name('staff-performance.feedback');
Route::get('staff-performance/feedback_view/{id}', 'HRM\StaffPerformanceController@feedbackView')->name('staff-performance.feedback_view');
Route::get('staff-performance-detail', 'HRM\StaffPerformanceController@performanceDetail')->name('staff-performance.detail');
Route::get('resign-request-list', 'HRM\StaffPerformanceController@resignList')->name('resign-request-list');

Route::get('/resignrequestceo/approve/{id}', ['uses' => 'HRM\ResignController@resignRequestCeoApprove', 'as' => 'resignrequestceo.approve']);
Route::get('/resignrequestceo/reject/{id}', ['uses' => 'HRM\ResignController@resignRequestCeoReject', 'as' => 'resignrequestceo.reject']);

Route::get('/resignrequesthr/approve/{id}', ['uses' => 'HRM\ResignController@resignRequestHrApprove', 'as' => 'resignrequesthr.approve']);
Route::get('/resignrequesthr/reject/{id}', ['uses' => 'HRM\ResignController@resignRequestHrReject', 'as' => 'resignrequesthr.reject']);

Route::get('my_resign-request-list', 'HRM\ResignController@myResign')->name('my-resign-request-list');

Route::get('experience_letter_send/{id}', 'HRM\ResignController@experienceLetterSend')->name('experience_letter_send');
Route::post('experience_letter_store', 'HRM\ResignController@experienceLetterStore')->name('experience_letter_store');

Route::get('upload_document/{id}', 'HRM\ResignController@uploadDocument')->name('upload_document');

Route::post('upload_document_experience', 'HRM\ResignController@uploadDocumentExperience')->name('upload_document_experience');

Route::get('upload_clearance_form/{id}', 'HRM\ResignController@uploadClearanceFrom')->name('upload_clearance_form');

Route::post('upload_clearance_form_file', 'HRM\ResignController@uploadClearanceFromFile')->name('upload_clearance_form_file');

Route::post('get-boards-against-branch','HierarchyController@getBoards')->name('get-boards-against-branch');
Route::post('get-programs-against-board','HierarchyController@getPrograms')->name('get-programs-against-board');
Route::post('get-class-against-program','HierarchyController@getClasses')->name('get-class-against-program');
Route::post('get-sections-against-class','HierarchyController@getSections')->name('get-sections-against-class');

Route::post('get-courses-againts-program','HierarchyController@getCourses')->name('get-courses-againts-program');

Route::get('/find_board','HierarchyController@get_board')->name('find_board');

Route::get('/find_student_board','HierarchyController@get_student_board')->name('find_student_board');

 Route::get('/find_student_board_program','HierarchyController@get_program')->name('find_student_board_program');

Route::get('/find_asset','Admin\FixAssetsController@get_asset')->name('find_asset');



 Route::get('/find_student_board_program_class','HierarchyController@get_student_board_program_class');
 Route::get('/find_student_board_program_class_section','HierarchyController@get_student_board_program_class_section');

// tax Module Routes

Route::group(['middleware' => ['auth'], 'prefix' => 'hrm', 'as' => 'hrm.'], function () {

    // Leave Performance Reports starts here
    Route::get('leave_performance_report', ['uses' => 'HRM\Reports\HrReportsController@leavePerformanceIndex', 'as' => 'leave_performance.report']);
    Route::post('leave_performance_report', ['uses' => 'HRM\Reports\HrReportsController@getLeavePerformanceReport', 'as' => 'leave_performance.post']);
    Route::post('leave_performance_report_pdf', ['uses' => 'HRM\Reports\HrReportsController@getLeavePerformanceReportPDF', 'as' => 'leave_performance.report.pdf']);


    // Attendance Reports starts here
    Route::get('attendance_report', ['uses' => 'HRM\Reports\HrReportsController@index', 'as' => 'attendance.report']);
    Route::post('attendance_report', ['uses' => 'HRM\Reports\HrReportsController@getSimpleReport', 'as' => 'attendance.post']);
    Route::get('attendance_report_detail/{id}', ['uses' => 'HRM\Reports\HrReportsController@detail', 'as' => 'attendance.report.detail']);
    Route::get('attendance_reportpdf/{id}', ['uses' => 'HRM\Reports\HrReportsController@pdfReport', 'as' => 'attendance_report.detail.pdf']);

    //Leave Reports starts here
    Route::get('leave_report', ['uses' => 'HRM\Reports\HrReportsController@leaveindex', 'as' => 'leave.report']);
    Route::post('leave_report', ['uses' => 'HRM\Reports\HrReportsController@getSimpleLeaveReport', 'as' => 'leave.post']);
    Route::get('leave_report_detail/{id}', ['uses' => 'HRM\Reports\HrReportsController@leavedetail', 'as' => 'leave.report.detail']);
    Route::get('leave_reportpdf/{id}', ['uses' => 'HRM\Reports\HrReportsController@leavePdfReport', 'as' => 'leave_report.detail.pdf']);

    // Pay Grades Resrouce Starts Here
    Route::patch('pay_grades/active/{id}', ['uses' => 'HRM\PayGradesController@active', 'as' => 'pay_grades.active']);
    Route::patch('pay_grades/inactive/{id}', ['uses' => 'HRM\PayGradesController@inactive', 'as' => 'pay_grades.inactive']);
    Route::resource('pay_grades', 'HRM\PayGradesController');
    // Pay Grades Resrouce Ends Here
    // Pay Roll Resrouce Starts Here
    Route::patch('pay_roll/active/{id}', ['uses' => 'HRM\PayRollController@active', 'as' => 'pay_roll.active']);
    Route::patch('pay_roll/inactive/{id}', ['uses' => 'HRM\PayRollController@inactive', 'as' => 'pay_roll.inactive']);
    Route::resource('pay_roll', 'HRM\PayRollController');
    // Pay Grades Resrouce Ends Here

    // Employment Statuses Resrouce Starts Here
    Route::patch('employment_statuses/active/{id}', ['uses' => 'HRM\EmploymentStatusesController@active', 'as' => 'employment_statuses.active']);
    Route::patch('employment_statuses/inactive/{id}', ['uses' => 'HRM\EmploymentStatusesController@inactive', 'as' => 'employment_statuses.inactive']);
    Route::resource('employment_statuses', 'HRM\EmploymentStatusesController');
    // Employment Statuses Resrouce Ends Here

    // Job Categories Resrouce Starts Here
    Route::patch('job_categories/active/{id}', ['uses' => 'HRM\JobCategoriesController@active', 'as' => 'job_categories.active']);
    Route::patch('job_categories/inactive/{id}', ['uses' => 'HRM\JobCategoriesController@inactive', 'as' => 'job_categories.inactive']);
    Route::resource('job_categories', 'HRM\JobCategoriesController');
    // Job Categories Resrouce Ends Here

    // Job Categories Resrouce Starts Here
    Route::patch('job_title/active/{id}', ['uses' => 'HRM\JobTitleController@active', 'as' => 'job_title.active']);
    Route::patch('job_title/inactive/{id}', ['uses' => 'HRM\JobTitleController@inactive', 'as' => 'job_title.inactive']);
    Route::resource('job_title', 'HRM\JobTitleController');
    // Job Categories Resrouce Ends Here

    // Work Shifts Resrouce Starts Here
    Route::patch('work_shifts/active/{id}', ['uses' => 'HRM\WorkShiftsController@active', 'as' => 'work_shifts.active']);
    Route::patch('work_shifts/inactive/{id}', ['uses' => 'HRM\WorkShiftsController@inactive', 'as' => 'work_shifts.inactive']);
    Route::resource('work_shifts', 'HRM\WorkShiftsController');
    // Work Shifts Resrouce Ends Here

    // Organizations Resrouce Starts Here
    Route::patch('organizations/active/{id}', ['uses' => 'HRM\OrganizationsController@active', 'as' => 'organizations.active']);
    Route::patch('organizations/inactive/{id}', ['uses' => 'HRM\OrganizationsController@inactive', 'as' => 'organizations.inactive']);
    Route::resource('organizations', 'HRM\OrganizationsController');
    // Organizations Resrouce Ends Here

    // Organization Locations Resrouce Starts Here
    Route::patch('organization_locations/active/{id}', ['uses' => 'HRM\OrganizationLocationsController@active', 'as' => 'organization_locations.active']);
    Route::patch('organization_locations/inactive/{id}', ['uses' => 'HRM\OrganizationLocationsController@inactive', 'as' => 'organization_locations.inactive']);
    Route::resource('organization_locations', 'HRM\OrganizationLocationsController');
    // Organization Locations Resrouce Ends Here

    // Skills Resrouce Starts Here
    Route::patch('skills/active/{id}', ['uses' => 'HRM\SkillsController@active', 'as' => 'skills.active']);
    Route::patch('skills/inactive/{id}', ['uses' => 'HRM\SkillsController@inactive', 'as' => 'skills.inactive']);
    Route::resource('skills', 'HRM\SkillsController');
    // Skills Resrouce Ends Here

    // Educations Resrouce Starts Here
    Route::patch('educations/active/{id}', ['uses' => 'HRM\EducationsController@active', 'as' => 'educations.active']);
    Route::patch('educations/inactive/{id}', ['uses' => 'HRM\EducationsController@inactive', 'as' => 'educations.inactive']);
    Route::resource('educations', 'HRM\EducationsController');
    // Educations Resrouce Ends Here

    // Licenses Resrouce Starts Here
    Route::patch('licenses/active/{id}', ['uses' => 'HRM\LicensesController@active', 'as' => 'licenses.active']);
    Route::patch('licenses/inactive/{id}', ['uses' => 'HRM\LicensesController@inactive', 'as' => 'licenses.inactive']);
    Route::resource('licenses', 'HRM\LicensesController');
    // Licenses Resrouce Ends Here

    // Languages Resrouce Starts Here
    Route::patch('languages/active/{id}', ['uses' => 'HRM\LanguagesController@active', 'as' => 'languages.active']);
    Route::patch('languages/inactive/{id}', ['uses' => 'HRM\LanguagesController@inactive', 'as' => 'languages.inactive']);
    Route::resource('languages', 'HRM\LanguagesController');
    // Languages Resrouce Ends Here

    // Memberships Resrouce Starts Here
    Route::patch('memberships/active/{id}', ['uses' => 'HRM\MembershipsController@active', 'as' => 'memberships.active']);
    Route::patch('memberships/inactive/{id}', ['uses' => 'HRM\MembershipsController@inactive', 'as' => 'memberships.inactive']);
    Route::resource('memberships', 'HRM\MembershipsController');
    // Memberships Resrouce Ends Here

    // Leave Periods Resrouce Starts Here
    Route::resource('leave_periods', 'HRM\LeavePeriodsController');
    // Leave Periods Resrouce Ends Here

    // Leave Types Resrouce Starts Here
    Route::patch('leave_types/active/{id}', ['uses' => 'HRM\LeaveTypesController@active', 'as' => 'leave_types.active']);
    Route::patch('leave_types/inactive/{id}', ['uses' => 'HRM\LeaveTypesController@inactive', 'as' => 'leave_types.inactive']);
    Route::resource('leave_types', 'HRM\LeaveTypesController');
    // Leave Types Resrouce Ends Here

    // Work Weeks Resrouce Starts Here
    Route::resource('work_weeks', 'HRM\WorkWeeksController');
    // Work Weeks Resrouce Ends Here

    // Holidays Resrouce Starts Here
    Route::patch('holidays/active/{id}', ['uses' => 'HRM\HolidaysController@active', 'as' => 'holidays.active']);
    Route::patch('holidays/inactive/{id}', ['uses' => 'HRM\HolidaysController@inactive', 'as' => 'holidays.inactive']);
    Route::resource('holidays', 'HRM\HolidaysController');
    // Holidays Resrouce Ends Here

    // Leave Entitlements Resrouce Starts Here
    Route::get('leave_entitlements/employee_search', ['uses' => 'HRM\LeaveEntitlementsController@employee_search', 'as' => 'leave_entitlements.employee_search']);
    Route::resource('leave_entitlements', 'HRM\LeaveEntitlementsController');
    // Leave Entitlements Resrouce Ends Here

    // Leave Requests Resrouce Starts Here
    Route::get('leave_requests/employee_search', ['uses' => 'HRM\LeaveRequestsController@employee_search', 'as' => 'leave_requests.employee_search']);
    Route::get('leave_requests/leave_balance', ['uses' => 'HRM\LeaveRequestsController@leave_balance', 'as' => 'leave_requests.leave_balance']);
    Route::patch('leave_requests/active/{id}', ['uses' => 'HRM\LeaveRequestsController@active', 'as' => 'leave_requests.active']);
    Route::patch('leave_requests/inactive/{id}', ['uses' => 'HRM\LeaveRequestsController@inactive', 'as' => 'leave_requests.inactive']);
    Route::resource('leave_requests', 'HRM\LeaveRequestsController');
    // Leave Requests Resrouce Ends Here

    // Leaves  Resrouce Starts Here
    Route::get('leaves/employee_available', ['uses' => 'HRM\LeavesController@employee_available', 'as' => 'leaves.employee_available']);
    Route::post('leaves/available_result', ['uses' => 'HRM\LeavesController@available_result', 'as' => 'leaves.available_result']);
    Route::get('leaves/employee_search', ['uses' => 'HRM\LeavesController@employee_search', 'as' => 'leaves.employee_search']);
    Route::get('leaves/leave_balance', ['uses' => 'HRM\LeavesController@leave_balance', 'as' => 'leaves.leave_balance']);
    Route::patch('leaves/active/{id}', ['uses' => 'HRM\LeavesController@active', 'as' => 'leaves.active']);
    Route::patch('leaves/inactive/{id}', ['uses' => 'HRM\LeavesController@inactive', 'as' => 'leaves.inactive']);
    Route::post('leaves/change_status', ['uses' => 'HRM\LeavesController@change_status', 'as' => 'leaves.change_status']);
    Route::get('leaves/leave_cron/{date}', ['uses' => 'HRM\LeavesController@leave_cron', 'as' => 'leaves.leave_cron']);

    Route::get('leaves/approved', ['uses' => 'HRM\LeavesController@approved', 'as' => 'leaves.approved']);

    Route::resource('leaves', 'HRM\LeavesController');
    // Leaves  Resrouce Ends Here



    // Employees Resrouce Starts Here
    Route::patch('employees/active/{id}', ['uses' => 'HRM\EmployeesController@active', 'as' => 'employees.active']);

    Route::patch('employees/inactive/{id}', ['uses' => 'HRM\EmployeesController@inactive', 'as' => 'employees.inactive']);
    Route::resource('employees', 'HRM\EmployeesController');
    // Employees Resrouce Ends Here

    Route::get('attendance/sync_attendance', ['uses' => 'HRM\EmployeeAttendanceController@syncAttendance', 'as' => 'attendance.sync']);

    Route::get('attendance/requests', ['uses' => 'HRM\EmployeeAttendanceController@requests', 'as' => 'attendance.requests']);
    Route::post('attendance/delete', ['uses' => 'HRM\EmployeeAttendanceController@delete', 'as' => 'attendance.delete']);
    Route::get('attendance/active', ['uses' => 'HRM\EmployeeAttendanceController@active', 'as' => 'attendance.active']);
    Route::post('attendance/inactive', ['uses' => 'HRM\EmployeeAttendanceController@inactive', 'as' => 'attendance.inactive']);
    Route::post('attendance/datatables', ['uses' => 'HRM\EmployeeAttendanceController@datatables', 'as' => 'attendance.datatables']);
    Route::post('attendance/datatables1', ['uses' => 'HRM\EmployeeAttendanceController@datatables1', 'as' => 'attendance.datatables1']);
    Route::get('attendance/details/{id}', ['uses' => 'HRM\EmployeeAttendanceController@details', 'as' => 'attendance.details']);
    Route::get('attendance/transfer', ['uses' => 'HRM\EmployeeAttendanceController@transfer', 'as' => 'attendance.transfer']);
    Route::post('attendance/transfer_datatables', ['uses' => 'HRM\EmployeeAttendanceController@transfer_datatables', 'as' => 'attendance.transfer_datatables']);
    Route::post('attendance/transfer_attendance', ['uses' => 'HRM\EmployeeAttendanceController@transfer_attendance', 'as' => 'attendance.transfer_attendance']);
    Route::resource('attendance', 'HRM\EmployeeAttendanceController');

   // Route::resource('attendance', 'HRM\EmployeeAttendanceController');

    Route::post('payroll/delete', ['uses' => 'HRM\PayrollController@destroy', 'as' => 'payroll.delete']);
    Route::post('payroll/active', ['uses' => 'HRM\PayrollController@active', 'as' => 'payroll.active']);
    Route::post('payroll/inactive', ['uses' => 'HRM\PayrollController@inactive', 'as' => 'payroll.inactive']);
    Route::post('payroll/datatables', ['uses' => 'HRM\PayrollController@datatables', 'as' => 'payroll.datatables']);
    Route::get('payroll/details/{id}', ['uses' => 'HRM\PayrollController@details', 'as' => 'payroll.details']);
    Route::post('payroll/getEmployeeDetail', ['uses' => 'HRM\PayrollController@getEmployeeDetail', 'as' => 'payroll.getEmployeeDetail']);
    Route::post('payroll/transfer_datatables', ['uses' => 'HRM\PayrollController@transfer_datatables', 'as' => 'payroll.transfer_datatables']);
    Route::post('payroll/transfer_payroll', ['uses' => 'HRM\PayrollController@transfer_payroll', 'as' => 'payroll.transfer_payroll']);
    Route::get('salary_sheet', ['uses' => 'HRM\PayrollController@salary_sheet', 'as' => 'payroll.salary_sheet']);
    Route::post('payroll_report', ['uses' => 'HRM\PayrollController@payroll_report', 'as' => 'payroll.payroll_report']);
    Route::post('calculate_payroll', ['uses' => 'HRM\PayrollController@calculate_payroll', 'as' => 'payroll.calculate_payroll']);

    Route::resource('payroll', 'HRM\PayrollController');

    Route::post('payroll/get_bank_name', ['uses' => 'HRM\PayrollController@getBankName', 'as' => 'payroll.get_bank_name']);


    // Leave Types Resrouce Starts Here
    Route::patch('attendance_settings/active/{id}', ['uses' => 'HRM\HrmAttendanceSettingsController@active', 'as' => 'attendance_settings.active']);
    Route::patch('attendance_settings/inactive/{id}', ['uses' => 'HRM\HrmAttendanceSettingsController@inactive', 'as' => 'attendance_settings.inactive']);
    Route::resource('attendance_settings', 'HRM\HrmAttendanceSettingsController');
    // Leave Types Resrouce Ends Here

    // Employee Resrouce Starts Here
    Route::post('employee_resources/delete', ['uses' => 'HRM\EmployeeResourcesController@delete', 'as' => 'employee_resources.delete']);
    Route::post('employee_resources/active', ['uses' => 'HRM\EmployeeResourcesController@active', 'as' => 'employee_resources.active']);
    Route::post('employee_resources/inactive', ['uses' => 'HRM\EmployeeResourcesController@inactive', 'as' => 'employee_resources.inactive']);
    Route::post('employee_resources/datatables', ['uses' => 'HRM\EmployeeResourcesController@datatables', 'as' => 'employee_resources.datatables']);
    Route::get('employee_resources/details/{id}', ['uses' => 'HRM\EmployeeResourcesController@details', 'as' => 'employee_resources.details']);
    Route::post('employee_resources/getEmployeeDetail', ['uses' => 'HRM\EmployeeResourcesController@getEmployeeDetail', 'as' => 'employee_resources.getEmployeeDetail']);
    Route::post('employee_resources/transfer_datatables', ['uses' => 'HRM\EmployeeResourcesController@transfer_datatables', 'as' => 'employee_resources.transfer_datatables']);
    Route::post('employee_resources/transfer_employee_resources', ['uses' => 'HRM\EmployeeResourcesController@transfer_employee_resources', 'as' => 'employee_resources.transfer_employee_resources']);
    Route::resource('employee_resources', 'HRM\EmployeeResourcesController');


    // Employee Banks Starts Here
    Route::post('employee_banks/delete', ['uses' => 'HRM\EmployeeBanksController@delete', 'as' => 'employee_banks.delete']);
    Route::post('employee_banks/active', ['uses' => 'HRM\EmployeeBanksController@active', 'as' => 'employee_banks.active']);
    Route::post('employee_banks/inactive', ['uses' => 'HRM\EmployeeBanksController@inactive', 'as' => 'employee_banks.inactive']);
    Route::post('employee_banks/datatables', ['uses' => 'HRM\EmployeeBanksController@datatables', 'as' => 'employee_banks.datatables']);
    Route::get('employee_banks/details/{id}', ['uses' => 'HRM\EmployeeBanksController@details', 'as' => 'employee_banks.details']);
    Route::post('employee_banks/getEmployeeDetail', ['uses' => 'HRM\EmployeeBanksController@getEmployeeDetail', 'as' => 'employee_banks.getEmployeeDetail']);
    Route::post('employee_banks/transfer_datatables', ['uses' => 'HRM\EmployeeBanksController@transfer_datatables', 'as' => 'employee_banks.transfer_datatables']);
    Route::post('employee_banks/transfer_employee_banks', ['uses' => 'HRM\EmployeeBanksController@transfer_employee_banks', 'as' => 'employee_banks.transfer_employee_banks']);
    Route::resource('employee_banks', 'HRM\EmployeeBanksController');

    // Employee Resrouce Starts Here
    Route::post('gratuity/delete', ['uses' => 'HRM\GratuityController@delete', 'as' => 'gratuity.delete']);
    Route::post('gratuity/active', ['uses' => 'HRM\GratuityController@active', 'as' => 'gratuity.active']);
    Route::post('gratuity/inactive', ['uses' => 'HRM\GratuityController@inactive', 'as' => 'gratuity.inactive']);
    Route::post('gratuity/datatables', ['uses' => 'HRM\GratuityController@datatables', 'as' => 'gratuity.datatables']);
    Route::get('gratuity/details/{id}', ['uses' => 'HRM\GratuityController@details', 'as' => 'gratuity.details']);
    Route::post('gratuity/getGratuityDetail', ['uses' => 'HRM\GratuityController@getGratuityDetail', 'as' => 'gratuity.getGratuityDetail']);
    Route::post('gratuity/transfer_datatables', ['uses' => 'HRM\GratuityController@transfer_datatables', 'as' => 'gratuity.transfer_datatables']);
    Route::post('gratuity/transfer_gratuity', ['uses' => 'HRM\GratuityController@transfer_gratuity', 'as' => 'gratuity.transfer_gratuity']);
    Route::resource('gratuity', 'HRM\GratuityController');



    // Employee Resrouce Starts Here
    Route::post('employee_target/delete', ['uses' => 'HRM\EmployeeTargetController@delete', 'as' => 'employee_target.delete']);
    Route::post('employee_target/active', ['uses' => 'HRM\EmployeeTargetController@active', 'as' => 'employee_target.active']);
    Route::post('employee_target/inactive', ['uses' => 'HRM\EmployeeTargetController@inactive', 'as' => 'employee_target.inactive']);
    Route::post('employee_target/datatables', ['uses' => 'HRM\EmployeeTargetController@datatables', 'as' => 'employee_target.datatables']);
    Route::get('employee_target/details/{id}', ['uses' => 'HRM\EmployeeTargetController@details', 'as' => 'employee_target.details']);
    Route::post('employee_target/getEmployeeDetail', ['uses' => 'HRM\EmployeeTargetController@getEmployeeDetail', 'as' => 'employee_target.getEmployeeDetail']);
    Route::post('employee_target/transfer_datatables', ['uses' => 'HRM\EmployeeTargetController@transfer_datatables', 'as' => 'employee_target.transfer_datatables']);
    Route::post('employee_target/transfer_employee_target', ['uses' => 'HRM\EmployeeTargetController@transfer_employee_target', 'as' => 'employee_target.transfer_employee_target']);
    Route::resource('employee_target', 'HRM\EmployeeTargetController');



    // Employee Resrouce Starts Here

    Route::post('assign_target/ytdValue', ['uses' => 'HRM\AssignTargetController@ytdValue', 'as' => 'assign_target.ytdValue']);
    Route::get('assign_target/ytd', ['uses' => 'HRM\AssignTargetController@ytd', 'as' => 'assign_target.ytd']);
    Route::post('assign_target/delete', ['uses' => 'HRM\AssignTargetController@delete', 'as' => 'assign_target.delete']);
    Route::post('assign_target/active', ['uses' => 'HRM\AssignTargetController@active', 'as' => 'assign_target.active']);
    Route::post('assign_target/inactive', ['uses' => 'HRM\AssignTargetController@inactive', 'as' => 'assign_target.inactive']);
    Route::post('assign_target/datatables', ['uses' => 'HRM\AssignTargetController@datatables', 'as' => 'assign_target.datatables']);
    Route::get('assign_target/details/{id}', ['uses' => 'HRM\AssignTargetController@details', 'as' => 'assign_target.details']);
    Route::post('assign_target/getEmployeeDetail', ['uses' => 'HRM\AssignTargetController@getEmployeeDetail', 'as' => 'assign_target.getEmployeeDetail']);
    Route::post('assign_target/transfer_datatables', ['uses' => 'HRM\AssignTargetController@transfer_datatables', 'as' => 'assign_target.transfer_datatables']);
    Route::post('assign_target/transfer_assign_target', ['uses' => 'HRM\AssignTargetController@transfer_assign_target', 'as' => 'assign_target.transfer_assign_target']);
    Route::resource('assign_target', 'HRM\AssignTargetController');


    // Employee Resrouce Starts Here
    Route::post('employee_tada/delete', ['uses' => 'HRM\EmployeeTadaController@delete', 'as' => 'employee_tada.delete']);
    Route::post('employee_tada/active', ['uses' => 'HRM\EmployeeTadaController@active', 'as' => 'employee_tada.active']);
    Route::post('employee_tada/inactive', ['uses' => 'HRM\EmployeeTadaController@inactive', 'as' => 'employee_tada.inactive']);
    Route::post('employee_tada/datatables', ['uses' => 'HRM\EmployeeTadaController@datatables', 'as' => 'employee_tada.datatables']);
    Route::get('employee_tada/details/{id}', ['uses' => 'HRM\EmployeeTadaController@details', 'as' => 'employee_tada.details']);
    Route::post('employee_tada/getEmployeeDetail', ['uses' => 'HRM\EmployeeTadaController@getEmployeeDetail', 'as' => 'employee_tada.getEmployeeDetail']);
    Route::post('employee_tada/transfer_datatables', ['uses' => 'HRM\EmployeeTadaController@transfer_datatables', 'as' => 'employee_tada.transfer_datatables']);
    Route::post('employee_tada/transfer_employee_tada', ['uses' => 'HRM\EmployeeTadaController@transfer_employee_tada', 'as' => 'employee_tada.transfer_employee_tada']);
    Route::resource('employee_tada', 'HRM\EmployeeTadaController');

    // Employee Resrouce Starts Here
    Route::post('employees_tada_amount/delete', ['uses' => 'HRM\EmployeesTadaAmountController@delete', 'as' => 'employee_tada.delete']);
    Route::post('employees_tada_amount/active', ['uses' => 'HRM\EmployeesTadaAmountController@active', 'as' => 'employees_tada_amount.active']);
    Route::post('employees_tada_amount/inactive', ['uses' => 'HRM\EmployeesTadaAmountController@inactive', 'as' => 'employees_tada_amount.inactive']);
    Route::post('employees_tada_amount/datatables', ['uses' => 'HRM\EmployeesTadaAmountController@datatables', 'as' => 'employees_tada_amount.datatables']);
    Route::get('employees_tada_amount/details/{id}', ['uses' => 'HRM\EmployeesTadaAmountController@details', 'as' => 'employees_tada_amount.details']);
    Route::post('employees_tada_amount/getEmployeeDetail', ['uses' => 'HRM\EmployeesTadaAmountController@getEmployeeDetail', 'as' => 'employees_tada_amount.getEmployeeDetail']);
    Route::post('employees_tada_amount/transfer_datatables', ['uses' => 'HRM\EmployeesTadaAmountController@transfer_datatables', 'as' => 'employees_tada_amount.transfer_datatables']);
    Route::post('employees_tada_amount/transfer_employees_tada_amount', ['uses' => 'HRM\EmployeesTadaAmountController@transfer_employees_tada_amount', 'as' => 'employees_tada_amount.transfer_employees_tada_amount']);
    Route::resource('employees_tada_amount', 'HRM\EmployeesTadaAmountController');

    // Employee Resrouce Starts Here
    Route::post('employees_suggestion/delete', ['uses' => 'HRM\EmployeesSuggestionController@delete', 'as' => 'employee_tada.delete']);
    Route::post('employees_suggestion/active', ['uses' => 'HRM\EmployeesSuggestionController@active', 'as' => 'employees_suggestion.active']);
    Route::post('employees_suggestion/inactive', ['uses' => 'HRM\EmployeesSuggestionController@inactive', 'as' => 'employees_suggestion.inactive']);
    Route::post('employees_suggestion/datatables', ['uses' => 'HRM\EmployeesSuggestionController@datatables', 'as' => 'employees_suggestion.datatables']);
    Route::get('employees_suggestion/details/{id}', ['uses' => 'HRM\EmployeesSuggestionController@details', 'as' => 'employees_suggestion.details']);
    Route::post('employees_suggestion/getEmployeeDetail', ['uses' => 'HRM\EmployeesSuggestionController@getEmployeeDetail', 'as' => 'employees_suggestion.getEmployeeDetail']);
    Route::post('employees_suggestion/transfer_datatables', ['uses' => 'HRM\EmployeesSuggestionController@transfer_datatables', 'as' => 'employees_suggestion.transfer_datatables']);
    Route::post('employees_suggestion/transfer_employees_suggestion', ['uses' => 'HRM\EmployeesSuggestionController@transfer_employees_suggestion', 'as' => 'employees_suggestion.transfer_employees_suggestion']);
    Route::resource('employees_suggestion', 'HRM\EmployeesSuggestionController');

    //Employee Overtime Resource Starts Here
    Route::resource('employees_overtime','HRM\EmployeesOvertimeController');
    Route::post('employees_overtime/datatables', ['uses' => 'HRM\EmployeesOvertimeController@datatables', 'as' => 'employees_overtime.datatables']);
    Route::post('employees_overtime/getEmployeeDetail', ['uses' => 'HRM\EmployeesOvertimeController@getEmployeeDetail', 'as' => 'employees_overtime.getEmployeeDetail']);
    Route::post('employees_overtime/{id}/delete', ['uses' => 'HRM\EmployeesOvertimeController@destroy', 'as' => 'employees_overtime.destroy']);

    //Employee Document Types Resource Starts Here
    Route::post('employee_document_types/datatables', ['uses' => 'HRM\EmployeeDocumentTypesController@datatables', 'as' => 'employee_document_types.datatables']);
    Route::resource('employee_document_types', 'HRM\EmployeeDocumentTypesController');
    Route::post('employee_document_types/{id}/delete', ['uses' => 'HRM\EmployeeDocumentTypesController@destroy', 'as' => 'employee_document_types.destroy']);
    //Employee Document Types Resource Ends Here

    //Employee Documents Resource Starts Here
    Route::post('employee_documents/datatables', ['uses' => 'HRM\EmployeeDocumentsController@datatables', 'as' => 'employee_documents.datatables']);
    Route::resource('employee_documents', 'HRM\EmployeeDocumentsController');
    Route::post('employee_documents/{id}/delete', ['uses' => 'HRM\EmployeeDocumentsController@destroy', 'as' => 'employee_documents.destroy']);
    Route::get('employee_documents/{id}/view', ['uses' => 'HRM\EmployeeDocumentsController@view', 'as' => 'employee_documents.view']);
    //Employee Documents Resource Ends Here

    //Advance Payroll Resource Starts Here
    Route::post('advance_payroll/datatables', ['uses' => 'HRM\AdvancePayrollController@datatables', 'as' => 'advance_payroll.datatables']);
    Route::resource('advance_payroll', 'HRM\AdvancePayrollController');
    Route::post('advance_payroll/{id}/delete', ['uses' => 'HRM\AdvancePayrollController@destroy', 'as' => 'advance_payroll.destroy']);
    //Advance Payroll Resource Ends Here

    //Workdays Resrouce Starts Here
    Route::post('workdays/{id}/delete', ['uses' => 'HRM\WorkdaysController@destroy', 'as' => 'workdays.destroy']);
    Route::post('workdays/datatables', ['uses' => 'HRM\WorkdaysController@datatables', 'as' => 'workdays.datatables']);
    Route::post('workdays/active', ['uses' => 'HRM\WorkdaysController@active', 'as' => 'workdays.active']);
    Route::post('workdays/inactive', ['uses' => 'HRM\WorkdaysController@inactive', 'as' => 'workdays.inactive']);
    Route::resource('workdays', 'HRM\WorkdaysController');
    //Workdays Resrouce Ends Here

    //Employee Working Days Starts Here

    Route::get('employee_working_days', ['uses' => 'HRM\EmployeeWorkingDaysController@index', 'as' => 'employee_working_days']);
    Route::post('employee_working_days/employee_in_outs', ['uses' => 'HRM\EmployeeWorkingDaysController@getEmployeeInOuts', 'as' => 'employee_working_days.employee_in_outs']);
    Route::post('employee_working_days/fetch_filter_records', ['uses' => 'HRM\EmployeeWorkingDaysController@fetchFilterRecords', 'as' => 'employee_working_days.fetch_filter_records']);
    Route::post('employee_working_days/change_attendance_type', ['uses' => 'HRM\EmployeeWorkingDaysController@changeAttendanceType', 'as' => 'employee_working_days.change_attendance_type']);

    //Employee Working Days Ends Here

    //Employee Kpis starts here
    Route::post('employee_kpis/delete', ['uses' => 'HRM\EmployeeKpisController@delete', 'as' => 'employee_kpis.delete']);
    Route::post('employee_kpis/active', ['uses' => 'HRM\EmployeeKpisController@active', 'as' => 'employee_kpis.active']);
    Route::post('employee_kpis/inactive', ['uses' => 'HRM\EmployeeKpisController@inactive', 'as' => 'employee_kpis.inactive']);
    Route::get('employee_kpis/details/{id}', ['uses' => 'HRM\EmployeeKpisController@details', 'as' => 'employee_kpis.details']);
    Route::post('employee_kpis/datatables', ['uses' => 'HRM\EmployeeKpisController@datatables', 'as' => 'employee_kpis.datatables']);
    Route::resource('employee_kpis', 'HRM\EmployeeKpisController');

    //Employee KPIs Ends Here

});

Route::post('load-subject-against-program', 'HierarchyController@loadSubjectAgainstProgram')->name('load-subject-against-program');
Route::fallback(function () {
    return view('404');
});
