<?php

Route::group(['prefix' => 'v1'], function () {

     // Department Resrouce Starts Here
     Route::get('all_departments', 'Api\AdminSettingController@departmentIndex');
     Route::get('all_cities', 'Api\AdminSettingController@cityIndex');
     Route::get('all_branches', 'Api\AdminSettingController@branchIndex');

});

Route::get('count_leave_request_notification','Notifications\Notification_Controller@leave_request_count_notification');

Route::get('test-chart-data','Dashboard\DashboardController@get_income');
Route::get('test-chart-data-two','Dashboard\DashboardController@get_income_two');
Route::get('receviables','Dashboard\DashboardController@get_receivables');

Route::get('all_staff','Dashboard\DashboardController@getAllStaff');
Route::get('all_student','Dashboard\DashboardController@getAllStudents');
Route::get('all_sos','Dashboard\DashboardController@getAllSOS');
Route::get('all_sos_staff','Dashboard\DashboardController@getAllSOS_staff');
Route::get('receivables_one','Dashboard\DashboardController@get_receivables_one');



