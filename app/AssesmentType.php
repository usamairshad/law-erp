<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssesmentType extends Model
{
    protected $fillable = [
        "name"
    ];
}
