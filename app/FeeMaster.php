<?php

namespace App;

use App\Models\FeeCollection;
use App\Models\FeeHead;
use App\Models\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class FeeMaster extends Model
{
    protected $table = 'erp_fee_masters';
    protected $fillable = ['created_by', 'last_updated_by','fee_master_basic_id','fee_discount_percentage','scholarship','scholarship_no_of_a','fee_head_term','program_id','discount_type', 'fee_discount_comment','fee_structure_code', 'session_id','branch_id', 'students_id', 'fee_head','fee_due_date','fee_amount','status'];

    public function students()
    {
        return $this->belongsTo(Student::class, 'students_id');
    }
    public function feeHead()
    {
        return $this->belongsTo(FeeHead::class, 'fee_head');
    }
    public function secFee(){
        return $this->belongsTo(FeeHead::class, 'fee_head')->where('slug',Config::get('constants.fee.collectStatus'));
    }
    public function feeCollect()
    {
        return $this->hasMany(FeeCollection::class, 'fee_masters_id');
    }
}
