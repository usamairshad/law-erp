<?php

namespace App\Providers;

use App\Models\HRM\StaffTransferBranch;
use App\Models\HRM\Termination;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\HRM\Trainee;
use App\Models\QA\GroupEventStudent;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer("*", function ($view) {
            $notification_count = 0;

            if (Auth::check()) {

                $trainingsOfUser = Trainee::with('training')
                    ->where("staff_id", Auth::id())->get()
                    ->unique("training_id");

                $trainingOfUserCount = Trainee::where("staff_id", Auth::id())
                    ->where("acknowledge", 0)
                    ->get()
                    ->unique("training_id");

                $notification_count += count($trainingOfUserCount);

                $warning = Termination::where("user_id", Auth::id())->first();
                //------------- warning notification ----------//

                if ($warning != null && $warning->acknowledge == "no") {
                    $notification_count += 1;
                }

                //--------------------- branch transfer acceptance notification -------------------//

                $staffTransferBranch = StaffTransferBranch::with('branch')
                    ->where("user_id", Auth::id())
                    ->first();
                if ($staffTransferBranch != null && $staffTransferBranch->staff_status == "pending") {
                    $notification_count += 1;
                }
                //--------------------------------Group event notification -------------------------//

              /*   $groupEvents = GroupEventStudent::with("group", "student")
                    ->where("student_id", Auth::id())
                    ->where("notification", 1)
                    ->latest()
                    ->get()
                    ->unique("group_event_id");

                $groupNotification = 0;

                    //dd($groupEvents);
                foreach ($groupEvents as $groupEvent) {

                    if ($groupEvent->acknowledge == 0) {
                        
                        $groupNotification = 1;
                        $notification_count += 1;
                    }
                } */

               // dd($groupNotification);
               // view()->share("allData", ["groupNotification" => $groupNotification, "studentNotification" => $groupEvents, "staffTransferBranch" => $staffTransferBranch, "warning" => $warning, 'trainingsOfUser' => $trainingsOfUser, "notification_count" => $notification_count]);
                view()->share("allData", ["staffTransferBranch" => $staffTransferBranch, "warning" => $warning, 'trainingsOfUser' => $trainingsOfUser, "notification_count" => $notification_count]);
            }
        });

        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //


    }
}
