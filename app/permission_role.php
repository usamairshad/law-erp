<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permission_role extends Model
{
    protected $table = 'erp_permission_role';
    protected $fillable = ['permission_id ', 'role_id '];
}
