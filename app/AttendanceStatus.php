<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceStatus extends Model
{
    protected $table = 'erp_attendance_statuses';
    protected $fillable = ['created_by', 'last_updated_by', 'title', 'display_class', 'status'];

}
