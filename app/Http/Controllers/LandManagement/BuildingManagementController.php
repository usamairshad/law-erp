<?php

namespace App\Http\Controllers\LandManagement;
use App\Models\LandManagement\Building;
use App\Models\Vendor;
use App\Models\Admin\Branches;
use App\Payees_amount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class BuildingManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Building::SelectRaw('*')
        ->join('erp_branches','erp_branches.id','=','erp_buildings.branch_id')
        ->join('erp_payee_lists','erp_payee_lists.branch_id','=','erp_buildings.branch_id')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payee_lists.payee_name')
        ->get();


       



       
  
  


        return view('landmanagement.building',compact('data'));
    }


    public function edit_building_list($id)
    {
        # code...

        $Branches = Branches::get();
        $data=Building::join('erp_branches','erp_branches.id',"=",'erp_buildings.branch_id')
        ->first();
        return view('landmanagement.building.edit', compact('data','Branches'));

    }


    public function update_building_list(Request $request)
    {
        # code...



        $done=Building::where('building_id',$request->building_id)->update(['aggreement_date'=>$request->agreement_start_date, 'agreement_end_date'=>$request->agreement_end_date,'increment'=>$request->increment,'tenure'=>$request->tenure,'security'=>$request->security_rent,'basic_rent'=>$request->campus_rent,'branch_id'=>$request->branch_id]);

flash('Record has been created successfully.')->success()->important();

return redirect()->route('building-list');



    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $Branches = Branches::get();
        return view('landmanagement.building.create', compact('Branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Building::create([
            'aggreement_date'  => $request['agreement_start_date'],
            'basic_rent'    => $request['campus_rent'],
            'branch_id'     => $request['branch_id'],
            'agreement_end_date'     => $request['agreement_end_date'], 
            'security'     => $request['security_rent'],
            'tenure'     => $request['tenure'],
            'increment'     => $request['increment'],      
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('building-list');



    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetch()
    {

      //  $data = Building::with('payeelists')->get();
      //  return response()->json($data);
    }
    public function delete_payee(Request $request){
        Payees_amount::where('building_id',$request->building_id)->where('payee_id' , $request->payee_id)->delete();
        return response()->json("success");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $Branches = Branches::get();
        $building = BUilding::where('id',$id)->get();

        return view('landmanagement.building.edit', compact('Branches','building'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Building::where('id',$id)->delete();
        App\Payees_amount::where('building_id',$id)->delete();


    }

}
