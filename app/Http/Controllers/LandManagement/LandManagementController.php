<?php

namespace App\Http\Controllers\LandManagement;

use Auth;
use App\Models\Admin\Branches;
use App\Models\LandManagement\PayeeList;
use App\Models\Vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
class LandManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PayeeList::SelectRaw('erp_vendor.vendor_name,erp_branches.name,erp_payee_lists.*')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payee_lists.payee_name')
        ->join('erp_branches','erp_branches.id','=','erp_payee_lists.branch_id')->get();
          return view('landmanagement.payee_list', compact('data'));

    }
    public function show($id)
    {
        $data = PayeeList::SelectRaw('erp_vendor.vendor_name,erp_branches.name,erp_payee_lists.*')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payee_lists.payee_name')
        ->join('erp_branches','erp_branches.id','=','erp_payee_lists.branch_id')
        ->where('erp_payee_lists.id','=',$id)->get();
     
          return view('landmanagement.payee_list', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $vendorlist = Vendor::get();
        return view('landmanagement.land.create', compact('vendorlist'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $id=$request->id;
        if ($id==0 || $id==''){

             PayeeList::create($inputs);
        }
        else{

            PayeeList::where('id', $id)->update($inputs);

        }
        return redirect()->route('landlord-details');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendorlist = Vendor::get();
         $data = PayeeList::SelectRaw('erp_vendor.vendor_name,erp_branches.name,erp_payee_lists.*')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payee_lists.payee_name')
        ->join('erp_branches','erp_branches.id','=','erp_payee_lists.branch_id')
        ->where('erp_payee_lists.id','=',$id)->get();
       
        return view('landmanagement.land.edit', compact('data','vendorlist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       
$id = $request['id'];
        $asset_type = PayeeList::where('id',$id);
        $asset_type->update([
            'payee_name' => $request['payee_name'],
            'branch_id' => $request['branch_id'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'address' => $request['address']
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('landlord-details');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

            PayeeList::where('id',$id)->delete();
            flash('Record has been Deleted successfully.')->success()->important();

            return redirect()->route('landlord-details');

    }
}
