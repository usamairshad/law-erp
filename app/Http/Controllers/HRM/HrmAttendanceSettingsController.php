<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\HrmAttendaceSettings\StoreUpdateRequest;
use App\Models\HRM\LeaveTypes;
use App\Models\HRM\AttendanceSettings;
use App\Models\HRM\WorkShifts;
use Auth;

class HrmAttendanceSettingsController extends Controller
{
    /**
     * Display a listing of LeaveType.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('attendance_settings_manage')) {
            return abort(401);
        }

        $AttendanceSettings = AttendanceSettings::OrderBy('created_at','desc')->get();

        return view('hrm.attendance_settings.index', compact('AttendanceSettings'));
    }

    /**
     * Show the form for creating new LeaveType.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('attendance_settings_create')) {
            return abort(401);
        }
        $WorkShifts = WorkShifts::pluckActiveOnly();
        $WorkShifts->prepend('Select a Work Shift', '');
        return view('hrm.attendance_settings.create' ,compact( 'WorkShifts'));
    }

    /**
     * Store a newly created LeaveType in storage.
     *
     * @param  \App\Http\Requests\HRM\LeaveTypes\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('attendance_settings_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        // If Condition set then set values


        AttendanceSettings::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.attendance_settings.index');
    }


    /**
     * Show the form for editing LeaveType.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('attendance_settings_edit')) {
            return abort(401);
        }
        $WorkShifts = WorkShifts::pluckActiveOnly();
        $WorkShifts->prepend('Select a Work Shift', '');
        $AttendanceSetting = AttendanceSettings::findOrFail($id);

        return view('hrm.attendance_settings.edit', compact('AttendanceSetting', 'WorkShifts'));
    }

    /**
     * Update LeaveType in storage.
     *
     * @param  \App\Http\Requests\HRM\LeaveTypes\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('attendance_settings_edit')) {
            return abort(401);
        }

        $AttendanceSetting = AttendanceSettings::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        // If Condition set then set values


        $AttendanceSetting->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.attendance_settings.index');
    }


    /**
     * Remove LeaveType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (! Gate::allows('attendance_settings_destroy')) {
            return abort(401);
        }

        $LeaveType = AttendanceSettings::findOrFail($id);

        $LeaveType->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.attendance_settings.index');
    }

    /**
     * Activate LeaveType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('attendance_settings_active')) {
            return abort(401);
        }

        $LeaveType = LeaveTypes::findOrFail($id);
        $LeaveType->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.attendance_settings.index');
    }

    /**
     * Inactivate LeaveType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('attendance_settings_inactive')) {
            return abort(401);
        }

        $LeaveType = LeaveTypes::findOrFail($id);
        $LeaveType->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.attendance_settings.index');
    }

    /**
     * Delete all selected LeaveType at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('attendance_settings_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = LeaveTypes::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
