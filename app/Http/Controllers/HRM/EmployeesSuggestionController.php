<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Requests\HRM\HrmEmployeesSuggestion\StoreUpdateRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;


use App\Models\HRM\EmployeesSuggestion;
use App\Models\HRM\JobTitle;
use App\Models\HRM\Employees;

use Auth;
use Config;

class EmployeesSuggestionController extends Controller
{

    public function index()
    {

        if (!Gate::allows('employees_suggestion_manage')) {
            return abort(401);
        }

        $EmployeesSuggestions = EmployeesSuggestion::OrderBy('created_at', 'desc')->get();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        return view('hrm.employees_suggestion.index', compact('EmployeesSuggestions','Employees'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('employees_suggestion_create')) {
            return abort(401);
        }

        $JobTitles = JobTitle::pluckActiveMrkOnly();
        $JobTitles->prepend('Select a Job Title', '');

        return view('hrm.employees_suggestion.create');

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employees_suggestion_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $data['user_id'] = Auth::user()->id;
//        $data['ratings'] = 'A';
        $data['display_status'] = 1;
//dd($data);

        EmployeesSuggestion::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employees_suggestion.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employees_suggestion_edit')){
            return abort(401);
        }

        $EmployeesSuggestion   = EmployeesSuggestion::findOrFail($id);

        return view('hrm.employees_suggestion.edit', compact('EmployeesSuggestion'));
    }


    public function details($id)
    {
        if (! Gate::allows('employees_suggestion_details')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $Databanks = Databanks::pluckActiveOnly($loggedIn);
        $Databanks->prepend('Select a Databank', '');
        $Contact = Contacts::contactById($id);

        return view('hrm.employees_suggestion.details', compact('Contact','Employees','Databanks'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('employees_suggestion_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $EmployeesSuggestion = EmployeesSuggestion::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $EmployeesSuggestion->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employees_suggestion.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employees_suggestion_destroy')) {
            return abort(401);
        }
        $Employee = Contacts::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(StoreUpdateRequest $request)
    {
//echo 'abc'; exit;

        if (! Gate::allows('employees_suggestion_datatables')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        if( isset($data['user'] )){
            $Suggestion = EmployeesSuggestion::where('user_id', $data['user'])->OrderBy('created_at', 'desc')->get();
        }
        else{
            $Suggestion = EmployeesSuggestion::OrderBy('created_at', 'desc')->get();
        }

        return  datatables()->of($Suggestion)
            ->addColumn('user_name', function($Suggestion) {

                return isset($Suggestion->user_name->first_name) ? $Suggestion->user_name->first_name : 'Admin';

            })
            ->addColumn('created_at', function($Suggestion) {

                return date('d-m-Y h:m a', strtotime($Suggestion->created_at));

            })

            ->addColumn('action', function ($Suggestion) {
                $action_column = '<a href="employees_suggestion/'.$Suggestion->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('employees_suggestion_destroy')){
                    $action_column .= '<form method="post" action="employees_suggestion/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Suggestion->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['user_name','branch','created_at','action'])
            ->toJson();
        //return view('hrm.employees_suggestion.details', compact('Contact','OrganizationLocations','Employees'));
    }

    public function delete(Request $request)
    {

        if (! Gate::allows('employees_suggestion_destroy')) {
            return abort(401);
        }
        $data = $request->all();
        $Contact = EmployeesSuggestion::findOrFail($data['first_name']);
        $Contact->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employees_suggestion.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employees_suggestion_active')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.employees_suggestion.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employees_suggestion_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.employees_suggestion.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public function fileUpload(Request $request)

    {

       $this->validate($request, [

            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);


        $image = $request->file('image');

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/uploads/profile_image');

        $image->move($destinationPath, $input['imagename']);


        //$this->postImage->add($input);


        return $input['imagename'];

    }

}
