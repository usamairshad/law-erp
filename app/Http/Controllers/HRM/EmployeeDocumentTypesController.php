<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Requests\HRM\HrmEmployeeDocumentTypes\StoreUpdateRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\HRM\JobTitle;
use App\Models\HRM\Employees;
use App\Models\HRM\EmployeeDocumentTypes;
use Auth;

class EmployeeDocumentTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('employee_document_types_manage')) {
            return abort(401);
        }
        $EmployeeDocumentTypes = EmployeeDocumentTypes::orderBy('created_at','desc')->get()->pluck('name','id');
        $EmployeeDocumentTypes->prepend('Select a Document Type');
        return view('hrm.employee_document_types.index',compact('EmployeeDocumentTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(! Gate::allows('employee_document_types_create')) {
            return abort(401);
        }
        return view('hrm.employee_document_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_document_types_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        EmployeeDocumentTypes::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employee_document_types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employee_document_types_edit')){
            return abort(401);
        }
        $EmployeeDocumentType = EmployeeDocumentTypes::findOrFail($id);
        return view('hrm.employee_document_types.edit', compact('EmployeeDocumentType', 'EmployeeDocumentTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('employee_document_types_edit')) {
            return abort(401);
        }

        $EmployeeDocumentType = EmployeeDocumentTypes::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $EmployeeDocumentType->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employee_document_types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employee_document_types_destroy')) {
            return abort(401);
        }

        $EmployeeDocumentType = EmployeeDocumentTypes::findOrFail($id);

        if(!empty($EmployeeDocumentType->employee_documents()->get()->toArray())){
            flash('Type has documents in posession. Please delete the documents first.')->error()->important();
            return redirect()->route('hrm.employee_document_types.index');
        }

        $EmployeeDocumentType->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employee_document_types.index');
    }

    public function datatables(Request $request)
    {
        // if (! Gate::allows('employee_document_types_datatables')) {
        //     return abort(401);
        // }
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        $Target = EmployeeDocumentTypes::OrderBy('created_at', 'desc')->get();
        return  datatables()->of($Target)
            ->addColumn('id', function($Target) {

                return $Target->id;

            })
            ->addColumn('name', function($Target) {

                return $Target->name;

            })
            ->addColumn('created_at', function($Target) {

                return $Target->created_at;

            })
            ->addColumn('action', function ($Target) {
                $action_column = '<a href="employee_document_types/'.$Target->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('employee_document_types_destroy')){
                    $action_column .= '<form method="post" action="employee_document_types/'.$Target->id.'/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Target->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['name','action'])
            ->toJson();
        }

}
