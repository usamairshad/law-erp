<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Carbon\Carbon;
use App\Models\HRM\StaffPerformance;
use App\Models\HRM\Resign;

class StaffPerformanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $teachers = User::where('branch_id' , Auth::user()->branch_id)->get();
        //dd($teachers);
       

        return view('hrm.staff_performance.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function performanceDetail()
    {
        $get_data = StaffPerformance::all();

        return view('hrm.staff_performance.feedbackviewdetail', compact('get_data'));

    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
       
        $user_id = (int)$request->user_id;
       $month = Carbon::now()->format('M');
       $ldate = date('Y-m-d');
       
       StaffPerformance::create([
            'user_id'           => $user_id,
            'lecture_deliver'    => $request['lecture_deliver'],
            'confidence'    => $request['confidence'],
            'punctuality'    => $request['punctuality'],
            'moral_values'    => $request['moral_values'],
            'student_feedback'    => $request['student_feedback'],
            'month'    => $month,
            'date'     => $ldate, 
            'created_by'     => Auth::user()->id,
            'remarks'    => $request['remarks'],    
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('staff-performance.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function feedbackView($id)
    {
        $get_data  =StaffPerformance::where('user_id' , $id)->get();
  
        return view('hrm.staff_performance.feedbackview', compact('get_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function feedback($id)
    {
        $user_id = $id;
        return view('hrm.staff_performance.feedback', compact('user_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function resignList()
    {
        $data = Resign::all();

         return view('hrm.resign.detail', compact('data'));
    }

}



