<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\JobPost;
use App\Models\Admin\BranchUser;
use App\Models\JobPostRequest;
use App\Models\Academics\SubjectDetail;
use App\Models\Academics\Faculty;
use Redirect;
use Carbon\Carbon;

class JobPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= JobPost::all();
         
        return view('hrm.recruitment.job_post.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        $data=JobPostRequest::where('id',$id)->first();
        
        
        return view('hrm.recruitment.job_post.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input= $request->all();

        $branch_id = JobPostRequest::where('id',$request->job_post_request_id)->value('branch_id');
        $input['branch_id'] = $branch_id;
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $input['status']=2;


        
        $job_post=JobPost::create($input);

        $val=2;

        $test=JobPostRequest::where('id',$request->job_post_request_id)->update(['status'=>$val]);


        

  
        $temp=  JobPost::find($job_post->id);
        $url =  url()->current();
        $data=  substr($url, 0, -32);
        $public_link= $data."job-post".'/'.$temp->id ;

        $temp= JobPost::find($temp->id)->update(['public_link'=> $public_link]);
        flash('Job Post has been created successfully.')->success()->important();
        return redirect()->route('admin.recruitment.job-post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showPublicPost($id)
    {
        $job_post = JobPost::find($id);
        return view('hrm.recruitment.job_post.job-post-public',compact('job_post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= JobPost::find($id);
        $branch_id= JobPostRequest::where('id',$data->job_post_request_id)->value('branch_id');

        $sections = ['Junior','Senior'];
        return view('hrm.recruitment.job_post.edit',compact('branch_id','data','sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $input= $request->all();
        $branch_id = BranchUser::where('user_id',Auth::user()->id)->value('branch_id');

        $input['branch_id'] = $request->branch_id;
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $input['updated_at'] = Carbon::now();

        JobPost::find($request->id)->update($input);
        flash('Job Post has been updated successfully.')->success()->important();
         return redirect()->route('admin.recruitment.job-post');       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function updateStatusActive($id)
    {
       JobPost::find($id)->update(['status'=>1]);
    }
    public function updateStatusInActive($id)
    {
       JobPost::find($id)->update(['status'=>0]);
    }
}
