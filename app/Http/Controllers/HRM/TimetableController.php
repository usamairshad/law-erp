<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin\Branches;
use App\Models\HRM\TimeTable;   
use App\Models\HRM\TimetableCategory; 

class TimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_timetable_manage')) {
            return abort(401);
        }
        $timetables = TimeTable::OrderBy('created_at','desc')->get();
         return view('hrm.timetables.index', compact('timetables')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_timetable_create')) {
            return abort(401);
        }
        $branchList = Branches::pluck('name', 'id');
        $timetable_category=  TimetableCategory::pluck('name', 'id');
        return view('hrm.timetables.create', compact('branchList' ,'timetable_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       
        if (! Gate::allows('erp_timetable_create')) {
            return abort(401);
        }
        $this->validate($request, [
            'timetable_category_id' => 'required',
            'branch_id' => 'required',
            'title' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
        TimeTable::create([
            'timetable_category_id' => $request['timetable_category_id'],
            'branch_id'      => $request['branch_id'],
            'title'          => $request['title'],
            'start_time'     => $request['start_time'],
            'end_time'       => $request['end_time'],
            'created_by'     => Auth::user()->id,    
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.timetable.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_timetable_destroy')) {
            return abort(401);
        }
        // $entries = TimeTableCategory::where('id', $id)->delete();
        // flash('Record has been deleted successfully.')->success()->important();

        // return redirect()->route('admin.timetable_category.index');
    }
}
