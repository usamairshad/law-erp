<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Holidays\StoreUpdateRequest;
use App\Models\HRM\Holidays;
use Auth;

class HolidaysController extends Controller
{
    /**
     * Display a listing of Holiday.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       // if (! Gate::allows('holidays_manage')) {
       //     return abort(401);
       // }

        $Holidays = Holidays::OrderBy('created_at','desc')->get();

        return view('hrm.holidays.index', compact('Holidays'));
    }

    /**
     * Show the form for creating new Holiday.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     //   if (! Gate::allows('holidays_create')) {
     //       return abort(401);
     //   }
        return view('hrm.holidays.create');
    }

    /**
     * Store a newly created Holiday in storage.
     *
     * @param  \App\Http\Requests\HRM\Holidays\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
    //    if (! Gate::allows('holidays_create')) {
    //        return abort(401);
    //    }

        $data = $request->all();

        // Set recurring field
        if(!$request->get('is_recurring')) {
            $data['is_recurring'] = 0;
        }

        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        Holidays::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.holidays.index');
    }


    /**
     * Show the form for editing Holiday.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     **/

    public function edit($id)
    {
    //    if (! Gate::allows('holidays_edit')) {
    //        return abort(401);
    //    }
        $Holiday = Holidays::findOrFail($id);

        return view('hrm.holidays.edit', compact('Holiday'));
    }

    /**
     * Update Holiday in storage.
     *
     * @param  \App\Http\Requests\HRM\Holidays\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
    //    if (! Gate::allows('holidays_edit')) {
    //        return abort(401);
    //    }

        $Holiday = Holidays::findOrFail($id);

        $data = $request->all();

        // Set recurring field
        if(!$request->get('is_recurring')) {
            $data['is_recurring'] = 0;
        }

        $data['updated_by'] = Auth::user()->id;

        $Holiday->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.holidays.index');
    }


    /**
     * Remove Holiday from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    //    if (! Gate::allows('holidays_destroy')) {
    //        return abort(401);
    //    }
        $Holiday = Holidays::findOrFail($id);
        $Holiday->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.holidays.index');
    }

    /**
     * Activate Holiday from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
    //    if (! Gate::allows('holidays_active')) {
    //        return abort(401);
    //    }

        $Holiday = Holidays::findOrFail($id);
        $Holiday->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.holidays.index');
    }

    /**
     * Inactivate Holiday from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
    //    if (! Gate::allows('holidays_inactive')) {
    //        return abort(401);
    //    }

        $Holiday = Holidays::findOrFail($id);
        $Holiday->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.holidays.index');
    }

    /**
     * Delete all selected Holiday at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
     //   if (! Gate::allows('holidays_mass_destroy')) {
     //       return abort(401);
     //   }
        if ($request->input('ids')) {
            $entries = Holidays::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
