<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Requests\HRM\HrmEmployeeDocuments\StoreUpdateRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\HRM\JobTitle;
use App\Models\HRM\Employees;
use App\Models\HRM\EmployeeDocuments;
use App\Models\HRM\EmployeeDocumentTypes;
use Auth;
use Response;

class EmployeeDocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (! Gate::allows('employee_documents_manage')) {
            return abort(401);
        }
        $EmployeeDocuments = EmployeeDocuments::all();
        $Employees = Employees :: pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        return view('hrm.employee_documents.index',compact('EmployeeDocuments','Employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(! Gate::allows('employee_documents_create')) {
            return abort(401);
        }
        $Employees = Employees :: pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $EmployeeDocumentTypes = EmployeeDocumentTypes::orderBy('id')->get()->pluck('name','id');
        $EmployeeDocumentTypes->prepend('Select a Document Type');
        return view('hrm.employee_documents.create',compact('EmployeeDocumentTypes','Employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_documents_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        $file = $data['file_location'];
        $file_new_name = strtolower(Employees::getEmployeeNameByUserId($data['user_id']).EmployeeDocumentTypes::getDocumentNameByDocumentId($data['document_type'])).time();
        $file_new_name = strtr($file_new_name,[' '=>'_']);
        $destinationPath = public_path('uploads/hrm/employees/documents');
        $file->move($destinationPath, $file_new_name);
        $data['file_location'] = $destinationPath.'/'.$file_new_name;
        EmployeeDocuments::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employee_documents.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employee_documents_edit')){
            return abort(401);
        }
        $EmployeeDocument = EmployeeDocuments::findOrFail($id);
        $Employees = Employees :: pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $EmployeeDocumentTypes = EmployeeDocumentTypes::orderBy('id')->get()->pluck('name','id');
        $EmployeeDocumentTypes->prepend('Select a Document Type');
        return view('hrm.employee_documents.edit', compact('EmployeeDocument','Employees','EmployeeDocumentTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('employee_documents_edit')) {
            return abort(401);
        }

        $EmployeeDocument = EmployeeDocuments::findOrFail($id);
        $data = $request->all();

        if($request->hasFile('file_location')){
            if(file_exists($EmployeeDocument->file_location)){
                unlink($EmployeeDocument->file_location);
            }
            $file = $data['file_location'];
            $file_new_name = Employees::getEmployeeNameByUserId($data['user_id']).'_'.EmployeeDocumentTypes::getDocumentNameByDocumentId($data['document_type']);
            $file_new_name = strtr($file_new_name,[' '=>'_']);
            $destinationPath = public_path('uploads/hrm/employees/documents');
            $file->move($destinationPath, $file_new_name);
            $data['file_location'] = $destinationPath.'/'.$file_new_name;
        }                                          

        $data['updated_by'] = Auth::user()->id;

        $EmployeeDocument->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employee_documents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employee_documents_destroy')) {
            return abort(401);
        }

        $EmployeeDocument = EmployeeDocuments::findOrFail($id);
        $EmployeeDocument->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employee_documents.index');
    }

    public function view($id){
        if (! Gate::allows('employee_documents_manage')) {
            return abort(401);
        }
        
        $EmployeeDocument = EmployeeDocuments::findOrFail($id);
        $path = $EmployeeDocument->file_location;

        return response()->file($path);
    }


    public function datatables(Request $request)
    {
        // if (! Gate::allows('employee_documents_datatables')) {
        //     return abort(401);
        // }
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        if($data['user_id'] != 'abcd')
            $Target = EmployeeDocuments::where('user_id',$data['user_id'])->get();
        else
            $Target = EmployeeDocuments::where('user_id', 0)->OrderBy('created_at','desc')->get();
        return  datatables()->of($Target)
            ->addColumn('id', function($Target) {

                return $Target->id;

            })
            ->addColumn('document_type', function($Target) {

                return EmployeeDocumentTypes::getDocumentNameByDocumentId($Target->document_type);

            })
            ->addColumn('user_name', function($Target) {

                return Employees::employeeDetailByUserId($Target->user_id)->first_name;

            })
            ->addColumn('created_at', function($Target) {

                return $Target->created_at;

            })
            ->addColumn('action', function ($Target) {
                $action_column = '<a href="employee_documents/'.$Target->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a><br>';
                $action_column .= '<a href="employee_documents/'.$Target->id.'/view" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-eye-open"></i> View</a>';

                if(Gate::check('employee_overtime_destroy')){
                    $action_column .= '<form method="post" action="employee_documents/'.$Target->id.'/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Target->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['user_id','action'])
            ->toJson();
        }

}
