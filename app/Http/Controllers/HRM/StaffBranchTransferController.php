<?php

namespace App\Http\Controllers\HRM;

use App\Helpers\Helper;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\ActiveSessionTeacher;
use App\Models\Academics\Staff;
use App\Models\HRM\StaffTransferBranch;
use App\User;
use Illuminate\Support\Facades\Auth;

class StaffBranchTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //$input = $req->except("_token");

        StaffTransferBranch::updateOrCreate(
            ["user_id" => $req->user_id],
            ["branch_id" => $req->branch_id, "staff_status" => "pending"]
        );

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function delete($id)
    {
        $obj = StaffTransferBranch::where("user_id", $id)->delete();

        return redirect()->back();
    }


    public function acceptingOrNot($status)
    {

        $staff_previous_branch   = Staff::where("user_id", Auth::id())->first()->branch_id;

        $obj = StaffTransferBranch::where("user_id", Auth::id())->first();
        $obj->staff_status = "$status";
        $obj->save();

        $sameBranch = false;
        if ($staff_previous_branch == $obj->branch_id) {
            $sameBranch = true;
        }

        switch ($status) {

            case "accept":
                $staff = Staff::where("user_id", $obj->user_id)->first();
                $staff->branch_id = $obj->branch_id;
                $staff->save();
                $user = User::findOrFail(Auth::id());
                $user->branch_id = $obj->branch_id;
                $user->save();
                /////-------Delete active session record and teacher record--------//////
                $staff_id = Helper::getStaffIDByUserID(Auth::id());

                if (!$sameBranch) {

                    $activeSessionTeachers = ActiveSessionTeacher::where("staff_id", $staff_id)->get();
                    if (!empty($activeSessionTeachers)) {
                        foreach ($activeSessionTeachers as $activeSessionTeacher) {

                            $activeSessionTeacher->delete();
                        }
                    }
                }
                break;
        }



        return redirect()->back();
    }
}
