<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Skills\StoreUpdateRequest;
use App\Models\HRM\Skills;
use Auth;

class SkillsController extends Controller
{
    /**
     * Display a listing of Skill.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('skills_manage')) {
            return abort(401);
        }

        $Skills = Skills::OrderBy('created_at','desc')->get();

        return view('hrm.skills.index', compact('Skills'));
    }

    /**
     * Show the form for creating new Skill.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('skills_create')) {
            return abort(401);
        }
        return view('hrm.skills.create');
    }

    /**
     * Store a newly created Skill in storage.
     *
     * @param  \App\Http\Requests\HRM\Skills\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('skills_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        Skills::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.skills.index');
    }


    /**
     * Show the form for editing Skill.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('skills_edit')) {
            return abort(401);
        }
        $Skill = Skills::findOrFail($id);

        return view('hrm.skills.edit', compact('Skill'));
    }

    /**
     * Update Skill in storage.
     *
     * @param  \App\Http\Requests\HRM\Skills\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('skills_edit')) {
            return abort(401);
        }

        $Skill = Skills::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $Skill->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.skills.index');
    }


    /**
     * Remove Skill from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('skills_destroy')) {
            return abort(401);
        }
        $Skill = Skills::findOrFail($id);
        $Skill->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.skills.index');
    }

    /**
     * Activate Skill from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('skills_active')) {
            return abort(401);
        }

        $Skill = Skills::findOrFail($id);
        $Skill->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.skills.index');
    }

    /**
     * Inactivate Skill from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('skills_inactive')) {
            return abort(401);
        }

        $Skill = Skills::findOrFail($id);
        $Skill->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.skills.index');
    }

    /**
     * Delete all selected Skill at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('skills_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Skills::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
