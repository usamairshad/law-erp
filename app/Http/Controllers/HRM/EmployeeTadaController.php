<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Requests\HRM\HrmEmployeeTada\StoreUpdateRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;


use App\Models\HRM\EmployeesTada;
use App\Models\HRM\JobTitle;
use App\Models\HRM\Employees;

use Auth;
use Config;

class EmployeeTadaController extends Controller
{

    public function index()
    {

        if (!Gate::allows('employee_tada_manage')) {
            return abort(401);
        }

        $EmployeesTada = EmployeesTada::OrderBy('created_at', 'desc')->get();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        return view('hrm.employee_tada.index', compact('EmployeesTada','Employees'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('employee_tada_create')) {
            return abort(401);
        }

        $JobTitles = JobTitle::pluckActiveMrkOnly();
        $JobTitles->prepend('Select a Job Title', '');

        return view('hrm.employee_tada.create', compact('JobTitles'));

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_tada_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        $data = $request->all();

        EmployeesTada::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employee_tada.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employee_tada_edit')){
            return abort(401);
        }

        $EmployeeTada   = EmployeesTada::findOrFail($id);
        $JobTitles = JobTitle::pluckActiveMrkOnly();
        $JobTitles->prepend('Select a Job Title', '');
        // echo '<pre>'; print_r($checkin_time); exit;
        return view('hrm.employee_tada.edit', compact('EmployeeTada', 'JobTitles'));
    }


    public function details($id)
    {
        if (! Gate::allows('employee_tada_details')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $Databanks = Databanks::pluckActiveOnly($loggedIn);
        $Databanks->prepend('Select a Databank', '');
        $Contact = Contacts::contactById($id);

        return view('hrm.employee_tada.details', compact('Contact','Employees','Databanks'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('employee_tada_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $EmployeesTada = EmployeesTada::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $EmployeesTada->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employee_tada.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employee_tada_destroy')) {
            return abort(401);
        }
        $Employee = Contacts::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(StoreUpdateRequest $request)
    {
//echo 'abc'; exit;

        if (! Gate::allows('employee_tada_datatables')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        if( isset($data['user'] )){
            $Target = EmployeesTada::where('user_id', $data['user'])->OrderBy('created_at', 'desc')->get();
        }
        else{
            $Target = EmployeesTada::OrderBy('created_at', 'desc')->get();
        }

        return  datatables()->of($Target)
            ->addColumn('job_title', function($Target) {

                return $Target->job_name->name;

            })
            ->addColumn('created_at', function($Target) {

                return date('d-m-Y h:m a', strtotime($Target->created_at));

            })

            ->addColumn('action', function ($Target) {
                $action_column = '<a href="employee_tada/'.$Target->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('employee_tada_destroy')){
                    $action_column .= '<form method="post" action="employee_tada/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Target->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['user_name','branch','created_at','action'])
            ->toJson();
        //return view('hrm.employee_tada.details', compact('Contact','OrganizationLocations','Employees'));
    }

    public function delete(StoreUpdateRequest $request)
    {

        if (! Gate::allows('employee_tada_destroy')) {
            return abort(401);
        }
        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employee_tada.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_tada_active')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.employee_tada.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_tada_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.employee_tada.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public function fileUpload(Request $request)

    {

       $this->validate($request, [

            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);


        $image = $request->file('image');

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/uploads/profile_image');

        $image->move($destinationPath, $input['imagename']);


        //$this->postImage->add($input);


        return $input['imagename'];

    }

}
