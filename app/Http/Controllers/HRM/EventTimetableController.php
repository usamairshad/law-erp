<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\HRM\EventTimeTable;
use App\Models\HRM\TimeTable;
use App\Models\HRM\EventModel;
use Auth;

class EventTimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event_timetables = EventTimeTable::OrderBy('created_at','desc')->get();
        return view('hrm.event_timetables.index', compact('event_timetables')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $time_tables = TimeTable::pluck('title', 'id');
        $events= EventModel::pluck('title', 'id');
        
        return view('hrm.event_timetables.create', compact('time_tables','events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'time_table_id' => 'required',
            'event_id' => 'required',
            'date' => 'required',
        ]);
        EventTimeTable::create([
            'time_table_id'      => $request['time_table_id'],
            'event_id'           => $request['event_id'],
            'created_by '        => Auth::user()->id,
            'date'              =>$request['date'],
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.event_timetable.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_class_time_table_destroy')) {
            return abort(401);
        }

        $entries = EventTimeTable::where('id', $id)->delete();
        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.event_timetable.index');
    }
}
