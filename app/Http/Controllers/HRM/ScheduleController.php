<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\JobPostRequest;
use App\Models\Admin\BranchUser;
use App\Models\Schedule;
use App\Models\Academics\SubjectDetail;
use App\Models\Academics\Faculty;
use App\Models\CandidateScore;
use App\Models\Recruitment_Feedback;
use Redirect;
use Carbon\Carbon;
use App\Models\HRM\CandidateForm;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data= Schedule::all();
        
        $now=Carbon::now()->format('Y-m-d H:i:s');
        // $CandidateScore= CandidateScore::all();
        return view('hrm.recruitment.schedule.index',compact('data','now'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


            
        


        $candidate= CandidateForm::find($request->candidate_form_id);
        $post_id=$candidate['job_post_id'];
        $branch= $candidate->jobpost->jobpostrequest->branch;




        if(($request->reschedule) and $request->catgory=="test" and $candidate->status == "Short Listed")
        {
            CandidateForm::where('id',$request->candidate_form_id)->update(['status'=>'Test_Pending']);

            Schedule::where('id',$request->schedule_id)->update(['scheduled_time' => $request->scheduled_time, 'required_doc' => $request->required_doc]);
            //return redirect()->route('admin.schedule.index');  
            return redirect()->back(); 
        }
        elseif($request->category=="test" and $candidate->status == "Short Listed" )
        {
            $input= $request->all();
            $input['status']="Test_Pending";
            $input['job_post_id']= $post_id;
            $input['branch_id'] = $branch->id;
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            CandidateForm::where('id',$input['candidate_form_id'])->where('job_post_id',$input['job_post_id'])->update(['status'=>'Test_Pending']);

            
            Schedule::create($input);
            
            return redirect()->back(); 
           // return redirect()->route('admin.schedule.index');              
        }
        elseif ($request->category=="testfeedback" and $candidate->status == "Test_Pending") 
        {
            # code...
            $input= $request->all();

                

            $input['candidate_form_id']=$request->candidate_form_id;
            $input['job_post_id']=$post_id;   
            $input['status']="Recommended_Test"; 
            CandidateForm::where('id',$input['candidate_form_id'])->where('job_post_id',$post_id)->update(['status'=>'Test_Passed']);


            $rec_feed=Recruitment_Feedback::create($input);
            Schedule::where('id',$input['candidate_form_id'])->where('job_post_id',$input['job_post_id'])->update(['status'=>'Test_Passed','test_feedback'=>$rec_feed['id']]);


            return redirect()->back();
        }
        elseif (($request->reschedule) and $request->category=="interview" and $candidate->status == "Test_Passed" ) {
            # code...
            dd("ReSchedule Interview");       
        }
        elseif ($request->category=="interview" and $candidate->status == "Test_Passed") {
            # code...
            $input= $request->all();

            $input['status']="Interview_Pending";
            $input['job_post_id']= $post_id;
            $input['branch_id'] = $branch->id;
            $input['created_by'] = Auth::user()->id;
            $input['updated_by'] = Auth::user()->id;
            CandidateForm::where('id',$input['candidate_form_id'])->where('job_post_id',$input['job_post_id'])->update(['status'=>'Interview_Pending']);

            Schedule::where('id',$input['candidate_form_id'])->where('job_post_id',$input['job_post_id'])->where('branch_id',$input['branch_id'])->update(['status'=>'Interview_Pending','interview_time'=>$input['interview_time'],'interview_venue'=>$input['interview_venue'],'interview_doc'=>$input['interview_doc']]);
            
            //return redirect()->route('admin.schedule.index');   
            return redirect()->back();     
        }

        elseif ($request->category=="interviewfeedback" and $candidate->status == "Interview_Pending") {
            # code...

            $input= $request->all();

            

            $input['candidate_form_id']=$request->candidate_form_id;
            $input['job_post_id']=$post_id;   
            $input['status']="Recommended_Interview"; 
            CandidateForm::where('id',$input['candidate_form_id'])->where('job_post_id',$post_id)->update(['status'=>'Interview_Passed']);
            $rec_feed=Recruitment_Feedback::create($input);
            Schedule::where('id',$input['candidate_form_id'])->where('job_post_id',$input['job_post_id'])->update(['status'=>'Interview_Passed','test_feedback'=>$rec_feed['id']]);


            return redirect()->back();
        }



    }
    public function score(Request $request)
    {
        $input= $request->all();
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;

        CandidateForm::where('id',$input['candidate_form_id'])->update(['status'=>'Interview-Taken']);
        Schedule::where('id',$input['schedule_id'])->update(['status'=>'Interview-Taken']);
        CandidateScore::create($input);
        return redirect()->route('admin.schedule.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

       

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    public function reject($id)
    {
        CandidateForm::find($id)->update(['status'=>'Rejected']);
        return redirect()->route('candidate-form.index');
    }
    public function getCandidateScore(Request $request)
    {

        $candidate_score=CandidateScore::where('schedule_id',$request->employee_id)->first();
        \Log::info($candidate_score);
         return $candidate_score;
       
    }
    public function hire($id)
    {

        $data=Schedule::find($id)->update(['status'=>'Selected']);
        $candidate_form_id= Schedule::where('id',$id)->first();
        CandidateForm::where('id',$candidate_form_id->candidate_form_id)->update(['status'=>'Selected']);
        return redirect()->route('admin.staff.create');
    }

    public function notHired($id)
    {
        Schedule::find($id)->update(['status'=>'Rejected']);
        return redirect()->route('admin.schedule.index');

    }

    public function registerTeacher($id)
    {
        dd("Asda");
        // $candidate = CandidateForm::find($id);

        // DB::table('users')->insert([
        //     'name' => $candidate->name,
        //     'email' => $candidate->email,
        //     'password' => $candidate->,
        //     'contact_number' => $candidate->phone,
        //     'address' => $candidate->
        //     'role_id' => 10,
        //     'branch_id' =>  $candidate->job_post_id->branch_id,
        //     'hook_id' => ,
        //     'status' => 1,
        // ]);
    }
}
