<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\PayRoll\StoreUpdateRequest;
use App\Models\HRM\Payroll;
use App\Models\HRM\Employees;
use App\Models\HRM\EmployeeAttendance;
use App\Models\HRM\EmployeeWorkingDays;
use App\Models\HRM\EmployeesOvertime;
use App\Models\HRM\Holidays;
use App\Models\HRM\Leaves;
use App\Models\Admin\TaxSettings;
use App\Models\HRM\WorkWeeks;
use App\Helpers\HRMHelper;
use Auth;

class PayrollController extends Controller
{
    /**
     * Display a listing of PayRoll.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        if (! Gate::allows('payroll_manage')) {
            return abort(401);
        }
        $Payroll = Payroll::all();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');

//        $response = $this->attendance_postmartam('14', '2019-04-01','2019-04-30');
        //dd($response);
        return view('hrm.payroll.index',compact('Payroll','Employees'));
    }

    /**
     * Show the form for creating new PayRoll.
     *
     * @return \Illuminate\Http\Response
     **/


    public function datatables(Request $request){

        if (! Gate::allows('payroll_datatables')) {
            return abort(401);
        }

        $data = $request->all();

        if( isset($data['user_id']) && ! isset( $data['from_date']) && ! isset($data['to_date']) ){

            $Payroll = Payroll::payrollByUserId( $data['user_id'] );

        }
        else if(! isset($data['user_id']) && isset( $data['from_date']) && ! isset($data['to_date']) ){
            
            $Payroll = Payroll::payrollByDate( $data['from_date'] );

        }
        else if( isset($data['user_id']) && isset( $data['from_date']) && ! isset($data['to_date']) ){
            
            $Payroll = Payroll::payrollByUserAndDate( $data['user_id'] , $data['from_date'] );
        }
        else if( isset($data['user_id']) && isset( $data['from_date']) && isset($data['to_date']) ){
            
            $Payroll = Payroll::payrollByDatesAndUser( $data['from_date'] , $data['to_date'] , $data['user_id'] );

        }
        else if( ! isset($data['user_id']) && ! isset($data['from_date']) && ! isset($data['to_date']) ){
            $Payroll = Payroll::all();
        }
        else{
            $Payroll = Array();
        }

        return  datatables()->of($Payroll)
            ->editColumn('id', function($Payroll) {
                return $Payroll->id;
            })
            ->editColumn('user_name', function($Payroll) {
                return Employees::getEmployeeNameByUserId($Payroll->user_id);
            })
            ->editColumn('month', function($Payroll) {
                if(ceil(log10($Payroll->month)) == '1')
                    return \Config::get('hrm.month_array.0'.$Payroll->month);
                return \Config::get('hrm.month_array.'.$Payroll->month);
            })
            ->editColumn('year', function($Payroll) {
                return '20'.$Payroll->year ;
            })
            ->editColumn('gross_total', function($Payroll) {
                return 'Rs. '.$Payroll->gross_total ;
            })
            ->editColumn('paid_date', function($Payroll) {
                return  date('d-m-Y ', strtotime($Payroll->paid_date)) ;
            })
            ->editColumn('date_created', function($Payroll) {
                return  date('d-m-Y h:m a', strtotime($Payroll->created_at)) ;
            })
            ->addColumn('action', function ($Payroll) {
                $action_column = '';
                if(Gate::check('payroll_destroy')){
                    $action_column .= '<form method="post" action="payroll/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Payroll->id.' name="name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;

            })->rawColumns(['name','action'])
            ->toJson();
    }


    public function create(){
        if (! Gate::allows('payroll_create')) {
            return abort(401);
        }
        $Employees = Employees :: pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        return view('hrm.payroll.create' ,compact('Employees'));
    }

    /**
     * Store a newly created PayRoll in storage.
     *
     * @param  \App\Http\Requests\HRM\PayRoll\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request){
        if (! Gate::allows('payroll_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
//        dd($data);
        if(count(Payroll::where(['user_id' => $data['user_id'], 'year' => $data['year'], 'month' => $data['month']])->get())){
            flash('Record Already Exists.')->success()->important();
            return redirect()->back();
        }

        PayRoll::create($data);

        flash('Record has been created successfully.')->warning()->important();

        return redirect()->route('hrm.payroll.index');
    }
    public function show($id){

    }

    /**
     * Show the form for editing PayRoll.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        if (! Gate::allows('payroll_edit')) {
            return abort(401);
        }
        $PayRoll = PayRoll::findOrFail($id);

        return view('hrm.payroll.edit', compact('PayRoll'));
    }

    /**
     * Update PayRoll in storage.
     *
     * @param  \App\Http\Requests\HRM\PayRoll\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id){
        if (! Gate::allows('payroll_edit')) {
            return abort(401);
        }

        $PayRoll = PayRoll::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $PayRoll->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.payroll.index');
    }


    /**
     * Remove PayRoll from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){

        if (! Gate::allows('payroll_destroy')) {
            return abort(401);
        }

        $data = $request->all();
        
        $PayRoll = PayRoll::findOrFail($data['name']);
        $PayRoll->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.payroll.index');
    }

    /**
     * Activate PayRoll from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id){
        if (! Gate::allows('payroll_active')) {
            return abort(401);
        }

        $PayRoll = PayRoll::findOrFail($id);
        $PayRoll->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.payroll.index');
    }

    /**
     * Inactivate PayRoll from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id){
        if (! Gate::allows('payroll_inactive')) {
            return abort(401);
        }

        $PayRoll = PayRoll::findOrFail($id);
        $PayRoll->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.payroll.index');
    }

    /**
     * Delete all selected PayRoll at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request){
        if (! Gate::allows('payroll_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = PayRoll::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public  function getEmployeeDetail(Request $request){
        $data = $request->all();
        $Employee = Employees::employeeFullDetailByUserId($data['user_id']);
       
        $from_date = $data['year'].'-'.$data['month'].'-01';
        
        $to_date = $data['year'].'-'.$data['month'].'-30';

       $attendance_postmartam =  $this->attendance_postmartam($data['user_id'], $from_date, $to_date);
    //    dd($attendance_postmartam);
        return response()->json($attendance_postmartam);

        $EmployeeWorkingDays = EmployeeAttendance::workingHoursOfGivenMonth($data['month'], $data['year']);
        $Attendance = EmployeeWorkingDays::parseAttendanceArrays($EmployeeWorkingDays);

         //short,half, full days calculation starts here

        $short = 0;
        $half = 0;
        $full = 0;
        $present = 0;
        $time_slots = HRMHelper::getTimeSlots(1);
        //dd($time_slots);
        $TempAttendance = EmployeeAttendance::attendanceByDatesAndUser( $data['user_id'], $from_date , $to_date);
        dd($TempAttendance);
        // time to get first_in and last_out for each day
        $day_wise_attendance = HRMHelper::firstInLastOut($TempAttendance);
        $first_in_last_out = $day_wise_attendance['first_in_last_out'];
        foreach ($first_in_last_out as $key => $value){
            $slot_found = false;
            foreach ($time_slots as $k => $slot){
                if( ( $value[0] >= $slot['in_start'] && $value[0] <= $slot['in_end'] ) && ($value[1] >= $slot['out_start'] && $value[1] <= $slot['out_end'] ) ){
                    $slot_found = true;
                    if($slot['attendance_type'] == 1 ){
                        $short ++;
                    }elseif ($slot['attendance_type'] == 2){
                        $half ++;
                    }elseif ($slot['attendance_type'] == 3){
                        $full ++;
                    }elseif ($slot['attendance_type'] == 4){
                        $present ++;
                    }
                }

            }
        }
        $temp_array['short'] = $short;
        $temp_array['half'] = $half;
        $temp_array['full'] = $full;
        $temp_array['pres'] = $present;

        // short,half, full days calculation ends here


        $Holidays = Holidays::holidayBetween($data['month'] ,'20'.$data['year'] );
        $Leaves = Leaves::approvedHolidays($data['month'] ,'20'.$data['year'], $data['user_id']);
        $halfs = Leaves::approvedHalfDays($data['month'] ,'20'.$data['year'], $data['user_id']);
        $tax_slab = TaxSettings::getAnnualTaxByBasicSalary( $Employee->basic_salary );
        $working_days = WorkWeeks::getWorkingDays();

        $days_attendance = Array();
        $overtimes = EmployeesOvertime::getOvertime($data['user_id'], $data['month'], $data['year']);
        foreach($overtimes as $overtime){
            $day_attendance = EmployeeAttendance::getWorkHoursByUserAndDate($data['user_id'], $overtime->for_date);
            if(!empty($day_attendance)){
                if($day_attendance['worked_hours'] >= $overtime['allowed_hours'])
                    array_push($days_attendance, array('date' => $overtime->for_date, 'worked_hours' => $overtime['allowed_hours']));
                else
                    array_push($days_attendance, array('date' => $overtime->for_date, 'worked_hours' => $day_attendance['worked_hours']));
            }
            else{
                array_push($days_attendance, array('date' => $overtime->for_date, 'worked_hours' => '0'));
            }
        }
        $overtimes = $days_attendance;

        $response_data['employee'] = $Employee;
        $response_data['attendance'] = $Attendance;
        $response_data['holidays'] = $Holidays;
        $response_data['leaves'] = $Leaves;
        $response_data['halfs'] = $halfs;
        $response_data['overtimes'] = $overtimes;
        $response_data['tax_slab'] = $tax_slab;
        $response_data['working_days'] = $working_days;
        $response_data['temp_array'] = $temp_array;
        $response_data['first_in_last_out'] = $first_in_last_out;



        return response()->json($response_data);

    }

    public function getBankName(Request $request){
        $data = $request->all();
        return array( 'name' => \App\Models\Admin\BanksModel::getBankNameById($data['id']),'id'=> $data['id'] );
    }

    public function salary_sheet(){
//        dd('ok');
        return view('hrm.payroll.sheet');
    }


    public function payroll_report(Request $request){

        if (! Gate::allows('payroll_manage')) {
            return abort(401);
        }

        $data = $request->all();
        $month = (int)$data['month'];
        $year = (int)$data['year'];

        $Reports = Payroll::where(['month' => $month, 'year' => $year])->get();
       // dd($Reports);
        return view('hrm.payroll.salarysheet', compact('Reports', 'Departments', 'Locations'));

    }


    public function attendance_postmartam($user_id,$from_date, $to_date){
        $month = date("m",strtotime($from_date));
        $year = date("Y",strtotime($from_date));
        $day = (int) date("d",strtotime($to_date));
        $employee_info = array();
      //  dd($month);
        $Employee = Employees::employeeFullDetailByUserId($user_id);
        $employee_info['basic_salary'] = $Employee->basic_salary;
        
        $employee_info['house_rent'] = $Employee->is_house_rent == 1 ? ((int)$Employee->basic_salary) * 0.45 : 0;// 45 percent of basic
//        $employee_info['house_rent'] = 0;
        $employee_info['utility'] = $Employee->is_utility == 1 ? ((int)$Employee->basic_salary) * 0.05 : 0; // 5 percent of the basic
//        $employee_info['utility'] = 0;
        $employee_info['sc_amount'] = $Employee->is_sc == 1 ? (int) $Employee->sc_amount : 0;
        $employee_info['overtime'] = 0;
        $employee_info['bnk_name'] = $Employee->bnk_name;
        $employee_info['account_number'] = $Employee->account_number;
        $employee_info['pf'] = 0;
        $employee_info['total_bonus'] = 0;
        $employee_info['total_commission'] = 0;
        $employee_info['bank_id'] = $Employee->bank_name;
        $employee_info['gross_total'] = $employee_info['basic_salary'] + $employee_info['house_rent']+ $employee_info['utility']+
            $employee_info['sc_amount']+ $employee_info['total_bonus'] + $employee_info['total_commission'];



        $month_all_days = array();
        $all_working_days = array();
        $all_D_days = array();
        $weekends = array();
        $working_days = WorkWeeks::getWorkingDays();
        $working_days = json_decode(json_encode($working_days), true);
        $off_days = array();
        // exclude weekends from working days
        for($d=1; $d<=$day; $d++)
        {
            $time=mktime(12, 0, 0, $month, $d, $year);
            if (date('m', $time)==$month){
                $month_all_days[] = date('Y-m-d', $time);
                $day_name = date('D', $time);

                if($working_days[strtolower($day_name)] != 8){
                    $all_working_days[] = date('Y-m-d', $time);
                }else{
                    if(!in_array(strtolower(date('D', $time)), $weekends)){
                        $weekends[] =  strtolower(date('D', $time));
                    }

                }
            }
        }
        $monthHolidays = Holidays::monthHoliday($month ,$year);
        $monthHolidays = json_decode(json_encode($monthHolidays), true);
        // exclude gazetted holidays from working days
        $all_working_days = array_diff($all_working_days, $monthHolidays);

       // dd($all_working_days);

        $short = 0;
        $half = 0;
        $full = 0;
        $present = 0;
        $short_days = array();
        $half_days = array();
        $full_days = array();
        $present_days = array();

       // dd($tax_amount);
        $halfs = Leaves::approvedHalfDays($month ,$year, $user_id);
        $Leaves = Leaves::approvedHolidays($month ,$year, $user_id);
        $time_slots = HRMHelper::getTimeSlots(1);
        
       

        $TempAttendance = EmployeeAttendance::attendanceByDatesAndUser( $user_id , $from_date , $to_date);

        // time to get first_in and last_out for each day
        $day_wise_attendance = HRMHelper::firstInLastOut($TempAttendance);
        // dd($day_wise_attendance);
//        $first_in_last_out = $day_wise_attendance['first_in_last_out'];
        $first_in_last_out = $day_wise_attendance['day_wise_firstin_lastout'];
        
//        dd(array_keys($first_in_last_out));
        $marked_days = array_keys($first_in_last_out);
        // absent days
        $unmarked_working_days = array_diff($all_working_days, $marked_days);

       // dd($unmarked_working_days);
//        array_keys($value)[0];
        foreach ($first_in_last_out as $key => $value){
            //echo '<br>'.$key;
            
           if(in_array ( $key, $all_working_days )){
                // $slot_found = false;
                foreach ($time_slots as $k => $slot){
                    
                    if( ( $value[0] >= $slot['in_start'] && $value[0] <= $slot['in_end'] ) && ($value[1] >= $slot['out_start'] && $value[1] <= $slot['out_end'] ) ){
                        $slot_found = true;
                        if($slot['attendance_type'] == 1 ){
                            array_push($short_days,$key);
                           
                            $short ++;
                        }elseif ($slot['attendance_type'] == 2){
                            array_push($half_days,$key);
                           
                            $half ++;
                        }elseif ($slot['attendance_type'] == 3){
                            array_push($full_days,$key);
                           
                            $full ++;
                        }elseif ($slot['attendance_type'] == 4){
                            array_push($present_days,$key);
                           
                            $present ++;
                        }
                    }

                }
           }
        }
        //time to calculate sandwich leaves
        foreach ($unmarked_working_days as $key => $val){
            array_push($full_days, $val);
        }

        $count_of_off_days = count($weekends);
        $sandwitch_count = 0;
        foreach ($full_days as $key => $val){
            $leave_day = (int) date("d",strtotime($val));
            $leave_month = (int) date("m",strtotime($val));
         
            $next_date = date('Y-m-d', strtotime($val . ' +1 day'));
            $next_month = date('m', strtotime($next_date ));
            $next_day = strtolower( date('D', strtotime($next_date )));
            // sandwich leave will be calculated for the same month
            if($next_month == $leave_month){
                // leave is elegible to be tested for sandwich
                if(in_array($next_day, $weekends) ){
                    $date_after_next_date = date('Y-m-d', strtotime($next_date . ' +1 day'));
                    if(in_array($date_after_next_date, $full_days)){
                        $sandwitch_count++;
                    }
                }

            }

        }
        // 3 short days will be equal to one half day
        $fined_shorts = floor(count($short_days)/3);
        

//        $full_days[] = $unmarked_working_days;
        $allowed_leaves = count($Leaves) + (count($halfs)/2);
        $deducted_amount = 0;
        $deduted_leaves = 0;
        $total_absents = $sandwitch_count + count($full_days) + (count($half_days)/2) + $fined_shorts;
        if($total_absents > $allowed_leaves){
            $deduted_leaves = $total_absents - $allowed_leaves;
        }
        $per_day_fine = ($employee_info['gross_total'] * 12) / 365;
        $deducted_amount = (int)( $per_day_fine * $deduted_leaves);
        $gross_salary = $employee_info['gross_total'] - $deducted_amount;

        $tax_amount = TaxSettings::getTaxSlab($employee_info['basic_salary']);
//   dd($tax_amount);
        $resp_array['short'] = $short;
        $resp_array['half'] = $half;
        $resp_array['full'] = $full;
        $resp_array['pres'] = $present;

        $resp_array['short_days'] = $short_days;
        $resp_array['half_days'] = $half_days;
        $resp_array['full_days'] = $full_days;
        $resp_array['present_days'] = $present_days;
        $resp_array['unmarked_working_days'] = $unmarked_working_days;
        $resp_array['weekends'] = $weekends;
        $resp_array['sandwitch_count'] = $sandwitch_count;

        $resp_array['all_working_days'] = $all_working_days;
        $resp_array['allowed_leaves'] = $allowed_leaves;
        $resp_array['employee_info'] = $employee_info;
        $resp_array['tax_amount'] = $tax_amount;
        $resp_array['deducted_amount'] = $deducted_amount;
        $resp_array['total_after_tax'] = $gross_salary - $tax_amount;
        $resp_array['gross_salary'] = $gross_salary;
        $resp_array['emp_balance'] = 0;

        return $resp_array;

    }

    public function calculate_payroll(Request $request){
        if (! Gate::allows('payroll_create')) {
            return abort(401);
        }

        $data = $request->all();


        $Users = Employees::where('status',1)->get();

        $start_date = '20'.$data['salary_year'].'-'.$data['salary_month'].'-01';
        $days_in_month = cal_days_in_month(CAL_GREGORIAN, $data['salary_month'], '20'.$data['salary_year']);
        $end_date = '20'.$data['salary_year'].'-'.$data['salary_month'].'-'.$days_in_month;

        $payroll_array = array();
        foreach($Users as $user){

            $user_id = $user->user_id;
            if($user_id  != 1){
                $resp_array = $this->attendance_postmartam($user_id, $start_date, $end_date);

                array_push($payroll_array, $this->build_array($user_id, $data['salary_month'], $data['salary_year'], $resp_array));


            }
        }

//        dd($payroll_array);

        Payroll::insert($payroll_array);

        flash('Payroll calculated successfully.')->success()->important();

        return redirect()->route('hrm.payroll.index');

    }

    function build_array ($user_id,$month, $year, $data){

        $user_data = $data['employee_info'];
//        dd($data);
        $payroll_data = $data;
        $data_array['user_id'] = $user_id;
        $data_array['account_number'] = $user_data['account_number'];
        $data_array['basic_salary'] = $user_data['basic_salary'];
        $data_array['sc_amount'] = $user_data['sc_amount'];
        $data_array['utilities'] = $user_data['utility'];
        $data_array['house_rent'] = $user_data['house_rent'];
        $data_array['bank_id'] = $user_data['bank_id'];

        $data_array['month'] = $month;
        $data_array['year'] = $year;

        $data_array['half_leaves'] = $payroll_data['half'];
        $data_array['sandwich_leaves'] =$payroll_data['sandwitch_count'];
        $data_array['wages'] = 0;
        $data_array['total_after_tax'] = $payroll_data['total_after_tax'];

        $data_array['overtime'] = $user_data['overtime'];
        $data_array['total_pf'] = $user_data['pf'];
        $data_array['total_bonus'] = $user_data['total_bonus'];
        $data_array['total_commission'] = $user_data['total_commission'];
        $data_array['tax_percentage'] = 0;

        $data_array['tax_amount'] = $payroll_data['tax_amount'];
        $data_array['leaves'] = $payroll_data['full'];
        $data_array['short_leaves'] = $payroll_data['short'];
        $data_array['allowed_leaves'] = $payroll_data['allowed_leaves'];
        $data_array['presents'] = $payroll_data['pres'];


        $data_array['paid_amount'] = $payroll_data['total_after_tax'];
        $data_array['pending_amount'] = 0;
        $data_array['deducted_amount'] = $payroll_data['deducted_amount'];
        $data_array['gross_total'] = $payroll_data['gross_salary'];
//        $data_array['paid_by'] = Auth::user()->id;

//        $data_array['comments'] = 'Payroll Automatic calculated. Errors and omissions are expected';
        $data_array['status'] = 1;
        $data_array['created_by'] = Auth::user()->id;
        $data_array['created_at'] = date('Y-m-d H:m:s');
        $data_array['updated_at'] = date('Y-m-d H:m:s');
        return $data_array;
        
    }


}
