<?php

namespace App\Http\Controllers\HRM;

use App\Models\HRM\OrganizationLocations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Organizations\StoreUpdateRequest;
use App\Models\HRM\Organizations;
use Auth;

class OrganizationsController extends Controller
{
    /**
     * Display a listing of Organization.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('organizations_manage')) {
            return abort(401);
        }

        $Organization = Organizations::findOrFail(1);
        $OrganizationLocation = OrganizationLocations::findOrFail(1);

        return view('hrm.organizations.index', compact('Organization', 'OrganizationLocation'));
    }


    /**
     * Show the form for editing Organization.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('organizations_edit')) {
            return abort(401);
        }

        $Organization = Organizations::findOrFail($id);

        return view('hrm.organizations.edit', compact('Organization'));
    }

    /**
     * Update Organization in storage.
     *
     * @param  \App\Http\Requests\HRM\Organizations\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('organizations_edit')) {
            return abort(401);
        }

        $Organization = Organizations::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $Organization->update($data);

        // Update Default Location's Phone & Fax Information
        $OrganizationLocation = OrganizationLocations::findOrFail(1);
        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;
        $OrganizationLocation->update($data);


        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.organizations.index');
    }

}
