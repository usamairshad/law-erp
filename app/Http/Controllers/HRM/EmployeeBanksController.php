<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\EmployeeBanks\StoreUpdateRequest;
use App\Models\HRM\EmployeeBanks;
use App\Models\Admin\ProductsModel;
use App\Models\HRM\Employees;

use Auth;
use Config;

class EmployeeBanksController extends Controller
{

    public function index()
    {

        if (!Gate::allows('employee_banks_manage')) {
            return abort(401);
        }

        $EmployeeBanks = EmployeeBanks::OrderBy('created_at', 'desc')->get();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        return view('hrm.employee_banks.index', compact('EmployeeBanks','Employees'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('employee_banks_create')) {
            return abort(401);
        }

        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $Products = ProductsModel::pluckActiveOnly();
        $Products->prepend('Select a Product', '');
        return view('hrm.employee_banks.create', compact('Employees','Products'));

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_banks_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        EmployeeBanks::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employee_banks.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employee_banks_edit')){
            return abort(401);
        }

        $EmployeeBanks = EmployeeBanks::findOrFail($id);
        //echo '<pre>'; print_r($EmployeeBanks); exit;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        return view('hrm.employee_banks.edit', compact('EmployeeBanks', 'Employees'));
    }


    public function details($id)
    {
        if (! Gate::allows('employee_banks_details')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $Databanks = Databanks::pluckActiveOnly($loggedIn);
        $Databanks->prepend('Select a Databank', '');
        $Contact = Contacts::contactById($id);

        return view('hrm.employee_banks.details', compact('Contact','Employees','Databanks'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('employee_banks_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $EmployeeBanks = EmployeeBanks::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;
//dd($data);
        $EmployeeBanks->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employee_banks.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employee_banks_destroy')) {
            return abort(401);
        }
        $Employee = Contacts::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(StoreUpdateRequest $request)
    {

//
//        if (! Gate::allows('employee_banks_datatables')) {
//            return abort(401);
//        }
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        if( isset($data['user'] )){
            $Resource = EmployeeBanks::where('user_id', $data['user'])->OrderBy('created_at', 'desc')->get();
        }
        else{
            $Resource = EmployeeBanks::OrderBy('created_at', 'desc')->get();
        }

        return  datatables()->of($Resource)
            ->addColumn('user_name', function($Resource) {
                $text = '<a href="employees/details/'.$Resource->user_id.'" > '.$Resource->emp_name->first_name .'</a>';
                return $text;

            })
            ->addColumn('bank_name', function($Resource) {

                return $Resource->bank_name->bank_name;

            })
            ->addColumn('created_at', function($Resource) {

                return date('d-m-Y h:m a', strtotime($Resource->created_at));

            })

            ->addColumn('action', function ($Resource) {
                $action_column = '<a href="employee_banks/'.$Resource->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('employee_banks_destroy')){
                    $action_column .= '<form method="post" action="employee_banks/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Resource->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['user_name','branch','created_at','action'])
            ->toJson();
        //return view('hrm.employee_banks.details', compact('Contact','OrganizationLocations','Employees'));
    }

    public function delete(StoreUpdateRequest $request)
    {

        if (! Gate::allows('employee_banks_destroy')) {
            return abort(401);
        }
        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employee_banks.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_banks_active')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.employee_banks.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_banks_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.employee_banks.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public function fileUpload(Request $request)

    {

       $this->validate($request, [

            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);


        $image = $request->file('image');

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/uploads/profile_image');

        $image->move($destinationPath, $input['imagename']);


        //$this->postImage->add($input);


        return $input['imagename'];

    }

}
