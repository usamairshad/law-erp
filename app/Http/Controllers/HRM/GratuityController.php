<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Gratuity\StoreUpdateRequest;
use App\Models\HRM\Gratuity;
use App\Models\Admin\ProductsModel;
use App\Models\HRM\Employees;

use Auth;
use Config;
use DB;

class GratuityController extends Controller
{

    public function index()
    {

        if (!Gate::allows('gratuity_manage')) {
            return abort(401);
        }

 //       $Gratuity = Gratuity::OrderBy('created_at', 'desc')->get();
        $Gratuity = Gratuity::where('status', '1')->OrderBy('created_at', 'desc')->get();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        return view('hrm.gratuity.index', compact('Gratuity','Employees'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('gratuity_create')) {
            return abort(401);
        }

        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $Products = ProductsModel::pluckActiveOnly();
        $Products->prepend('Select a Product', '');
        return view('hrm.gratuity.create', compact('Employees','Products'));

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('gratuity_create')) {
            return abort(401);
        }

        $data = $request->all();

        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $data['paid_on'] = date("Y-m-d H:i:s");
        $data['status'] = 1;
      //  dd($data);
        Gratuity::create($data);


        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.gratuity.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('gratuity_edit')){
            return abort(401);
        }

        $Gratuity = Gratuity::findOrFail($id);
        //echo '<pre>'; print_r($Gratuity); exit;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $Products = ProductsModel::pluckActiveOnly();
        $Products->prepend('Select a Product', '');

        // echo '<pre>'; print_r($checkin_time); exit;
        return view('hrm.gratuity.edit', compact('Gratuity', 'Employees','Products'));
    }


    public function details($id)
    {
        if (! Gate::allows('gratuity_details')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $Databanks = Databanks::pluckActiveOnly($loggedIn);
        $Databanks->prepend('Select a Databank', '');
        $Contact = Contacts::contactById($id);

        return view('hrm.gratuity.details', compact('Contact','Employees','Databanks'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('gratuity_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $Gratuity = Gratuity::findOrFail($id);
        $data = $request->all();
        $data['checkin_time'] = $data['att_date'] .' '. $data['checkin_time'];
        $data['checkout_time'] = $data['att_date'] .' '. $data['checkout_time'];
        $data['date_time'] = $data['att_date'] .' '. $data['checkin_time'];
        $data['branch_id'] = 0;
        $data['updated_by'] = Auth::user()->id;

        $Gratuity->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.gratuity.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('gratuity_destroy')) {
            return abort(401);
        }
        $Employee = Contacts::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(StoreUpdateRequest $request)
    {


        if (! Gate::allows('gratuity_datatables')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;

        $Gratuity = Gratuity::where('status', '1')->OrderBy('created_at', 'desc')->get();

        return  datatables()->of($Gratuity)
            ->addColumn('user_name', function($Gratuity) {
                $text = '<a href="employees/details/'.$Gratuity->user_id.'" > '.$Gratuity->emp_name->first_name .'</a>';
                return $text;

            })

            ->addColumn('action', function ($Gratuity) {
                $action_column = '<a href="gratuity/'.$Gratuity->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('gratuity_destroy')){
                    $action_column .= '<form method="post" action="gratuity/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Gratuity->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['user_name','branch','action'])
            ->toJson();
        //return view('hrm.gratuity.details', compact('Contact','OrganizationLocations','Employees'));
    }

    public function delete(StoreUpdateRequest $request)
    {

        if (! Gate::allows('gratuity_destroy')) {
            return abort(401);
        }
        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.gratuity.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(StoreUpdateRequest $request)
    {
        if (! Gate::allows('gratuity_active')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.gratuity.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(StoreUpdateRequest $request)
    {
        if (! Gate::allows('gratuity_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.gratuity.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    public function getGratuityDetail(Request $request)
    {
        if (! Gate::allows('gratuity_create')) {
            return abort(401);
        }
        if ($request->input('user_id')) {

            $Gratuity = Gratuity::employeeGratuityDetailByUserId($request->input('user_id'));
            if(count($Gratuity) > 0){
                $response_data['status'] = 0;
                return response()->json($response_data);
            }

            $emp_detail = Employees::employeeFullDetailByUserId( $request->input('user_id') );

            $years = Employees::selectRaw('floor(datediff(perm_end_date,perm_start_date) / 365) as years')
                ->where('user_id',$request->input('user_id'))->first();
            // get last salary of the last year
            $second_last = DB::table('hrm_employees_salary_detail')->where('employee_id', $request->input('user_id'))
                ->offset(1)
                ->limit(1)
                ->orderBy('id', 'desc')->first();
            if(! isset($emp_detail) || ! isset($second_last) || ! isset($years)){
                // missing some information
                $response_data['status'] = 2;
                return response()->json($response_data);
            }
            $response_data['status'] = 1;
            $response_data['basic'] = $emp_detail;
            $response_data['second_last'] = $second_last;
            $response_data['years'] = $years;

             return response()->json($response_data);

        }
    }

}
