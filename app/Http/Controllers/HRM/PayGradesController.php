<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\PayGrades\StoreUpdateRequest;
use App\Models\HRM\PayGrades;
use Auth;

class PayGradesController extends Controller
{
    /**
     * Display a listing of PayGrade.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('pay_grades_manage')) {
            return abort(401);
        }

        $PayGrades = PayGrades::OrderBy('created_at','desc')->get();

        return view('hrm.pay_grades.index', compact('PayGrades'));
    }

    /**
     * Show the form for creating new PayGrade.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('pay_grades_create')) {
            return abort(401);
        }
        return view('hrm.pay_grades.create');
    }

    /**
     * Store a newly created PayGrade in storage.
     *
     * @param  \App\Http\Requests\HRM\PayGrades\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('pay_grades_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        PayGrades::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.pay_grades.index');
    }


    /**
     * Show the form for editing PayGrade.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('pay_grades_edit')) {
            return abort(401);
        }
        $PayGrade = PayGrades::findOrFail($id);

        return view('hrm.pay_grades.edit', compact('PayGrade'));
    }

    /**
     * Update PayGrade in storage.
     *
     * @param  \App\Http\Requests\HRM\PayGrades\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('pay_grades_edit')) {
            return abort(401);
        }

        $PayGrade = PayGrades::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $PayGrade->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.pay_grades.index');
    }


    /**
     * Remove PayGrade from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('pay_grades_destroy')) {
            return abort(401);
        }
        $PayGrade = PayGrades::findOrFail($id);
        $PayGrade->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.pay_grades.index');
    }

    /**
     * Activate PayGrade from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('pay_grades_active')) {
            return abort(401);
        }

        $PayGrade = PayGrades::findOrFail($id);
        $PayGrade->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.pay_grades.index');
    }

    /**
     * Inactivate PayGrade from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('pay_grades_inactive')) {
            return abort(401);
        }

        $PayGrade = PayGrades::findOrFail($id);
        $PayGrade->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.pay_grades.index');
    }

    /**
     * Delete all selected PayGrade at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('pay_grades_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = PayGrades::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
