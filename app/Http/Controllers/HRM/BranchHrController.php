<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\HRM\Employees\StoreUpdateRequest;
use App\User;
use App\Helpers\Helper;
use App\Models\Admin\BranchUser;
use Auth;
use DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BranchHrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_branchhr_manage')) {
            return abort(401);
        }
        
        $hr = User::getSpecificRoleUsers("HR");
        $branch_id = User::getSpecificRoleUsers("HR")->pluck('branch_id');

        // $hr= User::where('name', 'like', '%'.'HR'.'%')->where('name','!=', 'HR head')->get();
        return view('hrm.branch_hr.index', compact('hr','branch_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_branchhr_create')) {
            return abort(401);
        }
        return view('hrm.branch_hr.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_branchhr_create')) {
            return abort(401);
        }

        $this->validate($request, [
            
            'email' => 'required|email|unique:erp_users',
        ]);

        $data = $request->all();
        $data['active'] = 1;      
        $data['created_by'] = Auth::user()->id; 
        $data['password'] = bcrypt('12345678');    
        //dd($data); 
        $user = User::create($data);
        $user->assignRole('HR');
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.branchhr.index');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_branchhr_edit')) {
            return abort(401);
        }
        $hr = User::findOrFail($id);

        return view('hrm.branch_hr.edit', compact('hr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_branchhr_edit')) {
            return abort(401);
        }


        $hr = User::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $hr->update($data);

        $temp=DB::table('erp_branch_users')->update([
            'branch_id'=>$data['branch_id'],
            'updated_by'=>$data['updated_by'],
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.branchhr.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_branchhr_destroy')) {
            return abort(401);
        }
        $Employee = User::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.branchhr.index');
    }
    public function active($id)
    {
        if (! Gate::allows('erp_branchhr_active')) {
            return abort(401);
        }
        
        $hr = User::findOrFail($id);
        $hr->update(['active' => 1]);
        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.branchhr.index');
    }
    public function inactive($id)
    {
        if (! Gate::allows('erp_branchhr_inactive')) {
            return abort(401);
        }
       
        $hr = User::findOrFail($id);
        $hr->update(['active' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.branchhr.index');
    }
}
