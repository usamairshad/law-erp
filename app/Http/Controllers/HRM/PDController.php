<?php

namespace App\Http\Controllers\HRM;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\ActiveSessionTeacher;
use App\Models\Academics\BoardBranch;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\BranchBoardPrograms;
use App\Models\Academics\Course;
use App\Models\Academics\Staff;
use App\Models\Academics\TeacherClass;
use App\User;
use Spatie\Permission\Models\Role;

class PDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        if ($req->user_id != "" && $req->user_id != null && $req->role_id != "" && $req->role_id != null) {

            $user = User::with('staff')->where("id", $req->user_id)->first();

            $role = Role::find($req->role_id);

            RoleUser::where('id',Auth::user()->id)->update(['role_id'=>$role_id]);

            $user->staff->staff_type = $role->name;

            $user->staff->save();

            $user->syncRoles([$role->name]);
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getBoard($branch_id)
    {
        //---------------------get board old -------------------//
        /*  $getboradsByBranch = BranchBoardPrograms::with('branch', 'board')->where("branch_id", $branch_id)->get();
        return $getboradsByBranch; */
        //---------------------get board old end-------------------//

        $getboradsByBranch = ActiveSession::with('board')
            ->where("branch_id", $branch_id)
             ->groupBy('board_id')
            ->get();

        return $getboradsByBranch;
    }


    public function getProgram($branch_id, $board_id)
    {
        $getboradsByBranch = ActiveSession::with("program", 'branch', 'board')
            ->where("branch_id", $branch_id)
            ->where("board_id", $board_id)
            ->groupBy('program_id')
            ->get();

        return $getboradsByBranch;
    }



    public function getClasses($branch_id, $board_id, $programs_id)
    {
        $getClasses = ActiveSession::with("getClass")
            ->where("board_id", $board_id)
            ->where("program_id", $programs_id)
            ->where("branch_id", $branch_id)
            ->groupBy('class_id')
            ->get();

        return $getClasses;
    }


    public function assginClass(Request $req)
    {


        if (
            $req->active_session_id == null || $req->active_session_id == "null"
            || empty($req->courses) == true
        ) {
            flash("something went wrong")->error()->important();
            return redirect()->back();
        }

        $staffID = Helper::getStaffIDByUserID($req->active_teacher_id)->id;

        foreach ($req->courses as $course) {

            ActiveSessionTeacher::updateOrCreate([
                "active_session_id" => $req->active_session_id,
                "course_id" => $course,
                "staff_id" => $staffID
            ]);
        }


        return redirect()->back();
        /* dd($req->input());
        if ($req->user_id != null || $req->class_id != null) {
            return redirect()->back();
        }
        TeacherClass::updateOrCreate(
            ["user_id" => $req->user_id, "class_id" => $req->class_id],
        );

        flash("Class successfully changed")->success()->important();
        return redirect()->back(); */
    }
}
