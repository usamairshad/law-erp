<?php

namespace App\Http\Controllers\HRM;

use App\Models\HRM\AttendanceMachine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\EmployeeAttendance\StoreUpdateRequest;
use App\Models\HRM\EmployeeAttendance;
use App\Models\HRM\Employees;
use App\Models\Admin\Branches;
use Auth;
use Config;

class EmployeeAttendanceController extends Controller
{

    public function index(){
        if (!Gate::allows('attendance_manage')) {
            return abort(401);
        }

        // dd(EmployeeAttendance::attendanceByUserId( 1 ));

        $EmployeeAttendance = EmployeeAttendance::OrderBy('created_at', 'desc')->get();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
//dd($EmployeeAttendance);
        return view('hrm.employee_attendance.index', compact('EmployeeAttendance','Employees'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        if (! Gate::allows('attendance_create')) {
            return abort(401);
        }

        $Employees= array();
//        // for super admin get all employee
        if(Auth::user()->id == 1){
            $Employees = Employees::pluckActiveWithID();
            $Employees->prepend('Select an Employee ', '');
        }

        return view('hrm.employee_attendance.create', compact('Employees'));

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request){
        if (! Gate::allows('attendance_create')) {
            return abort(401);
        }

        $loggedIn = Auth::user()->id;
        $data = $request->all();
        $data['user_id'] = isset( $data['user_id'] ) ? $data['user_id'] :  Auth::user()->id;
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        $emp = Employees::employeeDetailByUserId($loggedIn);

        if( $emp->job_title == 19 || $emp->job_title == 20 || $emp->job_title == 24 ||$emp->job_title == 25  ){
            // set status to 3 for branch manager approval
            $data['status'] = 3;
        }
        else{
            // For other employees on HR approval
            $data['status'] = 2;
        }
        if($loggedIn == 1){
            $data['status'] = 1;
        }
        $data['date_time'] = $data['att_date'] .' '. $data['checkin_time'];
        unset($data['checkin_time']);
        // $data['branch_id'] = Employees::getBranchByUserId($data['user_id']);
        $data['branch_id'] = 1;

        EmployeeAttendance::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.attendance.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        if (! Gate::allows('employee_attendance_edit')){
            return abort(401);
        }

        $EmployeeAttendance = EmployeeAttendance::attendanceDetailById($id);

        //echo '<pre>'; print_r($EmployeeAttendance); exit;
        isset($EmployeeAttendance->checkout_time) ? $checkout_time = date("H:i",strtotime($EmployeeAttendance->checkout_time)) : $checkout_time='';
        isset($EmployeeAttendance->checkin_time) ? $checkin_time = date("H:i",strtotime($EmployeeAttendance->checkin_time)) : $checkin_time='';
        isset($EmployeeAttendance->created_at) ? $att_date = date("Y-m-d",strtotime($EmployeeAttendance->created_at)) : $att_date='';

        $EmployeeAttendance->checkout_time = $checkout_time;
        $EmployeeAttendance->checkin_time = $checkin_time;

        $EmployeeAttendance['att_date'] = explode(' ', $EmployeeAttendance->date_time)[0];
        $EmployeeAttendance['checkin_time'] = explode(' ', $EmployeeAttendance->date_time)[1];

        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a Employee', '');

        // echo '<pre>'; print_r($checkin_time); exit;
        return view('hrm.employee_attendance.edit', compact('EmployeeAttendance', 'Employees'));
    }


    public function details($id){
        if (! Gate::allows('employee_attendance_details')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $Databanks = Databanks::pluckActiveOnly($loggedIn);
        $Databanks->prepend('Select a Databank', '');
        $Contact = Contacts::contactById($id);

        return view('hrm.attendance.details', compact('Contact','Employees','Databanks'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id){
        if (! Gate::allows('employee_attendance_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $EmployeeAttendance = EmployeeAttendance::findOrFail($id);
        $data = $request->all();
        // $data['checkin_time'] = $data['att_date'] .' '. $data['checkin_time'];
        // $data['checkout_time'] = $data['att_date'] .' '. $data['checkout_time'];
        $data['date_time'] = $data['att_date'] .' '. $data['checkin_time'];
        // $data['branch_id'] = Employees::getBranchByUserId($data['user_id']);
        $data['branch_id'] = 1;
        unset($data['checkin_time']);
        $data['updated_by'] = Auth::user()->id;

        $EmployeeAttendance->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.attendance.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
        // if (! Gate::allows('employee_attendance_destroy')) {
        //     return abort(401);
        // }
       dd($request);
        $Employee = EmployeeAttendance::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(Request $request){

        if (! Gate::allows('attendance_datatables')) {
            return abort(401);
        }
        //dd('not ok');
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        $Attendance = Array();

        if( isset($data['user_id']) && ! isset($data['from_date']) && ! isset($data['to_date']) ){

            $Attendance = EmployeeAttendance::attendanceByUserId( $data['user_id'] );

        }
        else if(! isset($data['user_id']) && isset($data['from_date']) && ! isset($data['to_date']) ){

            $Attendance = EmployeeAttendance::attendanceByDate( $data['from_date'] );

        }
        else if( isset($data['user_id']) && isset($data['from_date']) && ! isset($data['to_date']) ){

            $Attendance = EmployeeAttendance::attendenceByUserAndDate( $data['user_id'] , $data['from_date'] );
        }
        else if( isset($data['user_id']) && isset($data['from_date']) && isset($data['to_date']) ){

            $Attendance = EmployeeAttendance::attendanceByDatesAndUser($data['user_id'] , $data['from_date'] , $data['to_date']);

        }
        //$loggedIn = Auth::user()->id;


        return  datatables()->of($Attendance)
            ->addColumn('user_name', function($Attendance) {
                $text = '<a href="attendance/details/'.$Attendance->id.'" > '.$Attendance->first_name .'</a>';
                return $text;

            })
            ->addColumn('branch', function($Attendance) {
                $text = '<a href="/hrm/databanks/details/'.$Attendance->branch.'" > '.$Attendance->name .'</a>';
                return $text;

            })
            ->addColumn('attendance_status', function($Attendance) {
                return Config::get('hrm.attendance_status_array.'. $Attendance->type) ;

            })
            ->addColumn('action', function ($Attendance) {
//                $action_column = '<a href="attendance/'.$Attendance->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                $action_column='';
//                if($Attendance->status && Gate::check('attendance_inactive')){
//
//                    $action_column .= '<form method="post" action="attendance/inactive" >';
//                    $action_column .=  csrf_field();
//                    $action_column .= '<input type="hidden" value='.$Attendance->id.' name="first_name" >';
//                    $action_column .= '<input   value="Inactivate"  class="btn btn-xs btn-warning" type="submit"  onclick="return confirm(\'Are you sure to inactivate this record? \')"> </form>';
//
//                }else if (! $Attendance->status && Gate::check('attendance_active'))
//                {
//                    $action_column .= '<form method="post" action="attendance/active" >';
//                    $action_column .=  csrf_field();
//                    $action_column .= '<input type="hidden" value='.$Attendance->id.' name="first_name" >';
//                    $action_column .= '<input   value="Activate"  class="btn btn-xs btn-primary" type="submit"  onclick="return confirm(\'Are you sure you want to activate this record? \')"> </form>';
//                }
 // if (! Gate::allows('employee_attendance_destroy')) {
        //     return abort(401);
        // }
                if(Gate::check('employee_attendance_destroy')){
                    $action_column .= '<form method="post" action="attendance/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Attendance->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['user_name','branch','action'])
            ->toJson();
        //return view('hrm.attendance.details', compact('Contact','OrganizationLocations','Employees'));
    }

    public function datatables1(Request $request){
        if (! Gate::allows('employee_attendance_datatables')) {
            return abort(401);
        }

        $data = $request->all();
        $Target = Array();

        if( isset($data['user_id']) && ! isset($data['from_date']) && ! isset($data['to_date']) ){

            if(!empty(EmployeeAttendance::evaluateFaultedAttendanceByUser($data['user_id']))){

                $Temp = EmployeeAttendance::evaluateFaultedAttendanceByUser($data['user_id']);

                foreach($Temp as $temp){

                    foreach(EmployeeAttendance::attendenceByUserAndDate($temp[0],$temp[1]) as $item){
                        array_push($Target, $item);
                    }
                }
                //Converting array to laravel collection
                $Target = collect($Target);
            }

        }
        else if(! isset($data['user_id']) && isset($data['from_date']) && ! isset($data['to_date']) ){
            
            if(!empty(EmployeeAttendance::evaluateFaultedAttendanceByDate( $data['from_date'] ))){

                $Temp = EmployeeAttendance::evaluateFaultedAttendanceByDate( $data['from_date'] );

                foreach($Temp as $temp){
                    
                    foreach(EmployeeAttendance::attendenceByUserAndDate($temp[0],$temp[1]) as $item){
                        array_push($Target, $item);
                    }
                }
                //Converting array to laravel collection
                $Target = collect($Target);
            }

        }
        else if( isset($data['user_id']) && isset($data['from_date']) && ! isset($data['to_date']) ){
            
            if(!empty(EmployeeAttendance::evaluateFaultedAttendanceByUserAndDate($data['user_id'], $data['from_date']))){

                $Temp = EmployeeAttendance::evaluateFaultedAttendanceByUserAndDate( $data['user_id'], $data['from_date'] );

                foreach($Temp as $temp){
                    
                    foreach(EmployeeAttendance::attendenceByUserAndDate($temp[0],$temp[1]) as $item){
                        array_push($Target, $item);
                    }
                }
                //Converting array to laravel collection
                $Target = collect($Target);
            }
            
        }
        else if( isset($data['user_id']) && isset($data['from_date']) && isset($data['to_date']) ){
            
            if(!empty(EmployeeAttendance::evaluateFaultedAttendance($data['user_id'], $data['from_date'], $data['to_date']))){

                $Temp = EmployeeAttendance::evaluateFaultedAttendance($data['user_id'], $data['from_date'], $data['to_date']);

                foreach($Temp as $temp){

                    foreach(EmployeeAttendance::attendenceByUserAndDate($temp[0],$temp[1]) as $item){
                        array_push($Target, $item);
                    }
                }
                //Converting array to laravel collection
                $Target = collect($Target);
            }

        }
        else{
            $Target = Array();
        }

        return datatables()->of($Target)
            ->addColumn('id', function($Target){

                return $Target->id;

            })->addColumn('user_name', function($Target){

                return Employees::getEmployeeNameByUserId($Target->user_id);

            })->addColumn('job_title', function($Target){

                return Employees::getJobTitleByUserId($Target->user_id);

            })->addColumn('time', function($Target){
                
                $time = explode(' ',$Target->date_time);
                return $time[1];

            })->addColumn('date', function($Target){

                $time = explode(' ',$Target->date_time);
                return $time[0];

            })->addColumn('type', function($Target){

                return Config::get('hrm.attendance_status_array.'.$Target->type);

            })->addColumn('created_at', function($Target){

                return $Target->created_at;

            })->rawColumns(['user_name','date','time'])
            ->toJson();
    }    

    public function delete(Request $request){

        // if (! Gate::allows('employee_attendance_destroy')) {
        //     return abort(401);
        // }
        $data = $request->all();
        // dd($data);
        $EmployeeAttendance = EmployeeAttendance::findOrFail($data['first_name']);
        $EmployeeAttendance->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.attendance.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request){

        if (! Gate::allows('employee_attendance_final_approve') && ! Gate::allows('employee_attendance_approve')) {
            return abort(401);
        }

        $data = $request->all();
        $loggedIn = Auth::user()->id;
        $EmployeeAttendance = EmployeeAttendance::findOrFail($data['id']);
        //
        if( Gate::allows('employee_attendance_final_approve') ){
            $updated_data['status'] = 1;
            $updated_data['approved_by'] = $loggedIn;
            $updated_data['updated_by'] = $loggedIn;
            $EmployeeAttendance->update($updated_data);

        } else if(Gate::allows('employee_attendance_approve')){
            $updated_data['status'] = 2;
            $updated_data['updated_by'] = $loggedIn;
            $updated_data['approved_by'] = $loggedIn;
            $EmployeeAttendance->update($updated_data);

        }

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.attendance.requests');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(Request $request){
        if (! Gate::allows('employee_attendance_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.attendance.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request){
        if (! Gate::allows('employee_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public function fileUpload(Request $request){

       $this->validate($request, [

            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);


        $image = $request->file('image');

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/uploads/profile_image');

        $image->move($destinationPath, $input['imagename']);


        //$this->postImage->add($input);


        return $input['imagename'];

    }

    public function requests (Request $request){
        if (!Gate::allows('attendance_manage')) {
            return abort(401);
        }

        $loggedIn = Auth::user()->id;

        $emp = Employees :: employeeDetailByUserId($loggedIn);
       // echo $loggedIn;
        $EmployeeAttendance=array();
        // fetch branch data only
        if (Gate::allows('employee_attendance_request')) {
            if ($emp->job_title == 23 ) {
//                dd('ok');
                $EmployeeAttendance = EmployeeAttendance::allReqByBranch(array($emp->branch_id), $loggedIn);

            }elseif($emp->job_title == 22){

                $Branches = Branches::where('region_id',$emp->region_id)->pluck('id');
                $EmployeeAttendance = EmployeeAttendance::allReqByBranch($Branches, $loggedIn);

            }elseif ($emp->job_title == 8){
                $EmployeeAttendance = EmployeeAttendance::allReq();
            }
             else {

                $EmployeeAttendance = EmployeeAttendance::requestByUserId(Auth::user()->id);
            }
        }
       // dd($EmployeeAttendance);
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');

        return view('hrm.employee_attendance.requests', compact('EmployeeAttendance','Employees'));
    }


    public function syncAttendance(){

        $syncAtt=AttendanceMachine::syncAttendance();
        if($syncAtt){
            flash('Attendance Has been synced.')->success()->important();
        }else{
            flash('There\'s been an issue while Syncing.')->error()->important();

        }
        return redirect()->route('hrm.attendance.index');
    }

}
