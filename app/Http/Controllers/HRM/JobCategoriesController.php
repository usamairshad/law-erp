<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\JobCategories\StoreUpdateRequest;
use App\Models\HRM\JobCategories;
use Auth;

class JobCategoriesController extends Controller
{
    /**
     * Display a listing of PayGrade.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('job_categories_manage')) {
            return abort(401);
        }

        $JobCategories = JobCategories::OrderBy('created_at','desc')->get();

        return view('hrm.job_categories.index', compact('JobCategories'));
    }

    /**
     * Show the form for creating new JobCategorieGrade.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('job_categories_create')) {
            return abort(401);
        }
        return view('hrm.job_categories.create');
    }

    /**
     * Store a newly created JobCategorie in storage.
     *
     * @param  \App\Http\Requests\HRM\JobCategories\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('job_categories_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        JobCategories::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.job_categories.index');
    }


    /**
     * Show the form for editing JobCategorie.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('job_categories_edit')) {
            return abort(401);
        }
        $JobCategorie = JobCategories::findOrFail($id);

        return view('hrm.job_categories.edit', compact('JobCategorie'));
    }

    /**
     * Update JobCategorie in storage.
     *
     * @param  \App\Http\Requests\HRM\JobCategories\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('job_categories_edit')) {
            return abort(401);
        }

        $JobCategorie = JobCategories::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $JobCategorie->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.job_categories.index');
    }


    /**
     * Remove JobCategorie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('job_categories_destroy')) {
            return abort(401);
        }
        $JobCategorie = JobCategories::findOrFail($id);
        $JobCategorie->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.job_categories.index');
    }

    /**
     * Activate JobCategorie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('job_categories_active')) {
            return abort(401);
        }

        $JobCategorie = JobCategories::findOrFail($id);
        $JobCategorie->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.job_categories.index');
    }

    /**
     * Inactivate JobCategorie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('job_categories_inactive')) {
            return abort(401);
        }

        $JobCategorie = JobCategories::findOrFail($id);
        $JobCategorie->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.job_categories.index');
    }

    /**
     * Delete all selected JobCategorie at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('job_categories_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = JobCategories::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
