<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Employees\StoreUpdateRequest;
use App\Models\Academic\Staff;
use Auth;

class EmployeesController extends Controller
{
    /**
     * Display a listing of Employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('employees_manage')) {
            return abort(401);
        }

        $Employees = Staff::OrderBy('created_at','desc')->get();

        return view('hrm.employees.index', compact('Employees'));
    }

    /**
     * Show the form for creating new Employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('employees_create')) {
            return abort(401);
        }
        return view('hrm.employees.create');
    }

    /**
     * Store a newly created Employee in storage.
     *
     * @param  \App\Http\Requests\HRM\Employees\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employees_create')) {
            return abort(401);
        }

        $data = $request->all();
        print_r($data);exit;
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        Staff::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employees.index');
    }


    /**
     * Show the form for editing Employee.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employees_edit')) {
            return abort(401);
        }
        $Employee = Staff::findOrFail($id);

        return view('hrm.employees.edit', compact('Employee'));
    }

    /**
     * Update Employee in storage.
     *
     * @param  \App\Http\Requests\HRM\Employees\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('employees_edit')) {
            return abort(401);
        }

        $Employee = Staff::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $Employee->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employees.index');
    }


    /**
     * Remove Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employees_destroy')) {
            return abort(401);
        }
        $Employee = Staff::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employees.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('employees_active')) {
            return abort(401);
        }

        $Employee = Staff::findOrFail($id);
        $Employee->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.employees.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('employees_inactive')) {
            return abort(401);
        }

        $Employee = Staff::findOrFail($id);
        $Employee->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.employees.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('employees_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Staff::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


}
