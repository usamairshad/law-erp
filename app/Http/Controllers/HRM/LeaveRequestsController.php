<?php

namespace App\Http\Controllers\HRM;

use App\Models\HRM\Employees;
use App\Models\Academics\Staff;
use App\Models\HRM\LeaveEntitlements;
use App\Models\HRM\Leaves;
use App\Models\HRM\LeaveStatuses;
use App\Models\HRM\LeaveTypes;
use App\Models\HRM\WorkShifts;
use App\Models\HRM\WorkWeeks;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\LeaveRequests\StoreUpdateRequest;
use App\Models\HRM\LeaveRequests;
use Auth;
use DB;

class LeaveRequestsController extends Controller
{
    /**
     * Display a listing of Leave.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

   //     if (! Gate::allows('leave_requests_manage')) {
   //         return abort(401);
   //     }

        $loggedIn = Auth::user()->id;
//        $emp = Employees :: employeeDetailByUserId($loggedIn);
        if($loggedIn == 1)
        {
        $LeaveRequests = LeaveRequests::join('staff','staff.user_id','=', 'erp_hrm_leave_requests.employee_id')->get();
        }
        else
        {
            $LeaveRequests = LeaveRequests::where('employee_id', $loggedIn)->OrderBy('created_at','desc')->get();
        }
        return view('hrm.leave_requests.index', compact('LeaveRequests'));
    }

    /**
     * Show the form for creating new Leave.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    //    if (! Gate::allows('leave_requests_create')) {
    //        return abort(401);
    //    }

        $WorkShifts = WorkShifts::pluckActiveOnly();
        $WorkShifts->prepend('Select a Work Shift', '');

        $LeaveTypes = LeaveTypes::pluckActiveOnly();
        $LeaveTypes->prepend('Select a Leave Type', '');
        $Employees= array();
//        // for super admin get all employee
        if(Auth::user()->id == 1){
            $Employees = Staff::pluckActiveWithID();
            $Employees->prepend('Select an Employee ', '');
        }

   //    dd($WorkShifts);


        $WorkShift = WorkShifts::getFirstWorkShift();

        return view('hrm.leave_requests.create', compact('LeaveTypes', 'Employees', 'WorkShift', 'WorkShifts'));
    }

    /**
     * Store a newly created Leave in storage.
     *
     * @param  \App\Http\Requests\HRM\LeaveRequests\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
   //     if (! Gate::allows('leave_requests_create')) {
   //         return abort(401);
   //     }

        $logged_in = Auth::user()->id;

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $data['applied_date'] = Carbon::now()->format('Y-m-d');

        if(!isset($data['employee_id'])){
            $data['employee_id'] = Auth::user()->id; // the applicant
        }

        $data = $this->generateData($data);

        // Insert Leave Type and Work Shift data as well
        $data['work_shift_data'] = WorkShifts::findOrFail($data['work_shift_id'])->toJson();
        $data['leave_type_data'] = LeaveTypes::findOrFail($data['leave_type_id'])->toJson();


        // Transaction has been started
        DB::beginTransaction();

        $LeaveRequest = LeaveRequests::create($data);


        $leaves_data = $this->generateLeaveData($LeaveRequest->id, $data);

        //dd($leaves_data);

        if(!sizeof($leaves_data)) {
            // Transaction Rolled Back
            DB::rollBack();

            flash('Error in data integritiy, please check again later.')->error()->important();
            $request->flash();
            return redirect()->back()
                ->withInput();
        }

        $appllied_leaves = Leaves::where(['employee_id' => $logged_in, 'status' => 1])->pluck('leave_date')->toArray();

        foreach ($leaves_data as $key => $val){

            if(in_array($val['leave_date'] , $appllied_leaves)){
                DB::rollBack();
                flash('You have already approved leave on the same date')->error()->important();
                $request->flash();
                return redirect()->back()
                    ->withInput();
            }

        }

        Leaves::insert($leaves_data);

        // Transaction Successfull
        DB::commit();

        // Transaction has been ended

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.leave_requests.index');
    }

    /**
     * Skip required durations from array.
     *
     * @param  string $skipper
     * @return mixed $reset_data
     */
    private function skipSection($skipper = null) {
        $reset_data = array();
        $allowed_phrases = ['single', 'all_days', 'starting', 'ending'];

        if(in_array($skipper, $allowed_phrases)) {
            $reset_data[$skipper . '_duration'] = null;
            $reset_data[$skipper . '_shift'] = null;
            $reset_data[$skipper . '_hours_start'] = null;
            $reset_data[$skipper . '_hours_end'] = null;
            $reset_data[$skipper . '_hours_duration'] = null;
        }

        return $reset_data;
    }

    /**
     * Skip Full, Half and Specify data from array.
     *
     * @param  string $skipper
     * @return mixed $reset_data
     */
    private function skipDuration($skipper = null, $duration = false) {
        $reset_data = array();

        $allowed_phrases = ['single', 'all_days', 'starting', 'ending'];
        $allowed_durations = ['full_day', 'half_day', 'specify_time'];

        if(in_array($skipper, $allowed_phrases) && in_array($duration, $allowed_durations)) {
            if($duration == 'full_day') {
                $reset_data[$skipper . '_shift'] = null;
                $reset_data[$skipper . '_hours_start'] = null;
                $reset_data[$skipper . '_hours_end'] = null;
                $reset_data[$skipper . '_hours_duration'] = null;
            } else if($duration == 'half_day') {
                $reset_data[$skipper . '_hours_start'] = null;
                $reset_data[$skipper . '_hours_end'] = null;
                $reset_data[$skipper . '_hours_duration'] = null;
            } else {
                $reset_data[$skipper . '_shift'] = null;
            }
        }

        return $reset_data;
    }

    /**
     * Generate Leave Request data to store in databaae.
     *
     * @param  string $skipper
     * @return mixed $reset_data
     */
    private function generateData($data) {

        $start_date = Carbon::parse($data['start_date']);
        $end_date = Carbon::parse($data['end_date']);
        $difference = ($end_date->diffInDays($start_date) + 1);

        if($difference == '1') {
            $reset_data['partial_days'] = null;
            $reset_data = array_merge($reset_data, $this->skipSection('all_days'));
            $reset_data = array_merge($reset_data, $this->skipSection('starting'));
            $reset_data = array_merge($reset_data, $this->skipSection('ending'));

            $reset_data = array_merge($reset_data, $this->skipDuration('single', $data['single_duration']));
        } else {
            $reset_data = array();
            $reset_data = array_merge($reset_data, $this->skipSection('single'));

            // All Full days have to deduct
            if($data['partial_days'] == 'none') {
                $reset_data = array_merge($reset_data, $this->skipSection('all_days'));
                $reset_data = array_merge($reset_data, $this->skipSection('starting'));
                $reset_data = array_merge($reset_data, $this->skipSection('ending'));
            } else if($data['partial_days'] == 'all') {
                // Discard Start Day and End Day Data
                $reset_data = array_merge($reset_data, $this->skipSection('starting'));
                $reset_data = array_merge($reset_data, $this->skipSection('ending'));

                $reset_data = array_merge($reset_data, $this->skipDuration('all_days', $data['all_days_duration']));
            } else {
                // Start or End Day Process
                $reset_data = array();
                $reset_data = array_merge($reset_data, $this->skipSection('all_days'));

                if($data['partial_days'] == 'start') {
                    $reset_data = array_merge($reset_data, $this->skipSection('ending'));
                    $reset_data = array_merge($reset_data, $this->skipDuration('starting', $data['starting_duration']));
                } else if($data['partial_days'] == 'end') {
                    $reset_data = array_merge($reset_data, $this->skipSection('starting'));
                    $reset_data = array_merge($reset_data, $this->skipDuration('ending', $data['ending_duration']));
                } else {
                    $reset_data = array_merge($reset_data, $this->skipDuration('starting', $data['starting_duration']));
                    $reset_data = array_merge($reset_data, $this->skipDuration('ending', $data['ending_duration']));
                }
            }
        }

        return array_merge($data, $reset_data);
    }

    /**
     * Adjust hours in provided leaves array.
     *
     * @param  mixed $data
     * @param  string $skipper
     * @param  int $day
     * @param  mixed $Leaves
     * @param  mixed $WorkWeek
     * @param  mixed $work_shift_criteria
     * @return void
     */
    private function adjustHours($leave_request_id, $data, $skipper, $day, $WorkWeek, $work_shift_criteria) {

        $date = Carbon::parse($data['start_date']);
        $date->addDays($day);
        $formated_day = strtolower($date->format('D'));

        $leave = array(
            'leave_request_id' => $leave_request_id,
            'employee_id' => $data['employee_id'],
            'leave_type_id' => $data['leave_type_id'],
            'work_shift_id' => $data['work_shift_id'],
            'leave_date' => $date->format('Y-m-d'),
            'status' => 3,
            'start_time' => '00:00',
            'end_time' => '00:00',
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'shift' => null,
        );

        if($data[$skipper .'_duration'] == 'full_day') {
            if($WorkWeek[$formated_day] == 0) {
                $leave['hours'] = $work_shift_criteria['full_hours'];
                $leave['leave_status_id'] = 3;
                $leave['shift'] = 'full';
            } else if($WorkWeek[$formated_day] == 4) {
                $leave['hours'] = $work_shift_criteria['half_hours'];
                $leave['leave_status_id'] = 3;
                $leave['shift'] = 'half';
            } else if($WorkWeek[$formated_day] == 8) {
                $leave['hours'] = 0;
                $leave['leave_status_id'] = 6;
                $leave['shift'] = 'full';
            }
        } else if($data[$skipper .'_duration'] == 'half_day') {
            if($WorkWeek[$formated_day] == 0) {
                $leave['hours'] = $work_shift_criteria['half_hours'];
                $leave['leave_status_id'] = 3;
                $leave['shift'] = 'half';
            } else if($WorkWeek[$formated_day] == 4) {
                $leave['hours'] = $work_shift_criteria['half_hours'];
                $leave['leave_status_id'] = 3;
                $leave['shift'] = 'half';
            } else if($WorkWeek[$formated_day] == 8) {
                $leave['hours'] = 0;
                $leave['leave_status_id'] = 6;
                $leave['shift'] = 'full';
            }
        } else {
            $hours_difference = Carbon::parse($data[$skipper . '_hours_start'])->diffInMinutes(Carbon::parse($data[$skipper . '_hours_end'])) / 60;

            if($WorkWeek[$formated_day] == 0) {
                $leave['hours'] = $hours_difference;
                $leave['leave_status_id'] = 3;
                $leave['shift'] = 'specific';
                $leave['start_time'] = $data[$skipper . '_hours_start'];
                $leave['end_time'] = $data[$skipper . '_hours_end'];
            } else if($WorkWeek[$formated_day] == 4) {
                $leave['hours'] = $hours_difference;
                $leave['leave_status_id'] = 3;
                $leave['shift'] = 'specific';
                $leave['start_time'] = $data[$skipper . '_hours_start'];
                $leave['end_time'] = $data[$skipper . '_hours_end'];
            } else if($WorkWeek[$formated_day] == 8) {
                $leave['hours'] = 0;
                $leave['leave_status_id'] = 6;
                $leave['shift'] = 'specific';
                $leave['start_time'] = $data[$skipper . '_hours_start'];
                $leave['end_time'] = $data[$skipper . '_hours_end'];
            }
        }

        return $leave;
    }

    /**
     * Generate Leave data to store in databaae.
     *
     * @param  string $skipper
     * @return mixed $reset_data
     */
    private function generateLeaveData($leave_request_id, $data) {

        $start_date = Carbon::parse($data['start_date']);
        $end_date = Carbon::parse($data['end_date']);
        $difference = ($end_date->diffInDays($start_date) + 1);

        $WorkWeek = WorkWeeks::first()->toArray();
        $WorkShift = WorkShifts::findOrFail($data['work_shift_id']);

        $work_shift_criteria = array(
            'full_hours' => $WorkShift->working_hours_per_day,
            'half_hours' => $WorkShift->working_hours_per_day / 2,
        );

        // Create Leaves from Date
        $Leaves = array();
        // Ahsan Edited start here


        // Ahsan Edited end here

        if($difference == 1) {

            $day = 0;
            $Leaves[$day] = $this->adjustHours($leave_request_id, $data, 'single', $day, $WorkWeek, $work_shift_criteria);

        } else {
            for($day = 0; $day < $difference; $day++) {
                if($data['partial_days'] == 'none') {
                    $data['all_days_duration'] = 'full_day';
                    $Leaves[$day] = $this->adjustHours($leave_request_id, $data, 'all_days', $day, $WorkWeek, $work_shift_criteria);
                } else if($data['partial_days'] == 'all') {
                    $Leaves[$day] = $this->adjustHours($leave_request_id, $data, 'all_days', $day, $WorkWeek, $work_shift_criteria);
                } else {
                    if($data['partial_days'] == 'start') {
                        if ($day == 0) {
                            $Leaves[$day] = $this->adjustHours($leave_request_id, $data, 'starting', $day, $WorkWeek, $work_shift_criteria);
                            continue;
                        }
                    }
                    if($data['partial_days'] == 'end') {
                        if(($day + 1) == $difference) {
                            $Leaves[$day] = $this->adjustHours($leave_request_id, $data, 'ending', $day, $WorkWeek, $work_shift_criteria);
                            continue;
                        }
                    }
                    if($data['partial_days'] == 'start_end') {
                        if ($day == 0) {
                            $Leaves[$day] = $this->adjustHours($leave_request_id, $data, 'starting', $day, $WorkWeek, $work_shift_criteria);
                            continue;
                        }

                        if(($day + 1) == $difference) {
                            $Leaves[$day] = $this->adjustHours($leave_request_id, $data, 'ending', $day, $WorkWeek, $work_shift_criteria);
                            continue;
                        }
                    }

                    $data['all_days_duration'] = 'full_day';
                    $Leaves[$day] = $this->adjustHours($leave_request_id, $data, 'all_days', $day, $WorkWeek, $work_shift_criteria);
                }
            }
        }

        return $Leaves;
    }

    /**
     * Show the form for editing Leave.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //  if (! Gate::allows('leave_requests_edit')) {
      //      return abort(401);
      //  }
     
        $LeaveRequest = LeaveRequests::findOrFail(1);
        
        $WorkShifts = WorkShifts::pluckActiveOnly();
        $WorkShifts->prepend('Select a Work Shift', '');
     
        $LeaveTypes = LeaveTypes::pluckActiveOnly();
        $LeaveTypes->prepend('Select a Leave Type', '');

        $Employees = Staff::pluckActiveWithID();
        $Employees->prepend('Select an Employee ', '');
       
        $WorkShift = WorkShifts::getFirstWorkShift();

        return view('hrm.leave_requests.edit', compact('LeaveRequest', 'LeaveTypes', 'Employees', 'WorkShift', 'WorkShifts'));
    }

    /**
     * Update Leave in storage.
     *
     * @param  \App\Http\Requests\HRM\LeaveRequests\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
      //  if (! Gate::allows('leave_requests_edit')) {
      //      return abort(401);
      //  }

        $Leave = LeaveRequests::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $data = $this->generateData($data);

        // Insert Leave Type and Work Shift data as well
        $data['work_shift_data'] = WorkShifts::findOrFail($data['work_shift_id'])->toJson();
        $data['leave_type_data'] = LeaveTypes::findOrFail($data['leave_type_id'])->toJson();

        // Transaction has been started
        DB::beginTransaction();

        $Leave->update($data);


        $leaves_data = $this->generateLeaveData($id, $data);

        if(!sizeof($leaves_data)) {
            // Transaction Rolled Back
            DB::rollBack();

            flash('Error in data integritiy, please check again later.')->error()->important();
            $request->flash();
            return redirect()->back()
                ->withInput();
        }

        $Leave->leaves()->forceDelete();

        Leaves::insert($leaves_data);

        // Transaction Successfull
        DB::commit();

        // Transaction has been ended

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.leave_requests.index');
    }


    /**
     * Remove Leave from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    //    if (! Gate::allows('leave_requests_destroy')) {
    //        return abort(401);
    //    }
        $Leave = LeaveRequests::findOrFail($id);
        $Leave->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.leave_requests.index');
    }

    /**
     * Activate Leave from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
    //    if (! Gate::allows('leave_requests_active')) {
    //        return abort(401);
    //    }

        $Leave = LeaveRequests::findOrFail($id);
        $Leave->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.leave_requests.index');
    }

    /**
     * Inactivate Leave from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
    //    if (! Gate::allows('leave_requests_inactive')) {
    //        return abort(401);
    //    }

        $Leave = LeaveRequests::findOrFail($id);
        $Leave->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.leave_requests.index');
    }

    public function actived($leave_id,$status_id)
    {

    //    if (! Gate::allows('leave_requests_edit')) {
    //        return abort(401);
    //    }

        $Leave = LeaveRequests::findOrFail($leave_id);
        $Leave->update(['leave_status' => $status_id]);

        flash('Record has been upated successfully.')->success()->important();

        return redirect()->route('hrm.leave_requests.index');
    }

    /**
     * Delete all selected Leave at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
    //    if (! Gate::allows('leave_requests_mass_destroy')) {
    //        return abort(401);
    //    }
        if ($request->input('ids')) {
            $entries = LeaveRequests::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    /**
     * Employee Leave Balance
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function leave_balance(Request $request)
    {
       // $Employee = Employees::findOrFail($request->get('employee_id'));
        if( ! $request->get('employee_id')  == null ){
            $employee_id = $request->get('employee_id');
        }else{
            $employee_id = Auth::user()->id;;
        }
       
        $Employee = Staff::where('user_id','=',$employee_id)->first();
   
        $LeaveType = LeaveTypes::findOrFail($request->get('leave_type_id'));
//dd($LeaveType);
        $current_date = Carbon::now()->format('Y-m-d');

        $LeaveEntitlement = LeaveEntitlements::where(array(
            'employee_id' => $employee_id,
            'leave_type_id' => $request->get('leave_type_id'),
        ))
            ->where('start_date','<=', $current_date)
            ->where('end_date','>=', $current_date)
            ->select('id', 'no_of_days', 'days_used')
            ->first();

        $leaves = Leaves::where(['leave_type_id' => $request->get('leave_type_id') , 'employee_id' => $employee_id])->get();
        $taken_count = 0 ;
        $scheduled = 0 ;
        $pending_approval = 0 ;

        foreach ($leaves as $key => $val){
            if($val->leave_status_id == 3){
                $pending_approval += $this->leaveValue($val->shift);
            }elseif($val->leave_status_id == 4){
                $scheduled += $this->leaveValue($val->shift);
            }
            elseif($val->leave_status_id == 5){
                $taken_count += $this->leaveValue($val->shift);
            }

        }
//        dd($leaves);

//        $Leaverequest = LeaveRequests::where(array(
//            'employee_id' => $request->get('employee_id'),
//            'leave_type_id' => $request->get('leave_type_id'),
//        ))
//            ->where('start_date','<=', $current_date)
//            ->where('end_date','>=', $current_date)
//            ->select('id', 'total_days', 'days_used')
//            ->first();

        if(!$LeaveEntitlement) {
            $LeaveEntitlement = new \stdClass();
            $LeaveEntitlement->id = null;
            $LeaveEntitlement->no_of_days = $LeaveType->permitted_days;
            $LeaveEntitlement->days_used = 0;
            $LeaveEntitlement->pending_approval = $pending_approval;
            $LeaveEntitlement->scheduled = $scheduled;
            $LeaveEntitlement->taken_count = $taken_count;

        }
       
        return view('hrm.leave_requests.leave_balance', compact('Employee', 'LeaveType', 'LeaveEntitlement'));
    }
     function leaveValue($value) {
        if($value == 'full'){
            return 1;

        }elseif ($value == 'half'){
            return 0.5;

        }elseif ($value == 'specific'){
            return 0.25;
        }
    }
    /**
     * Employees Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function employee_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
            $Employees = Employees::where(['status' => 1])
                ->where(function ($query) {
                    global $request;
                    $query->where('first_name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('middle_name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('last_name', 'LIKE', "%{$request['item']}%");
                })->OrderBy('first_name','asc')->get();

            $result = array();
            if($Employees->count()) {
                foreach ($Employees as $Employee) {
                    $result[] = array(
                        'text' => $Employee->first_name . ' ' . $Employee->last_name,
                        'id' => $Employee->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

}
