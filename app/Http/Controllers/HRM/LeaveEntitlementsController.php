<?php

namespace App\Http\Controllers\HRM;

use App\Models\HRM\Employees;
use App\Models\HRM\LeavePeriods;
use App\Models\HRM\LeaveTypes;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\LeaveEntitlements\StoreRequest;
use App\Http\Requests\HRM\LeaveEntitlements\UpdateRequest;
use App\Models\HRM\LeaveEntitlements;
use Auth;

class LeaveEntitlementsController extends Controller
{
    /**
     * Display a listing of LeaveEntitlement.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('leave_entitlements_manage')) {
            return abort(401);
        }

        $Employees = Employees::select('id', 'first_name', 'father_name')->get()->getDictionary();
        $LeaveTypes = LeaveTypes::select('id', 'name')->get()->getDictionary();

        $LeaveEntitlements = LeaveEntitlements::OrderBy('created_at','desc')->get();

        return view('hrm.leave_entitlements.index', compact('LeaveEntitlements', 'Employees', 'LeaveTypes'));
    }

    /**
     * Show the form for creating new LeaveEntitlement.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('leave_entitlements_create')) {
            return abort(401);
        }

        $LeavePeriod = LeavePeriods::findOrFail(1);
        $entitlement_dates = array(
            'current_year_start' => \Carbon\Carbon::createFromDate(date('Y'), $LeavePeriod->start_month, $LeavePeriod->start_day),
            'current_year_end' => \Carbon\Carbon::createFromDate(date('Y'), $LeavePeriod->start_month, $LeavePeriod->start_day)->addYear(1)->subDay(1),
            'next_year_start' => \Carbon\Carbon::createFromDate(date('Y'), $LeavePeriod->start_month, $LeavePeriod->start_day)->addYear(1),
            'next_year_end' => \Carbon\Carbon::createFromDate(date('Y'), $LeavePeriod->start_month, $LeavePeriod->start_day)->addYear(2)->subDay(1),
        );

        $EmployeeCount = Employees::getTotalEmployees();
        $LeaveTypes = LeaveTypes::pluckActiveOnly();
        $LeaveTypes->prepend('Select a Leave Type', '');

        return view('hrm.leave_entitlements.create', compact('EmployeeCount', 'LeaveTypes', 'entitlement_dates'));
    }

    /**
     * Store a newly created LeaveEntitlement in storage.
     *
     * @param  \App\Http\Requests\HRM\LeaveEntitlements\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        if (! Gate::allows('leave_entitlements_create')) {
            return abort(401);
        }

        $created_by = $updated_by = Auth::user()->id;
        $credited_date = Carbon::now()->format('Y-m-d');

        if(!$request->get('condition') && $request->get('employee_id')) {
            // Single Employee Entitlement Create/Update Process
            $LeaveEntitlement = LeaveEntitlements::firstOrNew(array(
               'employee_id' => $request->get('employee_id'),
               'leave_type_id' => $request->get('leave_type_id'),
               'start_date' => $request->get('start_date'),
               'end_date' => $request->get('end_date'),
            ));

            $LeaveEntitlement->created_by = $created_by;
            $LeaveEntitlement->updated_by = $updated_by;
            $LeaveEntitlement->credited_date = $credited_date;
            $LeaveEntitlement->no_of_days = $LeaveEntitlement->no_of_days + $request->get('no_of_days');
            $LeaveEntitlement->save();
        } else {
            // Bulk Employee Entitlement Create/Update Process
            $Employees = Employees::pluckActiveIDsOnly();

            if($Employees) {
                foreach($Employees as $employee_id) {
                    $LeaveEntitlement = LeaveEntitlements::firstOrNew(array(
                        'employee_id' => $employee_id,
                        'leave_type_id' => $request->get('leave_type_id'),
                        'start_date' => $request->get('start_date'),
                        'end_date' => $request->get('end_date'),
                    ));

                    $LeaveEntitlement->created_by = $created_by;
                    $LeaveEntitlement->updated_by = $updated_by;
                    $LeaveEntitlement->credited_date = $credited_date;
                    $LeaveEntitlement->no_of_days = $LeaveEntitlement->no_of_days + $request->get('no_of_days');
                    $LeaveEntitlement->save();
                }
            }
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.leave_entitlements.index');
    }


    /**
     * Show the form for editing LeaveEntitlement.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('leave_entitlements_edit')) {
            return abort(401);
        }
        $LeaveEntitlement = LeaveEntitlements::findOrFail($id);
        $Employee = Employees::findOrFail($LeaveEntitlement->employee_id);
        $LeaveType = LeaveTypes::findOrFail($LeaveEntitlement->leave_type_id);

        return view('hrm.leave_entitlements.edit', compact('LeaveEntitlement', 'Employee', 'LeaveType'));
    }

    /**
     * Update LeaveEntitlement in storage.
     *
     * @param  \App\Http\Requests\HRM\LeaveEntitlements\UpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if (! Gate::allows('leave_entitlements_edit')) {
            return abort(401);
        }

        $LeaveEntitlement = LeaveEntitlements::findOrFail($id);

        try {
            if(
                ($LeaveEntitlement->employee_id != decrypt($request->get('employee_id'))) ||
                ($LeaveEntitlement->leave_type_id != decrypt($request->get('leave_type_id')))
            ) {
                flash('What were you trying to do? Please try with correct data.')->error()->important();
                return redirect()->route('hrm.leave_entitlements.index');
            }
        } catch (DecryptException $e) {
            flash($e->getMessage())->error()->important();
            return redirect()->route('hrm.leave_entitlements.index');
        }

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;
        $data['employee_id'] = decrypt($data['employee_id']);
        $data['leave_type_id'] = decrypt($data['leave_type_id']);

        $LeaveEntitlement->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.leave_entitlements.index');
    }


    /**
     * Remove LeaveEntitlement from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('leave_entitlements_destroy')) {
            return abort(401);
        }
        $LeaveEntitlement = LeaveEntitlements::findOrFail($id);
        $LeaveEntitlement->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.leave_entitlements.index');
    }

    /**
     * Employees Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function employee_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {

            $Employees = Employees::where(['status' => 1])
                ->where(function ($query) {
                    global $request;
                    $query->where('first_name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('father_name', 'LIKE', "%{$request['item']}%");
                })->OrderBy('first_name','asc')->get();

            $result = array();
            if($Employees->count()) {
                foreach ($Employees as $Employee) {
                    $result[] = array(
                        'text' => $Employee->first_name . ' ' . $Employee->father_name,
                        'id' => $Employee->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

}
