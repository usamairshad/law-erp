<?php

namespace App\Http\Controllers\HRM;

use App\Models\Support\Quotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\AssignTarget\StoreUpdateRequest;
use App\Models\HRM\AssignTarget;
use App\Models\HRM\EmployeesTarget;
use App\Models\HRM\JobTitle;
use App\Models\HRM\Employees;
use App\Models\Marketing\SalePipelines;
use App\Helpers\Marketing;
use App\Models\HRM\EmployeeKpis;

use App\Models\Admin\SalesInvoiceDetailModel;

use Auth;
use Config;
use DateTime;

class AssignTargetController extends Controller
{

    public function index()
    {

        if (!Gate::allows('assign_target_manage')) {
            return abort(401);
        }

        $AssignTarget = AssignTarget::OrderBy('created_at', 'desc')->get();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        return view('hrm.assign_target.index', compact('AssignTarget','Employees'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('assign_target_create')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $user_ids = Employees::pluckDownTree($loggedIn);
        //array_push($user_ids, $loggedIn);
        $Employees = Employees::pluckChildsNamesByIds($user_ids);
        //$Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $EmployeesTarget = EmployeesTarget::pluckMyTargets($loggedIn);

        $EmployeesTarget->prepend('Select a Target', '');



        return view('hrm.assign_target.create', compact('Employees','EmployeesTarget'));

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('assign_target_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;


        AssignTarget::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.assign_target.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('assign_target_edit')){
            return abort(401);
        }

        $AssignTarget   = AssignTarget::findOrFail($id);
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $EmployeesTarget = EmployeesTarget::pluckActiveOnly();

        $EmployeesTarget->prepend('Select a Target', '');
        // echo '<pre>'; print_r($checkin_time); exit;
        return view('hrm.assign_target.edit', compact('AssignTarget','EmployeesTarget', 'Employees'));
    }


    public function details($id)
    {
        if (! Gate::allows('assign_target_details')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $Databanks = Databanks::pluckActiveOnly($loggedIn);
        $Databanks->prepend('Select a Databank', '');
        $Contact = Contacts::contactById($id);

        return view('hrm.assign_target.details', compact('Contact','Employees','Databanks'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('assign_target_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $AssignTarget = AssignTarget::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $AssignTarget->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.assign_target.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('assign_target_destroy')) {
            return abort(401);
        }
        $Employee = Contacts::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(StoreUpdateRequest $request)
    {
        if (! Gate::allows('assign_target_datatables')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        if( isset($data['user'] )){
            $Target = AssignTarget::where('user_id', $data['user'])->OrderBy('created_at', 'desc')->get();
        }
        else{
            $user_ids = Employees::pluckDownTree($loggedIn);
            array_push($user_ids,$loggedIn);
            $Target = AssignTarget::getMyTeamTargets($user_ids);
        }

        return  datatables()->of($Target)
            ->addColumn('target_name', function($Target) {
                $text =  '<a href="employee_target/details/'.$Target->target_id.'"  target="_blank"> '.$Target->target_name->name.'</a>';
                return $text;
                return $Target->target_name->name;
            })
            ->addColumn('user_name', function($Target) {
                return $Target->user_name->first_name;
            })
            ->addColumn('target_type', function($Target) {
                return Config::get('hrm.target_type_array.'.$Target->target_name->type);
            })

            ->addColumn('created_by', function($Target) {
                return $Target->creator_name->first_name;
            })

            ->addColumn('action', function ($Target) {

                $action_column = 'No Action';
                if ($Target->created_by == Auth::user()->id){
                    $action_column = '<a href="assign_target/'.$Target->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                    if(Gate::check('assign_target_destroy')){
                        $action_column .= '<form method="post" action="assign_target/delete" >';
                        $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                        $action_column .= '<input type="hidden" value='.$Target->id.' name="first_name" >';
                        $action_column .=  csrf_field();
                        $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                    }
                }
                return $action_column;
            })->rawColumns(['target_name','user_name','branch','action'])
            ->toJson();
        //return view('hrm.assign_target.details', compact('Contact','OrganizationLocations','Employees'));
    }

    public function delete(StoreUpdateRequest $request)
    {

        if (! Gate::allows('assign_target_destroy')) {
            return abort(401);
        }
        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.assign_target.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(StoreUpdateRequest $request)
    {
        if (! Gate::allows('assign_target_active')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.assign_target.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(StoreUpdateRequest $request)
    {
        if (! Gate::allows('assign_target_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.assign_target.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public  function ytd(Request $request){
        if (! Gate::allows('assign_target_details')) {
            return abort(401);
        }
       // dd($request->all());
        $loggedIn = Auth::user()->id;
        $user_ids = Employees::pluckDownTree($loggedIn);
        array_push($user_ids, $loggedIn);
        $Employees = Employees::pluckChildsNamesByIds($user_ids);
        //$Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        return view('hrm.assign_target.ytd', compact('Employees'));

    }

    public  function ytdValue(Request $request){
        if (! Gate::allows('assign_target_details')) {
            return abort(401);
        }
        $data = $request->all();
        $AssignTarget = AssignTarget::where('user_id', $data['user_id'])->get();
        $emp_detail = Employees::where('user_id' , $data['user_id'])->first();

//        dd($AssignTarget);
        $targets = array();
        $saleDetail = array();
        $all_team_user_ids = Marketing::pluckDownTree((int)$data['user_id']);
        //include userid as well
        array_push($all_team_user_ids, $data['user_id']);

        $ytd_array = array();
        $counter=0;
        $total_weight_percent = 0;
        $employee_name = '';

        foreach ($AssignTarget as $key => $value){
            $counter++;
            $EmployeesTarget = EmployeesTarget::findOrFail($value->target_id);
            //if(isset($EmployeesTarget->product_id)  ){
                $tmp_array['emp_name'] = $employee_name = $value->user_name->first_name;
                $tmp_array['target_name'] = $value->target_name->name;
                $target_value = $value->value;
                $tmp_array['value'] = number_format((float)$target_value, 2);
                $tmp_array['weight'] = $value->weight;
                $tmp_array['type'] = Config::get('hrm.target_type_array.'. $EmployeesTarget->type);
                $start_date = "20". $EmployeesTarget->year ."-01-01";

                if($emp_detail->department_id == 4){

                    $salePipeLines = SalePipelines :: YTDProductReport ($all_team_user_ids,array($EmployeesTarget->product_id),$EmployeesTarget->brand,$EmployeesTarget->category,$EmployeesTarget->class_type,$EmployeesTarget->product_type ,$start_date ,'' , array());
                    $net_value = 0;
                    if($EmployeesTarget->type == 1){

                        foreach ($salePipeLines as $key => $val){
                            $net_value += $val->net_income; // net income of sales
                        }

                        $ytd_acheived = $net_value;

                    }else if($EmployeesTarget->type == 2){

                        foreach ($salePipeLines as $key => $val){
                            $net_value += $val->product_quantity; // net income of sales
                        }
                        $ytd_acheived = $net_value;
                    }
                }
//                dd($emp_detail->department_id );
                if($emp_detail->department_id == 3){

                    $quote_ids = Quotes::where(['assigned_to' => $data['user_id'], 'quote_status' => 1])->pluck('id')->toArray();
                    $engrSales = SalesInvoiceDetailModel::engrYTDReport($all_team_user_ids, $quote_ids, $EmployeesTarget->brand, $start_date, '');
                    $net_value = 0;
                    if($EmployeesTarget->type == 1){

                        foreach ( $engrSales as $key => $val ){
                            $net_value += $val->net_income; // net income of sales
                        }

                        $ytd_acheived = $net_value;

                    }else if( $EmployeesTarget->type == 2 ){

                        foreach ( $engrSales as $key => $val ){
                            $net_value += $val->product_quantity; // net income of sales
                        }
                        $ytd_acheived = $net_value;
                    }

                }


                $tmp_array['ytd_acheived'] = number_format((float)$ytd_acheived, 2);

                $date1 = new DateTime( date('Y-m-d',strtotime($start_date)));
                $date2 = new DateTime( date("Y-m-t", strtotime(date('Y-m-d'))));
                $diff = ($date1->diff($date2)->m) + 1;
                $tmp_array['month'] =  $diff; // ytd months

                $ytd_value = (((float)$target_value) / 12) * (int)$diff;
                $tmp_array['ytd_value'] =  number_format((float)$ytd_value, 2);
                $acheived_percent = ((int)$ytd_acheived / (float)$ytd_value) * 100;

                $tmp_array['acheived_percent'] =  number_format((float)$acheived_percent, 2);

                $weight_percent = (float)((int)$value->weight * $acheived_percent) / 100;
            
                if($weight_percent > 100){
                    $weight_percent = 100;
                }

                $tmp_array['weight_percent'] = number_format($weight_percent, 2) ;

                $total_weight_percent += $weight_percent;
                if($emp_detail->department_id == 4){
                    array_push($saleDetail,$salePipeLines);
                }elseif ($emp_detail->department_id == 3){
                    array_push($saleDetail,$engrSales);
                }

                array_push($ytd_array, $tmp_array);
            //}
        }

        // Now check for the kpis
        $employee_kpis = EmployeeKpis::where(['user_id'=>$data['user_id'], 'year' => date("y")])->get();

        foreach ($employee_kpis as $key => $val){


            $tmp_array['emp_name'] = $employee_name ;
            $tmp_array['target_name'] = $val->name;
            $target_value = $val->value;
            $tmp_array['value'] = number_format((float)$target_value, 2);
            $tmp_array['weight'] = $val->weight;
            $tmp_array['type'] = 'KPI';
            $start_date = "20". $EmployeesTarget->year ."-01-01";


            $tmp_array['ytd_acheived'] = number_format((float)$val->acheived, 2);

            $date1 = new DateTime( date('Y-m-d',strtotime($start_date)));
            $date2 = new DateTime( date("Y-m-t", strtotime(date('Y-m-d'))));

            $diff = ($date1->diff($date2)->m) + 1;
            $tmp_array['month'] =  $diff; // ytd months

            $ytd_value = (((float)$val->value) / 12) * (int)$diff;
            $tmp_array['ytd_value'] =  number_format((float)$ytd_value, 2);
            $acheived_percent = ((int)$val->acheived / (float)$ytd_value) * 100;

            $tmp_array['acheived_percent'] =  number_format((float)$acheived_percent, 2);
            $weight_percent = (float)((int)$val->weight * $acheived_percent) / 100;
            if($weight_percent > 100){
                $weight_percent = 100;
            }
            $tmp_array['weight_percent'] = number_format($weight_percent, 2) ;

            $total_weight_percent += $weight_percent;
            array_push($ytd_array, $tmp_array);



        }

        $result['result'] = $saleDetail;
        $result['total_weight_percent'] = number_format($total_weight_percent, 2) ;
        $result['ytd_array'] = $ytd_array;

        return response()->json($result);

    }
}
