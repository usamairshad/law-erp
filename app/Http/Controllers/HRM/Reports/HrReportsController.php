<?php

namespace App\Http\Controllers\HRM\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Support\Complaints;
use App\Models\HRM\Employees;
use App\Models\HRM\EmployeeAttendance;
use Yajra\Datatables\Datatables;
use App\Models\HRM\LeaveTypes;
use App\Models\HRM\LeaveRequests;
use App\Models\HRM\Leaves;
use App\Models\HRM\LeaveStatuses;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;
use Config;
use Auth;
use PDF;
use DB;

class HrReportsController extends Controller
{
    public function index(){
        $loggedIn = Auth::user()->id;
        $Employee = Employees::employeeDetailByUserId($loggedIn);
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        
        return view('hrm.hrm_reports.attendance_reports.index', compact('Employees', 'Employee'));
    }

    public function getSimpleReport(Request $request){
        $data = $request->all();
        $where = array();
        $loggedIn = Auth::user()->id;
        $Employee = Employees::employeeDetailByUserId($loggedIn);
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        if ($data['sdate'] == null && $data['edate'] == null && $data['employee'] == null && $data['status'] == null) {
            $where = array(
                'hrm_employee_attendance.type' => 'I',
            );
        }
        
        if($data['sdate'] && $data['edate']) {
            $start_date = date('Y-m-d', strtotime($data['sdate']));
            $end_date = date('Y-m-d', strtotime($data['edate']));
        }
        elseif($data['sdate']) {
            $start_date = date('Y-m-d', strtotime($data['sdate']));
            $end_date = date('Y-m-d', strtotime("+1 month"));
        } 
        elseif($data['edate']) {
            $start_date = date('Y-m-d', strtotime("-1 month"));
            $end_date = date('Y-m-d', strtotime($data['edate']));
        } 
        else {
            $start_date = date('Y-m-d', strtotime("-1 month"));
            $end_date  = date('Y-m-d');
        }
        // check employee if exists
        if($data['employee']) {
           
            $where['hrm_employee_attendance.user_id'] = $data['employee'];
        }
        // check status if exists
         if($data['status']) {
            $where['hrm_employee_attendance.type'] = $data['status'];
        }


        $In = EmployeeAttendance::where($where)->where('type', 'I')->whereBetween('date_time', [$start_date, $end_date]);
        $attendance1 = $In->count();    

        $Out = EmployeeAttendance::where($where)->where('type', 'O')->whereBetween('date_time', [$start_date, $end_date]);
        $attendance2 = $Out->count();    

        $query = EmployeeAttendance::where($where)->whereBetween('date_time', [$start_date, $end_date]);
        $attendance = $query->get();    
        return view('hrm.hrm_reports.attendance_reports.report', compact('Employees', 'Products', 'Employee', 'attendance1','attendance2', 'data','attendance'));
        
    }
    

    public function detail($id){
      
        $detail = EmployeeAttendance::where('user_id', $id)->get();

        return view('hrm.hrm_reports.attendance_reports.reporttable_details', compact('Employees', 'Products', 'Employee', 'attendance1','attendance2', 'data','detail', 'id'));
        
    }

    public function pdfReport($id){
        $detail = EmployeeAttendance::where('user_id', $id)->get();
        
    	$pdf = PDF::loadView('hrm.hrm_reports.attendance_reports.attandencereport_pdf', ['detail' => $detail]);
    	return $pdf->stream('attendance_report.pdf');
    
    }

    public function leaveindex(){
        $loggedIn = Auth::user()->id;
        $Employee = Employees::employeeDetailByUserId($loggedIn);

        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $LeaveTypes = LeaveTypes::pluckActiveOnly();
        $LeaveTypes->prepend('Select a Leave Type', '');
        
        return view('hrm.hrm_reports.leave_report.index', compact('Employees', 'Employee', 'LeaveTypes'));
    }

    public function getSimpleLeaveReport(Request $request){
        $data = $request->all();

        $where = array();
        $loggedIn = Auth::user()->id;
        $Employee = Employees::employeeDetailByUserId($loggedIn);
        
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        
        $LeaveTypes = LeaveTypes::pluckActiveOnly();
        $LeaveTypes->prepend('Select a Leave Type', '');
        
        if ($data['sdate'] == null && $data['edate'] == null && $data['employee'] == null && $data['leave_type_id'] == null) {
            $where = array(
                'hrm_leave_requests.leave_type_id' => 1,
                'hrm_leave_requests.leave_type_id' => 2,
                'hrm_leave_requests.leave_type_id' => 3
            );
        }
        
        if($data['sdate'] && $data['edate']) {
            $start_date = date('Y-m-d', strtotime($data['sdate']));
            $end_date = date('Y-m-d', strtotime($data['edate']));
        }
        elseif($data['sdate']) {
            $start_date = date('Y-m-d', strtotime($data['sdate']));
            $end_date = date('Y-m-d', strtotime("+1 month"));
        } 
        elseif($data['edate']) {
            $start_date = date('Y-m-d', strtotime("-1 month"));
            $end_date = date('Y-m-d', strtotime($data['edate']));
        } 
        else {
            $start_date = date('Y-m-d', strtotime("-1 month"));
            $end_date  = date('Y-m-d');
        }
        // check employee if exists
        if($data['employee']) {
            
            $where['hrm_leave_requests.employee_id'] = $data['employee'];
        }
        // check status if exists
        if($data['leave_type_id']) {
            $where['hrm_leave_requests.leave_type_id'] = $data['leave_type_id'];
        }
        
        
        $sick = LeaveRequests::where($where)->where('leave_type_id', 1)->whereBetween('applied_date', [$start_date, $end_date]);
        $leave1 = $sick->count();    
        
        $vacation = LeaveRequests::where($where)->where('leave_type_id', 2)->whereBetween('applied_date', [$start_date, $end_date]);
        $leave2 = $vacation->count();   

        $casual = LeaveRequests::where($where)->where('leave_type_id', 3)->whereBetween('applied_date', [$start_date, $end_date]);
        $leave3 = $casual->count();    
        
        // $query = LeaveRequests::where($where)->whereBetween('applied_date', [$start_date, $end_date]);
        // $attendance = $query->get();    
       // dd($attendance);
        return view('hrm.hrm_reports.leave_report.report', compact('Employees', 'leave1', 'Employee', 'leave2','leave3', 'data','attendance', 'LeaveTypes'));
        
    }

    public function leavedetail($id){
      
        $detail = LeaveRequests::where('employee_id', $id)->get();

        return view('hrm.hrm_reports.leave_report.reporttable_details', compact('Employees', 'Products', 'Employee', 'attendance1','attendance2', 'data','detail', 'id'));
        
    }

    public function leavePdfReport($id){
        $detail = LeaveRequests::where('employee_id', $id)->get();
        
    	$pdf = PDF::loadView('hrm.hrm_reports.leave_report.leavereport_pdf', ['detail' => $detail]);
    	return $pdf->stream('leave_report.pdf');
    
    }

    public function leavePerformanceIndex(){
        $loggedIn = Auth::user()->id;
        $Employee = Employees::employeeDetailByUserId($loggedIn);

        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $Leave_status = LeaveStatuses::pluckActiveOnly();
        $Leave_status->prepend('Select an Status', '');

        $LeaveTypes = LeaveTypes::pluckActiveOnly();
        $LeaveTypes->prepend('Select a Leave Type', '');
        
        return view('hrm.hrm_reports.leaveperformance_report.index', compact('Employees', 'Employee', 'Leave_status', 'LeaveTypes'));
    }

    public function getLeavePerformanceReport(Request $request){
        $data = $request->all();
        $employee_id = $data['employee'];
        $where = array();
        $loggedIn = Auth::user()->id;
        $Employee = Employees::employeeDetailByUserId($loggedIn);
        
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $Leave_status = LeaveStatuses::pluckActiveOnly();
        $Leave_status->prepend('Select an Status', '');

        $LeaveTypes = LeaveTypes::pluckActiveOnly();
        $LeaveTypes->prepend('Select a Leave Type', '');
        
        $start_date =  date('Y-m-d',strtotime(date('Y-01-01')));
        $end_date = date('Y-m-d', strtotime('12/31'));

        // check employee if exists
        if($data['employee']) {
            
            $where['hrm_leaves.employee_id'] = $data['employee'];
        }
        // check status if exists
        if($data['status']) {
            $where['hrm_leaves.leave_status_id'] = $data['status'];
        }
         // check type if exists
         if($data['leave_type_id']) {
            $where['hrm_leaves.leave_type_id'] = $data['leave_type_id'];
        }
        
        
        
        $total_leaves = LeaveRequests::where('employee_id', $data['employee'])->sum('total_days');

        $sick_rejected = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 1)->where('leave_type_id', 1)->count('leave_status_id');
         $sick_cancelled = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 2)->where('leave_type_id', 1)->count('leave_status_id');
         $sick_pending_approval = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 3)->where('leave_type_id', 1)->count('leave_status_id');
         $sick_schedule = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 4)->where('leave_type_id', 1)->count('leave_status_id');
         $sick_taken = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 5)->where('leave_type_id', 1)->count('leave_status_id');

         $vacation_rejected = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 1)->where('leave_type_id', 2)->count('leave_status_id');
         $vacation_cancelled = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 2)->where('leave_type_id', 2)->count('leave_status_id');
         $vacation_pending_approval = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 3)->where('leave_type_id', 2)->count('leave_status_id');
         $vacation_schedule = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 4)->where('leave_type_id', 2)->count('leave_status_id');
         $vacation_taken = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 5)->where('leave_type_id', 2)->count('leave_status_id');

         $casual_rejected = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 1)->where('leave_type_id', 3)->count('leave_status_id');
         $casual_cancelled = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 2)->where('leave_type_id', 3)->count('leave_status_id');
         $casual_pending_approval = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 3)->where('leave_type_id', 3)->count('leave_status_id');
         $casual_schedule = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 4)->where('leave_type_id', 3)->count('leave_status_id');
         $casual_taken = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 5)->where('leave_type_id', 3)->count('leave_status_id');
          
        
        $query = Leaves::where($where)->whereBetween('leave_date', [$start_date, $end_date]);
        $leave = $query->get();    
       //dd($leave);
        return view('hrm.hrm_reports.leaveperformance_report.report', compact('Employees', 'Employee', 'Leave_status', 'leave', 'total_leaves', 'LeaveTypes', 'sick_rejected', 'sick_cancelled', 'sick_pending_approval', 'sick_schedule', 'sick_taken', 'vacation_rejected', 'vacation_cancelled', 'vacation_pending_approval', 'vacation_schedule', 'vacation_taken', 'casual_rejected', 'casual_cancelled', 'casual_pending_approval', 'casual_schedule', 'casual_taken', 'employee_id'));
        
    }

    public function getLeavePerformanceReportPDF(Request $request){
      //  dd($request);

        $data = $request->all();
        $employee_id = $data['employee'];
        $where = array();
        $loggedIn = Auth::user()->id;
        $Employee = Employees::employeeDetailByUserId($loggedIn);
        
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        $Leave_status = LeaveStatuses::pluckActiveOnly();
        $Leave_status->prepend('Select an Status', '');

        $LeaveTypes = LeaveTypes::pluckActiveOnly();
        $LeaveTypes->prepend('Select a Leave Type', '');
        
        $start_date =  date('Y-m-d',strtotime(date('Y-01-01')));
        $end_date = date('Y-m-d', strtotime('12/31'));

        // check employee if exists
        if($data['employee']) {
            
            $where['hrm_leaves.employee_id'] = $data['employee'];
        }
        // check status if exists
        if($data['status']) {
            $where['hrm_leaves.leave_status_id'] = $data['status'];
        }
         // check type if exists
         if($data['leave_type_id']) {
            $where['hrm_leaves.leave_type_id'] = $data['leave_type_id'];
        }
        
        
        
        $total_leaves = LeaveRequests::where('employee_id', $data['employee'])->sum('total_days');

        $sick_rejected = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 1)->where('leave_type_id', 1)->count('leave_status_id');
         $sick_cancelled = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 2)->where('leave_type_id', 1)->count('leave_status_id');
         $sick_pending_approval = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 3)->where('leave_type_id', 1)->count('leave_status_id');
         $sick_schedule = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 4)->where('leave_type_id', 1)->count('leave_status_id');
         $sick_taken = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 5)->where('leave_type_id', 1)->count('leave_status_id');

         $vacation_rejected = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 1)->where('leave_type_id', 2)->count('leave_status_id');
         $vacation_cancelled = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 2)->where('leave_type_id', 2)->count('leave_status_id');
         $vacation_pending_approval = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 3)->where('leave_type_id', 2)->count('leave_status_id');
         $vacation_schedule = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 4)->where('leave_type_id', 2)->count('leave_status_id');
         $vacation_taken = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 5)->where('leave_type_id', 2)->count('leave_status_id');

         $casual_rejected = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 1)->where('leave_type_id', 3)->count('leave_status_id');
         $casual_cancelled = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 2)->where('leave_type_id', 3)->count('leave_status_id');
         $casual_pending_approval = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 3)->where('leave_type_id', 3)->count('leave_status_id');
         $casual_schedule = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 4)->where('leave_type_id', 3)->count('leave_status_id');
         $casual_taken = Leaves::where('employee_id', $data['employee'])->where('leave_status_id', 5)->where('leave_type_id', 3)->count('leave_status_id');
          
        
        $query = Leaves::where($where)->whereBetween('leave_date', [$start_date, $end_date]);
        $leave = $query->get();    

        $pdf = PDF::loadView('hrm.hrm_reports.leaveperformance_report.leavePerformance_report_pdf', ['Employees'=> $Employees, 'Employee'=> $Employee, 'employee_id'=>$employee_id,'Leave_status'=> $Leave_status, 'leave' => $leave, 'total_leaves' => $total_leaves, 'sick_rejected' => $sick_rejected, 'sick_cancelled' => $sick_cancelled, 'sick_pending_approval' => $sick_pending_approval, 'sick_schedule' => $sick_schedule, 'sick_taken' => $sick_taken, 'vacation_rejected' => $vacation_rejected, 'vacation_cancelled' => $vacation_cancelled, 'vacation_pending_approval' => $vacation_pending_approval, 'vacation_schedule' => $vacation_schedule, 'vacation_taken' => $vacation_taken, 'casual_rejected' => $casual_rejected, 'casual_cancelled'=> $casual_cancelled, 'casual_pending_approval' => $casual_pending_approval, 'casual_schedule' => $casual_schedule, 'casual_taken' => $casual_taken]);
    	return $pdf->stream('leavePerformance_report_pdf');
    }

}