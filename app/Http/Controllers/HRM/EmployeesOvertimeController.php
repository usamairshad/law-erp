<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Requests\HRM\HrmEmployeesOvertime\StoreUpdateRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\HRM\JobTitle;
use App\Models\HRM\Employees;
use App\Models\HRM\EmployeesOvertime;
use Auth;

class EmployeesOvertimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('employee_overtime_manage')) {
            return abort(401);
        }

        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        return view('hrm.employees_overtime.index', compact('Employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('employee_overtime_create')) {
            return abort(401);
        }

        $JobTitles = JobTitle::pluckActiveMrkOnly();
        $JobTitles->prepend('Select a Job Title', '');
        $OvertimeEmployees = Employees::getOvertimeEmployeesOnly();
        $OvertimeEmployees->prepend('Select a User', '');
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');

        return view('hrm.employees_overtime.create', compact('JobTitles', 'Employees', 'OvertimeEmployees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('employee_overtime_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        EmployeesOvertime::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employees_overtime.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employee_overtime_edit')){
            return abort(401);
        }

        $EmployeeOvertime    = EmployeesOvertime::findOrFail($id);
        $JobTitles = JobTitle::pluckActiveMrkOnly();
        $JobTitles->prepend('Select a Job Title', '');
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        $OvertimeEmployees = Employees::getOvertimeEmployeesOnly();
        $OvertimeEmployees->prepend('Select a User', '');
        // echo '<pre>'; print_r($checkin_time); exit;
        return view('hrm.employees_overtime.edit', compact('EmployeeOvertime', 'JobTitles', 'Employees', 'OvertimeEmployees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('employee_overtime_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $EmployeeOvertime = EmployeesOvertime::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $EmployeeOvertime->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employees_overtime.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employee_overtime_destroy')) {
            return abort(401);
        }
        $EmployeeOvertime = EmployeesOvertime::findOrFail($id);
        $EmployeeOvertime->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employees_overtime.index');
    }

    public function datatables(StoreUpdateRequest $request)
    {


        if (! Gate::allows('employee_overtime_datatables')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        if( isset($data['user'] )){
            $Target = EmployeesOvertime::where('user_id', $data['user'])->OrderBy('created_at', 'desc')->get();
        }
        else{
            $Target = EmployeesOvertime::OrderBy('created_at', 'desc')->get();
        }

        return  datatables()->of($Target)
            ->addColumn('id', function($Target) {

                return $Target->id;

            })
            ->addColumn('user_name', function($Target) {

                return Employees::employeeDetailByUserId($Target->user_id)->first_name;

            })
            ->addColumn('allowed_hours', function($Target) {

                return $Target->allowed_hours;

            })
            ->addColumn('for_date', function($Target) {

                return $Target->for_date;

            })
            ->addColumn('allowed_by', function($Target) {

                if($Target->allowed_by)
                    return Employees::getEmployeeNameByUserId($Target->allowed_by);
                return '<p class="small text-danger">Not allowed yet</p>';

            })
            ->addColumn('approved_by', function($Target) {
                if($Target->approved_by)
                    return Employees::getEmployeeNameByUserId($Target->approved_by);
                return '<p class="small text-danger">Not approved yet</p>';

            })
            ->addColumn('work_status', function($Target) {

                return $Target->work_status;

            })
            ->addColumn('created_at', function($Target) {

                return $Target->created_at;

            })
            ->addColumn('action', function ($Target) {
                $action_column = '<a style="margin-bottom: 2px;" href="employees_overtime/'.$Target->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('employee_overtime_destroy')){
                    $action_column .= '<form method="post" action="employees_overtime/'.$Target->id.'/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Target->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['user_id','allowed_by','approved_by','action'])
            ->toJson();
        //return view('hrm.employees_tada_amount.details', compact('Contact','OrganizationLocations','Employees'));
    }


    public function getEmployeeDetail(StoreUpdateRequest $request){
        $data = $request->all();
        $Employee_data = Employees::where('user_id',$data['user_id'])->get();
        $response_array = array();

        if(!empty($Employee_data)>0){
            $response_array['job_title'] = $Employee_data[0]->job_title()->get()->first()->name;
            $response_array['job_title_id'] = $Employee_data[0]->job_title;
        }

        return json_encode($response_array);

   }


}
