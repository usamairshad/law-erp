<?php

namespace App\Http\Controllers\HRM;

use App\Http\Controllers\Controller;
use App\Models\HRM\Training;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin\Branches;
use App\Models\HRM\Trainee;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\User;
use DB;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('erp_training_manage')) {
            return abort(401);
        }
        $trainings = Training::with("branch")->get();

        $roles = Role::all();


        return view('hrm.training.index', compact('trainings', 'roles'));
    }
    public function assign_training($id,$name)
    {
        $trainings = $id;
        $branch_id = $name;
      
        $branches = Branches::all();
        return view('hrm.training.assign_training', compact('trainings','branches','branch_id'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('erp_training_create')) {
            return abort(401);
        }
        $branches = Branches::all();
        return view('hrm.training.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //dd($req);


        $staff_id=Auth::user()->id;
       
        if (!Gate::allows('erp_training_create')) {
            return abort(401);
        }
        
        $input= $req->all();
        
        foreach ($input['branch_id'] as $key => $value) 
        {
            Training::create([
              'branch_id' => $value,
                'venue'=>$input['venue'],
                'department' =>$input['department'],
                'designation'=>$input['designation'],
                'description'=>$input['description'],
                'date'=>$input['date'],
                'time'=>$input['time'],
                'training_type'=>$input['training_type'],
                'training_by'=>$input['training_by'],
                'no_of_days'=>$input['no_of_days'],
                'training_charges'=>$input['training_charges'],
                'created_by'=>$staff_id,
            ]);
        }
       

      
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.training.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('erp_training_edit')) {
            return abort(401);
        }
        $training = Training::find($id);
        $branches = Branches::all();

        return view("hrm.training.edit", compact("training", "branches"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (!Gate::allows('erp_training_edit')) {
            return abort(401);
        }

        $training = Training::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::id();

        $training->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route("admin.training.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('erp_training_destroy')) {
            return abort(401);
        }
        $training = Training::findOrFail($id);
        $training->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.training.index');
    }



    public function getUsersByRole($role_id)
    {

        $users = User::whereHas("roles", function ($q) use ($role_id) {
            $q->where("id", $role_id);
        })->get();

        return $users;
    }

    public function assignTraining(Request $req)
    {

        
        if (empty($req->users) || $req->users == null) {
            flash("Please select a user")->error()->important();
            return back();
        }


        $data = [];

        $training_id = $req->training_id;
        
        
        
        foreach ($req->users as $user)
        {


            
            $insertion=Trainee::create([
                'staff_id'=>$user,
                'training_id'=>$training_id,

            ]);

           // array_push($data, ["staff_id" => $user, "training_id" => $req->training_id]);
        }

        

        
        
        //dd($data);

       // $insertion = Trainee::insert($data);
        
        if (!$insertion) {
            flash("Unable to assign")->error()->important();
        } else {
            flash("Training successfully assign to staff.")->success()->important();
        }

        return redirect()->route('admin.training.index');
    }


    public function trainingAcceptanceForm($id)
    {
        
        
        //$training=Trainee::where("staff_id", Auth::id())->where("id", $id)->update([
        //    'acknowledge'=> 1
        //]);
        // $training = Trainee::with("training.branch")->where("staff_id", Auth::id())->where("training_id", $id)->first();

        // dd($training);
        // $training != null ? $training->update(["acknowledge" => 1]) : null;
       // dd($training);
        // if (!$training) {
        //     return redirect()->back();
        // }
        // else{
        //     $training=Trainee::where("staff_id", Auth::id())->where("id", $id)->where('acknowledge',0)->first();
        // }
        $training=Trainee::where("staff_id", Auth::id())->where("id", $id)->where('acknowledge',0)->first();
        
        if (!$training) {
            return redirect()->back();
        }
        else{
            return view("hrm.training.inviationacceptance", compact("training"));
        }

        
    }

    public function trainingAcceptanceFormPost(Request $req)
    {

        $trainee = Trainee::where("staff_id", $req->staff_id)
            ->where("training_id", $req->training_id)
            ->latest()
            ->first();

           

        $trainee->update(
            ["acknowledge"=> $req->avalibility ,"avalibility" => $req->avalibility, "remarks" => $req->remarks]
        );

        if (!$req->avalibility) {
            flash('training Invitation not accepted yet.')->error()->important();
        } else {
            flash('training Invitation accepted.')->success()->important();
        }

        return redirect()->to(url("admin/training_request"));
    }

    public function getAssignedUsers($training_id)
    {


        
        $trainings = Trainee::with("user", "training")->where("training_id", $training_id)
            ->latest()
            ->get()
            ->unique("staff_id");

            

        return view("hrm.training.training_assign_to_users", compact("trainings"));
    }
    public function getTrainingRequest()
    {

        $trainings = DB::table('trainees')->where('staff_id' , Auth::user()->id)
            ->join('trainings', 'trainees.training_id', '=', 'trainings.id')
        
            ->select('trainees.*', 'trainings.venue','trainings.category','trainings.department','trainings.designation','trainings.branch_id','trainings.description','trainings.date','trainings.time','trainings.training_by','trainings.no_of_days','trainings.training_charges','trainings.training_type')
            ->get();
          //dd($trainings);  
        return view("hrm.training.training_request", compact("trainings"));

    }


    public function acknowledgeTraining()
    {

    }

    

    
}
