<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\EmploymentStatuses\StoreUpdateRequest;
use App\Models\HRM\EmploymentStatuses;
use Auth;

class EmploymentStatusesController extends Controller
{
    /**
     * Display a listing of EmploymentStatus.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('employment_statuses_manage')) {
            return abort(401);
        }

        $EmploymentStatuses = EmploymentStatuses::OrderBy('created_at','desc')->get();

        return view('hrm.employment_statuses.index', compact('EmploymentStatuses'));
    }

    /**
     * Show the form for creating new EmploymentStatus.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('employment_statuses_create')) {
            return abort(401);
        }
        return view('hrm.employment_statuses.create');
    }

    /**
     * Store a newly created EmploymentStatus in storage.
     *
     * @param  \App\Http\Requests\HRM\EmploymentStatuses\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employment_statuses_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        EmploymentStatuses::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employment_statuses.index');
    }


    /**
     * Show the form for editing EmploymentStatus.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employment_statuses_edit')) {
            return abort(401);
        }
        $EmploymentStatus = EmploymentStatuses::findOrFail($id);

        return view('hrm.employment_statuses.edit', compact('EmploymentStatus'));
    }

    /**
     * Update EmploymentStatus in storage.
     *
     * @param  \App\Http\Requests\HRM\EmploymentStatuses\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('employment_statuses_edit')) {
            return abort(401);
        }

        $EmploymentStatus = EmploymentStatuses::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $EmploymentStatus->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employment_statuses.index');
    }


    /**
     * Remove EmploymentStatus from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employment_statuses_destroy')) {
            return abort(401);
        }
        $EmploymentStatus = EmploymentStatuses::findOrFail($id);
        $EmploymentStatus->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employment_statuses.index');
    }

    /**
     * Activate EmploymentStatus from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('employment_statuses_active')) {
            return abort(401);
        }

        $EmploymentStatus = EmploymentStatuses::findOrFail($id);
        $EmploymentStatus->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.employment_statuses.index');
    }

    /**
     * Inactivate EmploymentStatus from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('employment_statuses_inactive')) {
            return abort(401);
        }

        $EmploymentStatus = EmploymentStatuses::findOrFail($id);
        $EmploymentStatus->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.employment_statuses.index');
    }

    /**
     * Delete all selected EmploymentStatus at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('employment_statuses_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = EmploymentStatuses::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
