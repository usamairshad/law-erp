<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\Eobi_Notification_Model;
use App\Models\Academics\Staff;

class EobiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function govt_reg_id(Request $req)
    {
        # code...

        $eobi=Eobi_Notification_Model::orderBy('id','DESC')->first(); 
        
        Staff::where('user_id',$req->user_id)->update(['eobi_govt_id'=>$req->eobi_govt_id, 'eobi_join_date'=>$req->date,'eobi'=>"yes",'eobi_amount'=>$eobi->amount]);
        flash("EOBI has been successfully Applied")->success()->important();
        return redirect()->back();

    }

    public function eobi_applied_staff()
    {
                # code...
        $Staffs=Staff::where('eobi','yes')->get();
        return view('hrm/eobi/eobi_applied_staff',compact('Staffs')); 

    }

    public function index()
    {
        //
        $data=Eobi_Notification_Model::all();
        return view('hrm/eobi/index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('hrm/eobi/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input=$request->all();
        $obj=Eobi_Notification_Model::create($input);
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('eobi_notification.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data=Eobi_Notification_Model::where('id',$id)->first();
        return view('hrm/eobi/edit',compact('data'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $EBOI=Eobi_Notification_Model::findOrFail($id);
        $input=$request->all();
        $EBOI->fill($input)->save();
        flash('Record has been updated successfully.')->success()->important();
        return redirect()->route('eobi_notification.index'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $EOBI=Eobi_Notification_Model::findOrFail($id);
        $EOBI->delete();
        flash('Record has been Deleted successfully.')->success()->important();
        return redirect()->route('eobi_notification.index');
    }
}
