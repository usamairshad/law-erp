<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\LeaveTypes\StoreUpdateRequest;
use App\Models\HRM\LeaveTypes;
use Auth;

class LeaveTypesController extends Controller
{
    /**
     * Display a listing of LeaveType.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

     //   if (! Gate::allows('leave_types_manage')) {
     //       return abort(401);
     //   }

        $LeaveTypes = LeaveTypes::OrderBy('created_at','desc')->get();

        return view('hrm.leave_types.index', compact('LeaveTypes'));
    }

    /**
     * Show the form for creating new LeaveType.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     //   if (! Gate::allows('leave_types_create')) {
     //       return abort(401);
     //   }
        return view('hrm.leave_types.create');
    }

    /**
     * Store a newly created LeaveType in storage.
     *
     * @param  \App\Http\Requests\HRM\LeaveTypes\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
      //  if (! Gate::allows('leave_types_create')) {
      //      return abort(401);
      //  }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        // If Condition set then set values
        if(!$request->get('condition')) {
            $data['condition'] = 0;
            $data['allowed_number'] = null;
            $data['allowed_type'] = 0;
        }

        LeaveTypes::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.leave_types.index');
    }


    /**
     * Show the form for editing LeaveType.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //  if (! Gate::allows('leave_types_edit')) {
      //      return abort(401);
      //  }
        $LeaveType = LeaveTypes::findOrFail($id);

        return view('hrm.leave_types.edit', compact('LeaveType'));
    }

    /**
     * Update LeaveType in storage.
     *
     * @param  \App\Http\Requests\HRM\LeaveTypes\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
     //   if (! Gate::allows('leave_types_edit')) {
     //       return abort(401);
     //   }

        $LeaveType = LeaveTypes::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        // If Condition set then set values
        if(!$request->get('condition')) {
            $data['condition'] = 0;
            $data['allowed_number'] = null;
            $data['allowed_type'] = 0;
        }

        $LeaveType->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.leave_types.index');
    }


    /**
     * Remove LeaveType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     //   if (! Gate::allows('leave_types_destroy')) {
     //       return abort(401);
     //   }
        $LeaveType = LeaveTypes::findOrFail($id);
        $LeaveType->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.leave_types.index');
    }

    /**
     * Activate LeaveType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
    //    if (! Gate::allows('leave_types_active')) {
    //        return abort(401);
   //     }

        $LeaveType = LeaveTypes::findOrFail($id);
        $LeaveType->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.leave_types.index');
    }

    /**
     * Inactivate LeaveType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
    //    if (! Gate::allows('leave_types_inactive')) {
    //        return abort(401);
    //    }

        $LeaveType = LeaveTypes::findOrFail($id);
        $LeaveType->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.leave_types.index');
    }

    /**
     * Delete all selected LeaveType at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
     //   if (! Gate::allows('leave_types_mass_destroy')) {
     //       return abort(401);
     //   }
        if ($request->input('ids')) {
            $entries = LeaveTypes::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
