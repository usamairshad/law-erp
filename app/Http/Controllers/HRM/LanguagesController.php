<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Languages\StoreUpdateRequest;
use App\Models\HRM\Languages;
use Auth;

class LanguagesController extends Controller
{
    /**
     * Display a listing of Language.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('languages_manage')) {
            return abort(401);
        }

        $Languages = Languages::OrderBy('created_at','desc')->get();

        return view('hrm.languages.index', compact('Languages'));
    }

    /**
     * Show the form for creating new Language.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('languages_create')) {
            return abort(401);
        }
        return view('hrm.languages.create');
    }

    /**
     * Store a newly created Language in storage.
     *
     * @param  \App\Http\Requests\HRM\Languages\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('languages_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        Languages::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.languages.index');
    }


    /**
     * Show the form for editing Language.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('languages_edit')) {
            return abort(401);
        }
        $Language = Languages::findOrFail($id);

        return view('hrm.languages.edit', compact('Language'));
    }

    /**
     * Update Language in storage.
     *
     * @param  \App\Http\Requests\HRM\Languages\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('languages_edit')) {
            return abort(401);
        }

        $Language = Languages::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $Language->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.languages.index');
    }


    /**
     * Remove Language from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('languages_destroy')) {
            return abort(401);
        }
        $Language = Languages::findOrFail($id);
        $Language->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.languages.index');
    }

    /**
     * Activate Language from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('languages_active')) {
            return abort(401);
        }

        $Language = Languages::findOrFail($id);
        $Language->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.languages.index');
    }

    /**
     * Inactivate Language from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('languages_inactive')) {
            return abort(401);
        }

        $Language = Languages::findOrFail($id);
        $Language->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.languages.index');
    }

    /**
     * Delete all selected Language at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('languages_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Languages::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
