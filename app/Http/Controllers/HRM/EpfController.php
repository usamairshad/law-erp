<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Academics\Staff;

class EpfController extends Controller
{
    //

	public function index()
	{
		# code...
		$Staffs=Staff::where('epf',"No")->orwhere('eobi',"No")->get();


		return view('hrm/epf/index',compact('Staffs'));

	}

	public function give_epf($reg_no)
	{
		# code...


		$date=date("Y/m/d");

		$staff_data=Staff::where('reg_no',$reg_no)->where('epf','no')->first();

		

		$epf_amount= (int)($staff_data->basic_salary*5)/100;
		$obj=Staff::where('reg_no',$reg_no)->update(['epf'=>"yes",'epf_join_date'=>$date,'epf_amount'=>$epf_amount]);
		flash('EPF Assigned to Corresponding Staff.')->success()->important();
		return redirect()->back();

	}

	public function epf_applied_staff()
	{
		# code...
		$Staffs=Staff::where('epf','yes')->get();
		return view('hrm/epf/epf_applied_staff',compact('Staffs'));	
	}

}
