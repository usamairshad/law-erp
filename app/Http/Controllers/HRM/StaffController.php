<?php

namespace App\Http\Controllers\HRM;

use App\Roles;
use Config;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Ledgers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\HRM\Employees\StoreUpdateRequest;
use App\Models\Academics\TeacherAttendancePayroll;
use App\Models\Academics\ActiveSessionTeacher;
use App\Models\Academics\Course;
use App\Models\Academics\Staff;
use App\Models\Admin\Branches;
use App\Models\Attendance;

use App\Models\Admin\EntryItems;
use App\Models\Admin\Entries;
use Auth;
use App\Helpers\GroupsTree;
use Validator;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Admin\Groups;
use App\Models\Admin\Company;
use App\Models\HRM\Document;
use App\Models\Admin\City;
use App\Models\Academics\BankAccountRequest;
use App\Models\Academics\StaffBranches;
use DB;

use App\User;
use App\Models\Role as AcademicsRole;
use App\RoleUser;
use App\Helpers\Helper;
use PDF;
use App\Models\Payroll\PaySalary;   
use App\Models\HRM\Eobi_Notification_Model;
use App\Models\Admin\TaxSettings; 
use App\Models\HRM\EobiEpfModel;
use App\Models\HRM\Request_Loan_Employee;

class StaffController extends Controller
{
    /**

     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        if (!Gate::allows('erp_staff_manage')) {
            return abort(401);
        }

        $staffs = Staff::with(['user.roles',  'stafftransferbranch'])
            ->OrderBy('created_at', 'desc')
            ->get();


        $branches = Branches::all();
        $roles = Role::all();
        $courses = Course::all();


        return view('hrm.staff.index', compact('staffs', "branches", "roles", "courses"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (!Gate::allows('erp_staff_create')) {
            return abort(401);
        }
        $branchList = Branches::all();



        $staffList = AcademicsRole::whereIn('name', ['teacher', 'Principal', 'sr.coordinator','admission manager','account','    fee manager','quality assurance'])
            ->get()
            ->sortBy('name', SORT_NATURAL | SORT_FLAG_CASE)
            ->pluck('name', 'id');
        $branch_id = Auth::user()->branch_id;

        return view('hrm.staff.create', compact('branch_id', 'staffList', 'branchList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        

        
         
        $tax_percentage=0;
        $taxable_salary = $request->salary-$request->medical;

        $date1 = date('Y-m-d');
        $date2 = "2021-07-01";

        $diff = abs(strtotime($date2) - strtotime($date1));

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));


        if($request->filertype == "Filer")
        {

            if($years==0)
            {


                $taxable_salary=$taxable_salary * $months;
             
                $tax_slabs=TaxSettings::where('year',2021)->where('law_id',1)->get();

                foreach ($tax_slabs as  $tax_slab) 
                {
                    # code...
                    if ($tax_slab->start_range <= $taxable_salary and $taxable_salary < $tax_slab->end_range) {
                        # code...
                        $tax_percentage= $tax_slab->tax_percent;
                    }
                 
                }            
            }
            else
            {
                $taxable_salary=$taxable_salary * 12;
                $tax_slabs=TaxSettings::where('year',2021)->get();

                foreach ($tax_slabs as $key => $tax_slab) 
                {
                    # code...
                   if ($tax_slab->start_range <= $taxable_salary and $taxable_salary < $tax_slab->end_range) {
                        # code...
                        $tax_percentage= $tax_slab->tax_percent;
                    }
                } 
            }

        }

        



        
        

        $salary_per_day =  $request->salary / 30;

        $salary_per_hour = $salary_per_day / $request->working_hours;
        $salary_per_hour_round = round($salary_per_hour);
        if (!Gate::allows('erp_staff_create')) {
            return abort(401);
        }

        $validated1=$this->validate($request, ['staff_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);


        $validated2=$this->validate($request, ['email' => 'required|email|unique:staff',
                                                'cnic'=>'required|unique:staff',
                                                'email' => 'required|email|unique:erp_users',]);
        
        






        



        $user_data = $request->all();
        if ($request->hasFile('staff_image')) {
            $image = $request->file('staff_image');
            $imageName = time() . '.' . $image->extension();
            $image->move(public_path('/images'), $imageName);
        }




        

        if($request->role_type == 'admission manager' || $request->role_type == 'library' || $request->role_type == 'sr.coordinator' || $request->role_type == 'teacher' || $request->role_type == 'Principal' || $request->role_type == 'fee manager' || $request->role_type == 'quality assurance'  )
        {
            $role=Role::where('name',$request->role_type)->value('id');

            $user_data['created_by'] = Auth::user()->id;
            $user_data['updated_by'] = Auth::user()->id;
            $user_data['password'] = bcrypt('12345678');
            $user_data['staff_image'] = '/images/'.$imageName;
            $user_data['staff_type'] = $request->role_type;
            $user_data['branch_id'] = $user_data['branch_id'];
            $user_data['name'] = $user_data['first_name'];
            $user_data['email'] = $user_data['email'];
            $user_data['password'] = $user_data['password'];
         //   $user_data['country'] = $user_data['country'];
            $user_data['city'] = $user_data['city'];
            $user_data['job_status'] = $user_data['job_status'];
            $user_data['salary'] = $user_data['salary'];
            $user_data['basic_salary']=$user_data['basic_salary'];
            $user_data['salary_per_hour'] = $salary_per_hour_round;
            $user_data['active'] = 1;

            $user_data['role_id'] =$role;
            $user_data['user_type'] = 'both';

            $user_data['start_day_contract'] = $user_data['start_day_contract'];
            $user_data['end_day_contract'] = $user_data['end_day_contract'];
            $user_data['probation'] = $user_data['probation'];
            $user_data['medical_allowances'] = $user_data['medical'];
            $user_data['conveyances'] = $user_data['conveyance'];
            $user_data['house_rent'] = $user_data['HouseRent'];
           //$user_data['provident_fund'] = $user_data['providentFund'];
           // $user_data['provident_amount'] = $user_data['pf_per_month'];
           //$user_data['eobi_amount'] = $user_data['eobi_per_month'];
           // $user_data['EOBI'] = $user_data['eobi'];
            $user_data['filer_type'] = $user_data['filertype'];
            $user_data['ntn'] = $user_data['ntn'];
            $user_data['reg_no'] = rand(2,10000);


             


            
          


            //dd($user_data);

            $user2 = User::create($user_data);
            $user_data['user_id'] = $user2->id;
            

            $user2->assignRole($request->role_type);


            RoleUser::create(['user_id'=>$user2->id, 'role_id'=>$role]);
        }
        elseif ($request->role_type == 'DGM Finance'|| $request->role_type == 'Accountant' || $request->role_type == 'HR_head' || $request->role_type == 'HR' || $request->role_type == 'DFIT' || $request->role_type == 'DFIT_head' || $request->role_type == 'Supporting Staff' || $request->role_type == 'GM Finance' || $request->role_type == 'Tax Manager' || $request->role_type == 'Deputy Manager' || $request->role_type == 'Assistant Accounts' || $request->role_type == 'Candidate') {
            $role=Role::where('name',$request->role_type)->value('id');
            $user_data['created_by'] = Auth::user()->id;
            $user_data['updated_by'] = Auth::user()->id;
            $user_data['password'] = bcrypt('12345678');
            $user_data['staff_image'] = '/images/'.$imageName;
            $user_data['staff_type'] = $request->role_type;
            $user_data['branch_id'] = $user_data['branch_id'];
            $user_data['name'] = $user_data['first_name'];
            $user_data['email'] = $user_data['email'];
            $user_data['password'] = $user_data['password'];
          //  $user_data['country'] = $user_data['country'];
            $user_data['city'] = $user_data['city'];
            $user_data['job_status'] = $user_data['job_status'];
            $user_data['salary'] = $user_data['salary'];
            $user_data['basic_salary']=$user_data['basic_salary'];
            $user_data['salary_per_hour'] = $salary_per_hour_round;
            $user_data['active'] = 1;
            $user_data['role_id'] =$role;
            $user_data['user_type'] = 'both';
            $user_data['start_day_contract'] = $user_data['start_day_contract'];
            $user_data['end_day_contract'] = $user_data['end_day_contract'];
            $user_data['probation'] = $user_data['probation'];
            $user_data['medical_allowances'] = $user_data['medical'];
            $user_data['conveyances'] = $user_data['conveyance'];
            $user_data['house_rent'] = $user_data['HouseRent'];
            $user_data['filer_type'] = $user_data['filertype'];
            $user_data['ntn'] = $user_data['ntn'];






            $user2 = User::create($user_data);
            $user_data['user_id'] = $user2->id;
            $user2->assignRole($request->role_type);
        }

        $branch_code= Helper::branchIdToCode($user_data['branch_id']);

        //$data = User::where('id',$user2->id)->value('id');

        $reg_no = "E-".$branch_code;
        $temp_reg = str_pad($user2->id,5,"0",STR_PAD_LEFT);
        $reg_no = $reg_no.$temp_reg;
        $user_data['reg_no'] = $reg_no;



if($request['branches']!= null)
{
        if(count($request['branches']) > 0)
        {
            //dd(count($request['branches']));

            foreach ($request['branches'] as $key => $branch) 
            {
                # code...
                    $emp_branches['staff_id']= $user_data['reg_no'];
                    $emp_branches['branch_id']= $branch;      
                    StaffBranches::create($emp_branches);        
            }

        }
        


    }
        

        if($user_data['bank']=="yes")
        {
            
            $user_data['bank_account']=$user_data['bank'];
            $user_data['account_title']=$user_data['account_title'];
            $user_data['account_number']=$user_data['account_number'];
            $user_data['bank_branch']=$user_data['bank_branch'];

        }
        else
        {

            $user_data['bank_account']=$user_data['bank'];


            //Data for Request

            $account_req['staff_id']=$user_data['reg_no'];
            $account_req['first_name']=$user_data['first_name'];
            $account_req['last_name']=$user_data['last_name'];
            $account_req['father_name']=$user_data['father_name'];
            $account_req['cnic']=$user_data['cnic'];
            $account_req['designation']=$user_data['role_type'];
            $account_req['joining_date']=$user_data['start_day_contract'];
            $account_req['branch']=$user_data['branch_id'];
            $account_req['salary']=$user_data['salary'];


            BankAccountRequest::create($account_req);
        }

        $user_data['tax_percentage'] = $tax_percentage;
        $user_data['eobi']="No";
        $user_data['epf']="No";

        $user = Staff::create($user_data);



        




        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.staff.index');
    }


    public function bank_account_requests()
    {
        # code...

        
        $data=BankAccountRequest::all();
        $branches = Branches::all();
        return view('hrm.bank_account_request.index', compact('data','branches'));
    }

    public function print_account_request(Request $request,$id)
    {
        # code...
        
        $data=BankAccountRequest::where('id',$id)->first();
        $pdf = PDF::loadView('hrm.bank_account_request.print_request', compact('data'));
        return $pdf->stream('account_opening_form.pdf');

        

    }
    public function update_staff_account_details_view($id)
    {
        # code...
        $staff_data = Staff::where('reg_no',$id)->first();

        return view('hrm.bank_account_request.update_staff_account_details_view',compact('staff_data'));

        

    }

    public function update_staff_account_details_submit(Request $request)
    {   

        # code...



        Staff::where('reg_no',$request->staff_reg_no)
                ->update(['bank_account' => "yes",'account_number' => $request->account_number,'account_title'=>$request->account_title,'bank_branch'=>$request->bank_branch]);
        flash('Record has been updated successfully.')->success()->important();
        return redirect('bank_account_request-list');   

    }






    public function fileUpload(Request $request)
    {
        $this->validate($request, [
            'input_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        // creating a automatic ledger of Staff
        /*      $branch_list = Branches::join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->where('erp_branches.id','=',69)->get();
            print_r($branch_list);
            foreach($branch_list as $branch)
            {
                $id = 27;
                $groups = Groups::hasChildGroups($id);
                if($groups!=null)
                {
                }
                else
                {
                $groups = Groups::where('erp_groups.parent_id','=','27')->get();
                }

            }  */

        flash('Record has been created successfully.')->success()->important();

        //   'input_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //]);

        if ($request->hasFile('input_img')) {
            $image = $request->file('input_img');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $name);
            $this->save();

            return back()->with('success', 'Image Upload successfully');
        }
        return redirect()->route('admin.staff.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {

        $user_id = Auth::user()->id;
        $users = Staff::where('user_id', Auth::user()->id)->first();
        $image = $users->staff_image;
        $documents = Document::where('member_id', $user_id)->OrderBy('created_at', 'desc')->get();
        $user_role = User::find($users->user_id)->roles->pluck('name');
     
        $role = $user_role[0];
        //$image = Staff::where('id', $id)->value('staff_image');
        return view('hrm.profile.index', compact('users', 'documents','image','user_role','role'));
    }

    public function show($id)
    {
        $users = Staff::where('id', $id)->first();
        $image = Staff::where('id', $id)->value('staff_image');
        $reg_no=$users['reg_no'];
        $branches=StaffBranches::where('staff_id',$reg_no)->get();

        $user_role = User::find($users->user_id)->roles->pluck('name');
        $role = $user_role[0];
        $user_role = User::where('id','=',$users->user_id)->first();
        $role = Roles::where('id','=',$user_role->role_id)->first();
        $role = $role->name;

        return view('hrm.staff.show', compact('users','image','role','branches'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('erp_staff_edit')) {
            return abort(401);
        }
        $Staff = Staff::find($id);
        $branchList = Branches::pluck('name', 'id');
        $cityList = City::all('name', 'id');
        $branch_id = Auth::user()->branch_id;
        return view('hrm.staff.edit', compact('Staff', 'branch_id', 'branchList','cityList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {



        $salary_per_day =  $request->salary / 30;

        $salary_per_hour = $salary_per_day / $request->working_hours;
        $salary_per_hour_round = round($salary_per_hour);


        if (!Gate::allows('erp_staff_edit')) {
            return abort(401);
        }
        $Staff = Staff::findOrFail($id);

        $data = $request->all();
        

        $data['updated_by'] = Auth::user()->id;
        $data['salary_per_hour'] = $salary_per_hour_round;

        $Staff->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.staff.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('erp_staff_destroy')) {
            return abort(401);
        }

        // $staff = Staff::findOrFail($id);
        // if($staff)
        // {
        //     $staff->delete();
        //     $user = User::find($staff->user_id)->delete();
        // }
        // else
        // {
        //     $staff->delete();
        // }


        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.staff.index');
    }

    public function active($id)
    {
        if (!Gate::allows('erp_staff_active')) {
            return abort(401);
        }

        $Employee = Staff::findOrFail($id);
        $Employee->update(['status' => 'Active']);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.staff.index');
    }
    public function inactive($id)
    {
        if (!Gate::allows('erp_staff_inactive')) {
            return abort(401);
        }

        $Employee = Staff::findOrFail($id);
        $Employee->update(['status' => 'InActive']);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.staff.index');
    }
    public function editStaffView($id)
    {
        $employee = Staff::findOrFail($id);
        $roles = Roles::all();
        $branches = Branches::all();
        return view('hrm.staff.edit_staff_profile',compact('employee','roles','branches'));
    }
    public function updateProfile(Request $request, $id)
    {




//        $image = $request->file('staff_image');
//        $imageName = time() . '.' . $image->extension();
//        $image->move(public_path('/images'), $imageName);
        $data = request()->except(['_token']);


        Staff::where('id',$id)->update($data);
        return redirect()->back()->with('success','Profile has been updated');
    }
    public function moveToSos($id){

         
        Staff::where('id',$id)->update(
            ['status'=>'InActive']
        );
        return redirect()->back()->with('success','SOS Moved to InActive');
    }
    public function reMoveToSos($id){
        Staff::where('id',$id)->update(
            ['status'=>'Active']
        );
        return redirect()->back()->with('success','SOS Moved to Active');

    }






    public function salary_sheet_create()
    {
        # code...

        

        $branches=Branches::all();
        return view('hrm/staff/salary_sheet_create',compact('branches'));

    }

    public function find_branch_staff(Request $request)
    {
        # code...


        


        $eobi=Eobi_Notification_Model::orderBy('id','DESC')->first(); 
        $staff=PaySalary::where('branch_id',$request->branch_id)->where('month_id',$request->month_id)->first();
        

        if(isset($staff))
        {
            flash('salary Already Paid.')->success()->important();
            //return redirect()->route('salary_sheet_create'); 
            $branches=Branches::all();
            return view('hrm/staff/salary_sheet_create',compact('branches'));

        }
        else
        {
            $Staffs=Staff::where('branch_id',$request->branch_id)->get();
        }


        

      //  $Staffs=Staff::where('branch_id',$request->branch_id)->get();
        
        $total_salary=0;
        $total_eobi=0;
        $total_epf=0;
        $total_tax=0;

        foreach ($Staffs as $Staff) 
        {
                # code...



//Recently Added
            $total_attendance = Attendance::selectRaw('count(*)')->where('user_id', $Staff['user_id'])->get();
            

            if($Staff->tax_percentage==0)
            {
                $tax_amount=0;
            }
            else
            {
                $tax_amount=(int)($Staff->tax_percentage*$Staff->salary)/100;

            }


            $missed_lectures_deduction=0;
            if($Staff->job_status=="Lecture base")
            {
                //Attendace_deduction
                $data=Staff::where('id',$Staff->id)->first();
                $salary_per_hour = $data->salary_per_hour;
                $year=date('Y');

                



                $missed_lectures=TeacherAttendancePayroll::where('month',$request->month_id)->where('year',$year)->where('staff_id',$Staff->id)->where('branch_id',$request->branch_id)->where('attendance','0')->count();
                $missed_lectures_deduction=$missed_lectures * $salary_per_hour;

                
            }

            $Staff['eobi_amount']=$eobi->amount;
            

            $Staff['missed_lectures_deduction']=$missed_lectures_deduction;


            $deduction=$Staff['epf_amount']+$Staff['eobi_amount']+$tax_amount + $missed_lectures_deduction;


            $net_salary=$Staff['salary']-$deduction;
            $Staff['net_salary']=$net_salary;
            $Staff['tax_amount']=$tax_amount;


            $total_salary=$total_salary+$net_salary;
            $total_eobi= $total_eobi + $Staff['eobi_amount'];
            $total_epf= $total_epf + $Staff['epf_amount'];
            $total_tax= $total_tax + $Staff['tax_amount'];

        }    
        

        $Staff['total_salary']=$total_salary;

        return (compact('total_salary','Staffs','total_epf','total_eobi','total_tax'));


        
        //return $Staffs;
    }
public function get_ledger_id($branch_id,$city_id,$company_id,$parent_id)
{ 
    $fetch = Groups::where(['parent_id'=>$parent_id,'erp_groups.company_id'=>$company_id])->get();
    $group_id = 0;
    foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    }


    $fetch = Groups::where(['parent_id'=>$group_id,'erp_groups.company_id'=>$company_id])->get();

    foreach ($fetch as $key => $value) {
        # code.
        if($value->city_id==$city_id)
        {
            
        $group_id = $value->id;
        $city_id = $value->city_id;
        }
    }
  
    $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'erp_groups.company_id'=>$company_id])->get();
    foreach ($fetch as $key => $value) {
        # code.
        if($value->branch_id==$branch_id)
        {
        $group_id = $value->id;
        }
    }
    
    $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
      foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    }

    $rec_ledger = Ledgers::where(['group_id' => $group_id,
            'branch_id' => $branch_id
        ])->first();
         
        return $rec_ledger;
}
    public function salary_sheet_store(Request $request)
    {
        # code...

    $salary_group_id = 170;
    $provident_group_id = 164;
    $loan_group_id  = 112;
    $tax_group_id  = 155;
    $eobi_group_id  = 163;
    $branch = Branches::where('id',$request->select_branch)->first();
        $salary_ledger_id = StaffController::get_ledger_id($branch->id,$branch->city_id, $branch->company_id,$salary_group_id);
        $provident_ledger_id = StaffController::get_ledger_id($branch->id,$branch->city_id, $branch->company_id, $provident_group_id);
        $loan_ledger_id = StaffController::get_ledger_id($branch->id,$branch->city_id, $branch->company_id, $loan_group_id);
        $tax_ledger_id = StaffController::get_ledger_id($branch->id,$branch->city_id, $branch->company_id, $tax_group_id);
        $eobi_ledger_id = StaffController::get_ledger_id($branch->id,$branch->city_id, $branch->company_id, $eobi_group_id);
        $bank_ledger_id = $request->bank_ledger_id;
      
        $rules=array();
        $total_amount_bank = $request->staff_total_salary;
        $total_salary_expense = $request->staff_total_epf+$request->staff_total_eobi+$request->staff_total_tax+$request->staff_total_salary;
        $rules['entry_type_id'] = 1;
        $rules['dr_total'] =  $total_salary_expense;
        $rules['cr_total'] = $total_salary_expense;
        $rules['narration'] = "Monthly Salary of Branch :".$branch->name;
        $rules['branch_id'] = $branch->id;
        $rules['created_by'] = Auth::user()->id;
        $rules['updated_by'] = Auth::user()->id;
        $rules['employee_id'] = Auth::user()->id;
        $rules['status'] = 1;
        $rules['voucher_date'] = date('Y-m-d');
        $entry = Entries::create($rules);

        $entry->update(array(
            'number' => CoreAccounts::generateNumber($entry->id),
        ));
   
        $entry_itemsss = array();
if($request->staff_total_epf >0)
{
    $entry_itemss = array(
        'status' => 0,
        'entry_type_id' => 1,
        'entry_id' => $entry->id,
        'voucher_date' =>  date('Y-m-d'),
        'ledger_id' => $provident_ledger_id->id,
        'narration' =>"Monthly Provident Fund of Branch :".$branch->name
    );
        $entry_itemss['amount'] = $request->staff_total_epf;
        $entry_itemss['dc'] = 'd';
        $entry_itemsss[] = $entry_itemss;
}
if($request->staff_total_eobi >0)
{
    $entry_itemss = array(
        'status' => 0,
        'entry_type_id' => 1,
        'entry_id' => $entry->id,
        'voucher_date' =>  date('Y-m-d'),
        'ledger_id' => $eobi_ledger_id->id,
        'narration' =>"Monthly EOBI of Branch :".$branch->name
    );
        $entry_itemss['amount'] = $request->staff_total_eobi;
        $entry_itemss['dc'] = 'd';
        $entry_itemsss[] = $entry_itemss;
}
if($request->staff_total_tax >0)
{
    $entry_itemss = array(
        'status' => 0,
        'entry_type_id' => 1,
        'entry_id' => $entry->id,
        'voucher_date' =>  date('Y-m-d'),
        'ledger_id' =>$tax_ledger_id->id,
        'narration' =>"Monthly Salary Tax of Branch :".$branch->name
    );
        $entry_itemss['amount'] = $request->staff_total_eobi;
        $entry_itemss['dc'] = 'd';
        $entry_itemsss[] = $entry_itemss;
}
if($request->staff_total_salary >0)
{
    $entry_itemss = array(
        'status' => 0,
        'entry_type_id' => 1,
        'entry_id' => $entry->id,
        'voucher_date' =>  date('Y-m-d'),
        'ledger_id' => $salary_ledger_id->id,
        'narration' =>"Monthly Salary Paid of Branch :".$branch->name
    );
        $entry_itemss['amount'] = $request->staff_total_salary;
        $entry_itemss['dc'] = 'd';
        $entry_itemsss[] = $entry_itemss;
}
$entry_itemss = array(
    'status' => 0,
    'entry_type_id' => 1,
    'entry_id' => $entry->id,
    'voucher_date' =>  date('Y-m-d'),
    'ledger_id' => $request->bank_ledger_id,
    'narration' =>"Monthly Salary Paid of Branch :".$branch->name
);
    $entry_itemss['amount'] =   $total_salary_expense;
    $entry_itemss['dc'] = 'c';
    $entry_itemsss[] = $entry_itemss;
    EntryItems::insert($entry_itemsss);




        $branch_id=$request['select_branch'];
        $branches_data=Branches::where('id',$branch_id)->first();
        $company_id=$branches_data['company_id'];
        $month=$request['select_month'];
        $year=date('Y');
        $date=date("Y-m-d");
        $input=$request->all();
        $sheet_code=rand(100000,1000000);


        for ($i=0; $i < count($input['staff']) ; $i++) 
        { 
            # code...
            
            $check=PaySalary::create([
                'sheet_code'=>$sheet_code,
                'branch_id' => $branch_id,
                'staff_id'=>$request->staff[$i],
                'ledger_id'=>$request->staff[$i],
                'name' =>$request->name[$i],
                'gross_salary'=>$request->salary[$i],
                'basic_salary'=>$request->basic_salary[$i],
                'conveyance_allowance'=>$request->conveyance[$i],
                'house_rent_allowance'=>$request->house_rent[$i],
                'medical_allowance'=>$request->medical_allowances[$i],
                'leaves'=>$request->leaves[$i],
                'leaves_deduction'=>$request->leave_deduction[$i],
                'gross_before_adj'=>$request->gross_before_adj[$i],
                'epf_amount'=>$request->epf_amount[$i],
                'eobi_amount'=>$request->eobi_amount[$i],
                'tax_amount'=>$request->tax_amount[$i],
                'training'=>$request->training[$i],
                'loan_amount'=>$request->loan[$i],
                'net_salary'=>$request->net_salary[$i],
                'date'=>$date,
                'month_id'=>$month,
                'year_id'=>$year,
                'total_epf'=>$request->staff_total_eobi,
                'total_eobi'=>$request->staff_total_epf,
                'total_tax'=>$request->staff_total_tax,
                'total_salary'=>$request->staff_total_salary,

            ]);


            

            if($request->epf_amount[$i] != 0)
            {
                EobiEpfModel::create([

                    'staff_id'=>$request->staff[$i],
                    'branch_id'=>$branch_id,
                    'company_id'=>$company_id,  
                    'amount'=>$request->epf_amount[$i],
                    'status'=>"EPF",
                    'month'=>$month,
                    'year'=>$year
                ]);

            }
            if($request->eobi_amount[$i] != 0)
            {
                EobiEpfModel::create([
                    'staff_id'=>$request->staff[$i],
                    'branch_id'=>$branch_id,
                    'company_id'=>$company_id,
                    'amount'=>$request->eobi_amount[$i],
                    'status'=>"EOBI",
                    'month'=>$month,
                    'year'=>$year
                ]);

            }

            


        }

        flash('Salaries Paid to the Staffs')->success()->important();
        return redirect()->back();
     


    }



    public function request_loan_employee_view()
    {
        # code...
        $staffs=Request_Loan_Employee::all();
        return view('hrm/staff/request_loan_employee_view',compact('staffs'));

    }

    public function loan_request_approve_hr($id)
    {
        # code...
        


        $data=Request_Loan_Employee::where('id',$id)->update(['approved_by_hr'=>1]);
        flash("Loan has been Approved by Human Resource Manager")->success()->important();
        return redirect()->back();

    }
    

    public function loan_request_reject_hr($id)
    {
        # code...
        
        $data=Request_Loan_Employee::where('id',$id)->update(['approved_by_hr'=>2]);
        flash("Loan has been Rejected by Human Resource Manager")->success()->important();
        return redirect()->back();

    }

    public function loan_request_approve_ceo($id)
    {
        # code...
        


        $data=Request_Loan_Employee::where('id',$id)->update(['approved_by_ceo'=>1]);
        flash("Loan has been Approved by CEO")->success()->important();
        return redirect()->back();

    }
    

    public function loan_request_reject_ceo($id)
    {
        # code...
        
        $data=Request_Loan_Employee::where('id',$id)->update(['approved_by_ceo'=>2]);
        flash("Loan has been Rejected by CEO")->success()->important();
        return redirect()->back();

    }


    public function eobi_epf_register_view()
    {
        # code...

        $data=Company::all();
         return view('eobiepfregister/create',compact('data'));

    }

    public function find_company_wise_eobiepf(Request $request)
    {
        # code...
        $company_id = $request['company_id'];
        $branches = Branches::where('company_id',$company_id)->get();
        $eobi_epf_data = EobiEpfModel::where('company_id',$company_id)->get();
        
        return (compact('branches','eobi_epf_data'));
        
    }

    public function find_company_branch_wise_eobiepf(Request $request)
    {
        # code...
        $company_id = $request['company_id'];
        $branch_id =$request['branch_id'];
        $eobi_epf_data = EobiEpfModel::where('company_id',$company_id)->where('branch_id',$branch_id)->get();


        return $eobi_epf_data;


    }


    public function salary_sheet_approval_index()
    {
        # code...
        $data=PaySalary::groupBy('sheet_code')->get();
        return view('hrm/staff/salary_sheet_approval',compact('data'));
    }


    public function salary_sheet_approval_view($sheet_code)
    {
        # code...
        $data=PaySalary::where('sheet_code',$sheet_code)->get();


        return view('hrm/staff/salary_sheet_approval_view',compact('data'));
    }

    public function salary_sheet_approval_accountant($sheet_code)
    {
        # code...
        PaySalary::where('sheet_code',$sheet_code)->update(['salary_status'=>"Accountant"]);

        flash("Salary Sheet Approved by Accountant")->success()->important();
        return redirect()->back();



    }

    public function salary_sheet_approval_dgm($sheet_code)
    {
        # code...
        PaySalary::where('sheet_code',$sheet_code)->update(['salary_status'=>"DGM"]);

        flash("Salary Sheet Approved by DGM")->success()->important();
        return redirect()->back();



    }
    public function salary_sheet_approval_gm($sheet_code)
    {
        # code...
        PaySalary::where('sheet_code',$sheet_code)->update(['salary_status'=>"GM"]);

        flash("Salary Sheet Approved by GM")->success()->important();
        return redirect()->back();



    }

    public function salary_sheet_approval_ceo($sheet_code)
    {
        # code...
        PaySalary::where('sheet_code',$sheet_code)->update(['salary_status'=>"CEO"]);

        flash("Salary Sheet Approved by CEO")->success()->important();
        return redirect()->back();



    }



    public function staff_attendance_report()
    {
        # code...

        $data=Branches::all();
        return view('hrm/staff/staff_attendance_report',compact('data'));
    }

    public function find_branch_staff_attendance_report(Request $request)
    {
        # code...
        $branch_id=$request->branch_id;
        $month=$request->month_id;
        $year=$request->year_id;



        


        $attendance_report = TeacherAttendancePayroll::select('staff_id','month','year','branch_id', DB::raw('count(*) as total'))
                 ->where('attendance','0')
                 ->where('month',$month)
                 ->where('year',$year)
                ->where('branch_id',$branch_id)
                 ->groupBy('staff_id')
                 ->get();

        foreach ($attendance_report as $key => $data) {
            
                     # code...
                    

                    
                    
                   $attendance_report[$key]['staff_name']=Helper::getStaffIDToFirstName($data['staff_id']);
                 }         


                 

        return (compact('attendance_report'));




    }






}