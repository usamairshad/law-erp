<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Educations\StoreUpdateRequest;
use App\Models\HRM\Educations;
use Auth;

class EducationsController extends Controller
{
    /**
     * Display a listing of Education.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('educations_manage')) {
            return abort(401);
        }

        $Educations = Educations::OrderBy('created_at','desc')->get();

        return view('hrm.educations.index', compact('Educations'));
    }

    /**
     * Show the form for creating new Education.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('educations_create')) {
            return abort(401);
        }
        return view('hrm.educations.create');
    }

    /**
     * Store a newly created Education in storage.
     *
     * @param  \App\Http\Requests\HRM\Educations\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('educations_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        Educations::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.educations.index');
    }


    /**
     * Show the form for editing Education.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('educations_edit')) {
            return abort(401);
        }
        $Education = Educations::findOrFail($id);

        return view('hrm.educations.edit', compact('Education'));
    }

    /**
     * Update Education in storage.
     *
     * @param  \App\Http\Requests\HRM\Educations\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('educations_edit')) {
            return abort(401);
        }

        $Education = Educations::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $Education->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.educations.index');
    }


    /**
     * Remove Education from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('educations_destroy')) {
            return abort(401);
        }
        $Education = Educations::findOrFail($id);
        $Education->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.educations.index');
    }

    /**
     * Activate Education from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('educations_active')) {
            return abort(401);
        }

        $Education = Educations::findOrFail($id);
        $Education->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.educations.index');
    }

    /**
     * Inactivate Education from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('educations_inactive')) {
            return abort(401);
        }

        $Education = Educations::findOrFail($id);
        $Education->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.educations.index');
    }

    /**
     * Delete all selected Education at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('educations_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Educations::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
