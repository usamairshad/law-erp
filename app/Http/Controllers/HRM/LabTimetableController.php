<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\HRM\TimeTable;
use App\Models\HRM\LabTimeTable;
use App\Models\HRM\Lab; 
use Auth;

class LabTimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_class_time_table_manage')) {
            return abort(401);
        }

        $lab_timetables = LabTimeTable::OrderBy('created_at','desc')->get();
        return view('hrm.lab_timetables.index', compact('lab_timetables')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_class_time_table_create')) {
            return abort(401);
        }

        $time_tables = TimeTable::pluck('title', 'id');
        $labs= Lab::pluck('name', 'id');
        
        return view('hrm.lab_timetables.create', compact('time_tables','labs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if (! Gate::allows('erp_class_time_table_create')) {
            return abort(401);
        }

        $this->validate($request, [
            'time_table_id' => 'required',
            'lab_id' => 'required',
            'date' => 'required',
        ]);
        LabTimeTable::create([
            'time_table_id'      => $request['time_table_id'],
            'lab_id'           => $request['lab_id'],
            'created_by '        => Auth::user()->id,
            'date'              =>$request['date'],
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.lab_timetable.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_class_time_table_destroy')) {
            return abort(401);
        }

        $entries = LabTimeTable::where('id', $id)->delete();
        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.lab_timetable.index');
    }
}
