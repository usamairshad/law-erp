<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\HrmEmployeeTarget\StoreUpdateRequest;
use App\Models\HRM\EmployeesTarget;
use App\Models\HRM\JobTitle;
use App\Models\Admin\SuppliersModel;
use App\Models\HRM\Employees;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\StockCategoryModel;


use Auth;
use Config;

class EmployeeTargetController extends Controller
{

    public function index()
    {

        if (!Gate::allows('employee_target_manage')) {
            return abort(401);
        }

        $EmployeesTarget = EmployeesTarget::OrderBy('created_at', 'desc')->get();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        return view('hrm.employee_target.index', compact('EmployeesTarget','Employees'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('employee_target_create')) {
            return abort(401);
        }

        $Employees = Employees::pluckActiveOnly();

        $Employees->prepend('Select an Employee', '');
        $SuppliersModel = SuppliersModel::pluckActiveOnly();
        $SuppliersModel->prepend('Select a Brand', '');
        $Products = ProductsModel::pluckProducts();
        $Products->prepend('Select a Product', '');

        $StockCategoryModel = StockCategoryModel::pluckActiveOnly();
        $StockCategoryModel->prepend('Select a Category', '');

        return view('hrm.employee_target.create', compact('Employees','Products','SuppliersModel','StockCategoryModel'));

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_target_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        EmployeesTarget::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employee_target.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employee_target_edit')){
            return abort(401);
        }

        $EmployeeTarget   = EmployeesTarget::findOrFail($id);
        $Employees = Employees::pluckActiveOnly();

        $Employees->prepend('Select an Employee', '');
        $SuppliersModel = SuppliersModel::pluckActiveOnly();
        $SuppliersModel->prepend('Select a Brand', '');
        $Products = ProductsModel::pluckProducts();
        $Products->prepend('Select a Product', '');

        $StockCategoryModel = StockCategoryModel::pluckActiveOnly();
        $StockCategoryModel->prepend('Select a Category', '');
        // echo '<pre>'; print_r($checkin_time); exit;
        return view('hrm.employee_target.edit', compact('EmployeeTarget', 'Employees','SuppliersModel','Products','StockCategoryModel'));
    }


    public function details($id)
    {

        if (! Gate::allows('employee_target_details')) {
            return abort(401);
        }

        $EmployeesTarget   = EmployeesTarget::findOrFail($id);
       // dd($EmployeeTarget);
        return view('hrm.employee_target.details', compact('EmployeesTarget'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('employee_target_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $EmployeesTarget = EmployeesTarget::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $EmployeesTarget->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employee_target.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employee_target_destroy')) {
            return abort(401);
        }
        $Employee = Contacts::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(StoreUpdateRequest $request)
    {


        if (! Gate::allows('employee_target_datatables')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $user_ids = Employees::pluckDownTree($loggedIn);
        array_push($user_ids, $loggedIn);

        $data = $request->all();
        if( isset($data['user'] )){
            $Target = EmployeesTarget::where('user_id', $data['user'])->OrderBy('created_at', 'desc')->get();
        }
        else{

            $Target = EmployeesTarget::whereIn('created_by', $user_ids)->OrderBy('created_at', 'desc')->get();
        }
        // dd($Target);

        return  datatables()->of($Target)
            ->addColumn('type', function($Target) {
                return Config::get('hrm.target_type_array.'.$Target->type);
            })
            ->addColumn('name', function($Target) {
                $text =  '<a href="employee_target/details/'.$Target->id.'"  target="_blank"> '.$Target->name.'</a>';
                return $text;
            })

            ->addColumn('created_by', function($Target) {
                return $Target->target_creator->first_name;
            })
            ->addColumn('action', function ($Target) {
                $action_column = 'No Action';
                if ($Target->created_by == Auth::user()->id){
                    $action_column = '<a href="employee_target/' . $Target->id . '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                    if (Gate::check('employee_target_destroy')) {
                        $action_column .= '<form method="post" action="employee_target/delete" >';
                        $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                        $action_column .= '<input type="hidden" value=' . $Target->id . ' name="first_name" >';
                        $action_column .= csrf_field();
                        $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                    }
            }
                return $action_column;
            })->rawColumns(['name','user_name','branch','action'])
            ->toJson();
        //return view('hrm.employee_target.details', compact('Contact','OrganizationLocations','Employees'));
    }

    public function delete(Request $request)
    {

        if (! Gate::allows('employee_target_destroy')) {
            return abort(401);
        }
        $data = $request->all();
        $Contact = EmployeesTarget::findOrFail($data['first_name']);
        $Contact->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employee_target.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request)
    {
        if (! Gate::allows('employee_target_active')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = EmployeesTarget::findOrFail($data['first_name']);
        $Contact->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.employee_target.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(Request $request)
    {
        if (! Gate::allows('employee_target_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = EmployeesTarget::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.employee_target.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
