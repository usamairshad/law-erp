<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Requests\HRM\Workdays\StoreUpdateRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\HRM\Employees;
use Auth;
use Response;
use Config;
use App\Models\HRM\Workdays;
use App\Models\Admin\Departments;


class WorkdaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // if (! Gate::allows('workday_manage')) {
       //     return abort(401);
       // }
        $WorkDays = WorkDays::all();
        return view('hrm.workdays.index', compact('WorkDays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     //   if (! Gate::allows('workday_create')) {
     //       return abort(401);
     //   }
        $Departments = Departments::pluckActiveOnly()->toArray();
        return view('hrm.workdays.create', compact('Departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
     //   if (! Gate::allows('workday_create')) {
     //       return abort(401);
     //   }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        Workdays::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.workdays.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
   //     if (! Gate::allows('workday_edit')) {
   //         return abort(401);
   //     }

        $Workday = Workdays::findOrFail($id);
        $Departments = Departments::pluckActiveOnly()->toArray();

        return view('hrm.workdays.edit', compact('Workday','Departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
     //   if (! Gate::allows('workday_edit')) {
    //        return abort(401);
     //   }

        $Workday = Workdays::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $Workday->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.workdays.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // if (! Gate::allows('workday_destroy')) {
       //     return abort(401);
      //  }
        $Workday = Workdays::findOrFail($id);
        $Workday->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.workday.index');
    }

    public function datatables(){
     //   if (! Gate::allows('workday_datatables')) {
    //        return abort(401);
    //    }

        $Target = Workdays::all();
        return datatables()->of($Target)
            ->addColumn('id',function($Target){

                return $Target->id;

            })
            ->addColumn('title',function($Target){
                
                return $Target->title;

            })
            ->addColumn('date',function($Target){
                
                return $Target->date;
                
            })
            ->addColumn('workday_length',function($Target){
                
                return Config::get('hrm.working_full_half_array')[$Target->workday_length];
                
            })
            ->addColumn('applicable_to',function($Target){
                
                if(empty($Target->applicable_to))
                    return 'Everyone';
                return Departments::getNameByDepartmentId($Target->applicable_to).' Department';    
                
            })
            ->addColumn('created_at',function($Target){
                
                return $Target->created_at;
                
            })
            ->addColumn('action',function($Target){
                $action_column = '<a href="workdays/'.$Target->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('advance_payroll_destroy')){
                    $action_column .= '<form method="post" action="workdays/'.$Target->id.'/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Target->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                if($Target->status && Gate::check('workday_inactive')){
                    $action_column .= '<form method="post" action="workdays/inactive" >';
                    $action_column .= csrf_field();
                    $action_column .= '<input type="hidden" value=' . $Target->id . ' name="id" >';
                    $action_column .= '<input   value="Inactivate"  class="btn btn-xs btn-warning" type="submit"  onclick="return confirm(\'Are you sure to inactivate this record? \')"> </form>';
                }
                else if ( Gate::check('workday_active'))
                {
                    $action_column .= '<form method="post" action="workdays/active" >';
                    $action_column .= csrf_field();
                    $action_column .= '<input type="hidden" value=' . $Target->id . ' name="id" >';
                    $action_column .= '<input   value="Activate"  class="btn btn-xs btn-primary" type="submit"  onclick="return confirm(\'Are you sure you want to activate this record? \')"> </form>';
                }

                return $action_column;
                
            })->rawColumns(['title','action'])
            ->toJson();
    }

    public function active(Request $request){
     //   if (! Gate::allows('workday_active')) {
     //       return abort(401);
     //   }


        $Workday = Workdays::findOrFail($request->id);
        $Workday->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.workdays.index');
    }

    public function inactive(Request $request){
    //    if (! Gate::allows('workday_inactive')) {
    //        return abort(401);
    //    }

        $Workday = Workdays::findOrFail($request->id);
        $Workday->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.workdays.index');
    }

}
