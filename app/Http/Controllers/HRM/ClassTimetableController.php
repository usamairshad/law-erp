<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\ClassTimeTable;
use App\Models\HRM\TimeTable;
use App\Models\Academics\Staff;
use App\Models\Academics\Course;
use App\Models\Academics\Classes;
use App\Models\Academics\Board;
use App\Models\Academics\Branch;
use App\Models\Academics\Program;
use App\Models\Academics\Section;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\ActiveSessionSection;
use Illuminate\Support\Facades\Gate;
use Auth;

class ClassTimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (! Gate::allows('erp_class_time_table_manage')) {
            return abort(401);
        }

        $class_timetables = ClassTimeTable::OrderBy('created_at','desc')->get();
        return view('hrm.class_timetables.index', compact('class_timetables')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
         if (! Gate::allows('erp_class_time_table_create')) {
            return abort(401);
        }

        $branch_data=Branch::pluck('name','id');

        $time_tables = TimeTable::pluck('title', 'id');
        
        $teacher = Staff::where('staff_type','Teacher')->pluck('reg_no', 'id');
        
        return view('hrm.class_timetables.create', compact('time_tables','branch_data','teacher'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      
        
         if (! Gate::allows('erp_class_time_table_create')) {
            return abort(401);
        }
        $input= $request->all();
        
        $branch_id = Branch::where('name' , $request->branch_name)->value('id');


        $active_session_id = ActiveSession::where('board_id',$request->SelectBoard)->where('program_id',$request->programs)->where('class_id',$request->classes)->where('branch_id',$branch_id)->value('id');
       

        $active_session_section = ActiveSessionSection::where('active_session_id',$active_session_id)->value('id');

         

            //dd($value);
           ClassTimeTable::create([
            'time_table_id'      => $request['time_table_id'],
            'active_session_section_id'  => $active_session_section,
            'subject_id'     =>  $request['courses'],
            'day'             => $request['day'],
            'created_by '     => Auth::user()->id,
            ]);
        
        
        // $this->validate($request, [
        //     'time_table_id' => 'required',
        //     'board_id' => 'required',
        //     'program_id' => 'required',
        //     'class_id' => 'required',
        //     'section_id' => 'required',
        //     'subject_id' => 'required',
        //     'day' => 'required',
        // ]);





       // 
        

       
       // if ($active_session_id == null) {
       //      flash("Active Session does not Exist")->error()->important();
       //      return redirect()->back();
       // }
       // else{
       //  
       //  if ($active_session_section == null) {

       //      flash("Active Session Section does not Exist")->error()->important();
       //      return redirect()->back();
       //  }

       //  else
       //  {
       //  ClassTimeTable::create([
       //      'time_table_id'      => $request['time_table_id'],
       //      'active_session_section_id'  => $active_session_section,
       //      'subject_id'     => $request['subject_id'],
       //      'day'             => $request['day'],
       //      'created_by '     => Auth::user()->id,
       //  ]);
       //  }


      // }
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.class_timetable.index');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if (! Gate::allows('erp_class_time_table_destroy')) {
            return abort(401);
        }

        $entries = ClassTimeTable::where('id', $id)->delete();
        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.class_timetable.index');
    }
      public function loadBoardPrograms(Request $req)
    {
      $programs = BoardProgram::where('board_id', $req->id)->get();

      $program_data=[];
        foreach( $programs as $key => $program )
        {
            $data = Program::where('id',$program->program_id)->get();
            array_push($program_data, $data);
        }
      
      return $program_data;
    }

    public function loadBoardProgramClass(Request $request)
    {
        // \Log::info($request);
        $class_id=BoardProgramClass::where('board_id', $request->board_id)->where('program_id',$request->program_id)->get();
        $class_data=[];
        foreach ($class_id as $key => $value) {
             $data=Classes::where('id',$value->class_id)->get();
             array_push($class_data, $data);
        }

        return $class_data;
    } 
    public function loadClassSection(Request $request)
    {   
        $bpc= BoardProgramClass::where('board_id', $request->board_id)->where('program_id',$request->program_id)->where('class_id',$request->class_id)->value('id');
        $section_id=AssignClassSection::where('board_program_class_id', $bpc)->get();
        $section_data=[];
        foreach ($section_id as $key => $value) {
             $data=Section::where('id',$value->section_id)->get();
             array_push($section_data, $data);
        }

        return $section_data;

    }

}
