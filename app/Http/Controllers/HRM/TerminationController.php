<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\Termination;
use App\Models\HRM\Notifications;
use Symfony\Component\Console\Terminal;

use App\User;
use Auth;

class TerminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *


     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function warning_list()
    {
        # code...

        $role=Auth::user()->role->name;
        
        if(Auth::user()->role->name=="HR_head" || Auth::user()->role->name=="admin")
        {
            $data=Notifications::all();
            return view('hrm/staff/warning',compact('data','role'));

            

        }
        else
        {
            $data=Notifications::where('rid',Auth::user()->id)->get();
        return view('hrm/staff/warning',compact('data','role'));
            
        }

        

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function justification(Request $request)
    {
        # code...


        $obj=Notifications::where('id',$request->user_id)->update(['justification'=>$request->reason]);
        return redirect()->back();
    }

    public function store(Request $req)
    {




        $sid=Auth::user()->id;
        $rid=$req->user_id;

        $input['sid']=$sid;
        $input['rid']=$rid;
        $input['title']=$req->title;
        $input['reason']=$req->reason;
        $input['type']="warning";


        $obj = Notifications::create($input);


        // Retrieve user by id, or create it with the name and delayed attributes...
        // $obj = Termination::updateOrCreate(
        //     ["user_id" => $req->user_id],
        //     ["reason" => $req->reason]
        // );


        
        // switch ($obj->warnings) {
        //     case "first":
        //         $obj->warnings = "second";
        //         $obj->acknowledge = "no";
        //         $obj->save();
        //         break;
        //     case "second":
        //         $obj->warnings = "third";
        //         $obj->acknowledge = "no";
        //         $obj->save();
        //         break;

        //     case "third":
        //         $obj->acknowledge = "no";
        //         $obj->save();
        //         break;
        // }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    public function terminatedstaff()
    {

                # code...
        $status=0;
        $data=User::where('active',$status)->get();
        return view('hrm/staff/terminatedstaff',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function acknowledge()
    {
        $obj = Termination::where("user_id", Auth::id())->first();

        if ($obj->acknowledge == "no") {
            $obj->acknowledge = "yes";
            $obj->save();
        }

        return "working";
    }


    public function terminateEmploye($user_id)
    {



        $user = User::findOrFail($user_id);

        
        if ($user->active == 1) {
            $user->active = 0;
            $user->save();
        }



        // \Session::getHandler()->destroy($user->session_id);
        flash("$user->name terminated")->success()->important();
        return redirect()->back();
    }

    public function reActivate($user_id)
    {

        $user = User::with("termination")->where("id", $user_id)->first();
        $user->active = 1;
        if ($user->termination) {
            $termination = $user->termination;
            $termination->warnings = "first";
            $termination->save();
        }
        $user->save();
        flash("$user->name Account activated")->success()->important();
        return redirect()->back();
    }
}
