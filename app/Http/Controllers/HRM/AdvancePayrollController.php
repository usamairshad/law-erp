<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Requests\HRM\AdvancePayroll\StoreUpdateRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\HRM\JobTitle;
use App\Models\HRM\Employees;
use App\Models\HRM\AdvancePayroll;
use Auth;
use Response;
use Config;

class AdvancePayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('advance_payroll_manage')) {
            return abort(401);
        }
        $AdvancePayrolls = AdvancePayroll::all();
        return view('hrm.advance_payroll.index',compact('AdvancePayrolls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('advance_payroll_create')) {
            return abort(401);
        }
        $Employees = Employees :: pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        return view('hrm.advance_payroll.create',compact('Employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('advance_payroll_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        AdvancePayroll::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.advance_payroll.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('advance_payroll_edit')) {
            return abort(401);
        }
        $Employees = Employees :: pluckActiveOnly();
        $AdvancePayroll = AdvancePayroll::findOrFail($id);
        return view('hrm.advance_payroll.edit',compact('AdvancePayroll','Employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('advance_payroll_edit')) {
            return abort(401);
        }

        $AdvancePayroll = AdvancePayroll::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $AdvancePayroll->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.advance_payroll.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('advance_payroll_destroy')) {
            return abort(401);
        }
        $AdvancePayroll = AdvancePayroll::findOrFail($id);
        $AdvancePayroll->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.advance_payroll.index');
    }

    public function datatables(){
        if (! Gate::allows('advance_payroll_datatables')) {
            return abort(401);
        }
        
        $Target = AdvancePayroll::OrderBy('created_at','desc');
        return datatables()->of($Target)
            ->addColumn('id',function($Target){

                return $Target->id;

            })
            ->addColumn('user_name',function($Target){

                return Employees::getEmployeeNameByUserId($Target->user_id);

            })
            ->addColumn('month',function($Target){

                return Config::get('hrm.month_array')[$Target->month];

            })
            ->addColumn('year',function($Target){

                return '20'.$Target->year;

            })
            ->addColumn('salary_type',function($Target){

                return Config::get('hrm.salary_type_array')[$Target->salary_type];

            })
            ->addColumn('comments',function($Target){

                return $Target->comments;

            })
            ->addColumn('created_at',function($Target){

                return $Target->created_at;

            })
            ->addColumn('action',function($Target){
                $action_column = '<a href="advance_payroll/'.$Target->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('advance_payroll_destroy')){
                    $action_column .= '<form method="post" action="advance_payroll/'.$Target->id.'/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Target->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }

                return $action_column;

            })->rawColumns(['user_name','month','year','action'])
            ->toJson();
    }

}
