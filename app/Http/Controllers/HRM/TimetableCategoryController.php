<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\TimetableCategory;
use Illuminate\Support\Facades\Gate;
use Auth;

class TimetableCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (! Gate::allows('erp_class_time_table_manage')) {
            return abort(401);
        }
        $timetable_category = TimetableCategory::OrderBy('created_at','desc')->get();
        return view('hrm.timetable_category.index', compact('timetable_category')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_class_time_table_create')) {
            return abort(401);
        }
        return view('hrm.timetable_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_class_time_table_create')) {
            return abort(401);
        }

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);
        TimetableCategory::create([
            'name'          => $request['name'],
            'description'     => $request['description'],
            'created_by'    => Auth::user()->id,
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.timetable_category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_class_time_table_destroy')) {
            return abort(401);
        }

        $entries = TimetableCategory::where('id', $id)->delete();
        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.timetable_category.index');
            
    }
}
