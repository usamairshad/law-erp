<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\TimeTable;
use Illuminate\Support\Facades\Gate;
use App\Models\HRM\ExamTimeTable;
use App\Models\HRM\Exam;
use Auth;

class ExamTimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exam_timetables = ExamTimeTable::OrderBy('created_at','desc')->get();
        return view('hrm.exam_timetables.index', compact('exam_timetables')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $time_tables = TimeTable::pluck('title', 'id');
        $exams = Exam::pluck('title', 'id');
        
        return view('hrm.exam_timetables.create', compact('time_tables','exams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'time_table_id' => 'required',
            'exam_id' => 'required',
            'date' => 'required',
        ]);

        ExamTimeTable::create([
            'time_table_id'      => $request['time_table_id'],
            'exam_id'           => $request['exam_id'],
            'created_by'           => Auth::user()->id,
            'date'              =>$request['date'],
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.exam_timetable.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_class_time_table_destroy')) {
            return abort(401);
        }

        $entries = ExamTimeTable::where('id', $id)->delete();
        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.exam_timetable.index');
            
    }
}
