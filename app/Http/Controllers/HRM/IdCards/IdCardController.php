<?php

namespace App\Http\Controllers\HRM\IdCards;

use App\Models\HRM\IdCards\IdCard;
use App\Models\Academics\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Academics\Branch;
use App\Roles;

class IdCardController extends Controller
{
    public function idCard(Request $request){

        IdCard::create(['link_id' => $request->link_id, 'link_type' => $request->link_type, 'status' => 'pending',
            'issue_date'=> Carbon::now(), 'expiry_date' =>Carbon::now(),
            'approve_at' => Carbon::now()]);
      return  redirect()->back();
    }
    public function approveCard(){



        $records = IdCard::all();


        $data =[];
        foreach ( $records as $record)
        {
            $created_at = $record->created_at;
           
            $user_name = Staff::where('user_id',$record->link_id)->value('first_name');
            $user_id = Staff::where('user_id',$record->link_id)->value('user_id');
            $branch_id = User::where('id' , $user_id)->value('branch_id');

            $role_id = User::where('id' , $user_id)->value('role_id');

            $role = Roles::where('id' , $role_id)->value('name');

            $branch_name  = Branch::where('id' , $branch_id)->value('name');
            $record['branch_name'] = $branch_name;
            $record['name'] = $user_name;
            $record['role'] = $role;
            $record['created_at'] = $created_at;
            array_push($data,$record);
        }

        return view('hrm.idcards.idcard' , compact('data'));
    }


    public function id_card_requests_gm()
    {
        # code...



        $records = IdCard::where('status','approved')->get();




        $data =[];
        foreach ( $records as $record)
        {
            
            $created_at = $record->created_at;
           
            $user_name = Staff::where('user_id',$record->link_id)->value('first_name');
            $user_id = Staff::where('user_id',$record->link_id)->value('user_id');
            $branch_id = User::where('id' , $user_id)->value('branch_id');

            $role_id = User::where('id' , $user_id)->value('role_id');

            $role = Roles::where('id' , $role_id)->value('name');

            $branch_name  = Branch::where('id' , $branch_id)->value('name');
            $record['branch_name'] = $branch_name;
            $record['name'] = $user_name;
            $record['role'] = $role;
            $record['created_at'] = $created_at;
            array_push($data,$record);
        }


        return view('hrm.idcards.idcard_gm' , compact('data'));

    }






    public function approvRequest($id){

        IdCard::where('id',$id)->update([ 'status' => 'approved',
            'issue_date'=> Carbon::now(), 'expiry_date' =>Carbon::now(),
            'approve_at' => Carbon::now(),'approve_by' => Auth::id()]);
       return redirect()->back();
    }
}
