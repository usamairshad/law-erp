<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\HrmEmployeesTadaAmount\StoreUpdateRequest;
use App\Models\HRM\EmployeesTada;
use App\Models\HRM\EmployeesTadaAmount;
use App\Models\HRM\JobTitle;
use App\Models\HRM\Employees;

use Auth;
use Config;

class EmployeesTadaAmountController extends Controller
{

    public function index()
    {

        if (!Gate::allows('employees_tada_amount_manage')) {
            return abort(401);
        }

        $EmployeesTadaAmount = EmployeesTadaAmount::OrderBy('created_at', 'desc')->get();
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        return view('hrm.employees_tada_amount.index', compact('EmployeesTadaAmount','Employees'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('employees_tada_amount_create')) {
            return abort(401);
        }

        $JobTitles = JobTitle::pluckActiveMrkOnly();
        $JobTitles->prepend('Select a Job Title', '');
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');

        return view('hrm.employees_tada_amount.create', compact('JobTitles', 'Employees'));

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employees_tada_amount_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        $data = $request->all();


        EmployeesTadaAmount::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employees_tada_amount.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employees_tada_amount_edit')){
            return abort(401);
        }

        $EmployeeTadaAmount    = EmployeesTadaAmount::findOrFail($id);
        $JobTitles = JobTitle::pluckActiveMrkOnly();
        $JobTitles->prepend('Select a Job Title', '');
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select a User', '');
        // echo '<pre>'; print_r($checkin_time); exit;
        return view('hrm.employees_tada_amount.edit', compact('EmployeeTadaAmount', 'JobTitles','Employees'));
    }


    public function details($id)
    {
        if (! Gate::allows('employees_tada_amount_details')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');
        $Databanks = Databanks::pluckActiveOnly($loggedIn);
        $Databanks->prepend('Select a Databank', '');
        $Contact = Contacts::contactById($id);

        return view('hrm.employees_tada_amount.details', compact('Contact','Employees','Databanks'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('employees_tada_amount_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $EmployeesTadaAmount = EmployeesTadaAmount::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $EmployeesTadaAmount->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employees_tada_amount.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employees_tada_amount_destroy')) {
            return abort(401);
        }
        $Employee = Contacts::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(StoreUpdateRequest $request)
    {


        if (! Gate::allows('employees_tada_amount_datatables')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $data = $request->all();
        if( isset($data['user'] )){
            $Target = EmployeesTadaAmount::where('user_id', $data['user'])->OrderBy('created_at', 'desc')->get();
        }
        else{
            $Target = EmployeesTadaAmount::OrderBy('created_at', 'desc')->get();
        }

        return  datatables()->of($Target)
            ->addColumn('job_title', function($Target) {

                return $Target->job_name->name;

            })
            ->addColumn('user_id', function($Target) {

                return $Target->employee_name->first_name;

            })
            ->addColumn('allowed_by', function($Target) {

                return $Target->allowed_name->first_name;

            })
            ->addColumn('approved_by', function($Target) {

                return $Target->approve_name->first_name;

            })


            ->addColumn('action', function ($Target) {
                $action_column = '<a href="employees_tada_amount/'.$Target->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if(Gate::check('employees_tada_amount_destroy')){
                    $action_column .= '<form method="post" action="employees_tada_amount/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Target->id.' name="first_name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }
                return $action_column;
            })->rawColumns(['user_id','branch','action'])
            ->toJson();
        //return view('hrm.employees_tada_amount.details', compact('Contact','OrganizationLocations','Employees'));
    }

    public function delete(StoreUpdateRequest $request)
    {

        if (! Gate::allows('employees_tada_amount_destroy')) {
            return abort(401);
        }
        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employees_tada_amount.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employees_tada_amount_active')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.employees_tada_amount.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employees_tada_amount_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.employees_tada_amount.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

   public function getEmployeeDetail(StoreUpdateRequest $request){
        $data = $request->all();
        $Employee_data = Employees::employeeDetailByUserId($data['user_id']);
       $current_year = date("Y");
       $year_val = substr($current_year, -2);
       $EmployeesTada = EmployeesTada::where('job_title_id', $Employee_data->job_title)->where('year',$year_val)->get();
       $response_array = array();

       if(count($EmployeesTada)>0){
           $response_array['job_title'] = $EmployeesTada[0]->job_name->name;
           $response_array['job_title_id'] = $EmployeesTada[0]->job_title_id;
           $response_array['tada_amount'] = $EmployeesTada[0]->tada_amount;
       }

        return json_encode($response_array);

   }
}
