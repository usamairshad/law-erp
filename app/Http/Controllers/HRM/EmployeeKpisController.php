<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\HrmEmployeeKpi\StoreUpdateRequest;
use App\Models\HRM\Employees;
use App\Models\HRM\EmployeeKpis;
use Auth;
use Config;

class EmployeeKpisController extends Controller
{

    public function index()
    {

        if (!Gate::allows('employee_kpis_manage')) {
            return abort(401);
        }

        $EmployeeKpis = EmployeeKpis::OrderBy('created_at', 'desc')->get();

        return view('hrm.employee_kpis.index', compact('EmployeeKpis'));
    }

    /**
     * Show the form for creating new Contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('employee_kpis_create')) {
            return abort(401);
        }

        // pluck subordinate only
        $Employees = Employees::pluckActiveOnly();

        return view('hrm.employee_kpis.create', compact('Employees'));

    }

    /**
     * Store a newly created Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('employee_kpis_create')) {
            return abort(401);
        }

        $data = $request->all();
//        dd($data);
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $data['acheived'] = 0;

        EmployeeKpis::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.employee_kpis.index');
    }


    /**
     * Show the form for editing Contacts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('employee_kpis_edit')){
            return abort(401);
        }

        $EmployeeKpi   = EmployeeKpis::findOrFail($id);
        $Employees = Employees::pluckActiveOnly();

        return view('hrm.employee_kpis.edit', compact('EmployeeKpi', 'Employees'));
    }


    public function details($id)
    {

        if (! Gate::allows('employee_kpis_details')) {
            return abort(401);
        }

        $EmployeeKpis   = EmployeeKpis::findOrFail($id);
       // dd($EmployeeKpi);
        return view('hrm.employee_kpis.details', compact('EmployeeKpis'));
    }
    /**
     * Update Contacts in storage.
     *
     * @param  \App\Http\Requests\Marketing\Contacts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('employee_kpis_edit')) {
            return abort(401);
        }

        //echo '<pre>'; print_r($data); exit;
        $EmployeeKpis = EmployeeKpis::findOrFail($id);
        $data = $request->all();

        $data['updated_by'] = Auth::user()->id;

        $EmployeeKpis->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.employee_kpis.index');
    }


    /**
     * Remove Contacts from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('employee_kpis_destroy')) {
            return abort(401);
        }
        $Employee = Contacts::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();
        echo 'deleted'; exit;
      //  return redirect()->route('hrm.contact.index');
    }


    public function datatables(Request $request)
    {


        if (! Gate::allows('employee_kpis_datatables')) {
            return abort(401);
        }
        $loggedIn = Auth::user()->id;
        $user_ids = Employees::pluckDownTree($loggedIn);
        array_push($user_ids, $loggedIn);

        $data = $request->all();
        if( isset($data['user'] )){
            $Kpis = EmployeeKpis::where('user_id', $data['user'])->OrderBy('created_at', 'desc')->get();
        }
        else{

            $Kpis = EmployeeKpis::whereIn('created_by', $user_ids)->OrderBy('created_at', 'desc')->get();
        }
        $Kpis = EmployeeKpis::OrderBy('created_at', 'desc')->get();
        return  datatables()->of($Kpis)

            ->addColumn('title', function($Kpis) {
                $text =  '<a href="employee_kpis/details/'.$Kpis->id.'"  target="_blank"> '.$Kpis->name.'</a>';
                return $text;
            })

            ->addColumn('created_by', function($Kpis) {
                return $Kpis->creator_name->first_name;
            })
            ->addColumn('user_name', function($Kpis) {
                return $Kpis->user_name->first_name;
            })

            ->addColumn('action', function ($Kpis) {
                $action_column = 'No Action';
                if ($Kpis->created_by == Auth::user()->id){
                    $action_column = '<a href="employee_kpis/' . $Kpis->id . '/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                    if (Gate::check('employee_kpis_destroy')) {
                        $action_column .= '<form method="post" action="employee_kpis/delete" >';
                        $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                        $action_column .= '<input type="hidden" value=' . $Kpis->id . ' name="first_name" >';
                        $action_column .= csrf_field();
                        $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                    }
            }
                return $action_column;
            })->rawColumns(['title','user_name','branch','action'])
            ->toJson();

    }

    public function delete(StoreUpdateRequest $request)
    {

        if (! Gate::allows('employee_kpis_destroy')) {
            return abort(401);
        }
        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.employee_kpis.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_kpis_active')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.employee_kpis.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive(StoreUpdateRequest $request)
    {
        if (! Gate::allows('employee_kpis_inactive')) {
            return abort(401);
        }

        $data = $request->all();
        $Contact = Contacts::findOrFail($data['first_name']);
        $Contact->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.employee_kpis.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
