<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Licenses\StoreUpdateRequest;
use App\Models\HRM\Licenses;
use Auth;

class LicensesController extends Controller
{
    /**
     * Display a listing of License.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('licenses_manage')) {
            return abort(401);
        }

        $Licenses = Licenses::OrderBy('created_at','desc')->get();

        return view('hrm.licenses.index', compact('Licenses'));
    }

    /**
     * Show the form for creating new License.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('licenses_create')) {
            return abort(401);
        }
        return view('hrm.licenses.create');
    }

    /**
     * Store a newly created License in storage.
     *
     * @param  \App\Http\Requests\HRM\Licenses\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('licenses_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        Licenses::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.licenses.index');
    }


    /**
     * Show the form for editing License.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('licenses_edit')) {
            return abort(401);
        }
        $License = Licenses::findOrFail($id);

        return view('hrm.licenses.edit', compact('License'));
    }

    /**
     * Update License in storage.
     *
     * @param  \App\Http\Requests\HRM\Licenses\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('licenses_edit')) {
            return abort(401);
        }

        $License = Licenses::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $License->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.licenses.index');
    }


    /**
     * Remove License from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('licenses_destroy')) {
            return abort(401);
        }
        $License = Licenses::findOrFail($id);
        $License->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.licenses.index');
    }

    /**
     * Activate License from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('licenses_active')) {
            return abort(401);
        }

        $License = Licenses::findOrFail($id);
        $License->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.licenses.index');
    }

    /**
     * Inactivate License from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('licenses_inactive')) {
            return abort(401);
        }

        $License = Licenses::findOrFail($id);
        $License->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.licenses.index');
    }

    /**
     * Delete all selected License at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('licenses_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Licenses::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
