<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\WorkShifts\StoreUpdateRequest;
use App\Models\HRM\WorkShifts;
use Auth;
use Illuminate\Support\Facades\Cache;
class WorkShiftsController extends Controller
{
    /**
     * Display a listing of WorkShift.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('work_shifts_manage')) {
            return abort(401);
        }

        $WorkShifts = WorkShifts::OrderBy('created_at','desc')->get();

        return view('hrm.work_shifts.index', compact('WorkShifts'));
    }

    /**
     * Show the form for creating new WorkShift.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('work_shifts_create')) {
            return abort(401);
        }
        return view('hrm.work_shifts.create');
    }

    /**
     * Store a newly created WorkShift in storage.
     *
     * @param  \App\Http\Requests\HRM\WorkShifts\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('work_shifts_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        WorkShifts::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.work_shifts.index');
    }


    /**
     * Show the form for editing WorkShift.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('work_shifts_edit')) {
            return abort(401);
        }
        $WorkShift = WorkShifts::findOrFail($id);

        return view('hrm.work_shifts.edit', compact('WorkShift'));
    }

    /**
     * Update WorkShift in storage.
     *
     * @param  \App\Http\Requests\HRM\WorkShifts\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('work_shifts_edit')) {
            return abort(401);
        }

        $WorkShift = WorkShifts::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $WorkShift->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.work_shifts.index');
    }


    /**
     * Remove WorkShift from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('work_shifts_destroy')) {
            return abort(401);
        }
        $WorkShift = WorkShifts::findOrFail($id);
        $WorkShift->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.work_shifts.index');
    }

    /**
     * Activate WorkShift from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('work_shifts_active')) {
            return abort(401);
        }

        $WorkShift = WorkShifts::findOrFail($id);
        $WorkShift->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.work_shifts.index');
    }

    /**
     * Inactivate WorkShift from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('work_shifts_inactive')) {
            return abort(401);
        }

        $WorkShift = WorkShifts::findOrFail($id);
        $WorkShift->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.work_shifts.index');
    }

    /**
     * Delete all selected WorkShift at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('work_shifts_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = WorkShifts::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
