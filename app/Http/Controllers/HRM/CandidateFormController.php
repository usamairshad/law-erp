<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\HRM\CandidateForm;
use App\Models\Admin\BranchUser;
use App\Models\Academics\SubjectDetail;
use App\Models\Academics\Faculty;
use App\Models\JobPost;
use App\Models\Schedule;
use App\Models\Recruitment_Feedback;
use Redirect;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;


class CandidateFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data= CandidateForm::all();
        return view('hrm.recruitment.candidate-form.index',compact('data'));
    }

    public function candidate_list_job($id)
    {
        # code...


//Job for Head Office
        $post_data=JobPost::where('job_post_request_id',$id)->first();
        $post_id=$post_data['id'];
        if(Auth::user()->branch_id == 4)
        {

            $data=CandidateForm::where('job_post_id',$post_id)->get();

        }
        else
        {

        $data=CandidateForm::where('job_post_id',$post_id)->where('status','<>',"Pending")->get();

        }

        // $data=CandidateForm::where('job_post_id',$post_id)->where('status','<>',"Pending")->get();

        return view('hrm.recruitment.candidate-form.candidates_job',compact('data')); 
    }

    public function recommended_candidate_list_job($id)
    {
            # code...
        // $data=CandidateForm::where('job_post_id',$id)->where('status',"Interview_Passed")->where('status',"Hired")->orwhere('status',"Rejected")->get();

          $data= CandidateForm:: where('job_post_id',$id)
          ->where(function($query){
            $query->where('status',"Hired")->orwhere('status',"Rejected")->orwhere('status',"Interview_Passed");
          })->get();



        return view('hrm.recruitment.job_post.recommended_candidates',compact('data'));
    } 


    public function candidate_feedback_job($id)
    {
        # code...

        $data=CandidateForm::where('id',$id)->first();

           

        $data=Recruitment_Feedback::where('candidate_form_id',$id)->where('job_post_id',$data['job_post_id'])->get();

        

        return view('hrm.recruitment.job_post.recruitment_feedback_job',compact('data'));



    }




    public function candidate_hired_job($id)
    {
        # code...
        CandidateForm::where('id',$id)->update(['status'=>'Hired']);

        return redirect()->back();

    }

     public function candidate_rejected_job($id)
    {
        # code...
        CandidateForm::where('id',$id)->update(['status'=>'Rejected']);

        return redirect()->back();

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
         $data= JobPost::find($id);
         return view('hrm.recruitment.candidate-form.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated=$request->validate([
            'email' => 'required|unique:candidate_form',
    
        ]);

        
        if($request->profile_updated)
        {
            CandidateForm::where('id',$request->candidate_id)->update(['status'=>'Acknowledged']);
            if ($request->files->get('cnic'))
            {   
                $temp=  auth()->user()->name;
                $document_cninc = $request->files->get('cnic');
                $document_cninc_name = $temp.'-cnic'.'.'.$document_cninc->getClientOriginalExtension();
                $document_cninc->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $document_cninc_name);
                CandidateForm::where('id',$request->candidate_id)->update(['cnic'=>$document_cninc_name]);       
            }
            if ($request->files->get('domicile'))
            {   
                $temp=  auth()->user()->name;
                $document_cninc = $request->files->get('domicile');
                $document_cninc_name = $temp.'-domicile'.'.'.$document_cninc->getClientOriginalExtension();
                $document_cninc->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $document_cninc_name);
                CandidateForm::where('id',$request->candidate_id)->update(['domicile'=>$document_cninc_name]);       
            }
            if ($request->files->get('passport'))
            {   
                $temp=  auth()->user()->name;
                $document_cninc = $request->files->get('passport');
                $document_cninc_name = $temp.'-passport'.'.'.$document_cninc->getClientOriginalExtension();
                $document_cninc->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $document_cninc_name);
                CandidateForm::where('id',$request->candidate_id)->update(['passport'=>$document_cninc_name]);       
            }
            if ($request->files->get('passport_size_photo'))
            {   
                $temp=  auth()->user()->name;
                $document_cninc = $request->files->get('passport_size_photo');
                $document_cninc_name = $temp.'-passport_size_photo'.'.'.$document_cninc->getClientOriginalExtension();
                $document_cninc->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $document_cninc_name);
                CandidateForm::where('id',$request->candidate_id)->update(['passport_size_photo'=>$document_cninc_name]);       
            }
            if ($request->files->get('degree'))
            {   
                $temp=  auth()->user()->name;
                $document_cninc = $request->files->get('degree');
                $document_cninc_name = $temp.'-degree'.'.'.$document_cninc->getClientOriginalExtension();
                $document_cninc->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $document_cninc_name);
                CandidateForm::where('id',$request->candidate_id)->update(['degree'=>$document_cninc_name]);       
            }
            if ($request->files->get('awards'))
            {   
                $temp=  auth()->user()->name;
                $document_cninc = $request->files->get('awards');
                $document_cninc_name = $temp.'-awards'.'.'.$document_cninc->getClientOriginalExtension();
                $document_cninc->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $document_cninc_name);
                CandidateForm::where('id',$request->candidate_id)->update(['awards'=>$document_cninc_name]);       
            }
            if ($request->files->get('experience_letter'))
            {   
                $temp=  auth()->user()->name;
                $document_cninc = $request->files->get('experience_letter');
                $document_cninc_name = $temp.'-experience_letter'.'.'.$document_cninc->getClientOriginalExtension();
                $document_cninc->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $document_cninc_name);
                CandidateForm::where('id',$request->candidate_id)->update(['experience_letter'=>$document_cninc_name]);       
            }
            if ($request->files->get('recommendation_letter'))
            {   
                $temp=  auth()->user()->name;
                $document_cninc = $request->files->get('recommendation_letter');
                $document_cninc_name = $temp.'-recommendation_letter'.'.'.$document_cninc->getClientOriginalExtension();
                $document_cninc->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $document_cninc_name);
                CandidateForm::where('id',$request->candidate_id)->update(['recommendation_letter'=>$document_cninc_name]);       
            }

        return redirect('/admin/home');
        }
        else
        {
            
            $input = $request->all();

            $rules['email']  =  'required|unique:candidate_form';
            
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $request->flash();
                return redirect()->back()
                    ->withErrors($validator->errors())
                    ->withInput();
            }
            else
            {
                if($request->file('cv'))
                {
                    $image = $request->file('cv');
                    $imageName = $input['name'].'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'uploadDocuments'.DIRECTORY_SEPARATOR, $imageName);
                    
                    $input['cv'] = '/public/images/uploadDocuments/'.$imageName;
                    $candidate = CandidateForm::create($input);  

                }

                $job_post= JobPost::where('id',$input['job_post_id'])->value('job_title');
                $user = User::create([
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'password' => bcrypt($input['password']),
                    'active' => 1
                ]);

                $candidate->update(['user_id'=>$user->id]);
                $user->assignRole('candidate');
                $url=$_SERVER['SERVER_NAME'];
                return view('hrm.recruitment.candidate-form.thankyou',compact('job_post','url'));    
            }

        
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function short_list_candidate($id)
    {
        # code...

        $short_listed=CandidateForm::where('id',$id)->update(['status'=>"Short Listed"]);
        return redirect()->back();



        
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function candidateHired()
    {
        $data= CandidateForm::where('status','Hired')->get();
        return view('hrm.recruitment.hired_candidate.index',compact('data'));
    }

    public function candidate_waiting_list()
    {
        $data= CandidateForm::where('status','Interview_Passed')->get();

        return view('hrm.recruitment.waiting_candidates.index',compact('data'));

        
    }
}
