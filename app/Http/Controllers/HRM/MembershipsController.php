<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\Memberships\StoreUpdateRequest;
use App\Models\HRM\Memberships;
use Auth;

class MembershipsController extends Controller
{
    /**
     * Display a listing of Membership.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('memberships_manage')) {
            return abort(401);
        }

        $Memberships = Memberships::OrderBy('created_at','desc')->get();

        return view('hrm.memberships.index', compact('Memberships'));
    }

    /**
     * Show the form for creating new Membership.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('memberships_create')) {
            return abort(401);
        }
        return view('hrm.memberships.create');
    }

    /**
     * Store a newly created Membership in storage.
     *
     * @param  \App\Http\Requests\HRM\Memberships\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('memberships_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        Memberships::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.memberships.index');
    }


    /**
     * Show the form for editing Membership.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('memberships_edit')) {
            return abort(401);
        }
        $Membership = Memberships::findOrFail($id);

        return view('hrm.memberships.edit', compact('Membership'));
    }

    /**
     * Update Membership in storage.
     *
     * @param  \App\Http\Requests\HRM\Memberships\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('memberships_edit')) {
            return abort(401);
        }

        $Membership = Memberships::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $Membership->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.memberships.index');
    }


    /**
     * Remove Membership from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('memberships_destroy')) {
            return abort(401);
        }
        $Membership = Memberships::findOrFail($id);
        $Membership->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.memberships.index');
    }

    /**
     * Activate Membership from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('memberships_active')) {
            return abort(401);
        }

        $Membership = Memberships::findOrFail($id);
        $Membership->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.memberships.index');
    }

    /**
     * Inactivate Membership from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('memberships_inactive')) {
            return abort(401);
        }

        $Membership = Memberships::findOrFail($id);
        $Membership->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.memberships.index');
    }

    /**
     * Delete all selected Membership at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('memberships_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Memberships::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
