<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\WorkWeeks\StoreUpdateRequest;
use App\Models\HRM\WorkWeeks;
use Auth;

class WorkWeeksController extends Controller
{
    /**
     * Display a listing of WorkWeek.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       // if (! Gate::allows('work_weeks_manage')) {
       //     return abort(401);
       // }
       
        $WorkWeek = WorkWeeks::findOrFail(1);

        return view('hrm.work_weeks.index', compact('WorkWeek'));
    }


    /**
     * Show the form for editing WorkWeek.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     //   if (! Gate::allows('work_weeks_edit')) {
     //       return abort(401);
      //  }
        $WorkWeek = WorkWeeks::findOrFail($id);

        return view('hrm.work_weeks.edit', compact('WorkWeek'));
    }

    /**
     * Update WorkWeek in storage.
     *
     * @param  \App\Http\Requests\HRM\WorkWeeks\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
     //   if (! Gate::allows('work_weeks_edit')) {
     //       return abort(401);
     //   }

        $WorkWeek = WorkWeeks::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        // Calculate Working Days
        $data['working_days'] = 0;

        if($request->get('mon') == 0) {
            $data['working_days']++;
        } else if($request->get('mon') == 4) {
            $data['working_days'] = $data['working_days'] + 0.5;
        }

        if($request->get('tue') == 0) {
            $data['working_days']++;
        } else if($request->get('tue') == 4) {
            $data['working_days'] = $data['working_days'] + 0.5;
        }

        if($request->get('wed') == 0) {
            $data['working_days']++;
        } else if($request->get('wed') == 4) {
            $data['working_days'] = $data['working_days'] + 0.5;
        }

        if($request->get('thu') == 0) {
            $data['working_days']++;
        } else if($request->get('thu') == 4) {
            $data['working_days'] = $data['working_days'] + 0.5;
        }

        if($request->get('fri') == 0) {
            $data['working_days']++;
        } else if($request->get('fri') == 4) {
            $data['working_days'] = $data['working_days'] + 0.5;
        }

        if($request->get('sat') == 0) {
            $data['working_days']++;
        } else if($request->get('sat') == 4) {
            $data['working_days'] = $data['working_days'] + 0.5;
        }

        if($request->get('sun') == 0) {
            $data['working_days']++;
        } else if($request->get('sun') == 4) {
            $data['working_days'] = $data['working_days'] + 0.5;
        }

        $WorkWeek->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.work_weeks.index');
    }

}
