<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\Document;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Auth;
use DB;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (! Gate::allows('erp_document_manage')) {
            return abort(401);
        }

       $documents = Document::OrderBy('created_at','desc')->get();

        return view('hrm.document.index', compact('documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_document_create')) {
            return abort(401);
        }
        $branch_id = Auth::user()->branch_id;

        return view('hrm.document.create',compact($branch_id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if (! Gate::allows('erp_document_create')) {
            return abort(401);
        }
        
        $data = $request->all();

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $fileName = time() . '.' . $file->extension();
            $file->move(public_path('/images/uploadDocuments'), $fileName);
        }

        $data['created_by'] = Auth::user()->id;
        $data['status'] = 1;
        $data['file'] = 'public/images/uploadDocuments/'.$fileName;
        $user2 = Document::create($data);
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.staff.profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_document_edit')) {
            return abort(401);
        }
        $document = Document::find($id);
        
        return view('hrm.document.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_document_edit')) {
            return abort(401);
        }
        $document = Document::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $document->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.staff.profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_document_destroy')) {
            return abort(401);
        }
        $document = Document::findOrFail($id);
        $document->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.staff.profile');
    }
}
