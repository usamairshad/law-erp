<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\JobPostRequest;
use App\Models\Admin\BranchUser;
use App\Models\Academics\Course;
use App\Models\Academics\Faculty;
use App\Models\Academics\Program;
use App\Models\Academics\Board;
use App\Models\Academics\Section;
use App\Models\Academics\Staff;
use Redirect;
use Carbon\Carbon;
use URL;
class JobPostRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $staff_data=Staff::where('user_id',Auth::user()->id)->first();
        
        if(Auth::user()->branch_id == 4)
        {
            $data= JobPostRequest::all();
        }
        else{

            

            $data= JobPostRequest::where('branch_id',Auth::user()->branch_id)->get();
        }
         
        
    
         return view('hrm.recruitment.job_post_request.index',compact('data','staff_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $branch_id = BranchUser::where('user_id',Auth::user()->id)->value('branch_id');
        $subjects  = Course::all();
        $boards    = Board::all();
        $programs  = Program::all();
 
        return view('hrm.recruitment.job_post_request.create',compact('subjects','programs','boards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $input= $request->all();
        $branch_id =Auth::user()->branch_id;
        $input['branch_id'] = $branch_id;



        $input['created_by'] = Auth::user()->id;



        
        JobPostRequest::create($input);

        


        flash('Job Post Request has been created successfully.')->success()->important();
        return redirect()->route('admin.recruitment.job-request');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= JobPostRequest::find($id);

        $sections = ['Junior','Senior'];
        $subjects = Course::all();
        $programs  = Program::all();

        return view('hrm.recruitment.job_post_request.edit',compact('subjects','programs','data','sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $input= $request->all();
        $branch_id = BranchUser::where('user_id',Auth::user()->id)->value('branch_id');

        $input['branch_id'] = $branch_id;
        $input['created_by'] = Auth::user()->id;
        $input['updated_by'] = Auth::user()->id;
        $input['updated_at'] = Carbon::now();

        // dd($input);
        JobPostRequest::find($request->id)->update($input);
        flash('Job Post Request has been updated successfully.')->success()->important();
        return redirect()->route('admin.recruitment.job-request');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
