<?php

namespace App\Http\Controllers\HRM;

use App\Models\HRM\Organizations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\OrganizationLocations\StoreUpdateRequest;
use App\Models\HRM\OrganizationLocations;
use Auth;

class OrganizationLocationsController extends Controller
{
    /**
     * Display a listing of OrganizationLocation.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('organization_locations_manage')) {
            return abort(401);
        }

        $OrganizationLocations = OrganizationLocations::OrderBy('created_at','desc')->get();

        return view('hrm.organization_locations.index', compact('OrganizationLocations'));
    }

    /**
     * Show the form for creating new OrganizationLocation.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('organization_locations_create')) {
            return abort(401);
        }
        return view('hrm.organization_locations.create');
    }

    /**
     * Store a newly created OrganizationLocation in storage.
     *
     * @param  \App\Http\Requests\HRM\OrganizationLocations\StoreUpdateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('organization_locations_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        OrganizationLocations::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('hrm.organization_locations.index');
    }


    /**
     * Show the form for editing OrganizationLocation.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('organization_locations_edit')) {
            return abort(401);
        }
        $OrganizationLocation = OrganizationLocations::findOrFail($id);

        return view('hrm.organization_locations.edit', compact('OrganizationLocation'));
    }

    /**
     * Update OrganizationLocation in storage.
     *
     * @param  \App\Http\Requests\HRM\OrganizationLocations\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('organization_locations_edit')) {
            return abort(401);
        }

        $OrganizationLocation = OrganizationLocations::findOrFail($id);

        $data = $request->all();
        $data['is_default'] = decrypt($data['is_default']);
        $data['organization_id'] = decrypt($data['organization_id']);
        $data['updated_by'] = Auth::user()->id;

        $OrganizationLocation->update($data);

        // Update Parent Organization's Phone & Fax Information
        if($data['is_default']) {
            // Default Location is updated
            $Organizations = Organizations::findOrFail(1);
            $data = $request->all();
            $data['updated_by'] = Auth::user()->id;
            $Organizations->update($data);
        }

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.organization_locations.index');
    }


    /**
     * Remove OrganizationLocation from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('organization_locations_destroy')) {
            return abort(401);
        }
        $OrganizationLocation = OrganizationLocations::findOrFail($id);

        if($OrganizationLocation->is_default) {
            flash('Default Location can not be deleted.')->error()->important();
            return redirect()->route('hrm.organization_locations.index');
        }

        $OrganizationLocation->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('hrm.organization_locations.index');
    }

    /**
     * Activate OrganizationLocation from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('organization_locations_active')) {
            return abort(401);
        }

        $OrganizationLocation = OrganizationLocations::findOrFail($id);

        if($OrganizationLocation->is_default) {
            flash('Default Location can not be activated.')->error()->important();
            return redirect()->route('hrm.organization_locations.index');
        }

        $OrganizationLocation->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('hrm.organization_locations.index');
    }

    /**
     * Inactivate OrganizationLocation from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('organization_locations_inactive')) {
            return abort(401);
        }

        $OrganizationLocation = OrganizationLocations::findOrFail($id);

        if($OrganizationLocation->is_default) {
            flash('Default Location can not be inactivated.')->error()->important();
            return redirect()->route('hrm.organization_locations.index');
        }

        $OrganizationLocation->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('hrm.organization_locations.index');
    }

    /**
     * Delete all selected OrganizationLocation at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('organization_locations_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = OrganizationLocations::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
