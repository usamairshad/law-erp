<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\LeavePeriods\StoreUpdateRequest;
use App\Models\HRM\LeavePeriods;
use Auth;

class LeavePeriodsController extends Controller
{
    /**
     * Display a listing of LeavePeriod.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       // if (! Gate::allows('leave_periods_manage')) {
       //     return abort(401);
       // }

        $LeavePeriod = LeavePeriods::findOrFail(1);
        $date_start = \Carbon\Carbon::createFromDate(date('Y'), $LeavePeriod->start_month, $LeavePeriod->start_day);
        $date_end = \Carbon\Carbon::createFromDate(date('Y'), $LeavePeriod->start_month, $LeavePeriod->start_day)->addYear(1)->subDay(1);

        return view('hrm.leave_periods.index', compact('LeavePeriod', 'date_start', 'date_end'));
    }


    /**
     * Show the form for editing LeavePeriod.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    //    if (! Gate::allows('leave_periods_edit')) {
    //        return abort(401);
    //    }

        $LeavePeriod = LeavePeriods::findOrFail($id);
        $LeavePeriod->start_date = \Carbon\Carbon::createFromDate(date('Y'), $LeavePeriod->start_month, $LeavePeriod->start_day)->format('Y-m-d');
        $LeavePeriod->end_date = \Carbon\Carbon::createFromDate(date('Y'), $LeavePeriod->end_month, $LeavePeriod->end_day)->format('Y-m-d');
        $date_start =  $LeavePeriod->start_date;
        $date_end =  $LeavePeriod->end_date;
        return view('hrm.leave_periods.edit', compact('LeavePeriod', 'date_start', 'date_end'));
    }

    /**
     * Update LeavePeriod in storage.
     *
     * @param  \App\Http\Requests\HRM\LeavePeriods\StoreUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
     //   if (! Gate::allows('leave_periods_edit')) {
     //       return abort(401);
     //   }

        $LeavePeriod = LeavePeriods::findOrFail($id);

        // Extract Day and Year from dates
        $start_date = explode('-', $request->get('start_date'));
        $end_date = explode('-', $request->get('end_date'));

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $data['start_day'] = $start_date[2];
        $data['start_month'] = $start_date[1];
        $data['end_day'] = $end_date[2];
        $data['end_month'] = $end_date[1];

        $LeavePeriod->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('hrm.leave_periods.index');
    }

}
