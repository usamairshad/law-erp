<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\Resign;
use App\Models\HRM\UploadExperienceLetter;
use App\Models\Academics\Staff;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Helpers\Helper;
use App\User;
class ResignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $resigns = Resign::with("user.staff.branch")
            ->latest()  
            ->get();

               
        return view("hrm.resign.index", compact('resigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {   
       // dd($req);

        $join_date_staff=Staff::where('user_id',Auth::id())->value("join_date");
        


        $date = "";
        $date .= $req->from . " | ";
        $date .= $req->to . " | ";

        $input = $req->except(["_token", "date"]);
        $input["notice_period"] = $date;
        $input["user_id"] = Auth::id();
        $input["join_date"]=$join_date_staff;
        $input["experience_letter"] = $req->experience_letter;
        $input["last_day_of_work"] = $req->last_day_of_work;


        Resign::create($input);

        flash("Successfully request forwarded to HR")->success()->important();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)

    { 

        Resign::find($id)->delete();
        flash("successfully data deleted")->success()->important();
        return redirect()->back();
    }

    public function myResign()
    {
        $data = Resign::where('user_id' , Auth::user()->id)->get();
        return view("hrm.resign.my_resign", compact('data')); 
    }
    public function experienceLetterSend($id)
    {

        

        $user_id = Resign::where('id' , $id)->value('user_id');
        $staff_id = Staff::where('user_id' ,$user_id)->value('id');
        $data = UploadExperienceLetter::where('user_id' , $user_id)->first();



        $user_status = User::where('id' , $user_id)->value('active');
        $staff_status = Staff::where('user_id' , $user_id)->value('status');
            
        
        return view("hrm.resign.experience_letter", compact('data' ,'user_status','user_id','staff_id','staff_status')); 
        
        // $class = $data->class;
        // $subject = $data->subject;
        // $last_day_of_work = $data->last_day_of_work;
        // //dd($subject);
        // $user_id= $data->user->id;

        // $data= Staff::where('user_id',$user_id)->first();
       
        // $branch_name = Helper::branchIdToName($data->branch_id);
       
        // return view("hrm.resign.experience_letter", compact('data' , 'branch_name' ,'class' ,'subject','last_day_of_work')); 
    }

    public function resignRequestCeoApprove($id)
    {


        $data = Resign::where('id' , $id)->update(['approved_by_ceo' => 'Approved']);
        $resignEntry=Resign::where('id',$id)->first();
        $staff_data=Staff::where('user_id',$resignEntry['user_id'])->first();

        



        if ($resignEntry->approved_by_ceo=="Approved" && $resignEntry->approved_by_hr_head=="Approved" ) {
            # code...
                    $input["user_id"] = $resignEntry->user_id;
                    $input["name"] = $staff_data->first_name;
                    $input["reg_no"] = $staff_data->reg_no;
                    $input["designation"] = $staff_data->designation;
                    $input["branch_id"] = $staff_data->branch_id;
                    $input["class"] = $resignEntry->class;
                    $input["subject"] = $resignEntry->subject;
                    $input["home_address"] = $staff_data->address;
                    $input["contact"] = $staff_data->start_day_contract;
                    $input["join_date"] = $staff_data->join_date;
                    $input["last_day_of_work"] = $resignEntry->last_day_of_work;
                    UploadExperienceLetter::create($input);
        }






        return redirect()->back();
    }
    
    public function resignRequestCeoReject($id)
    {
        $data = Resign::where('id' , $id)->update(['approved_by_ceo' => 'Rejected']);
        return redirect()->back();
    }
    
    public function resignRequestHrApprove($id)
    {
        $data = Resign::where('id' , $id)->update(['approved_by_hr_head' => 'Approved']);
        $resignEntry=Resign::where('id',$id)->first();
        $staff_data=Staff::where('user_id',$resignEntry['user_id'])->first();


        if ($resignEntry->approved_by_ceo=="Approved" && $resignEntry->approved_by_hr_head=="Approved" ) {
            # code...
                    $input["user_id"] = $resignEntry->user_id;
                    $input["name"] = $staff_data->first_name;
                    $input["reg_no"] = $staff_data->reg_no;
                    $input["designation"] = $staff_data->designation;
                    $input["branch_id"] = $staff_data->branch_id;
                    $input["class"] = $resignEntry->class;
                    $input["subject"] = $resignEntry->subject;
                    $input["home_address"] = $staff_data->address;
                    $input["contact"] = $staff_data->start_day_contract;
                    $input["join_date"] = $staff_data->join_date;
                    $input["last_day_of_work"] =     $resignEntry->last_day_of_work;
                    UploadExperienceLetter::create($input);
        }


        return redirect()->back();
    }
    
    public function resignRequestHrReject($id)
    {
        $data = Resign::where('id' , $id)->update(['approved_by_hr_head' => 'Rejected']);

          return redirect()->back();
    }

    public function experienceLetterStore(Request $req)
    {
        dd($req);
        
        $data = $req->all();
        $user =(int)$req->user_id;

        $data_exist = UploadExperienceLetter::where('user_id', $user)->first();

        //dd($data_exist);
        if ($data_exist) {
            flash("Already Uploaded Experience Letter of this Employee")->success()->important();
         return redirect()->route('admin.resign.index');
        }
        else{

        

        flash("Successfully Uploaded Details of this Employee")->success()->important();
         return redirect()->route('admin.resign.index');
        }



    }

    public function uploadDocument($id)
    {
        //dd('asa');
      $resign_id = $id;

      return view("hrm.resign.upload_doc", compact('resign_id'));   
    }
    public function uploadDocumentExperience(Request $request)
    {
        $resign_id = $request->resign_id;
        $user_id = Resign::where('id' , $resign_id)->value('user_id');
        $experience_letter = UploadExperienceLetter::where('user_id' , $user_id)->value('user_id');
       
     if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = time() . '.' . $file->extension();
            $file->move(public_path('/images/uploadDocuments'), $fileName);
    
     }else{
         flash("File No Selected")->success()->important();
         return redirect()->route('admin.resign.index');
     }
    if ($experience_letter) {
        # code...
   
     $data['experiene_letter_file'] = 'public/images/uploadDocuments/'.$fileName;

      UploadExperienceLetter::where('user_id' , $user_id)->update($data);
      flash("File Upload Successfully")->success()->important();
        return redirect()->route('admin.resign.index');
     }else{

        flash("No Uploaded Experience Letter of this Employee. Please Upload User Details then upload document")->success()->important();
        return redirect()->route('admin.resign.index');
     }

      
    }

     public function uploadClearanceFrom($id)
    {
        //dd('asa');
      $resign_id = $id;

      return view("hrm.resign.upload_clearance_form", compact('resign_id'));   
    }

    public function uploadClearanceFromFile(Request $request)
    {
        $resign_id = $request->resign_id;
        $user_id = Resign::where('id' , $resign_id)->value('user_id');
        $clearance_letter = UploadExperienceLetter::where('user_id' , $user_id)->value('user_id');
       
     if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = time() . '.' . $file->extension();
            $file->move(public_path('/images/uploadDocuments'), $fileName);
    
     }else{
         flash("File No Selected")->success()->important();
         return redirect()->route('admin.resign.index');
     }
    if ($clearance_letter) {
       
     $data['clearance_file'] = 'public/images/uploadDocuments/'.$fileName;

      UploadExperienceLetter::where('user_id' , $user_id)->update($data);
      flash("File Upload Successfully")->success()->important();
        return redirect()->route('admin.resign.index');
     }else{

        flash("No Uploaded Experience Letter of this Employee. Please Upload User Details then upload document")->success()->important();
        return redirect()->route('admin.resign.index');
     }

      
    }

}



        
    