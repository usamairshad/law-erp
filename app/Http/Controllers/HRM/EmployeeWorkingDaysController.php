<?php

namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\HRM\EmployeeWorkingDays\StoreUpdateRequest;
use App\Models\HRM\EmployeeAttendance;
use App\Models\HRM\Employees;
use App\Models\HRM\EmployeeWorkingDays;
use App\Models\HRM\EmployeeDocumentTypes;
use Illuminate\Support\Collection;
use Auth;
use Config;

class EmployeeWorkingDaysController extends Controller
{

    public function index()
    {

        if (!Gate::allows('employee_working_days_manage')) {
            return abort(401);
        }

        $EmployeeWorkingDays = EmployeeAttendance::workingHoursOfPreviousMonth();
        $EmployeeWorkingDays = EmployeeWorkingDays::parseAttendanceArrays($EmployeeWorkingDays);

        return view('hrm.employee_working_days.index', compact('EmployeeWorkingDays'));
    }

    public function getEmployeeInOuts(Request $request){

        if (!Gate::allows('employee_working_days_manage')) {
            return abort(401);
        }

        $data = $request->all();
        $Target = EmployeeAttendance::attendenceByUserAndDate($data['id'], $data['date']);

        return datatables()->of($Target)
            ->addColumn('date_time', function($Target){

                return explode(' ', $Target->date_time)[1];

            })->addColumn('attendance_type', function($Target){

                return $Target->type;

            })->rawColumns(['date_time','attendance_type'])
            ->toJson();
    }

    public function changeAttendanceType(Request $request){

        if (!Gate::allows('employee_working_days_edit')) {
            return abort(401);
        }
        
        $data = $request->all();

        $EmployeeWorkingDay = EmployeeWorkingDays::where('user_id', $data['user_id'])->get()->first();
        if(!empty($EmployeeWorkingDay)){
            $data['updated_by'] = Auth::user()->id;
            $data['updated_at'] = \Carbon\Carbon::now();
            $EmployeeWorkingDay->update($data);
        }
        else{
            $data['created_by'] = Auth::user()->id;
            $data['created_at'] = \Carbon\Carbon::now();
            $data['updated_by'] = Auth::user()->id;
            $data['updated_at'] = \Carbon\Carbon::now();
            EmployeeWorkingDays::create($data);
        }
        return;
    }

    public function fetchFilterRecords(Request $request){

        if (!Gate::allows('employee_working_days_manage')) {
            return abort(401);
        }

        $data = $request->all();

        if($data['mode'] == 'given_month')
            $EmployeeWorkingDays = EmployeeAttendance::workingHoursOfGivenMonth($data['month'], $data['year']);
        else
            $EmployeeWorkingDays = EmployeeAttendance::workingHoursOfPreviousMonth();

        $EmployeeWorkingDays = EmployeeWorkingDays::parseAttendanceArrays($EmployeeWorkingDays);
        $Target = $EmployeeWorkingDays['collection_wise'];

        return datatables()->of($Target)
            ->addColumn('id', function($Target) {

                return $Target['id'];

            })
            ->addColumn('user_name', function($Target) {

                return Employees::getEmployeeNameByUserId($Target['user_id']);

            })
            ->addColumn('job_title', function($Target) {

                return Employees::getJobTitleByUserId($Target['user_id']);

            })
            ->addColumn('date', function($Target) {

                return date('d-m-Y ', strtotime($Target['date']));

            })
            ->addColumn('office_in', function($Target) {

                return $Target['office_in'];

            })
            ->addColumn('office_out', function($Target) {

                return $Target['office_out'];

            })
            ->addColumn('working_hours', function($Target) {

                return $Target['worked_hours'];

            })
            ->addColumn('day_status', function($Target) {

                return \Config::get('hrm.attendance_type_array.'.$Target['attendance_type']);

            })
            ->addColumn('action', function ($Target) {
                $action_column = '<a href="#" style= "margin-right: 2px;" class="btn btn-xs btn-info" data-toggle="modal" data-mode="view" data-id='.$Target['user_id'].' data-date='.$Target['date'].' data-title='.Employees::getEmployeeNameByUserId($Target['user_id']).' : '.$Target['date'].' data-target="#favoritesModal"><i class="glyphicon glyphicon-eye-open"></i></a>';
                $action_column .= '<a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-mode="edit" data-id='.$Target['user_id'].' data-date='.$Target['date'].' data-title='.Employees::getEmployeeNameByUserId($Target['user_id']).' : '.$Target['date'].' data-target="#favoritesModal"><i class="glyphicon glyphicon-edit"></i></a>';
                return $action_column;
            })->rawColumns(['user_name','action'])
            ->toJson();
    }

}
