<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payroll\AttendancePayroll;
use App\Models\Academics\Staff;
use App\Models\Academics\ActiveSessionTeacher;
use App\Models\Academics\ActiveSessionSection;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\Classes;
use App\Models\Academics\Board;
use App\Models\Academics\Branch;
use App\Models\Academics\Program; 
use App\Models\Academics\StudentAttendancePayroll;
use App\Models\Academics\TeacherAttendancePayroll;
use App\Models\Academics\Student;
use App\Models\Academics\StudentEnrollment;
use App\Models\Academics\Course;




use Auth;
use App\Models\Payroll\LeaveManagement;
use Illuminate\Support\Facades\Gate;
use DB;
use Carbon\carbon;

class LeaveManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $leaves = LeaveManagement::all();
        return view("hrm.leave_management.index", compact('leaves'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                //Getting Teacher Data From Staff Table 
          $staff_data=Staff::select('*')->where('user_id',Auth::user()->id)->first();
        $branch_data=Branch::where('id',$staff_data->branch_id)->first();


        //$boards_data=ActiveSession::with()->where('branch_id',$branch_data->id)->get();

        $boards_data=DB::table("active_sessions")
        ->join("boards","active_sessions.board_id","boards.id")
        ->where('active_sessions.branch_id',$branch_data->id)
        ->get();

        return view("hrm.leave_management.create",compact('staff_data','branch_data','boards_data'));

            }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request);
        $staff_id =Staff::where('reg_no' , $request->teacher_id)->value('id');
      
      


        if(isset($request['courses']))

        {
             $active_session_id = ActiveSession::where('board_id',$request->SelectBoard)->where('program_id',$request->programs)->where('class_id',$request->classes)->where('branch_id',$request->branch_id)->value('id');
       

        $active_session_section = ActiveSessionSection::where('active_session_id',$active_session_id)->value('id');
            foreach ($request['courses'] as $key => $course_id) {

                $active_session_teacher_id = ActiveSessionTeacher::where('active_session_section_id' , $active_session_section)->where('staff_id' , $staff_id)->where('course_ids' , $course_id)->value('id');
                

                LeaveManagement::create([
                    'active_session_teacher_id'=> $active_session_teacher_id,
                    'staff_id'=>$staff_id,
                    'leave_type'=>'Lecture base',
                    'leave_date'    => $request->class_date,
                    'course_id' => $course_id
                    
                ]);
            }

        }
        else
        {
            



            LeaveManagement::create([
                    
                    'staff_id'=>$staff_id,
                    'leave_type'=>'Full Day',
                    'leave_date'    => $request->class_date,
                    
                    
                ]);


        }

        
   
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.leave_management.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $leaves = LeaveManagement::where('id',$id)->first();
        return view('hrm.leave_management.show', compact('leaves'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $leaves = LeaveManagement::find($id);        
        return view('hrm.leave_management.edit', compact('leaves'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function leaveRequestList(Request $request)
    {
        $leaves = LeaveManagement::orderBy('status','DESC')->get();
        return view("hrm.leave_management.leave_request", compact('leaves'));
        
    }
    public function leaveRequestApprove($id)
    {
         
        $leaves = LeaveManagement::findOrFail($id);
    
       // dd($leaves);

        $leave_id = $leaves->id;
        $get_data = LeaveManagement::where('id' , $leave_id)->first();
        
       $test = AttendancePayroll::where('active_session_teacher_id' , $get_data->active_session_teacher_id)->where('staff_id' , $get_data->staff_id)->where('course_id' , $get_data->course_id)->update(['attendance' => 1]);

        $leaves->update(['status' => 'Allowed']);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.leave_request.list');
    }
    public function leaveRequestUnapprove($id)
    {
         
        $leaves = LeaveManagement::findOrFail($id);
        

        $leaves->update(['status' => 'Not Allowed']);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.leave_request.list');
    }
}
