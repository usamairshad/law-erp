<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Academics\Staff;
use App\Models\Payroll\AttendancePayroll; 
use App\Models\Payroll\PaySalary;
use App\Models\Academics\ActiveSessionTeacher;
use App\Models\Admin\TaxSettings;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\Loan;
use App\Models\Admin\Eobi;
use App\Models\Admin\Groups;
use App\Models\Admin\Ledgers;
use App\Models\Admin\IncrementPf;
use App\Models\Admin\StaffSalaries;
use App\Models\Admin\branches;
use App\Models\Admin\Entries;
use App\Models\Admin\EntryItems;
use App\Models\SalaryHead;
class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $all_staff = Staff::SelectRaw('staff.*,erp_staff_salaries.id as sid, erp_staff_salaries.month_id,erp_staff_salaries.year_id,erp_staff_salaries.net_salary')
        ->leftjoin('erp_staff_salaries', 'staff.id','=','erp_staff_salaries.staff_id')
        ->get();
        return view("hrm.salary_payroll.index", compact('all_staff'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //



        
        
    }

    



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }
  


    public function full_time_view($id)
    {
        # code...
        $staff_data=Staff::where('id',$id)->first();

        $loan_data=Loan::where('user_id',$staff_data->user_id)->first();

        $eobi_data=Eobi::where('user_id',$staff_data->user_id)->first();

        $filersalary = Staff::where('id' ,$id)->where('filer_type' ,'Filer')->value('salary');

            if(isset($filersalary))
            {
                $yearly_salary = $filersalary * 12;
          //dd($yearly_salary);
                $tax_percent = TaxSettings::where('start_range', '<', $yearly_salary)->where('end_range', '>', $yearly_salary)->where('status','=','1')->get();
             $tax_range = 0;
             $percent = 0;
             $fixed_amount = 0;
               foreach($tax_percent as $tax)
               {
                 //  print_r($tax['start_range']);
                   $tax_range =$tax['end_range']-$tax['start_range'];
                   $percent =$tax['tax_percent'] ;
                   $fixed_amount = $tax['fix_amount'];
                 
               }
               
               $exceed = ($tax_range/100)*$percent;
               $tax_amount = $exceed+$fixed_amount;
                $tax_amount=$tax_amount/12;



            }
            else
            {
                $tax_amount=0;
            }



        
        



        return view("hrm.salary_payroll.full_time_salary",compact('staff_data','loan_data','eobi_data','tax_amount'));

    }

    public function lecture_based_salary($id)
    {
    
        $staff_data=Staff::where('id',$id)->first();





        return view("hrm.salary_payroll.salary_lecture" ,compact('staff_data'));

    }

    public function salary(Request $request)
    {
            $get_data = Staff::join('erp_branches','staff.branch_id', '=', 'erp_branches.id')
            ->join('erp_companies','erp_branches.company_id','=','erp_companies.id')
            ->where('staff.id',$request->staff_id)->get();
        //dd($get_data);
       
        $staff_data=Staff::where('id',$request->staff_id)->first();
        


        if ($staff_data->job_status=="Full Time") {
            # code...


            return view("hrm.salary_payroll.salary",compact('staff_data'));
        }
        else
        {


            $from = $request->start_date;
            $to = $request->end_date;


            $data=AttendancePayroll::whereBetween('class_Date', [$from, $to])->get();



            $salary_amount = DB::table('teacher_attendance_payroll')
            ->join('active_session_teachers', 'teacher_attendance_payroll.active_session_teacher_id', '=', 'active_session_teachers.id')
            ->where('active_session_teachers.staff_id',$staff_data->id)
            ->whereBetween('class_Date', [$from, $to])
            ->where('teacher_attendance_payroll.attendance',1)
            ->sum('rate_per_lecture');


            $deduction = DB::table('teacher_attendance_payroll')
            ->join('active_session_teachers', 'teacher_attendance_payroll.active_session_teacher_id', '=', 'active_session_teachers.id')
            ->where('active_session_teachers.staff_id',$staff_data->id)
            ->whereBetween('class_Date', [$from, $to])
            ->where('teacher_attendance_payroll.attendance',0)
            ->sum('rate_per_lecture');


            $absents = DB::table('teacher_attendance_payroll')
            ->join('active_session_teachers', 'teacher_attendance_payroll.active_session_teacher_id', '=', 'active_session_teachers.id')
            ->where('active_session_teachers.staff_id',$staff_data->id)
            ->whereBetween('class_Date', [$from, $to])
            ->where('teacher_attendance_payroll.attendance',0)
            ->count();



            $basic_deduction=$absents*$staff_data['salary_per_hour']; 




            //dd($salary_amount);
            $staff_data['deduction']=(int)$deduction + $basic_deduction;

            $staff_data['salary']=(int)$salary_amount+(int)$staff_data->salary;

            $net_salary=$staff_data['salary']-$deduction-$basic_deduction;

          

            
            $staff_data['net_salary']=(int)$net_salary;


            $loan_data=Loan::where('user_id',$staff_data->user_id)->first();

            $eobi_data=Eobi::where('user_id',$staff_data->user_id)->first();






            $filersalary = Staff::where('id' ,$request->staff_id)->where('filer_type' ,'Filer')->value('salary');

            if(isset($filersalary))
            {
                $yearly_salary = $filersalary * 12;
          
                $tax_percent = TaxSettings::where('start_range', '<', $yearly_salary)->where('end_range', '>', $yearly_salary)->value('tax_percent');

                $tax_amount=($tax_percent*$yearly_salary)/100;
                $tax_amount=$tax_amount/12;



            }
            else
            {
                $tax_amount=0;
            }
            
     

    
            
            if ($eobi_data) {
             
                return view("hrm.salary_payroll.salary",compact('staff_data','loan_data','eobi_data','tax_amount'));
            }
            else
            {
              $eobi_data = 0;
                return view("hrm.salary_payroll.salary",compact('staff_data','loan_data','eobi_data','tax_amount'));
            }
            

        }

    }






    public function pay_salary(Request $request)
    {
      $date = \Carbon\Carbon::now();
      $data=$request->all();
      $city_id = 0;
      $branch_id = 0;
      $company_id = 0;
      $staff_details = 0;
      $group_id = 0;
      $amount_rec = 0;
      $amount_inc = 0;
      $entry_id =0;
      //get entry_id
      $get_id =Entries::get();
      if($get_id)
      {
        foreach($get_id as $id)
        {
            $entry_id =$id->id;
            
            $entry_id++;
          
        }
      
      }
    else
    {
        $entry_id = 1;
    }

      //get_staff details
      $get_staff_data = Staff::join('erp_branches','staff.branch_id', '=', 'erp_branches.id')
      ->join('erp_companies','erp_branches.company_id','=','erp_companies.id')
      ->where('staff.id',$request->reg_no)->get();
      foreach ($get_staff_data as $key => $value) {
    # code...
    $city_id = $value->city_id;
    $branch_id = $value->branch_id;
    $company_id = $value->company_id;
    $staff_details = $value->reg_no.' '.$value->first_name.' '.$value->last_name;
}
  //get salary heads
      $get_data = SalaryHead::get();
      
foreach($get_data as $key => $vaalue) {
   
 
    $check_group = $vaalue->rec_group_id;
    $fetch = Groups::where(['parent_id'=>$vaalue->rec_group_id,'erp_groups.company_id'=>$company_id])->get();

    foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    }
    $fetch = Groups::where(['parent_id'=>$group_id,'erp_groups.company_id'=>$company_id])->get();
    foreach ($fetch as $key => $value) {
        # code.
        if($value->city_id==$city_id)
        {
            
        $group_id = $value->id;
        $city_id = $value->city_id;
        }
    }
  
    /* $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'erp_groups.company_id'=>$company_id])->get();
    foreach ($fetch as $key => $value) {
        # code.
        if($value->branch_id==$branch_id)
        {
        $group_id = $value->id;
        }
    }
 print_r($group_id);
 print_r($city_id);
 print_r($branch_id);
 print_r($company_id); */
    $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
      foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    }
$amount_rec = 0;
    $rec_ledger = Ledgers::where(['group_id' => $group_id,
            'branch_id' => $branch_id
        ])->first();
    if($check_group==111)
    {
       $amount_rec =  $data['loan_amount'];
    }
    if($check_group==163)
    {
       $amount_rec =  $data['eobi'];
    }
    if($check_group==164)
    {
       $amount_rec =  $data['provident_fund'];
    }
    if($check_group==160)
    {
       $amount_rec =  $data['med_all'];
    }
    if($check_group==161)
    {
       $amount_rec =  $data['rent_all'];
    }
    if($check_group==162)
    {
       $amount_rec =  $data['con_all'];
    }
    if($check_group==172)
    {
       $amount_rec =  $data['training_all'];
    }
    if($check_group==101)
    {
       $amount_rec = $data['net_salary'];
    }
    if($check_group==150)
    {
       $amount_rec = $data['tax_amount'];
    }
   
 
        $item = array(
            'status' => 0,
            'entry_type_id' => 7,
            'entry_id' => $entry_id,
            'voucher_date' =>   $date,
            'ledger_id' => @$rec_ledger->id,
            'narration' => $staff_details,
        );
        $item['amount'] = $amount_rec;
        $item['dc'] = 'c';
          EntryItems::insert($item); 
 
  
    $check_group = $vaalue->inc_group_id;
    $amount_inc =0;
    //income
    $fetch = Groups::where(['parent_id'=>$vaalue->inc_group_id,'erp_groups.company_id'=>$company_id])->get();
    
    foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    }
    $fetch = Groups::where(['parent_id'=>$group_id,'erp_groups.company_id'=>$company_id])->get();
    foreach ($fetch as $key => $value) {
        # code.
        if($value->city_id==$city_id)
        {
            
        $group_id = $value->id;
        $city_id = $value->city_id;
        }
    }
 /*    $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'erp_groups.company_id'=>$company_id])->get();
    foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    } */
    $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
      foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    }
 
    $inc_ledger = Ledgers::where(['group_id' => $group_id,
            'branch_id' => $branch_id
        ])->first();
       
$add=0;
        if($check_group==111)
        {
            $add=1;
         //  $amount_inc =  $data['loan_amount'];
        }
        if($check_group==163)
        {
            $add = 1;
           //$amount_inc =  $data['eobi'];
        }
        if($check_group==393)
        {
            $add = 1;
           //$amount_inc =  $data['provident_fund'];
        }
        if($check_group==167)
        {
           $amount_inc =  $data['med_all'];
        }
        if($check_group==168)
        {
           $amount_inc =  $data['rent_all'];
        }
        if($check_group==169)
        {
           $amount_inc =  $data['con_all'];
        }
        if($check_group==171)
        {
           $amount_inc =  $data['training_all'];
        }
        if($check_group==170)
        {
           $amount_inc = $data['net_salary'];
        }
        
        if($check_group==155)
        {
           $amount_inc = $data['tax_amount'];
        }
    $itemm = array(
        'status' => 0,
        'entry_type_id' => 7,
        'entry_id' => $entry_id,
        'voucher_date' =>  $date,
        'ledger_id' => @$inc_ledger->id,
        'narration' => $staff_details,
    );

    
    $itemm['amount'] = $amount_inc;
    $itemm['dc'] = 'd';
 
  if($add==0)
  {
    EntryItems::insert($itemm);
  } 

}
$dataa = [
    'voucher_date'  => date('y-m-d'),
    'entry_type_id' => 7,
    'id'=>$get_id,
    'branch_id'     => $branch_id,
    'narration'     => $staff_details,
    'dr_total'      => 0,
    'cr_total'      => 0,
    
];
        
  Entries::insert($dataa);

        
        $dat =  $date->format('Y-m-d');

        $salary_slip=$data['reg_no'].$dat;
        
        $lastMonth =  $date->subMonth()->format('M');
        $description=$lastMonth." "."Salary";



        

        $data = array(
                    "ledger_id" => $data['reg_no'],
                    "description"=>$description,
                    "Amount" => $data['salary'],
                    "deduction"=>$data['deduction'],
                    "net_salary"=>$data['net_salary'],
                    "date" => $dat,
                    "staff_id"=>$data['reg_no'],
                    "medical_allowance"=>$data['med_all'],
                    "house_rent_allowance"=>$data['rent_all'],
                    "conveyance_allowance"=>$data['con_all'],

                    "training_all"=>$data['training_all'],

                    "loan_amount"=>$data['loan_amount'],
                    "remaining_amount"=>$data['remaining_amount'],   
                    "pay_loan"=>$data['pay_loan'],
                    "provident_fund"=>$data['provident_fund'],
                    "month_id"=>$data['month_name'],
                    "year_id"=>$data['year_name'],
                    "leaves"=>$data['leaves'],



                    "eobi"=>$data['eobi'],
                    "tax_amount"=>$data['tax_amount'],


                    "salary_slip_id"=>$data['reg_no'],

                    

                );
        $user_id = Staff::where('reg_no' , $request->reg_no)->value('user_id');

        $load_update = Loan::where('user_id' ,$user_id)->update(['remaining' => $request->remaining_amount]);

      //  dd($load_update);







           if (DB::table('erp_staff_salaries')->where('staff_id', $data['staff_id'])->where('month_id',$data['month_id'])->where('year_id',$data['year_id'])->exists()) {

          flash('Salary Already Paid')->success()->important();
        return redirect()->route('admin.salary_payroll.index');

    // ...
        }
        else
        {
          $paid = PaySalary::create($data);
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.salary_payroll.index');

        }









/*

Working Fine

*/


        // $paid = PaySalary::create($data);
        // flash('Record has been created successfully.')->success()->important();
        // return redirect()->route('admin.salary_payroll.index');




    }
    public function salary_permanent($id)
    {

        
      
        $staff_data=Staff::where('id',$id)->first();
        
       // $staff_data=Staff::where('id',$request->staff_id)->get();
       // dd($staff_data);


        if ($staff_data->job_status=="Full Time") {
            # code...
            return view("hrm.salary_payroll.salary",compact('staff_data'));
        }
        else
        {
            $from = $request->start_date;
            $to = $request->end_date;
            $data=AttendancePayroll::whereBetween('class_Date', [$from, $to])->get();
            $salary_amount = DB::table('teacher_attendance_payroll')
            ->join('active_session_teachers', 'teacher_attendance_payroll.active_session_teacher_id', '=', 'active_session_teachers.id')
            ->where('active_session_teachers.staff_id',$staff_data->id)
            ->whereBetween('class_Date', [$from, $to])
            ->sum('rates');
            $staff_data['salary']=$salary_amount;

            
            return view("hrm.salary_payroll.salary",compact('staff_data'));

        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

       


    }

    public function salary_details($id)
    {
        $get_data = StaffSalaries::join('staff','staff.id','=','erp_staff_salaries.staff_id')
        ->where('erp_staff_salaries.id' ,$id)->first(); 

        
        if($get_data)
        {
            return view("hrm.salary_payroll.detail",compact('get_data'));
        }
        else
        {
            return redirect()->back();
        }
       


    }
    
    public function salaryprint($id) 
    {
       // $id = $request->$id;
        
        $get_data = StaffSalaries::join('staff','staff.id','=','erp_staff_salaries.staff_id')
        ->where('erp_staff_salaries.id' ,$id)->first(); 
       
            return view('hrm.salary_payroll.print', compact('get_data'));
        
       


    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function salary_print($id){
        return view('hrm.salary_payroll.print');
    }
    public function company(Request $request){
        $input = $request->company_id;
        $data = Branches::where('company_id','=',$input)->get();
        return response()->json($data);
    }
    public function branch(Request $request){
        $input = $request->branch_id;
        $data = Staff::where('branch_id','=',$input)->get();
        return response()->json($data);
    }
    public function staff(Request $request){
        $input = $request->staffid;

        $data = StaffSalaries::where('staff_id','=',$input)
        ->join('staff','erp_staff_salaries.staff_id','=','staff.id')
        
        
        ->get();
        return response()->json($data);
    }
}
