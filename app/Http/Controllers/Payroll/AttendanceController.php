<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payroll\AttendancePayroll;
use App\Models\Academics\Staff;
use App\Models\Academics\ActiveSessionTeacher;
use App\Models\Academics\ActiveSessionSection;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\Classes;
use App\Models\Academics\Board;
use App\Models\Academics\Branch;
use App\Models\Academics\Program;
use App\Models\Academics\StudentAttendancePayroll;
use App\Models\Academics\TeacherAttendancePayroll;
use App\Models\Academics\Student;
use App\Models\Academics\StudentEnrollment;
use App\Models\Academics\Course;
use App\Models\Academics\CompulsoryCourses;
use App\Models\Academics\AcademicsGroupCourse;
use App\Models\Academics\AcademicsGroup;
use App\Helpers\Helper;
use App\Models\Academics\ActiveSessionStudent;

use App\Models\HRM\TimeTable;
use Validator;
use Illuminate\Support\Facades\Gate;
use Auth;
use DB;
use Carbon\carbon;
use App\User;

class AttendanceController extends Controller
{
    /**

    
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()


    {

        

        // if (!Gate::allows('erp_attendance_payroll_manage')) {
        //     return abort(401);
        // }

        $user_id = Auth::user()->id;
        $staff_id = Staff::where('user_id', $user_id)->value('id');

        // dd($user_id);
        $attendance_payrolls = AttendancePayroll::where('staff_id', $staff_id)->get();

        return view("hrm.attendance_payroll.index", compact('attendance_payrolls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function get_course_name(Request $req)
    {

        $data = Course::where('id', $req->course_id)->first();

        return $data;
    }

    public function get_program_courses_timetable(Request $request)
    {
        # code...



        // $course_data=DB::table("compulsory_courses")
        // ->join("courses","compulsory_courses.course_id","courses.id")
        // ->where('compulsory_courses.program_id',$request->program_id)
        // ->get();

        // $academics_group_data=where('program_id',$request->program_id)->get();
        $courses = [];
        $course_with_name = [];
        $program_id = $request->program_id;
        $compulsory_courses = CompulsoryCourses::where('program_id', $program_id)->pluck('course_id');
        array_push($courses, $compulsory_courses);
        $academic_groups = AcademicsGroup::where('program_id', $program_id)->get();

        foreach ($academic_groups as $key => $academic_group) {
            $academic_group_courses = AcademicsGroupCourse::where('academics_group_id', $academic_group->id)->pluck('course_id');
            array_push($courses, $academic_group_courses);
        }


        foreach ($courses as $key => $course_array) {
            foreach ($course_array as $key => $course) {
                $course_with_name[$key]['id'] = $course;
                $course_with_name[$key]['name'] = Helper::subjectIdToName($course);
            }
        }


        return $course_with_name;
    }


    public function get_timetable_branch(Request $req)
    {
             $data = TimeTable::with("branch")
            ->where("id", $req->time_table_id)
            ->get();

        \Log::info("-------------------------------");
        \Log::info($data);
       // $data = TimeTable::where('id', $req->time_table_id)->get();

       
       return $data;
    }

    public function get_board(Request $req)
    {
        \Log::info('----------------------------------------------');
        \Log::info($req);
        
        $branch_id = $req->branch_id;


        $board_data = ActiveSession::with('board')
        ->where('branch_id', $branch_id)
        ->groupBy('board_id')
        ->get();

       return $board_data;

        # code...
    }


    public function get_program(Request $req)
    {
       \Log::info($req);
        
        $board_id = $req->board_id;
        $branch_id = $req->branch_id;


        $program_data = ActiveSession::with('program')->where('branch_id', $branch_id)->where('board_id', $board_id)->groupBy('program_id')->get();

      
        return $program_data;

        # code...
    }



    public function get_program_class(Request $req)
    {

        $board_id = $req->board_id;
        $branch_id = $req->branch_id;
        $program_id = $req->program_id;
        $class_data = ActiveSession::with('getClass')->where('branch_id', $branch_id)->where('board_id', $board_id)->where('program_id', $program_id)->groupBy('class_id')->get();

        return $class_data;
    }



    public function store_teacher_attendance_daywise()
    {



        $course_data = DB::table("active_session_teachers")
            ->join("class_time_tables", "active_session_teachers.class_timetable_id", "class_time_tables.id")
            ->join("time_tables", "class_time_tables.time_table_id", "time_tables.id")
            ->select('active_session_teachers.staff_id', 'active_session_teachers.id', 'active_session_teachers.course_ids', 'class_time_tables.day', 'time_tables.start_time')

            ->get();

        $i = 0;

        $date = date('Y-m-d');
        $month=date("M");
        $year=date("Y");

        




        foreach ($course_data as $key => $value) {

            $data=Staff::where('id',$value->staff_id)->first();
            


            $sattdata[$i] = array(
                "active_session_teacher_id" => $value->id,
                "staff_id" => $value->staff_id,
                "course_id" => $value->course_ids,
                "day" => $value->day,
                "class_time"  => $value->start_time,
                "day"  => $value->day,
                "attendance"  => 0,
                "attendance_type"  => 'Lecture base',
                "class_Date"    => $date,
                "month" => $month,
                "year"=>$year,
                "branch_id"=>$data['branch_id'],

            );
            $i++;
        }







        foreach ($sattdata as $key => $attendance_payroll) {




            if (DB::table('teacher_attendance_payroll')->where('staff_id', $attendance_payroll['staff_id'])->where('class_Date', $attendance_payroll['class_Date'])->where('class_time', $attendance_payroll['class_time'])->exists()) 
            {

                flash('Daily Schedule of Classes Already  Set.')->success()->important();

            

            }
            else
            {
                $user = TeacherAttendancePayroll::create($attendance_payroll);
                flash('Daily Schedule of Classes Set.')->success()->important();

            }
        }



        return redirect()->back();
    }

    public function get_program_class_section(Request $req)
    {

        $board_id = $req->board_id;
        $branch_id = $req->branch_id;
        $program_id = $req->program_id;
        $class_id = $req->class_id;


        $class_data = ActiveSession::where('branch_id', $branch_id)->where('board_id', $board_id)->where('program_id', $program_id)->where('class_id', $class_id)->first();



        $section_data = ActiveSessionSection::with('section')->where('active_session_id', $class_data->id)->get();

        \Log::info('class_id'.$class_data);


        return $section_data;
    }





    public function get_program_class_section_course(Request $req)
    {
        # code...


        $staff = Staff::where('reg_no', $req->teacher_id)->first();
        $course_data = DB::table("active_session_teachers")
            ->join("courses", "active_session_teachers.course_ids", "courses.id")
            ->where('active_session_teachers.staff_id', $staff->id)
            ->get();


        return $course_data;
    }

    public function get_program_class_section_students(Request $req)
    {
      //  \Log::info($req);

        $students = Student::where('branch_id', $req->branch_id)->where('board_id', $req->board_id)->where('program_id', $req->program_id)->where('class_id', $req->class_id)->where('section_id', $req->section_id)->get();



        return $students;
    }


    public function create()
    {
        // if (!Gate::allows('erp_attendance_payroll_create')) {
        //     return abort(401);
        // }

        //Getting Teacher Data From Staff Table 


        $staff_data = Staff::select('*')->where('user_id', Auth::user()->id)->first();
        $branch_data = Branch::where('id', $staff_data->branch_id)->first();


        //$boards_data=ActiveSession::with()->where('branch_id',$branch_data->id)->get();

        $boards_data = DB::table("active_sessions")
            ->join("boards", "active_sessions.board_id", "boards.id")
            ->where('active_sessions.branch_id', $branch_data->id)
            ->get();

        //dd($boards_data);



        /*  
        $active_session_teacher_data=ActiveSessionTeacher::select('*')->where('staff_id',$staff_id)->get();





        $active_session_section_data=ActiveSessionSection::select('*')->where('id',$active_session_teacher_data[0]->active_session_section_id)->first();

        $active_teacher_section_id=$active_session_section_data->section_id;
        $active_teacher_session_id=$active_session_section_data->active_session_id;
        
        $active_session_data=ActiveSession::select('*')->where('id',$active_teacher_session_id)->first();
       
        $active_session_teacher_class_data=Classes::select('*')->where('id',$active_session_data->class_id)->first();
        $active_session_teacher_program_data=Program::select('*')->where('id',$active_session_data->program_id)->first();
        $active_session_teacher_board_data=Board::select('*')->where('id',$active_session_data->board_id)->first();

        $array_courses;
        $index=0;
        foreach ($active_session_teacher_data as $key => $value) {
            # code...
            
            $array_courses[$index]=Course::select('name')->where('id',$value->course_ids)->value('name');
            $index++;
        }



        $active_session_teacher_course_data=Course::select('*')->where('id',$active_session_teacher_data[0]->course_ids)->first();




        dd($active_session_teacher_course_data);*/







        return view("hrm.attendance_payroll.create", compact('staff_data', 'branch_data', 'boards_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  dd($request['student'][0]);
        // if (!Gate::allows('erp_attendance_payroll_create')) {
        //     return abort(401);
        // }

        //dd($request);
        $user_data = $request->all();





        //dd($request->class_time);
        //dd($user_data);

        $students = $request->student;
        $attendance = $request->attendance;


        $i = 0;
        //dd($attendance[$i]);
        $dayOfYear = today()->dayOfYear;

        $course_id = $user_data['courses'];
        $program_id = $user_data['programs'];
        $branch_id = $user_data['branch_id'];
        $board_id = $user_data['SelectBoard'];
        $class_id = $user_data['classes'];
        $section_id = $user_data['sections'];


        $active_session_sections_student_data = DB::table("active_session_sections")
            ->join("active_sessions", "active_session_sections.active_session_id", "active_sessions.id")
            ->where('active_sessions.branch_id', $branch_id)
            ->where('active_sessions.board_id', $board_id)
            ->where('active_sessions.class_id', $class_id)
            ->where('active_sessions.program_id', $program_id)
            ->where('active_session_sections.section_id', $section_id)
            ->first();
        //dd($active_session_sections_student_data);


        $active_session_student_id = ActiveSessionStudent::where('active_session_section_id', $active_session_sections_student_data->id)->where('student_id', $request['student'][0])->value('id');

        //dd($active_session_student_id); 

         $day = Carbon::now()->format('D');

        foreach ($students as $key => $student) {

            $sattdata[$key] = array(
                "student_id" => $student,
                "attendance" => $attendance[$key],
                "class_date" => $request->class_date,
                "class_time" => $request->class_time,
                "attendance_type"  => $request->attendance_type,
                "day"       => $day,
                "active_session_student_id" => $active_session_student_id,
            );
            $i++;
        }


        foreach ($sattdata as $key => $st_data) {
            $user = StudentAttendancePayroll::create($st_data);
        }



        $teacher_reg_no = $user_data['teacher_id'];
        $staff_data = Staff::where('reg_no', $teacher_reg_no)->first();





        $active_session_sections_data = DB::table("active_session_sections")
            ->join("active_sessions", "active_session_sections.active_session_id", "active_sessions.id")
            ->where('active_sessions.branch_id', $branch_id)
            ->where('active_sessions.board_id', $board_id)
            ->where('active_sessions.class_id', $class_id)
            ->where('active_sessions.program_id', $program_id)
            ->first();



        $active_session_teacher_data = ActiveSessionTeacher::where('active_session_section_id', $active_session_sections_data->id)->where('staff_id', $staff_data['id'])->where('course_ids', $course_id)->first();

        $data = AttendancePayroll::where('active_session_teacher_id', $active_session_teacher_data->id)
            ->where('class_Date', $request->class_date)
            // ->where('class_time', $request->class_time)
            ->where('course_id', $request->courses)
            ->get();

        

        
        if(count($data)==0)
        {   
            flash('No Class at this Time Set.')->success()->important();
             return redirect()->route('admin.attendance_payroll.index');
        }
        else
        {   
            AttendancePayroll::where('id', $data[0]['id'])->update(array('attendance' => '1', 'current_time' => $request->class_time));

        }

        



        // $data = array(
        //             "active_session_teacher_id" => $active_session_teacher_data['id'],
        //             "class_Date" => $user_data['class_date'],
        //             "class_time" => $user_data['class_time'],
        //             "attendance"=>1,
        //             "attendance_type"=>$user_data['attendance_type'],
        //             "course_id"=>$user_data['courses']

        //         );


        //$user = AttendancePayroll::create($data);


        //Student Attendance Entries



        //Student Attendance Entries End
        flash('Attendance has been marked successfully.')->success()->important();
        return redirect()->route('admin.attendance_payroll.index');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // if (!Gate::allows('erp_staff_manage')) {
        //     return abort(401);
        // }
        $attendance = AttendancePayroll::where('id', $id)->first();

        return view('hrm.attendance_payroll.show', compact('attendance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //  if (!Gate::allows('erp_attendance_payroll_edit')) {
        //     return abort(401);
        // }
        $attendance_values = AttendancePayroll::find($id);
        return view('hrm.attendance_payroll.edit', compact('attendance_values'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // if (!Gate::allows('erp_attendance_payroll_edit')) {
        //     return abort(401);
        // }

        $Attendace_update = AttendancePayroll::findOrFail($id);
        $data = $request->all();

        $Attendace_update->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.attendance_payroll.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function get_student_attendance_detail_list(Request $request)
    {
        //dd($request);
        /*  $get_students = Student::where('branch_id' , $request->branch_name)
        ->where('board_id' , $request->SelectBoard)
        ->where('program_id' , $request->programs)
        ->where('class_id' , $request->classes)
        ->where('section_id' , $request->sections)
        ->get();       
        */

        // dd($request);
        $get_students = new Student();
        //dd($get_students);
        if ($request->branch_name != "null" && $request->branch_name != null) {
            
            $get_students =  $get_students->where('branch_id', $request->branch_name);
        }
        if ($request->SelectBoard != "null" && $request->SelectBoard != null) {

            $get_students =  $get_students->where('board_id', $request->SelectBoard);
        }
        if ($request->programs != "null" && $request->programs != null) {
            $get_students =  $get_students->where('program_id', $request->programs);
        }
        if ($request->classes != "null" &&  $request->classes != null) {
            $get_students = $get_students->where('class_id', $request->classes);
        }
        if ($request->sections != "null" &&  $request->sections != null) {
            $get_students =   $get_students->where('section_id', $request->sections);
        }

        $get_students = $get_students->pluck("id");

        //dd($get_students);

        /* foreach ($get_students as $key => $get_student) {

            $student_attendance = StudentAttendancePayroll::where('student_id', $get_student->id)

                ->where('class_date', '>', $request->dateInput1)

                ->where('class_date', '<', $request->dateInput2)

                ->get();
        } */


        $student_attendance = StudentAttendancePayroll::whereIn('student_id', $get_students);
        if ($request->dateInput1 != "null" && $request->dateInput1 != null) {
            $student_attendance = $student_attendance->where('class_date', '>', $request->dateInput1);
        }
        if ($request->dateInput2 != "null" && $request->dateInput2!= null) {
            $student_attendance = $student_attendance->where('class_date', '<', $request->dateInput2);
        }
        $student_attendance = $student_attendance->get();


        return view("admin.attendance.table", compact('student_attendance'));
    }


    

    public function get_staff_list()
    {

        // if (!Gate::allows('erp_staff_manage')) {
        //     return abort(401);
        // }
        $staffs = Staff::all();


        return view('hrm.staff.sos_staff_list', compact('staffs'));
    }
    public function staff_status_active($id)
    {

        // if (!Gate::allows('erp_staff_manage')) {
        //     return abort(401);
        // }
        $staff = Staff::findOrFail($id);
        $staff->update(['status' => 'Active']);

        flash('Staff has been inactivated successfully.')->success()->important();

       return redirect()->route('staff_status_list');
    }

    public function staff_status_inactive($id)
    {


        $staff = Staff::findOrFail($id);
        $staff->update(['status' => 'InActive']);

        flash('Staff has been inactivated successfully.')->success()->important();

        return redirect()->route('staff_status_list');
    }



    public function student_attendance_form()
    {
        # code...

        return view("hrm.attendance_payroll.index");

    }


    public function get_student_attendance_detail()
    {
        dd("Student Attendance");
        $Attendance_Data=Helper::attendance_sheet_details();
        return view("hrm.attendance_payroll.attendance_sheet_details", compact('Attendance_Data'));

        $branch_data=Branch::pluck('name','id'); 
        return view("admin.attendance.detail" , compact('branch_data'));
    }
    public function terminatedStaffList()
    {
        //$users = User::where('active' , 0)->get();
         $users= DB::table('erp_users')
            ->where('active' , 0)
            ->join('staff', 'erp_users.id', '=', 'staff.user_id')
           
            ->select('erp_users.name','erp_users.id', 'erp_users.email', 'erp_users.role_id' , 'staff.reg_no','staff.branch_id')
            ->get();

      //  dd($users);
        return view("hrm.staff.termination" , compact('users'));
    }







}