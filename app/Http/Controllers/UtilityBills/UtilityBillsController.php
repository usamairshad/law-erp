<?php

namespace App\Http\Controllers\UtilityBills;

use App\Models\Asset\Asset;
use App\Models\Asset\AssignAsset;
use App\Models\UtilityBills\UtilityBills;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UtilityBillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utility_bills = UtilityBills::all();
        return view('UtilityBills.index',compact('utility_bills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->hide_id == '' || $request->hide_id == 0){
            UtilityBills::create([
                'branch_id' => $request->branch,
                'month' => $request->month,
                'meter_no' => $request->meter_no,
                'bill_amount'=>$request->bill_amount,
                'nature'=>$request->nature,
                'with_holding_tax'=>$request->with_holding_tax,
                'total_bill'=>$request->total_bill,
                'last_month_bill'=>$request->last_month_bill,
                'increase_decease_bill'=>$request->increase_decrease_bill,
                'reason'=>$request->reason,

            ]);
            return redirect()->back()->with('success','Data has been created');
        }
        else{
            UtilityBills::find($request->hide_id)->update([
                'branch_id' => $request->branch,
                'month' => $request->month,
                'meter_no' => $request->meter_no,
                'bill_amount'=>$request->bill_amount,
                'nature'=>$request->nature,
                'with_holding_tax'=>$request->with_holding_tax,
                'total_bill'=>$request->total_bill,
                'last_month_bill'=>$request->last_month_bill,
                'increase_decease_bill'=>$request->increase_decrease_bill,
                'reason'=>$request->reason,
            ]);
            return redirect()->back()->with('success','Data has been updated');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UtilityBills::where('id',$id)->delete();
        return redirect()->back()->with('deleted','Data Deleted successfully!');
    }
}
