<?php

namespace App\Http\Controllers\QA;


use App\Http\Controllers\Controller;
use App\Models\Academics\Board;
use App\Models\Academics\Classes;
use App\Models\Academics\Program as AcademicsProgram;
use App\Models\Academics\Student;
use App\Models\Admin\Branches;
use App\Models\Admin\Program as AdminProgram;
use App\Models\QA\GroupEvent;
use App\Models\QA\GroupEventStudent;
use App\UploadedDocument;
use Illuminate\Http\File as HttpFile;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Program;
use Illuminate\Support\Facades\Storage;
use ZipArchive;
use ZipStream\File;
use ZipStream\Option\File as OptionFile;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows("erp_group_event_manage")) {

            return abort(401);
        }


        $groups = GroupEvent::with('groupstudentevent.student')->get();
     
        return view("QA.groups.index", compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows("erp_group_event_manage")) {

            return abort(401);
        }




        $branches = Branches::all();

        return view("QA.groups.create", compact('branches'));
    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {

        if (!Gate::allows("erp_group_event_create")) {

            return abort(401);
        }


        $messages = [
            "required" => 'The group name is required'
        ];
        $rules = [
            "category" => "required"
        ];


        $validator = Validator::make(["category" => $req->category], $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $groupEvent = GroupEvent::create(["category" => $req->category]);

        $students_id = $req->student_ids;
        if (!empty($students_id)) {

            foreach ($students_id as  $student_id) {

                GroupEventStudent::create(

                    ["group_event_id" => $groupEvent->id, "student_id" => $student_id]
                );
            }

            flash("successfully group created")->success()->important();

            /////////////////=========================Upload documents==============================/////////////
            if ($req->hasFile('files')) {

                $files = $req->file('files');

                foreach ($files as $file) {

                    $filename = $file->getClientOriginalName();

                    UploadedDocument::create(["group_event_id" =>  $groupEvent->id, "files_path" => $filename]);

                    $file->move(public_path('/document'), $filename);
                }
            }


            return redirect()->back();
        }


        flash("something went wrong")->error()->important();
        return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows("erp_group_event_manage")) {

            return abort(401);
        }



        $group = GroupEvent::with('groupstudentevent.student')
            ->where("id", $id)
            ->first();

        $branches = Branches::all();

        return view("QA.groups.edit", compact('group', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {

        if (!Gate::allows("erp_group_event_edit")) {

            return abort(401);
        }





        $groupEvent = GroupEvent::findOrFail($id);

        $messages = [
            "required" => 'The group name is required'
        ];
        $rules = [
            "category" => "required"
        ];
        $validator = Validator::make(["category" => $req->category], $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $groupEvent->category = $req->category;
        $groupEvent->save();

        $students_id = $req->student_ids;
        if (!empty($students_id)) {

            foreach ($students_id as  $student_id) {

                GroupEventStudent::updateOrCreate(

                    ["group_event_id" => $groupEvent->id, "student_id" => $student_id]

                );
            }

            flash("successfully group created")->success()->important();
            return redirect()->back();
        }


        flash("something went wrong")->error()->important();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows("erp_group_event_destroy")) {
            return abort(401);
        }




        GroupEvent::find($id)->delete();
        flash("Deletd successfuly")->success()->important();
        return redirect()->back();
    }


    public function getStudents($branch_id, $board_id, $program_id, $class_id)
    {

        $students = Student::where("branch_id", $branch_id)
            ->where("board_id", $board_id)
            ->where("program_id", $program_id)
            ->where("class_id", $class_id)
            ->get();


        return $students;
    }


    public function archiveGroup($id)
    {
        $group = GroupEvent::find($id);

        $group->archive = true;

        $group->save();

        flash("Group Succesfully Archived")->success()->important();

        return redirect()->back();
    }


    public function archiveGroupView()
    {

        $groups = GroupEvent::with('groupstudentevent.student')->where("archive", 1)->get();


        //dd($groups);
        return view("QA.groups.archive", compact('groups'));
    }


    public function unarchiveGroup($id)
    {
        $group = GroupEvent::find($id);

        $group->archive = false;

        $group->save();

        flash("Group Succesfully Unarchived")->success()->important();

        return redirect()->back();
    }


    public function downloadDocument($id)
    {

        $groupDocuments = GroupEvent::with('uploadeddocument')->where("id", $id)->first();

        //dd($groupDocuments);

        $zip = new ZipArchive;

        $fileName = 'myNewFiles.zip';

        // if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE) {
        if (true === ($zip->open(public_path($fileName), ZipArchive::CREATE | ZipArchive::OVERWRITE))) {

            foreach ($groupDocuments->uploadeddocument  as  $document) {

                $relativeNameInZipFile = basename($document->files_path);


                $zip->addFile(public_path("document/$document->files_path"), $relativeNameInZipFile);
            }

            $zip->close();
        }

        return response()->download(public_path($fileName), $fileName);
    }


    public function sendNotification($group_id)
    {
        $groupStudents = GroupEventStudent::where("group_event_id", $group_id)->get();

        foreach ($groupStudents as $groupStudent) {

            $groupStudent->notification = true;
            $groupStudent->acknowledge = false;

            $groupStudent->save();
        }

        flash("succesfully notification has been sended")->success()->important();

        return redirect()->back();
    }
}
