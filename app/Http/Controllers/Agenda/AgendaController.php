<?php

namespace App\Http\Controllers\Agenda;

use App\Models\Academics\Staff;
use App\Models\Agenda\Agenda;
use App\Models\Agenda\AgendaApprove;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Auth;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agendas = Agenda::where('created_by' , Auth::user()->id)->get();
 
        return view('Agenda.agenda',compact('agendas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request,[
            'title' => 'required',
            'subject' => 'required',
            'description' => 'required',
            'auth_id'  => 'required'
        ]);
        $id=$request->id;
        if ($id==0 || $id==''){
        Agenda::create([
            'title'           => $request['title'],
            'subject'           => $request['subject'],
            'description'           => $request['description'],
            'created_by'     => Auth::user()->id,    
        ]);
           
            return redirect()->back()->with('success','Data Created successfully!');
        }
        else{
            Agenda::where('id',$id)->update(['title' => $request->title, 'subject' =>
                $request->subject, 'description' => $request->description, 'created_by' => $request->auth_id]);
            return redirect()->back()->with('success','Data Updated successfully!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Agenda::where('id',$id)->delete();
        return redirect()->back()->with('deleted','Data Deleted successfully!');
    }
    public function agendaReject(Request $request, $id)
    { 
        //dd($request->reason);
        $data = Agenda::where('id' , $id)->update(['reason' => $request->reason]);
        $data = Agenda::where('id' , $id)->update(['status' => 'rejected']);
        return redirect()->back()->with('error','Rejected Successfully!');
    }
    public function agendaapprove(Request $request){
        $this->validate($request,[
            'time' => 'required',
            'date' => 'required',
            'description' => 'required',
            'radio' => 'required'
        ]);

        if ($request->status == 'scheduled'){
            AgendaApprove::where('agenda_request_id',$request->agenda_request_id)->update(['date'=>
                $request->date,'time'=>$request->time,'in_campus'=>
                $request->radio,'location'=>$request->location, 'description'=>$request->description]);
            Agenda::where('id',$request->agenda_request_id)->update(['status'=>'rescheduled']);
        }
        else{
            AgendaApprove::create(['agenda_request_id' => $request->agenda_request_id, 'date'=>
                $request->date,'time'=>$request->time,'in_campus'=>
                $request->radio,'location'=>$request->location, 'description'=>$request->description]);
            Agenda::where('id',$request->agenda_request_id)->update(['status'=>'scheduled']);
        }
       return redirect()->back()->with('success','Approved successfully!');
    }
    public function agendaList(){
        $agendas = Agenda::all();
       
        return view('Agenda.agendalist',compact('agendas'));
    }
    public function schedule_date_time($id){
        $record = AgendaApprove::where('agenda_request_id', $id)->get();
        return response()->json($record);
    }
}
