<?php

namespace App\Http\Controllers\gmfinancedashboard;

use App\Models\Academics\Board;
use App\Models\Academics\Company;
use App\Models\Academics\Course;
use App\Models\Academics\Program;
use App\Models\Academics\Staff;
use App\Models\Academics\Student;
use App\Models\Admin\Branches;
use App\Models\LandManagement\Building;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActiveSessionSection;
use App\Helpers\Helper;
use App\Http\Controllers\LandManagement\BuildingManagementController;
use App\IdCard;
use App\Models\Academics\ActiveSession;
use App\Models\Admin\Ledgers;
use App\Models\Academics\ActiveSessionStudent;
use App\Models\Academics\BoardBranch;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\Branch;

use App\Roles;
use App\RoleUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;
class GmFinanceDashboardController extends Controller
{
    public function index(){
        $branches = Branches::count();
        $programs = Program::count();
        $boards = Board::count();
        $companies = Company::count();
        $buildings = Building::count();
        $staff  = Staff::count();
        $students = Student::count();
        $courses = Course::count();

        $branch_wise_sos = [];
        $branch_wise_walkin = [];
        $branches_for_charts = Branches::where('company_id',1)->get();
        foreach ($branches_for_charts as $key => $branch) {

            //for sos
            $sos_student = Student::where('branch_id',$branch->id)
                ->where('status','InActive')
                ->count();
            $sos_teacher = Staff::where('branch_id',$branch->id)
                ->where('status','InActive')
                ->count();

            $branch_wise_sos[$key] = [$branch->name,$sos_student,$sos_teacher];

            //for walkin
            $walkin = DB::table('walk_in_registration')->where('branch_id',$branch->id)->count();

            $branch_wise_walkin[$key] = [$branch->name,$walkin];

        }
        $branch_wise_sos_json = json_encode($branch_wise_sos);
        return view('gmfinacedashboard.gmfinancedashboard', compact('branches'
            ,'programs','boards','companies',
            'buildings','staff', 'students', 'courses','branch_wise_sos','branch_wise_walkin'));

    }
}
