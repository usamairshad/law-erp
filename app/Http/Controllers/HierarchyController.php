<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Branches;
use App\Models\Board;
use App\Models\Academics\Course;
use App\Models\BranchBoardPrograms;
use DB;
use Response;
use App\Helpers\Helper;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\Student;
class HierarchyController extends Controller
{
    public function getBoards(Request $request)
    {
       $boards = DB::table('branch_board_program')->where('branch_id',$request->branch_id)->groupBy('board_id')->get();
    	$boards_data = [];
    	foreach ($boards as $key => $board) {
    		$board_id = $board->board_id;
    		$board_name = Helper::boardIdToName($board->board_id);
    		$temp_array = array('id' => $board_id, 'name' => $board_name);
    		array_push($boards_data, $temp_array); 
    	}
    	$boards_data = json_encode($boards_data);

    	return Response::json($boards_data);
    }

    public function getPrograms(Request $request)
    {
    	$programs = DB::table('branch_board_program')
    		->where('branch_id',$request->branch_id)
    		->where('board_id',$request->board_id)
    		->groupBy('program_id')
    		->get();
    	$programs_data = [];
    	foreach ($programs as $key => $program) {
    		$program_id = $program->program_id;
    		$program_name = Helper::programIdToName($program->program_id);
    		$temp_array = array('id' => $program_id, 'name' => $program_name);
    		array_push($programs_data, $temp_array); 
    	}
    	$programs_data = json_encode($programs_data);
    	return Response::json($programs_data);
    }

    public function getClasses(Request $request)
    {
    	$classes = DB::table('board_program_class')
    		->where('board_id',$request->board_id)
    		->where('program_id',$request->program_id)
    		->groupBy('class_id')
    		->get();
    	$class_data = [];
    	foreach ($classes as $key => $class) {
    		$class_id = $class->class_id;
    		$class_name = Helper::newClassIdToName($class->class_id);
    		$temp_array = array('id' => $class_id, 'name' => $class_name);
    		array_push($class_data, $temp_array); 
    	}
    	$class_data = json_encode($class_data);
    	return Response::json($class_data);
    }

    public function getSections(Request $request)
    {
    	$board_program_class_id = DB::table('board_program_class')
    		->where('board_id',$request->board_id)
    		->where('program_id',$request->program_id)
    		->where('class_id',$request->class_id)
    		->value('id');
   
    	$sections =  DB::table('assign_class_section')
    		->where('board_program_class_id',$board_program_class_id)
    		->get();

    	$section_data = [];
    	foreach ($sections as $key => $section) {
    		$section_id = $section->section_id;
    		$section_name = Helper::newSectionIdToName($section->section_id);
    		$temp_array = array('id' => $section_id, 'name' => $section_name);
    		array_push($section_data, $temp_array); 
    	}
    	$section_data = json_encode($section_data);
    	return Response::json($section_data);
    }
    public function getCourses(Request $request)
    {
        $courses = Helper::getProgramCourses($request->program_id);
        $courses = json_encode($courses);
        return Response::json($courses);
    }


    public function get_student_board(Request $req)
    {

        $branch_id=$req->branch_id;

        $board_data=Student::with('board')
        ->where('branch_id',$branch_id)
        ->groupBy('board_id')
        ->get();

        //\Log::info($board_data);

        return $board_data;
    }

    public function get_program(Request $req)
    {


        $board_id=$req->board_id;
        $branch_id=$req->branch_id;
        
        //\Log::info("Get Programs");
        $program_data=Student::with('program')
        ->where('branch_id',$branch_id)
        ->where('board_id',$board_id)
        ->groupBy('program_id')
        ->get(); 

       // \Log::info($program_data);
       return $program_data; 

        # code...
    }

    public function get_student_board_program_class(Request $req)
    {
        

        $board_id=$req->board_id;
        $branch_id=$req->branch_id;
        $program_id=$req->program_id;
        
        //\Log::info("Get Programs");
        $class_data=Student::with('getClass')
        ->where('branch_id',$branch_id)
        ->where('board_id',$board_id)
         ->where('program_id',$program_id)
        ->groupBy('class_id')
        ->get(); 

        //\Log::info($class_data);
       return $class_data; 

        # code...
    }
    public function get_student_board_program_class_section(Request $req)
    {
        $board_id=$req->board_id;
        $branch_id=$req->branch_id;
        $program_id=$req->program_id;
        $class_id=$req->class_id;
    
        $section_data=Student::with('Section')
        ->where('branch_id',$branch_id)
        ->where('board_id',$board_id)
        ->where('program_id',$program_id)
        ->where('class_id',$class_id)
        ->groupBy('section_id')
        ->get(); 

    return $section_data; 


    }
    public function loadSubjectAgainstProgram(Request $request){

        \Log::info($request);
        $data= Course::where('program_id',$request->program_id)->get();

        return $data;   
    }

    

}
