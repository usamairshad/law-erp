<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PaymentMethods\StoreUpdateRequest;
use App\Models\Admin\PaymentMethods;
use Auth;

class PaymentMethodsController extends Controller
{
    /**
     * Display a listing of PaymentMethod.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_payment_methods_manage')) {
            return abort(401);
        }

        $PaymentMethods = PaymentMethods::OrderBy('created_at','desc')->get();

        return view('admin.payment_methods.index', compact('PaymentMethods'));
    }

    /**
     * Show the form for creating new PaymentMethod.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_payment_methods_create')) {
            return abort(401);
        }
        return view('admin.payment_methods.create');
    }

    /**
     * Store a newly created PaymentMethod in storage.
     *
     * @param  \App\Http\Requests\Admin\PaymentMethods\StoreUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('erp_payment_methods_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        PaymentMethods::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.payment_methods.index');
    }


    /**
     * Show the form for editing PaymentMethod.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_payment_methods_edit')) {
            return abort(401);
        }
        $PaymentMethod = PaymentMethods::findOrFail($id);

        return view('admin.payment_methods.edit', compact('PaymentMethod'));
    }

    /**
     * Update PaymentMethod in storage.
     *
     * @param  \App\Http\Requests\Admin\PaymentMethods\StoreUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('erp_payment_methods_edit')) {
            return abort(401);
        }

        $PaymentMethod = PaymentMethods::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $PaymentMethod->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.payment_methods.index');
    }


    /**
     * Remove PaymentMethod from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_payment_methods_destroy')) {
            return abort(401);
        }
        $PaymentMethod = PaymentMethods::findOrFail($id);
        $PaymentMethod->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.payment_methods.index');
    }

    /**
     * Activate PaymentMethod from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_payment_methods_active')) {
            return abort(401);
        }

        $PaymentMethod = PaymentMethods::findOrFail($id);
        $PaymentMethod->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.payment_methods.index');
    }

    /**
     * Inactivate PaymentMethod from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_payment_methods_inactive')) {
            return abort(401);
        }

        $PaymentMethod = PaymentMethods::findOrFail($id);
        $PaymentMethod->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.payment_methods.index');
    }

    /**
     * Delete all selected PaymentMethod at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_payment_methods_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = PaymentMethods::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
