<?php


namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TaxSettings\StoreUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\LoanInstallment;
use Auth;

class LoanInstallmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asset_types = LoanInstallment::OrderBy('created_at','desc')->get();
        return view('loanInstallment.index', compact('asset_types')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('loanInstallment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        
        LoanInstallment::create([
            'loan_installment'           => $request['loan_plan'],
            'tenure'    => $request['Duration']
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('loan-installment-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset_types = LoanInstallment::where('loan_plan_id',$id)->first();
        return view('loanInstallment.edit', compact('asset_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
        {
   $id = $request['id'];

        $Branche = LoanInstallment::where('loan_plan_id',$id);
        $Branche->update([
            'loan_installment'           => $request['loan_plan'],
            'tenure'    => $request['tenure'],   
        ]);
       

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('loan-installment-list');
    }

    public function destroy($id)
    {
       
        $Branche = LoanInstallment::where('loan_plan_id',$id);
        
        $Branche->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('loan-installment-list');
    }
}
