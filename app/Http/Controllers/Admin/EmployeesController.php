<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Territory;
use App\Models\HRM\EmployeesEducationDetail;
use App\Models\Admin\Branches;
use App\Models\HRM\EmployeesExperienceDetail;
use App\Models\HRM\EmployeesJobDetail;
use App\Models\HRM\EmployeesResignationDetail;
use App\Models\HRM\EmployeesSalaryDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Employees\StoreUpdateRequest;
use App\Models\HRM\Employees;
use App\Models\Admin\Departments;
use App\Models\HRM\Leaves;
use App\User;
use App\Helpers\CoreAccounts;
use Auth;
use Config;
use DB;


class EmployeesController extends Controller
{
    /**
     * Display a listing of Employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_employees_manage')) {
            return abort(401);
        }

        $Employees = Employees::with(['report_to_name'])->OrderBy('created_at','desc')->get();

        return view('admin.employees.index', compact('Employees'));
    }

    /**
     * Show the form for creating new Employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_employees_create')) {
            return abort(401);
        }
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select Department', '');
        $Employees = Employees :: pluckActiveOnly();
        $allSalesman = Employees :: allSalesman();

        $Branches = Branches::allBranches();
        $Territory = Territory::allTerritory();

        return view('admin.employees.create', compact('Departments','Employees','Branches','Territory','allSalesman'));
    }

    /**
     * Store a newly created Employee in storage.
     *
     * @param  \App\Http\Requests\Admin\Employees\StoreUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        //dd($request->all());
        if (! Gate::allows('erp_employees_create')) {
            return abort(401);
        }
        $userarray = [];
        $data = $request->all();
        if (trim($data['email'])== '')
        {
            $userarray['_token'] = $data['_token'];
            $userarray['name'] = $data['first_name'] ;
            $userarray['email'] = '' ;
            $userarray['active'] = 1;
            $userarray['password'] = 'vWBBW';
        }else
        {
            $userarray['_token'] = $data['_token'];
            $userarray['name'] = $data['first_name'] ;
            $userarray['email'] = $data['email'] ;
            $userarray['active'] = 1;
            $userarray['password'] = 'vWBBW';
        }
        $user = User::create($userarray);
        $roles = $request->input('roles') ? $request->input('roles') : [2];
        $user->assignRole($roles);
        $data['user_id'] = $user['id'];
        $data['created_by'] = Auth::user()->id;
        $employeeid = Employees::create($data);
        //Creating Employee Ledgers
        if($data['department_id'] == 1 || $data['department_id'] == 2){
            $parent_gourp = Config::get('constants.admin_accounts_salaries');
        }elseif ($data['department_id'] == 3){
            $parent_gourp = Config::get('constants.engineering_expenses');
        }else{
            $parent_gourp = Config::get('constants.sales_expenses');
        }
        //$emp_salary = CoreAccounts::getConfigGroup(Config::get('constants.admin_accounts_salaries'));
        for($i=1;$i<=5;$i++){
            $salary = array(
                'name' => $request['first_name'],
                'group_id' => $parent_gourp,
                'balance_type' => 'd',
                'opening_balance' => 0,
                'parent_type' => $employeeid->id,
            );
            CoreAccounts::createLedger($salary);
            $parent_gourp = $parent_gourp+1;

        }

        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.employees.index');
    }


    /**
     * Show the form for editing Employee.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $Employee = Employees::findOrFail($id);
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select Department', '');
        $Employees = Employees :: pluckActiveOnly();
        $allSalesman = Employees :: allSalesman();
        $Branches = Branches::allBranches();
        $Territory = Territory::allTerritory();

        return view('admin.employees.edit', compact('Employee','Departments','allSalesman','Employees','Branches','Territory'));
    }
    //echo '<pre>'; print_r($Branches); exit;


    /**
     * Update Employee in storage.
     *
     * @param  \App\Http\Requests\Admin\Employees\StoreUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }

        $Employee = Employees::findOrFail($id);
        $Users = User::findOrFail($Employee->user_id);
        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        if(! isset($data['is_sc'])){
            $data['is_sc'] = 0;
        }

        if(! isset($data['is_eobi'])){
            $data['is_sc'] = 0;
        }

        if(! isset($data['is_house_rent'])){
            $data['is_house_rent'] = 0;
        }
        if(! isset($data['is_utility'])){
            $data['is_utility'] = 0;
        }
        if(! isset($data['is_overtime'])){
            $data['is_overtime'] = 0;
        }

        if(! isset($data['is_eobi'])){
            $data['is_eobi'] = 0;
        }



        $Employee->update($data);

        $user_data = array(
            'name' => $data['first_name'],
            'email' => $data['email'],
            'active' => 1,
            );
        $Users->update($user_data);
        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.employees.index');
    }


    /**
     * Remove Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_employees_destroy')) {
            return abort(401);
        }
        $Employee = Employees::findOrFail($id);
        $Employee->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.employees.index');
    }

    /**
     * Activate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_employees_active')) {
            return abort(401);
        }
        // dd('ok');
        $Employee = Employees::findOrFail($id);
        $user = User::findOrFail($Employee->user_id);
        $user->update(['active' => 1]);
        $Employee->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.employees.index');
    }

    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_employees_inactive')) {
            return abort(401);
        }
        $Employee = Employees::findOrFail($id);
        $user = User::findOrFail($Employee->user_id);
        //dd($user);
        $user->update(['active' => 0]);
        $Employee->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.employees.index');
    }

    /**
     * Delete all selected Employee at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_employees_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Employees::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
    /**
     * Inactivate Employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_employee_job_detail($id)
    {

        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $Employee= [];

        $Employee1 = EmployeesJobDetail:: where('employee_id', $id)->first();

        if(count($Employee1)==0)
        {
            $Employee = Employees::findOrFail($id);
        }
       // print_r($Employee1);exit;
        return view('admin.employees.job_detail', compact('Employee','Employee1'));

    }
    public function insert_employee_job_data(StoreUpdateRequest $request,$id)
    {
        //echo 'hiiiiii';exit;
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $data = $request->all();
        $Employee= [];

        $Employee1 = EmployeesJobDetail:: where('employee_id', $id)->first();

        if(count($Employee1)==0)
        {
                $input=[];
                $input['employee_id'] = $id;
                $input['job_title'] = $data['job_title'];
                $input['job_category'] = $data['job_category'];
                $input['department_id'] = $data['department_id'];
                $input['employee_status'] = $data['employee_status'];
                $input['date_of_joining'] = $data['date_of_joining'];
                $input['probation_period'] = $data['probation_period'];
                $input['contract_start_date'] = $data['contract_start_date'];
                $input['branch_id'] = $data['branch_id'];
                $input['region_id'] = $data['region_id'];
                $input['territory_id'] = $data['territory_id'];
                $input['contract_end_date'] = $data['contract_end_date'];
                $input['report_to'] = $data['report_to'];

                $input['created_by'] = Auth::user()->id;
                $input['updated_by'] = Auth::user()->id;

           $result = EmployeesJobDetail::create($input);
            $Employee1 = EmployeesJobDetail:: where('id', $result->id)->first();

        }else
            {
                EmployeesJobDetail::where('employee_id',$id)->update([

                    'job_title' => $data['job_title'],
                    'job_category' => $data['job_category'],
                    'department_id' => $data['department_id'],
                    'employee_status' => $data['employee_status'],
                    'date_of_joining' => $data['date_of_joining'],
                    'probation_period' => $data['probation_period'],
                    'contract_start_date' => $data['contract_start_date'],
                    'branch_id' => $data['branch_id'],
                    'region_id' => $data['region_id'],
                    'territory_id' => $data['territory_id'],
                    'contract_end_date' => $data['contract_end_date'],
                    'report_to' => $data['report_to'],
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id

                ]);
            }



        return view('admin.employees.job_detail', compact('Employee1','Employee'));

    }
    public function update_employee_salary_detail($id)
    {

        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $Employee= [];

        $Employee1 = EmployeesSalaryDetail:: where('employee_id', $id)->first();

        if(count($Employee1)==0)
        {
            $Employee = Employees::findOrFail($id);
        }
        // print_r($Employee1);exit;
        return view('admin.employees.salary_detail', compact('Employee','Employee1'));

    }
    public function update_employee_salary_data(StoreUpdateRequest $request,$id)
    {
        //echo 'hiiiiii';exit;
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $data = $request->all();
        $input =[];
        $Employee= [];
        $Employee1 = EmployeesSalaryDetail:: where('employee_id', $id)->first();

        if(count($Employee1)==0)
        {
            $input['employee_id'] = $id;
            $input['basic_salary'] = $data['basic_salary'];
            $input['account_number'] = $data['account_number'];
            $input['bank_name'] = $data['bank_name'];

            $input['updated_by'] =  Auth::user()->id;
            $input['created_by'] =  Auth::user()->id;
            $result = EmployeesSalaryDetail::create($input);
            $Employee1 = EmployeesSalaryDetail:: where('id', $result->id)->first();
        }else
            {
                EmployeesSalaryDetail::where('employee_id',$id)->update([


                    'basic_salary' => $data['basic_salary'],
                    'account_number' => $data['account_number'],
                    'bank_name' => $data['bank_name'],
                    'updated_by' =>  Auth::user()->id,


                ]);
                $Employee1 = EmployeesSalaryDetail:: where('employee_id', $id)->first();
            }

        //$Employee = Employees::findOrFail($id);
        return view('admin.employees.salary_detail', compact('Employee','Employee1'));

    }
    public function update_employee_experience_detail($id)
    {
        //echo 'hiiiiii';exit;
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $Employee= [];

        $Employee1 = EmployeesExperienceDetail:: where('employee_id', $id)->first();

        if(count($Employee1)==0)
        {
            $Employee = Employees::findOrFail($id);
        }
        // print_r($Employee1);exit;
        return view('admin.employees.experience_detail', compact('Employee','Employee1'));

    }
    public function update_employee_experience_data(StoreUpdateRequest $request,$id)
    {
        //echo 'hiiiiii';exit;
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $data = $request->all();
       // echo '<pre>';print_r($data);exit;
        $Employee= [];
        $input =[];
        $Employee1 = EmployeesExperienceDetail:: where('employee_id', $id)->first();
        if($data['skill_title'] == '')
        {
            $data['skill_title'] == 'NULL';
        }if($data['skills_experiance'] == '')
        {
            $data['skills_experiance'] == 'NULL';
        }
        if(count($Employee1)==0)
        {
            $input['employee_id'] = $id;
            $input['pre_company'] = $data['pre_company'];
            $input['pre_job_title'] = $data['pre_job_title'];
            $input['pre_job_date_from'] = $data['pre_job_date_from'];
            $input['pre_job_date_to'] = $data['pre_job_date_to'];
            $input['skill_title'] = $data['skill_title'];
            $input['skills_experiance'] = $data['skills_experiance'];

            $input['updated_by'] =  Auth::user()->id;
            $input['created_by'] =  Auth::user()->id;
            $result = EmployeesExperienceDetail::create($input);
            $Employee1 = EmployeesExperienceDetail:: where('id', $result->id)->first();
        }
        else
            {
                EmployeesExperienceDetail::where('employee_id',$id)->update([
                    'pre_company' => $data['pre_company'],
                    'pre_job_title' => $data['pre_job_title'],
                    'pre_job_date_from' => $data['pre_job_date_from'],
                    'pre_job_date_to' => $data['pre_job_date_to'],
                    'skill_title' => $data['skill_title'],
                    'skills_experiance' => $data['skills_experiance'],


                    'updated_by' =>  Auth::user()->id

                ]);
            }


        return view('admin.employees.experience_detail', compact('Employee','Employee1'));

    }
    public function update_employee_education_detail($id)
    {
        //echo 'hiiiiii';exit;
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $Employee= [];

        $Employee1 = EmployeesEducationDetail:: where('employee_id', $id)->first();

        if(count($Employee1)==0)
        {
            $Employee = Employees::findOrFail($id);
        }
        // print_r($Employee1);exit;

        return view('admin.employees.education_detail', compact('Employee','Employee1'));

    }
    public function update_employee_education_data(StoreUpdateRequest $request,$id)
    {
        //echo 'hiiiiii';exit;
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $data = $request->all();
        $Employee= [];
        $Employee1 = EmployeesEducationDetail:: where('employee_id', $id)->first();
        $input =[];
        if(count($Employee1)==0)
        {
            $input['employee_id'] = $id;
            $input['edu_level'] = $data['edu_level'];
            $input['edu_institute'] = $data['edu_institute'];
            $input['edu_specialization'] = $data['edu_specialization'];
            $input['edu_year'] = $data['edu_year'];
            $input['edu_score'] = $data['edu_score'];
            $input['edu_start_date'] = $data['edu_start_date'];
            $input['edu_end_date'] = $data['edu_end_date'];


            $input['updated_by'] =  Auth::user()->id;
            $input['created_by'] =  Auth::user()->id;
            $result = EmployeesEducationDetail::create($input);
            $Employee1 = EmployeesEducationDetail:: where('id', $result->id)->first();
        }else
        {
            EmployeesEducationDetail::where('employee_id',$id)->update([
                'edu_level' => $data['edu_level'],
                'edu_institute' => $data['edu_institute'],
                'edu_specialization' => $data['edu_specialization'],
                'edu_year' => $data['edu_year'],
                'edu_score' => $data['edu_score'],
                'edu_start_date' => $data['edu_start_date'],
                'edu_end_date' => $data['edu_end_date'],
                'updated_by' =>  Auth::user()->id

            ]);
        }


        return view('admin.employees.education_detail', compact('Employee','Employee1'));

    }
    public function update_employee_resignation_detail($id)
    {
        //echo 'hiiiiii';exit;
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $Employee= [];

        $Employee1 = EmployeesResignationDetail:: where('employee_id', $id)->first();

        if(count($Employee1)==0)
        {
            $Employee = Employees::findOrFail($id);
        }
        return view('admin.employees.resignation_detail', compact('Employee','Employee1'));

    }
    public function update_employee_resignation_data(StoreUpdateRequest $request,$id)
    {
        //echo 'hiiiiii';exit;
        if (! Gate::allows('erp_employees_edit')) {
            return abort(401);
        }
        $data = $request->all();
        $Employee= [];
        $input=[];
        $Employee1 = EmployeesResignationDetail:: where('employee_id', $id)->first();

        if(count($Employee1)==0)
        {
            $input['employee_id'] = $id;
            $input['date_of_resign'] = $data['date_of_resign'];
            $input['termination_reason'] = $data['termination_reason'];



            $input[ 'created_by'] =  Auth::user()->id;
            $input[ 'updated_by'] =  Auth::user()->id;
            $result = EmployeesResignationDetail::create($input);
            $Employee = Employees:: where('id', $id)->first();
            $Employee->update(['status' => 0]);

            flash('Record has been updated successfully.')->success()->important();

            return redirect()->route('admin.employees.index');
        }else
            {
                EmployeesResignationDetail::where('employee_id',$id)->update([
                    'date_of_resign' => $data['date_of_resign'],
                    'termination_reason' => $data['termination_reason'],
                    'status' => 0,


                    'updated_by' =>  Auth::user()->id

                ]);
                $Employee = Employees:: where('id', $id)->first();
                $Employee->update(['status' => 0]);

                flash('Record has been updated successfully.')->success()->important();

                return redirect()->route('admin.employees.index');
                flash('Record has been updated successfully.')->success()->important();

                return redirect()->route('admin.employees.index');
            }


        return view('admin.employees.resignation_detail', compact('Employee'));

    }
    public function get_branches_ajax(Request $request)
    {

        $input = $request->all();
        $region_id = $input['region_id'];

        $branches =  ((array)\DB::select("SELECT `id`, `name` FROM `branches` WHERE `region_id` = $region_id"));
        $JsonData = array();
        if(count($branches)) {

            foreach ($branches as $branches) {
                $JsonData[] = array(
                    'id' => $branches->id,
                    'name' => $branches->name
                );

            }

        }return response()
        ->json(array(
            'branchesdata' => $JsonData,

        ))->withCallback($request->input('callback'));


        // return $SaleData;




        exit;


    }
    public function get_territory_ajax(Request $request)
    {

        $input = $request->all();

        $branch_id = $input['branch_id'];

        $territory =  ((array)\DB::select("SELECT `id`,`name` FROM `territory` WHERE `branch_id` = $branch_id"));
        $JsonData = array();
        if(count($territory)) {

            foreach ($territory as $territory) {
                $JsonData[] = array(
                    'id' => $territory->id,
                    'name' => $territory->name
                );

            }

        }return response()
        ->json(array(
            'territorydata' => $JsonData,

        ))->withCallback($request->input('callback'));


        // return $SaleData;




        exit;


    }
    public function get_job_title_by_departmen_id_ajax(Request $request)
    {

        $input = $request->all();

        $department_id = $input['department_id'];

        $jobtitle =  ((array)\DB::select("SELECT `id`,`name` FROM `hrm_job_title` WHERE `department_id` = $department_id"));
        $JsonData = array();
        if(count($jobtitle)) {

            foreach ($jobtitle as $jobtitle) {
                $JsonData[] = array(
                    'id' => $jobtitle->id,
                    'name' => $jobtitle->name
                );

            }

        }return response()
        ->json(array(
            'jobtitledata' => $JsonData,

        ))->withCallback($request->input('callback'));


        // return $SaleData;




        exit;


    }

    public function getBranches(StoreUpdateRequest $request){
        if (! Gate::allows('erp_employees_create')) {
            return abort(401);
        }
      $Branches = Branches::pluckRegionBranches($request['item']);
        $response_data = array();
        foreach ($Branches as $key => $val){
            $temp_array['id'] = $key;
            $temp_array['name'] = $val;
            array_push($response_data,$temp_array);
        }
        return json_encode($response_data);
    }

    public function EmpAvailable(){
        $Employees = Employees::OrderBy('created_at','desc')->pluck('first_name', 'user_id');
        $Employees->prepend('Select an Employee', '');

        return view('admin.employees.available', compact('Employees'));
//       dd('ok');
    }
    public function availableStatus(StoreUpdateRequest $request){
        $data = $request->all();
        $end_date = isset($data['to_date']) ? $data['to_date'] : $data['from_date'];
        $leaves = Leaves::employeeLeaves($data['user_id'], $data['from_date'], $end_date);
        $leave_data = array();
        foreach ($leaves as $key => $val){

            $temp_array['emp_name'] = $val->employee->full_name;
            $temp_array['leave_type'] = $val->leave_type->name;
            $temp_array['leave_status'] = $val->leave_status->leave_name;
            $temp_array['leave_date'] = $val->leave_date;
            $temp_array['created_at'] = date('Y-m-d',strtotime($val->created_at));
            array_push($leave_data, $temp_array);
        }
        return json_encode($leave_data);

    }
}
