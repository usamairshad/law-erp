<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\Classes;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\Program;
use Auth;
use Config;
use App\Helpers\Helper;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $class= Classes::all();

        return view('admin.class.index',compact('class'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.class.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   


        $this->validate($request, [
            'name' => 'required | unique:classes',
            'description' => 'required',
        ]);
       $input= $request->all();
        for($i = 0; $i < count($input['name']) ; $i++) { 
            $arr= new Classes;
            $arr->name=$input['name'][$i];
            $arr->description=$input['description'][$i];
            $arr->save();
        }
         $class= Classes::all();

        return view('admin.class.index',compact('class'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classes=Classes::find($id);
        return view('admin.class.edit',compact('classes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);
        Classes::where('id',$request->class_id)->update([
            'name'=>$request->name,
            'description'=>$request->description
        ]);

        return redirect()->route('admin.classes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Classes::find($id)->delete();
        return redirect()->route('admin.classes.index');
    }
    public function assignBoardProgramClass()
    {
        $programs= Program::all();
        $boards= Board::all();
        $classes= Classes::all();
        
        return view('admin.class.assign-board-program-class',compact('programs','boards','classes'));
    }
    public function boardProgramClassStore(Request $request)
    {


        $input= $request->all();
        foreach ($input['class_id'] as $key => $value) {
            BoardProgramClass::create([
                'class_id'=>$value,
                'board_id'=>$input['board_id'],
                'program_id'=>$input['program_id']
            ]);
        }
       
        return redirect()->route('admin.view-board-program-class');
    }
}
