<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\SuppliersModel;
use App\Models\Admin\StockCategoryModel;
use App\Models\Admin\Groups;
use App\Models\Admin\Ledgers;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreSuppliersRequest;
use App\Http\Requests\Admin\UpdateSuppliersRequest;
use App\Helpers\CoreAccounts;
use Auth;
use Config;


class SuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Suppliers = SuppliersModel::all();
        return view('admin.suppliers.index', compact('Suppliers'));
    }

    public function datatables()
        {
            $Suppliers = SuppliersModel::all();
            return  datatables()->of($Suppliers)
                ->addColumn('sup_type', function($Suppliers) {
                     $variable = $Suppliers->sup_type;
                    if ($variable == 2) {
                        return $type ="Import";
                    }else{
                        return $type ="Local";
                    }
                })->addColumn('action', function ($Suppliers) {
                    $action_column = '<a href="suppliers/'.$Suppliers->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                    $action_column .= '<form method="post" action="suppliers/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
                    $action_column .= '<input type="hidden" value="21" name="products_name" >';
                    $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
                    $action_column .= '<input type="hidden" value='.$Suppliers->id.' name="name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                    return $action_column;
                })->rawColumns(['sup_type','action'])
                ->toJson();
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_suppliers_manage')) {
            return abort(401);
        }
        return view('admin.suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreSuppliersRequest $request)
    {
        if (! Gate::allows('erp_suppliers_manage')) {
            return abort(401);
        }
       $StockCategory = StockCategoryModel::pluckActiveOnly()->toarray();
       $product_array = Config::get('admin.product_function_array');
       //dd($product_array);
       $suplierid = SuppliersModel::create([
            'sup_name' => $request['sup_name'],
            'sup_address' => $request['sup_address'],
            'sup_country' => $request['sup_country'],
            'sup_city' => $request['sup_city'],
            'sup_contact' => $request['sup_contact'],
            'sup_email' => $request['sup_email'],
            'sup_mobile' => $request['sup_mobile'],
            'sup_website' => $request['sup_website'],
            'sup_type' => $request['sup_type'],
            'sup_bank_account' => $request['sup_bank_account'],
            'created_by' => Auth::user()->id,
        ]);//product_function_array
        if($request['sup_type']==2){
            $Suppliers_stock = CoreAccounts::getConfigGroup(Config::get('constants.accounts_stock_inventory'));
            $Suppliers_Sales = CoreAccounts::getConfigGroup(Config::get('constants.accounts_sale_revenue'));
            $Suppliers_COGS  = CoreAccounts::getConfigGroup(Config::get('constants.accounts_cost_sale'));
            if(count($Suppliers_stock)>0 && count($Suppliers_Sales)>0 && count($Suppliers_COGS)>0){
                $parent_head = array(
                    1 => $Suppliers_stock['group']->id,
                    2 => $Suppliers_Sales['group']->id,
                    3 => $Suppliers_COGS['group']->id,
                );
            }
           $parent_type = $suplierid->id;
            //$parent_type = 152;
            $responses = CoreAccounts::createCustomGroup($parent_head,$product_array,$StockCategory,$request['sup_name'],$parent_type);
            $Supl_tt_advance = CoreAccounts::getConfigGroup(Config::get('constants.accounts_tt_advance'));
            $Group = array(
                'name' => 'TT/Advance (Control Account)- '.$request['sup_name'],
                'parent_id' => $Supl_tt_advance['group']->id,
                'parent_type' => $parent_type,
            );
            CoreAccounts::createGroup($Group);
            $Sup_BME_importer = CoreAccounts::getConfigGroup(Config::get('constants.accounts_BME_importer'));
            $Group = array(
                'name' => 'Imported (Other Than L/C)'.$request['sup_name'],
                'parent_id' => $Sup_BME_importer['group']->id,
                'parent_type' => $parent_type,
            );
            CoreAccounts::createGroup($Group);
        }else{
            $Suppliers_local = CoreAccounts::getConfigGroup(Config::get('constants.accounts_cd_local'));
            $Ledgerstoc = array(
                'name' => $request['sup_name'],
                'group_id' => $Suppliers_local['group']->id,
                'balance_type' => 'c',
                'opening_balance' => 0,
                'parent_type' => $suplierid->id,
            );
            CoreAccounts::createLedger($Ledgerstoc);
        }
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.suppliers.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Supplier = SuppliersModel::find($id);
        return view('admin.suppliers.home', compact('Supplier'));

        // show the view and pass the nerd to it
        //return View::make('admin.suppliers.home')->with($Suppliers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_suppliers_manage')) {
            return abort(401);
        }
        $Suppliers = SuppliersModel::findOrFail($id);
        return view('admin.suppliers.edit', compact('Suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_suppliers_manage')) {
            return abort(401);
        }
        ///dd($request->all());
        SuppliersModel::where('id',$id)->update([
            'sup_name' => $request['sup_name'],
            'sup_address' => $request['sup_address'],
            'sup_country' => $request['sup_country'],
            'sup_city' => $request['sup_city'],
            'sup_contact' => $request['sup_contact'],
            'sup_email' => $request['sup_email'],
            'sup_mobile' => $request['sup_mobile'],
            'sup_website' => $request['sup_website'],
            'sup_type' => $request['sup_type'],
            'sup_bank_account' => $request['sup_bank_account'],
            'updated_by' => Auth::user()->id,
        ]);
        if($request['sup_type']==2){
            Groups::where('parent_type',$id)->where('parent_id',Config::get('constants.accounts_stock_inventory'))->update([
                'name' => 'Stock-'.$request['sup_name'],
                'updated_by' => Auth::user()->id,
            ]);
            Groups::where('parent_type',$id)->where('parent_id',Config::get('constants.accounts_sale_revenue'))->update([
                'name' => 'Sales-'.$request['sup_name'],
                'updated_by' => Auth::user()->id,
            ]);
            Groups::where('parent_type',$id)->where('parent_id',Config::get('constants.accounts_cost_sale'))->update([
                'name' => 'COS-'.$request['sup_name'],
                'updated_by' => Auth::user()->id,
            ]);
            Groups::where('parent_type',$id)->where('parent_id',Config::get('constants.accounts_BME_importer'))->update([
                'name' => 'Imported (Other Than L/C)'.$request['sup_name'],
                'updated_by' => Auth::user()->id,
            ]);
            Groups::where('parent_type',$id)->where('parent_id',Config::get('constants.accounts_tt_advance'))->update([
                'name' => 'TT/Advance (Control Account)- '.$request['sup_name'],
                'updated_by' => Auth::user()->id,
            ]);
        }else{
            Ledgers::where('parent_type',$id)->where('group_id',Config::get('constants.accounts_cd_local'))->update([
                'name' => $request['sup_name'],
                'updated_by' => Auth::user()->id,
            ]);
        }

        return redirect()->route('admin.suppliers.index');
    }

    /**
     * Activate Suppliers from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_suppliers_active')) {
            return abort(401);
        }

        $Suppliers = SuppliersModel::findOrFail($id);
        $Suppliers->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.suppliers.index');
    }

    /**
     * Inactivate Suppliers from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_suppliers_inactive')) {
            return abort(401);
        }

        $Suppliers = SuppliersModel::findOrFail($id);
        $Suppliers->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.suppliers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function delete(Request $request)
    {
        if (! Gate::allows('erp_suppliers_manage')) {
            return abort(401);
        }
        $data = $request->all();
        $Suppliers = SuppliersModel::findOrFail($data['name']);
        $Suppliers->delete();
        flash('Record has been deleted successfully.')->success()->important();
        return redirect()->route('admin.suppliers.index');
    }


    /**
     * Delete all selected Entry at once.
     *
     * @param Request $request
     */
}
