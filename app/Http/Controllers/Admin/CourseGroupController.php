<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\ProgramGroup;
use App\Models\Academics\Section;
use App\Models\Academics\Program;
use App\Models\Academics\AssignClassSection;
use App\Models\Academics\Classes;
use App\Models\Academics\CourseGroup;
use Auth;
use Config;
use App\Helpers\Helper;

class CourseGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cg= CourseGroup::all();
        return view('admin.course-group.course-group-index',compact('cg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boards= Board::all();
        return view('admin.course-group.course-group-create',compact('boards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $bp= BoardProgram::where('board_id',$input['board_id'])->where('program_id',$input['program_id'])->first();
        $pg= ProgramGroup::where('board_program_id',$bp->id)->first();
        $board_name = $bp->board->name;
        $program_name = $bp->program->name;
        $name = $pg->name."-".$input['name'];
        
        CourseGroup::create([
                'program_group_id' =>$pg->id,
                'name' => $name
        ]);

        return redirect()->route('admin.course-group.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function loadBoardProgramProgramgroup(Request $request)
    {
        $bg_id=BoardProgram::where('board_id',$request->board_id)->where('program_id',$request->program_id)->value('id');
        $pg_id= ProgramGroup::where('board_program_id',$bg_id)->get();

        \Log::info($pg_id);
        return $pg_id;
    }

    public function loadProgramGroupCourseGroup(Request $request)
    {
        $cg= CourseGroup::where('program_group_id',$request->program_group_id)->get();
        return $cg;
    }
}
