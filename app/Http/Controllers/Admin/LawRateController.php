<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TaxSettings\StoreUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\LawDetails;
use App\Models\Laws;
use App\Models\Admin\TaxSettings;
use Auth;

class LawRateController extends Controller
{
    /**
     * Display a listing of Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // if (! Gate::allows('erp_tax_settings_manage')) {
        //     return abort(401);
        // }


        $Law = LawDetails::join('erp_laws','erp_laws.law_id','=','erp_law_rates.law_id')->OrderBy('erp_law_rates.created_at','desc')->get();

        return view('admin.lawrate.index', compact('Law'));
    }

    /**
     * Show the form for creating new Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $laws = Laws::get();
        return view('admin.lawrate.create',compact('laws'));
    }

    /**
     * Store a newly created Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreUpdateTaxSettings  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
        $input= $request->all();
      //  for($i = 0; $i < count($input['name']) ; $i++) { 
            $arr= new LawDetails;
            $arr->law_id=$input['name'];
            $arr->type=$input['filer'];
            $arr->percentage=$input['per'];
            $arr->amount_limit=$input['amount'];
            $arr->status=$input['status'];
            $arr->save();
        //}
    
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('tax-law-rate');
    }


    /**
     * Show the form for editing Branche.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $laws = Laws::get();
        $Branche = LawDetails::where('law_rate_id',$id)->first();

        return view('admin.lawrate.edit', compact('Branche','laws'));
    }

    /**
     * Update Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateBrancheRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
$input = $request;
$id = $input->id;
        $Branche = LawDetails::where('law_rate_id',$id);
       
        $Branche->update([
            'law_id'=> $input->name,
            'type'=> $input->filer,
           'percentage'=> $input->per,
            'amount_limit'=> $input->amount,
            'status'=>$input['status']
           // 'updated_by' => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('tax-law-rate');
    }


    /**
     * Remove Branche from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        

        $Branche = LawDetails::where('law_rate_id',$id);
        $Branche->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('tax-law-rate');
    }

}
