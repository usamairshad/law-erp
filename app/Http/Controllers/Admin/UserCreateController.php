<?php

namespace App\Http\Controllers\Admin;

use App\Roles as Erprole;
use App\RoleUser;
use App\Models\Admin\Role as AcademicsRole;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserCreateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = User::with('branchuser','roleuser')->get();
        foreach ($records as $key => $value) {

            $role_id= $value['role_id'];

            if($value['user_type'] == 'erp')
            {
                $d=Role::find($role_id);
                $records[$key]['n']=$d->name;
            }
            elseif ($value['user_type'] == 'academics') {
                  $d= AcademicsRole::find($role_id);
                  $records[$key]['n']=$d->name;
            }
            else
            {

            } 
       
        }

        return view('admin.usercreate.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        
       if($request->role_type == 'erp')
       {
            $id=$request->id;
            $role_name= Role::where('id',$request->role_id)->value('name');
            $password = Hash::make($request->password);
            if ($id==0 || $id==''){
                $this->validate($request,[
                    'branch_id' => 'required',
                    'role_id' => 'required',
                    'name' => 'required',
                    'email' => 'required',
                    'password' => 'required'
                ]);
                $return_id = User::create(['branch_id'=>$request->branch_id, 'role_id'=>$request->role_id,'user_type'=>$request->role_type, 'name'=>$request->name, 'email'=>$request->email, 'password'=>$password,'active'=>1]);
                $user_id = $return_id->id;
                $return_id->assignRole($role_name);
                return redirect()->back()->with('success','User has been updated');
            }
        }
        elseif ($request->role_type == 'academics') {
            $id=$request->id;
            $role_name= AcademicsRole::where('id',$request->role_id)->value('name');
            $password = Hash::make($request->password);
            if ($id==0 || $id=='')
            {
                $this->validate($request,[
                    'branch_id' => 'required',
                    'role_id' => 'required',
                    'name' => 'required',
                    'email' => 'required',
                    'password' => 'required'
                ]);
                $return_id = User::create(['branch_id'=>$request->branch_id, 'role_id'=>$request->role_id,'user_type'=>$request->role_type, 'name'=>$request->name, 'email'=>$request->email, 'password'=>$password,'active'=>1]);
                $user_id = $return_id->id;
                $return_id->assignRole($role_name);

                RoleUser::create(['user_id'=>$user_id, 'role_id'=>$request->role_id]);
            }
       
            return redirect()->back()->with('success','User has been created');
        }
        else{

            $this->validate($request,[
                'branch_id' => 'required',
                'role_id' => 'required',
                'name' => 'required',
                'email' => 'required',
                'password' => 'required'
            ]);
            User::where('id',$id)->update(['branch_id'=>$request->branch_id, 'user_type'=>$request->role_type, 'role_id'=>$request->role_id,
                'name'=>$request->name, 'email'=>$request->email, 'password'=>$request->password]);
            RoleUser::where('id',$id)->update(['user_id'=>$id,'role_id'=>$request->role_id]);
            return redirect()->back()->with('success','User has been updated');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user= User::find($id);
        $roles=Role::whereIn('name',['Ceo','administrator'])->pluck('name','id');
        
        return view('admin.usercreate.edit', compact('roles','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
 
            $this->validate($request,[
                'branch_id' => 'required',
                'role_id' => 'required',
                'role_type' => 'required',
                'name' => 'required',
                'email' => 'required',
                'password' => 'required'
            ]);
        $password=Hash::make($request->password);
        $role_name= Role::where('id',$request->role_id)->value('name');
  
        $return_id=  User::where('id',$id)->update(['branch_id'=>$request->branch_id, 'user_type'=>$request->role_type, 'role_id'=>$request->role_id,
                'name'=>$request->name, 'email'=>$request->email, 'password'=>$password]);
        $user_data= User::where('id',$id)->first();

       if($request->role_type == 'erp')
       {
            $user_data->assignRole($role_name);
       }
       elseif ($request->role_type == 'academics') {
           RoleUser::where('id',$id)->update(['user_id'=>$id,'role_id'=>$request->role_id]);
       }
       return redirect()->route('create-user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->delete();
        RoleUser::where('user_id',$id)->delete();
        return redirect()->back()->with('delete','User has been deleted');
    }
}
