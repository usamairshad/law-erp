<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin\DutyModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\ComercialInvoiceModel;
use App\Models\Admin\LcBankModel;
use App\Models\Admin\PerformaStockModel;
use App\Models\Admin\Orders;
use App\Models\Admin\OrderDetailModel;
use App\Models\Admin\DolerLcModel;
use App\Models\Admin\Ledgers;
use App\Models\Admin\LcDutyModel;
use App\Models\Admin\Groups;
use App\Helpers\CoreAccounts;
use Auth;
use Config;
use DB;
class ComercialInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ComercialInvoiceModel = ComercialInvoiceModel::all();
        return view('admin.cinvoice.index',compact('ComercialInvoiceModel'));
    }
    public function datatables()
    {
//        $lcdutydata=LcDutyModel::all();
//        $viewid= LcDutyModel::where('id',$lcdutydata->ledger_id)->get();
        $ComercialInvoiceModel = ComercialInvoiceModel::all();
        return datatables()->of($ComercialInvoiceModel)
            ->addColumn('atp', function ($ComercialInvoiceModel) {
                return date('d-m-Y', strtotime($ComercialInvoiceModel->atp));
            })
            ->addColumn('etd', function ($ComercialInvoiceModel) {
                return date('d-m-Y', strtotime($ComercialInvoiceModel->etd));
            })
            ->addColumn('eta', function ($ComercialInvoiceModel) {
                return date('d-m-Y', strtotime($ComercialInvoiceModel->eta));
            })
            ->addColumn('lcno', function ($ComercialInvoiceModel) {
                return $ComercialInvoiceModel->lCmodel->lc_no;
            }) ->addColumn('dollar_rate', function ($ComercialInvoiceModel) {
                return $ComercialInvoiceModel->today_doller;
            })->addColumn('action', function ($ComercialInvoiceModel) {
//                $action_column = '<a href="cinvoice/costing/'.$ComercialInvoiceModel->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Costing</a>';
//                $action_column .= '<a href="cinvoice/viewduty/' . $ComercialInvoiceModel->id . '" class="btn btn-xs btn-info"><i class="glyphicon glyphicon"></i> View Duty</a>';
                  $lcdutydata = LcDutyModel::where('ledgerLc_id',$ComercialInvoiceModel->lcno )->get();
                 // print_r($lcdutydata);exit;
                if (count($lcdutydata)>0) {
                    $action_column = '<a href="cinvoice/costing/' . $ComercialInvoiceModel->id . '" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Costing</a>';
                    $action_column .= '<a href="cinvoice/viewduty/' . $ComercialInvoiceModel->id . '" class="btn btn-xs btn-info"><i class="glyphicon glyphicon"></i> View Duty</a>';
                    $action_column .= '<a href="cinvoice/'.$ComercialInvoiceModel->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> DO Rate</a>';
                } else {
                    $action_column = '<a href="cinvoice/costing/' . $ComercialInvoiceModel->id . '" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Costing</a>';
                    $action_column .= '<a href="cinvoice/'.$ComercialInvoiceModel->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> DO Rate</a>';
                }
                
                    
                
                return $action_column;
            })->rawColumns(['atp', 'etd', 'eta', 'lcno', 'dollar_rate', 'action'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $LcBankModel = LcBankModel::where('lc_status',1)->get();
        return view('admin.cinvoice.create',compact('LcBankModel'));
    }

    public function getprpforma($id)
    {
        $LcBankModel = LcBankModel::where('id',$id)->first();
        $proforma = explode(',',$LcBankModel->pf_no);
        return view('admin.cinvoice.productlist',compact('proforma','LcBankModel'));
    }

    public function viewduty(ComercialInvoiceModel $ComercialInvoiceModel)
    {
        $result = LcDutyModel::where('Comercial_id',$ComercialInvoiceModel->id)->get();
        return view('admin.cinvoice.viewduty', compact('result'));
    }

    public function lccosting(ComercialInvoiceModel $ComercialInvoiceModel)
    {
       // dd($ComercialInvoiceModel);
        $stock_product = array();
        $LcBankModel = LcBankModel::where('id',$ComercialInvoiceModel->lcno)->first();
        $result = LcDutyModel::getDuties($ComercialInvoiceModel->lcno,$ComercialInvoiceModel->id);
        $result_I_Bnk = LcDutyModel::getInsuranceDuties($ComercialInvoiceModel->lcno);
            if($LcBankModel->transaction_type == 1){
                $doller_rate = $ComercialInvoiceModel->today_doller;
                $totalLCamount = ($doller_rate * $ComercialInvoiceModel->usd_amt);
            }else{
                $doller_rate = $ComercialInvoiceModel->dollar_rate;
                if($LcBankModel->lc_type == 2){
                    $DolerLcModel = DolerLcModel::selectRaw('COUNT(id) AS count,SUM(rate) AS amount')
                        ->where('lc_id',$LcBankModel->id)
                        ->groupBy('lc_id')->first();
                    $total_fluc_rate = $DolerLcModel->amount + $doller_rate;
                    $number_fluc_rate = $DolerLcModel->count;
                    if($total_fluc_rate!= '' && $number_fluc_rate !=''){
                        $avg_price = round($total_fluc_rate/$number_fluc_rate);
                    }else{
                        $avg_price = $doller_rate;
                    }
                    $totalLCamount = ($avg_price * $ComercialInvoiceModel->usd_amt);
                }else{
                    $totalLCamount = ($doller_rate * $ComercialInvoiceModel->usd_amt);
                }

            }
        $Performastock = PerformaStockModel::where('ci_id',$ComercialInvoiceModel->id)->get();
        if($result){
            foreach ($Performastock as $pstock){
                $ptotal_qty = $pstock['total_qty'];
               // $order_qty = OrderDetailModel::getOrderQty($pstock['peroforma_no'],$pstock['product_name']);
                $total_qty_insu = round(($result_I_Bnk['insurance']* $pstock['amount_rs'])/($LcBankModel->lc_amt*$doller_rate));
                //$per_unit_insu =  round($total_qty_insu/$order_qty,4);
                //$pinsurance =  round($per_unit_insu*$ptotal_qty,4);
                $pinsurance =  $total_qty_insu;
                //$pinsurance = round(($result['insurance']*$pstock["amount_rs"])/$totalLCamount,4);
                $pbank_charges = round(($result_I_Bnk['bank_charges']*$pstock["amount_rs"])/($LcBankModel->lc_amt*$doller_rate));
                $pfreight = round(($result['freight']*$pstock["amount_rs"])/$totalLCamount);
                $pprofit_investment = round(($result['profit_invest']*$pstock["amount_rs"])/$totalLCamount);
                $murahbaProfit = round(($result['murahaba_profit']*$pstock["amount_rs"])/$totalLCamount);
                $pmovement = round(($result['mvmnt']*$pstock["amount_rs"])/$totalLCamount);
                $pduty = round(($result['duty']*$pstock["amount_rs"])/$totalLCamount);
                $paf_total_amt = round($pstock["amount_rs"]+$pinsurance+$pbank_charges+$pfreight+$pprofit_investment+$murahbaProfit+$pmovement+$pduty);
                $cost_per_unit = round($paf_total_amt/$ptotal_qty);
                //$balance_lc = round($pbalance_qty*$cost_per_unit);
                $stock_product['insurance'] = $pinsurance;
                $stock_product['bank_charges'] = $pbank_charges;
                $stock_product['freight'] = $pfreight;
                $stock_product['profit_investment'] = $pprofit_investment;
                //changes made by shahryar
//                $stock_product['murahba_profit'] = $pprofit_investment;
                $stock_product['murahba_profit'] = $murahbaProfit;
                $stock_product['mvmnt_bndg'] = $pmovement;
                $stock_product['Duty'] = $pduty;
                $stock_product['f_total_amount'] = $paf_total_amt;
                $stock_product['costing_u_price'] = $cost_per_unit;
                PerformaStockModel::where('id',$pstock->id)->update($stock_product);
            }
        }
        $Performastock = PerformaStockModel::where('ci_id',$ComercialInvoiceModel->id)->get();
        //return redirect()->to('admin/cinvoice/costing/'.$ComercialInvoiceModel->id);
//       dd($murahbaProfit);
        return view('admin.lcbank.costing', compact('Performastock','ComercialInvoiceModel','doller_rate'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     //dd($request->all());
        $dolerrate = LcBankModel::where('id', $request['lc_no'])->first();
        $rat = $request['today_doller'];
        if ($dolerrate->transaction_type == '1') {
            $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_Lc_Transit')])->where(['parent_type' => $dolerrate->bnk_id])->first();
            $Assets_id = $TR_ledger->id;
            $transit_ledger = Ledgers::where('parent_type',$request['lc_no'])->where('group_id',$Assets_id)->first();
            $laibilities_Group = Groups::where(['parent_id' => Config::get('constants.account_bank_cd_importer')])->where(['parent_type' => $dolerrate->bnk_id])->first();
            $laibilities_ledger = Ledgers::where('parent_type',$request['lc_no'])->where('group_id',$laibilities_Group->id)->first();
            $calculated_margin_party = round($request['total_of_product'] * $rat);
            $calculated_margin_transit = round($request['total_of_product'] * $rat);
            $cr_id = $laibilities_ledger->id;
        }else{
            if($dolerrate->lc_type == 2){
                $DolerLcModel = DolerLcModel::selectRaw('COUNT(id) AS count,SUM(rate) AS amount')
                    ->where('lc_id',$request['lc_no'])
                    ->groupBy('lc_id')->first();
                $total_fluc_rate = $DolerLcModel->amount + $rat;
                $number_fluc_rate = $DolerLcModel->count;
                if($total_fluc_rate!= '' && $number_fluc_rate !=''){
                    $avg_price = round($total_fluc_rate/$number_fluc_rate);
                }else{
                    $avg_price = $rat;
                }
                $TR_ledger = Groups::where(['parent_id' => Config::get('constants.accounts_tt_advance')])->where(['parent_type' => $dolerrate->bnk_id])->first();
                $transit_ledger = Ledgers::where('parent_type',$request['lc_no'])->where('group_id',$TR_ledger->id)->first();
                $laibilities_Group = Groups::where(['parent_id' => Config::get('constants.accounts_BME_importer')])->where(['parent_type' => $dolerrate->bnk_id])->first();
                $laibilities_ledger = Ledgers::where('parent_type',$request['lc_no'])->where('group_id',$laibilities_Group->id)->first();
                $calculated_margin_party = $request['total_of_product'];
                $calculated_margin_transit = round($request['total_of_product'] * $avg_price);
                $cr_id = $laibilities_ledger->id;
            }else{
                $TR_ledger = Groups::where(['parent_id' => Config::get('constants.account_bank_contract')])->where(['parent_type' => $dolerrate->bnk_id])->first();
                $transit_ledger = Ledgers::where('parent_type',$request['lc_no'])->where('group_id',$TR_ledger->id)->first();
                $calculated_margin_party = ($request['total_of_product'] * $rat);
                $calculated_margin_transit = ($request['total_of_product'] * $rat);
                $cr_id = Config::get('constants.admin_accounts_cash');
            }
        }
         $comid = ComercialInvoiceModel::create([
                'ci_no' => $request['ci_no'],
                'atp' => $request['atp'],
                'etd' => $request['etd'],
                'eta' => $request['eta'],
                'lcno' => $request['lc_no'],
                'mos' => $request['mos'],
                'usd_amt' => $request['total_of_product'],
                'today_doller' => $request['today_doller'],
                'lc_expiry_date' => $request['lc_expiry_date'],
                'total_days' => $request['total_days'],
                'document_date' => $request['document_date'],
                'document_received' => $request['document_received'],
                'bl_no' => $request['bl_no'],
                'container_status' => $request['container_status'],
                'margin_rat' => $request['margin_rate'],
                'murahba_profit_rate' => $request['mu_profit'],
                'status' => 1,
                'created_by' => Auth::user()->id,
        ]);
        foreach($request['line_items'] as $keys ) {
            foreach ($keys['product_id'] as $key => $prod) {
                if($keys['rel_qty'][$key] == null || $keys['rel_qty'][$key] == 0){
                    continue;
                }
                $totalamt = ($keys['unit_price'][$key] * $keys['rel_qty'][$key]);
                $amount_rs = round($totalamt * $rat);
                $calculated_margin = round($amount_rs * $request['margin_rate'] / 100);
                $total_murahba = $amount_rs - $calculated_margin;
                $murahba_profit = round(((($total_murahba*$request['mu_profit'])/100)/365)*$request['total_days']);
                $net_murahba = $total_murahba + $murahba_profit;
                $cost_do_unit = round($net_murahba / $keys['rel_qty'][$key]);
                PerformaStockModel::create([
                    'ci_id' => $comid->id,
                    'peroforma_no' => $keys['pfno'],
                    'product_name' => $keys['product_id'][$key],
                    'total_qty' => $keys['rel_qty'][$key],
                    'balanace_qty' => $keys['rel_qty'][$key],
                    'amount' => $keys['unit_price'][$key],
                    'total_amount' => $totalamt,
                    'amount_rs' => $amount_rs,
                    'cost_per_unit' => round(($keys['unit_price'][$key] * $rat)),
                    'costing_u_price' => round(($keys['unit_price'][$key] * $rat)),
                    'costing_do_price' => $cost_do_unit,
                ]);
                OrderDetailModel::where('id', $keys['detail_id'][$key])->update([
                    'remaining_qty' => ($keys['total_qty'][$key] - $keys['rel_qty'][$key]),
                ]);
            }
            $itemscount = OrderDetailModel::where('perchas_id', $keys['pfno'])->sum('remaining_qty');
            if ($itemscount == 0) {
                Orders::where('id', $keys['pfno'])->update([
                    'status' => 3,
                ]);
            }
        }
        $Orders_status = Orders::where(['id' => json_decode($request['lcorder'],true)])->where(['status' => '2'])->count();
        if($Orders_status == 0){
            $LcBankModel = LcBankModel::where('id', $request['lc_no'])->update([
                'lc_status' => 2,
            ]);
        }
        $total_difference = round($calculated_margin_transit - $calculated_margin_party);
        $entriessale = array(
            "number" => "??????",
            "voucher_date" =>  date('Y-m-d'),
            "employee_id" => Auth::user()->id,
            "narration" => "Record the liability to the Party for the Purchase",
            "entry_type_id" => "1",
            "entry_items" => array(
                1 => [
                    'ledger_id' => $transit_ledger->id,
                    'dr_amount' => $calculated_margin_transit,
                    'cr_amount' => 0,
                    'narration' => 'Import Purchases booked',
                ],
                2 => [
                    'ledger_id' =>$cr_id,
                    'dr_amount' => 0,
                    'cr_amount' => $calculated_margin_party,
                    'narration' => 'Record the liability to the Party for the Purchase',
                ],
            ),
            "diff_total" => $total_difference,
            "dr_total" => $calculated_margin_transit,
            "cr_total" =>$calculated_margin_party,
        );
        CoreAccounts::createJEntry($entriessale);
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.cinvoice.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Performastock = array();
        $count = 0;
        $ComercialInvoiceModel = ComercialInvoiceModel::find($id);
        $LcBankModel = LcBankModel::where('id',$ComercialInvoiceModel->lcno)->first();
        $morahba_profit = $ComercialInvoiceModel->murahba_profit_rate;
        $margin_rate = $ComercialInvoiceModel->margin_rat;
        $Ordersproduct = PerformaStockModel::where('ci_id',$id)->get();
        if(count($Ordersproduct) > 0){
            foreach ($Ordersproduct as $Ordersproducts){
                $product_name = $Ordersproducts['product_name'];
                $product_qty = $Ordersproducts['total_qty'];
                $remain_qty = $Ordersproducts['balanace_qty'];
                $product_amount = $Ordersproducts['amount'];
                $total_amount = round($product_qty *$product_amount);
                $amount_rs = round($total_amount*$ComercialInvoiceModel->today_doller);
                $calculated_margin = round($amount_rs * $margin_rate / 100);
                $total_murahba = $amount_rs - $calculated_margin;
                //$murahba_profit = round(($morahba_profit*$amount_rs) / $total_lc_amount);
                $murahba_profit = round(((($total_murahba*$morahba_profit)/100)/365)*$ComercialInvoiceModel->total_days);
                $net_murahba = $total_murahba + $murahba_profit;
                $cost_per_unit = round($net_murahba / $product_qty);
                $balance_murahba = round($remain_qty * $cost_per_unit);
                $Performastock[$count]['product_name'] = $Ordersproducts->productsModel->products_short_name. ' - '. $Ordersproducts->productsModel->item_code;
                $Performastock[$count]['total_qty'] = $product_qty;
                $Performastock[$count]['bal_qty'] = $remain_qty;
                $Performastock[$count]['amount'] = $product_amount;
                $Performastock[$count]['total_amount'] = $total_amount;
                $Performastock[$count]['amount_rs'] = $amount_rs;
                $Performastock[$count]['margin'] = $calculated_margin;
                $Performastock[$count]['total_murahba'] = $total_murahba;
                $Performastock[$count]['murahba_profit'] = $murahba_profit;
                $Performastock[$count]['net_murahba'] = $net_murahba;
                $Performastock[$count]['cost_per_unit'] = $cost_per_unit;
                $Performastock[$count]['balance_murahba'] = $balance_murahba;
                $count++;
            }
        }
        return view('admin.lcbank.cindex', compact('Performastock','LcBankModel','ComercialInvoiceModel'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
//        dd($request->all());
        $lc = LcBankModel::all();
        $cm = ComercialInvoiceModel::find($id);
//        dd($request);
        $cm->update([
            'close_date' => $request['close_date'],
        ]);
        return redirect()->back();
        // return view('admin.lcbank.grimport', compact('comid','cm','lc'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function active($id)
    {
//        if (! Gate::allows('erp_customers_active')) {
//            return abort(401);
//        }

        $dutyid = LcDutyModel::findOrFail($id);

//        $ledgerid = LcDutyModel::where('ledger_id',$id)->pluck('ledger_id');
        $dutyid->update(['status' => 1]);


        flash('Record has been activated successfully.')->success()->important();

       return redirect()->back();
    }
    public function inactive($id)
    {
//        if (! Gate::allows('erp_customers_inactive')) {
//            return abort(401);
//        }

        $dutyid = LcDutyModel::findOrFail($id);
//        $ledgerid = LcDutyModel::where('ledger_id',$id)->first();

        $dutyid->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();
        return redirect()->back();
    }
}
