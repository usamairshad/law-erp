<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SalesInvoice\StoreUpdateRequest;
use App\Http\Controllers\Controller;
use App\Models\Admin\Branches;
use App\Models\HRM\Employees;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Admin\SalesInvoiceDetailModel;
use App\Models\Admin\Customers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Marketing\PipelineProducts;
use App\Models\Marketing\PipelineServices;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\InvoiceDetailsModel;
use App\Models\Admin\SalesInvoiceService;
use App\Models\Admin\StockItemsModel;
use App\Models\Marketing\SalePipelines;
use App\Models\Admin\Ledgers;
use App\Models\Admin\OrderProductSerial;
use App\Helpers\CoreAccounts;
use App\Models\Admin\DealerModel;

use App\Models\Admin\InventoryTransferDetailModel;
use App\Models\Admin\InventoryTransferModel;

use Auth;
use PDF;
use DB;
use Config;

class SalesInvoiceController extends Controller
{
    public function index(){

        if (! Gate::allows('erp_invoice_manage')) {
            return abort(401);
        }
        $SalesInvoiceDetailModel = SalesInvoiceDetailModel::all();

        return view('admin.invoices.index' ,compact('SalesInvoiceDetailModel'));

    }
    public function datatables()
    {
//        dd('no ok');
        $SalesInvoiceMode = SalesInvoiceDetailModel::where('status',1)->get();
        return  datatables()->of($SalesInvoiceMode)
            ->addColumn('invoice_date', function($SalesInvoiceMode) {
                return  date('d-m-Y  ', strtotime($SalesInvoiceMode->invoice_date));
            })
            ->addColumn('customer_name', function($SalesInvoiceMode) {
                if($SalesInvoiceMode->invoice_to == 'customer'){
                    return $SalesInvoiceMode->CustomersModel->name;
                }
                if($SalesInvoiceMode->invoice_to == 'branch'){
                    return $SalesInvoiceMode->BranchName->name;
                }
                if($SalesInvoiceMode->invoice_to == 'dealer'){
                    return $SalesInvoiceMode->DealerName->name;
                }

            })
            ->addColumn('due_date', function($SalesInvoiceMode) {
                return  date('d-m-Y  ', strtotime($SalesInvoiceMode->due_date));
            })
            ->addColumn('confirm_status', function($SalesInvoiceMode) {
                if($SalesInvoiceMode->confirm_status==0){
                    return 'Not Confirmed';
                }else if($SalesInvoiceMode->confirm_status==1){
                    return '<b> Confirmed </b>';
                }
                return $SalesInvoiceMode->CustomersModel->name;

            })
            ->addColumn('created_by', function($SalesInvoiceMode) {
//dd($SalesInvoiceMode->createdBy->first_name);
                return $SalesInvoiceMode->createdBy->name;

            })
            ->addColumn('action', function ($SalesInvoiceMode) {

//                $action_column .= '<form method="post" action="saleinvoice/delete" >';
//                $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
//                $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
//                $action_column .= '<input type="hidden" value="21" name="perfomra_no" >';
//                $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
//                $action_column .= '<input type="hidden" value='.$SalesInvoiceMode->id.' name="name" >';
//                $action_column .=  csrf_field();
//                $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                // $action_column .= '<a href="saleinvoice/downloadPDF/'.$SalesInvoiceMode->id.'" target="_blank" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i>Download <u>P</u>DF</a>';
                $action_column = ' <a href="salesinvoice/view/'.$SalesInvoiceMode->id.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i>view</a>';
                if($SalesInvoiceMode->confirm_status == 0){
                    $action_column .= '<a href="salesinvoice/'.$SalesInvoiceMode->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                    $action_column .= '<form method="post" action="salesinvoice/confirm" >';
                    $action_column .= csrf_field();
                    $action_column .= '<input type="hidden" value=' . $SalesInvoiceMode->id . ' name="invoice" >';
                    $action_column .= '<input   value="Confirm"  class="btn btn-xs btn-success" type="submit"  onclick="return confirm(\'Are you sure to Confirm invoice? \')"> </form>';
                }


                if(Gate::check('sales_destroy')){

                        $action_column .= '<form method="post" action="databanks/delete" >';
                        $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                        $action_column .= '<input type="hidden" value='.$SalesInvoiceMode->id.' name="name" >';
                        $action_column .=  csrf_field();
                        $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';

                }
                return $action_column;
            })->rawColumns(['customer_name','company','grand_total','confirm_status','created_by','action'])
            ->toJson();
    }
    public function view($id){

        if (! Gate::allows('erp_invoice_detail')) {
            return abort(401);
        }
        // dd($id);

        $SalesInvoiceMode = SalesInvoiceDetailModel::where('id',$id)->first();
        $credit_limit = Customers::where('databank_id', $SalesInvoiceMode->customer_id)->pluck('credit_limit')->first();
        $customer_credit = SalesInvoiceDetailModel::customerTotalCredit($SalesInvoiceMode->customer_id);
        if(!$customer_credit){
            $customer_credit=0;
        }

        $Products = InvoiceDetailsModel::where('salesinvoice_id',$SalesInvoiceMode->id)->get();
        $Services = SalesInvoiceService::where('salesinvoice_id',$SalesInvoiceMode->id)->get();

       return view('admin.saleseinvoice.saleinvoicedisplay', compact('SalesInvoiceMode','Products','Services','credit_limit','customer_credit'));
    }

    public function confirm(Request $request){

        if (! Gate::allows('erp_invoice_confirm')) {
            return abort(401);
        }
        //dd($request->all());

        $logged_in =  Auth::user()->id;
        $Cost_entry = array();
        $Sale_entry = array();
        $counter = 1;
        $count = 1;
        $totalsale_price = 0;
        $totalsaless = 0;
        $totalCost = 0;
        $finalAmountdr = 0;
        $finalAmountcr = 0;
        $totalDsicount = 0;
        $data = $request->all();
        $salesInvoiceDetailModel = SalesInvoiceDetailModel::where(['id' => $data['invoice'], 'confirm_status' => '0'])->first();
        if($salesInvoiceDetailModel){
            $totalDsicount = $salesInvoiceDetailModel->overall_discount + $salesInvoiceDetailModel->total_discount;
        }
        if( $salesInvoiceDetailModel->invoice_to == 'customer' ){
            //$customer_id = Customers::where('id', $salesInvoiceDetailModel->customer_id)->pluck('id')->first();
            $TR_ledger =  Ledgers::where(['group_id' => Config::get('constants.accounts_debt_customer')])->where(['parent_type' => $salesInvoiceDetailModel->customer_id])->first();
            $customer_ledger_id = $TR_ledger->id;
        }elseif($salesInvoiceDetailModel->invoice_to == 'branch'){
            $TR_ledger =  Ledgers::where(['group_id' => Config::get('constants.accounts_debt_branch')])->where(['parent_type' => $salesInvoiceDetailModel->customer_id])->first();
            $customer_ledger_id = $TR_ledger->id;
        }else{
            $TR_ledger =  Ledgers::where(['group_id' => Config::get('constants.accounts_debt_dealer')])->where(['parent_type' => $salesInvoiceDetailModel->customer_id])->first();
            $customer_ledger_id = $TR_ledger->id;
        }
        $message = '';
        $non_stock_flag = false;
        $transfer = null;
        DB::beginTransaction();
        if( $salesInvoiceDetailModel->invoice_to == 'branch' ){
            // transfer inventory only to relevant branch. No account enteries
            $transfer_model = array(
                'branch_from' => '1',
                'branch_to' => $salesInvoiceDetailModel->customer_id,
                'sup_id' => '1'
            );
            $transfer = InventoryTransferModel::create($transfer_model);
        }

//        dd($salesInvoiceDetailModel);
      $order_product_serial_array =  array();

        if($salesInvoiceDetailModel){
            $Products = InvoiceDetailsModel::where('salesinvoice_id',$salesInvoiceDetailModel->id)->get();
            foreach ($Products as $key => $value){

                $serial_nos = explode(',' , $value->serialNo);
                if(count($serial_nos) == $value->quantity){
                    foreach ($serial_nos as $key => $val){
                        //dd($val);
                        $temp_array['order_id'] = $salesInvoiceDetailModel->id;
                        $temp_array['product_id'] = $value->product_id;
                        $temp_array['serial_no'] = $val;
                        $temp_array['created_by'] = $logged_in;

                        $temp_array['installation_date'] = $salesInvoiceDetailModel->installation_date;
                        $temp_array['warrenty'] = $salesInvoiceDetailModel->warrenty;
                        $temp_array['expires_at'] = $salesInvoiceDetailModel->expires_at;

                        $temp_array['updated_by'] = $logged_in;
                        $temp_array['created_at'] = date('Y-m-d');
                        $temp_array['updated_at'] = date('Y-m-d');

                        $exists = OrderProductSerial::where(['serial_no' => $val])->first();

                        if($exists){
                            flash('Erorr!. Duplicate Serial Number')->error()->important();
                            return redirect()->route('admin.salesinvoice.index');
                        }
                        array_push($order_product_serial_array , $temp_array);
                    }
                }else{

                    flash('Serial Numbers are  incorrectly entered')->error()->important();
                    return redirect()->route('admin.salesinvoice.index');

                }

            }

            OrderProductSerial::insert($order_product_serial_array);
//            dd($Products, $order_product_serial_array);
            //dd($Products);

            if(count($Products) > 0){
                // Transaction has been started

                foreach ($Products as $product){
                $stockItem = StockItemsModel::where(['st_name' => $product->product_id ])->where('closing_stock','<>',0)->first();
                $line_cost_price = 0;
                $line_sale_price = 0;
                  if($stockItem['closing_stock']){
                    $remain_qty = $product->quantity;
                    $stock_quantity = $stockItem['closing_stock'];
                      $temp_cost = 0;
                    while ($remain_qty != 0 ){

                        if($stock_quantity){
                            if( $stock_quantity> $remain_qty){
                                // update line item with subtracted qty
//                                $update_stock['closing_stock'] = $stock_quantity - $product->quantity;
                                $line_cost_price += $stockItem['st_unit_price'] * $remain_qty;
                                $update_stock['closing_stock'] = $stock_quantity - $remain_qty;
                                $stockItem->update($update_stock);
                                $remain_qty = 0;

                            }else{
                                // FIF logic goes here
//                                echo 'Rem1 : '. $remain_qty;
                                $remain_qty = $product->quantity -  $stock_quantity;
                                $line_cost_price += $stockItem['st_unit_price'] * $product->quantity;
                                $update_stock['closing_stock'] = 0;
                                $stockItem->update($update_stock);
                                $stockItem = StockItemsModel::where(['st_name' => $product->product_id ])->where('closing_stock','<>',0)->first();
//                                echo 'Rem : '. $remain_qty. ' - '.$product->quantity;
//                                dd($stockItem);
                                $stock_quantity = $stockItem['closing_stock'];
                            }
                        }else{
                            $non_stock_flag = true;
                            $message = 'Invoice Cannot be confirmed, Some items are missing from the stock';
                        }
                    }
                  }else{
                      $non_stock_flag = true;
                      $message = 'Invoice Cannot be confirmed, Some items are missing from the stock';
                  }

                  if( $salesInvoiceDetailModel->invoice_to == 'branch' ){

                      // transfer inventory only to relevant branch. No account enteries
                      if($transfer){

                      $transfer_detail_model = array(
                          'i_transfer_id' => $salesInvoiceDetailModel->id,
                          'product_id' => $product->product_id,
                          'transfer_qty' => $product->quantity,
                          );

                      $transfer_detail = InventoryTransferDetailModel::create($transfer_detail_model);

                      }

                  }
                    //cost
                    $product_Cost_ledger = ProductsModel::getCostGroup($product->product_id);
                    $Cost_entry[$count]['ledger_id'] = $product_Cost_ledger;
                    $Cost_entry[$count]['dr_amount'] = $line_cost_price;
                    $Cost_entry[$count]['cr_amount'] = 0;
                    $Cost_entry[$count]['narration'] = 'Sales has been done';
                    $count++;
                    //assets
                    $product_Stock_ledger = ProductsModel::getStockGroup($product->product_id);
                    $Cost_entry[$count]['ledger_id'] = $product_Stock_ledger;
                    $Cost_entry[$count]['dr_amount'] = 0;
                    $Cost_entry[$count]['cr_amount'] = $line_cost_price;
                    $Cost_entry[$count]['narration'] = 'Sales has been done';
                    $count++;
                    //sale
                    $product_Sale_ledger = ProductsModel::getSaleGroup($product->product_id);
                    $Sale_entry[$counter]['ledger_id'] = $product_Sale_ledger;
                    $Sale_entry[$counter]['dr_amount'] = 0;
                    $Sale_entry[$counter]['cr_amount'] = $product->line_total;
                    $Sale_entry[$counter]['narration'] = 'Sales has been done';
                    $counter++;
                    $totalCost += $line_cost_price;
                    $totalsaless += $product->line_total;

                }
                if($salesInvoiceDetailModel->total_tax !='' && $salesInvoiceDetailModel->total_tax >0){
                    $vencounter = count($Sale_entry);
                    $vencounter = $vencounter+1;
                    $Sale_entry[$vencounter]['ledger_id'] =  Config::get('constants.admin_accounts_SalesTax');
                    $Sale_entry[$vencounter]['dr_amount'] =  0;
                    $Sale_entry[$vencounter]['cr_amount'] = $salesInvoiceDetailModel->total_tax;
                    $Sale_entry[$vencounter]['narration'] = 'Tax on Sales';
                }
                if($totalDsicount !='' && $totalDsicount >0){
                    $odencounter = count($Sale_entry);
                    $odencounter = $odencounter+1;
                    $Sale_entry[$odencounter]['ledger_id'] =  Config::get('constants.admin_accounts_discount');
                    $Sale_entry[$odencounter]['dr_amount'] = $totalDsicount;
                    $Sale_entry[$odencounter]['cr_amount'] = 0;
                    $Sale_entry[$odencounter]['narration'] = 'Discount on Sales';
                }
                if($salesInvoiceDetailModel->service_amount !='' && $salesInvoiceDetailModel->service_amount >0){
                    $serivcecounter = count($Sale_entry);
                    $serivcecounter = $serivcecounter+1;
                    $Sale_entry[$serivcecounter]['ledger_id'] =  Config::get('constants.admin_accounts_service_amount');
                    $Sale_entry[$serivcecounter]['dr_amount'] = 0;
                    $Sale_entry[$serivcecounter]['cr_amount'] = $salesInvoiceDetailModel->service_amount;
                    $Sale_entry[$serivcecounter]['narration'] = 'Service contract has been done';
                }
                if($customer_ledger_id){
                    $cencounter = count($Sale_entry);
                    $cencounter = $cencounter+1;
                    $Sale_entry[$cencounter]['ledger_id'] = $customer_ledger_id;
                    $Sale_entry[$cencounter]['dr_amount'] = $salesInvoiceDetailModel->payable_amount;
                    $Sale_entry[$cencounter]['cr_amount'] = 0;
                    $Sale_entry[$cencounter]['narration'] = 'Sales has been done';
                }
                $finalAmountdr = $salesInvoiceDetailModel->payable_amount + $totalDsicount;
                $finalAmountcr = $totalsaless + $salesInvoiceDetailModel->total_tax;
                $entriessale = array(
                    "number" => "??????",
                    "voucher_date" =>  date('Y-m-d'),
                    "employee_id" => Auth::user()->id,
                    "narration" => "Sales has been done",
                    "entry_type_id" => "1",
                    "entry_items" =>$Cost_entry,
                    "diff_total" => "0",
                    "dr_total" => $totalCost,
                    "cr_total" => $totalCost,
                    "cheque_no" => null,
                    "cheque_date" => date('Y-m-d'),
                    "invoice_no" => null,
                    "invoice_date" => date('Y-m-d'),
                );
                CoreAccounts::createJEntry($entriessale);
                $entries_Sale = array(
                    "number" => "??????",
                    "voucher_date" =>  date('Y-m-d'),
                    "employee_id" => Auth::user()->id,
                    "narration" => "Sales has been done",
                    "entry_type_id" => "1",
                    "entry_items" =>$Sale_entry,
                    "diff_total" => "0",
                    "dr_total" => $finalAmountdr,
                    "cr_total" => $finalAmountcr,
                    "cheque_no" => null,
                    "invoice_no" => null,
                    "invoice_date" => date('Y-m-d'),
                );
                CoreAccounts::createJEntry($entries_Sale);

                if($non_stock_flag){
                    flash($message)->error()->important();
                    return redirect()->route('admin.salesinvoice.index');
                }

                $invoice_data['confirm_status'] =1;
                SalePipelines::where('id',$salesInvoiceDetailModel->order_id)->update(['confirm_status' => 3]);
                $salesInvoiceDetailModel->update($invoice_data);

            }
            if($message){
                flash($message)->warning()->important();
            }else{
                flash('Stock Updated Succesfully')->success()->important();
            }

        }else{
            flash('Invoice Already Confirmed')->error()->important();
        }
        DB::commit();
        // Transaction Successfull

        return redirect()->route('admin.salesinvoice.index');

    }

    public function downloadPDF($id){

        if (! Gate::allows('invoice_pdf')) {
            return abort(401);
        }
// dd($id);
        $SalesInvoiceMode=SalesInvoiceDetailModel::where('id',$id)->first();
        $Products=InvoiceDetailsModel::where('salesinvoice_id',$SalesInvoiceMode->id)->get();
//        dd($SalesInvoiceMode);
//        dd($Products);

        $Services = SalesInvoiceService::where('salesinvoice_id',$SalesInvoiceMode->id)->get();
        $test = 1000025.05;

        //$digit = new \NumberFormatter( 'en_US', \NumberFormatter::SPELLOUT );


//        $pdf = PDF::loadView('admin.saleseinvoice.pdf_saleseinvoice',compact( 'SalesInvoiceMode','Products','Services','digit'));
        $pdf = PDF::loadView('admin.saleseinvoice.newpdf_invoice',compact( 'SalesInvoiceMode','Products','Services','digit'));
        return $pdf->stream('invoice.pdf');

    }
    public function create(){

        if (! Gate::allows('erp_invoice_create')) {
            return abort(401);
        }

        // Get all sales persons // from engineering and marketing
        $Employees = Employees::whereIn('department_id',[3,4])->pluck('first_name','user_id');
        $Employees->prepend('Select a sales person' , '');
        // get all branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a branch' , '');

        // get all customers
        $Customers = Customers::pluckActiveOnly();
        // dd($Customers);
        $Customers->prepend('Select a customer' , '');
        // get all dealers
        $Dealers = DealerModel::pluck('name', 'id');
        $Dealers->prepend('Select a dealer' , '');

        return view('admin.invoices.create' ,compact( 'Branches', 'Customers','Dealers','Employees'));

    }

    public function getOrderDetail(Request $request){

        if (! Gate::allows('erp_sales_manage')) {
            return abort(401);
        }

        $data = $request->all();
        dd($data);
        $order_id = explode('-', $data['order_no']);

        $Sales = SalePipelines::findOrFail($order_id);
        //return response()->json($Sales);
        $products = PipelineProducts::getProductsByOrderId($order_id);
//        dd($products);
        $services = PipelineServices::lineItemsByProspect($order_id);
        $response_data['data'] = Array();
        $response_data['gross_amount'] = 0;
        $count = 0;
        $response_data['tax_amount'] = 0;
        $response_data['tax_percent'] = 0;
        $products_array =  array();

        foreach($products as $product){
//            dd($product);
//            $productName=    ProductsModel::getDescriptionByProductId($product->product_id)->products_name.' ('.ProductsModel::getDescriptionByProductId($product->product_id)->products_description.')';
            $product['p_name'] = $product->products_name->products_short_name;
                array_push($products_array,$product);
            $response_data['tax_amount'] += $product->tax_amount;
            $response_data['tax_percent'] = $product->tax_percent;

        }

        $response_data['order_id'] = $order_id;
        $response_data['products_array'] = $products_array;
        $response_data['services'] = $services;

        $response_data['sales'] = $Sales[0];
        return response()->json($response_data);
    }

    public function store(StoreUpdateRequest $request){
        if (! Gate::allows('erp_invoice_create')) {
            return abort(401);
        }

        $data = $request->all();

        if($data['invoice_to'] == 'customer'){

            $data['customer_id'] = $data['customer_id'];

        }elseif ( $data['invoice_to'] == 'branch' ){

            $data['customer_id'] = $data['branch_id'];

        }elseif($data['invoice_to'] == 'dealer'){

            $data['customer_id'] = $data['dealer_id'];
        }

        $data['remaining_amount'] = $data['payable_amount'];
        $data['paid_amount'] = 0;
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $data['invoice_no'] = 'SLE-'.date("y").date("m").'-'.substr(time(), 0, 5);

       /* There can be only one confirmed sale invoice.
        Means that for one sale order we may have multiple unconfirmed sales invoice..
        once any of the unconfirmed sales invoices has been confirmed no other sale invoice (confirmed or unconfirmed) can be generated from that sale order.
        If any item needs to be added in invoice, new sale order will be created and so does the new sale invoice.
        In case of decreasing quantity it will be done through sale return module. */

        DB::beginTransaction();
        $salesinvoice = SalesInvoiceDetailModel::create($data);
        $unique_string = "00000";
        $pre = substr($unique_string, 0, 5- strlen($salesinvoice->id) );
        $invoice_no['invoice_no'] = 'SLE-'.date("y").date("m").'-'.$pre.$salesinvoice->id;
        $salesinvoice->update($invoice_no);

        $insert_data =  array();
        $line_items_array_start = $data['line_items']['product_id'];
        foreach ($line_items_array_start as $key => $val){

            $invoiceData['salesinvoice_id'] = $salesinvoice->id;
            $invoiceData['type'] = $data['line_items']['type'][$key];
            $invoiceData['product_id'] = $data['line_items']['product_id'][$key];
            $invoiceData['quantity'] = $data['line_items']['product_qty'][$key];
            $invoiceData['return_qty'] = $data['line_items']['product_qty'][$key];
            $invoiceData['print_name'] = $data['line_items']['print_name'][$key];
            $invoiceData['total_count'] = $data['line_items']['no_of_print'][$key];
            $invoiceData['serialNo'] = $data['line_items']['serial_no'][$key];
            $invoiceData['unit_price'] = $data['line_items']['unit_price'][$key];
            $invoiceData['discount'] = $data['line_items']['discount_val'][$key];
            $invoiceData['discount_type'] = $data['line_items']['disc_type'][$key];
            $invoiceData['tax_percent'] = $data['line_items']['tax_percent'][$key];
            $invoiceData['tax_amount'] = $data['line_items']['tax_amount'][$key];
            $invoiceData['discount_amount'] = $data['line_items']['disc_amount'][$key];
            $invoiceData['sale_price'] = $data['line_items']['sale_price'][$key];
            $invoiceData['total_price'] = $data['line_items']['total_price'][$key];
            $invoiceData['line_total'] = $data['line_items']['line_total'][$key];
            $invoiceData['misc_amount'] = $data['line_items']['misc_amount'][$key];
            $invoiceData['net_income'] = $data['line_items']['net_income'][$key];
            array_push($insert_data , $invoiceData);

        }

        InvoiceDetailsModel::insert($insert_data);
        DB::commit();
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.salesinvoice.index');
    }


    public function edit($id){

//        if (! Gate::allows('invoice_create')) {
//            return abort(401);
//        }
//dd('edit');
        $SalesInvoiceDetailModel = SalesInvoiceDetailModel::findOrFail($id);
        $InvoiceDetailsModel = InvoiceDetailsModel::where('salesinvoice_id', $id)->get();
        if($SalesInvoiceDetailModel->invoice_to == 'branch'){
            $SalesInvoiceDetailModel->branch_id = $SalesInvoiceDetailModel->customer_id;
        }

        if( $SalesInvoiceDetailModel->invoice_to == 'dealer'){
            $SalesInvoiceDetailModel->dealer_id == $SalesInvoiceDetailModel->customer_id;
        }

//        dd($InvoiceDetailsModel);
        // Get all sales persons //s from engineering and marketing
        $Employees = Employees::whereIn('department_id',[3,4])->pluck('first_name','user_id');
        $Employees->prepend('Select a sales person' , '');
        // get all branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a branch' , '');
        // get all customers
        $Customers = Customers::pluckActiveOnly();
        $Customers->prepend('Select a customer' , '');
        // get all dealers
        $Dealers = DealerModel::pluck('name', 'id');
        $Dealers->prepend('Select a dealer' , '');
        $Products = ProductsModel::pluckActiveOnly();
        return view('admin.invoices.edit', compact('SalesInvoiceDetailModel','InvoiceDetailsModel', 'Products','Employees','Branches','Customers','Dealers'));
    }

    public function update_invoice(StoreUpdateRequest $request, $id)
    {

        $SalesInvoiceDetailModel = SalesInvoiceDetailModel::findOrFail($id);
//        dd($SalesInvoiceDetailModel);
        $data = $request->all();

        if($data['invoice_to'] == 'customer'){

            $data['customer_id'] = $data['customer_id'];

        }elseif ( $data['invoice_to'] == 'branch' ){

            $data['customer_id'] = $data['branch_id'];

        }elseif($data['invoice_to'] == 'dealer'){

            $data['customer_id'] = $data['dealer_id'];
        }

//        $data['remaining_amount'] = $data['payable_amount'];
//        $data['paid_amount'] = 0;

        $data['updated_by'] = Auth::user()->id;
//        $data['invoice_no'] = 'SLE-'.date("y").date("m").'-'.substr(time(), 0, 5);

        /* There can be only one confirmed sale invoice.
         Means that for one sale order we may have multiple unconfirmed sales invoice..
         once any of the unconfirmed sales invoices has been confirmed no other sale invoice (confirmed or unconfirmed) can be generated from that sale order.
         If any item needs to be added in invoice, new sale order will be created and so does the new sale invoice.
         In case of decreasing quantity it will be done through sale return module. */

        DB::beginTransaction();

        $SalesInvoiceDetailModel->update($data);

        InvoiceDetailsModel::where('salesinvoice_id', $id)->delete();

        $insert_data =  array();
        $line_items_array_start = $data['line_items']['product_id'];
        foreach ($line_items_array_start as $key => $val){

            $invoiceData['salesinvoice_id'] = $id;
            $invoiceData['type'] = $data['line_items']['type'][$key];
            $invoiceData['product_id'] = $data['line_items']['product_id'][$key];
            $invoiceData['quantity'] = $data['line_items']['product_qty'][$key];
            $invoiceData['print_name'] = $data['line_items']['print_name'][$key];
            $invoiceData['total_count'] = $data['line_items']['no_of_print'][$key];
            $invoiceData['serialNo'] = $data['line_items']['serial_no'][$key];
            $invoiceData['unit_price'] = $data['line_items']['unit_price'][$key];
            $invoiceData['discount'] = $data['line_items']['discount_val'][$key];
            $invoiceData['discount_type'] = $data['line_items']['disc_type'][$key];
            $invoiceData['tax_percent'] = $data['line_items']['tax_percent'][$key];
            $invoiceData['tax_amount'] = $data['line_items']['tax_amount'][$key];
            $invoiceData['discount_amount'] = $data['line_items']['disc_amount'][$key];
            $invoiceData['sale_price'] = $data['line_items']['sale_price'][$key];
            $invoiceData['total_price'] = $data['line_items']['total_price'][$key];
            $invoiceData['line_total'] = $data['line_items']['line_total'][$key];
            $invoiceData['misc_amount'] = $data['line_items']['misc_amount'][$key];
            $invoiceData['net_income'] = $data['line_items']['net_income'][$key];
            array_push($insert_data , $invoiceData);

        }

        InvoiceDetailsModel::insert($insert_data);
        DB::commit();
        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.salesinvoice.index');

//
    }


}
