<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\City;
use App\Models\Admin\CountryModel;
use App\Models\Admin\Regions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreTerritoryRequest;
use App\Http\Requests\Admin\UpdateTerritoryRequest;
use App\Models\Admin\Territory;
use Auth;

class TerritoryController extends Controller
{
    /**
     * Display a listing of Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_territory_manage')) {
            return abort(401);
        }

        $Territory = Territory::OrderBy('created_at','desc')->get();

        return view('admin.territory.index', compact('Territory'));
    }

    /**
     * Show the form for creating new Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_territory_create')) {
            return abort(401);
        }
        $city = City::all()->pluck('city_name','id');
        $region = Regions::all()->pluck('name','id');
        $country = CountryModel::all()->pluck('country_name','id');
        return view('admin.territory.create',compact('city','region','country'));
    }

    /**
     * Store a newly created Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreTerritoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTerritoryRequest $request)
    {
        if (! Gate::allows('erp_territory_create')) {
            return abort(401);
        }

        Territory::create([
            'name' => $request['name'],
            'city_id' => $request['city_id'],
            'region_id' => $request['region_id'],
            'country_id' => $request['country_id'],
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.territory.index');
    }


    /**
     * Show the form for editing Branche.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_territory_edit')) {
            return abort(401);
        }
        $territory = Territory::findOrFail($id);
        $city = City::all()->pluck('city_name','id');
        $region = Regions::all()->pluck('name','id');
        $country = CountryModel::all()->pluck('country_name','id');

        return view('admin.territory.edit', compact('territory','city','region','country'));
    }

    /**
     * Update Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateBrancheRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTerritoryRequest $request, $id)
    {
        if (! Gate::allows('erp_territory_edit')) {
            return abort(401);
        }

        $territory = Territory::findOrFail($id);
        $territory->update([
            'name' => $request['name'],
            'city_id' => $request['city_id'],
            'region_id' => $request['region_id'],
            'country_id' => $request['country_id'],
            'updated_by' => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.territory.index');
    }


    /**
     * Remove Branche from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_territory_destroy')) {
            return abort(401);
        }

        $territory = Territory::findOrFail($id);
        $territory->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.territory.index');
    }

}
