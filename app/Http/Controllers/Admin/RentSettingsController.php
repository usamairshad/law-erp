<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TaxSettings\StoreUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;


use App\Models\Admin\TaxSettings;
use Auth;

class RentSettingsController extends Controller
{
    /**
     * Display a listing of Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // if (! Gate::allows('erp_tax_settings_manage')) {
        //     return abort(401);
        // }


        $TaxSettings = TaxSettings::where('status', '1' )->where('law_id', 2)->where('tax_type','1')->OrderBy('created_at','desc')->get();

        return view('admin.rent_settings.index', compact('TaxSettings'));
    }

    /**
     * Show the form for creating new Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $TaxSettings = TaxSettings::where('status', '1' )->where('law_id', 2)->where('tax_type','1')->OrderBy('created_at','desc')->get();
        return view('admin.rent_settings.create', compact('TaxSettings'));
    }

    /**
     * Store a newly created Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreUpdateTaxSettings  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        
        $data = $request->all();

        $TaxSettings =  TaxSettings::where('law_id', 2)->where('tax_type' , '1')->update(array('status' => 0, 'updated_by' => Auth::user()->id));
        
        //echo '<pre>';print_r($data); exit;
        $tax_lineitems=array();
        $line_items_array_start = $data['line_items']['tax_class'];
   
        foreach ($line_items_array_start as $key => $val) {
            if($key != '######'){
               

                $temp_data = array(
                        'tax_class' => $data['line_items']['tax_class'][$key],
                        'tax_type' => '1',
                        'fix_amount' => $data['line_items']['fix_amount'][$key],
                        'tax_percent' => $data['line_items']['tax_percent'][$key],
                        'start_range' => $data['line_items']['start_range'][$key],
                        'end_range' => $data['line_items']['end_range'][$key],
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id,
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                        'law_id'=>2
                    );

                    array_push($tax_lineitems, $temp_data);
            }
    
        }
       // echo '<pre>';print_r($tax_lineitems); exit;

        TaxSettings::insert($tax_lineitems);
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('rental-tax-settings');
    }


    /**
     * Show the form for editing Branche.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $Branche = TaxSettings::findOrFail($id);

        return view('admin.tax_settings.edit', compact('Branche'));
    }

    /**
     * Update Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateBrancheRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaxSettingsRequest $request, $id)
    {
        
        
        $Branche = TaxSettings::findOrFail($id);
        $Branche->update([
            'name' => $request['name'],
            'updated_by' => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.tax_settings.index');
    }


    /**
     * Remove Branche from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        

        $Branche = TaxSettings::findOrFail($id);
        $Branche->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.tax_settings.index');
    }

}
