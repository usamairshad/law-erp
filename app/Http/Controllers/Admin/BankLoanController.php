<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\BankLoan;
use App\Models\Admin\BanksModel;
use Auth;

class BankLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bank_loans = BankLoan::OrderBy('created_at','desc')->get();

        return view('admin.bank_loans.index', compact('bank_loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bank = BanksModel::all()->pluck('bank_name','id');
        
        return view('admin.bank_loans.create', compact('bank'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       
       $this->validate($request, [
            'bank_id' => 'required',
            'amount' => 'required',
            'installment_plan' => 'required',
            'deduction_each_month' => 'required',
            'kabis' => 'required',
            'bank_intrest' => 'required', 
        ]);

        $markup = (int)$request->kabis + (int)$request->bank_intrest;

        BankLoan::create([
            'bank_id'          => $request['bank_id'],
            'amount'     => $request['amount'],
            'markup'     => $markup,
            'installment_plan'     => $request['installment_plan'],
            'deduction_each_month'     => $request['deduction_each_month'],
            'kabis'     => $request['kabis'],
            'bank_intrest' => $request['bank_intrest'],
            'created_by'    => Auth::user()->id
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.bank_loan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank_loans = BankLoan::findOrFail($id);
         $bank = BanksModel::all()->pluck('bank_name','id');
        return view('admin.bank_loans.edit', compact('bank_loans','bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         
       $this->validate($request, [
            'bank_id' => 'required',
            'amount' => 'required',
            'installment_plan' => 'required',
            'deduction_each_month' => 'required',
            'kabis' => 'required',
            'bank_intrest' => 'required', 
        ]);

        $markup = (int)$request->kabis + (int)$request->bank_intrest;

            $bank_loans = BankLoan::findOrFail($id);
            $bank_loans->update([
            
            'bank_id'           => $request['bank_id'],
            'amount'            => $request['amount'],
            'markup'            => $markup,
            'installment_plan'     => $request['installment_plan'],
            'deduction_each_month'     => $request['deduction_each_month'],
            'kabis'             => $request['kabis'],
            'bank_intrest'      => $request['bank_intrest'],
            'updated_by'        => Auth::user()->id
            ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.bank_loan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank_loans = BankLoan::findOrFail($id);
        $bank_loans->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.bank_loan.index');   
    }
}
