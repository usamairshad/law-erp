<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CoreAccounts;
use App\Http\Requests\Admin\SalesReturn\StoreUpdateRequest;
use App\Http\Controllers\Controller;
use App\Models\Admin\Ledgers;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Admin\SalesInvoiceDetailModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin\InvoiceDetailsModel;
use App\Models\Admin\StockItemsModel;
use App\Models\Admin\SaleReturn;
use App\Models\Admin\SaleReturnDetail;
use App\Models\Admin\ProductsModel;

use Auth;
use PDF;
use DB;
use Config;

class SalesReturnController extends Controller
{
    public function index(){

        if (! Gate::allows('erp_sales_return_manage')) {
            return abort(401);
        }


        $SaleReturn = SaleReturn::all();
//dd($SalesInvoiceDetailModel);
        return view('admin.salesreturn.index' ,compact('SaleReturn'));

    }
    public function datatables()
    {
        $SaleReturn = SaleReturn::where('status',1)->get();
        return  datatables()->of($SaleReturn)

            ->addColumn('invoice_id', function($SaleReturn) {
               // return $SaleReturn->invoice->invoice_no;
               $text = '<a target="_blank" href="salesinvoice/view/'.$SaleReturn->invoice_id.'" > '. $SaleReturn->invoice->invoice_no .'</a>';
               // $text = '<a href="databanks/details/1" > abc</a>';
                return $text;

            })
            ->addColumn('description', function($SaleReturn) {
                return $SaleReturn->description ? $SaleReturn->description : 'N/A' ;
            })
            ->addColumn('return_date', function($SaleReturn) {
                return  date('d-m-Y', strtotime($SaleReturn->return_date));
            })
            ->addColumn('created_by', function($SaleReturn) {
                //return $SaleReturn->returned_by->first_name;
                return 'NA';
            })
            ->addColumn('action', function ($SaleReturn) {
                $action_column = ' <a target="_blank" href="salesreturn/view/'.$SaleReturn->id.'" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-edit"></i>view</a>';
                return $action_column;
            })->rawColumns(['customer_name','invoice_id','return_date','confirm_status','action'])
            ->toJson();
    }
    public function view($id){

        $SaleReturn = SaleReturn::where('id',$id)->first();
        $SaleReturnDetail = SaleReturnDetail::where(['sale_return_id' => $id,'status' => 1 ])->get();
        $SalesInvoiceDetailModel = SalesInvoiceDetailModel::where('id', $SaleReturn->invoice_id)->first();

       return view('admin.salesreturn.salereturndetail', compact('SaleReturn','SaleReturnDetail','SalesInvoiceDetailModel'));
    }

    public function confirm(Request $request){
        $data = $request->all();

        $salesInvoiceDetailModel = SalesInvoiceDetailModel::where(['id' => $data['invoice'], 'confirm_status' => '0'])->first();

        $message = '';
        if($salesInvoiceDetailModel){
            $Products=InvoiceDetailsModel::where('salesinvoice_id',$salesInvoiceDetailModel->id)->get();

            if(count($Products) > 0){

                // Transaction has been started
                DB::beginTransaction();
                foreach ($Products as $product){
                  $stock_quantity = StockItemsModel::where(['st_name' => $product->product_id])->pluck('quantity')->first();
                  //echo $stock_quantity.'<br>';
                  if($stock_quantity){
                      $update_stock['quantity'] = $stock_quantity - $product->quantity;
                      StockItemsModel::where(['st_name' => $product->product_id])->update($update_stock);
                  }else{
                      $message = 'Stock has been Updated. Some items are not in stock';
                  }

                }
                $invoice_data['confirm_status'] = 1;
                $salesInvoiceDetailModel->update($invoice_data);
                DB::commit();
            }
            if($message){
                flash($message)->warning()->important();
            }else{
                flash('Stock Updated Succesfully')->success()->important();
            }

        }else{
            flash('Invoice Already Confirmed')->error()->important();
        }
        // Transaction Successfull




        return redirect()->route('admin.salesinvoice.index');

    }

    public function downloadPDF($id){
        $SalesInvoiceMode=SalesInvoiceDetailModel::where('id',$id)->first();
        $Products=InvoiceDetailsModel::where('salesinvoice_id',$SalesInvoiceMode->id)->get();
        $Services = SalesInvoiceService::where('salesinvoice_id',$SalesInvoiceMode->id)->get();
        $test = 1000025.05;

        $digit = new \NumberFormatter( 'en_US', \NumberFormatter::SPELLOUT  );

        $pdf = PDF::loadView('admin.salesreturn.pdf_salesreturn',compact( 'SalesInvoiceMode','Products','Services','digit'));
        return $pdf->stream('invoice.pdf');

    }
    public function create(){

        if (! Gate::allows('erp_sales_return_create')) {
            return abort(401);
        }
        $invoices = SalesInvoiceDetailModel::where('confirm_status', '1')->get();
        //dd($invoices);
        // $invoices->prepend('Select Invoice' ,'');
        return view('admin.salesreturn.create' , compact('invoices'));

    }

    public function getOrderDetail(Request $request){

        if (! Gate::allows('erp_sales_manage')) {
            return abort(401);
        }

        $data = $request->all();
        $invoice_id = explode('-', $data['invoice_id']);
        $SalesInvoiceMode = SalesInvoiceDetailModel::where(['id' => $invoice_id,'confirm_status'=>1])->first();
        $Products=InvoiceDetailsModel::where('salesinvoice_id',$SalesInvoiceMode->id)->get();
        //$Services = SalesInvoiceService::where('salesinvoice_id',$SalesInvoiceMode->id)->get();
        $products_array =  array();

        foreach($Products as $product){

            $product['p_name'] = $product->productsModel->products_short_name;
                array_push($products_array,$product);
        }

        $response_data['invoice'] = $SalesInvoiceMode;
        $response_data['products_array'] = $products_array;
        //$response_data['services'] = $Services;

        return response()->json($response_data);
    }

    public function store(StoreUpdateRequest $request){

        $data = $request->all();
        $count = 1;
        $totalCost = 0;
        $totalsaleamt = 0;
        $totaltaxamt = 0;
        $totalpayable = 0;
        $Cost_entry = array();
        $salesInvoiceDetailModel = SalesInvoiceDetailModel::where(['id' => $data['invoice_id'], 'confirm_status' => '1'])->first();
       //dd($data);
        $TR_ledger =  Ledgers::where(['group_id' => Config::get('constants.accounts_debt_customer')])->where(['parent_type' => $salesInvoiceDetailModel->customer_id])->first();
        $customer_ledger_id = $TR_ledger->id;
        $Products = InvoiceDetailsModel::where('salesinvoice_id',$data['invoice_id'])->get();
        $qtys = $data['qty'];
        $serials = $data['serials'];
        DB::beginTransaction();
        $qtyFlag = false;
        //check if invoice already exist in sale return
        $saleReturn = SaleReturn::where(['invoice_id'=> $data['invoice_id'] ,'status'=>1])->first();
        if(!$saleReturn ){
            $data['created_by'] =  Auth::user()->id;
            $data['updated_by'] = Auth::user()->id;
            $saleReturn = SaleReturn::create($data);

        }

        // save and update only those products whose qty has been changed
        foreach($Products as $product){
            $line_cost_price = 0;
            $qty = $qtys[$product->product_id];
            if($qty <= $product->return_qty && $qty > 0) {
                $sale_qty = $product->return_qty - $qtys[$product->product_id];
                $temp_detail['sale_return_id'] = $saleReturn->id;
                $temp_detail['product_id'] = $product->product_id;
                $temp_detail['original_qty'] = $product->quantity;
                $temp_detail['return_qty'] = $qty;
                $temp_detail['return_serial'] = $serials[$product->product_id];
                $temp_detail['status'] = 1;
                $temp_detail['created_by'] = Auth::user()->id;
                $temp_detail['updated_by'] = Auth::user()->id;
                //update previos status to 0 for history purpose

                $stock_quantity = StockItemsModel::where(['st_name' => $product->product_id])->where('purchase_stock','<>','closing_stock')->orderBy('created_at', 'desc')->first();
                $purchase_stock = $stock_quantity->purchase_stock;
                $closing_stock = $stock_quantity->closing_stock;
                SaleReturnDetail::where(['sale_return_id' => $saleReturn->id, 'product_id' => $product->product_id])->update(['status' => 0]);
                InvoiceDetailsModel::where(['salesinvoice_id' => $data['invoice_id'], 'product_id' => $product->product_id])->update(['return_qty' => $sale_qty]);
                $return_qty = $qty + $closing_stock ;
                while ($return_qty > 0){
                    if( $return_qty < $purchase_stock ){

                        $update_stock['closing_stock']  = $return_qty;
                        $stock_quantity->update($update_stock);
                        $cost_unit = $return_qty - $closing_stock;
                        $line_cost_price += $stock_quantity->st_unit_price * $cost_unit;
                        $return_qty = 0;

                    }else{
                        $return_qty1 =  $return_qty -  $purchase_stock;
                        $update_stock['closing_stock'] = $purchase_stock;
                        $stock_quantity->update($update_stock);
                        $cost_unit = $return_qty - $closing_stock - $return_qty1;
                        $line_cost_price += $stock_quantity->st_unit_price * $cost_unit;
                        $stock_quantity = StockItemsModel::where(['st_name' => $product->product_id])->where('purchase_stock','<>','closing_stock')->orderBy('created_at', 'desc')->first();
                        $purchase_stock = $stock_quantity->purchase_stock;
                        $closing_stock = $stock_quantity->closing_stock;
                        $return_qty = $return_qty1 + $closing_stock ;

                    }

                }
                //cost
                $product_Cost_ledger = ProductsModel::getCostGroup($product->product_id);
                $Cost_entry[$count]['ledger_id'] = $product_Cost_ledger;
                $Cost_entry[$count]['dr_amount'] = 0;
                $Cost_entry[$count]['cr_amount'] = $line_cost_price;
                $Cost_entry[$count]['narration'] = 'Sales Return has been done';
                $count++;
                //assets
                $product_Stock_ledger = ProductsModel::getStockGroup($product->product_id);
                $Cost_entry[$count]['ledger_id'] = $product_Stock_ledger;
                $Cost_entry[$count]['dr_amount'] = $line_cost_price;
                $Cost_entry[$count]['cr_amount'] = 0;
                $Cost_entry[$count]['narration'] = 'Sales Return has been done';
                $count++;
                $totalCost += $line_cost_price;
                $return_date = \Carbon\Carbon::createFromFormat('Y-m-d', $data['return_date']);
                $invoice_date = \Carbon\Carbon::createFromFormat('Y-m-d', $data['invoice_date']);
                $diff_in_days = $invoice_date->diffInDays($return_date);

                if($diff_in_days < 15){
                    $totalsaleamt += ($product->unit_price * $qty)-($product->discount * $qty);
                    $totaltaxamt +=  round(($product->unit_price * $qty)*($product->tax_percent/100));
                    $totalpayable = $totalsaleamt +$totaltaxamt;
                }else{
                    $totalsaleamt += ($product->unit_price * $qty)-($product->discount * $qty);
                    $totalpayable = $totalsaleamt;
                }

                SaleReturnDetail::create($temp_detail);

            }

        }
                $payable_entry[1]['ledger_id'] = $customer_ledger_id;
                $payable_entry[1]['dr_amount'] = 0;
                $payable_entry[1]['cr_amount'] = $totalpayable;
                $payable_entry[1]['narration'] = 'Sales Return has been done';
                $payable_entry[2]['ledger_id'] =  Config::get('constants.admin_accounts_SalesReturn');
                $payable_entry[2]['dr_amount'] = $totalsaleamt;
                $payable_entry[2]['cr_amount'] = 0;
                $payable_entry[2]['narration'] = 'Sales Return has been done';
                if($totaltaxamt !='' && $totaltaxamt > 0){
                    $payable_entry[3]['ledger_id'] =  Config::get('constants.admin_accounts_SalesTax');
                    $payable_entry[3]['dr_amount'] = $totaltaxamt;
                    $payable_entry[3]['cr_amount'] = 0;
                    $payable_entry[3]['narration'] = 'Sales Return has been done';
                }
                $entriessale = array(
                    "number" => "??????",
                    "voucher_date" =>  date('Y-m-d'),
                    "employee_id" => Auth::user()->id,
                    "narration" => "Sales Return has been done",
                    "entry_type_id" => "1",
                    "entry_items" =>$Cost_entry,
                    "diff_total" => "0",
                    "dr_total" => $totalCost,
                    "cr_total" => $totalCost,
                    "cheque_no" => null,
                    "cheque_date" => date('Y-m-d'),
                    "invoice_no" => null,
                    "invoice_date" => date('Y-m-d'),
                );
                CoreAccounts::createJEntry($entriessale);
                $entries_Sale = array(
                    "number" => "??????",
                    "voucher_date" =>  date('Y-m-d'),
                    "employee_id" => Auth::user()->id,
                    "narration" => "Sales Return has been done",
                    "entry_type_id" => "1",
                    "entry_items" => $payable_entry,
                    "diff_total" => 0,
                    "dr_total" => $totalpayable,
                    "cr_total" => $totalpayable,
                );
                CoreAccounts::createJEntry($entries_Sale);
//        dd($purchase_stock, $closing_stock, $stock_quantity);
           DB::commit();
        flash('Record has been updated successfully!')->success()->important();

        return redirect()->route('admin.salesreturn.index');
    }

}
