<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AccountsList;
use App\Helpers\CoreAccounts;
use App\Helpers\GroupsTree;
use App\Helpers\LedgersTree;
use App\Models\Admin\AccountTypes;
use App\Models\Admin\Branches;
use App\Models\Admin\Currencies;
use App\Models\Admin\Departments;
use App\Models\Admin\Employees;
use App\Models\Admin\Entries;
use App\Models\Admin\EntryItems;
use App\Models\Admin\EntryTypes;
use App\Models\Admin\Groups;
use App\Models\Admin\StaffSalaries;
use App\Models\Admin\Students;
use App\Models\Admin\Ledgers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Admin\TaxSettings;
use App\Models\Academics\Staff;

//Excel Export Library
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use View;
use Config;

class AccountReportsController extends Controller
{
    // Variable to hold Excel Data
    protected $sheet;
    // Variable to hold Excel Loop counter
    protected static $excel_iterator;
    // Variable to hold Balance Sheet Loop counter
    protected static $bs_iterator;
    // Variable to hold Profit and Loss counter
    protected static $pandl_iterator;
    // Variable to hold Ledger Statement counter
    protected static $ls_iterator;

    /**
     * Display a listing of AccountReport.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }

        return view('admin.account_reports.index');
    }
    public function fee_campus_report()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }
        return view('admin.account_reports.fee.campus_wise_fee_report');
    }

    public function tax_calculation_236()
    {   
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');
        return view('admin.account_reports.tax.236i_tax',compact('Branches'));
    }
    public function tax_calculation_236_report(Request $request)
    {   
      /*   $date_range=explode('-', trim($request->date_range));
        $start_date = strtr($date_range[0], '/', '-');
         $start_date=date('Y-m-d', strtotime($start_date));
         $end_date = strtr($date_range[1], '/', '-');
         $end_date = date('Y-m-d', strtotime($end_date)); */
         tax_amount($branch_id,$company_id,$city_id,$start_date,$end_date);
         
    }

    public function security_refund_register()
    {   
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');
        return view('admin.account_reports.fee.security_refund_register',compact('Branches'));
    }
    public function recon_with_fee_department()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }
        return view('admin.account_reports.fee.recon_with_fee_department');
    }
    public function fee_monthly_report()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }
        return view('admin.account_reports.fee.fee_monthly_report');
    }
    public function fee_monthly_report_print(Request $request)
    {
        $date_range=explode('-', trim($request->date_range));
        $start_date = strtr($date_range[0], '/', '-');
         $start_date=date('Y-m-d', strtotime($start_date));
         $end_date = strtr($date_range[1], '/', '-');
         $end_date = date('Y-m-d', strtotime($end_date));
    
         $array = array();
        $company_id = $request->company_id;
        $branch_name;
        $balance = 0;
        $query = Ledgers::selectRaw('erp_branches.id as branch_id,erp_branches.city_id, erp_branches.name as name, sum(opening_balance) as balance' )
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
        ->where([['erp_ledgers.account_type_id', '=','1'],['erp_branches.company_id','=',$company_id]])
        ->groupBy('branch_id')
        ->get();
        $i=0;
        $security_amount =0;
        foreach ($query as $key) {
            # code...
             
         $balance=0.00; $tdr=0; $tcr=0; 
            $branch_name = $key['name'];
            $balance= $key['balance'];
            $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
             ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
             ->where([['erp_ledgers.account_type_id', '=','1'],['branch_id','=',$key['branch_id']]])->whereBetween('voucher_date', [''.$start_date.'', ''.$end_date.''])->get();
             $students = Students::where('branch_id', '=', $key['branch_id'])->get();
             $branch_id = $key['branch_id'];
             $city_id = $key['city_id'];
             $student_no = $students->count();
            $security =0;
            $tax_collected = 0;
             foreach ($Entrie as $Ent) {
                $dr=0.00; $cr=0.00;
                 if ($Ent->dc == 'd') {
                     $dr = $Ent->amount;
                     $balance=$balance+$dr;
                 } else {
                     $cr = floatval($Ent->amount);
                     $tcr=$tcr+$cr;
     
                 }
                 //$ob+=($dr);
                  
           
      
              
             }  
             $default = $balance-$tcr;
            
             $recovery = 0;
             if($tcr==0)
             {
                 $recovery=1;
             }
             else
             {

            //    $recovery = $tcr/$balance;
             }
             $security_amount =  AccountReportsController::security_received($branch_id,$company_id,$city_id,$start_date,$end_date);
             $tax_amount =  AccountReportsController::tax_amount($branch_id,$company_id,$city_id,$start_date,$end_date);
   
           //  $recovery = $recovery * 100;
          //   $recovery = number_format($recovery, 2);

        $i++;
     
        $array[] = array('id'=>$i,'branch'=>$branch_name,'balance' => CoreAccounts::dr_cr_balance($balance, 2),'tcr' =>$tcr,'no'=>$tax_amount,'rec'=>$security_amount);
       
        }
    if($request->type=='print'){
           return view('admin.account_reports.fee.fee_daily_outstanding_report_print', compact('array'));
    }
        else
        { 
     
        return response()->json(['data' => $array]);
       }
    }
    public function income_tax($id)
    {
       
        $get_data = Staff::leftjoin('erp_branches','erp_branches.id','=','staff.branch_id')
        ->leftjoin('erp_city','erp_branches.city_id','=','erp_city.id')
        ->leftjoin('erp_companies','erp_companies.id','=','erp_branches.company_id')
        ->where('staff.id' ,$id)->first();
        $tax_amount = 0; 
        $tax_words = 0;
        $period = 0;
        $yearly_salary=0;
            if(isset($get_data->salary))
            {
                $filersalary = $get_data->salary;
                $yearly_salary = $filersalary * 12;
            
                $tax_percent = TaxSettings::where('start_range', '<', $yearly_salary)->where('end_range', '>', $yearly_salary)->where('status','=','1')->get();
             $tax_range = 0;
             $percent = 0;
             $fixed_amount = 0;

            
               foreach($tax_percent as $tax)
               {
                 //  print_r($tax['start_range']);
                   $tax_range =$tax['end_range']-$tax['start_range'];
                   $percent =$tax['tax_percent'] ;
                   $fixed_amount = $tax['fix_amount'];
                   $period = $tax['tax_class'];
                 
               }
               $exceed = ($tax_range/100)*$percent;
       
               $tax_amount = $exceed+$fixed_amount;
                $tax_amount=$tax_amount/12;



            }
            else
            {
                $tax_amount=0;
             //   $tax_words = AccountReportsController::convertNumber($tax_amount);
            }
        
       // $numberToWord = new Numbers_Words();
       // $tax_words=$numberToWords->toWords($tax_amount);
                $number = AccountReportsController::convert_number_to_words($tax_amount);
            $tax_word = $number;
$total = $yearly_salary;
$period_array = explode("-",$period);

            return view('admin.account_reports.tax.income_tax',compact('get_data','tax_amount','tax_words','total','tax_word','period_array'));
       
      
    }

  public function convert_number_to_words($number) {
      
        $hyphen      = '-';
        $conjunction = '  ';
        $separator   = ' ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'Zero',
            1                   => 'One',
            2                   => 'Two',
            3                   => 'Three',
            4                   => 'Four',
            5                   => 'Five',
            6                   => 'Six',
            7                   => 'Seven',
            8                   => 'Eight',
            9                   => 'Nine',
            10                  => 'Ten',
            11                  => 'Eleven',
            12                  => 'Twelve',
            13                  => 'Thirteen',
            14                  => 'Fourteen',
            15                  => 'Fifteen',
            16                  => 'Sixteen',
            17                  => 'Seventeen',
            18                  => 'Eighteen',
            19                  => 'Nineteen',
            20                  => 'Twenty',
            30                  => 'Thirty',
            40                  => 'Fourty',
            50                  => 'Fifty',
            60                  => 'Sixty',
            70                  => 'Seventy',
            80                  => 'Eighty',
            90                  => 'Ninety',
            100                 => 'Hundred',
            1000                => 'Thousand',
            1000000             => 'Million',
            1000000000          => 'Billion',
            1000000000000       => 'Trillion',
            1000000000000000    => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
        );
      
        if (!is_numeric($number)) {
            return false;
        }
      
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }
     
        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }
      
        $string = $fraction = null;
      
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
      
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . AccountReportsController::convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = AccountReportsController::convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= AccountReportsController::convert_number_to_words($remainder);
                }
                break;
        }
      
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
      
        return $string;
    }
     
 
    public function recon_with_fee_department_report(Request $request)
    {
         $date_range=explode('-', trim($request->date_range));
        $start_date = strtr($date_range[0], '/', '-');
         $start_date=date('Y-m-d', strtotime($start_date));
         $end_date = strtr($date_range[1], '/', '-');
         $end_date = date('Y-m-d', strtotime($end_date));
    
         $array = array();
         $company_id = $request->company_id;

        $query = Ledgers::join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_city','erp_city.id','=','erp_branches.city_id' )
        ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
        ->where([['erp_ledgers.name', 'like','%(Ledger)(Tax 236i Payable)%'],['erp_branches.company_id','=',$company_id]])
        ->pluck('erp_ledgers.id');


$i=0;
foreach ($query as $key) {
    # code...
$i++;
$ledgerID = $key;
         $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
         ->where('ledger_id','=', $ledgerID)->whereBetween('voucher_date', [$start_date, $end_date])->get();
       
         $branch_name = Ledgers::join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
         ->where('erp_ledgers.id','=',$ledgerID)->pluck('erp_branches.name');
    
         $ledger_name=Ledgers::where('id', $ledgerID)->value('name');
         $opening_balance=CoreAccounts::opening_balance($start_date, $ledgerID);
         $ob_str = '<td colspan="6" align="right">Opening Balance AS on ' . $start_date . '</td>
         <td align="right">'.CoreAccounts::dr_cr_balance($opening_balance).'</td>';
         
        
         
         
         $balance=0.00; $ob=0; $tdr=0; $tcr=0; 
         $i=0;
           foreach ($Entrie as $Ent) {
            $dr=0.00; $cr=0.00;
             if ($Ent->dc == 'd') {
                 $dr = $Ent->amount;
             } else {
                 $cr = floatval($Ent->amount);
 
             }
             $ob+=($dr)-($cr);
             $balance=$opening_balance+$ob;
             $balance=number_format($balance, 2,'.', '');
            

                $tdr+=$dr; 
             
             

              $tcr+=$cr;
             
             //$array[] = array('voucher_date' => $Ent->voucher_date, 'number' => $vn[0],
             //'vt' => $vt[0], 'narration' => $Ent->narration, 'dr_amount' => number_format($dr, 2), 'cr_amount' => number_format($cr,2), 'balance' => CoreAccounts::dr_cr_balance($balance, 2));
            }
            if($balance==0)
            {
                $balance=$opening_balance;
            }
    $prBalance=CoreAccounts::dr_cr_balance(CoreAccounts::opening_balance($start_date, $ledgerID));
         
     $array[] = array('id'=>$key,'branch'=>$branch_name,'ob'=>$prBalance,'tdr'=>number_format($tcr, 2).' CR','tcr'=>number_format($tdr, 2).' DR','balance'=>CoreAccounts::dr_cr_balance($balance, 2));
        }
   
       if($request->type=='print'){
             return view('admin.account_reports.fee.recon_with_fee_department_report', compact('array', 'ledger_name', 'start_date', 'end_date', 'prBalance', 'tdr', 'tcr', 'balance'));
         }
         return response()->json(['data' => $array]);


         


    
    }
    public function fee_daily_outstanding_report()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }
        return view('admin.account_reports.fee.fee_daily_outstanding_report');
    }
    public function fee_daily_outstanding_report_save(Request $request)
    {
        $company_id = $request->company_id;
        $array = array();
        $branch_name;
        $balance = 0;
        $query = Ledgers::selectRaw('erp_branches.id as branch_id, erp_branches.name as name, sum(opening_balance) as balance' )
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
        ->where([['erp_ledgers.account_type_id', '=','1'],['erp_branches.company_id','=',$company_id]])
        ->groupBy('branch_id')
        ->get();
        $i=0;
        foreach ($query as $key) {
            # code...
             
         $balance=0.00; $tdr=0; $tcr=0; 
            $branch_name = $key['name'];
            $balance= $key['balance'];
            $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
             ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
             ->where([['erp_ledgers.account_type_id', '=','1'],['branch_id','=',$key['branch_id']]])->get();
             $students = Students::where('branch_id', '=', $key['branch_id'])->get();
             $student_no = $students->count();
             foreach ($Entrie as $Ent) {
                $dr=0.00; $cr=0.00;
                 if ($Ent->dc == 'd') {
                     $dr = $Ent->amount;
                     $balance=$balance+$dr;
                 } else {
                     $cr = floatval($Ent->amount);
                     $tcr=$tcr+$cr;
     
                 }
                 //$ob+=($dr);
             
      
                 //$array[] = array('voucher_date' => $Ent->voucher_date, 'number' => $vn[0],
                 //'vt' => $vt[0], 'narration' => $Ent->narration, 'dr_amount' => number_format($dr, 2), 'cr_amount' => number_format($cr,2), 'balance' => CoreAccounts::dr_cr_balance($balance, 2));
            
             }  
             $default = $balance-$tcr;
            
             $recovery = 0;
             if($tcr==0)
             {
                 $recovery=1;
             }
             else
             {

                $recovery = $tcr/$balance;
             }
        
             $recovery = $recovery * 100;
             $recovery = number_format($recovery, 2);

        $i++;
     
        $array[] = array('id'=>$i,'branch'=>$branch_name,'balance' => CoreAccounts::dr_cr_balance($balance, 2),'tcr' =>$tcr,'no'=>CoreAccounts::dr_cr_balance($default, 2),'rec'=>$recovery);
       
        }
    if($request->type=='print'){
           return view('admin.account_reports.fee.fee_daily_outstanding_report_print', compact('array'));
    }
        else
        { 
        return response()->json(['data' => $array]);
       }
    }
    public function fee_recover_report()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }
        return view('admin.account_reports.fee.fee_recovery_report');
    }
    public function fee_recover_report_print(Request $request)
    {
        $company_id = $request->company_id;
        $array = array();
        $branch_name;
        $balance = 0;
        $query = Ledgers::selectRaw('erp_branches.id as branch_id, erp_branches.name as name, sum(opening_balance) as balance' )
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
        ->where([['erp_ledgers.account_type_id', '=','1'],['erp_branches.company_id','=',$company_id]])
        ->groupBy('branch_id')
        ->get();
        $i=0;
        foreach ($query as $key) {
            # code...
             
         $balance=0.00; $tdr=0; $tcr=0; 
            $branch_name = $key['name'];
            $balance= $key['balance'];
            $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
             ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
             ->where([['erp_ledgers.account_type_id', '=','1'],['branch_id','=',$key['branch_id']]])->get();
             $students = Students::where('branch_id', '=', $key['branch_id'])->get();
             $student_no = $students->count();
             foreach ($Entrie as $Ent) {
                $dr=0.00; $cr=0.00;
                 if ($Ent->dc == 'd') {
                     $dr = $Ent->amount;
                     $balance=$balance+$dr;
                 } else {
                     $cr = floatval($Ent->amount);
                     $tcr=$tcr+$cr;
     
                 }
                 //$ob+=($dr);
             
      
                 //$array[] = array('voucher_date' => $Ent->voucher_date, 'number' => $vn[0],
                 //'vt' => $vt[0], 'narration' => $Ent->narration, 'dr_amount' => number_format($dr, 2), 'cr_amount' => number_format($cr,2), 'balance' => CoreAccounts::dr_cr_balance($balance, 2));
            
             }
       
    
             $recovery = 0;
             if($tcr==0)
             {
             }
             else
             {
                if($balance !=0)
                {
                $recovery = $tcr/$balance;
                }
             }

             $recovery = $recovery * 100;
             $recovery = number_format($recovery, 2);

        $i++;
     
        $array[] = array('id'=>$i,'branch'=>$branch_name,'balance' => CoreAccounts::dr_cr_balance($balance, 2),'tcr' =>CoreAccounts::dr_cr_balance($tcr, 2),'no'=>$student_no,'rec'=>$recovery);
       
        }
      if($request->type=='print'){
            return view('admin.account_reports.fee.fee_recovery_report_print', compact('array'));
        } 
        else
        {
        return response()->json(['data' => $array]);
        }
    }
    public function SalarySlip()
    {
    
        return view('admin.account_reports.payroll.payslip');
    }


    public function tax_amount($branch_id,$company_id,$city_id,$start_date,$end_date)
    {
        $amount_received = 0;
        $security_id = 148;
        $fetch = Groups::where(['parent_id'=>$security_id,'erp_groups.company_id'=>$company_id])->get();
       
        foreach ($fetch as $key => $value) {
            # code.
            $group_id = $value->id;
        }
  
        $fetch = Groups::where(['parent_id'=>$group_id,'erp_groups.company_id'=>$company_id])->get();
       
        foreach ($fetch as $key => $value) {
            # code.
            if($value->city_id==$city_id)
            {
                
            $group_id = $value->id;
            $city_id = $value->city_id;
            }
        }
     
        $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'erp_groups.company_id'=>$company_id])->get();
        foreach ($fetch as $key => $value) {
            # code.
            if($branch_id==$value->branch_id)
            {
            $group_id = $value->id;
            }
           
        }
  
     /*     $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
          foreach ($fetch as $key => $value) {
            # code.
            $group_id = $value->id;
        }  */
       
        $rec_ledger = Ledgers::where(['group_id' => $group_id,
                'branch_id' => $branch_id
            ])->first();
            $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
            ->where('erp_ledgers.id','=', $rec_ledger->id)->whereBetween('voucher_date', [''.$start_date.'', ''.$end_date.''])->get();
            foreach ($Entrie as $Entt) {
              
                if($Entt->dc=='c')
                {
                  $amount_received=$amount_received+$Entt->amount;
                }
              
            } 
         
            return $amount_received;
    }





    public function security_received($branch_id,$company_id,$city_id,$start_date,$end_date)
    {
        $amount_received = 0;
        $security_id = 137;
        $fetch = Groups::where(['parent_id'=>$security_id,'erp_groups.company_id'=>$company_id])->get();
       
        foreach ($fetch as $key => $value) {
            # code.
            $group_id = $value->id;
        }
      
        $fetch = Groups::where(['parent_id'=>$group_id,'erp_groups.company_id'=>$company_id])->get();
       
        foreach ($fetch as $key => $value) {
            # code.
            if($value->city_id==$city_id)
            {
                
            $group_id = $value->id;
            $city_id = $value->city_id;
            }
        }
        $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'erp_groups.company_id'=>$company_id])->get();
        foreach ($fetch as $key => $value) {
            # code.
            if($branch_id==$value->branch_id)
            {
            $group_id = $value->id;
            }
        }
        $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
          foreach ($fetch as $key => $value) {
            # code.
            $group_id = $value->id;
        }
        $rec_ledger = Ledgers::where(['group_id' => $group_id,
                'branch_id' => $branch_id
            ])->first();
            $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
            ->where('erp_ledgers.id','=', $rec_ledger->id)->whereBetween('voucher_date', [''.$start_date.'', ''.$end_date.''])->get();
            foreach ($Entrie as $Entt) {
              
                if($Entt->dc=='c')
                {
                  $amount_received=$amount_received+$Entt->amount;
                }
              
            } 
         
            return $amount_received;
    }


    public function security_refund_register_report(Request $request)
    {
      $security_id = 137;
        $security_payable = 35;
        $company_id = $request->company_id;
        $group_id = 0;
        $group_payable = 0;
        $city_id = $request->city_id;
        $branch_id = $request->branch_id;
        $city_payable = $request->city_id;
 if($city_id !=0 && $branch_id !=0 && $company_id !=0)
    {
        $fetch_payable = Groups::where(['parent_id'=>$security_payable,'erp_groups.company_id'=>$company_id])->get();
     
        foreach ($fetch_payable as $key => $vallue) {
            # code.
            $group_payable = $vallue->id;
        }
        $fetch_payable = Groups::where(['parent_id'=>$group_payable,'erp_groups.company_id'=>$company_id])->get();
     
        foreach ($fetch_payable as $key => $vallue) {
            # code.
            if($vallue->city_id==$city_payable)
            {
                
            $group_payable = $vallue->id;
            $city_payable = $vallue->city_id;
            }
        }
       
        $fetch_payable = Groups::where(['parent_id'=>$group_payable,'city_id'=>$city_payable,'erp_groups.company_id'=>$company_id])->get();
        foreach ($fetch_payable as $key => $vallue) {
            # code.
            if($branch_id==$vallue->branch_id)
            {
            $group_payable = $vallue->id;
            }
        }
       
        $fetch = Groups::where(['parent_id'=>$group_payable,'city_id'=>$city_payable,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
          foreach ($fetch_payable as $key => $vallue) {
            # code.
            $group_payable = $vallue->id;
        }
        $inc_ledger = Ledgers::where(['group_id' => $group_payable,
                'branch_id' => $branch_id
            ])->first();

        $fetch = Groups::where(['parent_id'=>$security_id,'erp_groups.company_id'=>$company_id])->get();
       
        foreach ($fetch as $key => $value) {
            # code.
            $group_id = $value->id;
        }
      
        $fetch = Groups::where(['parent_id'=>$group_id,'erp_groups.company_id'=>$company_id])->get();
       
        foreach ($fetch as $key => $value) {
            # code.
            if($value->city_id==$city_id)
            {
                
            $group_id = $value->id;
            $city_id = $value->city_id;
            }
        }
        $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'erp_groups.company_id'=>$company_id])->get();
        foreach ($fetch as $key => $value) {
            # code.
            if($branch_id==$value->branch_id)
            {
            $group_id = $value->id;
            }
        }
        $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
          foreach ($fetch as $key => $value) {
            # code.
            $group_id = $value->id;
        }
        $rec_ledger = Ledgers::where(['group_id' => $group_id,
                'branch_id' => $branch_id
            ])->first();
       $i =0;
       $array[]=array();
       $data = Students::join('erp_branches', 'erp_branches.id', '=', 'students.branch_id')
       ->join('erp_ledgers', 'erp_ledgers.branch_id','=','erp_branches.id')
       ->join('erp_city','erp_city.id','=','erp_branches.city_id')
       ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
       //->join('erp_ledger','erp_ledgers.id','=','erp_entry_items.ledger_id')
       ->where('erp_ledgers.id','=', @$rec_ledger->id)
       ->get();
         foreach ($data as $Ent) {
            $i++;
               $name=$Ent->reg_no . ' ' . $Ent->first_name . ' ' . $Ent->last_name;
              $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
              ->where([['narration','=', $name ],['erp_ledgers.id','=', $rec_ledger->id],['erp_ledgers.account_type_id','!=',4]])->get();











              $Entrie_payable = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
              ->where([['narration','=', $name ],['erp_ledgers.id','=', @$inc_ledger->id],['erp_ledgers.account_type_id','!=',4]])->get();
             $date=0;
              $amount_dr=0;
              $amount_cr=0;
              $amount_dr_paid = 0;
              $amount_cr_paid = 0;
              $date_payable = 0;
              foreach ($Entrie as $Entt) {
                  if($Entt->dc=='d')
                  {
                    $amount_dr=$amount_dr+$Entt->amount;
                  }
                  if($Entt->dc=='c')
                  {
                    $amount_cr=$amount_cr+$Entt->amount;
                  }
                $date = $Entt->voucher_date;
              } 
             
              foreach ($Entrie_payable as $Entt_payable) {
               
                if($Entt_payable->dc=='d')
                {
                  $amount_cr_paid=$amount_cr_paid+$Entt_payable->amount;
                  $date_payable = $Entt_payable->voucher_date;
                }
              
            } 
              if($date !=0){
             
                  $balance = $amount_cr-$amount_cr_paid;
            $array[] = array('id'=>$i,'first_name' => $Ent->first_name.' '.$Ent->last_name,'reg_no'=>$Ent->reg_no,'branch'=>$Ent->name,'amount'=>$amount_dr,'received'=>$amount_cr,'date'=>$date_payable,'amount_cr'=>$amount_cr_paid,'balance'=>$balance,'refund'=>$date_payable);
            
              }
        }  
        if($request->type=='print'){
            return view('admin.account_reports.fee.security_refund_register_report',compact('array'));
        }
        else
        {
      return response()->json(['data' => $array]);
        }
    }
else
if($company_id !=0 && $city_id !=0 )
{
   
    $array[]=array();
    $i =0;
   
    $fetch = Groups::where(['parent_id'=>$security_id,'erp_groups.company_id'=>$company_id])->get();
   
    foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    }

    $fetch = Groups::where(['parent_id'=>$group_id,'erp_groups.company_id'=>$company_id])->get();
   
    foreach ($fetch as $key => $value) {
        # code.
        if($value->city_id==$city_id)
        {
            
        $group_id = $value->id;
        $city_id = $value->city_id;
        }
    }
    $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'erp_groups.company_id'=>$company_id])->get();
   
    foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
        $branch_id=$value->branch_id;
    
    
    $fetch = Groups::where(['parent_id'=>$group_id,'city_id'=>$city_id,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
      foreach ($fetch as $key => $value) {
        # code.
        $group_id = $value->id;
    }
    $rec_ledger = Ledgers::where(['group_id' => $group_id,
            'branch_id' => $branch_id
        ])->first();
   
  
   $data = Students::join('erp_branches', 'erp_branches.id', '=', 'students.branch_id')
   ->join('erp_ledgers', 'erp_ledgers.branch_id','=','erp_branches.id')
   ->join('erp_city','erp_city.id','=','erp_branches.city_id')
   ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
   //->join('erp_ledger','erp_ledgers.id','=','erp_entry_items.ledger_id')
   ->where('erp_ledgers.id','=', @$rec_ledger->id)
   ->get();
   $i++;
   $inc_ledger = 0;
   
     foreach ($data as $Ent) {
        
           $name=$Ent->reg_no . ' ' . $Ent->first_name . ' ' . $Ent->last_name;
       
          $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
          ->where([['narration','=', $name ],['erp_ledgers.id','=', @$rec_ledger->id],['erp_ledgers.account_type_id','!=',4]])->get();

          $fetch_payable = Groups::where(['parent_id'=>$security_payable,'erp_groups.company_id'=>$company_id])->get();
  
          foreach ($fetch_payable as $key => $vallue) {
              # code.
              $group_payable = $vallue->id;
          
          $fetch_payablee = Groups::where(['parent_id'=>$group_payable,'erp_groups.company_id'=>$company_id])->get();
        
          foreach ($fetch_payablee as $key => $valllue) {
              # code.
              if($valllue->city_id==$city_payable)
              {
                  
              $group_payable = $valllue->id;
              $city_payable = $valllue->city_id;
              }
          }
          $fetch_payableee = Groups::where(['parent_id'=>$group_payable,'city_id'=>$city_payable,'erp_groups.company_id'=>$company_id])->get();
          
          foreach ($fetch_payableee as $key => $vallllue) {
              # code.
              if($branch_id== $vallllue->branch_id)
              {
               
              $group_payable = $vallllue->id;
              }
          }
          
          $fetch = Groups::where(['parent_id'=>$group_payable,'city_id'=>$city_payable,'branch_id'=>$branch_id,'erp_groups.company_id'=>$company_id])->get();
            foreach ($fetch as $key => $valllllue) {
              # code.
              $group_payable = $vallue->id;
          }
       
          $inc_ledger = Ledgers::where(['group_id' => $group_payable,
                  'branch_id' => $branch_id
              ])->first();
        
             

        



          $Entrie_payable = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
          ->where([['narration','=', $name ],['erp_ledgers.id','=', @$inc_ledger->id],['erp_ledgers.account_type_id','!=',4]])->get();



         $date=0;
          $amount_dr=0;
          $amount_cr=0;
          $amount_dr_paid = 0;
          $amount_cr_paid = 0;
          $date_payable = 0;
          foreach ($Entrie as $Entt) {
              if($Entt->dc=='d')
              {
                $amount_dr=$amount_dr+$Entt->amount;
              }
              if($Entt->dc=='c')
              {
                $amount_cr=$amount_cr+$Entt->amount;
              }
            $date = $Entt->voucher_date;
          }
 
          foreach ($Entrie_payable as $Entt_payable) {
           
         
            if($Entt_payable->dc=='d')
            {
              $amount_cr_paid=$amount_cr_paid+$Entt_payable->amount;
              $date_payable = $Entt_payable->voucher_date;
            }
          
        } 
          if($date !=0){
         
            $balance = $amount_cr-$amount_cr_paid;
        $array[] = array('id'=>$i,'first_name' => $Ent->first_name.' '.$Ent->last_name,'reg_no'=>$Ent->reg_no,'branch'=>$Ent->name,'amount'=>$amount_dr,'received'=>$amount_cr,'date'=>$date_payable,'amount_cr'=>$amount_cr_paid,'balance'=>$balance,'refund'=>$date_payable);
        
          }
        }
        }
    
}
    if($request->type=='print'){
        return view('admin.account_reports.fee.security_refund_register_report',compact('array'));
    }
    else
    {
  return response()->json(['data' => $array]);
    }
}

        
      
    
    }
    /**
     * Display Chart of Accounts.
     *
     * @return \Illuminate\Http\Response
     */
    public function accounts_chart()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }

        $accountlist = new AccountsList();
        $accountlist->only_opening = false;
        $accountlist->start_date = null;
        $accountlist->end_date = null;
        $accountlist->affects_gross = -1;
        $accountlist->start(0);

        return view('admin.account_reports.index');
    }

    /**
     * Show Trial Balance.
     *
     * @return \Illuminate\Http\Response
     */
    public function trial_balance()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }
        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Account Types
        $AccountTypes = AccountTypes::getActiveListDropdown(true);

        // Get All Groups
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('group_id'));

        return view('admin.account_reports.trial_balance.index', compact( 'Branches', 'AccountTypes', 'Groups'));
    }

    /**
     * Load Report of Trial Balance.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function trial_balance_report(Request $request)
    {
       // print_r($request->all());exit;
        if($request->get('date_range')) {
            $date_range = explode(' - ', $request->get('date_range'));
            $start_date = date('Y-m-d', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1]));
        } else {
            $start_date = null;
            $end_date = null;
        }
     
        $accountlist = new AccountsList();
        $accountlist->only_opening = false;
        $accountlist->start_date = $start_date;
        $accountlist->end_date = $end_date;
        $accountlist->affects_gross = -1;
        $accountlist->filter = $request->all();

        $accountlist->start(($request->get('group_id')) ? $request->get('group_id') : 0);
   //  echo'<pre>';print_r($accountlist);echo'</pre>';exit;
         $DefaultCurrency = Currencies::getDefaultCurrencty();
        $ReportData = $accountlist->generateLedgerStatement($accountlist);
        
        switch($request->get('medium_type')) {
            case 'web':
                return view('admin.account_reports.trial_balance.report', compact('accountlist', 'ReportData', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
            case 'print':
                return view('admin.account_reports.trial_balance.report', compact('accountlist', 'ReportData', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
            case 'excel':
                $this->trialBalanceExcel($accountlist, $DefaultCurrency, $start_date, $end_date);
                break;
            case 'pdf':
                $content = View::make('admin.account_reports.trial_balance.report', compact('accountlist', 'ReportData', 'start_date', 'end_date', 'DefaultCurrency'))->render();
                // Create an instance of the class:
                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
                // Write some HTML code:
                $mpdf->WriteHTML($content);
                // Output a PDF file directly to the browser
                $mpdf->Output('TrialBalanceReport.pdf','D');
                break;
            default:
                return view('admin.account_reports.trial_balance.report', compact('accountlist', 'ReportData', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
        } 
    }

    /*
     * Function to export Trial Balance to Excel file.
     */
    protected function trialBalanceExcel($accountlist, $DefaultCurrency, $start_date, $end_date) {
        $spreadsheet = new Spreadsheet();

        // Set Default Attributes
        $spreadsheet->getProperties()->setCreator("Mustafa Mughal");
        $spreadsheet->getProperties()->setLastModifiedBy("Mustafa Mughal");
        $spreadsheet->getProperties()->setTitle("Trial Balance");
        $spreadsheet->getProperties()->setSubject("Trial Balance Report");
        $spreadsheet->getProperties()->setDescription("Function used to generate trial balance in excel format.");
        $this->sheet = $spreadsheet->getActiveSheet();
        $this->sheet->setCellValue('A1', 'Trial Balance from ' . $start_date . ' to ' . $end_date);

        $this->sheet->setCellValue('A2', 'Account Name');
        $this->sheet->setCellValue('B2', 'Account Type');
        $this->sheet->setCellValue('C2', 'Opening Balance (' . $DefaultCurrency->code . ')');
        $this->sheet->setCellValue('D2', 'Debit (' . $DefaultCurrency->code . ')');
        $this->sheet->setCellValue('E2', 'Credit (' . $DefaultCurrency->code . ')');
        $this->sheet->setCellValue('F2', 'Closing Balance (' . $DefaultCurrency->code . ')');

        // Print All Data into Excel File
        self::$excel_iterator = 3;
        $this->loopTrialBalanceExcel($accountlist, 0);

        // Now Print Footer into sheet
        $this->sheet->setCellValue('A' . self::$excel_iterator, '');
        $this->sheet->setCellValue('B' . self::$excel_iterator, '');
        $this->sheet->setCellValue('C' . self::$excel_iterator, 'Grand Total');
        $this->sheet->setCellValue('D' . self::$excel_iterator, CoreAccounts::toCurrency('d', $accountlist->dr_total));
        $this->sheet->setCellValue('E' . self::$excel_iterator, CoreAccounts::toCurrency('c', $accountlist->cr_total));


        $writer = new Xlsx($spreadsheet);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="TrialBalanceReport.xlsx"');
        // Write file to the browser
        $writer->save('php://output');
    }

    /*
     * Function to iterator over N-Dimentional Data.
     */
    private function loopTrialBalanceExcel($account, $c = 0)
    {
        $counter = $c;

        /* Print groups */
        if ($account->id != 0) {
            $this->sheet->setCellValue('A' . self::$excel_iterator, html_entity_decode(AccountsList::printSpace($counter)). AccountsList::toCodeWithName($account->code, $account->name));
            $this->sheet->setCellValue('B' . self::$excel_iterator, 'Group');
            $this->sheet->setCellValue('C' . self::$excel_iterator, CoreAccounts::toCurrency($account->op_total_dc, $account->op_total));
            $this->sheet->setCellValue('D' . self::$excel_iterator, CoreAccounts::toCurrency('d', $account->dr_total));
            $this->sheet->setCellValue('E' . self::$excel_iterator, CoreAccounts::toCurrency('c', $account->cr_total));

            if ($account->cl_total_dc == 'd') {
                $this->sheet->setCellValue('F' . self::$excel_iterator, CoreAccounts::toCurrency('d', $account->cl_total));
            } else {
                $this->sheet->setCellValue('F' . self::$excel_iterator, CoreAccounts::toCurrency('c', $account->cl_total));
            }

            self::$excel_iterator++;
        }

        /* Print child ledgers */
        if (count($account->children_ledgers) > 0) {
            $counter++;
            foreach ($account->children_ledgers as $id => $data) {
                $this->sheet->setCellValue('A' . self::$excel_iterator, html_entity_decode(AccountsList::printSpace($counter)). AccountsList::toCodeWithName($data['code'], $data['name']));
                $this->sheet->setCellValue('B' . self::$excel_iterator, 'Ledger');
                $this->sheet->setCellValue('C' . self::$excel_iterator, CoreAccounts::toCurrency($data['op_total_dc'], $data['op_total']));
                $this->sheet->setCellValue('D' . self::$excel_iterator, CoreAccounts::toCurrency('d', $data['dr_total']));
                $this->sheet->setCellValue('E' . self::$excel_iterator, CoreAccounts::toCurrency('c', $data['cr_total']));

                if ($account->cl_total_dc == 'd') {
                    $this->sheet->setCellValue('F' . self::$excel_iterator, CoreAccounts::toCurrency('d', $data['cl_total']));
                } else {
                    $this->sheet->setCellValue('F' . self::$excel_iterator, CoreAccounts::toCurrency('c', $data['cl_total']));
                }

                self::$excel_iterator++;
            }
            $counter--;
        }

        /* Print child groups recursively */
        foreach ($account->children_groups as $id => $data) {
            $counter++;
            $this->loopTrialBalanceExcel($data, $counter);
            $counter--;
        }
    }

    /**
     * Load Groups by Account Type.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function load_groups(Request $request)
    {

        $Groups = '';
        if($request->get('account_type_id')) {
            $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::where('level', $request->get('account_type_id'))->OrderBy('number', 'asc')->get()->toArray()), 0);
        } else {
            $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('number', 'asc')->get()->toArray()), 0);
        }

        return response()->json([
            'dropdown' => $Groups,
        ]);
    }

    /**
     * Load Ledgers by Account Type.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function load_ledgers(Request $request)
    {
        if($request->get('account_type_id')) {
            $id = 0;
            $DefaultGroup = Groups::where(['parent_id' => 0, 'account_type_id' => $request->get('account_type_id')])->first();
            if($DefaultGroup) {
                $id = $DefaultGroup->id;
            }
        } else {
            $id = 0;
        }

        $dropdown = '';

        /* Create list of ledgers to pass to view */
        $parentGroups = new LedgersTree();
        $parentGroups->current_id = -1;
        $parentGroups->restriction_bankcash = 1;
        $parentGroups->build($id);
        $parentGroups->toList($parentGroups, -1);
        $Ledgers = $parentGroups->ledgerList;

        if(count($Ledgers)) {
            foreach($Ledgers as $id => $data) {
                if($id == 0) { continue; }
                if($id < 0) {
                    $dropdown .= '<option value="' . $id . '" disabled="disabled">' . $data["name"] . '</option>';
                } else {
                    $dropdown .= '<option value="' . $id . '">' . $data["name"] . '</option>';
                }
            }
        }

        return response()->json([
            'dropdown' => $dropdown,
        ]);
    }


    /*
     * ------------------------------------------------------
     * Balance Sheet Report Start
     * ------------------------------------------------------
     */

    /**
     * Show Balance Sheet.
     *
     * @return \Illuminate\Http\Response
     */
    public function balance_sheet()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }

        // Get All Employees
     //   $Employees = Employees::pluckActiveOnly();
     //   $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
     //   $Departments = Departments::pluckActiveOnly();
     //   $Departments->prepend('Select a Department', '');

        // Get All Entry Types
     //   $EntryTypes = EntryTypes::pluckActiveOnly();
     //   $EntryTypes->prepend('Select an Entry Type', '');

        return view('admin.account_reports.balance_sheet.index', compact( 'Branches'));
    }

    /**
     * Load Report of Trial Balance.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function balance_sheet_report(Request $request)
    {

        if($request->get('date_range')) {
            $date_range = explode(' - ', $request->get('date_range'));
            $start_date = date('Y-m-d', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1]));
        } else {
            $start_date = null;
            $end_date = null;
        }

        /* Liabilities */
        $liabilities = new AccountsList();
        $liabilities->only_opening = 0;
        $liabilities->start_date = $start_date;
        $liabilities->end_date = $end_date;
        $liabilities->affects_gross = -1;
        $liabilities->filter = $request->all();
        $liabilities->start(Config::get('constants.accounts_equity_liabilities'));

        $bsheet['liabilities'] = $liabilities;
        //echo'<pre>';print_r($bsheet['liabilities']);echo'</pre>';exit;
        $bsheet['liabilities_data'] = $liabilities->generateSheet($liabilities, -1, 'c');

        $bsheet['liabilities_total'] = 0;
        if ($liabilities->cl_total_dc == 'c') {
            $bsheet['liabilities_total'] = $liabilities->cl_total;
        } else {
            $bsheet['liabilities_total'] = CoreAccounts::calculate($liabilities->cl_total, 0, 'n');
        }

        /* Assets */
        $assets = new AccountsList();
        $assets->only_opening = 0;
        $assets->start_date = $start_date;
        $assets->end_date = $end_date;
        $assets->affects_gross = -1;
        $assets->filter = $request->all();
        $assets->start(Config::get('constants.accounts_head_assets'));

        $bsheet['assets'] = $assets;
      ///echo'<pre>';print_r($bsheet['assets']);echo'</pre>';exit;
        //print_r($bsheet['assets']);exit;
        $bsheet['assets_data'] = $assets->generateSheet($assets, -1, 'd');

        $bsheet['assets_total'] = 0;
        if ($assets->cl_total_dc == 'd') {
            $bsheet['assets_total'] = $assets->cl_total;
        } else {
            $bsheet['assets_total'] = CoreAccounts::calculate($assets->cl_total, 0, 'n');
        }

        /* Profit and loss calculations */
        $income = new AccountsList();
        $income->only_opening = 0;
        $income->start_date = $start_date;
        $income->end_date = $end_date;
        $income->affects_gross = -1;
        $income->filter = $request->all();
        $income->start(Config::get('constants.accounts_head_incomes'));

        $expense = new AccountsList();
        $expense->only_opening = 0;
        $expense->start_date = $start_date;
        $expense->end_date = $end_date;
        $expense->affects_gross = -1;
        $expense->filter = $request->all();
        $expense->start(Config::get('constants.accounts_head_expenses'));

        if ($income->cl_total_dc == 'c') {
            $income_total = $income->cl_total;
        } else {
            $income_total = CoreAccounts::calculate($income->cl_total, 0, 'n');
        }
        if ($expense->cl_total_dc == 'd') {
            $expense_total = $expense->cl_total;
        } else {
            $expense_total = CoreAccounts::calculate($expense->cl_total, 0, 'n');
        }

        $bsheet['pandl'] = CoreAccounts::calculate($income_total, $expense_total, '-');

        /* Difference in opening balance */
        $bsheet['opdiff'] = Ledgers::getOpeningDiff();
        if (CoreAccounts::calculate($bsheet['opdiff']['opdiff_balance'], 0, '==')) {
            $bsheet['is_opdiff'] = false;
        } else {
            $bsheet['is_opdiff'] = true;
        }

        /**** Final balancesheet total ****/
        $bsheet['final_liabilities_total'] = $bsheet['liabilities_total'];
        $bsheet['final_assets_total'] = $bsheet['assets_total'];

        /* If net profit add to liabilities, if net loss add to assets */
        if (CoreAccounts::calculate($bsheet['pandl'], 0, '>=')) {
            $bsheet['final_liabilities_total'] = CoreAccounts::calculate(
                $bsheet['final_liabilities_total'],
                $bsheet['pandl'], '+');
        } else {
            $positive_pandl = CoreAccounts::calculate($bsheet['pandl'], 0, 'n');
            $bsheet['final_assets_total'] = CoreAccounts::calculate(
                $bsheet['final_assets_total'],
                $positive_pandl, '+');
        }

        /**
         * If difference in opening balance is Dr then subtract from
         * assets else subtract from liabilities
         */
        if ($bsheet['is_opdiff']) {
            if ($bsheet['opdiff']['opdiff_balance_dc'] == 'd') {
                $bsheet['final_assets_total'] = CoreAccounts::calculate(
                    $bsheet['final_assets_total'],
                    $bsheet['opdiff']['opdiff_balance'], '+');
            } else {
                $bsheet['final_liabilities_total'] = CoreAccounts::calculate(
                    $bsheet['final_liabilities_total'],
                    $bsheet['opdiff']['opdiff_balance'], '+');
            }
        }

        $DefaultCurrency = Currencies::getDefaultCurrencty();

        switch($request->get('medium_type')) {
            case 'web':
                return view('admin.account_reports.balance_sheet.report', compact('bsheet', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
            case 'print':
                return view('admin.account_reports.balance_sheet.report', compact('bsheet', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
            case 'excel':
                $this->balanceSheetExcel($bsheet, $DefaultCurrency, $start_date, $end_date);
                break;
            case 'pdf':
                $content = View::make('admin.account_reports.balance_sheet.report', compact('bsheet', 'start_date', 'end_date', 'DefaultCurrency'))->render();
                // Create an instance of the class:
                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
                // Write some HTML code:
                $mpdf->WriteHTML($content);
                // Output a PDF file directly to the browser
                $mpdf->Output('BalanceSheetReport.pdf','D');
                break;
            default:
                return view('admin.account_reports.balance_sheet.report', compact('bsheet', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
        }
    }

    /*
     * Function to export Trial Balance to Excel file.
     */
    protected function balanceSheetExcel($bsheet, $DefaultCurrency, $start_date, $end_date) {
        $spreadsheet = new Spreadsheet();

        // Set Default Attributes
        $spreadsheet->getProperties()->setCreator("Mustafa Mughal");
        $spreadsheet->getProperties()->setLastModifiedBy("Mustafa Mughal");
        $spreadsheet->getProperties()->setTitle("Balance Sheet");
        $spreadsheet->getProperties()->setSubject("Balance Sheet Report");
        $spreadsheet->getProperties()->setDescription("Function used to generate balance sheet in excel format.");
        $this->sheet = $spreadsheet->getActiveSheet();
        $this->sheet->setCellValue('A1', 'Balance sheet as on' . $end_date);

        $this->sheet->setCellValue('A2', 'Assets (Dr)');
        $this->sheet->setCellValue('B2', 'Amount (' . $DefaultCurrency->code . ')');

        // Print All Data into Excel File
        self::$bs_iterator = 3;
        $this->loopBalanceSheetExcel($bsheet['assets'], -1);
        /* Total Assets */
        $this->sheet->setCellValue('A' . self::$bs_iterator, 'Total');
        $this->sheet->setCellValue('B' . self::$bs_iterator, CoreAccounts::toCurrency('d', $bsheet['assets_total']));
        self::$bs_iterator++;

        // Put Empty rows
        $this->sheet->setCellValue('A' . self::$bs_iterator, '');
        $this->sheet->setCellValue('B' . self::$bs_iterator, '');
        self::$bs_iterator++;
        $this->sheet->setCellValue('A' . self::$bs_iterator, '');
        $this->sheet->setCellValue('B' . self::$bs_iterator, '');
        self::$bs_iterator++;

        /*
         * Liabilities Starts
         */
        $this->sheet->setCellValue('A2', 'Liabilities and Owners Equity (Cr)');
        $this->sheet->setCellValue('B2', 'Amount (' . $DefaultCurrency->code . ')');

        $this->loopBalanceSheetExcel($bsheet['liabilities'], -1);
        /* Total Liabilities */
        $this->sheet->setCellValue('A' . self::$bs_iterator, 'Total');
        $this->sheet->setCellValue('B' . self::$bs_iterator, CoreAccounts::toCurrency('c', $bsheet['liabilities_total']));
        self::$bs_iterator++;
        // Put Empty rows
        $this->sheet->setCellValue('A' . self::$bs_iterator, '');
        $this->sheet->setCellValue('B' . self::$bs_iterator, '');
        self::$bs_iterator++;
        $this->sheet->setCellValue('A' . self::$bs_iterator, '');
        $this->sheet->setCellValue('B' . self::$bs_iterator, '');
        self::$bs_iterator++;


        $writer = new Xlsx($spreadsheet);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="TrialBalanceReport.xlsx"');
        // Write file to the browser
        $writer->save('php://output');
    }

    /*
     * Function to iterator over N-Dimentional Data.
     */
    private function loopBalanceSheetExcel($account, $c = 0)
    {
        $counter = $c;
        if (!in_array($account->id, Config('constants.accounts_main_heads')))
        {
            $this->sheet->setCellValue('A' . self::$bs_iterator, html_entity_decode(AccountsList::printSpace($counter)). AccountsList::toCodeWithName($account->code, $account->name));

            if(count($account->children_groups)) {
                $this->sheet->setCellValue('B' . self::$bs_iterator, '');
            } else {
                $this->sheet->setCellValue('B' . self::$bs_iterator, html_entity_decode(CoreAccounts::toCurrency($account->cl_total_dc, $account->cl_total)));
            }

            self::$bs_iterator++;
        }
        if(count($account->children_groups)) {
            foreach ($account->children_groups as $id => $data)
            {
                $counter++;
                $this->loopBalanceSheetExcel($data, $counter);
                $counter--;
            }

            if (!in_array($account->id, Config('constants.accounts_main_heads'))) {
                $this->sheet->setCellValue('A' . self::$bs_iterator, html_entity_decode(AccountsList::printSpace($counter)). 'Total of ' . AccountsList::toCodeWithName($account->code, $account->name));
                $this->sheet->setCellValue('B' . self::$bs_iterator, html_entity_decode(CoreAccounts::toCurrency($account->cl_total_dc, $account->cl_total)));
                self::$bs_iterator++;
            }
        }
    }

    /*
     * ------------------------------------------------------
     * Balance Sheet Report End
     * ------------------------------------------------------
     */


    /*
     * ------------------------------------------------------
     * Profit & Loss Report Start
     * ------------------------------------------------------
     */

    /**
     * Show Profit & Loss.
     *
     * @return \Illuminate\Http\Response
     */
    public function profit_loss_comparative()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }

        // Get All Employees
      //  $Employees = Employees::pluckActiveOnly();
      //  $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
     //   $Departments = Departments::pluckActiveOnly();
     //   $Departments->prepend('Select a Department', '');

        // Get All Entry Types
     //   $EntryTypes = EntryTypes::pluckActiveOnly();
     //   $EntryTypes->prepend('Select an Entry Type', '');

        return view('admin.account_reports.profit_loss.comp_index', compact('Branches'));
    }
    public function profit_loss_report_comparative(Request $request)
    {
        if($request->get('date_range')) {
            $date_range = explode(' - ', $request->get('date_range'));
            $start_date = date('Y-m-d', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1]));
        } else {
            $start_date = null;
            $end_date = null;
        }
        if($request->get('date_range_previous')) {
            $date_range = explode(' - ', $request->get('date_range_previous'));
            $previous_start_date = date('Y-m-d', strtotime($date_range[0]));
            $previous_end_date = date('Y-m-d', strtotime($date_range[1]));
        } else {
            $previous_start_date = null;
            $previous_end_date = null;
        }
        
       /*   /**********************************************************************/
         /*********************** GROSS CALCULATIONS ***************************/
         /**********************************************************************/

         /* Gross P/L : Expenses */
            $gross_expenses = new AccountsList();
         $gross_expenses->only_opening = false;
         $gross_expenses->start_date = $start_date;
         $gross_expenses->end_date = $end_date;
         $gross_expenses->affects_gross = 1;
         $gross_expenses->filter = $request->all();
         $gross_expenses->start(Config::get('constants.accounts_expenses'));
    
         $pandl['gross_expenses'] = $gross_expenses;
         $pandl['gross_expenses_data'] = $gross_expenses->generateSheet($gross_expenses, -1, 'd');

         $pandl['gross_expense_total'] = 0;
         if ($gross_expenses->cl_total_dc == 'd') {
             $pandl['gross_expense_total'] = $gross_expenses->cl_total;
         } else {
             $pandl['gross_expense_total'] = CoreAccounts::calculate($gross_expenses->cl_total, 0, 'n');
         }

         /* Gross P/L : Incomes */
        $gross_incomes = new AccountsList();
         $gross_incomes->only_opening = false;
         $gross_incomes->start_date = $start_date;
         $gross_incomes->end_date = $end_date;
         $gross_incomes->affects_gross = 1;
         $gross_incomes->filter = $request->all();
         $gross_incomes->start(Config::get('constants.accounts_incomes'));
  //print_r($gross_incomes);
         $pandl['gross_incomes'] = $gross_incomes;
         $pandl['gross_incomes_data'] = $gross_incomes->generateSheet($gross_incomes, -1, 'c');

         $pandl['gross_income_total'] = 0;
         if ($gross_incomes->cl_total_dc == 'c') {
             $pandl['gross_income_total'] = $gross_incomes->cl_total;
         } else {
             $pandl['gross_income_total'] = CoreAccounts::calculate($gross_incomes->cl_total, 0, 'n');
         }

       /* Calculating Gross P/L */
         $pandl['gross_pl'] = CoreAccounts::calculate($pandl['gross_income_total'], $pandl['gross_expense_total'], '-');

        /**********************************************************************/
         /************************* NET CALCULATIONS ***************************/
         /**********************************************************************/

        /* Net P/L : Expenses */
        $net_expenses = new AccountsList();
        $gross_expenses->start_date = $start_date;
        $gross_expenses->end_date = $end_date;
        $gross_expenses->affects_gross = 0;
         $gross_expenses->filter = $request->all();
         $gross_expenses->start(Config::get('constants.accounts_expenses'));

         $pandl['net_expenses'] = $net_expenses;
         $pandl['net_expenses_data'] = $gross_expenses->generateSheet($gross_expenses, -1, 'd');

         $pandl['net_expense_total'] = 0;
         if ($net_expenses->cl_total_dc == 'd') {
             $pandl['net_expense_total'] = $net_expenses->cl_total;
         } else {
             $pandl['net_expense_total'] = CoreAccounts::calculate($net_expenses->cl_total, 0, 'n');
         }

         /* Net P/L : Incomes */
         $net_incomes = new AccountsList();
         $gross_incomes->start_date = $start_date;
        $gross_incomes->end_date = $end_date;
        $gross_incomes->affects_gross = 0;
         $gross_incomes->filter = $request->all();
         $gross_incomes->start(Config::get('constants.accounts_incomes'));

         $pandl['net_incomes'] = $net_incomes;
         $pandl['net_incomes_data'] = $gross_incomes->generateSheet($gross_incomes, -1, 'c');

         $pandl['net_income_total'] = 0;
         if ($net_incomes->cl_total_dc == 'c') {
             $pandl['net_income_total'] = $net_incomes->cl_total;
         } else {
             $pandl['net_income_total'] = CoreAccounts::calculate($net_incomes->cl_total, 0, 'n');
         }

        /* Calculating Net P/L */
         $pandl['net_pl'] = CoreAccounts::calculate($pandl['net_income_total'], $pandl['net_expense_total'], '-');
         $pandl['net_pl'] = CoreAccounts::calculate($pandl['net_pl'], $pandl['gross_pl'], '+'); 

           /**********************************************************************/
        /*********************** GROSS PREVIOUS CALCULATIONS  ***************************/
        /**********************************************************************/

        /* Gross P/L : Expenses */
        $gross_expenses_previous = new AccountsList();
        $gross_expenses_previous->only_opening = false;
        $gross_expenses_previous->start_date = $previous_start_date;
        $gross_expenses_previous->end_date = $previous_end_date;
        $gross_expenses_previous->affects_gross = 1;
        $gross_expenses_previous->filter = $request->all();
        $gross_expenses_previous->start(Config::get('constants.accounts_expenses'));
    
        $pandl['gross_expenses_previous'] = $gross_expenses_previous;
        $pandl['gross_expenses_previous_data'] = $gross_expenses_previous->generateSheet($gross_expenses_previous, -1, 'd');

        $pandl['gross_expense_previous_total'] = 0;
        if ($gross_expenses_previous->cl_total_dc == 'd') {
            $pandl['gross_expense_previous_total'] = $gross_expenses_previous->cl_total;
        } else {
            $pandl['gross_expense_previous_total'] = CoreAccounts::calculate($gross_expenses_previous->cl_total, 0, 'n');
        }

        /* Gross P/L : Incomes */
        $gross_incomes_previous = new AccountsList();
        $gross_incomes_previous->only_opening = false;
        $gross_incomes_previous->start_date = $previous_start_date;
        $gross_incomes_previous->end_date = $previous_end_date;
        $gross_incomes_previous->affects_gross = 1;
        $gross_incomes_previous->filter = $request->all();
        $gross_incomes_previous->start(Config::get('constants.accounts_incomes'));
 //print_r($gross_incomes);
        $pandl['gross_incomes_previous'] = $gross_incomes_previous;
        $pandl['gross_incomes_previous_data'] = $gross_incomes_previous->generateSheet($gross_incomes_previous, -1, 'c');

        $pandl['gross_income_previous_total'] = 0;
        if ($gross_incomes_previous->cl_total_dc == 'c') {
            $pandl['gross_income_previous_total'] = $gross_incomes_previous->cl_total;
        } else {
            $pandl['gross_income_previous_total'] = CoreAccounts::calculate($gross_incomes_previous->cl_total, 0, 'n');
        }

        /* Calculating Gross P/L */
        $pandl['gross_pl_previous'] = CoreAccounts::calculate($pandl['gross_income_previous_total'], $pandl['gross_expense_previous_total'], '-');

        /**********************************************************************/
        /************************* NET CALCULATIONS ***************************/
        /**********************************************************************/

        /* Net P/L : Expenses */
        $net_expenses_previous = new AccountsList();
        $gross_expenses_previous->start_date = $previous_start_date;
        $gross_expenses_previous->end_date = $previous_end_date;
        $gross_expenses_previous->affects_gross = 0;
        $gross_expenses_previous->filter = $request->all();
        $gross_expenses_previous->start(Config::get('constants.accounts_expenses'));

        $pandl['net_expenses_previous'] = $net_expenses_previous;
        $pandl['net_expenses_previous_data'] = $gross_expenses_previous->generateSheet($gross_expenses_previous, -1, 'd');

        $pandl['net_expense_previous_total'] = 0;
        if ($net_expenses_previous->cl_total_dc == 'd') {
            $pandl['net_expense_previous_total'] = $net_expenses_previous->cl_total;
        } else {
            $pandl['net_expense_previous_total'] = CoreAccounts::calculate($net_expenses_previous->cl_total, 0, 'n');
        }

        /* Net P/L : Incomes */
        $net_incomes_previous = new AccountsList();
        $gross_incomes_previous->start_date = $start_date;
        $gross_incomes_previous->end_date = $end_date;
        $gross_incomes_previous->affects_gross = 0;
        $gross_incomes_previous->filter = $request->all();
        $gross_incomes_previous->start(Config::get('constants.accounts_incomes'));

        $pandl['net_incomes_previous'] = $net_incomes_previous;
        $pandl['net_incomes_previous_data'] = $gross_incomes_previous->generateSheet($gross_incomes_previous, -1, 'c');

        $pandl['net_income_previous_total'] = 0;
        if ($net_incomes_previous->cl_total_dc == 'c') {
            $pandl['net_income_previous_total'] = $net_incomes_previous->cl_total;
        } else {
            $pandl['net_income_previous_total'] = CoreAccounts::calculate($net_incomes_previous->cl_total, 0, 'n');
        }

        /* Calculating Net P/L */
        $pandl['net_pl_previous'] = CoreAccounts::calculate($pandl['net_income_previous_total'], $pandl['net_expense_previous_total'], '-');
        $pandl['net_pl_previous'] = CoreAccounts::calculate($pandl['net_pl_previous'], $pandl['gross_pl_previous'], '+');






        $DefaultCurrency = Currencies::getDefaultCurrencty();

        switch($request->get('medium_type')) {
            case 'web':
                return view('admin.account_reports.profit_loss.comp_report', compact('pandl', 'start_date', 'end_date','previous_start_date', 'previous_end_date', 'DefaultCurrency'));
                break;
            case 'print':
                return view('admin.account_reports.profit_loss.comp_report', compact('pandl', 'start_date', 'end_date','previous_start_date', 'previous_end_date', 'DefaultCurrency'));
                break;
            case 'excel':
                $this->profitLossExcelcomp($pandl, $DefaultCurrency, $start_date, $end_date, $previous_start_date,$previous_end_date);
                break;
            case 'pdf':
                $content = View::make('admin.account_reports.profit_loss.comp_report', compact('pandl', 'start_date', 'end_date','previous_start_date', 'previous_end_date', 'DefaultCurrency'))->render();
                // Create an instance of the class:
                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
                // Write some HTML code:
                $mpdf->WriteHTML($content);
                // Output a PDF file directly to the browser
                $mpdf->Output('ProfitNLossReport.pdf','D');
                break;
            default:
                return view('admin.account_reports.profit_loss.comp_report', compact('pandl', 'start_date', 'end_date','previous_start_date', 'previous_end_date', 'DefaultCurrency'));
                break;
        }
    }

    protected function profitLossExcelcomp($pandl, $DefaultCurrency, $start_date, $end_date) {
        $spreadsheet = new Spreadsheet();

        // Set Default Attributes
        $spreadsheet->getProperties()->setCreator("Bilal Zulfiqar");
        $spreadsheet->getProperties()->setLastModifiedBy("Bilal ZUlfiqar");
        $spreadsheet->getProperties()->setTitle("Profit & Loss Report");
        $spreadsheet->getProperties()->setSubject("Profit & Loss Report");
        $spreadsheet->getProperties()->setDescription("Function used to generate P&L in excel format.");
        $this->sheet = $spreadsheet->getActiveSheet();
        $this->sheet->setCellValue('A1', 'Profit and Loss as on ' . $end_date);

        $this->sheet->setCellValue('A2', 'Incomes (Cr)');
        $this->sheet->setCellValue('B2', 'Amount (' . $DefaultCurrency->code . ')');

        // Print All Data into Excel File
        self::$pandl_iterator = 3;
        $this->loopProfitLossExcel($pandl['gross_incomes'], -1);
        /* Total Assets */
        $this->sheet->setCellValue('A' . self::$pandl_iterator, 'Gross Incomes');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, CoreAccounts::toCurrency('d', $pandl['gross_income_total']));
        self::$pandl_iterator++;

        // Put Empty rows
        $this->sheet->setCellValue('A' . self::$pandl_iterator, '');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
        self::$pandl_iterator++;
        $this->sheet->setCellValue('A' . self::$pandl_iterator, '');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
        self::$pandl_iterator++;

        /*
         * Liabilities Starts
         */
        $this->sheet->setCellValue('A2', 'Expenses (Dr)');
        $this->sheet->setCellValue('B2', 'Amount (' . $DefaultCurrency->code . ')');

        $this->loopProfitLossExcel($pandl['gross_expenses'], -1);
        /* Total Liabilities */
        $this->sheet->setCellValue('A' . self::$pandl_iterator, 'Gross Expenses');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, CoreAccounts::toCurrency('c', $pandl['gross_expense_total']));
        self::$pandl_iterator++;
        // Put Empty rows
        $this->sheet->setCellValue('A' . self::$pandl_iterator, '');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
        self::$pandl_iterator++;
        $this->sheet->setCellValue('A' . self::$pandl_iterator, '');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
        self::$pandl_iterator++;


        $writer = new Xlsx($spreadsheet);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="ProfitNLossReport.xlsx"');
        // Write file to the browser
        $writer->save('php://output');
    }


    public function profit_loss()
    {
        if (! Gate::allows('erp_account_reports_manage')) {
            return abort(401);
        }

        // Get All Employees
      //  $Employees = Employees::pluckActiveOnly();
      //  $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
     //   $Departments = Departments::pluckActiveOnly();
     //   $Departments->prepend('Select a Department', '');

        // Get All Entry Types
     //   $EntryTypes = EntryTypes::pluckActiveOnly();
     //   $EntryTypes->prepend('Select an Entry Type', '');

        return view('admin.account_reports.profit_loss.index', compact('Branches'));
    }

    /**
     * Load Report of Profit & Loss.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function profit_loss_report(Request $request)
    {
        if($request->get('date_range')) {
            $date_range = explode(' - ', $request->get('date_range'));
            $start_date = date('Y-m-d', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1]));
        } else {
            $start_date = null;
            $end_date = null;
        }

        /**********************************************************************/
        /*********************** GROSS CALCULATIONS ***************************/
        /**********************************************************************/

        /* Gross P/L : Expenses */
        $gross_expenses = new AccountsList();
        $gross_expenses->only_opening = false;
        $gross_expenses->start_date = $start_date;
        $gross_expenses->end_date = $end_date;
        $gross_expenses->affects_gross = 1;
        $gross_expenses->filter = $request->all();
        $gross_expenses->start(Config::get('constants.accounts_expenses'));
    
        $pandl['gross_expenses'] = $gross_expenses;
        $pandl['gross_expenses_data'] = $gross_expenses->generateSheet($gross_expenses, -1, 'd');

        $pandl['gross_expense_total'] = 0;
        if ($gross_expenses->cl_total_dc == 'd') {
            $pandl['gross_expense_total'] = $gross_expenses->cl_total;
        } else {
            $pandl['gross_expense_total'] = CoreAccounts::calculate($gross_expenses->cl_total, 0, 'n');
        }

        /* Gross P/L : Incomes */
        $gross_incomes = new AccountsList();
        $gross_incomes->only_opening = false;
        $gross_incomes->start_date = $start_date;
        $gross_incomes->end_date = $end_date;
        $gross_incomes->affects_gross = 1;
        $gross_incomes->filter = $request->all();
        $gross_incomes->start(Config::get('constants.accounts_incomes'));
 //print_r($gross_incomes);
        $pandl['gross_incomes'] = $gross_incomes;
        $pandl['gross_incomes_data'] = $gross_incomes->generateSheet($gross_incomes, -1, 'c');

        $pandl['gross_income_total'] = 0;
        if ($gross_incomes->cl_total_dc == 'c') {
            $pandl['gross_income_total'] = $gross_incomes->cl_total;
        } else {
            $pandl['gross_income_total'] = CoreAccounts::calculate($gross_incomes->cl_total, 0, 'n');
        }

        /* Calculating Gross P/L */
        $pandl['gross_pl'] = CoreAccounts::calculate($pandl['gross_income_total'], $pandl['gross_expense_total'], '-');

        /**********************************************************************/
        /************************* NET CALCULATIONS ***************************/
        /**********************************************************************/

        /* Net P/L : Expenses */
        $net_expenses = new AccountsList();
        $gross_expenses->start_date = $start_date;
        $gross_expenses->end_date = $end_date;
        $gross_expenses->affects_gross = 0;
        $gross_expenses->filter = $request->all();
        $gross_expenses->start(Config::get('constants.accounts_expenses'));

        $pandl['net_expenses'] = $net_expenses;
        $pandl['net_expenses_data'] = $gross_expenses->generateSheet($gross_expenses, -1, 'd');

        $pandl['net_expense_total'] = 0;
        if ($net_expenses->cl_total_dc == 'd') {
            $pandl['net_expense_total'] = $net_expenses->cl_total;
        } else {
            $pandl['net_expense_total'] = CoreAccounts::calculate($net_expenses->cl_total, 0, 'n');
        }

        /* Net P/L : Incomes */
        $net_incomes = new AccountsList();
        $gross_incomes->start_date = $start_date;
        $gross_incomes->end_date = $end_date;
        $gross_incomes->affects_gross = 0;
        $gross_incomes->filter = $request->all();
        $gross_incomes->start(Config::get('constants.accounts_incomes'));

        $pandl['net_incomes'] = $net_incomes;
        $pandl['net_incomes_data'] = $gross_incomes->generateSheet($gross_incomes, -1, 'c');

        $pandl['net_income_total'] = 0;
        if ($net_incomes->cl_total_dc == 'c') {
            $pandl['net_income_total'] = $net_incomes->cl_total;
        } else {
            $pandl['net_income_total'] = CoreAccounts::calculate($net_incomes->cl_total, 0, 'n');
        }

        /* Calculating Net P/L */
        $pandl['net_pl'] = CoreAccounts::calculate($pandl['net_income_total'], $pandl['net_expense_total'], '-');
        $pandl['net_pl'] = CoreAccounts::calculate($pandl['net_pl'], $pandl['gross_pl'], '+');

        $DefaultCurrency = Currencies::getDefaultCurrencty();

        switch($request->get('medium_type')) {
            case 'web':
                return view('admin.account_reports.profit_loss.report', compact('pandl', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
            case 'print':
                return view('admin.account_reports.profit_loss.report', compact('pandl', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
            case 'excel':
                $this->profitLossExcel($pandl, $DefaultCurrency, $start_date, $end_date);
                break;
            case 'pdf':
                $content = View::make('admin.account_reports.profit_loss.report', compact('pandl', 'start_date', 'end_date', 'DefaultCurrency'))->render();
                // Create an instance of the class:
                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
                // Write some HTML code:
                $mpdf->WriteHTML($content);
                // Output a PDF file directly to the browser
                $mpdf->Output('ProfitNLossReport.pdf','D');
                break;
            default:
                return view('admin.account_reports.profit_loss.report', compact('pandl', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
        }
    }

    /*
     * Function to export Trial Balance to Excel file.
     */
    protected function profitLossExcel($pandl, $DefaultCurrency, $start_date, $end_date) {
        $spreadsheet = new Spreadsheet();

        // Set Default Attributes
        $spreadsheet->getProperties()->setCreator("Mustafa Mughal");
        $spreadsheet->getProperties()->setLastModifiedBy("Mustafa Mughal");
        $spreadsheet->getProperties()->setTitle("Balance Sheet");
        $spreadsheet->getProperties()->setSubject("Balance Sheet Report");
        $spreadsheet->getProperties()->setDescription("Function used to generate balance sheet in excel format.");
        $this->sheet = $spreadsheet->getActiveSheet();
        $this->sheet->setCellValue('A1', 'Profit and Loss as on ' . $end_date);

        $this->sheet->setCellValue('A2', 'Incomes (Cr)');
        $this->sheet->setCellValue('B2', 'Amount (' . $DefaultCurrency->code . ')');

        // Print All Data into Excel File
        self::$pandl_iterator = 3;
        $this->loopProfitLossExcel($pandl['gross_incomes'], -1);
        /* Total Assets */
        $this->sheet->setCellValue('A' . self::$pandl_iterator, 'Gross Incomes');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, CoreAccounts::toCurrency('d', $pandl['gross_income_total']));
        self::$pandl_iterator++;

        // Put Empty rows
        $this->sheet->setCellValue('A' . self::$pandl_iterator, '');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
        self::$pandl_iterator++;
        $this->sheet->setCellValue('A' . self::$pandl_iterator, '');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
        self::$pandl_iterator++;

        /*
         * Liabilities Starts
         */
        $this->sheet->setCellValue('A2', 'Expenses (Dr)');
        $this->sheet->setCellValue('B2', 'Amount (' . $DefaultCurrency->code . ')');

        $this->loopProfitLossExcel($pandl['gross_expenses'], -1);
        /* Total Liabilities */
        $this->sheet->setCellValue('A' . self::$pandl_iterator, 'Gross Expenses');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, CoreAccounts::toCurrency('c', $pandl['gross_expense_total']));
        self::$pandl_iterator++;
        // Put Empty rows
        $this->sheet->setCellValue('A' . self::$pandl_iterator, '');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
        self::$pandl_iterator++;
        $this->sheet->setCellValue('A' . self::$pandl_iterator, '');
        $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
        self::$pandl_iterator++;


        $writer = new Xlsx($spreadsheet);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="ProfitNLossReport.xlsx"');
        // Write file to the browser
        $writer->save('php://output');
    }

    /*
     * Function to iterator over N-Dimentional Data.
     */
    private function loopProfitLossExcel($account, $c = 0)
    {
        $counter = $c;
        if (!in_array($account->id, Config('constants.accounts_main_heads')))
        {
            $this->sheet->setCellValue('A' . self::$pandl_iterator, html_entity_decode(AccountsList::printSpace($counter)). AccountsList::toCodeWithName($account->code, $account->name));

            if(count($account->children_groups)) {
                $this->sheet->setCellValue('B' . self::$pandl_iterator, '');
            } else {
                $this->sheet->setCellValue('B' . self::$pandl_iterator, html_entity_decode(CoreAccounts::toCurrency($account->cl_total_dc, $account->cl_total)));
            }

            self::$pandl_iterator++;
        }
        if(count($account->children_groups)) {
            foreach ($account->children_groups as $id => $data)
            {
                $counter++;
                $this->loopProfitLossExcel($data, $counter);
                $counter--;
            }

            if (!in_array($account->id, Config('constants.accounts_main_heads'))) {
                $this->sheet->setCellValue('A' . self::$pandl_iterator, html_entity_decode(AccountsList::printSpace($counter)). 'Total of ' . AccountsList::toCodeWithName($account->code, $account->name));
                $this->sheet->setCellValue('B' . self::$pandl_iterator, html_entity_decode(CoreAccounts::toCurrency($account->cl_total_dc, $account->cl_total)));
                self::$pandl_iterator++;
            }
        }
    }

    /*
     * ------------------------------------------------------
     * Balance Sheet Report End
     * ------------------------------------------------------
     */

    /*
     * ------------------------------------------------------
     * Ledger Statement Report Start
     * ------------------------------------------------------
     */

    /**
     * Show Ledger Statement.
     *
     * @return \Illuminate\Http\Response
     */
    public function ledger_statement()
    {
        if (! Gate::allows('erp_account_reports_ledger_statement')) {
            return abort(401);
        }

        // Get All Employees
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        // Get All Account Types
        $AccountTypes = AccountTypes::getActiveListDropdown(true);

        // Get All Entry Types
        $EntryTypes = EntryTypes::pluckActiveOnly();
        $EntryTypes->prepend('Select an Entry Type', '');

        /* Create list of ledgers to pass to view */
        $parentGroups = new LedgersTree();
        $parentGroups->current_id = -1;
        $parentGroups->restriction_bankcash = 1;
        $parentGroups->build(0);
        $parentGroups->toList($parentGroups, -1);
        $Ledgers = $parentGroups->ledgerList;

        return view('admin.account_reports.ledger_statement.index', compact('Employees', 'Branches', 'Departments', 'AccountTypes', 'EntryTypes', 'Groups', 'Ledgers'));
    }

    /**
     * Load Report of Trial Balance.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function ledger_statement_report(Request $request)
    {
        if($request->get('date_range')) {
            $date_range = explode(' - ', $request->get('date_range'));
            $start_date = date('Y-m-d', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1]));
        } else {
            $start_date = null;
            $end_date = null;
        }

        $Ledger = Ledgers::find($request->get('group_id'));

        if(!$Ledger) {
            return;
        }
      // echo '<pre>';print_r($start_date);echo'</pre>';exit;
        // Openinig Balance
        $op = Ledgers::openingBalance($Ledger->id, $start_date, $request->all());
        //$op = $Ledger->opening_balance;

        // Closing Balance
        $cl = Ledgers::closingBalance($Ledger->id, null, $end_date);

        $where = array(
            'entry_items.status' => 0,
            'entry_items.ledger_id' => $Ledger->id,
        );

        // Set Branch ID if exists
        if($request->get('branch_id')) {
            $where['entries.branch_id'] = $request->get('branch_id');
        }
        // Set Department ID if exists
        if($request->get('department_id')) {
            $where['entries.department_id'] = $request->get('department_id');
        }
        // Set Employee ID if exists
        if($request->get('employee_id')) {
            $where['entries.employee_id'] = $request->get('employee_id');
        }

        $query = Entries::join('entry_items', 'entries.id', '=', 'entry_items.entry_id')
        ->where($where);
        if(!is_null($start_date) && $start_date) {
            $query->where('entry_items.voucher_date','>=', $start_date);
        }
        if(!is_null($end_date) && $end_date) {
            $query->where('entry_items.voucher_date','<=', $end_date);
        }

        $Entries = $query->get();
      //echo'<pre>'; print_r($Entries);echo'</pre>';exit;
        $DefaultCurrency = Currencies::getDefaultCurrencty();
        $EntryTypes = EntryTypes::all()->getDictionary();

        switch($request->get('medium_type')) {
            case 'web':
                return view('admin.account_reports.ledger_statement.report', compact('Entries', 'start_date', 'end_date', 'DefaultCurrency', 'op', 'cl', 'Ledger', 'EntryTypes'));
                break;
            case 'print':
                return view('admin.account_reports.ledger_statement.report', compact('Entries', 'start_date', 'end_date', 'DefaultCurrency', 'op', 'cl', 'Ledger', 'EntryTypes'));
                break;
            case 'excel':
                $this->ledgerStatementExcel($Entries, $start_date, $end_date, $DefaultCurrency, $op, $cl, $Ledger, $EntryTypes);
                break;
            case 'pdf':
                $content = View::make('admin.account_reports.ledger_statement.report', compact('Entries', 'start_date', 'end_date', 'DefaultCurrency', 'op', 'cl', 'Ledger', 'EntryTypes'))->render();
                // Create an instance of the class:
                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
                // Write some HTML code:
                $mpdf->WriteHTML($content);
                // Output a PDF file directly to the browser
                $mpdf->Output('TrialBalanceReport.pdf','D');
                break;
            default:
                return view('admin.account_reports.ledger_statement.report', compact('Entries', 'start_date', 'end_date', 'DefaultCurrency', 'op', 'cl', 'Ledger', 'EntryTypes'));
                break;
        }
    }


    /*
     * Function to export Trial Balance to Excel file.
     */
    protected function ledgerStatementExcel($Entries, $start_date, $end_date, $DefaultCurrency, $op, $cl, $Ledger, $EntryTypes) {
        $spreadsheet = new Spreadsheet();

        // Set Default Attributes
        $spreadsheet->getProperties()->setCreator("Mustafa Mughal");
        $spreadsheet->getProperties()->setLastModifiedBy("Mustafa Mughal");
        $spreadsheet->getProperties()->setTitle("Ledger Statement");
        $spreadsheet->getProperties()->setSubject("Ledger Statement Report");
        $spreadsheet->getProperties()->setDescription("Function used to generate ledger statement in excel format.");
        $this->sheet = $spreadsheet->getActiveSheet();
        $this->sheet->setCellValue('A1', 'Ledger Statement for ' . $Ledger->name . ' from '. $start_date . ' to ' . $end_date);

        $this->sheet->setCellValue('A3', 'Date');
        $this->sheet->setCellValue('B3', 'Number');
        $this->sheet->setCellValue('C3', 'Ledger');
        $this->sheet->setCellValue('D3', 'Entry Type');
        $this->sheet->setCellValue('E3', 'Debit (' . $DefaultCurrency->code . ')');
        $this->sheet->setCellValue('F3', 'Credit (' . $DefaultCurrency->code . ')');
        $this->sheet->setCellValue('G3', 'Balance (' . $DefaultCurrency->code . ')');

        $entry_balance['amount'] = $op['amount'];
        $entry_balance['dc'] = $op['dc'];

        $this->sheet->setCellValue('A4', 'Current opening balance');
        $this->sheet->setCellValue('G4', CoreAccounts::toCurrency($entry_balance['dc'], $entry_balance['amount']));

        self::$ls_iterator = 5;
        foreach ($Entries as $entry) {
            /* Calculate current entry balance */
            $entry_balance = CoreAccounts::calculate_withdc(
                $entry_balance['amount'], $entry_balance['dc'],
                $entry['amount'], $entry['dc']
            );

            $this->sheet->setCellValue('A' . self::$ls_iterator, $entry->voucher_date);
            $this->sheet->setCellValue('B' . self::$ls_iterator, $entry->number);
            $this->sheet->setCellValue('C' . self::$ls_iterator, Ledgers::entryLedgers($entry->id));
            $this->sheet->setCellValue('D' . self::$ls_iterator, $EntryTypes[$entry->entry_type_id]->code);
            if ($entry['dc'] == 'd') {
                $this->sheet->setCellValue('E' . self::$ls_iterator, CoreAccounts::toCurrency('d', $entry['amount']));
                $this->sheet->setCellValue('F' . self::$ls_iterator, '');
            } elseif ($entry['dc'] == 'c'){
                $this->sheet->setCellValue('E' . self::$ls_iterator, '');
                $this->sheet->setCellValue('F' . self::$ls_iterator, CoreAccounts::toCurrency('c', $entry['amount']));
            } else {
                $this->sheet->setCellValue('E' . self::$ls_iterator, '');
                $this->sheet->setCellValue('F' . self::$ls_iterator, '');
            }
            $this->sheet->setCellValue('G' . self::$ls_iterator, CoreAccounts::toCurrency($entry_balance['dc'], $entry_balance['amount']));

            self::$ls_iterator++;
        }

        $this->sheet->setCellValue('A' . self::$ls_iterator, 'Current closing balance');
        $this->sheet->setCellValue('G' . self::$ls_iterator, CoreAccounts::toCurrency($entry_balance['dc'], $entry_balance['amount']));

        self::$ls_iterator = 5;


        $writer = new Xlsx($spreadsheet);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="LedgerStatementReport.xlsx"');
        // Write file to the browser
        $writer->save('php://output');
    }

    /*
     * ------------------------------------------------------
     * Ledger Statement Report End
     * ------------------------------------------------------
     */
    public function company(Request $request){
        $input = $request->company_id;
        $data = Branches::where('company_id','=',$input)->get();
        return response()->json($data);
    }
}
