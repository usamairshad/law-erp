<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Admin\Groups;
use App\Helpers\GroupsTree;
use App\Helpers\CoreAccounts;
use Auth;
use Config;
use Illuminate\Support\Str;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_cities_manage')) {
            return abort(401);
        }
        
        $City = City::all();

        return view('admin.city.index', compact('City'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_cities_create')) {
            return abort(401);
        }
        $country = Country::all()->pluck('name','id');
        return view('admin.city.create', compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lower =  mb_strtolower($request['name'], 'UTF-8');
        if (! Gate::allows('erp_cities_create')) {
            return abort(401);
        }
        $this->validate($request, [
            
            'name' => 'required | alpha | unique:erp_city',

        ]);
        if ($request['name'] == null){
            return redirect()->back()->with('error','Name field is required');

        }
        $cities = City::all()->where('name',$request['name']);
        foreach ($cities as $city){
            if ($request['name'] == $city['name']){
                return redirect()->back()->with('error','Name is already exist');
            }

        }
        $City = City::create([
            'name' => $lower,
            'country_id' => $request['country_id'],
        ]);
       
        $city_group = Groups::where('company_id','!=',null)->count();
      
        if($city_group> 0){
            $company_count = Groups::where('company_id','!=',null)->get();
           
            if(count($company_count) > 0){ 
                foreach ($company_count as $key) {
                     
                     $param['parent_id'] = $key->id;

                     $param['company_id'] = $request['company_id'];
                     $param['city_id'] =  $City->id;
                     $param['name'] = City::whereId($City->id)->value('name');
                     $param['code']=$key->code;
                     $response = CoreAccounts::createGroup($param);  
                    
                }  
            }
            
        }
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.city.index');




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (! Gate::allows('erp_cities_edit')) {
            return abort(401);
        }
        $City = City::findOrFail($id);
        $country = Country::all()->pluck('name','id');
        return view('admin.city.edit', compact('City','country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lower =  mb_strtolower($request['name'], 'UTF-8');
        if ($request['name'] == null){
            return redirect()->back()->with('error','Name field is required');

        }
        $cities = City::all()->where('name',$request['name']);
        foreach ($cities as $city){
            if ($request['name'] == $city['name']){
                return redirect()->back()->with('error','Name is already exist');
            }

        }
        City::where('id',$id)->update([
            'name' => $request['name'],
            'country_id' => $request['country_id'],
        ]);
        flash('Record has been updated successfully.')->success()->important();
        return redirect()->route('admin.city.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
