<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\AcademicYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Academics\Session;
use App\Models\Academics\ActiveSession;
use Auth;

class AcademicYearController extends Controller
{
    /**
     * Display a listing of FinancialYear.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if (! Gate::allows('erp_academic_years_manage')) {
           return abort(401);
       }
        $AcademicYear = AcademicYear::OrderBy('created_at','desc')->get();

        return view('admin.academic_year.index', compact('AcademicYear'));
    }

    /**
     * Show the form for creating new FinancialYear.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if (! Gate::allows('erp_academic_years_create')) {
           return abort(401);
       }
        $active_session = Session::pluck('title','id');
        
        return view('admin.academic_year.create', compact('active_session'));
    }

    /**
     * Store a newly created FinancialYear in storage.
     *
     * @param  \App\Http\Requests\Admin\FinancialYears\StoreUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if (! Gate::allows('erp_academic_years_create')) {
           return abort(401);
       }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        AcademicYear::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.academic_year.index');
    }


    /**
     * Show the form for editing FinancialYear.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        if (! Gate::allows('erp_academic_years_edit')) {
//            return abort(401);
//        }
        $AcademicYear = AcademicYear::findOrFail($id);

        return view('admin.academic_year.edit', compact('AcademicYear'));
    }

    /**
     * Update FinancialYear in storage.
     *
     * @param  \App\Http\Requests\Admin\FinancialYears\StoreUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        if (! Gate::allows('erp_academic_years_edit')) {
//            return abort(401);
//        }

        if($request['is_default']) {
            // Default currency is changed, set all other financial_years as non-default to set this record as 'default'
            AcademicYear::query()->update(['is_default' => 0]);
        }

        $AcademicYear = AcademicYear::findOrFail($id);
        $AcademicYear->update([
            'start_date' => $request['start_date'],
            'start_end' => $request['start_end'],
            'name' => $request['name'],
            'code' => $request['code'],
            'symbol' => $request['symbol'],
            'rate' => $request['rate'],
            'is_default' => $request['is_default'],
            'updated_by' => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.academic_year.index');
    }


    /**
     * Remove FinancialYear from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        if (! Gate::allows('erp_academic_years_destroy')) {
//            return abort(401);
//        }
        $AcademicYear = AcademicYear::findOrFail($id);
        $AcademicYear->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.academic_year.index');
    }

    /**
     * Activate FinancialYear from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
       if (! Gate::allows('erp_academic_years_active')) {
           return abort(401);
       }

        $AcademicYear = AcademicYear::findOrFail($id);
        $AcademicYear->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.academic_year.index');
    }

    /**
     * Inactivate FinancialYear from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
       if (! Gate::allows('erp_academic_years_inactive')) {
           return abort(401);
       }

        $AcademicYear = AcademicYear::findOrFail($id);
        $AcademicYear->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.academic_year.index');
    }

    /**
     * Delete all selected FinancialYear at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
//        if (! Gate::allows('erp_academic_years_mass_destroy')) {
//            return abort(401);
//        }
        if ($request->input('ids')) {
            $entries = AcademicYear::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
