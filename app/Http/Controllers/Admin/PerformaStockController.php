<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Admin\PerformaStockModel;
use App\Models\Admin\StockItemsModel;
use App\Models\Admin\LcBankModel;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin\Orders;
use App\Models\Admin\SuppliersModel;
use App\Models\Admin\Branches;
use App\Models\Admin\ProductsModel;
use App\Http\Requests\Admin\StorePerformaStockRequest;
use App\Http\Requests\Admin\UpdatePerformaStockRequest;

use Validator;
use Auth;

class PerformaStockController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        $Performastock  = PerformaStockModel::selectRaw('performastock.id as perf_id,performastock.*, lcbank.*')
            ->Join('lcbank','performastock.lc_no','=','lcbank.id')->get();
        return view('admin.performastock.index', compact('Performastock'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_lcbank_create')) {
            return abort(401);
        }
        $LcBankModel = LcBankModel::all();
        return view('admin.performastock.create', compact('LcBankModel'));
    }

    public function performaInvoice()
    {
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        $SuppliersModel = SuppliersModel::all();
        $Branches = Branches::all();
        $Products = ProductsModel::all();

        return view('admin.performastock.sale_voucher.create', compact('SuppliersModel','Branches','Products'));
    }


    public function lccosting(PerformaStockModel $performaStockModel)
    {
        if (! Gate::allows('erp_lcbank_manage')) {
        return abort(401);
    }
        //dd($performaStockModel);
        return view('admin.performastock.costing',compact('performaStockModel'));
    }

    public static function updateImport($data)
    {
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        dd($data);
        return view('admin.performastock.costing',compact('performaStockModel'));
    }
    public static function productImport($id)
    {
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        $performastock =  PerformaStockModel::where('lc_no',$id)->get();
         return view('admin.lcbank.productlist',compact('performastock'));
    }




    public function submitcosting(PerformaStockModel $PerformaStockModel,Request $request)
    {
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        //dd($PerformaStockModel);
        $record_id = $PerformaStockModel->lc_no;
        $record_amount_rs = $PerformaStockModel->amount_rs;
        $total_qty = $PerformaStockModel->total_qty;
        $rel_qty = $PerformaStockModel->rel_qty;
        $users = DB::table('performastock')
            ->select(DB::raw('sum(amount_rs) as amount_count'))
            ->where('lc_no', '=', $record_id)
            ->first();
        $total_amount_rs = $users->amount_count;
        $bankcharges = 0;
        $mov_bndg = 0;
        $duty = 0;
        $insurance = round($request['insurance']*$record_amount_rs/$total_amount_rs);
        $freight = round($request['freight']*$record_amount_rs/$total_amount_rs);
        $profit_investment = round($request['profit_investment']*$record_amount_rs/$total_amount_rs);
        $murahba_profit = round($request['murahba_profit']*$record_amount_rs/$total_amount_rs);
        $bank_charg = round($bankcharges*$record_amount_rs/$total_amount_rs);
        $movment_bndg = round($mov_bndg*$record_amount_rs/$total_amount_rs);
        $total_duty = round($duty*$record_amount_rs/$total_amount_rs);
        $full_amt = round($record_amount_rs+$insurance+$freight+$profit_investment+$murahba_profit+$bank_charg+$movment_bndg+$total_duty);
        $bal_qty =$total_qty - $rel_qty;
        $a_cost_pu = round($full_amt/$total_qty);
        $balance_lc = round($bal_qty*$a_cost_pu);
        $PerformaStockModel->update([
                'insurance' => $insurance,
                'bank_charges' => $bank_charg,
                'freight' => $freight,
                'profit_investment' => $profit_investment,
                'mvmnt_bndg' => $movment_bndg,
                'Duty' => $total_duty,
                'f_total_amount' => $full_amt,
                'af_cp_unit' => $a_cost_pu,
                'balance_lc' =>$balance_lc,
                'af_murahba_prft' =>$murahba_profit,
                'af_balanc_qty' => $bal_qty,
                //'created_by' => Auth::user()->id,
                //'updated_by' => Auth::user()->id,
            ]);

        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.performastock.index');
        //$PerformaStockModel = PerformaStockModel::all();
        //return view('admin.performastock.create', compact('LcBankModel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_lcbank_create')) {
            return abort(401);
        }
        $data = $request->all();
        $releasstock = array();
        $counts = 1;
        for($h=0;$h<count($data['product_detail_id']);$h++){
            $releasstock[$counts]['lc_id'] = $data['lc_id'][$h];
            $releasstock[$counts]['product_id'] = $data['product_detail_id'][$h];
            $releasstock[$counts]['rel_qty'] = $data['rel_qty'][$h];
            $counts++;
        }
        $inventory = array();
        $counter = 1;
        for($i=0;$i<count($data['product_detail_id']);$i++){
            $inventory[$counter]['product_id'] = $data['product_detail_id'][$i];
            $inventory[$counter]['rel_qty'] = $data['rel_qty'][$i];
            $inventory[$counter]['unit_price'] = $data['unit_price'][$i];
            $inventory[$counter]['branch_id'] = 1;
            $counter++;
        }
        //$result = StockItemsController::importInventory($inventory,$releasstock);
        for($a=0;$a<count($data['product_detail_id']);$a++){
            PerformaStockModel::where('id',$data['row_id'])->update([
                'rel_qty' => $data['rel_qty'][$a],
                'balanace_qty' => $data['bal_qty'][$a],
                //'balance_lc' => $data['bal_amount'][$a],
                //'total_paid' => $data['total_paid'][$a],
                'updated_by'=> Auth::user()->id,
            ]);
        }
         flash('Record has been created successfully.')->success()->important();

         return redirect()->route('admin.stockitems.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_lcbank_edit')) {
            return abort(401);
        }
        $Performastock = PerformaStockModel::findOrFail($id);
        $Orders = Orders::all();
        return view('admin.performastock.edit', compact('Performastock','Orders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_lcbank_edit')) {
            return abort(401);
        }
        $Performastock = PerformaStockModel::findOrFail($id);
        $Performastock->update([
            'product_name' => $request['product_name'],
            'order_id' => $request['order_id'],
            'product_model' => $request['product_model'],
            'quantity' => $request['quantity'],
            'created_by' => 0,
            'updated_by' => 0,
            'deleted_by' => 0,
        ]);

        return redirect()->route('admin.performastock.index');
    }

    public function addItem(Request $request)
    {
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        //print_r($request->all());exit;
        $users = DB::table('stockitems')
                  ->select('closing_stock','id','stock_value')
                  ->where('sup_id','=',$request["sup_id"])
                  ->where('cat_id','=',$request["cat_id"])
                  ->where('st_model','=',$request["st_model"])
                  ->where('st_name','=',$request["st_name"])->first();


        if(!empty($users)){
            $clos_stock = $users->closing_stock;
            $total_st_stock = $users->stock_value;
            $opening_stock = $clos_stock;
            $closing_stock = $clos_stock+$request['quantity'];
            $movment_stock = $opening_stock+$request['quantity']- $closing_stock;
            $stock_value = $closing_stock*$request['af_cp_unit'];
                DB::table('stockitems')->where('id', $users->id)->update(
                [
                    'opening_stock'      => $opening_stock,
                    'purchase_stock'     => $request['quantity'],
                    'closing_stock'      => $closing_stock,
                    'moments_stock'      => $movment_stock,
                    'stock_value'        => $stock_value,
                    'st_unit_price'      => $request['af_cp_unit'],
                ]
            );

           // DB::update("update stockitems set opening_stock =  ".$closing_stock + $request['quantity'].",". purchase_stock = ",");

        }else{
            $opening_stock = 0;
            $closing_stock = $request['quantity'];
            $movment_stock = $opening_stock+$request['quantity']- $closing_stock;
            $stock_value = $closing_stock*$request['af_cp_unit'];
                StockItemsModel::create([
                    'sup_id'            => $request['sup_id'],
                    'cat_id'            => $request['cat_id'],
                    'st_name'           => $request['st_name'],
                    'st_model'          => $request['st_model'],
                    'opening_stock'      => $opening_stock,
                    'purchase_stock'     => $request['quantity'],
                    'closing_stock'      => $closing_stock,
                    'moments_stock'      => $movment_stock,
                    'stock_value'        => $stock_value,
                    'st_unit_price'      => $request['af_cp_unit'],
                ]);

        return response()->json($request->all());

    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_lcbank_destroy')) {
            return abort(401);
        }
        $Performastock = PerformaStockModel::findOrFail($id);
        $Performastock->delete();

        return redirect()->route('admin.performastock.index');
    }

    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_lcbank_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = PerformaStockModel::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
