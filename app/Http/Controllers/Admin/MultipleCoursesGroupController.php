<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\ProgramGroup;
use App\Models\Academics\Section;
use App\Models\Academics\Program;
use App\Models\Academics\AssignClassSection;
use App\Models\Academics\Classes;
use App\Models\Academics\Course;
use App\Models\Academics\CourseGroup;
use App\Models\Academics\ProgramGroupCourse;
use App\Models\Academics\ClassGroupCourse;
use App\Models\Academics\CourseGroupCourse;
use Auth;
use Config;
use App\Helpers\Helper;

class MultipleCoursesGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.multiple-courses-groups.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boards= Board::all();
        $courses= Course::all();
        $classes= Classes::all();
        return view('admin.multiple-courses-groups.multiple-program-group-course-create',compact('boards','courses','classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $input = $request->all();
        if($request->group == 'program_group_course')
        {   
            foreach ($input['course_id'] as $key => $value) {
                ProgramGroupCourse::create([
                'program_group_id' => $input['program_group_id'],
                'course_id' => $value
            ]);
            }
            
        }
        elseif ($request->group == 'class_group_course') {
            foreach ($input['course_id'] as $key => $value) {
                ClassGroupCourse::create([
                'class_group_id' => $input['class_group_id'],
                'course_id' => $value
            ]);
            }
        }
        elseif ($request->group == 'course_group_course') {
            foreach ($input['course_id'] as $key => $value) {
                CourseGroupCourse::create([
                'course_group_id' => $input['course_group_id'],
                'course_id' => $value
            ]);
            }
        }
         return redirect()->route('admin.multiple-courses-groups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function classGroupCourseCreate(Request $request)
    {

        $boards= Board::all();
        $courses= Course::all();
        $classes= Classes::all();
        return view('admin.multiple-courses-groups.multiple-class-group-course-create',compact('boards','courses','classes'));
    }
    public function courseGroupCourseCreate(Request $request)
    {

        $boards= Board::all();
        $courses= Course::all();
        $classes= Classes::all();
        return view('admin.multiple-courses-groups.multiple-course-group-course-create',compact('boards','courses','classes'));   
    }
}
