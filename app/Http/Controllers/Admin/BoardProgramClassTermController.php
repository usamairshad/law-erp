<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Academics\BPC_Term;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\Classes;
use App\Models\Academics\Program;
use App\Models\Academics\Term;
use Auth;
use Config;
use App\Helpers\Helper;

class BoardProgramClassTermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $board_program_class = BPC_Term::distinct('bpc_id')->pluck('bpc_id');   
        $class_sections =[];

        if($board_program_class)
        {
            foreach ($board_program_class as $key => $b_p_c) {
                $board = BPC_Term::where('bpc_id',$b_p_c)->first()->getBoardProgramClass->getBoard;
                $program = BPC_Term::where('bpc_id',$b_p_c)->first()->getBoardProgramClass->getProgram;
                $class = BPC_Term::where('bpc_id',$b_p_c)->first()->getBoardProgramClass->getClass;
                $terms = BPC_Term::where('bpc_id', $b_p_c)->pluck('term_id');
                $class_sections[$key]['board'] = $board;
                $class_sections[$key]['program'] = $program;
                $class_sections[$key]['class'] = $class;
                foreach ($terms as $k => $s) {
                   $terms[$k] = Term::find($s)->getName();
                }
                $class_sections[$key]['terms'] = $terms;
            }
        }

        return view('admin.bpc_term.index',compact('class_sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $programs= Program::all();
        $boards= Board::all();
        // dd($boards);
        $terms= Term::all();
        return view('admin.bpc_term.add',compact('programs','boards','terms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $input=$request->all();
        $bpc_id= BoardProgramClass::where('board_id',$input['board_id'])->where('program_id',$input['program_id'])->where('class_id',$input['class_id'])->value('id');
        foreach ($input['term_id'] as $key => $value) {
                BPC_Term::create([
                'bpc_id' =>$bpc_id,
                'term_id' => $value
            ]);
        }

        return redirect()->route('admin.bpc-term.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
