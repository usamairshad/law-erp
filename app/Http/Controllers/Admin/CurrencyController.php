<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Currencies;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Auth;
use Config;
class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if (! Gate::allows('erp_currencies_manage')) {
            return abort(401);
        };
        $currencys = Currencies::all();
        return view('admin.currency.index', compact('currencys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_currencies_create')) {
            return abort(401);
        }
        return view('admin.currency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_currencies_create')) {
            return abort(401);
        }
        $rules['name']  =  'required|unique';
        $rules['code']  =   'required';
        $rules['symbol']  =   'required';
        $rules['rate']  =   'required';

        $CountryModel = Currencies::create([
            'name' => $request['name'],
            'code' => $request['code'],
            'symbol' => $request['symbol'],
            'rate' => $request['rate'],
        ]);
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.currency.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_currencies_edit')) {
            return abort(401);
        }
        $currency = Currencies::findOrFail($id);

        return view('admin.currency.edit', compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_currencies_edit')) {
            return abort(401);
        }
        Currencies::where('id',$id)->update([
            'name' => $request['name'],
            'code' => $request['code'],
            'symbol' => $request['symbol'],
            'rate' => $request['rate'],
        ]);
        flash('Record has been Updated successfully.')->success()->important();
        return redirect()->route('admin.currency.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
