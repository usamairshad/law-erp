<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\LcBankModel;
use App\Models\Admin\ComercialInvoiceModel;
use App\Models\Admin\Ledgers;
use App\Models\Admin\EntryItems;
use App\Models\Admin\DutyModel;
use App\Models\Admin\LcDutyModel;
use App\Models\Admin\Groups;
use Auth;
use DB;
use Config;
class DutySegController extends Controller
{

    public function indexDuties()
    {

        $duty = LcDutyModel::groupBy('ledgerLc_id')->sum('amount');
        return view('admin.duty_seg.index', compact('duty'));
    }

    public function datatables()
    {
       // $duty = LcDutyModel::groupBy('ledgerLc_id')->sum('amount');
       $duty = DB::table('lcduty')
       ->select('id','ledgerLc_id', DB::raw('SUM(amount) as total_amount'))
       ->groupBy('ledgerLc_id')
       ->get();
        
        // dd($duty);
        //$duty = $data[0];
        return  datatables()->of($duty)
            ->addColumn('ledgerLc_id', function($duty) {
                return $duty->ledgerLc_id;
            })
           

            ->addColumn('total_amount', function($duty) {
                return $duty->total_amount;
            })
            ->addColumn('action', function ($duty) {
                $action_column = '<a href="detail/'.$duty->ledgerLc_id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> View</a></br>';
                //$action_column .= '<a href="orders/'.$duty->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
               
                return $action_column;
            })
            ->rawColumns(['ledgerLc_id','total_amount', 'action'])
            ->toJson();
    }
    public function viewDutySegrigation(){
        $LcBank = LcBankModel::all();
        return view('admin.duty_seg.create', compact('LcBank'));
    }
    public function storeDutySegrigation(Request $request){
      //  dd($request);
        foreach($request['InventoryItems']['duty_id'] as $key=>$value)
        {
            LcDutyModel::create([
                'ledgerLc_id'          => $request['lc_no'],
                'Comercial_id'          => $request['invoice_id'],
                'duties'          => $request['InventoryItems']['duty_id'][$key],
                'amount'          => $request['InventoryItems']['amount'][$key],
                'status'          => 1,
                'created_by'        => Auth::user()->id,
            ]);

        }
        flash('Record has been created successfully.')->success()->important();

        return redirect()->back();
    }

    public function Invoices(Request $request){
       $data = ComercialInvoiceModel::where('lcno', $request->id)->get();
        //echo'<pre>';print_r($data);echo'</pre>';exit;
       $LcBankModel = LcBankModel::where('id', $request->id)->first();
        if ($LcBankModel->transaction_type == '1') {
            $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_Lc_Transit')])->where(['parent_type' => $LcBankModel->bnk_id])->first();
            $Assets_id = $TR_ledger->id;
            $transit_ledger = Ledgers::where('parent_type',$request->id)->where('group_id',$Assets_id)->pluck('id');
        }else{
            if($LcBankModel->lc_type == 2){
                $TR_ledger = Groups::where(['parent_id' => Config::get('constants.accounts_tt_advance')])->where(['parent_type' => $LcBankModel->bnk_id])->first();
                $transit_ledger = Ledgers::where('parent_type',$request->id)->where('group_id',$TR_ledger->id)->pluck('id');
            }else{
                $TR_ledger = Groups::where(['parent_id' => Config::get('constants.account_bank_contract')])->where(['parent_type' => $LcBankModel->bnk_id])->first();
                $transit_ledger = Ledgers::where('parent_type',$request->id)->where('group_id',$TR_ledger->id)->pluck('id');
            }
        }

        //$Ledger = Ledgers::where('parent_type', $request->id)->pluck('id');
        $entry = EntryItems::whereIn('ledger_id', $transit_ledger)->get();
       //echo'<pre>';print_r($entry);echo'</pre>';exit;
        return view('admin.duty_seg.entries', compact('entry', 'data'));
    }

    public function duty(Request $request)
    {
        $data = $request->all();
        $duty = DutyModel::Duty($data['item']);
        $result = array();
        if($duty->count()) {
            foreach ($duty as $key => $value) {
                $result[] = array(
                    'text' => $value->duty,
                    'id' => $value->id,
                );
            }
        }
        return response()->json($result);
    }

    public function detailDuty($id){

        $duty = LcDutyModel::where('ledgerLc_id', $id)->get();

        return view('admin.duty_seg.detail', compact('duty'));
    }
}
