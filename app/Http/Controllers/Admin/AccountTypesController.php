<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AccountTypes\StoreUpdateRequest;
use App\Models\Admin\AccountTypes;
use Auth;

class AccountTypesController extends Controller
{
    /**
     * Display a listing of AccountType.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_account_types_manage')) {
            return abort(401);
        }

        $AccountTypes = AccountTypes::OrderBy('created_at','desc')->get();

        return view('admin.account_types.index', compact('AccountTypes'));
    }

    /**
     * Show the form for creating new AccountType.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_account_types_create')) {
            return abort(401);
        }
        return view('admin.account_types.create');
    }

    /**
     * Store a newly created AccountType in storage.
     *
     * @param  \App\Http\Requests\Admin\AccountTypes\StoreUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('erp_account_types_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        AccountTypes::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.account_types.index');
    }


    /**
     * Show the form for editing AccountType.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_account_types_edit')) {
            return abort(401);
        }
        $AccountType = AccountTypes::findOrFail($id);

        return view('admin.account_types.edit', compact('AccountType'));
    }

    /**
     * Update AccountType in storage.
     *
     * @param  \App\Http\Requests\Admin\AccountTypes\StoreUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('erp_account_types_edit')) {
            return abort(401);
        }

        $AccountType = AccountTypes::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $AccountType->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.account_types.index');
    }


    /**
     * Remove AccountType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_account_types_destroy')) {
            return abort(401);
        }
        $AccountType = AccountTypes::findOrFail($id);
        $AccountType->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.account_types.index');
    }

    /**
     * Activate AccountType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_account_types_active')) {
            return abort(401);
        }

        $AccountType = AccountTypes::findOrFail($id);
        $AccountType->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.account_types.index');
    }

    /**
     * Inactivate AccountType from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_account_types_inactive')) {
            return abort(401);
        }

        $AccountType = AccountTypes::findOrFail($id);
        $AccountType->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.account_types.index');
    }

    /**
     * Delete all selected AccountType at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_account_types_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = AccountTypes::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
