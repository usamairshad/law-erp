<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Academics\Session;
use Illuminate\Support\Facades\Gate;
use Auth;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_session_manage')) {
            return abort(401);
        }
        $session = Session::OrderBy('created_at','desc')->get();

        return view('admin.session.index', compact('session'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_session_create')) {
            return abort(401);
        }
        

        return view('admin.session.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_session_create')) {
            return abort(401);
        }

        $user_data = $request->all();
        //dd($user_data);
        $user_data['created_by'] = Auth::user()->id;

        $user2 = Session::create($user_data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.session.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_session_edit')) {
            return abort(401);
        }
        $session = Session::find($id);
        return view('admin.session.edit', compact('session'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_session_edit')) {
            return abort(401);
        }
        $session = Session::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $session->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.session.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_session_destroy')) {
            return abort(401);
        }
        $session = Session::findOrFail($id);
        $session->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.session.index');    }
}
