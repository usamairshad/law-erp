<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Loan;
use App\Models\Academics\Staff;
use App\User;
use App\Models\Role;
class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Loans = Loan::with('role','user')->get();

        return view('admin.Loan.index',compact('Loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   

        $roles = Role::whereNotIn('name',['student','super-admin','admin','jr.coordinator'])->pluck('name','id');
        //dd($roles);
        $staff = Staff::all()->pluck('first_name','id');
        return view('admin.Loan.create', compact('staff','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'amount' => 'required',
            'role_id' => 'required',
            'user_id'=> 'required',
            'type' => 'required',
            'paid_status'=> 'required',
            'return_date'=> 'required',
            'salary_deduct'=> 'required',
            'remaining'=> 'required'
        ]);
        $inputs = $request->input();
        //dd($inputs);
        Loan::create($inputs);
        flash('Record has been Created successfully.')->success()->important();
        return redirect('/loan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Loan = Loan::findOrFail($id);
        //dd($Loan);
        $role = Role::all()->pluck('name','id');
        $staff = Staff::all()->pluck('first_name','id');
        //dd($SalaryDeduction);
        return view('admin.Loan.edit', compact('Loan','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Loan::where('id',$id)->update([
            'role_id' => $request['role_id'],
            'user_id' => $request['user_id'],
            'amount' => $request['amount'],
            'paid_status' => $request['paid_status'],
            'return_date' => $request['return_date'],
            'salary_deduct' => $request['salary_deduct'],
            'remaining' => $request['remaining'],
            'type' => $request['type'],
        ]);
        flash('Record has been updated successfully.')->success()->important();
        return redirect('/loan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function RoleRelatedUser(Request $request){
        $data = User::where('role_id',$request->role_id)->get();
        //dd($data);
        return response()->json($data); 
    }
    
}
