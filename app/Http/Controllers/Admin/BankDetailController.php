<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Groups;
use App\Models\Admin\BankDetailModel;
use App\Models\Admin\BanksModel;
use App\Models\Admin\Ledgers;
use App\Models\HRM\Employees;
use Illuminate\Support\Facades\Gate;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Branches;
use App\Models\Admin\Company;
use App\Helpers\GroupsTree;
use Auth;
use Config;
use Validator;

class BankDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        if (! Gate::allows('erp_banksdeatil_manage')) {
//            return abort(401);
//        }

        $BankDetail = BankDetailModel::all();
        return view('admin.bank_detail.index', compact('BankDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        if (! Gate::allows('erp_banksdeatil_create')) {
//            return abort(401);
//        }
      
        $Banks = BanksModel::all();
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));
        return view('admin.bank_detail.create', compact('Banks','Groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  //dd($request->all());
//        if (! Gate::allows('erp_users_manage')) {
//            return abort(401);
//        }
        $bId=0;
        if(isset($request->branch_id)){
            $bId=$request->branch_id;
        }else{
            $bId=Auth::user()->branch_id;
        }
        $rules['account_no']       = 'required|unique:bankdetail';
//        $validator = Validator::make($request->all(), $rules);
//        if ($validator->fails()) {
//            $request->flash();
//            return redirect()->back()
//                ->withErrors($validator->errors())
//                ->withInput();
//        }
        if($request['employee']!=''){
            $accid = $request['employee'];
        }else{
            $accid = 0;

        }
        $bnkid = BankDetailModel::create([
            'group_id' =>$accid,
            'bank_id' => $request['bank_name'],
//            'accountType' => $request['accountType'],
            'acc_type' => $request['acc_type'],
            'bank_address' => $request['bank_address'],
            'branch_code' => $request['branch_code'],
            'account_no' => $request['account_no'],
            'phone_no' => $request['phone_no'],
            'city_id' => $request['city_id'],
            'group_id' => $request['parent_id'],
            'created_by' =>  Auth::user()->id,
            'branch_id' =>  $bId,
        ]);
//        dd($bnkid);
 
            $ledger_name =Branches::where('id', Auth::user()->branch_id)->value('name');
        
//        if($request['acc_type']==1){
//            $Banks = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_balance_local'));
//        }elseif($request['acc_type']==2){
//            $Banks = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_balance_euro'));
//        }else{
//            $Banks = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_balance_us'));
//        }
//        $Group = Groups::where(['parent_id' => 25])->where(['parent_type' => $request->bank_name])->first();
        $bank=BanksModel::where('id', $request->bank_name)->value('bank_name');
        $Ledger = array(
            'name' => $ledger_name.' ('.$bank.'-'.$request['branch_code'].') - '.$request['account_no'],
            'group_id' => $request['parent_id'],
            'balance_type' => 'd',
            'opening_balance' => 0,
            'parent_type' => $bnkid->id,
            'branch_id' => $bId,
        );
        $response = CoreAccounts::createLedger($Ledger);
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.bank_detail.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Bankes = BanksModel::all();
        $Banks = BankDetailModel::findOrFail($id);
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));
        
        return view('admin.bank_detail.edit', compact('Banks','Bankes','Groups'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            BankDetailModel::where('id',$id)->update([
                'bank_id' => $request['bank_name'],
                //'acc_type' => $request['acc_type'],
//                'accountType' => $request['accountType'],
                'bank_address' => $request['bank_address'],
                'branch_code' => $request['branch_code'],
                'account_no' => $request['account_no'],
                'phone_no' => $request['phone_no'],
                'city_id' => $request['city_id'],
                'group_id' => $request['parent_id'],
                'updated_by' =>  Auth::user()->id,
                'branch_id' =>  Auth::user()->branch_id,
            ]);
            if($request['emp_name'] > 0){
                $Employees = Employees::where('id',$request['emp_name'])->first();
                $ledger_name = $Employees->first_name;
            }else{
                $ledger_name =Branches::where('id', Auth::user()->branch_id)->value('name');
            }
        $bank=BanksModel::where('id', $request->bank_name)->value('bank_name');
        Ledgers::where('parent_type',$id)->where('group_id',25)->update([
            'name' => $ledger_name.' ('.$bank.'-'.$request['branch_code'].') - '.$request['account_no'],
            'updated_by' => Auth::user()->id,
            'branch_id' => $request->branch_id,
        ]);
        flash('Record has been updated successfully.')->success()->important();
        return redirect()->route('admin.bank_detail.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_banks_destroy')) {
            return abort(401);
        }
        $BankDetail = BankDetailModel::findOrFail($id);
        $BankDetail->delete();
        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.bank_detail.index');
    }
}
