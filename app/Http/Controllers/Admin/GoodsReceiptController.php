<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin\GoodsReceiptModel;
use App\Models\Admin\GoodsReceiptDetailModel;
use App\Models\Admin\StockItemsModel;
use App\Models\Admin\Branches;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\StockCategoryModel;
use App\Helpers\CoreAccounts;
use App\Models\Admin\UnitsModel;
use App\Helpers\GroupsTree;
use App\Models\Admin\Groups;
use Auth;

class GoodsReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $GoodsReceiptModel = GoodsReceiptModel::all();
        return view('admin.goodsreceipt.index', compact('GoodsReceiptModel'));
    }



    public function datatables()
    {
        $GoodsReceiptModel = GoodsReceiptModel::all();
       // print_r($GoodsReceiptModel);exit;
        return  datatables()->of($GoodsReceiptModel)
            -> addColumn('branch_to', function($GoodsReceiptModel) {
                return  $GoodsReceiptModel->branchTo->name;
            })
            ->addColumn('branch_from', function($GoodsReceiptModel) {
                return  $GoodsReceiptModel->branchFrom->name;
            })
            ->addColumn('action', function ($GoodsReceiptModel) {
                $action_column = '<a href="goodsreceipt/'.$GoodsReceiptModel->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                $action_column .= '<form method="post" action="goodsreceipt/delete" >';
                $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
                $action_column .= '<input type="hidden" value="21" name="products_name" >';
                $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
                $action_column .= '<input type="hidden" value='.$GoodsReceiptModel->id.' name="name" >';
                $action_column .=  csrf_field();
                $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                return $action_column;
            })
            ->rawColumns(['branch_to','branch_from','action'])
            ->toJson();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));
        $Branches = Branches::pluckActiveOnly();
        $UnitsModel = UnitsModel::pluckActiveOnly();
        $UnitsModel->prepend('Select a UOM', '');
        $InventoryItems= array();
        return view('admin.goodsreceipt.create', compact('Groups','Branches','InventoryItems','UnitsModel'));
    }


    public function getproduct($lc_no,$sup_id)
    {
        $results = ProductsModel::where('cat_id',$lc_no)->where('suplier_id',$sup_id)->get();
        return view('admin.goodsreceipt.performa')->with('results',$results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $GoodsReceiptModel = GoodsReceiptModel::create([
            'branch_to'             => $request['branch_to'],
            'branch_from'           => $request['branch_from'],
            'created_by'            => Auth::user()->id,
        ]);
        foreach($request['InventoryItems']['product_id'] as $key=>$value)
        {
            $productid = $request['InventoryItems']['product_id'][$key];
            $qunity = $request['InventoryItems']['qunity'][$key];
            $p_amount = $request['InventoryItems']['p_amount'][$key];
            $results = ProductsModel::where('id',$productid)->first();
            $products_name = $results->products_name;
            $suplier_id = $results->suplier_id;
            $cat_id = $results->cat_id;
            // goods receipt //

            GoodsReceiptDetailModel::create([
                'gr_detail_id'      => $GoodsReceiptModel->id,
                'product_id'        => $productid,
                'quantity'          => $qunity,
                'created_by'        => Auth::user()->id,
            ]);
            // end goods receipt
            $Ledger = array(
                'name' => $products_name,
                'group_id' => $request['parent_id'],
                'balance_type' => 'd',
                'opening_balance' => $request['InventoryItems']['amount'][$key],
                'closing_balance' => $request['InventoryItems']['amount'][$key],
            );
            $response = CoreAccounts::createLedger($Ledger);
            $opening_stock = 0;
            $closing_stock = $qunity;
            $movment_stock = $opening_stock+$qunity - $closing_stock;
            $stock_item =  StockItemsModel::create([
                'branch_id'            => $request['branch_to'],
                'group_id'             => $request['parent_id'],
                'ledger_id'            => $response['id'],
                'sup_id'               => $suplier_id,
                'cat_id'               => $cat_id,
                'st_name'              => $productid,
                'quantity'             => $qunity,
                'opening_stock'        => $opening_stock,
                'purchase_stock'       => $qunity,
                'closing_stock'        => $closing_stock,
                'moments_stock'        => $movment_stock,
                'stock_value'          => $request['InventoryItems']['amount'][$key],
                'st_unit_price'        => $p_amount,
                'sale_unit'            => $request['InventoryItems']['unit_name'][$key],
                'created_by'           => Auth::user()->id,
            ]);
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.stockitems.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
