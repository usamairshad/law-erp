<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Admin\FinancialYears;
use App\Models\Academics\Term;
use App\Models\Admin\AcademicYear;
use Auth;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_academic_years_manage')) {
           return abort(401);
       }
        $terms = Term::OrderBy('created_at','desc')->get();

        return view('admin.term.index', compact('terms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_academic_years_create')) {
           return abort(401);
       }
        $academic_year = AcademicYear::pluck('name','id');
        return view('admin.term.create', compact('academic_year'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_academic_years_create')) {
           return abort(401);
       }
        //dd('abc');
        $data = $request->all();
        $data['created_by'] = Auth::user()->id;

        Term::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.term.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function active($id)
    {
        if (! Gate::allows('erp_term_active')) {
           return abort(401);
       }
       
        $term = Term::findOrFail($id);
        $term->update(['status' => 'Active']);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.term.index');
    }

    public function inactive($id)
    {
        if (! Gate::allows('erp_term_inactive')) {
           return abort(401);
       }
        $term = Term::findOrFail($id);
        $term->update(['status' => 'InActive']);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.term.index');
    }
}
