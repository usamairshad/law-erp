<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreOrdersRequest;
use App\Models\Admin\Orders;
use App\Models\Admin\SuppliersModel;
use App\Models\Admin\Branches;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\UnitsModel;
use App\Models\Admin\OrderDetailModel;
use App\Helpers\CoreAccounts;
use Validator;
use Auth;
use Config;
use DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_orders_manage')) {
            return abort(401);
        }

        $Orders = Orders::OrderBy('id','desc')->get();
//        dd($Orders);
        return view('admin.orders.index', compact('Orders'));
    }

    public function importindex()
    {
        if (! Gate::allows('erp_orders_manage')) {
            return abort(401);
        }
        $Orders = Orders::where('order_type',2)->OrderBy('id','desc')->get();
        return view('admin.orders.import', compact('Orders'));
    }

    public function importdatatables()
    {
        $Orders = Orders::where('order_type',2)->OrderBy('id','DESC')->get();
        return  datatables()->of($Orders)
            ->addColumn('sup_id', function($Orders) {
                return $Orders->suppliersModel->sup_name;
            })
            ->addColumn('perfomra_no', function($Orders) {
                return $Orders->perfomra_no;
            })->addColumn('status', function($Orders) {
                if ($Orders->status == 1) {
                    return $status = "Pending";
                }elseif ($Orders->status == 2){
                    return $status = "In Process";
                }elseif ($Orders->status == 3){
                    return $status = "In Transit";
                }elseif ($Orders->status == 4){
                    return $status = "Completed";
                }else{
                    return $status = "Cancel";
                }
            })->addColumn('action', function ($Orders) {
                if($Orders->status == 1){
                    $action_column = '<a href="orders/'.$Orders->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> View</a></br>';
                    $action_column .= '<a href="orders/'.$Orders->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                    $action_column .= '<form method="post" action="orders/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
                    $action_column .= '<input type="hidden" value="21" name="perfomra_no" >';
                    $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
                    $action_column .= '<input type="hidden" value='.$Orders->id.' name="name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                } else{
                    $action_column = '<a href="orders/'.$Orders->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> View</a></br>';
                    $action_column .= '<a href="orders/'.$Orders->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                }
                return $action_column;
            })->rawColumns(['sup_id','perfomra_no','status','action'])
            ->toJson();
    }

    public function datatables()
    {
        $Orders = Orders::OrderBy('id','DESC')->get();
        return  datatables()->of($Orders)
            ->addColumn('sup_id', function($Orders) {
                return $Orders->suppliersModel->sup_name;
            })
            ->addColumn('po_date', function($Orders) {
                return date('d-m-Y  ', strtotime($Orders->po_date));
            })
            ->addColumn('perfomra_no', function($Orders) {
                return $Orders->perfomra_no;
            })->addColumn('status', function($Orders) {
                if ($Orders->status == 1) {
                    return $status = "Pending";
                }elseif ($Orders->status == 2){
                    return $status = "In Process";
                }elseif ($Orders->status == 3){
                    return $status = "In Transit";
                }elseif ($Orders->status == 4){
                    return $status = "Completed";
                }else{
                    return $status = "Cancel";
                }
            })->addColumn('action', function ($Orders) {
                if($Orders->status == 1){
                    $action_column = '<a href="orders/'.$Orders->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> View</a></br>';
                    $action_column .= '<a href="orders/'.$Orders->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                    $action_column .= '<form method="post" action="orders/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
                    $action_column .= '<input type="hidden" value="21" name="perfomra_no" >';
                    $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
                    $action_column .= '<input type="hidden" value='.$Orders->id.' name="name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                }else{
                    $action_column = '<a href="orders/'.$Orders->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> View</a></br>';
//                    $action_column .= '<a href="orders/'.$Orders->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                }
                return $action_column;
            })->rawColumns(['sup_id','po_date','perfomra_no','status','action'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_orders_create')) {
            return abort(401);
        }

        $foreignSuppliersModel = SuppliersModel::pluckForeignOnly();
        $localSuppliersModel = SuppliersModel::pluckLocalOnly();
        //dd($SuppliersModel);
        $Branches = Branches::all();
        $UnitsModel = UnitsModel::pluckActiveOnly();
        $UnitsModel->prepend('Select a Unit of Measurement', '');
        $Products = ProductsModel::pluckActiveOnly();
        $Products->prepend('Select a Product      ', '');
        $LineItems= array();
         return view('admin.orders.create', compact('foreignSuppliersModel', 'localSuppliersModel','Products','UnitsModel','LineItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function productlist(StoreOrdersRequest $request){
        $data = $request->all();
        $ProductsModel = ProductsModel::productsByNameForOrder($data['item'],$data['sup_id'] );
        $result = array();
        if($ProductsModel->count()) {
            foreach ($ProductsModel as $key => $value) {

                $result[] = array(
                    'text' => $value->item_code.' - '.$value->products_short_name,
//                    'text' => $value->products_short_name.' - '.$value->item_code,
                    'id' => $value->id,
                );
            }
        }
        return response()->json($result);
    }

    public function store(StoreOrdersRequest $request)
    {
        //dd($request->all());
        if (! Gate::allows('erp_orders_create')) {
            return abort(401);
        }
        //if($request['orderType'] == 2) {
            //$rules['po_number'] = 'required|unique:orders,deleted_at,NULL';
            //$rules['perfomra_no'] = 'required|unique:orders';

            // $validator = Validator::make($request->all(), $rules);
            // if ($validator->fails()) {
            //     $request->flash();
            //     return redirect()->back()
            //         ->withErrors($validator->errors())
            //         ->withInput();
            // }
        //}

        if($request->sup_name_for){
            $supplier = $request['sup_name_for'];
        }else{
            $supplier = $request['sup_name_loc'];
        }
        $order = Orders::create([
            'po_number' => $request['po_number'],
            'po_date' => $request['po_date'],
            'sup_id' => $supplier,
            'total_order_amt' => $request['grand_total'],
            'perfomra_no' => $request['perfomra_no'],
            'sale_order_no' => $request['sale_no'],
            'shipping' => $request['shipping'],
            'sal_date' => $request['sal_date'],
            'valid_upto' => $request['valid_upto'],
            'soruce' => $request['soruce'],
            'destination' => $request['destination'],
            'order_type' => $request['orderType'],
            'payment_terms' => $request['payemnt_terms'],
            'status' => '1',
        ]);
        foreach($request['line_items']['product_id'] as $key=>$value)
        {
            OrderDetailModel::create([
                'perchas_id'    =>$order->id,
                'product_id'    =>$request['line_items']['product_id'][$key],
                'unit_name'     =>$request['line_items']['unit_name'][$key],
                'qty'           =>$request['line_items']['qunity'][$key],
                'remaining_qty' =>$request['line_items']['qunity'][$key],
                'unit_price'    =>$request['line_items']['p_amount'][$key],
                'amount'        =>$request['line_items']['amount'][$key],
                'country_of_origin'        =>$request['line_items']['country'][$key],
            ]);
        }
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.orders.index');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('erp_orders_view')) {
            return abort(401);
        }

        $Orders = Orders::find($id);
        $Products = OrderDetailModel::where('perchas_id',$id)->get();
        return view('admin.orders.orderdisplay', compact('Products','Orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (! Gate::allows('erp_orders_edit')) {
            return abort(401);
        }
        $SuppliersModel = SuppliersModel::all();
        $Branches = Branches::all();
        $Products = ProductsModel::all();
        $UnitsModel = UnitsModel::all();
        $UnitsModels = UnitsModel::pluckActiveOnly();
        $UnitsModels->prepend('Select UOM', '');
        $Orders = Orders::find($id);
        $OrderDetail = OrderDetailModel::where('perchas_id',$id)->get();
        return view('admin.orders.edit', compact('SuppliersModel','UnitsModels','Branches','OrderDetail','Products','Orders','UnitsModel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_orders_edit')) {
            return abort(401);
        }

        $Orders = Orders::where('id',$id)->update([
            'perfomra_no' => $request['perfomra_no'],
            'sale_order_no' => $request['sale_no'],
            'shipping' => $request['shipping'],
            'sal_date' => $request['sal_date'],
            'valid_upto' => $request['valid_upto'],
            'soruce' => $request['soruce'],
            'destination' => $request['destination'],
            'po_number' => $request['po_number'],
            'po_date' => $request['po_date'],
            'total_order_amt' => $request['total_of_product'],
            'payment_terms' => $request['payment_terms'],
        ]);
        foreach($request->product_id as $key=>$value)
        {
            OrderDetailModel::where('id',$request->perchas_id[$key])->update([
                'product_id'        =>$request->product_id[$key],
                'unit_name'         =>$request->unit_name[$key],
                'qty'               =>$request->qunity[$key],
                'remaining_qty'     =>$request->qunity[$key],
                'unit_price'        =>$request->p_amount[$key],
                'amount'            =>$request->amount[$key],
                'country_of_origin' =>$request->country_origin[$key],
            ]);
        }
        if(isset($request['line_new']['product_id']))  {
            foreach($request['line_new']['product_id'] as $key=>$value)
            {
                if($request['line_new']['product_id'][$key]!=''){
                    OrderDetailModel::create([
                        'perchas_id'        => $id,
                        'product_id'        =>$request['line_new']['product_id'][$key],
                        'unit_name'         =>$request['line_new']['unit_name'][$key],
                        'qty'               =>$request['line_new']['qunity'][$key],
                        'remaining_qty'     =>$request['line_new']['qunity'][$key],
                        'unit_price'        =>$request['line_new']['p_amount'][$key],
                        'amount'            =>$request['line_new']['amount'][$key],
                        'country_of_origin' =>$request['line_new']['country_origin'][$key],
                    ]);
                }

            }
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {

        if (! Gate::allows('erp_orders_destroy')) {
            return abort(401);
        }

        $data = $request->all();
        $Orders = Orders::findOrFail($data['name']);
        $Orders->delete();
        flash('Record has been deleted successfully.')->success()->important();
        return redirect()->route('admin.orders.index');
    }

    /**
     * Delete all selected Orders at once.
     *
     * @param Request $request
     */
}
