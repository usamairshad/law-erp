<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Currency;
use App\Helpers\LedgersTree;
use App\Models\Admin\Companies;
use App\Models\Admin\Currencies;
use App\Models\Admin\Ledgers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Helpers\GroupsTree;
use App\Models\Admin\Groups;
use App\Models\Admin\Branches;
use App\Helpers\CoreAccounts;
use App\Models\Admin\City;
use App\Models\Admin\Company;
use Auth;
use Config;

class LedgersController extends Controller
{
    /**
     * Display a listing of Ledger.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /*  if (! Gate::allows('erp_ledgers_manage')) {
            return abort(401);
        } */

        /* Create list of parent groups */
        $parentGroups = new LedgersTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
        $parentGroups->toList($parentGroups, -1);
        $Ledgers = $parentGroups->ledgerList;
        $DefaultCurrency = Currencies::getDefaultCurrencty();
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('id', 'asc')->get()->toArray()), old('group_id'));

        return view('admin.ledgers.index', compact('Ledgers','DefaultCurrency', 'Groups'));
    }

    /**
     * Show the form for creating new Ledger.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_ledgers_create')) {
            return abort(401);
        }
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('id', 'asc')->get()->toArray()), old('group_id'));
        $Branches = Branches::pluck('name','id');
        $company = Company::all()->pluck('name','id');
        $city = City::all()->pluck('name','id');
        return view('admin.ledgers.create', compact('Groups','Branches','company','city'));
    }

    /**
     * Store a newly created Ledger in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_ledgers_create')) {
            return abort(401);
        }

        $input = $request->all();
     
        $err = 0;
        $ledger_name = $input['name'];
        foreach ($input['branches_id'] as $key => $branch) {
          $group_id = 0;
          $head_name = 0;
          $input['branch_id'] = $branch;
     
      if($input['branch_id'] == Config::get('constants.head_office_id')){
                $input['is_common'] = 1;
            }else{
                $input['is_common'] = 0;
            }
             

            $branch_name = Branches::find($branch)->getName($branch);
            
            $city_id = Branches::whereId($branch)->value('city_id');
            
            $city_name = City::whereId($city_id)->value('name');
            $company_id = Branches::whereId($branch)->value('company_id');
            $company_name = Company::whereId($company_id)->value('name');
            
            $name = '';
            $name = $branch_name.' ('.$ledger_name.')'.'('.$branch_name.')';
         
            $input['name'] = $name;
         
            $response = CoreAccounts::createLedger($input);
          
            if($response['status']) {
            } 
            else {
                $err = 1;
            }
        
    }
      

        if($err == 1)
        {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
        else
        {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.ledgers.index');
        }
//        $response = CoreAccounts::createLedger($request->all());
//
//        if($response['status']) {
//            flash('Record has been created successfully.')->success()->important();
//            return redirect()->route('admin.ledgers.index');
//        } else {
//            $request->flash();
//            return redirect()->back()
//                ->withErrors($response['error'])
//                ->withInput();
//        }
    }


    /**
     * Show the form for editing Ledger.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) 
    {
        if (! Gate::allows('erp_ledgers_edit')) {
            return abort(401);
        }

        $Ledger = Ledgers::findOrFail($id);
        $Branches = Branches::pluck('name','id');
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('id', 'asc')->get()->toArray()), $Ledger->group_id);
        $company = Company::all()->pluck('name','id');
        $city = City::all()->pluck('name','id');
        return view('admin.ledgers.edit', compact('Ledger','Groups','Branches','company','city'));
    }

    public function ledgerChild(Request $request){
      
        $group = Groups::where('id', $request->group_id)->first();
        $Ledgers = Ledgers::where('group_id', $request->group_id)->get();
        $DefaultCurrency = Currencies::getDefaultCurrencty();
        // dd($Ledgers);
        
        return view('admin.ledgers.detail', compact('Ledgers', 'DefaultCurrency', 'group'));
    }

    /**
     * Update Ledger in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_ledgers_edit')) {
            return abort(401);
        }

        $response = CoreAccounts::updateLedger($request->all(), $id);

        if($response['status']) {
            flash('Record has been updated successfully.')->success()->important();
            return redirect()->route('admin.ledgers.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();

        //echo 'Here I am'; exit;
    }
        $Ledger = Ledgers::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        // Get selected group
        $Group = Groups::findOrFail($data['group_id']);
        // Get Default Company
        $Companie = Companies::findOrFail(Config::get('constants.accounts_company_id'));

        $data['group_number'] = $Group->number;
        $data['account_type_id'] = $Group->account_type_id;
        $data['number'] = CoreAccounts::generateLedgerNumber($Companie->id, $Group->number, $Ledger->id);

        $Ledger->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.ledgers.index');
    }


    /**
     * Remove Ledger from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_ledgers_destroy')) {
            return abort(401);
        }
        $Ledger = Ledgers::findOrFail($id);
        $Ledger->delete();

        return redirect()->route('admin.ledgers.index');
    }

    /**
     * Delete all selected Ledger at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_ledgers_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Ledgers::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    public  function ledger_tree(){
        /* Create list of parent groups */
        $Groups=Groups::all();
        return view('admin.ledgers.ledger_tree', compact('Groups'));
    }
    public function get_ledger_tree($id)
    {
        $data = '';
        $Groups = Groups::where('parent_id', $id)->get();

        if ($Groups->isEmpty()) {
            $Ledgers=Ledgers::where('group_id', $id)->get();
            if($Ledgers->isNotEmpty()) {
                $data .= '<div class="accordion-inner"><ul class="nav nav-list">';
                foreach ($Ledgers as $ledger) {
                    $data .= '<li><a href="#"><i class="fa fa-angle-double-right"></i> ' . ucfirst($ledger->name) . '</a></li>';
                }
                $data .= '</ul></div>';
            }
        } else {
            foreach ($Groups as $Group) {
                $data .= '
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-parent="#equipamento1-1" data-toggle="collapse" href="#" onclick="get_group_ledger(this, ' . $Group->id . ', ' . $Group->level . ');" val="' . $Group->number . '">' . $Group->number . '-' . $Group->name . '</a>
                </div>
                <div class="accordion-body collapse" id="' . $Group->number . '"></div>
            </div>';
            }
        }
        return response()->json(['data' => $data]);
    }
}
