<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Company;
use App\Models\Admin\Branches;
use App\Models\Admin\City;
use App\Models\Admin\CountryModel;
use App\Models\Admin\Regions;
use App\Models\Admin\Territory;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Branch;
use App\Models\Admin\Groups;
use Auth;
use Config;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_companies_manage')) {
            return abort(401);
        }

        $Companies = Company::OrderBy('created_at','desc')->get();

        return view('admin.companies.index', compact('Companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_companies_create')) {
            return abort(401);
        }
        $cities=City::all();
        $groups = Groups::whereNull('company_id')->whereNull('city_id')->whereNull('branch_id')->where('level','!=',1)->get();
        return view('admin.companies.create',compact('cities','groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        //dd($request->input());
        // $rules['branch_code']  =  'required|unique:erp_branches';
        // $validator = Validator::make($request->all(),$rules);
        // if ($validator->fails()) {
        //     $request->flash();
        //     return redirect()->back()
        //         ->withErrors($validator->errors())
        //         ->withInput();
        // }
        
        $this->validate($request,[
            'name'=>'required | unique:erp_companies',
            'ntn'=>'required',
            'phone'=>'required',
            'fax'=>'required',
            'description'=>'required',
            'c_address'=>'required',
            'groups'=>'required'
        ]);
   
        $input= $request->all();

        $arr= new Company;
        $arr->name=$input['name'];
        $arr->ntn=$input['ntn'];
        // $arr->gst=$input['gst'];
        if ($request->files->get('logo'))
        {   
            $temp=  auth()->user()->name;
            $company_logo = $request->files->get('logo');
            $company_logo_name = $temp.'-logo'.'.'.$company_logo->getClientOriginalExtension();
            $company_logo->move(public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'companyLogo'.DIRECTORY_SEPARATOR, $company_logo_name);     
        }
        $arr->logo=$company_logo_name;
        // $arr->gst_type=$input['gst_type'];
        // $arr->vat=$input['vat'];
        $arr->phone=$input['phone'];
        $arr->fax=$input['fax'];
        $arr->description=$input['description'];
        $arr->address=$input['c_address'];
        $arr->save();
        $param = $request->all();
       
        if(count($param['groups']) > 0){ 
          $total = count([$param['groups']]);
         
            foreach ($param['groups'] as $key => $value) {
               
                $param['name'] = $request['name'];
                $param['company_id'] = $arr->id;
                $param['parent_id'] = $value;
             
                $parent_id= Groups::where('id',$param['parent_id'])->value('parent_id');
                if($parent_id == 269 || $parent_id == 270 || $parent_id == 271 || $parent_id == 272)
                {
                    $param['code'] = "Fee";
                }
            
                $response = CoreAccounts::createGroup($param);
     
            
                
            }  
        }
    

        //$company_id= Company::orderBy('created_at','DESC')->first();
        // for($i = 0; $i < count($input['branch_name']) ; $i++) { 
        //     $arr= new Branch;
        //     $arr->company_id=$company_id->id;
        //     $arr->name=$input['branch_name'][$i];
        //     $arr->phone=$input['branch_phone'][$i];
        //     $arr->cell=$input['branch_cell'][$i];
        //     $arr->fax=$input['branch_fax'][$i];
        //     $arr->city_id=$input['city'][$i];
        //     $arr->country_id=1;
        //     $arr->branch_code=$input['branch_code'][$i];
        //     $arr->address=$input['address'][$i];
        //     $arr->save();
        // }
        // if (! Gate::allows('erp_companies_create')) {
        //     return abort(401);
        // }
        // $rules = [
        //     'name' => 'required'
        // ];

        // $validator = Validator::make($request->all(), $rules);
        // if ($validator->fails()) {
        //     $request->flash();
        //     return redirect()->back()
        //         ->withErrors($validator->errors())
        //         ->withInput();
        // }

        // $company = Company::create([
        //     'name'              => $request['name'],
        //     'address'           => $request['address'],
        //     'created_by'        => Auth::user()->id,
        // ]);

        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.companies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Company = Company::find($id);
        return view ('admin.companies.show',compact('Company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_companies_edit')) {
            return abort(401);
        }
        $Company = Company::findOrFail($id);
        $cities=City::all();
        $groups = Groups::whereNull('company_id')->whereNull('city_id')->whereNull('branch_id')->where('level','!=',1)->get();
        
        return view('admin.companies.edit', compact('Company','cities','groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_companies_edit')) {
            return abort(401);
        }

        Company::where('id',$id)->update([
            'name'              => $request['name'],
            'address'           => $request['address'],
             'ntn'           => $request['ntn'],
            'phone'              => $request['phone'],
            'fax'               => $request['fax'],
            'description'           => $request['description'],
            'updated_by'        => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();
        return redirect()->route('admin.companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_companies_destroy')) {
            return abort(401);
        }

        $Company = Company::findOrFail($id);
        $Company->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.companies.index');
    }
    public function companies_detail($id){
        $company_details = Company::where('id',$id)->get();
        return view('admin.companies.show',compact('company_details'));
    }

}
