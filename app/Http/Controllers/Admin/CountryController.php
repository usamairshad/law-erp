<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Auth;
use Config;
class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if (! Gate::allows('erp_countries_manage')) {
            return abort(401);
        }

        $Country = Country::all();
        return view('admin.country.index', compact('Country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_countries_create')) {
            return abort(401);
        }
        return view('admin.country.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_countries_create')) {
            return abort(401);
        }
        $rules['name']  =  'required|unique:country';
        $Country = Country::create([
            'name' => $request['name'],
        ]);
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.country.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (! Gate::allows('erp_countries_edit')) {
            return abort(401);
        }
        $Country = Country::findOrFail($id);

        return view('admin.country.edit', compact('Country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_countries_edit')) {
            return abort(401);
        }
        Country::where('id',$id)->update([
            'name' => $request['name'],
        ]);
        flash('Record has been updated successfully.')->success()->important();
        return redirect()->route('admin.country.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
