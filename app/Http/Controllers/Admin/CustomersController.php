<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Branches;
use App\Models\Admin\Employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Customers\StoreUpdateRequest;
use App\Models\Admin\Customers;
use App\Helpers\CoreAccounts;
use Auth;
use Config;

class CustomersController extends Controller
{
    /**
     * Display a listing of Customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_customers_manage')) {
            return abort(401);
        }

        $Customers = Customers::OrderBy('created_at','desc')->get();

        return view('admin.customers.index', compact('Customers'));
    }

    /**
     * Show the form for creating new Customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_customers_create')) {
            return abort(401);
        }

        // Get All Employees
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        return view('admin.customers.create', compact('Employees', 'Branches'));
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param  \App\Http\Requests\Admin\Customers\StoreUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateRequest $request)
    {
        if (! Gate::allows('erp_customers_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        Customers::create($data);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.customers.index');
    }


    /**
     * Show the form for editing Customer.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_customers_edit')) {
            return abort(401);
        }

        // Get All Employees
        $Employees = Employees::pluckActiveOnly();
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        $Customer = Customers::findOrFail($id);

        return view('admin.customers.edit', compact('Customer', 'Employees', 'Branches'));
    }

    /**
     * Update Customer in storage.
     *
     * @param  \App\Http\Requests\Admin\Customers\StoreUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        if (! Gate::allows('erp_customers_edit')) {
            return abort(401);
        }

        $Customer = Customers::findOrFail($id);
        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;
        $Customer->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.customers.index');
    }


    /**
     * Remove Customer from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_customers_destroy')) {
            return abort(401);
        }
        $Customer = Customers::findOrFail($id);
        $Customer->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.customers.index');
    }

    /**
     * Activate Customer from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_customers_active')) {
            return abort(401);
        }

        $Customer = Customers::findOrFail($id);


        $Customer->update(['status' => 1]);

        $Customerrece = CoreAccounts::getConfigGroup(Config::get('constants.accounts_debt_customer'));
        $Ledgerrece = array(
            'name' => $Customer['name'],
            'group_id' => $Customerrece ['group']->id,
            'balance_type' => 'd',
            'opening_balance' => 0,
            'parent_type' => $id,
        );

        CoreAccounts::createLedger($Ledgerrece);


        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.customers.index');
    }

    /**
     * Inactivate Customer from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_customers_inactive')) {
            return abort(401);
        }

        $Customer = Customers::findOrFail($id);
        $Customer->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.customers.index');
    }

    /**
     * Delete all selected Customer at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_customers_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Customers::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
