<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\Classes;
use App\Models\Academics\Program;
use App\Models\Academics\Course;
use App\Models\Academics\AcademicsGroup;
use App\Models\Academics\AcademicsGroupCourse;
use Auth;
use Config;
use App\Helpers\Helper;
use Redirect;

class AcademicsGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $ag= AcademicsGroup::all();
        $courses=Course::all();

        return view('admin.acdemics-group.acdemics-group-index',compact('ag','courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $programs= Program::all();
        return view('admin.acdemics-group.acdemics-group-create',compact('programs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $input= $request->all();
        for($i = 0; $i < count($input['program_id']) ; $i++) { 
                $arr= new AcademicsGroup;
                $arr->program_id=$input['program_id'][$i];
                $arr->name=$input['name'][$i];
                $arr->save(); 

        }

        return redirect()->route('admin.academics-groups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function academic_courses($id)
    {
        $program = Program::where('id','=',$id)->first();
        $courses= Course::all();
  
        return view('admin.acdemics-group.add_academic_group',compact('program','courses'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function academicsGroupsCourses()
    {
         return view('admin.acdemics-group.acdemics-group-courses-index');
    }
    public function academicsGroupsCoursesCreate()
    {
        $programs= Program::all();
        return view('admin.acdemics-group.acdemics-group-create',compact('programs'));
    }

    public function academicsGroupsCoursesStore(Request $request)
    {
        $input=$request->all();
        foreach ($input['course_id'] as $key => $value) {
            AcademicsGroupCourse::create([
                'academics_group_id'=>$input['academics_group_id'],
                'course_id'=>$value
            ]);
        }
        return Redirect('admin/academics-groups');
    }
    public function loadAcademicsGroupCourses(Request $request)
    {

        $temp= AcademicsGroupCourse::where('academics_group_id',$request->academics_group_id)->get();

        $data=[];   
        foreach ($temp as $key => $value) {
            $data[$key]['course_id']= Helper::subjectIdToName($value['course_id']);
            $data[$key]['academics_group_id']= $value['academics_group_id'];
        }
        return $data;
    }
}
