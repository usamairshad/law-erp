<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\ProgramGroup;
use App\Models\Academics\Section;
use App\Models\Academics\Program;
use App\Models\Academics\AssignClassSection;
use App\Models\Academics\Classes;
use App\Models\Academics\Course;
use App\Models\Academics\CompulsoryCourses;
use Auth;
use Config;
use App\Helpers\Helper;
use Redirect;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd("Asas");
        $pg= ProgramGroup::all();
        
       
        $data=[];
        foreach ($pg as $key => $value) {

           $bg= BoardProgram::find($value['board_program_id']);
           $data[$key]['program_id']= Helper::programIdToName($bg['program_id']);
           $data[$key]['board_id']= Helper::boardIdToName($bg['board_id']);
           $data[$key]['name']=$pg[$key]['name'];
           $data[$key]['id']=$pg[$key]['id'];
        }

        return view('admin.program.program-group-index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boards= Board::all();
        return view('admin.program.program-group-create',compact('boards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
    $this->validate($request, [
            
            'name' => 'required | alpha | unique:erp_city',

        ]);
        $input=$request->all();
        $bp= BoardProgram::where('board_id',$input['board_id'])->where('program_id',$input['program_id'])->first();
        $board_name = $bp->board->name;
        $program_name = $bp->program->name;
        $name = $board_name."-".$program_name."-".$input['name'];
        
        ProgramGroup::create([
                'board_program_id' =>$bp->id,
                'name' =>$input['name']
        ]);
        return redirect()->route('admin.program-group.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function compulsoryCoursesStore(Request $request)
    {
        \Log::info($request);
        $input=$request->all();
        foreach ($input['course_id'] as $key => $value) {
            CompulsoryCourses::create([
                'program_id'=>$input['program_id'],
                'course_id'=>$value
            ]);
        }
        return Redirect::back();
    }
    public function loadProgramCompulsoryCourses(Request $request)
    {
        \Log::info($request);
        
        \Log::info('======================');
        $temp= CompulsoryCourses::where('program_id',$request->program_id)->get();

        \Log::info($temp);

        $data=[];   
        foreach ($temp as $key => $value) {
            $data[$key]['course_id']= Helper::subjectIdToName($value['course_id']);
            $data[$key]['program_id']= $value['program_id'];
        }
        return $data;
    }
}
