<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\GroupsTree;
use App\Models\Admin\AccountTypes;
use App\Models\Admin\Groups;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Admin\City;
use App\Models\Admin\Company;
use App\Models\Admin\Branches;
use Auth;
use Config;

use App\Helpers\CoreAccounts;

class GroupsController extends Controller
{
    /**
     * Display a listing of Group.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     /*    if (! Gate::allows('erp_groups_manage')) {
            return abort(401);
        } */
       $AccountTypes = AccountTypes::all()->getDictionary();

        /* Create list of parent groups */
        $parentGroups = new GroupsTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
       
        $parentGroups->toListView($parentGroups, -1);

        $Groups = $parentGroups->groupListView;

        return view('admin.groups.index', compact('Groups', 'AccountTypes'));
    }

    /**
     * Show the form for creating new Group.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_groups_create')) {
            return abort(401);
        }
        $company = Company::all()->pluck('name','id');
        $city = City::all()->pluck('name','id');
        $branches = Branches::all()->pluck('name','id');
         
        // Get All Account Types
        $AccountTypes = AccountTypes::getActiveListDropdown(true);

        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));

        return view('admin.groups.create', compact('Groups', 'AccountTypes','city','company','branches'));
    }

    /**
     * Store a newly created Group in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_groups_create')) {
            return abort(401);
        } 

        $response = CoreAccounts::createGroup($request->all());

        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.groups.index');
        } else {
            $request->flash();

            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }


    /**
     * Show the form for editing Group.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_groups_edit')) {
            return abort(401);
        }

        $Group = Groups::findOrFail($id);
        $company = Company::all()->pluck('name','id');
        $city = City::all()->pluck('name','id');
        $branches = Branches::all()->pluck('name','id');
        if(in_array($Group->id, Config::get('constants.accounts_main_heads')) && $Group->parent_id == 0) {
            return redirect()->route('admin.groups.index');
        }

        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray(), 0, $Group->id), $Group->parent_id);

        return view('admin.groups.edit', compact('Group', 'Groups','company','city','branches'));
    }

    /**
     * Update Group in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_groups_edit')) {
            return abort(401);
        }

        $response = CoreAccounts::updateGroup($request->all(), $id);
        if($response['status']) {
            flash('Record has been updated successfully.')->success()->important();
            return redirect()->route('admin.groups.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * Remove Group from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_groups_destroy')) {
            return abort(401);
        }
        $Group = Groups::findOrFail($id);

        // Those Ledgers can't be deleted who have child groups or ledgers associagted with it.
        if(Groups::hasChildLedgers($Group->id) || Groups::hasChildGroups($Group->id)) {
            flash('One or more Group(s) / Ledger(s) are associated with "' . $Group->name . '" group.')->error()->important();
        } else {
            $Group->delete();
        }

        return redirect()->route('admin.groups.index');
    }

    /**
     * Delete all selected Group at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_groups_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Groups::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
