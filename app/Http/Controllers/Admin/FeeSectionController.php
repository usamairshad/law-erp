<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Country;
use App\Models\Admin\FeeSection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Config;

class FeeSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if (! Gate::allows('erp_fee_section_manage')) {
            return abort(401);
        }

        $feeSection = FeeSection::all();
        return view('admin.fee_section.index', compact('feeSection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_fee_section_create')) {
            return abort(401);
        }
        return view('admin.fee_section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_fee_section_create')) {
            return abort(401);
        }
        $rules['feeSection_name']  =  'required|unique:country';
        $CountryModel = FeeSection::create([
            'name' => $request['name'],
            'description'=>$request['description'],
        ]);
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.fee_section.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_fee_section_edit')) {
            return abort(401);
        }
        $feeSection= FeeSection::findOrFail($id);
        return view('admin.fee_section.edit', compact('feeSection'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_fee_section_edit')) {
            return abort(401);
        }
        FeeSection::where('id',$id)->update([
            'name' => $request['name'],
            'description'=>$request['description'],
        ]);
        flash('Record has been updated successfully.')->success()->important();
        return redirect()->route('admin.fee_section.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
