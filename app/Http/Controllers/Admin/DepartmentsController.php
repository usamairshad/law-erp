<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Branches;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDepartmentsRequest;
use App\Http\Requests\Admin\UpdateDepartmentsRequest;
use App\Models\Admin\Departments;
use Illuminate\Support\Facades\Validator;
use Auth;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of Department.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_departments_manage')) {
            return abort(401);
        }

        $Departments = Departments::OrderBy('created_at','desc')->get();

        return view('admin.departments.index', compact('Departments'));
    }

    /**
     * Show the form for creating new Department.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_departments_create')) {
            return abort(401);
        }
        $branch = Branches::all()->pluck('name','id');
        return view('admin.departments.create', compact('branch'));
    }

    /**
     * Store a newly created Department in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreDepartmentsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepartmentsRequest $request)
    {
        if (! Gate::allows('erp_departments_create')) {
            return abort(401);
        }
        $this->validate($request, [
            'branch_id' => 'required',
        ]);
        Departments::create([
            'name'          => $request['name'],
            'branch_id'     => $request['branch_id'],
            'created_by'    => Auth::user()->id,
            'updated_by'    => Auth::user()->id,
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.departments.index');
    }


    /**
     * Show the form for editing Department.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_departments_edit')) {
            return abort(401);
        }
        $Department = Departments::findOrFail($id);
        $branch = Branches::all()->pluck('name','id');
        return view('admin.departments.edit', compact('Department','branch'));
    }

    /**
     * Update Department in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateDepartmentsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepartmentsRequest $request, $id)
    {
        if (! Gate::allows('erp_departments_edit')) {
            return abort(401);
        }
        $this->validate($request, [
            'branch_id' => 'required',
        ]);
        $Department = Departments::findOrFail($id);
        $Department->update([
            'name'          => $request['name'],
            'branch_id'     => $request['branch_id'],
            'updated_by'    => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.departments.index');
    }


    /**
     * Remove Department from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_departments_destroy')) {
            return abort(401);
        }
        $Department = Departments::findOrFail($id);
        $Department->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.departments.index');
    }

    /**
     * Delete all selected Department at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_departments_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Departments::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
