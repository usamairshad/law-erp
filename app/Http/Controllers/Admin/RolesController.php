<?php

namespace App\Http\Controllers\Admin;

// use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRolesRequest;
use App\Http\Requests\Admin\UpdateRolesRequest;
use App\Roles;
use App\Models\Admin\Role as AcademicRole;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_roles_manage')) {
            return abort(401);
        }

        $Roles = Role::all();

        return view('admin.roles.index', compact('Roles'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_roles_create')) {
            return abort(401);
        }

        // Get list of all allowed permissions for current role.
        $AllowedPermissions = Permission::join('erp_role_has_permissions', 'erp_role_has_permissions.permission_id', '=', 'erp_permissions.id')
            ->get()->pluck('name', 'id');
        if(!$AllowedPermissions) {
            $AllowedPermissions = [];
        }

        $GroupPermissions = Permission::where(['main_group' => 1])->get()->toArray();
        $SubPermissions = Permission::whereIn('parent_id', Permission::where(['main_group' => 1])->pluck('id'))->get()->toArray();

        //******Permission Variable remove due to undefined

        return view('admin.roles.create', compact('GroupPermissions', 'SubPermissions', 'AllowedPermissions'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreRolesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRolesRequest $request)
    {
        //dd($request);
        if (! Gate::allows('erp_roles_create')) {
            return abort(401);
        }
        $data = $request->all();
       // echo '<pre>'; print_r($data); exit;
        $role = Role::create($request->except('permission'));
       
        $permissions = $request->input('permission') ? $request->input('permission') : [];
        $role->givePermissionTo($permissions);
        

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.roles.index');
    }


    /**
     * Show the form for editing Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_roles_edit')) {
            return abort(401);
        }

        $Role = Role::findOrFail($id);

        // Get list of all allowed permissions for current role.
        $AllowedPermissions = Permission::join('erp_role_has_permissions', 'erp_role_has_permissions.permission_id', '=', 'erp_permissions.id')
            ->where(['erp_role_has_permissions.role_id' => $Role->id])
            ->get()->pluck('name', 'id');
        if(!$AllowedPermissions) {
            $AllowedPermissions = [];
        }

        $GroupPermissions = Permission::where(['main_group' => 1])->get()->toArray();
        $SubPermissions = Permission::whereIn('parent_id', Permission::where(['main_group' => 1])->pluck('id'))->get()->toArray();

        return view('admin.roles.edit', compact('Role', 'GroupPermissions', 'SubPermissions', 'AllowedPermissions'));
    }

    /**
     * Update Role in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateRolesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRolesRequest $request, $id)
    {
        if (! Gate::allows('erp_roles_edit')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->update($request->except('permission'));
        $permissions = $request->input('permission') ? $request->input('permission') : [];
        $role->syncPermissions($permissions);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.roles.index');
    }


    /**
     * Remove Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_roles_destroy')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->delete();
        flash('Record has been deleted successfully.')->success()->important();
        return redirect()->route('admin.roles.index');
    }


    /**
     * Activate Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_roles_active')) {
            return abort(401);
        }

        $Role = Role::findOrFail($id);
        $Role->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.roles.index');
    }

    /**
     * Inactivate Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_roles_inactive')) {
            return abort(401);
        }

        $Role = Role::findOrFail($id);
        $Role->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.roles.index');
    }
    public function getRoleByRoleType(Request $request)
    {
        $input= $request->all();

        
        if($input['id'] == 'erp')
        {
           $data=Role::whereIn('name',['Ceo','administrator'])->get();

            \Log::info($data);

        }
        // else
        // {
        //     $data=AcademicRole::all();
           
        // }

        return $data;
    }

    public function getRoleByRoleTypeStaff(Request $request)
    {
        $input= $request->all();

        
        if($input['id'] == 'erp')
        {
           $data=Role::whereNotIn('name',['Ceo','administrator'])->get();

            // \Log::info($data);

        }
        else
        {
            $data=AcademicRole::whereNotIn('name',['super-admin','admin'])->get();
           
        }

        return $data;
    }

}
