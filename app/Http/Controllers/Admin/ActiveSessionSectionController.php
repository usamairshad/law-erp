<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Academics\ActiveSessionSection;
use App\Models\Academics\Section;
use App\Models\Academics\ActiveSession;

class ActiveSessionSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $active_sessions = ActiveSessionSection::with('section','activesession')->OrderBy('created_at','desc')->get();
        //dd($active_sessions);
        return view('admin.active-session-section.index', compact('active_sessions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $sessionList = Section::all();
        $activesessionList = ActiveSession::all();
        return view('admin.active-session-section.create', compact('sessionList','activesessionList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);   
        $this->validate($request, [
            'active_session_id' => 'required_without:active_session_id|not_in:0',
            'section_id' => 'required_without:active_session_id|not_in:0',
            
        ]);
        $inputs = $request->input();
        //dd($inputs);
        $check = ActiveSessionSection::where('active_session_id', '=',$inputs['active_session_id'])->where('section_id', '=',$inputs['section_id'])->first();
        if ($check == '') {
            ActiveSessionSection::create($inputs);
        flash('Record has been Created successfully.')->success()->important();
        return redirect('/active-session-section');
        }else{
            flash('Data already exist.')->success()->important();
        return redirect('/active-session-section');
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
