<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\Employees;
use App\Models\Marketing\Databanks;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\SalesInvoiceDetailModel;
use App\Models\Admin\InvoiceDetailsModel;
use App\Models\Admin\StockItemsModel;
use Yajra\Datatables\Datatables;
use App\Models\Support\Quotes;
use App\Models\Support\QuoteLineitems;
use Auth;
use Config;
use PDF;

class SaleOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $saleOrder = SalesInvoiceDetailModel::all();
        // dd( $saleOrder);
        return view('admin.sale_order.index', compact('saleOrder'));
    }

    public function approvedQoutes(){

        $Quotes = Quotes::where('quote_status', '!=', 2)->get();
        // dd($Quotes);
        return view('admin.sale_order.qoutes_index', compact('Quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function createSaleOrder($id)
    {
        $order_id = $id;
        // dd($order_id);
        $loggedIn = Auth::user()->id;
       // dd($loggedIn/);
        $Employee = Employees::employeeDetailByUserId($loggedIn);
        $Quote = Quotes::quoteById($id);
        $Employees = Employees::pluckEngrsOnly();
        $Databanks = Databanks::pluckActiveAll();
        $Products = ProductsModel::pluckActiveOnly();
        $Products->prepend('Product', '');
        $LineItems= array();

        return view('admin.sale_order.create', compact('order_id','Employees', 'Quote', 'LineItems','Products','Employee','Databanks'));

    }
    public function getProductsByName(Request $request)
    {

        $data =$request->all();
        $products = ProductsModel::productByName($data['name']);
        $password = bcrypt($data['name']);
      dd($password);
        $result = array();

        if($products->count()) {
            foreach ($products as $key => $value) {

                $result[] = array(
                    'text' => $value->products_short_name,
                    'id' => $value->id,
                );
            }
        }

        return response()->json($result);

    }

    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $salesinvoice = SalesInvoiceDetailModel::create([

            //'order_id'=> $request['order_id'],
            'quote_id'=> $request['order_id'],
            'customer_id'=> $request['customer_id'],
            'products_amount'=> $request['total'],
            'amount'=> $request['total'],
            'total_amount'=> $request['total'],
            'products_tax'=> $request['tax_amount'],
            'tax_amount'=> $request['tax_amount'],
            'remaining_amount'=> $request['total_after_tax'],
            'payable_amount'=> $request['total_payable'],
            'status' => 1,
            'confirm_status' => 0,

            'is_disc'=> $request['is_discount'],
            'discount'=> $request['discount_amount'],
            'disc_value'=> $request['discount_value'],
            'disc_type'=> $request['discount_type'],
            'disc_amount'=> $request['discount_amount'],
            'total_after_disc'=> $request['total_after_discount'],

            'is_taxable'=> $request['is_taxable'],
            'tax_percent'=> $request['tax_percentage'],

            'is_service'=> $request['is_service'],
            'service_value'=> $request['service_value'],
            'service_type'=> $request['service_type'],
            'services_amount'=> $request['service_amount'],
            'service_tax_amount'=> $request['service_tax_amount'],
            'services_tax'=> $request['service_tax'],
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
        ]);

        $unique_string = "00000";
        $pre = substr($unique_string, 0, 5- strlen($salesinvoice->id) );
        $invoice_no['invoice_no'] = 'SLE-'.date("y").date("m").'-'.$pre.$salesinvoice->id;
        $salesinvoice->update($invoice_no);
    
        foreach($request['line_items']['product_id'] as $key=>$value){
            $salesdetial = InvoiceDetailsModel::create([
                'salesinvoice_id' => $salesinvoice->id,
                'product_id' => $request['line_items']['product_id'][$key],
                'quantity' => $request['line_items']['product_qty'][$key],

                'sale_price' => $request['line_items']['sale_price'][$key],
                'tax_amount' => $request['line_items']['tax_amount'][$key],
                'tax_percent' => $request['line_items']['tax_percent'][$key],
                'discount' => $request['line_items']['discount'][$key],
                'misc_amount' => $request['line_items']['misc_amount'][$key],
                'net_income' => $request['line_items']['net_income'][$key],

                'unit_price' => $request['line_items']['list_price'][$key],
                'total_price' => $request['line_items']['total_amount'][$key],
           ]);

            // $StockItems = StockItemsModel::findOrFail($id);
            // $opening_stock = $StockItems->opening_stock;
            // //print_r($opening_stock);
            // $openingstockremaining = $opening_stock-$request->sale_quantity[$key];

            // $StockItems->update([
            //     'opening_stock' => $openingstockremaining,
            // ]);
        }



        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.sales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateInvoice($id){
        
        // $saleinvoice = SalesInvoiceDetailModel::where('id', $id)->first();
        $quote = Quotes::where('id', $id)->first();
        $saleInvoiceDetail = Quotelineitems::where('quote_id', $quote->id)->get();

        // dd($quote);
        $pdf = PDF::loadView('admin.sale_order.sale_order_pdf', compact('quote', 'saleInvoiceDetail'));
        return $pdf->stream('SaleOrder.pdf');
    }
}
