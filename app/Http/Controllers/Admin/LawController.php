<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TaxSettings\StoreUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Admin\Groups;
use App\Helpers\GroupsTree;
use App\Models\Laws;
use App\Models\Admin\TaxSettings;
use Auth;

class LawController extends Controller
{
    /**
     * Display a listing of Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // if (! Gate::allows('erp_tax_settings_manage')) {
        //     return abort(401);
        // }


        $Law = Laws::OrderBy('created_at','desc')->get();

        return view('admin.law.index', compact('Law'));
    }

    /**
     * Show the form for creating new Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));
        return view('admin.law.create',compact('Groups'));
    }

    /**
     * Store a newly created Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreUpdateTaxSettings  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
        $input= $request->all();
        for($i = 0; $i < count($input['name']) ; $i++) { 
            $arr= new Laws;
            $arr->law_section=$input['name'][$i];
            $arr->description=$input['description'][$i];
            $arr->group_id=$input['parent_id'][$i];
            $arr->save();
        }
    
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('tax-law');
    }


    /**
     * Show the form for editing Branche.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
        $Branche = Laws::join('erp_groups','erp_groups.id','=','erp_laws.group_id')->where('law_id',$id)->first();
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));
        return view('admin.law.edit', compact('Branche','Groups'));
    }

    /**
     * Update Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateBrancheRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
$id = $request->id;
        $Branche = Laws::where('law_id',$id);
        $Branche->update([
            'law_section' => $request['name'],
            'description' => $request['description'],
            'group_id'=>$request['parent_id']
       //     'updated_by' => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('tax-law');
    }


    /**
     * Remove Branche from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        

        $Branche = Laws::where('law_id',$id);

      
        $Branche->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('tax-law');
    }

}
