<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Device;
use App\Models\Admin\Branches;
use Auth;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $devices = Device::all();

        return view('admin.device.index', compact('devices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $branchList = Branches::pluck('name', 'id');

         return view('admin.device.create', compact('branchList'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        Device::create([
                'branch_id' => $request->branch_id,
                'device_no' => $request->device_no,
                'ip_address' => $request->ip_address,
                'port' => $request->port,
                'created_by' => Auth::user()->id,
               
            ]);
         flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.device.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $devices = Device::find($id);
        $branchList = Branches::pluck('name', 'id');
        return view('admin.device.edit', compact('devices','branchList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
       $devices = Device::findOrFail($id);

        $data['updated_by'] = Auth::user()->id;

        $devices->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.device.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Device = Device::findOrFail($id);
        $Device->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.device.index');
    }
}
