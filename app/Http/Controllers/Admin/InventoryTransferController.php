<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Admin\InventoryTransferModel;
use App\Models\Admin\InventoryTransferDetailModel;
use App\Models\Admin\Branches;
use App\Models\Admin\SuppliersModel;
use App\Models\Admin\StockItemsModel;
use App\Models\Admin\Warehouse;
use App\Models\Admin\FixedAssetsModel;

use App\Models\LandManagement\PayeeList;
use Auth;

class InventoryTransferController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $InventoryTransfer = InventoryTransferModel::all();


        return view('admin.inventorytransfer.index', compact('InventoryTransfer'));
    }

    public function datatables()
    {
        $InventoryTransfer = InventoryTransferModel::all();
        return  datatables()->of($InventoryTransfer)
            -> addColumn('branch_from', function($InventoryTransfer) {
                return  $InventoryTransfer->branches->name;
            })
            ->addColumn('branch_to', function($InventoryTransfer) {
                return  $InventoryTransfer->branchesto->name;
            })->addColumn('sup_id', function($InventoryTransfer) {
                return  $InventoryTransfer->suppliersModel->sup_name;
            }) ->addColumn('created_at', function($InventoryTransfer) {
                return  date('d-m-Y  h:m a', strtotime($InventoryTransfer->created_at));
            })->addColumn('action', function ($InventoryTransfer) {
               $action_column  = '<a href="inventorytransfer/'.$InventoryTransfer->id.'" class="btn btn-xs btn-primary">View</a> </br>';
//                $action_column = '<a href="inventorytransfer/'.$InventoryTransfer->id.'/edit" class="btn btn-xs btn-primary">View</a>';
//                $action_column .= '<form method="post" action="inventorytransfer/delete" >';
//                $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
//                $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
//                $action_column .= '<input type="hidden" value="21" name="products_name" >';
//                $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
//                $action_column .= '<input type="hidden" value='.$InventoryTransfer->id.' name="name" >';
//                $action_column .=  csrf_field();
//                $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                return $action_column;
            })
            ->rawColumns(['branch_from','branch_to','sup_id','created_at','action'])
            ->toJson();
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $StockItems = StockItemsModel::all();
        $Warehouse = Branches::all();
        $Suppliers = PayeeList::all();
        return view('admin.inventorytransfer.create', compact('Suppliers','Warehouse','StockItems'));
    }

    public function stockrecord($id)
    {
        
        $StockItems = StockItemsModel::find($id);
        return view('admin.inventorytransfer.performa', compact('StockItems'));
    }
    public function stocklist(Request $request)
    {
       
        $data = $request->all();
        $StockItemsModel = FixedAssetsModel::where('product_name','like','%'.$data['item'].'%')->where('branch',$data['branch_id'])->get();
        $result = array();
        if($StockItemsModel->count()) {
            foreach ($StockItemsModel as $key => $value) {
                $result[] = array(
                    'text' => $value->product_name.' - ('.$value->created_at.') - ['.$value->closing_stock.']',
                    'id' => $value->id,
                );
            }
        }

        return response()->json($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       
   // dd($request->all());
        $inventory_id = InventoryTransferModel::create([
            'branch_from'           => $request['branch_id'],
            'branch_to'             => $request['branch_to'],
            'created_by'            => Auth::user()->id,
        ]);
        foreach($request['InventoryItems']['product_id'] as $key=>$value)
        {

            $getqty = FixedAssetsModel::where('id',$request['InventoryItems']['product_id'][$key])->first();

            
            $total_stock = $getqty->closing_stock;



            $product_name = $getqty->product_name;

            $suplier_id = $getqty->suplier_id;


            $st_unit_price = $getqty->st_unit_price;
            InventoryTransferDetailModel::create([
                'i_transfer_id'    =>$inventory_id->id,
                'product_id'       =>$getqty->id,
                'transfer_qty'     =>$request['InventoryItems']['qunity'][$key],
                'created_by'       => Auth::user()->id,
            ]);
            $remianing_stock = $total_stock - $request['InventoryItems']['qunity'][$key];
            $Stockverify = FixedAssetsModel::where('product_name',$product_name)->where('branch',$request['branch_to'])->first();


            //dd($Stockverify);



            if ($Stockverify) {
                $Inventory_update_stock = $Stockverify->closing_stock;
                $updatestock = FixedAssetsModel::where('id',$Stockverify->id)->update(array(
                    'closing_stock' => $Inventory_update_stock,
                ));

            }else{
                FixedAssetsModel::create([
                    'branch'               =>  $request['branch_to'],
                    'closing_stock'        => $request['InventoryItems']['qunity'][$key],
                    'product_name'         => $product_name,
                    'st_unit_price'        =>$st_unit_price,
                    'suplier_id'           =>$suplier_id,
                    'created_by'           => Auth::user()->id,
                ]);
            }

          FixedAssetsModel::where('id',$getqty->id)->update(array(
                'closing_stock' => $remianing_stock,
            ));
            
            
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.inventorytransfer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $InventoryTransferModel = InventoryTransferModel::findOrFail($id);
        $Transfer_detail = InventoryTransferDetailModel::where('i_transfer_id',$InventoryTransferModel->id)->get();
        return view('admin.inventorytransfer.orderdisplay', compact('Transfer_detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $StockItems = StockItemsModel::all();
        $InventoryTransferModel = InventoryTransferModel::findOrFail($id);
        $Transfer_detail = InventoryTransferDetailModel::where('i_transfer_id',$InventoryTransferModel->id)->get();
        $Branches = Branches::pluckActiveOnly();
        $Suppliers = SuppliersModel::all();
        return view('admin.inventorytransfer.edit', compact('Suppliers','Branches','StockItems','Transfer_detail','InventoryTransferModel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_inventorytransfer_edit')) {
            return abort(401);
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (! Gate::allows('erp_inventorytransfer_destroy')) {
            return abort(401);
        }
        //
    }

}
