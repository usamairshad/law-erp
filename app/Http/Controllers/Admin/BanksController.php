<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\BanksModel;
use App\Models\Admin\Groups;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\UpdateBanksRequest;
use App\Helpers\CoreAccounts;
use Illuminate\Support\Facades\Validator;
use Auth;
use Config;

class BanksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Banks = BanksModel::all();
//        dd($Banks);
        return view('admin.banks.index', compact('Banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.banks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request  $request)
    {

        $rules['bank_name']  =  'required|unique:banks';
        $validator = Validator::make($request->all(), $rules);
//        if ($validator->fails()) {
//            $request->flash();
//            return redirect()->back()
//                ->withErrors($validator->errors())
//                ->withInput();
//        }
       $BanksModel = BanksModel::create([
            'bank_name' => $request['bank_name'],
            'created_by' =>  Auth::user()->id,
        ]);
       $Group = array(
            'name' => $request['bank_name'],
            'parent_id' => 25,
            'parent_type' => $BanksModel->id,
        );

      
    /*     $Banks_balance_euro = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_balance_euro'));
        $Group = array(
            'name' => $request['bank_name'],
            'parent_id' => $Banks_balance_euro['group']->id,
            'parent_type' => $BanksModel->id,
        );
        CoreAccounts::createGroup($Group);
        $Banks_import = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_cd_importer'));
        $Group = array(
            'name' => $request['bank_name'],
            'parent_id' => $Banks_import['group']->id,
            'parent_type' => $BanksModel->id,
        );
        CoreAccounts::createGroup($Group);
        $Banks_contract = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_contract'));
        $Group = array(
            'name' => $request['bank_name'],
            'parent_id' => $Banks_contract['group']->id,
            'parent_type' => $BanksModel->id,
        );
        CoreAccounts::createGroup($Group);
        $Banks_lc = CoreAccounts::getConfigGroup(Config::get('constants.account_Lc_Transit'));
        $Group = array(
            'name' => $request['bank_name'],
            'parent_id' => $Banks_lc['group']->id,
            'parent_type' => $BanksModel->id,
        ); */
        CoreAccounts::createGroup($Group);
        return redirect()->route('admin.banks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $Banks = BanksModel::findOrFail($id);

        return view('admin.banks.edit', compact('Banks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBanksRequest $request, $id)
    {
     
        //$Banks = BanksModel::findOrFail($id);
        BanksModel::where('id',$id)->update([
            'bank_name' => $request['bank_name'],
            'updated_by' => Auth::user()->id,
        ]);
        $Banks_balance_local = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_balance_local'));
        Groups::where('parent_type',$id)->where('parent_id',25)->update([
          'name' => $request['bank_name'],
          'updated_by' => Auth::user()->id,
        ]);
       $Banks_balance_us = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_balance_us'));
//        Groups::where('parent_type',$id)->where('parent_id',$Banks_balance_us['group']->id)->update([
//            'name' => $request['bank_name'],
//            'updated_by' => Auth::user()->id,
//        ]);
//        $Banks_balance_euro = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_balance_euro'));
//        Groups::where('parent_type',$id)->where('parent_id',$Banks_balance_euro['group']->id)->update([
//            'name' => $request['bank_name'],
//            'updated_by' => Auth::user()->id,
//        ]);
//        $Banks_imported = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_cd_importer'));
//        Groups::where('parent_type',$id)->where('parent_id',$Banks_imported['group']->id)->update([
//            'name' => $request['bank_name'],
//            'updated_by' => Auth::user()->id,
//        ]);
//        $Bank_contract = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_contract'));
//        Groups::where('parent_type',$id)->where('parent_id',$Bank_contract['group']->id)->update([
//            'name' => $request['bank_name'],
//            'updated_by' => Auth::user()->id,
//        ]);
//        $Bank_transit = CoreAccounts::getConfigGroup(Config::get('constants.account_Lc_Transit'));
//        Groups::where('parent_type',$id)->where('parent_id',$Bank_transit['group']->id)->update([
//            'name' => $request['bank_name'],
//            'updated_by' => Auth::user()->id,
//        ]);

        return redirect()->route('admin.banks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     
        $Banks = BanksModel::findOrFail($id);
        $Banks->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.banks.index');
    }

    /**
     * Delete all selected Orders at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_banks_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = BanksModel::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
