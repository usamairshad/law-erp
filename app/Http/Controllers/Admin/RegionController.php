<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\CountryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRegionsRequest;
use App\Http\Requests\Admin\UpdateRegionsRequest;
use App\Models\Admin\Regions;
use Auth;

class RegionController extends Controller
{
    /**
     * Display a listing of Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_regions_manage')) {
            return abort(401);
        }

        $Regions = regions::OrderBy('created_at','desc')->get();

        return view('admin.regions.index', compact('Regions'));
    }

    /**
     * Show the form for creating new Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_regions_create')) {
            return abort(401);
        }
        $country = CountryModel::all()->pluck('country_name','id');
        return view('admin.regions.create', compact('country'));
    }

    /**
     * Store a newly created Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreregionsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreregionsRequest $request)
    {
        if (! Gate::allows('erp_regions_create')) {
            return abort(401);
        }

        regions::create([
            'name' => $request['name'],
            'country_id' => $request['country_id'],
            'created_by' => Auth::user()->id,
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.regions.index');
    }


    /**
     * Show the form for editing Branche.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_regions_edit')) {
            return abort(401);
        }
        $regions = regions::findOrFail($id);
        $country = CountryModel::all()->pluck('country_name','id');
        return view('admin.regions.edit', compact('regions','country'));
    }

    /**
     * Update Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateBrancheRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateregionsRequest $request, $id)
    {
        if (! Gate::allows('erp_regions_edit')) {
            return abort(401);
        }
        regions::where('id',$id)->update([
            'name' => $request['name'],
            'country_id' => $request['country_id'],
            'updated_by' => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.regions.index');
    }


    /**
     * Remove Branche from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_regions_destroy')) {
            return abort(401);
        }

        $regions = regions::findOrFail($id);
        $regions->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.regions.index');
    }

}
