<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\Session;
use App\Models\Admin\Branches;
use App\Models\Academics\Board;
use App\Models\Academics\Program;
use App\Models\Academics\Classes;
use App\Models\Academics\Term;
use Illuminate\Support\Facades\Gate;
use Auth;

class ActiveSessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_session_manage')) {
            return abort(401);
        }
        $active_sessions = ActiveSession::OrderBy('created_at','desc')->get();

        return view('admin.active_session.index', compact('active_sessions')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_session_create')) {
            return abort(401);
        }
        $sessionList = Session::pluck('title','id');
        $boardList = Board::pluck('name','id');
        $branches = Branches::all();
        $programList = Program::pluck('name','id');
        $classList = Classes::pluck('name','id');
        return view('admin.active_session.create',compact('sessionList','boardList','programList','classList','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_session_create')) {
            return abort(401);
        }
        $data = $request->all();
  
        $session_name = Session::where('id',$request->session_id)->value('title');
        $board_name = Board::where('id',$request->board_id)->value('name');
        $program_name = Program::where('id',$request->program_id)->value('name');
        $class_name = Classes::where('id',$request->class_id)->value('name');
        
        $title = $session_name . $board_name . $program_name . $class_name;
        
        foreach ($data['branches'] as $key => $value) {
            $branch_name = Branches::where('id',$value)->value('name');
            ActiveSession::create([
                'title'  => $branch_name."-".$title,
                'session_id' => $data['session_id'],
                'board_id' => $data['board_id'],
                'program_id' => $data['program_id'],
                'class_id' => $data['class_id'],
                'branch_id' => $value,
                'status' => $data['status'],
            ]);
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.active-session.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
