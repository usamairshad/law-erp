<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

use App\Models\Admin\StockItemsModel;
use App\Models\Admin\StockCategoryModel;
use App\Models\Admin\SuppliersModel;
use App\Models\Admin\UnitsModel;
use App\Models\Admin\PerformaStockModel;
use App\Models\Admin\Ledgers;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\Branches;
use App\Models\Admin\ReleasStockModel;
use App\Helpers\CoreAccounts;
use App\Helpers\GroupsTree;
use App\Models\Admin\Groups;
use App\Http\Requests\Admin\StoreStockItemsRequest;
use App\Http\Requests\Admin\UpdateStockItemsRequest;
use DB;
use Auth;

class StockItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SuppliersModel = StockItemsModel::brandByname();
        $SuppliersModel->prepend('Select a Brand', '');
        $StockCategoryModel = StockItemsModel::categoryByname();
        $StockCategoryModel->prepend('Select a Category', '');
        $StockItems = StockItemsModel::all();
        return view('admin.stockitems.index', compact('StockItems','StockCategoryModel','SuppliersModel'));
    }

    public function datatables(StoreStockItemsRequest $request)
    {
        $data = $request->all();
        if(!empty($data['sup_id']) AND !empty($data['cat_id'])){
            $StockItems = StockItemsModel::getproductBycatBrand($data['sup_id'],$data['cat_id']);
        }else if(!empty($data['sup_id'])){
            $StockItems = StockItemsModel::getproductByBrand($data['sup_id']);
        }else if(!empty($data['cat_id'])){
            $StockItems = StockItemsModel::getproductBycat($data['cat_id']);
        }else{
            $StockItems = StockItemsModel::where('closing_stock','>',0)->get();
        }
        return  datatables()->of($StockItems)
            ->addColumn('st_name', function($StockItems) {
                return  $StockItems->stockProducts->products_name.'   -   Stock ('.$StockItems->stockProducts->suppliersModel->sup_name.')';
            }) ->addColumn('short_name', function($StockItems) {
                return  $StockItems->stockProducts->products_short_name;
            })->addColumn('branch_id', function($StockItems) {
                return $StockItems->branches->name ;
            })->addColumn('categrory', function($StockItems) {
                return $StockItems->stockProducts->stockCategoryModel->cat_name ;
            })->addColumn('uom', function($StockItems) {
                return "Pc";
            })->addColumn('stock_value', function($StockItems) {
                $stockVale = round($StockItems->st_unit_price * $StockItems->closing_stock,2) ;
                return $stockVale ;
            })->addColumn('action', function ($StockItems) {

//                if($StockItems->status=='1'){
//                    $action_column = '<a href="stockitems/'.$StockItems->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
//                    $action_column .= '<form method="post" action="stockitems/delete" >';
//                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
//                    $action_column .= '<input type="hidden" value='.$StockItems->ledger_id.' name="ledger_id" >';
//                    $action_column .= '<input type="hidden" value="21" name="products_name" >';
//                    $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
//                    $action_column .= '<input type="hidden" value='.$StockItems->id.' name="name" >';
//                    $action_column .=  csrf_field();
//                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
//                }else{
                    $action_column ='N/A';
                //}

                return $action_column;
            })->rawColumns(['st_name','short_name','branch_id','categrory','uom','stock_value','action'])
            ->toJson();
    }

    public function performa($lc_no,$sup_id)
    {
        $results = ProductsModel::where('cat_id',$lc_no)->where('suplier_id',$sup_id)->get();
        return view('admin.stockitems.performa')->with('results',$results);
    }
    public function modelno($model_no){
        $ProductMdModel = ProductMdModel::where('pro_id',$model_no)->get();
        return view('admin.stockitems.models')->with('ProductMdModel',$ProductMdModel);
    }

    public static function importInventory($inventory){
        foreach ($inventory as $key => $value){
                ReleasStockModel::create([
                    'lc_id' => $value['commercial_invoice'],
                    'product_id' => $value['product_id'],
                    'rel_qty' => $value['rel_qty'],
                    'import_date' => $value['import_date'],
                ]);
                $stock_item =  StockItemsModel::create([
                    'branch_id'           => $value['branch_id'],
                    'st_name'             => $value['product_id'],
                    'opening_stock'       => 0,
                    'purchase_stock'      => $value['rel_qty'],
                    'closing_stock'       => $value['rel_qty'],
                    'moments_stock'       => 0,
                    'st_unit_price'       => $value['unit_price'],
                    'created_by'          => Auth::user()->id,
                ]);
        }
        return;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_stockitems_manage')) {
            return abort(401);
        }
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));
        $Branches = Branches::pluckActiveOnly();
        $UnitsModel = UnitsModel::pluckActiveOnly();
        $UnitsModel->prepend('Select a UOM', '');
        $InventoryItems= array();
        return view('admin.stockitems.create', compact('Groups','Branches','InventoryItems','UnitsModel'));
    }

    public function inventoryproduct(StoreStockItemsRequest $request){
        $data = $request->all();
        $ProductsModel = ProductsModel::productByName($data['item']);
        //print_r($ProductsModel);exit;
        $result = array();

        if($ProductsModel->count()) {
            foreach ($ProductsModel as $key => $value) {

                $result[] = array(
                    'text' => $value->products_short_name,
                    'id' => $value->id,
                );
            }
        }
        return response()->json($result);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStockItemsRequest $request)
    {
        foreach($request['InventoryItems']['product_id'] as $key=>$value)
        {
            $productid = $request['InventoryItems']['product_id'][$key];
            $qunity = $request['InventoryItems']['qunity'][$key];
            $p_amount = $request['InventoryItems']['p_amount'][$key];
            $results = ProductsModel::where('id',$productid)->first();
            $products_name = $results->products_name;
            $suplier_id = $results->suplier_id;
            $cat_id = $results->cat_id;
            $Ledger = array(
                'name' => $products_name,
                'group_id' => $request['parent_id'],
                'balance_type' => 'd',
                'opening_balance' => $request['InventoryItems']['amount'][$key],
                'closing_balance' => $request['InventoryItems']['amount'][$key],
            );
            $response = CoreAccounts::createLedger($Ledger);
            $opening_stock = 0;
            $closing_stock = $qunity;
            $movment_stock = $opening_stock+$qunity - $closing_stock;
           // $stock_value = $closing_stock*$p_amount;
            $stock_item =  StockItemsModel::create([
                'branch_id'            => $request['branch_id'],
                'group_id'             => $request['parent_id'],
                'ledger_id'            => $response['id'],
                'sup_id'               => $suplier_id,
                'cat_id'               => $cat_id,
                'st_name'              => $productid,
                'quantity'             => $qunity,
                'opening_stock'        => $opening_stock,
                'purchase_stock'       => $qunity,
                'closing_stock'        => $closing_stock,
                'moments_stock'        => $movment_stock,
                'stock_value'          => $request['InventoryItems']['amount'][$key],
                'st_unit_price'        => $p_amount,
                'sale_unit'            => $request['InventoryItems']['unit_name'][$key],
                'created_by'           => Auth::user()->id,
            ]);
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.stockitems.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_stockitems_edit')) {
            return abort(401);
        }
        $StockItems = StockItemsModel::findOrFail($id);
        $Branches = Branches::pluckActiveOnly();
        $Suppliers = SuppliersModel::all();
        $StockCategory = StockCategoryModel::all();
        $results = ProductsModel::all();
        return view('admin.stockitems.edit', compact('StockItems', 'Suppliers', 'StockCategory','Branches','results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStockItemsRequest $request, $id)
    {
        //dd($request->all());
        if (! Gate::allows('erp_stockitems_edit')) {
            return abort(401);
        }
        $results = ProductsModel::where('id',$request['ledger_product'])->first();
        $products_name = $results->products_name;
        $opening_stock = 0;
        $closing_stock = $request['quantity'];
        $movment_stock = $opening_stock+$request['quantity']- $closing_stock;
        $stock_value = $closing_stock*$request['st_unit_price'];
        StockItemsModel::where('id',$id)->update([
            'quantity'            => $request['quantity'],
            'opening_stock'       => $opening_stock,
            'purchase_stock'      => $request['quantity'],
            'closing_stock'       => $closing_stock,
            'moments_stock'       => $movment_stock,
            'stock_value'         => $stock_value,
            'st_unit_price'       => $request['st_unit_price'],
            'updated_by'          => Auth::user()->id,
        ]);
        $Ledger = array(
            'name' => $products_name,
            'group_id' => $request['group_id'],
            'balance_type' => 'd',
            'opening_balance' => $stock_value,
            'closing_balance' => $stock_value,
        );
        $response = CoreAccounts::updateLedger($Ledger,$request['ledger_id']);

        return redirect()->route('admin.stockitems.index');
    }

    /**
     * Activate Stockitems from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_stockitems_active')) {
            return abort(401);
        }

        $StockItems = StockItemsModel::findOrFail($id);
        $StockItems->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.stockitems.index');
    }

    /**
     * Inactivate Stockitems from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_stockitems_inactive')) {
            return abort(401);
        }

        $StockItems = StockItemsModel::findOrFail($id);
        $StockItems->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.stockitems.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function delete(Request $request)
    {
        $data = $request->all();
        $StockItems = StockItemsModel::findOrFail($data['name']);
        //$Ledger->delete();
        $StockItems->delete();
        flash('Record has been deleted successfully.')->success()->important();
        return redirect()->route('admin.stockitems.index');
    }

    /**
     * Delete all selected Entry at once.
     *
     * @param Request $request
     */

    public function StockReport(){
        $SuppliersModel = StockItemsModel::brandByname();
        $SuppliersModel->prepend('Select a Brand', '');
        $StockCategoryModel = StockItemsModel::categoryByname();
        $StockCategoryModel->prepend('Select a Category', '');
        $ProductModel = ProductsModel::pluckProducts();
        $ProductModel->prepend('Select a Product', '');

    $StockItems = DB::select(' SELECT s.st_name ,sum(s.closing_stock) AS QTy,p.products_short_name,sum(s.closing_stock * s.st_unit_price) AS stockvalue FROM `stockitems` as s join products as p ON p.id = s.st_name GROUP BY s.st_name HAVING(QTy > 0)');


    // dd($test);
        return view('admin.stockitems.stockitems_report', compact('StockItems','StockCategoryModel','SuppliersModel', 'ProductModel'));
    }

    public function FilterReport(Request $request){

        $SuppliersModel = StockItemsModel::brandByname();
        $SuppliersModel->prepend('Select a Brand', '');
        $StockCategoryModel = StockItemsModel::categoryByname();
        $StockCategoryModel->prepend('Select a Category', '');
        $ProductModel = ProductsModel::pluckProducts();
        $ProductModel->prepend('Select a Product', '');
        
        $data = $request->all();

        if(!empty($data['sup_id']) AND !empty($data['cat_id']) AND !empty($data['product_id'])){

            $StockItems = DB::select(DB::Raw(" SELECT s.st_name ,sum(s.closing_stock) AS QTy,p.products_short_name,sum(s.closing_stock * s.st_unit_price) AS stockvalue FROM `stockitems` as s join products as p ON p.id = s.st_name WHERE suplier_id = '".$data["sup_id"]."' AND cat_id = '".$data["cat_id"]."' AND p.id = '".$data["product_id"]."' GROUP BY s.st_name HAVING(QTy > 0)"));

        }else if(!empty($data['sup_id'])){
            $StockItems = DB::select(DB::Raw(" SELECT s.st_name ,sum(s.closing_stock) AS QTy,p.products_short_name,sum(s.closing_stock * s.st_unit_price) AS stockvalue FROM `stockitems` as s join products as p ON p.id = s.st_name WHERE suplier_id = '".$data["sup_id"]."' GROUP BY s.st_name HAVING(QTy > 0)"));
            ///$StockItems = StockItemsModel::getproductByBrand($data['sup_id']);
        }else if(!empty($data['cat_id'])){
            $StockItems = DB::select(DB::Raw(" SELECT s.st_name ,sum(s.closing_stock) AS QTy,p.products_short_name,sum(s.closing_stock * s.st_unit_price) AS stockvalue FROM `stockitems` as s join products as p ON p.id = s.st_name WHERE cat_id = '".$data["cat_id"]."' GROUP BY s.st_name HAVING(QTy > 0)"));
        }else if(!empty($data['product_id'])){
            $StockItems = DB::select(DB::Raw(" SELECT s.st_name ,sum(s.closing_stock) AS QTy,p.products_short_name,sum(s.closing_stock * s.st_unit_price) AS stockvalue FROM `stockitems` as s join products as p ON p.id = s.st_name WHERE p.id = '".$data["product_id"]."' GROUP BY s.st_name HAVING(QTy > 0)"));
        }else{
            $StockItems = DB::select(' SELECT s.st_name ,sum(s.closing_stock) AS QTy,p.products_short_name,sum(s.closing_stock * s.st_unit_price) AS stockvalue FROM `stockitems` as s join products as p ON p.id = s.st_name GROUP BY s.st_name HAVING(QTy > 0)');
        }
        //dd($StockItems);
         return view('admin.stockitems.stockitems_report', compact('StockItems','StockCategoryModel','SuppliersModel', 'ProductModel'));
    }

}
