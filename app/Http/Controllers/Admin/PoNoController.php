<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\PoNoModel;
use App\Models\Admin\SuppliersModel;
use Illuminate\Support\Facades\Gate;

class PoNoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        if (! Gate::allows('pono_manage')) {
//            return abort(401);
//        }
        $Ponumber = PoNoModel::all();
        return view('admin.pono.index', compact('Ponumber'));
    }
    public function datatables()
    {

        //        if (! Gate::allows('pono_manage')) {
//            return abort(401);
//        }

        $Product = PoNoModel::all();

        return  datatables()->of($Product)
            ->addColumn('products_name', function($Product) {
                return  $Product->products_name .' -  '.$Product->products_model;
            })->addColumn('suplier_id', function($Product) {
                return $Product->suppliersModel->sup_name;
            }) ->addColumn('action', function ($Product) {
                $action_column = '<a href="pono/'.$Product->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

                if($Product->status && Gate::check('pono_inactive')){

                    $action_column .= '<form method="post" action="pono/inactive" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input type="hidden" value='.$Product->id.' name="name" >';
                    $action_column .= '<input   value="Inactivate"  class="btn btn-xs btn-warning" type="submit"  onclick="return confirm(\'Are you sure to inactivate this record? \')"> </form>';

                }else if (! $Product->status && Gate::check('salepipelines_active'))
                {
                    $action_column .= '<form method="post" action="pono/active" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input type="hidden" value='.$Product->id.' name="name" >';
                    $action_column .= '<input   value="Activate"  class="btn btn-xs btn-primary" type="submit"  onclick="return confirm(\'Are you sure you want to activate this record? \')"> </form>';
                }
                //if(Gate::check('pono_destroy')){
                    $action_column .= '<form method="post" action="pono/delete" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value="12" name="brand_id" >';
                    $action_column .= '<input type="hidden" value="21" name="po_no" >';
                    $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
                    $action_column .= '<input type="hidden" value='.$Product->id.' name="name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
                //}
                return $action_column;
            })->rawColumns(['products_name','suplier_id','action'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $SuppliersModel = SuppliersModel::all();
        return view('admin.pono.create', compact('SuppliersModel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PoNoModel::create([
            'po_no' => $request['po_no'],
            'po_date' => $request['po_date'],
            'brand_id' => $request['brand_id'],
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.pono.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Ponumber = PoNoModel::findOrFail($id);
        $SuppliersModel = SuppliersModel::all();
        return view('admin.pono.edit', compact('SuppliersModel','Ponumber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PoNoModel::where('id',$id)->update([
            'po_no' => $request['po_no'],
            'po_date' => $request['po_date'],
            'brand_id' => $request['brand_id'],
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.pono.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        if (! Gate::allows('erp_users_manage')) {
//            return abort(401);
//        }
        $PoNoModel = PoNoModel::findOrFail($id);
        $PoNoModel->delete();

        return redirect()->route('admin.pono.index');
    }
    public function delete(Request $request)
    {

        $data = $request->all();
        $PoNoModel = PoNoModel::findOrFail($data['name']);
        $PoNoModel->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.pono.index');
    }
}
