<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\Section;
use App\Models\Academics\Program;
use App\Models\Academics\AssignClassSection;
use App\Models\Academics\Classes;
use Auth;
use Config;
use App\Helpers\Helper;
use Redirect;


class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $section= Section::all();
         return view('admin.section.index',compact('section'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required | unique:sections',
            'description' => 'required',
        ]);
        $input= $request->all();
        for($i = 0; $i < count($input['name']) ; $i++) { 
            $arr= new Section;
            $arr->name=$input['name'][$i];
            $arr->description=$input['description'][$i];
            $arr->save();
        }
       

        return redirect()->route('admin.section.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section=Section::find($id);
        return view('admin.section.edit',compact('section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Section::where('id',$request->section_id)->update([
            'name'=>$request->name,
            'description'=>$request->description
        ]);

        return redirect()->route('admin.section.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Section::find($id)->delete();
        return redirect()->route('admin.section.index');
    }
    public function assignClassSection()
    {
        $classes= Classes::all();
        $boards= Board::all();
        $sections= Section::all();

        return view('admin.section.assign-class-section',compact('classes','sections','boards'));
    }
    public function assignClassSectionStore(Request $request)
    {
    
        
        $input=$request->all();
        $bpc_id= BoardProgramClass::where('board_id',$input['board_id'])->where('program_id',$input['program_id'])->where('class_id',$input['class_id'])->value('id');
        foreach ($input['section_id'] as $key => $value) {
            AssignClassSection::create([
            'board_program_class_id' =>$bpc_id,
            'section_id' => $value
            ]);  

        }
        return redirect()->route('admin.view-board-program-class-section');
    }
    public function viewBoardProgramClassSection()
    {
        $board_program_class = AssignClassSection::distinct('board_program_class_id')->pluck('board_program_class_id');   
        $class_sections =[];

        if($board_program_class)
        {
            foreach ($board_program_class as $key => $b_p_c) {
                $board = AssignClassSection::where('board_program_class_id',$b_p_c)->first()->getBoardProgramClass->getBoard;
                $program = AssignClassSection::where('board_program_class_id',$b_p_c)->first()->getBoardProgramClass->getProgram;
                $class = AssignClassSection::where('board_program_class_id',$b_p_c)->first()->getBoardProgramClass->getClass;
                $sections = AssignClassSection::where('board_program_class_id', $b_p_c)->pluck('section_id');
                $class_sections[$key]['board'] = $board;
                $class_sections[$key]['program'] = $program;
                $class_sections[$key]['class'] = $class;
                foreach ($sections as $k => $s) {
                   $sections[$k] = Section::find($s)->getName();
                }
                $class_sections[$key]['sections'] = $sections;
            }
        }

        return view('admin.section.class-section-index',compact('class_sections'));
    }
}
