<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CoreAccounts;
use App\Models\Admin\Ledgers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Orders;
use App\Models\Admin\OrderDetailModel;
use App\Models\Admin\StockItemsModel;
use App\Models\Admin\warehouse;
use Config;
use Auth;

class LocalPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $order = Orders::where('order_type', 1)->get();
       $warehouse = Warehouse::all();
        return view('admin.local_purchase.grimportcreate', compact('order', 'warehouse' ,'warehouse', 'Ledgers', 'Banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $counter = 1;
        $data = $request->all();
        $order = Orders::where('id', $data['order_no'])->first();
        $Cr_ledger =  Ledgers::where('parent_type',$order->sup_id)->where('group_id',Config::get('constants.accounts_cd_local'))->first();
        //dd($Cr_ledger->id);
        foreach($data['product_detail_id'] as $key=>$value)
        {
            StockItemsModel::create([
                'st_name'           =>$data['product_detail_id'][$key],
                'st_unit_price'     =>$data['unit_price'][$key],
                'purchase_stock'    =>$data['rel_qty'][$key],
                'closing_stock'     =>$data['rel_qty'][$key],
                'branch_id'     =>$data['warehouse_id'],
                'status'            => 1,
            ]);
            OrderDetailModel::where('id',$data['row_id'][$key])->update([
                'remaining_qty' => $data['bal_qty'][$key],
            ]);
            $dr_ledger =  Ledgers::where('parent_type',$data['product_detail_id'][$key])->where('group_id',Config::get('constants.admin_local_stock'))->first();
            $Ledger_entry[$counter]['ledger_id'] = $dr_ledger->id;
            $Ledger_entry[$counter]['dr_amount'] = round($data['rel_qty'][$key] * $data['unit_price'][$key], 4);
            $Ledger_entry[$counter]['cr_amount'] = 0;
            $Ledger_entry[$counter]['narration'] = 'Stock Transfer from Gr to inventory';
            $counter++;
        }
        if($Cr_ledger->id && $request['paid_amount_total'] !=''){
            $vencounter = count($Ledger_entry);
            $vencounter = $vencounter+1;
            $Ledger_entry[$vencounter]['ledger_id'] = $Cr_ledger->id;
            $Ledger_entry[$vencounter]['dr_amount'] = 0;
            $Ledger_entry[$vencounter]['cr_amount'] = $request['paid_amount_total'];
            $Ledger_entry[$vencounter]['narration'] = 'Stock Transfer from Gr to inventory';
            $entries2 = array(
                "number" => "??????",
                "voucher_date" => date('Y-m-d'),
                "employee_id" => Auth::user()->id,
                "narration" => 'Stock Transfer from Gr to inventory',
                "entry_type_id" => "1",
                "entry_items" => $Ledger_entry,
                "diff_total" => "0",
                "dr_total" => $request['paid_amount_total'],
                "cr_total" => $request['paid_amount_total'],
                "cheque_no" => null,
                "invoice_no" => null,
            );
            CoreAccounts::createJEntry($entries2);
        }

        flash('Record has been Insert successfully.')->success()->important();
        return redirect()->route('admin.stockitems.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function productList($order_id)
    {
        //print_r($order_id);exit;
    $order =  Orders::findOrFail($order_id)->first();
       // print_r($order);exit;
    $orderDetail = OrderDetailModel::where('perchas_id', $order_id)->get();
    return view('admin.local_purchase.productlist',compact('order', 'orderDetail'));

    }
}
