<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\Classes;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\Course;
use App\Models\Academics\Program;
use Auth;
use Config;
use App\Helpers\Helper;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course= Course::all();

        return view('admin.course.index',compact('course'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $programs = Program::all();
        return view('admin.course.create' , compact('programs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // dd($request);
        
        $input= $request->all();
       
        for($i = 0; $i < count($input['name']) ; $i++) { 
            $arr= new Course;
            $arr->name=$input['name'][$i];
            $arr->subject_code=$input['subject_code'][$i];
            $arr->credit_hours=$input['credit_hours'][$i];
             $arr->program_id=$input['program_id'];
            $arr->type=$input['type'][$i];
            $arr->actual_fee=$input['actual_fee'][$i];
            $arr->subject_fee=$input['subject_fee'][$i];
            $arr->subject_optional_code=$input['subject_optional_code'][$i];
            $arr->description=$input['description'][$i];
            $arr->save();
        }
         return redirect()->route('admin.courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $courses=Course::find($id);
        return view('admin.course.edit',compact('courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         Course::where('id',$request->course_id)->update([
            'name'=>$request->name,
            'description'=>$request->description,
            'subject_code'=>$request->subject_code,
            'credit_hours'=>$request->credit_hours,
            'type'=>$request->type,
            'actual_fee'=>$request->actual_fee,
            'subject_fee'=>$request->subject_fee,
            'subject_optional_code'=>$request->subject_optional_code,
        ]);

        return redirect()->route('admin.courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
