<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\ErpFeeHead;
use Illuminate\Http\Request;
use App\Models\Admin\FeeSection;
use App\Helpers\GroupsTree;
use App\Models\Admin\Groups;
use App\Models\Admin\Program;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Auth;
use Config;

use App\Helpers\CoreAccounts;

class FeeHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_fee_head_manage')) {
            return abort(401);
        }

        $feeHeads= ErpFeeHead::all();
        return view('admin.fee_head.index', compact('feeHeads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_fee_head_create')) {
            return abort(401);
        }
        $feeSection = FeeSection::all()->pluck('name','id');
        $program = Program::all()->pluck('name', 'id');
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('id', 'asc')->get()->toArray()), old('group_id'));
        return view('admin.fee_head.create',compact('feeSection', 'program', 'Groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_fee_head_create')) {
            return abort(401);
        }

        $rules['title']  =  'required|unique:country';
        $rules['fee_section_id'] = 'required';
        $rules['description'] = 'required';
        $rules['group_id'] = 'required';
        $rules['rec_group_id'] = 'required';
        $fee_head = ErpFeeHead::create([
            'title' => $request['title'],
            'fee_section_id'=> $request['fee_section_id'],
            'description'=>$request['description'],
            'rec_parent_group_id'=>$request['rec_group_id'],
            'inc_parent_group_id'=>$request['group_id'],
        ]);

        $data['name'] = $request['title'];
        $data['parent_type'] = $fee_head->id;
        $data['parent_id'] = $request['group_id'];
        $res = CoreAccounts::createGroup($data);

        $dataa['name'] = $request['title'];
        $dataa['parent_type'] = $fee_head->id;
        $dataa['parent_id'] = $request['rec_group_id'];
        $resp = CoreAccounts::createGroup($dataa);


        ErpFeeHead::where('id',$fee_head->id)->update([
            'rec_group_id' => $res['id'],
            'inc_group_id'=> $resp['id']
        ]);


        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.fee_head.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        if (! Gate::allows('erp_fee_head_edit')) {
//            return abort(401);
//        }
//
//        $feeHead= ErpFeeHead::findOrFail($id);
//        $feeSection = FeeSection::all()->pluck('name','id');
//        $program = Program::all()->pluck('name', 'id');
//        return view('admin.fee_head.edit',compact('feeSection','feeHead', 'program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        if (! Gate::allows('erp_fee_head_edit')) {
//            return abort(401);
//        }
//        ErpFeeHead::where('id',$id)->update([
//            'title' => $request['title'],
//            'fee_section_id'=> $request['fee_section_id'],
//            'description'=>$request['description'],
//        ]);
//        flash('Record has been updated successfully.')->success()->important();
//        return redirect()->route('admin.fee_head.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
