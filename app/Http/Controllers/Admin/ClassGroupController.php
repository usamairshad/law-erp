<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\ProgramGroup;
use App\Models\Academics\Section;
use App\Models\Academics\Program;
use App\Models\Academics\AssignClassSection;
use App\Models\Academics\Classes;
use App\Models\Academics\CourseGroup;
use App\Models\Academics\ClassGroup;
use Auth;
use Config;
use App\Helpers\Helper;

class ClassGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cg= ClassGroup::all();
        return view('admin.class-group.class-group-index',compact('cg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boards= Board::all();

        $classes= Classes::all();

        return view('admin.class-group.class-group-create',compact('boards','classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input=$request->all();

        // $bp= BoardProgram::where('board_id',$input['board_id'])->where('program_id',$input['program_id'])->first();
        // $pg= ProgramGroup::where('board_program_id',$bp->id)->first();
        // $board_name = $bp->board->name;
        // $program_name = $bp->program->name;
        // $name = $pg->name."-".$input['name'];
        $cg= CourseGroup::find($input['course_group_id'])->value('name');
        
        $name= $cg."-".$input['name'];
        ClassGroup::create([
                'course_group_id' =>$input['course_group_id'],
                'class_id' =>$input['class_id'],
                'name' => $name
        ]);

         return redirect()->route('admin.class-group.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loadCourseGroupClassGroup(Request $request)
    {
        $cg=ClassGroup::where('course_group_id',$request->course_group_id)->first();
        return $cg;
    }
}
