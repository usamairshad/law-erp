<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\DiscountType;
use Illuminate\Support\Facades\Gate;
use Auth;

class DiscountTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('erp_discount_type_manage')) {
            return abort(401);
        }
        $discount_type = DiscountType::OrderBy('created_at','desc')->get();

        return view('admin.discount_type.index', compact('discount_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_discount_type_create')) {
            return abort(401);
        }
         return view('admin.discount_type.create');
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_discount_type_create')) {
            return abort(401);
        }
        $discount_type = $request->all();
        $discount_type['created_by'] = Auth::user()->id;
        $type = DiscountType::create($discount_type);
       
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.discount-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_discount_type_edit')) {
            return abort(401);
        }
        $discount_type = DiscountType::find($id);
        return view('admin.discount_type.edit', compact('discount_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_discount_type_edit')) {
            return abort(401);
        }
        $discount_type = DiscountType::findOrFail($id);

        $data = $request->all();
        $data['updated_by'] = Auth::user()->id;

        $discount_type->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.discount-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_discount_type_destroy')) {
            return abort(401);
        }
        $discount_type = DiscountType::findOrFail($id);
        $discount_type->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.discount-type.index');
    }
}
