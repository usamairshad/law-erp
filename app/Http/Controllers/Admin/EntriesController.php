<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CoreAccounts;
use App\Helpers\GroupsTree;
use App\Models\Admin\Currencies;
use App\Models\Admin\EntryItems;
use App\Models\Admin\EntryTypes;
use App\Models\Admin\Settings;
use Illuminate\Support\Facades\Session;
use App\Models\Admin\Ledgers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Admin\Entries;

use App\Models\Admin\Students;
use App\Models\Admin\Branches;
use App\Models\HRM\Employees;
use App\Models\Admin\Departments;
use App\Models\Admin\LcBankModel;
use App\Models\Admin\PerformaStockModel;
use App\Models\Admin\DutyModel;
use App\Models\Academics\Staff;
use Carbon\Carbon;
use Auth;
use Validator;
use PDF;
use Config;

class EntriesController extends Controller
{
    /**
     * Display a listing of Entrie.
     *
     * @return \Illuminate\Http\Response
     */

     // testing function
    public function expense()
    {
        $current_year =  date("Y");
        $data[] = array();  
        $branches = Branches::all()->getDictionary();
      for($i=1; $i<=12; $i++)
{
    
    if($i<10)
    {
        
        $i = "0".$i;
    }
    $month = $current_year.'-'.$i;
   
        foreach($branches as $branch)
        {
            $ledgers = Ledgers::branchwise($month,$branch->id);
            $data[$i][$branch->name] =  Ledgers::branchwise($month,$branch->id);
        }     
}
  $myJSON = json_encode($data);
      echo $myJSON;
//echo '<pre>';print_r($data);echo'</pre>';exit;
    }

    public function index()
    {

        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $EntryTypes = EntryTypes::all()->getDictionary();
        $Entries = Entries::OrderBy('created_at','desc')->get();
        $DefaultCurrency = Currencies::getDefaultCurrencty();
 
        return view('admin.entries.index', compact('Entries', 'EntryTypes', 'DefaultCurrency'));
    }
    public function getData(Request $request){
        $EntryTypes = EntryTypes::all()->getDictionary();
        $Entries = Entries::OrderBy('created_at','desc')->get();
      //  echo '<pre>';print_r($Entries);echo'</pre>';exit;

        return  DataTables()->of($Entries)
            ->addColumn('voucher_date', function($Entries) {
                return  date('d-m-Y', strtotime($Entries->voucher_date));
            })
            ->addColumn('voucher_month', function($Entries) {
                return  date('m', strtotime($Entries->voucher_date));
            })
            ->addColumn('entry_type', function($Entries ) {
                $EntryTypes = EntryTypes::all()->getDictionary();
                $text = '<p>'.$EntryTypes[$Entries->entry_type_id]->code .'</p>';
                return $text;
            
            })->addColumn('action', function ($Entries) {
                $action_column='<p></p>';
                if($Entries->status == 1){
                    $action_column .= '<form method="post" action="entries/inactive/'.$Entries->id.'" >';
                    $action_column .= csrf_field();
                    $action_column .= '<input type="hidden" value=' . $Entries->id . ' name="name" >';
                    $action_column .= '<input   value="Inactivate"  class="btn btn-xs btn-warning" type="submit"  onclick="return confirm(\'Are you sure to inactivate this record? \')"> </form>';

                }else{
                    $action_column .= '<form method="post" action="entries/activate/'.$Entries->id.'" >';
                    $action_column .= csrf_field();
                    $action_column .= '<input type="hidden" value=' . $Entries->id . ' name="name" >';
                    $action_column .= '<input   value="Activate"  class="btn btn-xs btn-primary" type="submit"  onclick="return confirm(\'Are you sure you want to activate this record? \')"> </form>';

                }
                    /*$action_column .= '<form method="post" action="entries/destroy/'.$Entries['id'].'" >';
                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
                    $action_column .= '<input type="hidden" value='.$Entries['id'].' name="name" >';
                    $action_column .=  csrf_field();
                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';*/
                    $action_column .= '<a href="entries/'.$Entries->id.'/entry" class="btn btn-xs btn-info">View</a></br>';
                    $action_column .= '<a href="entries/'.$Entries->id.'/edit" class="btn btn-xs btn-info">Edit</a></br>';
                ;
                return $action_column;
            })->rawColumns(['voucher_date','voucher_month','entry_type','action'])
            ->toJson();
    }
    /**
     * Show Entry View from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function entry($id)
    {
     
        $Entrie = Entries::findOrFail($id);
        
        $EntryType = EntryTypes::findOrFail($Entrie->entry_type_id);
        
        $EntryTypes = EntryTypes::all();
        $Branch = Branches::find($Entrie->branch_id);
        // dd($Branch);
//        $Employee = Employees::find($Entrie->employee_id);

        $Employee = Staff::whereId($Entrie->employee_id)->first();
        $Department = Departments::find($Entrie->department_id);


        $DefaultCurrency = Currencies::getDefaultCurrencty();

        // Get Entry Items Associated with this Entry
        $Entry_items = EntryItems::where(['entry_id' => $Entrie->id])->OrderBy('id','asc')->get();
        $Ledgers = Ledgers::whereIn('id',
            EntryItems::where(['entry_id' => $Entrie->id])->pluck('ledger_id')->toArray()
        )->get()->getDictionary();
        switch ($Entrie['entry_type_id']) {
            case '1':
                // This is journal voucher
                return view('admin.entries.voucher.journal_voucher.entry', compact('Entrie','id', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '2':
                // This is Cash Receipt Voucher
                return view('admin.entries.voucher.cash_voucher.cash_receipt.entry', compact('Entrie','id', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '3':
                // This is Cash Payment Voucher
                return view('admin.entries.voucher.cash_voucher.cash_payment.entry', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '4':
                // This is Bank Receipt Voucher
                return view('admin.entries.voucher.bank_voucher.bank_receipt.entry', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '5':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.bank_voucher.bank_payment.entry', compact('Entrie', 'id', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '6':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.lc_voucher.lc_payment.entry', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '7':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.lc_receipt.lc_receipt.entry', compact('Entrie','id', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;

            default:
                return view('admin.entries.voucher.journal_voucher.entry', compact('Entrie', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
        }

    }

    /**
     * Show Entry from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Entrie = Entries::findOrFail($id);

        return view('admin.entries.view', compact('Entrie'));
    }

    /**
     * Show the form for creating new Entrie.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_entries_create')) {
            return abort(401);
        }
        return view('admin.entries.create');
    }

    public function downloadPDF($id){

//        if (! Gate::allows('invoice_pdf')) {
//            return abort(401);
//        }
//dd($id );
        $Entrie = Entries::findOrFail($id);

        $EntryType = EntryTypes::findOrFail($Entrie->entry_type_id);

        $EntryTypes = EntryTypes::all();
        $Branch = Branches::find($Entrie->branch_id);
//        $Employee = Employees::find($Entrie->employee_id);
        $Employee = Staff::whereId($Entrie->employee_id)->first();
        $Department = Departments::find($Entrie->department_id);


        $DefaultCurrency = Currencies::getDefaultCurrencty();

        //dd( $Suppliers);
        // Get Entry Items Associated with this Entry
        $Entry_items = EntryItems::where(['entry_id' => $Entrie->id])->OrderBy('id','asc')->get();
        $Ledgers = Ledgers::whereIn('id',
            EntryItems::where(['entry_id' => $Entrie->id])->pluck('ledger_id')->toArray()
        )->get()->getDictionary();

        switch ($Entrie['entry_type_id']) {
            case '1':
                // This is journal voucher
                $pdf = PDF::loadView('admin.entries.voucher.journal_voucher.newpdf_invoice',compact( 'Entrie','id','EntryType','EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '2':
                // This is Cash Receipt Voucher
                $pdf = PDF::loadView('admin.entries.voucher.cash_voucher.cash_receipt.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '3':
                // This is Cash Payment Voucher
                $pdf = PDF::loadView('admin.entries.voucher.cash_voucher.cash_payment.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '4':
                // This is Bank Receipt Voucher
                $pdf = PDF::loadView('admin.entries.voucher.bank_voucher.bank_receipt.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '5':
                // This is Bank Payment Voucher
                $pdf = PDF::loadView('admin.entries.voucher.bank_voucher.bank_payment.newpdf_invoice',compact( 'Entrie','id','EntryType','EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '6':
                // This is Bank Payment Voucher
                $pdf = PDF::loadView('admin.entries.voucher.lc_voucher.lc_payment.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '7':
                // This is Bank Payment Voucher
                $pdf = PDF::loadView('admin.entries.voucher.lc_receipt.lc_receipt.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;

            default:
                return view('admin.entries.voucher.journal_voucher.entry', compact('Entrie', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
        }
//        $digit = new \NumberFormatter( 'en_US', \NumberFormatter::SPELLOUT );
//
//
////        $pdf = PDF::loadView('admin.saleseinvoice.pdf_saleseinvoice',compact( 'SalesInvoiceMode','Products','Services','digit'));
//        $pdf = PDF::loadView('admin.entries.voucher.journal_voucher.newpdf_invoice',compact( 'Entrie','id','EntryType','EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
//        return $pdf->stream('invoice.pdf');

    }
    /**
     * Store a newly created Entrie in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreEntriesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEntriesRequest $request)
    {
        if (! Gate::allows('erp_entries_create')) {
            return abort(401);
        }

        Entries::create([
            'name' => $request['name'],
            'shortcode' => $request['shortcode'],
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'status' => 0,
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.entries.index');
    }


    /**
     * Show the form for editing Entrie.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_entries_edit')) {
            return abort(401);
        }


        $VoucherData = Entries::findOrFail($id)->toArray();
        $VoucherData['entry_items'] = array();
        $VoucherData['ledger_array'] = array();

        // Fetch Entry Items and insert into voucher Data Array
        $EntryItems = EntryItems::where(['entry_id' => $VoucherData['id']])->OrderBy('id','asc')->get()->toArray();

        if(count($EntryItems)) {
            $counter = 1;
            $ledger_ids = array();
            foreach($EntryItems as $EntryItem) {
                $ledger_ids[] = $EntryItem['ledger_id'];
                $VoucherData['entry_items']['counter'][$counter] = $counter;
                $VoucherData['entry_items']['ledger_id'][$counter] = $EntryItem['ledger_id'];
                $VoucherData['entry_items']['narration'][$counter] = $EntryItem['narration'];
                if($EntryItem['dc'] == 'd') {
                    $VoucherData['entry_items']['dr_amount'][$counter] = $EntryItem['amount'];
                    $VoucherData['entry_items']['cr_amount'][$counter] = 0;
                } else {
                    $VoucherData['entry_items']['dr_amount'][$counter] = 0;
                    $VoucherData['entry_items']['cr_amount'][$counter] = $EntryItem['amount'];
                }
                $counter++;
            }
            // Get Ledgers for Entries
            $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
        }
        //dd($VoucherData);

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
       $Departments = Departments::pluckActiveOnly();
       $Departments->prepend('Select a Department', '');

        switch ($VoucherData['suppliers_id']) {
            case '1':
                // This is journal voucher
                return view('admin.entries.voucher.journal_voucher.edit', compact('Employees', 'Branches', 'VoucherData'));
                break;
            case '2':
                // This is Cash Receipt Voucher
                return view('admin.entries.voucher.cash_voucher.cash_receipt.edit', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
                break;
            case '3':
                // This is Cash Payment Voucher
                return view('admin.entries.voucher.cash_voucher.cash_payment.edit', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
                break;
            case '4':
                // This is Cash Receipt Voucher
                return view('admin.entries.voucher.bank_voucher.bank_receipt.edit', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
                break;
            case '5':
                // This is Cash Payment Voucher
                return view('admin.entries.voucher.bank_voucher.bank_payment.edit', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
                break;
            case '6':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.lc_voucher.lc_payment.entry', compact('Entrie', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '7':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.lc_receipt.lc_receipt.entry', compact('Entrie', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            default:
                return view('admin.entries.voucher.journal_voucher.edit', compact('Employees', 'Branches', 'VoucherData'));
                break;
        }
    }

    /**
     * Update Entrie in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateEntrieRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_entries_edit')) {
            return abort(401);
        }

        $response = CoreAccounts::updateEntry($request->all(), $id);

        if($response['status']) {
            flash('Record has been updated successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }


    /**
     * Remove Entrie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_entries_destroy')) {
            return abort(401);
        }
        $Entrie = Entries::findOrFail($id);
        $Entrie->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.entries.index');
    }

    /**
     * Activate Entrie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_entries_active')) {
            return abort(401);
        }

        $Entrie = Entries::findOrFail($id);
        $Entrie->update(['status' => 1]);
        EntryItems::where(['entry_id' => $Entrie->id])->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.entries.index');
    }

    /**
     * Inactivate Entrie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_entries_inactive')) {
            return abort(401);
        }

        $Entrie = Entries::findOrFail($id);
        $Entrie->update(['status' => 0]);
        EntryItems::where(['entry_id' => $Entrie->id])->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.entries.index');
    }

    /**
     * Create Journal Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */
    public function gjv_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
                // dd( $VoucherData);
            } else {
                $VoucherData['ledger_array'] = array();
            }
        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'suppliers_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'narration' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
         $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');
        
        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
//        $Departments = Departments::pluckActiveOnly();
//        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.journal_voucher.create', compact('Employees', 'Branches', 'VoucherData'));
    }

    /**
     * Journal Voucher Items Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function gjv_search(Request $request)
    {
        $bID=0;
        
        if(isset($request['item']) && $request['item']) {
            $ledgers = "";
            if(isset($request['branch_id']) && $request['branch_id']>0){
                $bID=$request['branch_id'];
                $ledgers = Ledgers::where(['status' => 1])->where('branch_id', $bID)
                ->where('name','LIKE',"%{$request['item']}%")
                ->orwhere('number','LIKE',"%{$request['item']}%")
                ->OrderBy('name','asc')->get();
            }else{
                $ledgers = Ledgers::where(['status' => 1])->where('name','LIKE',"%{$request['item']}%")
                ->orwhere('number','LIKE',"%{$request['item']}%")
                ->OrderBy('name','asc')->get();
                $bID=Auth::user()->branch_id;
                
            }
       
           
                // dd($ledgers[0]['group_id']);
            $result = array();

            //if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->number.'-'.$ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $text_ledger. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            //}

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    public function teacher_search(Request $request)
    {
        $bID=0;
        if($request['type']=="Staff")
        {
        if(isset($request['item']) && $request['item']) {
            $ledgers = "";
            if(isset($request['branch_id']) && $request['branch_id']>0){
                $bID=$request['branch_id'];
                $ledgers = Staff::where(['status' => 1])->where('branch_id', $bID)
                ->where('first_name','LIKE',"%{$request['item']}%")
               // ->orwhere('number','LIKE',"%{$request['item']}%")
                ->OrderBy('first_name','asc')->get();
            }else{
                $ledgers = Staff::where(['status' => 1])->where('first_name','LIKE',"%{$request['item']}%")
               // ->orwhere('number','LIKE',"%{$request['item']}%")
                ->OrderBy('first_name','asc')->get();
                $bID=Auth::user()->branch_id;
                
            }
 
           
                // dd($ledgers[0]['group_id']);
            $result = array();

            //if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->number.'-'.$ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        'text' => $ledger->reg_no.' '. $ledger->first_name.' '. $ledger->last_name,
                        'id' =>  $ledger->reg_no.' '. $ledger->first_name.' '. $ledger->last_name,
                    );
                }
            //}

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }
    else
    if($request['type']=="Student")
    {
        $bID=0;
        if(isset($request['item']) && $request['item']) {
            $ledgers = "";
            if(isset($request['branch_id']) && $request['branch_id']>0){
                $bID=$request['branch_id'];
                $ledgers = Students::where(['status' => 1])->where('branch_id', $bID)
                ->where('first_name','LIKE',"%{$request['item']}%")
               // ->orwhere('number','LIKE',"%{$request['item']}%")
                ->OrderBy('first_name','asc')->get();
            }else{
                $ledgers = Students::where(['status' => 1])->where('first_name','LIKE',"%{$request['item']}%")
               // ->orwhere('number','LIKE',"%{$request['item']}%")
                ->OrderBy('first_name','asc')->get();
                $bID=Auth::user()->branch_id;
                
            }
       
           
                // dd($ledgers[0]['group_id']);
            $result = array();

            //if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->number.'-'.$ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $ledger->reg_no.' '. $ledger->first_name.' '. $ledger->last_name,
                        'id' =>  $ledger->reg_no.' '. $ledger->first_name.' '. $ledger->last_name,
                    );
                }
            //}

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }
    else
    {
        $bID=0;
        
        if(isset($request['item']) && $request['item']) {
            $ledgers = "";
            if(isset($request['branch_id']) && $request['branch_id']>0){
                $bID=$request['branch_id'];
                $ledgers = Ledgers::where(['status' => 1])->where('branch_id', $bID)
                ->where('name','LIKE',"%{$request['item']}%")
                ->orwhere('number','LIKE',"%{$request['item']}%")
                ->OrderBy('name','asc')->get();
            }else{
                $ledgers = Ledgers::where(['status' => 1])->where('name','LIKE',"%{$request['item']}%")
                ->orwhere('number','LIKE',"%{$request['item']}%")
                ->OrderBy('name','asc')->get();
                $bID=Auth::user()->branch_id;
                
            }
       
           
                // dd($ledgers[0]['group_id']);
            $result = array();

            //if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->number.'-'.$ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $text_ledger. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            //}

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }
    }

    public function student_search(Request $request)
    {
   
    }

    /**
     * Store a newly created Journal Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function gjv_store(Request $request)
    {
        $response = CoreAccounts::createEntry($request->all());

        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }


    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- Cash Vouchers Starts -----------------------------------
     * ----------------------------------------------------------------------------------------
    */

    /**
     * Create Cash Receipt Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */
    public function crv_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
            } else {
                $VoucherData['ledger_array'] = array();
            }
        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'cdr_no' => '',
                'cdr_date' => '',
                'bdr_no' => '',
                'bdr_date' => '',
                'bank_name' => '',
                'bank_branch' => '',
                'drawn_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'department_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'narration' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.cash_voucher.cash_receipt.create', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
    }

    /**
     * Store a newly created Cash Receipt Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function crv_store(Request $request)
    {
        $response = CoreAccounts::createEntry($request->all());

        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * All Items except Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function crv_search(Request $request)
    {
        
        if(isset($request['item']) && $request['item']) {
            //            $Setting = Settings::findOrFail(Config::get('constants.accounts_cash_banks_head_setting_id'));
            //            $parentGroups = new GroupsTree();
            //            $parentGroups->current_id = -1;
            //            $parentGroups->build($Setting->description);
            //            $parentGroups->toListArray($parentGroups, -1);
            
                        $ledgers = Ledgers::where(['status' => 1])
                            //->whereNotIn('group_id', $parentGroups->groupListIDs)
                            ->where(function ($query) {
                                global $request;
                                $query->where('name', 'LIKE', "%{$request['item']}%")
                                    ->orwhere('number', 'LIKE', "%{$request['item']}%");
                            })->OrderBy('name','asc')->get();
            
                        $result = array();
                        if($ledgers->count()) {
                            foreach ($ledgers as $ledger) {
                                $prefix = Ledgers::getAllParent($ledger->group_id);
                                if($prefix == '0'){
                                    $text_ledger = '('. $ledger->groups['name'] .')';
                                }else{
                                    $text_ledger = $prefix;
                                }
                                $result[] = array(
                                    //'text' => $ledger->number . ' - ' . $ledger->name,
                                    'text' => $text_ledger. ' - ' . $ledger->name,
                                    'id' => $ledger->id,
                                );
                            }
                        }
            
                        return response()->json($result);
                    } else {
                        return response()->json([]);
                    }
    }

    /**
     * Create Cash Payment Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */
    public function cpv_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
            } else {
                $VoucherData['ledger_array'] = array();
            }
        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'cdr_no' => '',
                'cdr_date' => '',
                'bdr_no' => '',
                'bdr_date' => '',
                'bank_name' => '',
                'bank_branch' => '',
                'drawn_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'department_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'narration' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.cash_voucher.cash_payment.create', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
    }

    /**
     * Store a newly created Cash Payment Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cpv_store(Request $request)
    {
        $response = CoreAccounts::createEntry($request->all());

        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * All Items except Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cpv_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
//            $Setting = Settings::findOrFail(Config::get('constants.accounts_cash_banks_head_setting_id'));
//            $parentGroups = new GroupsTree();
//            $parentGroups->current_id = -1;
//            $parentGroups->build($Setting->description);
//            $parentGroups->toListArray($parentGroups, -1);

            $ledgers = Ledgers::where(['status' => 1])
                //->whereNotIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $text_ledger. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- Cash Vouchers Ends -----------------------------------
     * ----------------------------------------------------------------------------------------
    */


    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- Banks Vouchers Starts -----------------------------------
     * ----------------------------------------------------------------------------------------
    */

    /**
     * Create Bank Receipt Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */
    public function brv_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
            } else {
                $VoucherData['ledger_array'] = array();
            }
        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'cdr_no' => '',
                'cdr_date' => '',
                'bdr_no' => '',
                'bdr_date' => '',
                'bank_name' => '',
                'bank_branch' => '',
                'drawn_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'department_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'narration' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.bank_voucher.bank_receipt.create', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
    }

    /**
     * Store a newly created Cash Receipt Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function brv_store(Request $request)
    {
        $response = CoreAccounts::createEntry($request->all());

        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * All Items except Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function brv_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
            $Setting = Settings::findOrFail(Config::get('constants.accounts_cash_banks_head_setting_id'));
            $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;
            $parentGroups->build($Setting->description);
            $parentGroups->toListArray($parentGroups, -1);

            $ledgers = Ledgers::where(['status' => 1])
                //->whereNotIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $text_ledger. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }
    /**
     * Create LC Payment Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */

    public function lcpv_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
              //echo '<pre>';print_r($VoucherData);echo '</pre>';exit;
            } else {
                $VoucherData['ledger_array'] = array();
            }

        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'cdr_no' => '',
                'cdr_date' => '',
                'bdr_no' => '',
                'bdr_date' => '',
                'bank_name' => '',
                'bank_branch' => '',
                'drawn_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'department_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'lc_duties' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');
        // Get All Duties
        $DutyModel = DutyModel::pluckActiveOnly();
        $DutyModel->prepend('Select a Duty', '');
        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.lc_voucher.lc_payment.create', compact('DutyModel','Employees', 'Branches', 'Departments', 'VoucherData'));
    }


    /**
     * Store a newly created LC Payment Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lcpv_store(Request $request)
    {
        $response = CoreAccounts::createLcEntry($request->all());
        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * All Items except LC Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lcpv_search(Request $request)
    {

        if(isset($request['item']) && $request['item']) {
            $ledgers = LcBankModel::selectRaw('lcbank.*,c_invoice.ci_no as invoice_no,c_invoice.id as cid')
                ->join('c_invoice','c_invoice.lcno','=','lcbank.id')
                ->where('lcbank.lc_no', 'LIKE', "%{$request['item']}%")
                ->get();
//
         // echo '<pre>';print_r($ledgers);echo'</pre>';exit;
//            $ledgers = LcBankModel::where(['status' => 1])
//                //->whereIn('group_id', $parentGroups->groupListIDs)
//                ->where(function ($query) {
//                    global $request;
//                    $query
//                        //->where('name', 'LIKE', "%{$request['item']}%")
//                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
//                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
//                    $prefix = Ledgers::getAllParent($ledger->group_id);
//                    if($prefix == '0'){
//                        $text_ledger = '('. $ledger->groups['name'] .')';
//                    }else{
//                        $text_ledger = $prefix;
//                    }
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $ledger->lc_no. ' - ' . $ledger->invoice_no,
                        'id' => $ledger->cid,
                    );
                }
            }
            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- LC Vouchers Ends -----------------------------------
     * ----------------------------------------------------------------------------------------
    */

    /**
     * Create LC receipit Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */

    public function lrp_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
                echo '<pre>';print_r($VoucherData);echo '</pre>';exit;
            } else {
                $VoucherData['ledger_array'] = array();
            }
        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'cdr_no' => '',
                'cdr_date' => '',
                'bdr_no' => '',
                'bdr_date' => '',
                'bank_name' => '',
                'bank_branch' => '',
                'drawn_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'department_id' => '',
                'lc_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'narration' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');
        $LcBankModel = LcBankModel::where('lc_status' , '3')->get();
        $LcBankModelOptions = array();
        foreach($LcBankModel as $agreement){
            if($agreement->transaction_type =='1'){
                    $tra_type ="LC";
                }else{
                    $tra_type ="TT";
                }
            $LcBankModelOptions[$agreement->id] = $tra_type." - ".$agreement->lc_no.' -  $.'.$agreement->lc_amt;
        }

        array_unshift($LcBankModelOptions, "Select a Supplier");
        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.lc_voucher.lc_receipt.create', compact('Employees', 'Branches', 'Departments', 'VoucherData' ,'LcBankModelOptions'));
    }


    /**
     * Store a newly created LC Payment Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lrp_store(Request $request)
    {
        $response = CoreAccounts::createLcInventory($request->all());
        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * All Items except LC Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lrp_search($id)
    {
        $ledger = Ledgers::where('id', $id)->first();
        $lc_name = $ledger->name;
        $lc_name1 =  explode("-", $lc_name);
        $lc_name1 = trim($lc_name1[1]);
        $LcBankModel = LcBankModel::where('lc_no',$lc_name1)->first();
        $lc_id = $LcBankModel->id;
        $results = PerformaStockModel::where('lc_no',$lc_id)->get();
        return view('admin.entries.voucher.lc_voucher.lc_receipt.productlist', compact('results'));
    }

    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- LC receipt Vouchers Ends -----------------------------------
     * ----------------------------------------------------------------------------------------
    */

    /**
     * Create Cash Payment Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */
    public function bpv_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
            } else {
                $VoucherData['ledger_array'] = array();
            }
        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'cdr_no' => '',
                'cdr_date' => '',
                'bdr_no' => '',
                'bdr_date' => '',
                'bank_name' => '',
                'bank_branch' => '',
                'drawn_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'department_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'narration' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.bank_voucher.bank_payment.create', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
    }

    /**
     * Store a newly created Cash Payment Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function bpv_store(Request $request)
    {
        $response = CoreAccounts::createEntry($request->all());

        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        }
        else {
            $request->flash();
                return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * All Items except Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function bpv_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
//            $Setting = Settings::findOrFail(Config::get('constants.accounts_cash_banks_head_setting_id'));
//            $parentGroups = new GroupsTree();
//            $parentGroups->current_id = -1;
//            $parentGroups->build($Setting->description);
//            $parentGroups->toListArray($parentGroups, -1);

            $ledgers = Ledgers::where(['status' => 1])
                //->whereNotIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        'text' => $text_ledger. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- Cash Vouchers Ends -----------------------------------
     * ----------------------------------------------------------------------------------------
    */

    /**
     * Cash Ledgers Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cash_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
            $Group = CoreAccounts::getConfigGroup(Config::get('constants.account_cash_inHand'))['group'];
            $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;
            $parentGroups->build($Group->id);
            $parentGroups->toListArray($parentGroups, -1);
            $ledgers = Ledgers::where(['status' => 1])
                ->whereIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            //dd($ledgers);
            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $result[] = array(
                        'text' => $ledger->number . ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /**
     * Banks Ledger Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function bank_search(Request $request)
    {

        if(isset($request['item']) && $request['item']) {
          $Group = CoreAccounts::getConfigGroup(Config::get('constants.account_bank_balance'))['group'];

              $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;
            $parentGroups->build($Group->id);
            $parentGroups->toListArray($parentGroups, -1);

            $ledgers = Ledgers::where(['status' => 1])
                ->whereIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix_group = Ledgers::getParent($ledger->group_id);
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $prefix_group. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            } 

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /**
     * Cash & Banks Ledger Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cashbank_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
            $Group = CoreAccounts::getConfigGroup(Config::get('constants.assets_current_cash_balance'))['group'];
            $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;
            $parentGroups->build($Group->id);
            $parentGroups->toListArray($parentGroups, -1);

            $ledgers = Ledgers::where(['status' => 1])
                ->whereIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $result[] = array(
                        'text' => $ledger->number . ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }
}
