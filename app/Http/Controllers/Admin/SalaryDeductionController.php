<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\SalaryDeduction;

class SalaryDeductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salarydeductions = SalaryDeduction::all();
        return view('admin.salary_deduction.index',compact('salarydeductions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.salary_deduction.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'deduction' => 'required',
            'fixed' => '',
            'percent' => '',
            'type'=> '',
        ]);
        $inputs = $request->input();
        //dd($inputs);
        SalaryDeduction::create($inputs);
        flash('Record has been Created successfully.')->success()->important();
        return redirect('/salary-deduction');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $SalaryDeduction = SalaryDeduction::findOrFail($id);
        //dd($SalaryDeduction);
        return view('admin.salary_deduction.edit', compact('SalaryDeduction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        SalaryDeduction::where('id',$id)->update([
            'deduction' => $request['deduction'],
            'amount' => $request['amount'],
            'type' => $request['type'],
        ]);
        flash('Record has been updated successfully.')->success()->important();
        return redirect('/salary-deduction');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
