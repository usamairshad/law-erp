<?php


namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TaxSettings\StoreUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Loan;
use Auth;

class LoanTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asset_types = Loan::OrderBy('created_at','desc')->get();
        return view('loantype.index', compact('asset_types')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('loantype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        
        Loan::create([
            'loan_type'           => $request['loan_type'],
            'description'    => $request['description']
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('loan-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset_types = Loan::where('loan_id',$id)->first();
        return view('loantype.edit', compact('asset_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
        {
   $id = $request['id'];

        $Branche = Loan::where('loan_id',$id);
        $Branche->update([
            'loan_type'           => $request['loan_type'],
            'description'    => $request['description'],   
        ]);
       

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('loan-list');
    }

    public function destroy($id)
    {
       
        $Branche = Loan::where('loan_id',$id);
        
        $Branche->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('loan-list');
    }
}
