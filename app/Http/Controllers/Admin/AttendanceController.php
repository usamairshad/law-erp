<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\BanksModel;
use App\Models\Academics\Staff;
use App\Models\Academics\Student;
use App\Models\Admin\Groups;
use App\Models\Admin\Device;
use App\Models\Admin\Holiday;
use App\Models\AttendanceLogs;
use App\Models\Attendance;
use App\Models\Admin\Branches;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\UpdateBanksRequest;
use App\Helpers\zklib\ZKLib;
use App\Helpers\CoreAccounts;
use Illuminate\Support\Facades\Validator;
use Auth;
use Config;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Branches = Branches::get();
        $data = 0;
        return view('attendance.index',compact('Branches'));
    }
    public function getIpAddress(Request $request)
    {
        $branch_id=$request->branch_id;
        $get_students = Device::where('branch_id', $branch_id)->get();
        return $get_students;

    }
    public function fetch_data(Request $request)
    {
        $Branches = Branches::get();
       // dd($request->all());
        $zk = new ZKLib($request->ip_address, 4370);
        $ret = $zk->connect();
        if($ret)
        {
            $attendance = $zk->getAttendance();
            sleep(1);
           // dd($attendance);
            foreach($attendance as $attendancedata)
            {
                if ( $attendancedata[2] == 14 )
                $status = 'Check Out';
            else
                $status = 'Check In';
                
                $exist = AttendanceLogs::where('date',date( "Y-m-d", strtotime( $attendancedata[3] ) ))
                ->where('time',date( "H:i:s", strtotime( $attendancedata[3] ) ))
                ->where('user_id' ,$attendancedata[1])->get();
                $i = 0;
            foreach($exist as $ext)
            {
                $i++;
            }
            if($i==0)
            {
                AttendanceLogs::create([
                    'branch_id' =>$request->branch_id,
                    'date' => date( "Y-m-d", strtotime( $attendancedata[3] ) ),
                    'time' =>date( "H:i:s", strtotime( $attendancedata[3] ) ),
                    'status' =>$status,
                    'user_id' =>$attendancedata[1]
                ]);
            }
            $DailyExist = Attendance::where('user_id' ,$attendancedata[1])->where('date',date( "Y-m-d", strtotime( $attendancedata[3] ) ))->get();
            $j = 0;
            foreach($DailyExist as $daily)
            {
                $j++;
                if($daily->time_in<date( "H:i:s", strtotime( $attendancedata[3] ) ))
                {
                    Attendance::where('daily_attendance_id',$daily->daily_attendance_id)->update([
                        'time_out' => date( "H:i:s", strtotime( $attendancedata[3] ) )
                    ]);
                }
                if($daily->time_in>date( "H:i:s", strtotime( $attendancedata[3] ) ))
                {
                    Attendance::where('daily_attendance_id',$daily->daily_attendance_id)->update([
                        'time_in' => date( "H:i:s", strtotime( $attendancedata[3] ) ),
                        'time_out' => $daily->time_in,
                    ]);
                }
                
            }
            if($j==0)
            {
                Attendance::create([
                    'branch_id' =>$request->branch_id,
                    'date' => date( "Y-m-d", strtotime( $attendancedata[3] ) ),
                    'time_in' =>date( "H:i:s", strtotime( $attendancedata[3] ) ),
                    'time_out' =>date( "H:i:s", strtotime( $attendancedata[3] ) ),
                    'status' =>'Present',
                    'remarks' =>'Marked By Machine',
                    'user_id' =>$attendancedata[1]
                ]);
            }
            
            }
        }
        flash('Scyncronize all Data From Selected Device.')->success()->important();
        return view('attendance.index',compact('Branches'));

    }
    public function monthly_report()
    {
        $Branches = Branches::get();
        $data = array();
        return view('attendance.report',compact('Branches','data'));
    }
    public function getAttendanceUsers(Request $request)
    {
        if($request->type=="Student")
        {
            $users  = Students::where('branch_id',$request->branch_id)->get();
        }
        else
        {
            $users  = Staff::where('branch_id',$request->branch_id)->get();
        }
        return $users;
    }
    public function get_monthly_report(Request $request)
    {
      
        $getAttendance = Attendance::where('user_id','=', $request->user)->whereBetween('date', [$request->from, $request->To])->get();
	$date = $request->from;
	$end_date =$request->To;
    $getHolidays = Holiday::whereBetween('holiday_date', [$request->from, $request->To])->get();
    $data = array();
	while (strtotime($date) <= strtotime($end_date)) {
        $check_absent = 0;
        foreach($getAttendance as $attendance)
        {
            if($attendance->date == $date)
            {
                $check_absent++;
                $data[] = array('date' => $attendance->date, 'time_in' => $attendance->time_in,'time_out'=>$attendance->time_out,'status'=>$attendance->status,'remarks'=>$attendance->remarks);
            }
        }
    
         $check_sunday =  date('N', strtotime($date)) >= 7;
         if($check_sunday==1)
         {
            $check_absent++;
             $data[] = array('date' => $date, 'time_in' => "--:--",'time_out'=>'--:--','status'=>'Sunday','remarks'=>'Holiday');
         }
         foreach($getHolidays as $holiday)
         {
             if($holiday->holiday_date ==$date)
             {
            $check_absent++;
            $data[] = array('date' => $date, 'time_in' => "--:--",'time_out'=>'--:--','status'=>$holiday->holiday_name,'remarks'=>'Holiday');
             }
        }
         if(  $check_absent==0)
         {
            $data[] = array('date' => $date, 'time_in' => "00:00",'time_out'=>'00:00','status'=>'Absent','remarks'=>'Absent');
         }
               // echo "$date\n";

                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	}
        
      
        $Branches = Branches::get();

        return view('attendance.report',compact('Branches','data'));
   
    }
    public function loadMachines()
    {
        $machine = Device::get();
        return $machine;
    }  

}