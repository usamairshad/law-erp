<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\Classes;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\Course;
use App\Models\Academics\Program;
use App\Models\Academics\Lab;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\AssignActiveSessionCourseLab;
use Auth;
use Config;
use App\Helpers\Helper;

class LabsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labs= Lab::all();

        return view('admin.lab.index',compact('labs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lab.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input= $request->all();
        for($i = 0; $i < count($input['name']) ; $i++) { 
            $arr= new Lab;
            $arr->name=$input['name'][$i];
            $arr->description=$input['description'][$i];
            $arr->save();
        }
         return redirect()->route('admin.labs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $labs=Lab::find($id);
        return view('admin.lab.edit',compact('labs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         Lab::where('id',$request->lab_id)->update([
            'name'=>$request->name,
            'description'=>$request->description
        ]);

        return redirect()->route('admin.labs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignCourseLab()
    {
        $courses= Course::all();
        $boards= Board::all();
        $labs= Lab::all();

        return view('admin.lab.assign-course-lab',compact('courses','labs','boards'));
    }

    public function assignCourseLabStore(Request $request)
    {   

        // $active_session_id= ActiveSession::where('board_id',$request->board_id)->where('program_id',$request->program_id)->where('class_id',$request->class_id)->where('branch_id',Auth::user()->branch_id)->value('id');
        $active_session_id= ActiveSession::where('board_id',$request->board_id)->where('program_id',$request->program_id)->where('class_id',$request->class_id)->value('id');
        AssignActiveSessionCourseLab::create([
            'active_session_id'=>$active_session_id,
            'course_id'=>$request->course_id,
            'lab_id'=>$request->lab_id,
            'description'=>$request->description
        ]);

        return redirect()->route('admin.assign-course-lab-view');
        
    }


    public function assignCourseLabView()
    {
        $data=AssignActiveSessionCourseLab::all();
        return view('admin.lab.assign-course-lab-index',compact('data'));

    }
}
