<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Loan;
use App\Models\Admin\IncrementPf;
use App\Models\Academics\Staff;
use App\User;
use App\Models\Role;

class IncrementPfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Loans = IncrementPf::with('role','user','staff')->get();
        //dd($Loans);
        return view('admin.incrementPF.index',compact('Loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::whereNotIn('name',['student','super-admin','admin','jr.coordinator'])->pluck('name','id');
        //dd($roles);
        $staff = Staff::all()->pluck('first_name','id');
        return view('admin.incrementPF.create', compact('staff','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'reason' => 'required',
            'role_id' => 'required',
            'user_id'=> 'required',
            'basic' => 'required',
            'pf_previous'=> 'required',
            'pf_new'=> 'required',
            'New_salary_increment'=> 'required',
            'Amount'=> 'required',
            'increment_date'=>'required'
        ]);
        $inputs = $request->input();
        //dd($inputs);
        IncrementPf::create($inputs);
        flash('Record has been Created successfully.')->success()->important();
        return redirect('/incrementPf');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function RoleRelatedUser(Request $request){

        $data = Staff::where('staff_type',$request->role_id)->get();
        //dd($data);
        return response()->json($data); 
    }
    public function staff(Request $request){
        $data = Staff::where('id',$request->staff_id)->get();
        return response()->json($data);
    }
}
