<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\ChildBenefitStudent;
use App\Models\Academics\Staff;
use App\Models\Academics\Student;
use App\Models\Admin\ChildBenefitConfig;
class ChildBenefitStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ChildBenefitStudents = ChildBenefitStudent::with('staff','student','childbenefitconf')->get();
        //dd($ChildBenefitStudents);
        return view('admin.child-benefit-student.index',compact('ChildBenefitStudents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all()->pluck('first_name','id');

        $staff = Staff::all()->pluck('first_name','id');

        $childbenefit = ChildBenefitConfig::all()->pluck('child_name','id');
        return view('admin.child-benefit-student.create', compact('students','staff','childbenefit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'student_id' => 'required',
            'staff_id' => 'required',
            'child_benefit_config_id' => 'required',
        ]);
        $inputs = $request->input();
        //dd($inputs);
        ChildBenefitStudent::create($inputs);
        flash('Record has been Created successfully.')->success()->important();
        return redirect('/child/benefit-student');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ChildBenefitStudent = ChildBenefitStudent::findOrFail($id);
        //dd($ChildBenefitConfig);
        $students = Student::all()->pluck('first_name','id');

        $staff = Staff::all()->pluck('first_name','id');

        $childbenefit = ChildBenefitConfig::all()->pluck('child_name','id');
        return view('admin.child-benefit-student.edit', compact('ChildBenefitStudent','staff','students','childbenefit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request['child_benefit_config_id']);
        ChildBenefitStudent::where('id',$id)->update([
            'student_id' => $request['student_id'],
            'staff_id' => $request['staff_id'],
            'child_benefit_config_id' => $request['child_benefit_config_id'],
        ]);
        flash('Record has been updated successfully.')->success()->important();
        return redirect('/child/benefit-student');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
