<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Loan;
use App\Models\Academics\Staff;
use App\Models\Admin\Eobi;
use App\User;
use App\Models\Role;
use App\Models\Admin\Company;
use App\Models\Admin\Branches;

class EobiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$Loans = Loan::with('role','user')->get();
        $Loans = Eobi::with('role','user')->get();
        return view('admin.eobi.index',compact('Loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::whereNotIn('name',['student','super-admin','admin','jr.coordinator'])->pluck('name','id');
        //dd($roles);
        $staff = Staff::all()->pluck('first_name','id');
        return view('admin.eobi.create', compact('staff','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cnic' => 'required',
            'role_id' => 'required',
            //'user_id'=> 'required',
            'date_of_reg' => 'required',
            'year'=> 'required',
            'contribution'=> 'required',
            'deduction'=> 'required',
            'balance'=> 'required'
        ]);
        $inputs = $request->input();
        //dd($inputs);
        Eobi::create($inputs);
        flash('Record has been Created successfully.')->success()->important();
        return redirect('/eobi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function RoleRelatedUser(Request $request){
        $data = User::where('role_id',$request->role_id)->get();
        //dd($data);
        return response()->json($data); 
    }

    public function report_eobi(){

        $companies = Company::pluck('name','id');
        $branches = Branches::pluck('name','id');
        $staff = Staff::all()->pluck('first_name','id');
        $roles = Role::pluck('name','id');   
        return view('admin.eobi.eobi_report',compact('companies','branches','staff','roles'));
    }
    public function eobi_company(Request $request){
        $input = $request->comp_id;
        $data = Branches::where('company_id','=',$input)->get();
        return response()->json($data);
    }
    public function eobi_branch(Request $request){
        $input = $request->branch_id;
        $data = Staff::where('branch_id','=',$input)->get();
        return response()->json($data);
    }
    public function eobi_staff(Request $request){
        $input = $request->staff_id;
        $data = Eobi::join('staff','staff.id','=','eobis.user_id')
        ->join('erp_branches','staff.branch_id','=','erp_branches.id')
        ->where('staff.id','=',$input)->get();
        return response()->json($data);
    }
}
