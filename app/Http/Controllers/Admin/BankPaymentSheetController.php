<?php

namespace App\Http\Controllers\Admin;
use App\Models\LandManagement\PayeeList;
use App\Helpers\CoreAccounts;
use App\Helpers\GroupsTree;
use App\Models\Admin\Currencies;
use App\Models\Admin\EntryItems;
use App\Models\Admin\EntryTypes;
use App\Models\Admin\Settings;
use Illuminate\Support\Facades\Session;
use App\Models\Admin\Ledgers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Admin\Entries;
use App\Models\Admin\Company;
use App\Models\Admin\City;

use App\Models\Admin\Students;
use App\Models\Admin\Branches;
use App\Models\HRM\Employees;
use App\Models\Admin\Departments;
use App\Models\Admin\LcBankModel;
use App\Models\Admin\PerformaStockModel;
use App\Models\Admin\DutyModel;
use App\Models\PaymentSheet;
use App\Models\PaymentSheetDetails;
use App\Models\Laws;
use App\Models\LawsDetails;
use App\Models\Vendor;
use App\Models\Academics\Staff;
use Carbon\Carbon;
use Auth;
use Validator;
use PDF;
use Config;

class BankPaymentSheetController extends Controller
{
    /**
     * Display a listing of Entrie.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
      //  $asset_types = Vendor::OrderBy('vendor_name','desc')->get();

      if(Auth::user()->hasRole('GM Finance'))
      {
        $Entries = PaymentSheet::where('approved_by_accountant', '!=', "")
        ->where('approval_by_dgm','!=',"")
        ->OrderBy('sheet_id','desc')->get();
      }
      else
      if(Auth::user()->hasRole('Deputy GM Finance'))
      {
        $Entries = PaymentSheet::where('approved_by_accountant', '!=', "")
        ->OrderBy('sheet_id','desc')->get();
      }
      else
      {
        $Entries = PaymentSheet::OrderBy('sheet_id','desc')->get();
      }
       

        return view('bankpayment.index',compact('Entries'));
    }

    public function sheet_details($id)
    {
     
        $Entries = PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
        ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
        ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
        ->where('erp_payment_sheet.sheet_id','=',$id)
        ->get();
        
        return view('bankpayment.show',compact('Entries','id'));
    }
    public function rent_sheet_details($id)
    {
     
        $Entries = PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
        ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
        ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
        ->where('erp_payment_sheet.sheet_id','=',$id)
        ->get();
        
        return view('rentpayment.view',compact('Entries','id'));
    }



    /**
     * Show Entry View from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function entry($id)
    {
     
        $Entrie = Entries::findOrFail($id);
        
        $EntryType = EntryTypes::findOrFail($Entrie->entry_type_id);
        
        $EntryTypes = EntryTypes::all();
        $Branch = Branches::find($Entrie->branch_id);
        // dd($Branch);
//        $Employee = Employees::find($Entrie->employee_id);

        $Employee = Staff::whereId($Entrie->employee_id)->first();
        $Department = Departments::find($Entrie->department_id);


        $DefaultCurrency = Currencies::getDefaultCurrencty();

        // Get Entry Items Associated with this Entry
        $Entry_items = EntryItems::where(['entry_id' => $Entrie->id])->OrderBy('id','asc')->get();
        $Ledgers = Ledgers::whereIn('id',
            EntryItems::where(['entry_id' => $Entrie->id])->pluck('ledger_id')->toArray()
        )->get()->getDictionary();
        switch ($Entrie['entry_type_id']) {
            case '1':
                // This is journal voucher
                return view('admin.entries.voucher.journal_voucher.entry', compact('Entrie','id', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '2':
                // This is Cash Receipt Voucher
                return view('admin.entries.voucher.cash_voucher.cash_receipt.entry', compact('Entrie','id', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '3':
                // This is Cash Payment Voucher
                return view('admin.entries.voucher.cash_voucher.cash_payment.entry', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '4':
                // This is Bank Receipt Voucher
                return view('admin.entries.voucher.bank_voucher.bank_receipt.entry', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '5':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.bank_voucher.bank_payment.entry', compact('Entrie', 'id', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '6':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.lc_voucher.lc_payment.entry', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '7':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.lc_receipt.lc_receipt.entry', compact('Entrie','id', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;

            default:
                return view('admin.entries.voucher.journal_voucher.entry', compact('Entrie', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
        }

    }

    /**
     * Show Entry from storage.
     *
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Show the form for creating new Entrie.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_entries_create')) {
            return abort(401);
        }
        return view('admin.entries.create');
    }

    public function downloadPDF($id){

//        if (! Gate::allows('invoice_pdf')) {
//            return abort(401);
//        }
//dd($id );
        $Entrie = Entries::findOrFail($id);

        $EntryType = EntryTypes::findOrFail($Entrie->entry_type_id);

        $EntryTypes = EntryTypes::all();
        $Branch = Branches::find($Entrie->branch_id);
//        $Employee = Employees::find($Entrie->employee_id);
        $Employee = Staff::whereId($Entrie->employee_id)->first();
        $Department = Departments::find($Entrie->department_id);


        $DefaultCurrency = Currencies::getDefaultCurrencty();

        //dd( $Suppliers);
        // Get Entry Items Associated with this Entry
        $Entry_items = EntryItems::where(['entry_id' => $Entrie->id])->OrderBy('id','asc')->get();
        $Ledgers = Ledgers::whereIn('id',
            EntryItems::where(['entry_id' => $Entrie->id])->pluck('ledger_id')->toArray()
        )->get()->getDictionary();

        switch ($Entrie['entry_type_id']) {
            case '1':
                // This is journal voucher
                $pdf = PDF::loadView('admin.entries.voucher.journal_voucher.newpdf_invoice',compact( 'Entrie','id','EntryType','EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '2':
                // This is Cash Receipt Voucher
                $pdf = PDF::loadView('admin.entries.voucher.cash_voucher.cash_receipt.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '3':
                // This is Cash Payment Voucher
                $pdf = PDF::loadView('admin.entries.voucher.cash_voucher.cash_payment.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '4':
                // This is Bank Receipt Voucher
                $pdf = PDF::loadView('admin.entries.voucher.bank_voucher.bank_receipt.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '5':
                // This is Bank Payment Voucher
                $pdf = PDF::loadView('admin.entries.voucher.bank_voucher.bank_payment.newpdf_invoice',compact( 'Entrie','id','EntryType','EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '6':
                // This is Bank Payment Voucher
                $pdf = PDF::loadView('admin.entries.voucher.lc_voucher.lc_payment.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;
            case '7':
                // This is Bank Payment Voucher
                $pdf = PDF::loadView('admin.entries.voucher.lc_receipt.lc_receipt.newpdf_invoice', compact('Entrie', 'id','EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                return $pdf->stream('invoice.pdf');
                break;

            default:
                return view('admin.entries.voucher.journal_voucher.entry', compact('Entrie', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
        }
//        $digit = new \NumberFormatter( 'en_US', \NumberFormatter::SPELLOUT );
//
//
////        $pdf = PDF::loadView('admin.saleseinvoice.pdf_saleseinvoice',compact( 'SalesInvoiceMode','Products','Services','digit'));
//        $pdf = PDF::loadView('admin.entries.voucher.journal_voucher.newpdf_invoice',compact( 'Entrie','id','EntryType','EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
//        return $pdf->stream('invoice.pdf');

    }
    /**
     * Store a newly created Entrie in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreEntriesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEntriesRequest $request)
    {
        if (! Gate::allows('erp_entries_create')) {
            return abort(401);
        }

        Entries::create([
            'name' => $request['name'],
            'shortcode' => $request['shortcode'],
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'status' => 0,
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.entries.index');
    }


    /**
     * Show the form for editing Entrie.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_entries_edit')) {
            return abort(401);
        }


        $VoucherData = Entries::findOrFail($id)->toArray();
        $VoucherData['entry_items'] = array();
        $VoucherData['ledger_array'] = array();

        // Fetch Entry Items and insert into voucher Data Array
        $EntryItems = EntryItems::where(['entry_id' => $VoucherData['id']])->OrderBy('id','asc')->get()->toArray();

        if(count($EntryItems)) {
            $counter = 1;
            $ledger_ids = array();
            foreach($EntryItems as $EntryItem) {
                $ledger_ids[] = $EntryItem['ledger_id'];
                $VoucherData['entry_items']['counter'][$counter] = $counter;
                $VoucherData['entry_items']['ledger_id'][$counter] = $EntryItem['ledger_id'];
                $VoucherData['entry_items']['narration'][$counter] = $EntryItem['narration'];
                if($EntryItem['dc'] == 'd') {
                    $VoucherData['entry_items']['dr_amount'][$counter] = $EntryItem['amount'];
                    $VoucherData['entry_items']['cr_amount'][$counter] = 0;
                } else {
                    $VoucherData['entry_items']['dr_amount'][$counter] = 0;
                    $VoucherData['entry_items']['cr_amount'][$counter] = $EntryItem['amount'];
                }
                $counter++;
            }
            // Get Ledgers for Entries
            $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
        }
        //dd($VoucherData);

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
       $Departments = Departments::pluckActiveOnly();
       $Departments->prepend('Select a Department', '');

        switch ($VoucherData['suppliers_id']) {
            case '1':
                // This is journal voucher
                return view('admin.entries.voucher.journal_voucher.edit', compact('Employees', 'Branches', 'VoucherData'));
                break;
            case '2':
                // This is Cash Receipt Voucher
                return view('admin.entries.voucher.cash_voucher.cash_receipt.edit', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
                break;
            case '3':
                // This is Cash Payment Voucher
                return view('admin.entries.voucher.cash_voucher.cash_payment.edit', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
                break;
            case '4':
                // This is Cash Receipt Voucher
                return view('admin.entries.voucher.bank_voucher.bank_receipt.edit', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
                break;
            case '5':
                // This is Cash Payment Voucher
                return view('admin.entries.voucher.bank_voucher.bank_payment.edit', compact('Employees', 'Branches', 'Departments', 'VoucherData'));
                break;
            case '6':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.lc_voucher.lc_payment.entry', compact('Entrie', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            case '7':
                // This is Bank Payment Voucher
                return view('admin.entries.voucher.lc_receipt.lc_receipt.entry', compact('Entrie', 'EntryType', 'EntryTypes', 'Branch', 'Employee', 'Department', 'Entry_items', 'Ledgers', 'DefaultCurrency'));
                break;
            default:
                return view('admin.entries.voucher.journal_voucher.edit', compact('Employees', 'Branches', 'VoucherData'));
                break;
        }
    }

    /**
     * Update Entrie in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateEntrieRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_entries_edit')) {
            return abort(401);
        }

        $response = CoreAccounts::updateEntry($request->all(), $id);

        if($response['status']) {
            flash('Record has been updated successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }


    /**
     * Remove Entrie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 

    /**
     * Activate Entrie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_entries_active')) {
            return abort(401);
        }

        $Entrie = Entries::findOrFail($id);
        $Entrie->update(['status' => 1]);
        EntryItems::where(['entry_id' => $Entrie->id])->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.entries.index');
    }

    /**
     * Inactivate Entrie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_entries_inactive')) {
            return abort(401);
        }

        $Entrie = Entries::findOrFail($id);
        $Entrie->update(['status' => 0]);
        EntryItems::where(['entry_id' => $Entrie->id])->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.entries.index');
    }

    /**
     * Create Journal Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */
    public function gjv_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
                // dd( $VoucherData);
            } else {
                $VoucherData['ledger_array'] = array();
            }
        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'suppliers_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'narration' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
         $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');
        
        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
//        $Departments = Departments::pluckActiveOnly();
//        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.journal_voucher.create', compact('Employees', 'Branches', 'VoucherData'));
    }

    /**
     * Journal Voucher Items Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cpv_create()
    {
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');
        $asset_types = Vendor::OrderBy('vendor_name','asc')->get();
        $laws = Laws::OrderBy('law_Section','asc')->get();
        return view('cashpayment.create', compact('asset_types','laws'));
    }
    public function rpv_create()
    {


        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');
        $asset_types = Vendor::OrderBy('vendor_name','asc')->get();
        $property = PayeeList::join('erp_branches','erp_branches.id','=','erp_payee_lists.payee_name')->get();
        $laws = Laws::OrderBy('law_Section','asc')->get();
        return view('rentpayment.create', compact('asset_types','laws','property'));
    }
    public function rpv_store(Request $request)
    {
        $id = PaymentSheet::OrderBy('sheet_code','asc')->get();
        $a = 0;
        foreach($id as $code)
        {
            $a = $code->sheet_code;
            
        }
     
        if($a !=0)
        {
        
        
        $a = $a+1;
        }
        else
        {
            $a = 101;
        }
        $payment_sheet = PaymentSheet::create([
                    'sheet_type'           => "Rent Payment Sheet",
                    'sheet_code'    => $a,
                    'prepared_by'=>Auth::user()->name,
                    'created_by'     => date('Y-m-d'),    
                ]);
        $input = $request->all();
        
                for($i = 0; $i < count($input['ledger']) ; $i++) { 
                    $arr= new PaymentSheetDetails;
                    $arr->ledger_id=$input['ledger'][$i];
                    $arr->vendor_id=$input['pay'][$i];
                    $arr->amount=$input['amount'][$i];
                    $arr->cheque_amount=$input['amount'][$i];
                    $arr->law_id=$input['law'][$i];
                    $arr->advance=$input['advance'][$i];
                    $arr->pra=$input['pra'][$i];
                    $arr->amount_after=$input['amount-after'][$i];
                    $arr->amount_before=$input['amount-before'][$i];
                    $arr->advance_appears=$input['adjust'][$i];
                    $arr->tax_rate=$input['law_rate'][$i];
                    $arr->inv_amount=$input['invoice_total'][$i];
                    $arr->remarks=$input['narration'][$i];
                    $arr->status="Pending";
                    $arr->rent_id=$input['rent_id'][$i];
                    $arr->sheet_id=$payment_sheet->id;
                    $arr->mode='Cross';
                    $arr->save();
                }
        
                flash('Record has been created successfully.')->success()->important();
        
                return redirect()->route('bank-payment');
    }

    /**
     * Store a newly created Cash Payment Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cpv_store(Request $request)
    {
        $id = PaymentSheet::OrderBy('sheet_code','asc')->get();
        $a = 0;
        foreach($id as $code)
        {
            $a = $code->sheet_code;
            
        }
        
        if($a !=0)
        {
        
        
        $a = $a+1;
        }
        else
        {
            $a = 101;
        }
        $payment_sheet = PaymentSheet::create([
                    'sheet_type'           => "Cash Payment Sheet",
                    'sheet_code'    => $a,
                    'ledger_head_id'    => $request['bank_ledger_id'],
                    'city_id'    => $request['city_id'],
                    'company_id'    => $request['company_id'],
                    'prepared_by'=>Auth::user()->name,
                    'created_by'     => date('Y-m-d'),    
                ]);
        $input = $request->all();
        
                for($i = 0; $i < count($input['ledger']) ; $i++) { 
                    $arr= new PaymentSheetDetails;
                    $arr->ledger_id=$input['ledger'][$i];
                    $arr->vendor_id=$input['pay'][$i];
                    $arr->amount=$input['amount'][$i];
                    $arr->cheque_amount=$input['amount'][$i];
                    $arr->law_id=$input['law'][$i];
                    $arr->advance=$input['advance'][$i];
                    $arr->tax_rate=$input['law_rate'][$i];
                    $arr->inv_amount=$input['invoice_total'][$i];
                    $arr->remarks=$input['narration'][$i];
                    $arr->status="Pending";
                    $arr->sheet_id=$payment_sheet->id;
                    $arr->mode='Cross';
                    $arr->save();
                }
        


                flash('Record has been created successfully.')->success()->important();
        
                return redirect()->route('bank-payment');
    }

    /**
     * All Items except Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cpv_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {

            $ledgers = Ledgers::where(['status' => 1])
                //->whereNotIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $text_ledger. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- Cash Vouchers Ends -----------------------------------
     * ----------------------------------------------------------------------------------------
    */


    
    /**
     * Create LC Payment Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */

    public function lcpv_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
              //echo '<pre>';print_r($VoucherData);echo '</pre>';exit;
            } else {
                $VoucherData['ledger_array'] = array();
            }

        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'cdr_no' => '',
                'cdr_date' => '',
                'bdr_no' => '',
                'bdr_date' => '',
                'bank_name' => '',
                'bank_branch' => '',
                'drawn_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'department_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'lc_duties' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');
        // Get All Duties
        $DutyModel = DutyModel::pluckActiveOnly();
        $DutyModel->prepend('Select a Duty', '');
        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.lc_voucher.lc_payment.create', compact('DutyModel','Employees', 'Branches', 'Departments', 'VoucherData'));
    }


    /**
     * Store a newly created LC Payment Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lcpv_store(Request $request)
    {
        $response = CoreAccounts::createLcEntry($request->all());
        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * All Items except LC Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lcpv_search(Request $request)
    {

        if(isset($request['item']) && $request['item']) {
            $ledgers = LcBankModel::selectRaw('lcbank.*,c_invoice.ci_no as invoice_no,c_invoice.id as cid')
                ->join('c_invoice','c_invoice.lcno','=','lcbank.id')
                ->where('lcbank.lc_no', 'LIKE', "%{$request['item']}%")
                ->get();
//
         // echo '<pre>';print_r($ledgers);echo'</pre>';exit;
//            $ledgers = LcBankModel::where(['status' => 1])
//                //->whereIn('group_id', $parentGroups->groupListIDs)
//                ->where(function ($query) {
//                    global $request;
//                    $query
//                        //->where('name', 'LIKE', "%{$request['item']}%")
//                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
//                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
//                    $prefix = Ledgers::getAllParent($ledger->group_id);
//                    if($prefix == '0'){
//                        $text_ledger = '('. $ledger->groups['name'] .')';
//                    }else{
//                        $text_ledger = $prefix;
//                    }
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $ledger->lc_no. ' - ' . $ledger->invoice_no,
                        'id' => $ledger->cid,
                    );
                }
            }
            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- LC Vouchers Ends -----------------------------------
     * ----------------------------------------------------------------------------------------
    */

    /**
     * Create LC receipit Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */

    public function lrp_create()
    {
        if (! Gate::allows('erp_entries_manage')) {
            return abort(401);
        }

        $VoucherData = Session::get('_old_input');
        if(is_array($VoucherData) && !empty($VoucherData)) {
            // Fetch Ledger IDs to create Ledger Objects
            $ledger_ids = array();
            if(isset($VoucherData['entry_items']) && count($VoucherData['entry_items'])) {
                $entry_items = $VoucherData['entry_items'];
                foreach($entry_items['counter'] as $key => $val)
                {
                    if(isset($entry_items['ledger_id'][$val]) && $entry_items['ledger_id'][$val]) {
                        $ledger_ids[] = $entry_items['ledger_id'][$val];
                    } else {
                        $VoucherData['entry_items']['ledger_id'][$val] = '';
                    }
                }
            }
            if(count($ledger_ids)) {
                $VoucherData['ledger_array'] = Ledgers::whereIn('id', $ledger_ids)->get()->getDictionary();
                echo '<pre>';print_r($VoucherData);echo '</pre>';exit;
            } else {
                $VoucherData['ledger_array'] = array();
            }
        } else {
            $VoucherData = array(
                'number' => '',
                'cheque_no' => '',
                'cheque_date' => '',
                'invoice_no' => '',
                'invoice_date' => '',
                'cdr_no' => '',
                'cdr_date' => '',
                'bdr_no' => '',
                'bdr_date' => '',
                'bank_name' => '',
                'bank_branch' => '',
                'drawn_date' => '',
                'voucher_date' => '',
                'entry_type_id' => '',
                'branch_id' => '',
                'employee_id' => '',
                'department_id' => '',
                'lc_id' => '',
                'remarks' => '',
                'narration' => '',
                'dr_total' => '',
                'cr_total' => '',
                'diff_total' => '',
                'entry_items' => array(
                    'counter' => array(),
                    'ledger_id' => array(),
                    'dr_amount' => array(),
                    'cr_amount' => array(),
                    'narration' => array(),
                ),
                'ledger_array' => array(),
            );
        }

        // Get All Employees
        $Employees = Staff::get()->pluck('user_id','middle_name');
        $Employees->prepend('Select an Employee', '');

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');
        $LcBankModel = LcBankModel::where('lc_status' , '3')->get();
        $LcBankModelOptions = array();
        foreach($LcBankModel as $agreement){
            if($agreement->transaction_type =='1'){
                    $tra_type ="LC";
                }else{
                    $tra_type ="TT";
                }
            $LcBankModelOptions[$agreement->id] = $tra_type." - ".$agreement->lc_no.' -  $.'.$agreement->lc_amt;
        }

        array_unshift($LcBankModelOptions, "Select a Supplier");
        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        return view('admin.entries.voucher.lc_voucher.lc_receipt.create', compact('Employees', 'Branches', 'Departments', 'VoucherData' ,'LcBankModelOptions'));
    }


    /**
     * Store a newly created LC Payment Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lrp_store(Request $request)
    {
        $response = CoreAccounts::createLcInventory($request->all());
        if($response['status']) {
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.entries.index');
        } else {
            $request->flash();
            return redirect()->back()
                ->withErrors($response['error'])
                ->withInput();
        }
    }

    /**
     * All Items except LC Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lrp_search($id)
    {
        $ledger = Ledgers::where('id', $id)->first();
        $lc_name = $ledger->name;
        $lc_name1 =  explode("-", $lc_name);
        $lc_name1 = trim($lc_name1[1]);
        $LcBankModel = LcBankModel::where('lc_no',$lc_name1)->first();
        $lc_id = $LcBankModel->id;
        $results = PerformaStockModel::where('lc_no',$lc_id)->get();
        return view('admin.entries.voucher.lc_voucher.lc_receipt.productlist', compact('results'));
    }

    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- LC receipt Vouchers Ends -----------------------------------
     * ----------------------------------------------------------------------------------------
    */

    /**
     * Create Cash Payment Voucher Entry
     *
     * @return \Illuminate\Http\Response
     */
    public function bpv_create()
    {
      
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');
        $asset_types = Vendor::OrderBy('vendor_name','asc')->get();
        $laws = Laws::OrderBy('law_Section','asc')->get();
        $ledgers = Ledgers::OrderBy('name','asc')->get();
        return view('bankpayment.create', compact('asset_types','laws','ledgers'));
    }

    public function edit_sheet($id)
    {

        $company_data=Company::all();
        $city_data=City::all();
        $payment_sheet_data=PaymentSheet::where('sheet_id',$id)->first();
        $bank_data=Ledgers::where('id',$payment_sheet_data->ledger_head_id)->first();
        $payment_sheet_details_data=PaymentSheetDetails::where('sheet_id',$id)->get();

        
        
       





       



        $asset_types = Vendor::OrderBy('vendor_name','asc')->get();
        $laws = Laws::OrderBy('law_Section','asc')->get();
        $entry= PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
        ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
        ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
        ->where('erp_payment_sheet_details.sheet_id','=',$id)
        ->get();
        return view('bankpayment.edit', compact('company_data','city_data','bank_data','payment_sheet_data','payment_sheet_details_data','entry','asset_types','laws'));
    }



    public function bank_payment_sheet_update(Request $request)
    {
        # code...


        $data=PaymentSheet::where('sheet_id',$request->sheet_id)->update(['company_id'=>$request->company_id,'city_id'=>$request->city_id,'ledger_head_id'=>$request->bank_ledger_id]);

        $check=PaymentSheetDetails::where('sheet_id',$request->sheet_id);
        $check->delete();
        

$input = $request->all();
        for($i = 0; $i < count($input['ledger']) ; $i++) { 
            $arr= new PaymentSheetDetails;
            $arr->ledger_id=$input['ledger'][$i];
            $arr->vendor_id=$input['pay'][$i];
            $arr->amount=$input['amount'][$i];
            $arr->cheque_amount=$input['amount'][$i];
            $arr->law_id=$input['law'][$i];
            $arr->advance=$input['advance'][$i];
            $arr->tax_rate=$input['law_rate'][$i];
            $arr->inv_amount=$input['invoice_total'][$i];
            $arr->remarks=$input['narration'][$i];
            $arr->status="Pending";
            $arr->pra=$input['pra'][$i];
            $arr->sheet_id=$request->sheet_id;
            $arr->mode='Cross';
            $arr->save();



        }


        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('bank-payment');
        



        
        




    }

    /**
     * Store a newly created Cash Payment Voucher in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */




     public function dgm_approval($id)
     {
       
        $asset_type = PaymentSheet::where('sheet_id',$id);
        $asset_type->update([
            'approval_by_dgm' => Auth::user()->name
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('bank-payment');
     }
     public function accountant_approval($id)
     {
        $asset_type = PaymentSheet::where('sheet_id',$id);
        $asset_type->update([
            'approval_by_accountant' => Auth::user()->name
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('bank-payment');
     }
     public function gm_approval($id)
     {
        $asset_type = PaymentSheet::where('sheet_id',$id);
        $asset_type->update([
            'approval_by_gm' => Auth::user()->name
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('bank-payment');
     }
    public function bpv_store(Request $request)
    {

$id = PaymentSheet::OrderBy('sheet_code','asc')->get();

$a = 0;
        foreach($id as $code)
        {
            $a = $code->sheet_code;
            
        }
        
        if($a !=0)
        {
        
        
        $a = $a+1;
        }
        else
        {
            $a = 101;
        }
$payment_sheet = PaymentSheet::create([
            'sheet_type'           => "Bank Payment Sheet",
            'sheet_code'    => $a,
            'ledger_head_id'    => $request['bank_ledger_id'],
            'city_id'    => $request['city_id'],
            'company_id'    => $request['company_id'],
            'prepared_by'=>Auth::user()->name,
            'created_by'     => date('Y-m-d'),    
        ]);
    
$input = $request->all();
        for($i = 0; $i < count($input['ledger']) ; $i++) { 
            $arr= new PaymentSheetDetails;
            $arr->ledger_id=$input['ledger'][$i];
            $arr->vendor_id=$input['pay'][$i];
            $arr->amount=$input['amount'][$i];
            $arr->cheque_amount=$input['amount'][$i];
            $arr->law_id=$input['law'][$i];
            $arr->advance=$input['advance'][$i];
            $arr->tax_rate=$input['law_rate'][$i];
            $arr->inv_amount=$input['invoice_total'][$i];
            $arr->remarks=$input['narration'][$i];
            $arr->status="Pending";
            $arr->pra=$input['pra'][$i];
            $arr->sheet_id=$payment_sheet->id;
            $arr->mode='Cross';
            $arr->save();
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('bank-payment');
        
      
    }

    /**
     * All Items except Bank & Cash Search Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

     public function print_sheet($id)
     {
         $date = PaymentSheet::where('sheet_id','=',$id)->first();
        $Entries = PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
        ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
        ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
        ->where('erp_payment_sheet.sheet_id','=',$id)
        ->get();
        
        return view('bankpayment.print',compact('Entries','date'));
     }
     public function cash_print_sheet($id)
     {
         $date = PaymentSheet::where('sheet_id','=',$id)->first();
        $Entries = PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
        ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
        ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
        ->where('erp_payment_sheet.sheet_id','=',$id)
        ->get();
        
        return view('cashpayment.print',compact('Entries','date'));
     }
     public function rent_print_sheet($id)
     {
         $date = PaymentSheet::where('sheet_id','=',$id)->first();
        $Entries = PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
        ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
        ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
        ->where('erp_payment_sheet.sheet_id','=',$id)
        ->get();
        
        return view('rentpayment.print',compact('Entries','date'));
     }
    public function bpv_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
            $Setting = Settings::findOrFail(Config::get('constants.accounts_cash_banks_head_setting_id'));
            $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;
            $parentGroups->build($Setting->description);
            $parentGroups->toListArray($parentGroups, -1);

            $ledgers = Ledgers::where(['status' => 1])
                //->whereNotIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix = Ledgers::getAllParent($ledger->group_id);
                    if($prefix == '0'){
                        $text_ledger = '('. $ledger->groups['name'] .')';
                    }else{
                        $text_ledger = $prefix;
                    }
                    $result[] = array(
                        'text' => $text_ledger. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /*
     * ----------------------------------------------------------------------------------------
     * ------------------------------- Cash Vouchers Ends -----------------------------------
     * ----------------------------------------------------------------------------------------
    */

    /**
     * Cash Ledgers Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function cash_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
            $Group = CoreAccounts::getConfigGroup(Config::get('constants.account_cash_inHand'))['group'];
            $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;
            $parentGroups->build($Group->id);
            $parentGroups->toListArray($parentGroups, -1);
            $ledgers = Ledgers::where(['status' => 1])
                ->whereIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            //dd($ledgers);
            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $result[] = array(
                        'text' => $ledger->number . ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /**
     * Banks Ledger Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function bank_search(Request $request)
    {

        if(isset($request['item']) && $request['item']) {
          $Group = CoreAccounts::getConfigGroup(Config::get(63))['group'];

              $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;
            $parentGroups->build($Group->id);
            $parentGroups->toListArray($parentGroups, -1);

            $ledgers = Ledgers::where(['status' => 1])
                ->whereIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $prefix_group = Ledgers::getParent($ledger->group_id);
                    $result[] = array(
                        //'text' => $ledger->number . ' - ' . $ledger->name,
                        'text' => $prefix_group. ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            } 

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }

    /**
     * Cash & Banks Ledger Search
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function generate_voucher($id)
    {
        $entry_items= PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_payment_sheet.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
        ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
        ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
        ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
        ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
        ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
        ->where('erp_payment_sheet_details.ps_id','=',$id)
        ->get();

              $group_id = 0;
            $branch_id = 0;
            $rules=array();
            foreach($entry_items as $val)
            {
                $advance = $val->advance;
                $pra = $val->pra;
                $total_cheque = $val->inv_amount;
                $amount_total = $total_cheque-$advance-$pra;
                if($val->tax_rate!=0)
                {
                $total = $val->inv_amount;
                $tax_rate = $val->tax_rate;
                $total_tax = ($total/100) * $tax_rate;
                $amount_total = $amount_total -$total_tax;
                }
                $rules['entry_type_id'] = 1;
                $rules['dr_total'] = $amount_total;
                $rules['cr_total'] = $val->cheque_amount;
                $rules['narration'] = $val->remarks;
                $rules['branch_id'] = $val->branch_name;
                $rules['created_by'] = Auth::user()->id;
                $rules['updated_by'] = Auth::user()->id;
                $rules['employee_id'] = Auth::user()->id;
                $rules['status'] = 1;
                $rules['voucher_date'] = Carbon::Now();
                $group_id = $val->group_id;
                $branch_id = $val->branch_id;
            }
           
 
            $Group = CoreAccounts::getConfigGroup($group_id);
            $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;

            $parentGroups->build($Group['group']['id']);
            $parentGroups->toListArray($parentGroups, -1);
            $ledgers = Ledgers::where(['status' => 1])
                ->whereIn('group_id', $parentGroups->groupListIDs)
                ->where('branch_id', $branch_id)->OrderBy('name','asc')->first();
             
        $entry = Entries::create($rules);

        $entry->update(array(
            'number' => CoreAccounts::generateNumber($entry->id),
        ));

        /*
         * Create Entry Items records associated to Etnry now
         */
        $entry_itemsss = array();
        foreach($entry_items as $val)
        {




            $entry_itemss = array(
                'status' => 0,
                'entry_type_id' => 1,
                'entry_id' => $entry->id,
                'voucher_date' => Carbon::Now(),
                'ledger_id' => $val->ledger_head_id,
                'narration' => $val->remarks
            );
                $entry_itemss['amount'] = $val->cheque_amount;
                $entry_itemss['dc'] = 'd';
                $entry_itemsss[] = $entry_itemss;
if($val->tax_rate != 0)
{

                $total = $val->inv_amount;
                $tax_rate = $val->tax_rate;
                $total_tax = ($total/100) * $tax_rate;
                $entry_itemss = array(
                    'status' => 0,
                    'entry_type_id' => 1,
                    'entry_id' => $entry->id,
                    'voucher_date' => Carbon::Now(),
                    'ledger_id' => $ledgers->id,
                    'narration' => $val->remarks
                );
                    $entry_itemss['amount'] = $total_tax;
                    $entry_itemss['dc'] = 'd';
                    $entry_itemsss[] = $entry_itemss;
}
           
            $entry_itemss = array(
                'status' => 0,
                'entry_type_id' => 1,
                'entry_id' => $entry->id,
                'voucher_date' => Carbon::Now(),
                'ledger_id' => $val->ledger_id,
                'narration' => $val->remarks
            );
            $advance = $val->advance;
            $pra = $val->pra;
            $total_cheque = $val->inv_amount;
            $amount_total = $total_cheque-$advance-$pra;
                $entry_itemss['amount'] = $amount_total;
                $entry_itemss['dc'] = 'c';
            
            $entry_itemsss[] = $entry_itemss;
        }
        $update_record = PaymentSheetDetails::where('ps_id', $id);
              $update_record->update([
                 'status'=> 'Generated',
                 'entry_type'=>1,
                 'entry_id' => $entry->id
                 ]
             ); 
        EntryItems::insert($entry_itemsss);
        
        flash('Voucher Has been Created Successfully.')->success()->important();
 
        return redirect()->route('payment-sheet-list');




    }
    public function get_payment_list()
    {
      
      $Entries = PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
            ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
            ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
            ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
            ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
            ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
            ->where('erp_payment_sheet_details.status','Pending')
            ->where('erp_payment_sheet.sheet_type','Bank Payment Sheet')
            ->get();
            $cash_entries = PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
            ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
            ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
            ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
            ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
            ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
            ->where('erp_payment_sheet_details.status','Pending')
            ->where('erp_payment_sheet.sheet_type','Cash Payment Sheet')
            ->get();
            $rental_entries = PaymentSheet::SelectRaw('erp_payment_sheet_details.*,erp_vendor.*, erp_ledgers.*, erp_laws.*,erp_branches.name as branch_name')
            ->join('erp_payment_sheet_details','erp_payment_sheet_details.sheet_id','=','erp_payment_sheet.sheet_id')
            ->join('erp_ledgers','erp_ledgers.id','=','erp_payment_sheet_details.ledger_id')
            ->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')
            ->join('erp_laws','erp_laws.law_id','=','erp_payment_sheet_details.law_id')
            ->join('erp_vendor','erp_vendor.vendor_id','=','erp_payment_sheet_details.vendor_id')
            ->where('erp_payment_sheet_details.status','Pending')
            ->where('erp_payment_sheet.sheet_type','Rent Payment Sheet')
            ->get();
        
            return view('bankpayment.list',compact('Entries','cash_entries','rental_entries'));
    

    }
    public function cashbank_search(Request $request)
    {
        if(isset($request['item']) && $request['item']) {
            $Group = CoreAccounts::getConfigGroup(Config::get('constants.assets_current_cash_balance'))['group'];
            $parentGroups = new GroupsTree();
            $parentGroups->current_id = -1;
            $parentGroups->build($Group->id);
            $parentGroups->toListArray($parentGroups, -1);

            $ledgers = Ledgers::where(['status' => 1])
                ->whereIn('group_id', $parentGroups->groupListIDs)
                ->where(function ($query) {
                    global $request;
                    $query->where('name', 'LIKE', "%{$request['item']}%")
                        ->orwhere('number', 'LIKE', "%{$request['item']}%");
                })->OrderBy('name','asc')->get();

            $result = array();
            if($ledgers->count()) {
                foreach ($ledgers as $ledger) {
                    $result[] = array(
                        'text' => $ledger->number . ' - ' . $ledger->name,
                        'id' => $ledger->id,
                    );
                }
            }

            return response()->json($result);
        } else {
            return response()->json([]);
        }
    }
}
