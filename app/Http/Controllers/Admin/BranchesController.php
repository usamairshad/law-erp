<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branches;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Groups;
use App\Helpers\GroupsTree;
use App\Models\Admin\Company;
use App\Models\Academics\Board;
use App\Models\Academics\BranchBoardPrograms;
use Auth;
use Config;
use App\Helpers\Helper;
use Redirect;
class BranchesController extends Controller
{
    /**
     * Display a listing of Branch.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_branches_manage')) {
            return abort(401);
        }

        $Branches = Branches::OrderBy('created_at','desc')->get();
        $boards=Board::all();
        return view('admin.branches.index', compact('Branches','boards'));
    }

    /**
     * Show the form for creating new Branche.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $company_name=Helper::companyIdToName($id);
        if (! Gate::allows('erp_branches_create')) {
            return abort(401);
        }
       
        $city = City::all()->pluck('name','id');
        $company = Company::all()->pluck('name','id');
        $country = Country::all()->pluck('name','id');

        $common_ledgers = Ledgers::where('branch_id', Config::get('constants.head_office_id'))
            ->where('is_common',1)
            ->select('id','name','group_number')
            ->get();
            $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));


        $common_ledgers_list = $common_ledgers->map(function ($item, $key) {
            $name = rtrim(str_after($item['name'],'('), ')');
            $item['name'] = $name." (".$item['group_number'].")";
            return $item;
        });

        return view('admin.branches.create',compact('Groups','city','country', 'company', 'common_ledgers_list','company_name','id'));
        ///
    }

    /**
     * Store a newly created Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreBranchesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranchesRequest $request)
    {

        if (! Gate::allows('erp_branches_create')) {
            return abort(401);
        }
       
         $rules['branch_code']  =  'required|unique:erp_branches';
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()
                ->withErrors($validator->errors())
                ->withInput();
        }
       
        //dd($request);
        $branch = Branches::create([
            'name'              => $request['name'],
            'city_id'           => $request['city_id'],
            'company_id'         => $request['company_id'],
            'country_id'        => 1,
            'branch_code'       => $request['branch_code'],
            'phone'             => $request['phone'],
            'fax'               => $request['fax'],
            'cell'              => $request['cell'],
            'address'           => $request['address'],
            'created_by'        => Auth::user()->id,
        ]);
       
//        $branches_dr = CoreAccounts::getConfigGroup(Config::get('constants.accounts_debt_branch'));

        $input = $request->all();
        
        //////create city group/////////////
/*         $city_group = Groups::where('company_id',$request['company_id'])->where('city_id',$request['city_id'])->whereNull('branch_id')->count();
        if($city_group == 0){
            $company_count = Groups::where('company_id',$request['company_id'])->whereNull('city_id')->whereNull('branch_id')->get();
           
            if(count($company_count) > 0){ 
                foreach ($company_count as $key) { */
                      
             /*         $param['parent_id'] = $request['parent_id'];

                     $param['company_id'] = $request['company_id'];
                     $param['city_id'] =  $request['city_id'];
                     $param['name'] = City::whereId($request['city_id'])->value('name');
                     $param['code']=$key->code;
                     $response = CoreAccounts::createGroup($param);   */
                    
           //     }  
          //  }
            
       // }
        //////create branch group/////////////

         $city_count = Groups::where('company_id',$request['company_id'])->where('city_id',$request['city_id'])->whereNull('branch_id')->get();
  
            if(count($city_count) > 0){ 
                foreach ($city_count as $key) {

                    $city_group = Groups::where('city_id',$request['city_id'])->get();
                    $input['company_id'] = $request['company_id'];
                    $input['city_id'] =  $request['city_id'];
                    $input['parent_id'] = $key->id;
                    $input['branch_id'] = $branch->id;
                    $input['code']=$key->code;
                    
                    $response = CoreAccounts::createGroup($input);
                    
                }  
            }
         
        
        // foreach ($input['common_ledgers'] as $key => $common_ledger) {
        //     $ledger = Ledgers::find($common_ledger);
        //     $replaceFrom = str_before($ledger->name,'(');
        //     $name = str_replace($replaceFrom,$input['name']." ",$ledger->name);
        //     $ledger->name = $name;
        //     $Ledger = array(
        //         'name' => $ledger->name,
        //         'group_id' => $ledger->group_id,
        //         'balance_type' => $ledger->balance_type,
        //         'opening_balance' => 0,
        //         'parent_type' => $branch->id,
        //         'branch_id' => $branch->id
        //     );
        //     $response = CoreAccounts::createLedger($Ledger);
        // }
         $param= $request['company_id'];

//        $Ledger = array(
//            'name' => $request['name'],
//            'group_id' => $branches_dr['group']->id,
//            'balance_type' => 'd',
//            'opening_balance' => 0,
//            'parent_type' => $branch->id,
//        );
//        $response = CoreAccounts::createLedger($Ledger);
//        $branches_loan = CoreAccounts::getConfigGroup(Config::get('constants.accounts_loan_branch'));
//        $Ledger_loan = array(
//            'name' => $request['name'],
//            'group_id' => $branches_loan['group']->id,
//            'balance_type' => 'd',
//            'opening_balance' => 0,
//            'parent_type' => $branch->id,
//        );
//        $response = CoreAccounts::createLedger($Ledger_loan);
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.view-company-branch',[$param]);
    }


    /**
     * Show the form for editing Branche.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        
        $entries = array(
            "number" => "??????",
            "voucher_date" => date('Y-m-d'),
//            "employee_id" => Auth::user()->id,
            "branch_id" => 1,
//            "student_name" => $data['student_name'],
//            "registration_no" => $data['registration_no'],
//            "father_name" => $data['father_name'],
//            "father_cnic" => $data['father_cnic'],
//            "program_id" => $data['program_id'],
//            "bill_no" => $data['bill_no'],
            "narration" => "Security Fee Deposit",
            "entry_type_id" => "1",
            "entry_items" => array(
                1 => [
                    'ledger_id' => 2,
                    'dr_amount' => 0,
                    'cr_amount' => 1200,
                    'narration' => "Fee",
                ],
                2 => [
                    'ledger_id' => 3,
                    'dr_amount' => 1200,
                    'cr_amount' => 0,
                    'narration' => "Fee",
                ]
            ),
            "diff_total" => "0",
            "due_date" => date('Y-m-d'),
            'saletax_due_date' => date('Y-m-d'),
            "dr_total" => 1200,
            "cr_total" => 1200,
            "cheque_no" => null,
            "cheque_date" => date('Y-m-d'),
            "invoice_no" =>  123,
            "invoice_date" => date('Y-m-d'),
        );

        // dd($entries);
        CoreAccounts::createJEntry($entries);

        die('here');


        if (! Gate::allows('erp_branches_edit')) {
            return abort(401);
        }
        $Branche = Branches::findOrFail($id);
        $city = City::all()->pluck('name','id');
        $company = Company::all()->pluck('name','id');
        $country = Country::all()->pluck('name','id');
        return view('admin.branches.edit', compact('Branche','city','country','company'));
    }

    /**
     * Update Branche in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateBrancheRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBranchesRequest $request, $id)
    {
        if (! Gate::allows('erp_branches_edit')) {
            return abort(401);
        }

        Branches::where('id',$id)->update([
            'name'              => $request['name'],
            'territory_id'      => $request['territory_id'],
            'city_id'           => $request['city_id'],
            'region_id'         => $request['region_id'],
            'country_id'        => $request['country_id'],
            'branch_code'       => $request['branch_code'],
            'phone'             => $request['phone'],
            'fax'               => $request['fax'],
            'cell'              => $request['cell'],
            'address'           => $request['address'],
            'updated_by'        => Auth::user()->id,
        ]);
        $branches_dr = CoreAccounts::getConfigGroup(Config::get('constants.accounts_debt_branch'));
        Ledgers::where('parent_type',$id)->where('group_id',$branches_dr['group']->id)->update([
            'name' => $request['name'],
            'updated_by' => Auth::user()->id,
        ]);
//        $branches_loan = CoreAccounts::getConfigGroup(Config::get('constants.accounts_loan_branch'));
//        Ledgers::where('parent_type',$id)->where('group_id',$branches_loan['group']->id)->update([
//            'name' => $request['name'],
//            'updated_by' => Auth::user()->id,
//        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.branches.index');
    }


    /**
     * Remove Branche from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('erp_branches_destroy')) {
            return abort(401);
        }

        $Branche = Branches::findOrFail($id);
        $Branche->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.branches.index');
    }
    public function storeA(StoreBranchesRequest $request){
        if (! Gate::allows('erp_branches_create')) {
            return abort(401);
        }
        //dd($request);
        $rules['branch_code']  =  'required|unique:erp_branches';
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $request->flash();
            return redirect()->back()
                ->withErrors($validator->errors())
                ->withInput();
        }

        //dd($request);
        $branch = Branches::create([
            'name'              => $request['name'],
            'city_id'           => $request['city_id'],
            'company_id'         => $request['company_id'],
            'country_id'        => $request['country_id'],
            'branch_code'       => $request['branch_code'],
            'phone'             => $request['phone'],
            'fax'               => $request['fax'],
            'cell'              => $request['cell'],
            'address'           => $request['address'],
            'created_by'        => Auth::user()->id,
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.branches.index');

    }
    public function updateA(UpdateBranchesRequest $request, $id){
        if (! Gate::allows('erp_branches_edit')) {
            return abort(401);
        }
        Branches::where('id',$id)->update([
            'name'              => $request['name'],
            'city_id'           => $request['city_id'],
            'company_id'         => $request['company_id'],
            'country_id'        => $request['country_id'],
            'branch_code'       => $request['branch_code'],
            'phone'             => $request['phone'],
            'fax'               => $request['fax'],
            'cell'              => $request['cell'],
            'address'           => $request['address'],
            'created_by'        => Auth::user()->id,
        ]);
        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.branches.index');
    }
    public function viewCompanyBranches($id)
    {

        $Branches= Branches::where('company_id',$id)->get();
        $company_id=$id;
        $boards=Board::all();
        return view('admin.branches.index', compact('Branches','company_id','boards'));
    }

    public function branchBoardProgramStore(Request $request)
    {
       
       
        $input=$request->all();
        foreach ($input['programs'] as $key => $value) {
            BranchBoardPrograms::create([
                'board_id'=>$input['board_id'],
                'branch_id'=>$input['branch_id'],
                'program_id'=>$value
            ]);
        }
        return Redirect::back();
    }
    public function loadBranchBoardPrograms(Request $request)
    {
        $bbg= BranchBoardPrograms::where('branch_id',$request->id)->get();

        $data=[];
        foreach ($bbg as $key => $value) {
            $data[$key]['board_id']= Helper::boardIdToName($value['board_id']);
            $data[$key]['program_id']=Helper::programIdToName($value['program_id']);
        }
        return $data;
    }

    public function loadBranchBoardProgramsData(Request $request)
    {
     
        $bbg= BranchBoardPrograms::where('branch_id',$request->id)->get();

        $data=[];
        foreach ($bbg as $key => $value) {
            $data[$key]['board_id']= Helper::boardIdToName($value['board_id']);
            $data[$key]['program_id']=Helper::programIdToName($value['program_id']);
        }
        return $data;
    }

}
