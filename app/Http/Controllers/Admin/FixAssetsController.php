<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\LandManagement\PayeeList;

use App\Models\Admin\FixedAssetsModel;
use App\Models\Admin\Branches;
use App\Models\Admin\SuppliersModel;
use App\Models\Admin\Ledgers;
use App\Helpers\LedgersTree;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Groups;
use App\Helpers\GroupsTree;
use Auth;
use Config;

class FixAssetsController extends Controller
{

    public function index()
    {
        

        $FixedAssetsModel = FixedAssetsModel::all();
        return view('admin.fix_assets.index',compact('FixedAssetsModel'));
    }


    public function create()
    {

       
        $Branches = Branches::pluck('name' , 'id');


        $Ledgers = Ledgers::pluck('name' , 'id');

        $supplier = PayeeList::pluck('payee_name' , 'id');


        //$Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));

        return view('admin.fix_assets.create', compact('Ledgers','Branches', 'supplier'));
    }

    public function store(Request $request)
    {
     //dd($request);
       

//        $Lc_ledger = Ledgers::where('parent_type',$request['suplier_id'])->where('group_id','13')->first();

       // $Lc_ledger = Ledgers::where('parent_type',$request['suplier_id'])->where('group_id', Config::get('constants.accounts_cd_local'))->first();

// dd($Lc_ledger);
        $FixedAssetsModel =  FixedAssetsModel::create([
          'product_name'   =>  $request['products_name'],
          'products_date'   =>  $request['products_date'],
          'ledger_id'       =>  $request['ledger_id'],
          'suplier_id'      =>  $request['suplier_id'], 
          'closing_stock'      =>  $request['closing_stock'],  
          'item_is'         =>  $request['item_is'],
          'branch'          =>  $request['branch'],
          'price'           =>  $request['price'],
          'po_number'       =>  $request['po_number'],
          'serial_number'   =>  $request['serial_number'],
          'warranty_expires'=>  $request['warranty_expires'],
          'remarks'         =>  $request['remarks'],
          'products_desc'   =>   $request['products_desc']
        ]);

        // $entries2 = array(
        //     "number" => "??????",
        //     "voucher_date" => date('Y-m-d'),
        //     "employee_id" => Auth::user()->id,
        //     "narration" => $request['remarks'],
        //     "entry_type_id" => "1",
        //     "entry_items" => array(
        //         1 => [
        //             'ledger_id' => $request['cgroup_id'],
        //             'dr_amount' => $request['price'],
        //             'cr_amount' => 0,
        //             'narration' => $request['remarks'],
        //         ],
        //         2 => [
        //             'ledger_id' => $Lc_ledger->id,
        //             'dr_amount' => 0,
        //             'cr_amount' =>$request['price'],
        //             'narration' => $request['remarks'],
        //         ],
        //     ),
        //     "diff_total" => "0",
        //     "dr_total" => $request['price'],
        //     "cr_total" =>$request['price'],
        //     "cheque_no" => null,
        //     "cheque_date" => date('Y-m-d'),
        //     "invoice_no" => null,
        //     "invoice_date" => date('Y-m-d'),
        // );
        // CoreAccounts::createJEntry($entries2);
        flash('Record has been created successfully.')->success()->important();

        //dd('Hard coded enetry done with id 13 and entry_type_id 1, Remove dd when you make it dynamic');

        return redirect()->route('admin.fix_assets.index');
    }

    public function show($id)
    {
       
        //
    }


    public function edit($id)
    {
        $FixedAssetsModel = FixedAssetsModel::findOrFail($id);
        $Branches = Branches::pluckActiveOnly();
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('parent_id'));
        return view('admin.fix_assets.edit', compact('Branches', 'Groups', 'FixedAssetsModel'));
    }


    public function update(Request $request, $id)
    {
        
        FixedAssetsModel::where('id',$id)->update([
            'products_name'   =>  $request['products_name'],
            'products_date'   =>  $request['products_date'],
            'parent_id'       =>  $request['parent_id'],
            'item_is'         =>  $request['item_is'],
            'customer_name'   =>  $request['customer_name'],
            'branch'          =>  $request['branch'],
            'po_number'       =>  $request['po_number'],
            'serial_number'   =>  $request['serial_number'],
            'warranty_expires'=>  $request['warranty_expires'],
            'remarks'         =>  $request['remarks'],
            'products_desc'   =>   $request['products_desc']
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.fix_assets.index');
    }


    public function destroy($id)
    {
        
        //
    }

    public function get_asset(Request $request)
    {
    

       $data = FixedAssetsModel::where('branch' , $request->branch_id)->get();
        \Log::info($data);

       return $data;


    }



}
