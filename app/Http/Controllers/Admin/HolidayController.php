<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Holiday;
use Auth;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $holidays = Holiday::all();

        return view('admin.holiday.index', compact('holidays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.holiday.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        Holiday::create([
                'holiday_name' => $request->holiday_name,
                'holiday_date' => $request->holiday_date,
                'created_by' => Auth::user()->id,
               
            ]);
         flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.holiday.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $holidays = Holiday::find($id);
        return view('admin.holiday.edit', compact('holidays'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
       $holidays = Holiday::findOrFail($id);

        $data['updated_by'] = Auth::user()->id;

        $holidays->update($data);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.holiday.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Holiday = Holiday::findOrFail($id);
        $Holiday->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.holiday.index');
    }
}
