<?php


namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TaxSettings\StoreUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Vendor;
use App\Models\LawDetails;
use App\Models\Admin\TaxSettings;
use Auth;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     * 
     */
     public function get_vendor_id(Request $request)
    {
        # code...
        
        $vendor = Vendor::where('vendor_id',$request['pay_id'])->first();
        return $vendor;
    }


    public function get_tax_precentage(Request $request)
    {
        # code...
      
        $tax_per=LawDetails::where('law_id',$request['law_name'])->where('type',$request['payee_type_name'])->where('status',$request['payee_category_name'])->first();
       
        return $tax_per;
 
    }
    public function get_tax_amount_per(Request $request)
    {
        # code...
   
        $tax_per = 0;
        if($request['payee_type_name']=="Company")
        {
            $amount = $request['inv_total_id'];
            $amount = $amount*12;
        $tax_per=LawDetails::where('law_id',$request['law_name'])->where('type',$request['payee_type_name'])->where('status',$request['payee_category_name'])->first();
        foreach($tax_per as $tax)
        {
            //$fix_amount = $tax->fix_amount;
            $tax_percent = $tax->percentage;
        
            $tax_amount = ($amount/100)*$tax_percent;
     
            $tax_amount = $tax_amount/12;
            $tax_per = $tax_amount;
        }
        
        $amount = $request['inv_total_id'];
    }
        else
        {
          
            $amount = $request['inv_total_id'];
            $amount = $amount*12;
            
        $tax_per = TaxSettings::where('start_range','<=',$amount)->where('end_range','>=',$amount)->where('law_id','2')->get();

        foreach($tax_per as $tax)
        {
            $start = $tax->start_range;
            $end = $tax->end_range;
            $amount1 = $end-$start;
            $fix_amount = $tax->fix_amount;
            $tax_percent = $tax->tax_percent;
        
            $tax_amount = ($amount1/100)*$tax_percent;
     
            $tax_amount = $tax_amount+$fix_amount;
            $tax_amount = $tax_amount/12;
            $tax_per = $tax_amount;

        }

        
        }
     
        return $tax_per;
 
    }


    public function index()
    {
        $asset_types = Vendor::OrderBy('created_at','desc')->get();
        return view('vendorlist.index', compact('asset_types')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendorlist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        
        Vendor::create([
            'vendor_name'           => $request['name'],
            'cnic'    => $request['cnic'],
            'ntn'    => $request['ntn'],
            'salestaxno'    => $request['salestax'],
            'email'    => $request['email'],
            'contact'    => $request['contact'],
            'addresss'    => $request['address'],
            'type'    => $request['filer'],
            'category'    => $request['category'],
            'created_by'     => Auth::user()->id,    
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('vendor-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset_type = Vendor::where('erp_vendor.vendor_id',$id)->first();
   
        return view('vendorlist.show', compact('asset_type'));
    }
    public function fetch(Request $request)
    {
        $id = $request->id;
        $asset_type = Vendor::where('erp_vendor.vendor_id',$id)->first();
   
        return response()->json($asset_type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset_types = Vendor::where('erp_vendor.vendor_id',$id)->first();
        return view('vendorlist.edit', compact('asset_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
        {
   $id = $request['id'];

        $Branche = Vendor::where('vendor_id',$id);
        $Branche->update([
            'vendor_name'           => $request['name'],
            'cnic'    => $request['cnic'],
            'ntn'    => $request['ntn'],
            'salestaxno'    => $request['salestax'],
            'email'    => $request['email'],
            'contact'    => $request['contact'],
            'addresss'    => $request['address'],
            'type'    => $request['filer'],
            'category'    => $request['category'],
            'updated_by'     => Auth::user()->id,    
        ]);
       

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('vendor-list');
    }

    public function destroy($id)
    {
       
        $Branche = Vendor::where('vendor_id',$id);
        
        $Branche->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('vendor-list');
    }
}
