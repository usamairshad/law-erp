<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\ReleasStockModel;
use App\PayMargin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin\LcBankModel;
use App\Models\Admin\Ledgers;
use App\Models\Admin\CalculateMargin;
use App\Models\Admin\Orders;
use App\Models\Admin\Groups;
use App\Models\Admin\BanksModel;
use App\Models\Admin\PerformaStockModel;
use App\Models\Admin\OrderDetailModel;
use App\Models\Admin\ComercialInvoiceModel;
use App\Models\Admin\DolerLcModel;
use App\Models\Admin\Warehouse;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\GrBufferModel;
use App\Helpers\CoreAccounts;
use App\Helpers\LedgersTree;
use Auth;
use Config;
use DB;

class LcBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }

        $LcBank = LcBankModel::all();
        return view('admin.lcbank.index', compact('LcBank'));
    }
    public function createGrImport(){
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        $performa_id = PerformaStockModel::where('balanace_qty','>=',1)->pluck('ci_id');
        $c_invoice = ComercialInvoiceModel::whereIn('id', $performa_id)->get();
        $performa_status = PerformaStockModel::where('balanace_qty', 0)->pluck('ci_id');
        $itemscount = OrderDetailModel::whereIn('perchas_id', $performa_status)->pluck('perchas_id');
//        $itemscount = OrderDetailModel::where('perchas_id', $keys['pfno'])->sum('remaining_qty');
//        dd($performa_status,$itemscount);
        if ($itemscount) {
            $status1=Orders::whereIn('id', $itemscount)->update([
                'status' => 4,
            ]);
        }

        $warehouse = Warehouse::all();
        $Banks = BanksModel::all();
        $parentGroups = new LedgersTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
        $parentGroups->toList($parentGroups, -1);
        $Ledgers = $parentGroups->ledgerList;
        return view('admin.lcbank.grimportcreate', compact('c_invoice', 'warehouse', 'Ledgers', 'Banks'));
    }

    public function viewGrImport($id){
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        $result=ReleasStockModel::where('lc_id',$id)->get();
        return view('admin.lcbank.grimportview', compact( 'result'));
    }

    public function viewBuffer($id){

        $warehouse = Warehouse::all();
        $result=GrBufferModel::where('c_invoice', $id)->where('status', 0)->get();
        return view('admin.lcbank.grbuffer_detail', compact('result', 'id', 'warehouse'));
    }
    public function transferInventory(Request $request)
    {
       // dd($request);
        $counter = 1;
        $total_paid_amount = 0;
        $inventory = array();
        $Ledger_entry = array();
        $result = GrBufferModel::where('c_invoice', $request->c_id)->where('status', 0)->get()->toArray();
        if(count($result)==0){
            flash('There is no stock in bound.')->error()->important();
            return redirect()->route('admin.grimport');
        }
        $ComercialInvoiceModel = ComercialInvoiceModel::where('id', $request->c_id)->first();
        $dolerrate = LcBankModel::where('id', $ComercialInvoiceModel->lcno)->first();
        $parent_Id = $dolerrate->bnk_id;
        $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_bank_cd_importer')])->where(['parent_type' => $parent_Id])->first();
        $Assets_id = $TR_ledger->id;
        $transit_ledger = Ledgers::where('parent_type',$dolerrate->id)->where('group_id',$Assets_id)->first();
        for ($i = 0; $i < count($result); $i++) {
            if ($result[$i]['rel_qty'] == 0) {
                continue;
            }
           if($result[$i]['rel_qty']){

               $segregate_qty = $result[$i]['rel_qty'] * $result[$i]['rel_qty'];
               $segregate_unit_price = $result[$i]['unit_price'] / $result[$i]['rel_qty'];
           }else{
               $segregate_qty = $result[$i]['rel_qty'];
               $segregate_unit_price = $result[$i]['unit_price'];
           }
            $product_ledger = ProductsModel::getStockGroup($result[$i]['product_id']);
            $inventory[$counter]['commercial_invoice'] = $result[$i]['c_invoice'];
            $inventory[$counter]['product_id'] = $result[$i]['product_id'];
            $inventory[$counter]['rel_qty'] = $result[$i]['rel_qty'];
            $inventory[$counter]['import_date'] = $result[$i]['import_date'];
            $inventory[$counter]['unit_price'] = $result[$i]['unit_price'];
            $inventory[$counter]['branch_id'] = $request->warehouse_id;
            $Ledger_entry[$counter]['ledger_id'] = $product_ledger;
            $Ledger_entry[$counter]['dr_amount'] = round($result[$i]['rel_qty'] * $result[$i]['unit_price'], 4);
            $Ledger_entry[$counter]['cr_amount'] = 0;
            $Ledger_entry[$counter]['narration'] = 'Stock Transfer from Transit to inventory';
            $counter++;
            $total_paid_amount = $total_paid_amount + round($result[$i]['rel_qty'] * $result[$i]['unit_price'], 4);
        }
        $results = StockItemsController::importInventory($inventory);
       for($a=0;$a<count($result);$a++){
           GrBufferModel::where('id',$result[$a]['id'])->update([
               'status' => 1,
           ]);
       }
        if ($transit_ledger->id && $total_paid_amount != '') {
            $vencounter = count($Ledger_entry);
            $vencounter = $vencounter + 1;
            $Ledger_entry[$vencounter]['ledger_id'] = $transit_ledger->id;
            $Ledger_entry[$vencounter]['dr_amount'] = 0;
            $Ledger_entry[$vencounter]['cr_amount'] = $total_paid_amount;
            $Ledger_entry[$vencounter]['narration'] = 'Stock Transfer from Transit to inventory';
            $entries2 = array(
                "number" => "??????",
                "voucher_date" => date('Y-m-d'),
                "employee_id" => Auth::user()->id,
                "narration" => 'Stock Transfer from Transit to inventory',
                "entry_type_id" => "1",
                "entry_items" => $Ledger_entry,
                "diff_total" => "0",
                "dr_total" => $total_paid_amount,
                "cr_total" => $total_paid_amount,
                "cheque_no" => null,
                "invoice_no" => null,
            );
            CoreAccounts::createJEntry($entries2);
            // code to transfer buffer stock into inventory
        }

        return redirect()->route('admin.grimport');
    }

    public function sheetCsoting()
    {
        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        $lcBankModel = lcBankModel::where('lc_status','1')->get();
        return view('admin.lcbank.sheetcosting',compact('lcBankModel'));
    }

    public function sheet_datatables()
    {
        $LcBank = LcBankModel::where('lc_status','1')->get();
        return  datatables()->of($LcBank)
            ->addColumn('bnk_id', function($LcBank) {
                if($LcBank->transaction_type == 2 && $LcBank->lc_type == 2){
                    return $banks =  $LcBank->suppliersModel->sup_name;
                }else{
                    return $banks =  $LcBank->banksModel->bank_name;
                }
            })
            ->addColumn('lc_status', function($LcBank) {
                if ($LcBank->lc_status == 1) {
                    return $status ="Open";
                }elseif ($LcBank->lc_status == 2){
                    return $status = "IN Transit";
                }elseif ($LcBank->lc_status == 3){
                    return $status = "IN Bound";
                }elseif ($LcBank->lc_status == 4){
                    return $status = "Completed";
                }else{
                    return $status = "Cancel";
                }
            })->addColumn('action', function ($LcBank) {
                if($LcBank->today_doller !=''){
                    $action_column = '<a href="costing/'.$LcBank->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Costing</a>';
                }else{
                    $action_column = '<a href="costing/'.$LcBank->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> Costing</a>';
                }

                return $action_column;
            })->rawColumns(['bnk_id','lc_status','action'])
            ->toJson();
    }


    public function datatables(){
        $LcBank = LcBankModel::OrderBy('id','DESC')->get();
        return  datatables()->of($LcBank)
            ->addColumn('transaction_type', function($LcBank) {
                $transaction_type = array_get( Config::get('admin.mode_of_payment'), $LcBank->transaction_type);
                return $transaction_type;
            })->addColumn('lc_type', function($LcBank) {
                if($LcBank->transaction_type == 2){
                    $lc_type = array_get( Config::get('admin.tt_types'), $LcBank->lc_type);
                }else{
                    $lc_type = array_get( Config::get('admin.lc_types'), $LcBank->lc_type);
                }

                return $lc_type;
            })->addColumn('pf_no', function($LcBank) {
                $result = LcBankModel::order($LcBank->pf_no);
                return $result;
            })
            ->addColumn('bnk_id', function($LcBank) {
                if($LcBank->transaction_type == 2 && $LcBank->lc_type == 2){
                    return $banks =  $LcBank->suppliersModel->sup_name;
                }else{
                    return $banks =  $LcBank->banksModel->bank_name;
                }
            })->addColumn('today_doller', function($LcBank) {
                if($LcBank->today_doller !=''){
                    return $banks =  $LcBank->today_doller;
                }else{
                    return $banks = "N/A";
                }
            })->addColumn('open_date', function($LcBank) {
                if($LcBank->open_date !=''){
                    return $banks =  date('d-m-Y ', strtotime($LcBank->open_date));
                }else{
                    return $banks = "N/A";
                }
            })->addColumn('lc_expiry_date', function($LcBank) {
                if($LcBank->lc_expiry_date !=''){
                    return $banks =  date('d-m-Y ', strtotime($LcBank->lc_expiry_date));
                }else{
                    return $banks = "N/A";
                }
            })
            ->addColumn('lc_status', function($LcBank) {
                if ($LcBank->lc_status == 1) {
                    return $status ="Open";
                }elseif ($LcBank->lc_status == 2){
                    return $status = "IN Transit";
                }elseif ($LcBank->lc_status == 3){
                    return $status = "IN Bound";
                }elseif ($LcBank->lc_status == 4){
                    return $status = "Completed";
                }else{
                    return $status = "Cancel";
                }
            })->addColumn('action', function ($LcBank) {
                if($LcBank->today_doller !=''){
                    $action_column = '<a href="lcbank/'.$LcBank->id.'" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-eye-open"></i> DO Rate</a>';
                    $action_column .= '<a href="lcbank/'.$LcBank->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';

//                    $action_column .= '<form method="post" action="lcbank/delete" >';
//                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
//                    $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
//                    $action_column .= '<input type="hidden" value="21" name="perfomra_no" >';
//                    $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
//                    $action_column .= '<input type="hidden" value='.$LcBank->id.' name="name" >';
//                    $action_column .=  csrf_field();
//                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
//
                }else{
                    $action_column = '<a href="lcbank/'.$LcBank->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
//                    $action_column .= '<form method="post" action="lcbank/delete" >';
//                    $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
//                    $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
//                    $action_column .= '<input type="hidden" value="21" name="perfomra_no" >';
//                    $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
//                    $action_column .= '<input type="hidden" value='.$LcBank->id.' name="name" >';
//                    $action_column .=  csrf_field();
//                    $action_column .= '<input   value="Delete"  class="btn btn-xs btn-danger" type="submit"  onclick="return confirm(\'Are you sure you want to delete this record? \')"> </form>';
//
                }

                return $action_column;
            })->rawColumns(['pf_no','lc_type','bnk_id','lc_status','action','today_doller','transaction_type'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        if (! Gate::allows('erp_lcbank_create')) {
            return abort(401);
        }
        $Orders = Orders::where('status','1')->get();
        $Banks = BanksModel::all();
        return view('admin.lcbank.create',compact('Orders', 'Banks'));
    }

    public function create2(LcBankModel $lcBankModel){
        $orderid = $lcBankModel->pf_no;
        $order = Orders::find($orderid);
        $order_detail = $order->orderdetail;
        return view('admin.lcbank.create2',compact('lcBankModel','order_detail'));
    }

    public function shipmentSchedule(){
        $ComercialInvoiceModel = ComercialInvoiceModel::where('status','=','1')->get();
        return view('admin.lcbank.shipment',compact('ComercialInvoiceModel'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        if(is_array($request['pf_no'])){
            $prfn = implode(',',$request['pf_no']);
        }
        $transaction_type = $request['transaction_type'];
        if($transaction_type == 1){
            $parent_Id = $request['bnk_name'];
            $lc_type = $request['lc_type'];
        }else{
            $lc_type = $request['tt_types'];
            if($lc_type == 1){
                $parent_Id = $request['bnk_name'];
            }else{
                $Supplier = Orders::where('id',$request['pf_no'][0])->first();
                $parent_Id = $Supplier->sup_id;
            }

        }
        $lcid = LcBankModel::create([
            'transaction_type' => $transaction_type,
            'lc_type' => $lc_type,
            'delivery_term' => $request['delivery_term'],
            'pf_no' => $prfn,
            'bnk_id' => $parent_Id,
            'lc_amt' => $request['lcamt'],
            'lc_status' => '1',
            'dollar_rate' => $request['dollar_rate'],
            'description' => $request['description'],
            'created_by' => Auth::user()->id,
        ]);
        LcBankModel::where('id', $lcid->id)->update(array(
            'lc_no' => CoreAccounts::generateNumber($lcid->id),
        ));
        if(is_array($request['pf_no'])){
            for($i = 0;$i<count($request['pf_no']);$i++){
                $performa_id = $request['pf_no'][$i];
                if($performa_id!=''){
                    Orders::where('id', $performa_id)->update(array(
                        'status' => '2',
                        'confirm_status' => '0',
                    ));
                }
            }

        }

        $LcBankModel = LcBankModel::find($lcid->id);
        $lcno_ledger = $LcBankModel->lc_no;
        if ($transaction_type == 1) {
            $Tr_Name = 'LC-' .$lcno_ledger;
            $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_Lc_Transit')])->where(['parent_type' => $parent_Id])->first();
            $Assets_id = $TR_ledger->id;
            $laibilities_Group = Groups::where(['parent_id' => Config::get('constants.account_bank_cd_importer')])->where(['parent_type' => $parent_Id])->first();
            $Laiblity_name= 'Murahba-' . $lcno_ledger;
        }else{
            if($lc_type == 2){
                $Tr_Name = 'TT-' .$lcno_ledger;
                $TR_ledger = Groups::where(['parent_id' => Config::get('constants.accounts_tt_advance')])->where(['parent_type' => $parent_Id])->first();
                $Assets_id = $TR_ledger->id;
                $laibilities_Group = Groups::where(['parent_id' =>Config::get('constants.accounts_BME_importer')])->where(['parent_type' => $parent_Id])->first();
                $Laiblity_name = 'BME-' . $lcno_ledger;
            }else{
                $Tr_Name = 'TT-' .$lcno_ledger;
                $TR_ledger = Groups::where(['parent_id' => Config::get('constants.account_bank_contract')])->where(['parent_type' => $parent_Id])->first();
                $Assets_id = $TR_ledger->id;
            }
        }
        $Ledger = array(
            'name' => $Tr_Name,
            'group_id' => $Assets_id,
            'balance_type' => 'd',
            'opening_balance' => 0,
            'parent_type' => $lcid->id,
        );
        CoreAccounts::createLedger($Ledger);
        if($transaction_type == 2 && $lc_type == 1 ){

        }else{
            $Ledger2 = array(
                'name' => $Laiblity_name,
                'group_id' => $laibilities_Group->id,
                'balance_type' => 'c',
                'opening_balance' => 0,
                'parent_type' => $lcid->id,
            );
            CoreAccounts::createLedger($Ledger2);
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.lcbank.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Performastock = array();
        $count = 0;
        $LcBankModel = LcBankModel::find($id);
        $morahba_profit = $LcBankModel['murahabah_profit'];
        $today_doller = $LcBankModel['today_doller'];
        $total_lc_amount = $LcBankModel['total_lc_amount'];
        $total_days = $LcBankModel['total_days'];
        $margin_rate = $LcBankModel['margin_rate'];
        if (stristr($LcBankModel['pf_no'],',')){
            $order_no = explode(',',$LcBankModel['pf_no']);
        }else{
            $order_no = $LcBankModel['pf_no'];
        }
        if(is_array($order_no)){
            $Ordersproduct = OrderDetailModel::whereIn('perchas_id',$order_no)->get();

        }else{
            $Ordersproduct = OrderDetailModel::where('perchas_id',$order_no)->get();
        }
        if(count($Ordersproduct) > 0){
            foreach ($Ordersproduct as $Ordersproducts){
                $product_name = $Ordersproducts['product_id'];
                $product_qty = $Ordersproducts['qty'];
                $remain_qty = $Ordersproducts['remaining_qty'];
                $product_amount = $Ordersproducts['unit_price'];
                $total_amount = round($product_qty *$product_amount);
                $amount_rs = round($total_amount*$today_doller);
                $calculated_margin = round($amount_rs * $margin_rate / 100);
                $total_murahba = $amount_rs - $calculated_margin;
                //$murahba_profit = round(($morahba_profit*$amount_rs) / $total_lc_amount);
                $murahba_profit = round(((($total_murahba*$morahba_profit)/100)/365)*$total_days);
                $net_murahba = $total_murahba + $murahba_profit;
                $cost_per_unit = round($net_murahba / $product_qty);
                $balance_murahba = round($product_qty * $cost_per_unit);
                $Performastock[$count]['product_name'] = $Ordersproducts->productsModel->products_short_name. ' - '. $Ordersproducts->productsModel->item_code;
                $Performastock[$count]['total_qty'] = $product_qty;
                $Performastock[$count]['bal_qty'] = $remain_qty;
                $Performastock[$count]['amount'] = $product_amount;
                $Performastock[$count]['total_amount'] = $total_amount;
                $Performastock[$count]['amount_rs'] = $amount_rs;
                $Performastock[$count]['margin'] = $calculated_margin;
                $Performastock[$count]['total_murahba'] = $total_murahba;
                $Performastock[$count]['murahba_profit'] = $murahba_profit;
                $Performastock[$count]['net_murahba'] = $net_murahba;
                $Performastock[$count]['cost_per_unit'] = $cost_per_unit;
                $Performastock[$count]['balance_murahba'] = $balance_murahba;
                $count++;
            }
        }
        return view('admin.lcbank.cindex', compact('Performastock','LcBankModel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_lcbank_edit')) {
            return abort(401);
        }
        $LcBanks = LcBankModel::find($id);
        //  dd($LcBanks);
        $DolerLcModel = DolerLcModel::where('lc_id',$id)->get();
        if(stristr($LcBanks->pf_no,",")){
            $multiperforma = explode(",", $LcBanks->pf_no);
        }else{
            $multiperforma[] = $LcBanks->pf_no;
            
        }

         $Orders = Orders::where('status','1')->get();
        $Banks = BanksModel::all();
        $parentGroups = new LedgersTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
        $parentGroups->toList($parentGroups, -1);
        $Ledgers = $parentGroups->ledgerList;
        $MarginTable = CalculateMargin::where('lc_no',$id)->get();
        return view('admin.lcbank.edit', compact('MarginTable','LcBanks','Orders', 'Banks','multiperforma','Ledgers','DolerLcModel'));
    }

    public function lcamount($id)
    {
        $Orders = Orders::all();
        $Banks = BanksModel::all();
        $OrderDetailModel = OrderDetailModel::where('perchas_id',$id)->sum('amount');
        return view('admin.lcbank.create', compact('OrderDetailModel','Orders','Banks'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id){
       // dd($request->all());
        $test = LcBankModel::where('id', $id)->first();
        $prfn = explode(",", $test->pf_no);
        $arr1 = $request['pf_no'];
        $arr2 = $request['spf_no'];
        $result = array_diff($prfn, $arr2);

        $LcUpdate = Orders::whereIn('po_number', $result)->update([
            'status' => 1,
        ]);
        if(count($request['pf_no']) > 0){
            $merge = array_merge($arr1,$arr2);
            $newArray = implode(',',$merge);
        }else {
            $newArray = implode(',',$arr2);
        }
        
       
        
           
       
        $LcBankModel = LcBankModel::where('id', $id)->update([
            'delivery_term' => $request['delivery_term'],
            'murahabah_profit' => $request['mu_profit'],
            'lc_no' => $request['lc_no'],
            'pf_no' => $newArray,
            'open_date' => $request['open_date'],
            'description' => $request['description'],
            'lc_amt' => $request['totl_lc_amt'],
            'margin_rate' => $request['margin_rate'],
        ]);
        if(is_array($request['pf_no'])){
            for($i = 0;$i<count($request['pf_no']);$i++){
                $performa_id = $request['pf_no'][$i];
                if($performa_id!=''){
                    Orders::where('id', $performa_id)->update(array(
                        'status' => '2',
                    ));
                }
            }
        }
        $transaction_type = $request['transaction_type'];
        $parent_Id = $request['bank_id'];
        $lc_type = $request['lc_type'];
        $lcno_ledger = $request['lc_no'];
        if ($transaction_type == 1) {
            $Tr_Name = 'LC-' . $lcno_ledger;
            $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_Lc_Transit')])->where(['parent_type' => $parent_Id])->first();
            $Assets_id = $TR_ledger->id;
            $laibilities_Group = Groups::where(['parent_id' => Config::get('constants.account_bank_cd_importer')])->where(['parent_type' => $parent_Id])->first();
            $Laiblity_name = 'Murahba-' . $lcno_ledger;
            Ledgers::where('parent_type',$id)->where('group_id',$Assets_id)->update([
                'name' => $Tr_Name,
                'updated_by' => Auth::user()->id,
            ]);
            Ledgers::where('parent_type', $id)->where('group_id', $laibilities_Group->id)->update([
                'name' => $Laiblity_name,
                'updated_by' => Auth::user()->id,
            ]);
        }
        flash('Record has been created successfully.')->success()->important();
        return redirect()->route('admin.lcbank.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $LcBanks = LcBankModel::findOrFail($id);
        $LcBanks->delete();
        return redirect()->route('admin.lcbank.index');
    }

    public function delete(Request $request)
    {
        $data = $request->all();
        $LcBanks = LcBankModel::findOrFail($data['name']);
        $LcBanks->delete();
        flash('Record has been deleted successfully.')->success()->important();
        return redirect()->route('admin.lcbank.index');
    }

    public function indexGrImport(){

        if (! Gate::allows('erp_lcbank_manage')) {
            return abort(401);
        }
        $cInvoice = ComercialInvoiceModel::where('status', 1)->get();

        return view('admin.lcbank.grimport',compact('cInvoice'));
    }

    public static function invoiceImport($invoice_id)
    {
        $performastock =  PerformaStockModel::where('ci_id',$invoice_id)->where('balanace_qty','>',0)->get();
        $ComercialInvoiceModel =  ComercialInvoiceModel::findOrFail($invoice_id);
        $type = LcBankModel::where('id', $ComercialInvoiceModel->lcno)->first();
        $Banks = BanksModel::all();
        $parentGroups = new LedgersTree();
        $parentGroups->current_id = -1;
        $parentGroups->build(0);
        $parentGroups->toList($parentGroups, -1);
        $Ledgers = $parentGroups->ledgerList;

        return view('admin.lcbank.invoicelist',compact('performastock','ComercialInvoiceModel', 'type', 'Ledgers'));
    }

    public function storeReleaseStock(Request $request)
    {
        $data = $request->all();
        $inventory = array();
        $Ledger_entry = array();
        $counter = 1;
        $dolerrate = LcBankModel::where('id', $request['lc_id'])->first();
        $parent_Id = $dolerrate->bnk_id;
        if($dolerrate->transaction_type == 1){
            foreach($data['product_detail_id'] as $key=>$value)
            {
                if($data['seg_qty'][$key]){

                    $segregate_qty = $data['seg_qty'][$key] * $data['rel_qty'][$key];
                    $segregate_unit_price = round($data['unit_price_i'][$key] / $data['seg_qty'][$key],4);
                }else{
                    $segregate_qty = $data['rel_qty'][$key];
                    $segregate_unit_price = $data['unit_price_i'][$key];
                }
                GrBufferModel::create([
                    'c_invoice'    =>$data['lc_no'],
                    'product_id'   =>$data['product_detail_id'][$key],
                    'rel_qty'       =>$segregate_qty,
                    'warehouse_id'  =>$data['warehouse'],
                    'import_date'  =>$data['import_date'],
                    'unit_price'   =>$segregate_unit_price,
                ]);
            }
            for($a=0;$a<count($data['product_detail_id']);$a++){
                if($data['rel_qty'][$a] == 0){
                    continue;
                }
                PerformaStockModel::where('id',$data['row_id'][$a])->update([
                    'balanace_qty' => $data['bal_qty'][$a],
                    'updated_by'=> Auth::user()->id,
                ]);
            }
            $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_bank_cd_importer')])->where(['parent_type' => $parent_Id])->first();
            $Assets_id = $TR_ledger->id;
            $transit_ledger = Ledgers::where('parent_type',$dolerrate->id)->where('group_id',$Assets_id)->first();
            $MurahbaPaid = array(
                "number" => "??????",
                "voucher_date" =>  date('Y-m-d'),
                "employee_id" => Auth::user()->id,
                "narration" => "Murahbah has been paid",
                "entry_type_id" => "1",
                "entry_items" => array(
                    1 => [
                        'ledger_id' => $transit_ledger->id,
                        'dr_amount' => $data['paid_amount_total'],
                        'cr_amount' => 0,
                        'narration' => 'Murahbah has been paid',
                    ],
                    2 => [
                        'ledger_id' =>$data['cgroup_id'],
                        'dr_amount' => 0,
                        'cr_amount' => $data['paid_amount_total'],
                        'narration' => 'Murahbah has been paid',
                    ],
                ),
                "diff_total" => 0,
                "dr_total" => $data['paid_amount_total'],
                "cr_total" => $data['paid_amount_total'],
            );
            CoreAccounts::createJEntry($MurahbaPaid);
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.grimport');
        }else{
                if($dolerrate->lc_type == 1){
                    $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_bank_contract')])->where(['parent_type' => $parent_Id])->first();
                    $Assets_id = $TR_ledger->id;
                    $transit_ledger = Ledgers::where('parent_type',$dolerrate->id)->where('group_id',$Assets_id)->first();
                }else{
                    $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.accounts_tt_advance')])->where(['parent_type' => $parent_Id])->first();
                    $Assets_id = $TR_ledger->id;
                    $transit_ledger = Ledgers::where('parent_type',$dolerrate->id)->where('group_id',$Assets_id)->first();
                }
            for($i=0;$i<count($data['product_detail_id']);$i++){
                if($data['rel_qty'][$i] == 0){
                    continue;
                }
                if($data['seg_qty'][$i]){

                    $segregate_qty = $data['seg_qty'][$i] * $data['rel_qty'][$i];
                    $segregate_unit_price = round($data['unit_price'][$i] / $data['seg_qty'][$i],4);
                }else{
                    $segregate_qty = $data['rel_qty'][$i];
                    $segregate_unit_price = $data['unit_price'][$i];
                }
                $product_ledger = ProductsModel::getStockGroup($data['product_detail_id'][$i]);
                $inventory[$counter]['commercial_invoice'] = $data['lc_no'];
                $inventory[$counter]['product_id'] = $data['product_detail_id'][$i];
                $inventory[$counter]['rel_qty'] = $segregate_qty;
                $inventory[$counter]['`import_da`te'] = $data['import_date'];
                $inventory[$counter]['unit_price'] = $segregate_unit_price;
                $inventory[$counter]['branch_id'] = $data['warehouse'];
                $Ledger_entry[$counter]['ledger_id'] = $product_ledger;
                $Ledger_entry[$counter]['dr_amount'] = round($data['rel_qty'][$i]*$data['unit_price'][$i],4);
                $Ledger_entry[$counter]['cr_amount'] = 0;
                $Ledger_entry[$counter]['narration'] = 'Stock Transfer from Transit to inventory';
                $counter++;
            }
            $result = StockItemsController::importInventory($inventory);
            for($a=0;$a<count($data['product_detail_id']);$a++){
                if($data['rel_qty'][$a] == 0){
                    continue;
                }
                PerformaStockModel::where('id',$data['row_id'][$a])->update([
                    'balanace_qty' => $data['bal_qty'][$a],
                    'updated_by'=> Auth::user()->id,
                ]);
            }
            if($transit_ledger->id && $request['paid_amount_total'] !=''){
                $vencounter = count($Ledger_entry);
                $vencounter = $vencounter+1;
                $Ledger_entry[$vencounter]['ledger_id'] = $transit_ledger->id;
                $Ledger_entry[$vencounter]['dr_amount'] = 0;
                $Ledger_entry[$vencounter]['cr_amount'] = $request['paid_amount_total'];
                $Ledger_entry[$vencounter]['narration'] = 'Stock Transfer from Transit to inventory';
                $entries2 = array(
                    "number" => "??????",
                    "voucher_date" => date('Y-m-d'),
                    "employee_id" => Auth::user()->id,
                    "narration" => 'Stock Transfer from Transit to inventory',
                    "entry_type_id" => "1",
                    "entry_items" => $Ledger_entry,
                    "diff_total" => "0",
                    "dr_total" => $request['paid_amount_total'],
                    "cr_total" => $request['paid_amount_total'],
                    "cheque_no" => null,
                    "invoice_no" => null,
                );
                CoreAccounts::createJEntry($entries2);
            }else{
                flash('Transaction not completed successfully.')->error()->important();
                return redirect()->back();
            }
            flash('Record has been created successfully.')->success()->important();
            return redirect()->route('admin.grimport');
        }
    }


    public function marginCalculate(Request $request){

        if($request['murabha_id'] === 'true'){
            $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_bank_cd_importer')])->where(['parent_type' => $request['banksid']])->first();
            $narration = "Murahbah paid against LC";
        }else{
            $TR_ledger =  Groups::where(['parent_id' => Config::get('constants.account_Lc_Transit')])->where(['parent_type' => $request['banksid']])->first();
            $narration =  $request['margin_rate'].'% margin has been paid.';
        }
        $LedgerDr = Ledgers::where('parent_type',$request['dr_id'])->where('group_id',$TR_ledger->id)->first();
        $entries2 = array(
            "number" => "??????",
            "voucher_date" =>  date('Y-m-d'),
            "employee_id" => Auth::user()->id,
            "narration" => $narration,
            "entry_type_id" => "1",
            "entry_items" => array(
                1 => [
                    'ledger_id' => $LedgerDr->id,
                    'dr_amount' => $request['margin'],
                    'cr_amount' => 0,
                    'narration' => $narration,
                ],
                2 => [
                    'ledger_id' => $request['cgroup_id'],
                    'dr_amount' => 0,
                    'cr_amount' => $request['margin'],
                    'narration' => $narration,
                ],
            ),
            "diff_total" => "0",
            "dr_total" => $request['margin'],
            "cr_total" => $request['margin'],
            "cheque_no" => null,
            "cheque_date" => null,
            "invoice_no" => null,
            "invoice_date" => null,
        );
        // dd($request['margin_rate'], $request['margin']);

        if($request['murabha_id'] === 'true') {
           LcBankModel::where('id', $request['dr_id'])->update([
                'murahabah_rate' =>   $request['margin_rate'],
                'murahabah_amount' => $request['margin'],
            ]);
        }else{
            $check = array(
                "lc_no" => $request['dr_id'],
                'account_rate' => $request['account_rate'],
                'margin_rate' => $request['margin_rate'],
                'margin_amount' => $request['margin'],
                'updated_by' => Auth::user()->id,
                'created_by' => Auth::user()->id,
            );
            CalculateMargin::updateMargin($check);
        }
        CoreAccounts::createJEntry($entries2);
        echo "ok";
        return;
    }

    public function rateFluctuation(Request $request){
        DolerLcModel::create([
            'lc_id'=>$request['dr_id'],
            'rate'=>$request['Flut_rate'],
        ]);
        echo "ok";
        return;

    }
}
