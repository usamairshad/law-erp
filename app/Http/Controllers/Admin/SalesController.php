<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\SalesModel;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreSalesRequest;


use Illuminate\Support\Facades\Session;
use App\Models\Admin\Customers;
use App\Models\Admin\Branches;
use App\Models\Admin\Employees;
use App\Models\Admin\Departments;
use App\Models\Admin\StockItemsModel;
use App\Models\Admin\SuppliersModel;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\SalesDetialModel;

use App\Helpers\CoreAccounts;
use App\Models\Admin\Ledgers;
use App\Models\Marketing\SalePipelines;


use DB;


use Validator;
use Auth;
use Config;


class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (! Gate::allows('erp_sales_manage')) {
            return abort(401);
        }

        $Sales = SalePipelines :: saleorders();

        return view('admin.sales.index', compact('Sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_sales_manage')) {
            return abort(401);
        }
        $Sales = SalesModel::all();

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');
        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');
        // Get All Customers
        $Customers = Customers::pluckActiveOnly();
        $Customers->prepend('Select a Customers', '');
        $Customers = Customers::all();

        // Get All Employees
        $Employees = Employees::all();

        // Get All Suppliers
        $Suppliers = SuppliersModel::all();

        // Get All StockItems
        $StockItems = StockItemsModel::all();

        // Get All Products
        $Products = ProductsModel::all();

        // Get All Branches
        $Branches = Branches::all();

        $SalesDetail = SalesDetialModel::all();

        $Salno = SalesModel::Salno();
//        echo '<pre>';
//        print_r($Salno);
//        echo '</pre>';
//        exit;

        return view('admin.sales.create', compact( 'Branches', 'Employees', 'Salno','Department', 'Customers','Products','StockItems','Suppliers', 'DefaultCurrency'));
    }

    public function produtsdetail($id)
    {
        $results = StockItemsModel::where('sup_id',$id)->get();
        return view('admin.sales.produtsdetail')->with('results',$results)->with('sup_id',$id);
    }

    public function produtscol($id)
    {
        $produtscol = StockItemsModel::where('id',$id)->first();
        //dd($produtscol);exit;
        return view('admin.sales.produtscol')->with('produtscol',$produtscol);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(StoreSalesRequest $request)
    {
        if (! Gate::allows('erp_users_manage')) {
            return abort(401);
        }
        //$response = CoreAccounts::createSale($request->all());
        $Salnoaval = $request['sal_no'];

        $Sales = CoreAccounts::getConfigGroup(Config::get('constants.accounts_stock_items_head_setting_id'));
        $Ledger = array(
            'name' => $Salnoaval,
            'group_id' => $Sales['group']->id,
            'balance_type' => 'd',
            'opening_balance' => 0,
        );
        $response = CoreAccounts::createLedger($Ledger);

        //dd($request);exit;



        $sale_id = SalesModel::create([
            'group_id' => $Sales['group']->id,
            'sal_no' => $Salnoaval, //$Salnoaval,
            'sal_date'=> $request['sal_date'],
            'valid_upto'=> $request['valid_upto'],
            'customer_id'=> $request['customer_id'],
            'branch_id'=> $request['branch_id'],
            'employee_id'=> $request['employee_id'],
            'sup_id'=> $request['sup_id'],
            'description'=> $request['description'],
            'remarks'=> $request['remarks'],
            'discount'=> $request['discount'],
            'tax'=> $request['tax'],
            'final_amount'=> $request['final_amount'],
            'total_amount'=> $request['total_amount'],
            'total_balance'=> $request['total_balance'],
            'gst'=> $request['gst'],
            'shipping_charges'=> $request['shipping_charges'],
            'created_by' => 0,
            'updated_by' => 0,
            'deleted_by' => 0,
        ]);
        //dd($sale_id->id);exit;

        foreach ($request->st_name as $key=>$value) {
            $id = $request->id[$key];
            $salesdetial = SalesDetialModel::create([
                'sal_id' => $sale_id->id,
                'sal_st_name' => $request->st_name[$key],
                'sal_st_unit_price' => $request->st_unit_price[$key],
                'sale_quantity' => $request->sale_quantity[$key],
                'sale_amount' => $request->sale_amount[$key],
           ]);

            $StockItems = StockItemsModel::findOrFail($id);
            $opening_stock = $StockItems->opening_stock;
            //print_r($opening_stock);
            $openingstockremaining = $opening_stock-$request->sale_quantity[$key];

            $StockItems->update([
                'opening_stock' => $openingstockremaining,
            ]);
        }



        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.sales.index');
    }

    /**
     * Activate Sales from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        if (! Gate::allows('erp_sales_active')) {
            return abort(401);
        }

        $Sales = SalesModel::findOrFail($id);
        $Sales->update(['status' => 1]);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.sales.index');
    }

    /**
     * Inactivate Sales from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {
        if (! Gate::allows('erp_sales_inactive')) {
            return abort(401);
        }

        $Sales = SalesModel::findOrFail($id);
        $Sales->update(['status' => 0]);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.sales.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Sales = SalesModel::find($id);

        return view('admin.sales.home', compact('Sales'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        $Sales = SalesModel::findOrFail($id);

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
        $Branches->prepend('Select a Branch', '');

        // Get All Departments
        $Departments = Departments::pluckActiveOnly();
        $Departments->prepend('Select a Department', '');

        // Get All Customers
        $Customers = Customers::all();

        // Get All Employees
        $Employees = Employees::all();

        $Suppliers = SuppliersModel::all();
        $StockItems = StockItemsModel::all();
        $Products = ProductsModel::all();
        $Branches = Branches::all();
        //dd($request);exit;

        $Salno = SalesModel::Salno();
//        echo '<pre>';
//        print_r($Salno);
//        echo '</pre>';
//        exit;


        return view('admin.sales.edit', compact( 'Sales','Branches', 'Employees', 'Salno','Department', 'Customers','Products','StockItems','Suppliers', 'DefaultCurrency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        //dd($request);exit;
        $Sales = SalesModel::findOrFail($id);
        $Sales->update([

            'sal_no' => $request['sal_no'], //$Salnoaval,
            'sal_date'=> $request['sal_date'],
            'valid_upto'=> $request['valid_upto'],
            //'customer_id'=> $request['customer_id'],
            'branch_id'=> $request['branch_id'],
            'employee_id'=> $request['employee_id'],
            //'stock_item_id'=> $request['stock_item_id'],
            //'product_id'=> $request['product_id'],
            //'sup_id'=> $request['sup_id'],
            'description'=> $request['description'],
            'remarks'=> $request['remarks'],
            'discount'=> $request['discount'],
            'tax'=> $request['tax'],
            'final_amount'=> $request['final_amount'],
            'total_amount'=> $request['total_amount'],
            'total_balance'=> $request['total_balance'],
            'gst'=> $request['gst'],
            'shipping_charges'=> $request['shipping_charges'],
            'created_by' => 0,
            'updated_by' => 0,
            'deleted_by' => 0,
        ]);

        return redirect()->route('admin.sales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        $Sales = SalesModel::findOrFail($id);
        $Sales->delete();

        return redirect()->route('admin.sales.index');
    }

    /**
     * Delete all selected Entry at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('erp_sales_mass_destroy')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = SalesModel::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
