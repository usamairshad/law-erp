<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBranchesRequest;
use App\Http\Requests\Admin\UpdateBranchesRequest;
use App\Models\Admin\Branch;
use App\Models\Admin\City;
use App\Models\Admin\Country;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Ledgers;
use App\Helpers\CoreAccounts;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\Classes;
use App\Models\Academics\Program;
use App\Models\Academics\Course;
use App\Models\Academics\Session;
use Auth;
use Config;
use App\Helpers\Helper;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $Branches = Branches::OrderBy('created_at','desc')->get();
         $programs= Program::orderBy('id','asc')->get();
         $courses= Course::all();

        return view('admin.board.programs',compact('programs','courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd("asdas");
        return view('admin.board.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function compulsory($id)
    {
        $program = Program::where('id','=',$id)->first();
        $courses= Course::all();
        return view('admin.board.add_board_program',compact('courses','program'));
    }
    public function assignBoardProgram()
    {
        // dd("ASdas");
        $programs= Program::all();
        return view('admin.board.assign-board-program',compact('programs'));

    }
    public function store(Request $request)
    {

        $this->validate($request, [
            
            'name' => 'required | unique:programs',

        ]);
        $session_id= Session::where('active_status',1)->value('id');
        // $rules['branch_code']  =  'required|unique:erp_branches';
        // $rules['ntn']  =  'required|unique:erp_companies';
        // $validator = Validator::make($request->all(), $rules);
        // if ($validator->fails()) {
        //     $request->flash();
        //     return redirect()->back()
        //         ->withErrors($validator->errors())
        //         ->withInput();
        // }

        $input= $request->all();
        for($i = 0; $i < count($input['name']) ; $i++) { 
            $arr= new Program;
            $arr->name=$input['name'][$i];
            $arr->program=$input['name'][$i];
            $arr->description=$input['description'][$i];
            $arr->save();
        }
        $programs= Program::all();

        return redirect()->route('admin.boards.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $boards=Board::all();
    
        $programs=Program::all();

        return view('admin.board.index',compact('boards','programs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $programs=Program::find($id);
        return view('admin.board.program-edit',compact('programs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Program::where('id',$request->program_id)->update([
            'name'=>$request->name,
            'description'=>$request->description
        ]);

        return redirect()->route('admin.boards.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignBranchProgram()
    {
        $branches= Branches::all();
        $boards= Branches::all();
        return view('admin.branches.assign-branch-program');
    }

    public function assignBoardPrograms($id)
    {

        $branch_id = $id;

        $programs= Program::all();
        $boards= Board::all();

        return view('admin.branches.assign-board-program',compact('branch_id' ,'programs' , 'boards'));

    }
    public function addBoardProgram(Request $request)
    {
       
          $this->validate($request, [
            'name' => 'required |unique:boards',
            
        ]);
          
        $input=$request->all();
        foreach ($input['name'] as $key => $value) {
            $board= Board::create([
                'name' => $value
            ]);
            $programs= $input['programs'][$key];
            
                foreach ($programs as $key => $program) {
                    BoardProgram::create([
                    'board_id' => $board->id,
                    'program_id' => $program,
                ]);
            }
            
        }
        return redirect()->route('admin.board-program');

    }

    public function loadBoardPrograms(Request $req)
    {
      $programs = BoardProgram::where('board_id', $req->id)->get();

      $program_data=[];
        foreach( $programs as $key => $program )
        {
            $data = Program::where('id',$program->program_id)->get();
            array_push($program_data, $data);
        }
      
      return $program_data;
    }

    public function loadBoardProgramClass(Request $request)
    {
        // \Log::info($request);
        $class_id=BoardProgramClass::where('board_id', $request->board_id)->where('program_id',$request->program_id)->get();
        $class_data=[];
        foreach ($class_id as $key => $value) {
             $data=Classes::where('id',$value->class_id)->get();
             array_push($class_data, $data);
        }

        return $class_data;
    }   

    public function viewBoardPrograms($id)
    {
        $board_program=BoardProgram::where('board_id',$id)->get();

        $board_programs=[];
        foreach ($board_program as $key => $value) {
           $programs= Program::where('id',$value['program_id'])->first();

           array_push($board_programs, $programs);
        }
        

        return view('admin.board.view-board-program',compact('board_programs'));
    }

    public function viewBoardProgramsClasses()
    {

        
        $board_program_classes= BoardProgramClass::all();
        
        return view('admin.board.view-board-program-classes',compact('board_program_classes'));
    }
    public function addExistingBoardProgram(Request $request)
    {
        $input= $request->all();
        //$program_id= Program::orderBy('created_at','DESC')->first();

        BoardProgram::create([
            'board_id'=>$input['board_id'],
            'program_id'=>$input['program_id'],
        ]);

        return redirect()->route('admin.boards.show',1);
    }

    public function boardProgramshow()
    {
        $boards=Board::all();
    
        $programs=Program::all();

        return view('admin.board.index',compact('boards','programs'));
    }

    public function test(Request $request)
    {
       
          $this->validate($request, [
            'name' => 'required |unique:boards',
            
        ]);
          
        $input=$request->all();
        foreach ($input['name'] as $key => $value) {
            $board= Board::create([
                'name' => $value
            ]);
            $programs= $input['programs'][$key];
            
                foreach ($programs as $key => $program) {
                    BoardProgram::create([
                    'board_id' => $board->id,
                    'program_id' => $program,
                ]);
            }
            
        }
        return redirect()->route('admin.board-program');

    }
}
