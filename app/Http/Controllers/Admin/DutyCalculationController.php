<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\DutyModel;
use App\Models\Admin\DutyCalculation;
use App\Models\Admin\LcBankModel;
use App\Models\Admin\OrderDetailModel;
use App\Models\Admin\ProductsModel;
use App\Models\Admin\LcDutyModel;
use Auth;

class DutyCalculationController extends Controller
{
    public function addDuty(){

        $DutyModel = DutyModel::pluckActiveOnly();
        $DutyModel->prepend('Select a Duty', '');
        $InventoryItems= array();
        return view('admin.duty_calculation.create', compact('Groups','Branches','InventoryItems','DutyModel'));
    }

    public function storeDuty(Request $request){
        $data = $request->all();

        foreach($data['InventoryItems']['duty_name'] as $key=>$value)
        {
            if ($key!= '########'){

                $stock_item =  DutyCalculation::create([
                    'duty_id'              => $data['InventoryItems']['duty_name'][$key],
                    'percentage'           => $data['InventoryItems']['percent'][$key],
                    'created_by'           => Auth::user()->id,
                ]);

            }   
        }

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.account_reports.duty_report');
    }

    public function getDutyReport(){
       $duties = DutyCalculation::where('duty_id', '!=', null)->where('percentage', '!=', null )->get();
       return view('admin.duty_calculation.index', compact('duties'));
    }

    public function destroyy($id)
    {
        $duty = DutyCalculation::findOrFail($id);
         $duty->delete();
         flash('Record has been Deleted successfully.')->success()->important();
        return redirect()->route('admin.account_reports.duty_report');
    }

    public function calculateDuty(){
        $Lc = LcBankModel::all();
        $duties = DutyCalculation::where('duty_id', '!=', null)->where('percentage', '!=', null )->get();
        return view('admin.duty_calculation.duty_calculation_report', compact('Lc', 'duties'));
    }

    public function getProducts(Request $request){

        $lc = LcBankModel::where('id', $request->id)->first();
        $pf_no = $lc->pf_no;
        $pro = explode(",", $pf_no);
        $data1 = OrderDetailModel::whereIn('perchas_id', $pro)->pluck('product_id');
        $data = ProductsModel::whereIn('id', $data1)->get();
        return response()->json($data);
    }

    public function getProductPrices(Request $request){

       $data = OrderDetailModel::where('product_id', $request->product_id)->first();
        return response()->json($data);
    }

    public function DutyDemand(){
        $Lc = LcBankModel::all();
        return view('admin.duty_calculation.duty_demand_report', compact('Lc'));
    }

    public function getProductlist(Request $request){
       // dd($request);
        $Lc = LcBankModel::all();
        $lc = LcBankModel::where('id', $request->lc_bank)->first();
        $pro = explode(",", $lc->pf_no);
        $data = OrderDetailModel::whereIn('perchas_id', $pro)->get();
        $lc_duty =  LcDutyModel::groupBy('duties')
                                ->selectRaw('sum(amount) as sum, duties')
                                //->whereRaw('ledger_id', '=', $request->lc_bank)
                                ->get();
       return view('admin.duty_calculation.demand', compact('Lc','data', 'lc_duty'));
 
    }
}
