<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\Admin\GoodsIssueModel;
use App\Models\Admin\StockItemsModel;
use App\Models\Admin\Ledgers;
use App\Models\Admin\GoodsIssueDetailModel;
use App\Models\Admin\Branches;
use App\Models\Admin\SuppliersModel;
use App\Models\Admin\Warehouse;
use Auth;

class GoodsIssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('erp_goodsissue_manage')) {
            return abort(401);
        }
        $GoodsIssueModel = GoodsIssueModel::all();
        return view('admin.goodsissue.index', compact('GoodsIssueModel'));
    }


    public function datatables()
    {
        $GoodsIssueModel = GoodsIssueModel::all();
        return  datatables()->of($GoodsIssueModel)
            -> addColumn('branch_id', function($GoodsIssueModel) {
                return  $GoodsIssueModel->branches->name;
            })
            ->addColumn('sup_id', function($GoodsIssueModel) {
                return  $GoodsIssueModel->suppliersModel->sup_name;
            })
            ->addColumn('created_at', function($GoodsIssueModel) {
                return  date('d-m-Y  h:m a', strtotime($GoodsIssueModel->created_at));
            })
            ->addColumn('action', function ($GoodsIssueModel) {
                $action_column  = '<a href="goodsissue/'.$GoodsIssueModel->id.'" class="btn btn-xs btn-primary">View</a> </br>';
//                $action_column = '<a href="goodsissue/'.$GoodsIssueModel->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a>';
//                $action_column .= '<form method="post" action="goodsissue/delete" >';
//                $action_column .= '<input type="hidden" value="DELETE" name="_methode" >';
//                $action_column .= '<input type="hidden" value="12" name="suplier_id" >';
//                $action_column .= '<input type="hidden" value="21" name="products_name" >';
//                $action_column .= '<input type="hidden" value="000-00" name="po_date" >';
//                $action_column .= '<input type="hidden" value='.$GoodsIssueModel->id.' name="name" >';
//                $action_column .=  csrf_field();
                return $action_column;
            })
             ->rawColumns(['branch_id','sup_id','created_at','action'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('erp_goodsissue_create')) {
            return abort(401);
        }
        $StockItems = StockItemsModel::all();
        $Branches = Warehouse::all();
        $Suppliers = SuppliersModel::all();
        return view('admin.goodsissue.create', compact('Branches','StockItems','Suppliers'));
    }




    public function stockrecord($id)
    {
        $StockItems = StockItemsModel::find($id);
        return view('admin.goodsissue.performa', compact('StockItems'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('erp_goodsissue_create')) {
            return abort(401);
        }
        //dd($request->all());
        $GoodsIssueModel = GoodsIssueModel::create([
            'branch_id'             => $request['branch_id'],
            'sup_id'                => $request['sup_id'],
            'created_by'            => Auth::user()->id,
        ]);
        foreach($request['InventoryItems']['product_id'] as $key=>$value)
        {
            $getqty = StockItemsModel::where('id',$request['InventoryItems']['product_id'][$key])->first();
            $total_stock = $getqty->closing_stock;
            $st_name = $getqty->st_name;
            $st_unit_price = $getqty->st_unit_price;
            GoodsIssueDetailModel::create([
                'gissue_id'         => $GoodsIssueModel->id,
                'product_id'        => $st_name,
                'quantity'          => $request['InventoryItems']['qunity'][$key],
                'created_by'        => Auth::user()->id,
            ]);
            $remianing_stock = $total_stock - $request['InventoryItems']['qunity'][$key];
            $newstock_value = $remianing_stock*$st_unit_price;
            $updatestock = StockItemsModel::where('id',$request['InventoryItems']['product_id'][$key])->update(array(
                'closing_stock' => $remianing_stock,
            ));

        }
        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.goodsissue.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('erp_goodsissue_detail')) {
            return abort(401);
        }
        $GoodsIssueModel = GoodsIssueModel::findOrFail($id);
        $GoodsIssueDetailModel = GoodsIssueDetailModel::where('gissue_id',$GoodsIssueModel->id)->get();
        return view('admin.goodsissue.orderdisplay', compact('GoodsIssueDetailModel'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('erp_goodsissue_edit')) {
            return abort(401);
        }
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('erp_goodsissue_edit')) {
            return abort(401);
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('goodsissue_destroy')) {
            return abort(401);
        }
        //
    }
}
