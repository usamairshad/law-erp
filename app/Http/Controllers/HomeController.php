<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Admin\Branches;
use App\Models\Admin\Customers;
use App\Models\Admin\Employees;
use App\Models\Admin\Entries;
use App\Models\Admin\Groups;
use App\Models\Admin\LcBankModel;
use App\Models\Admin\Ledgers;
use App\Models\Admin\Orders;
use App\Models\Admin\ProductMdModel;
use App\Models\Admin\ProductsModel;
use App\Models\HRM\CandidateForm;
use App\Models\Admin\SalesDetialModel;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\Schedule;
use App\Models\JobPost;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('GM Finance'))
        {
           return redirect()->route('gmdashboard');
        }
        else
        {
            $Counts = array(
                'orders' => 0,
                'lcbank' => 0,
                'groups' => 0,
                'ledgers' => 0,
                'entries' => 0,
                'branches' => 0,
                'employees' => 0,
                'customers' => 0,
                'products' => 0,
                'productmodels' => 0,
            );
            $Sales= 0;
            $stock = 0;

            /*
             * Accounts Dashboard
             */

    //        $Sales = SalesDetialModel::orderBy('created_at', 'asc')
    //            ->take(5)
    //            ->get();
    //
    //        $monthlySaledata = DB::table('salesinvoice')->select(DB::raw(
    //            "SUM(IF(month = 'Jan', total, 0)) AS 'Jan',
    //        SUM(IF(month = 'Feb', total, 0)) AS 'Feb',
    //        SUM(IF(month = 'Mar', total, 0)) AS 'Mar',
    //        SUM(IF(month = 'Apr', total, 0)) AS 'Apr',
    //        SUM(IF(month = 'May', total, 0)) AS 'May',
    //        SUM(IF(month = 'Jun', total, 0)) AS 'Jun',
    //        SUM(IF(month = 'Jul', total, 0)) AS 'Jul',
    //        SUM(IF(month = 'Aug', total, 0)) AS 'Aug',
    //        SUM(IF(month = 'Sep', total, 0)) AS 'Sep',
    //        SUM(IF(month = 'Oct', total, 0)) AS 'Oct',
    //        SUM(IF(month = 'Nov', total, 0)) AS 'Nov',
    //        SUM(IF(month = 'Dec', total, 0)) AS 'Dec',
    //        SUM(total) AS Yearly_Sale"
    //        ))->from(DB::raw(
    //            ' (SELECT
    //        DATE_FORMAT(invoice_date, "%b") AS month,
    //        SUM(total_amount) as total
    //        FROM salesinvoice
    //        WHERE invoice_date <= NOW() and invoice_date >= Date_add(Now(),interval - 12 month)
    //        GROUP BY DATE_FORMAT(invoice_date, "%m-%Y")) as sub'
    //        ))
    //            ->get();
    //
    //        $monthlyPurchasedata = DB::table('orders')->select(DB::raw(
    //            "SUM(IF(month = 'Jan', total, 0)) AS 'Jan',
    //        SUM(IF(month = 'Feb', total, 0)) AS 'Feb',
    //        SUM(IF(month = 'Mar', total, 0)) AS 'Mar',
    //        SUM(IF(month = 'Apr', total, 0)) AS 'Apr',
    //        SUM(IF(month = 'May', total, 0)) AS 'May',
    //        SUM(IF(month = 'Jun', total, 0)) AS 'Jun',
    //        SUM(IF(month = 'Jul', total, 0)) AS 'Jul',
    //        SUM(IF(month = 'Aug', total, 0)) AS 'Aug',
    //        SUM(IF(month = 'Sep', total, 0)) AS 'Sep',
    //        SUM(IF(month = 'Oct', total, 0)) AS 'Oct',
    //        SUM(IF(month = 'Nov', total, 0)) AS 'Nov',
    //        SUM(IF(month = 'Dec', total, 0)) AS 'Dec'"
    //        ))->from(DB::raw(
    //            ' (SELECT
    //        DATE_FORMAT(po_date, "%b") AS month,
    //        SUM(total_order_amt) as total
    //        FROM orders
    //        WHERE po_date <= NOW() and po_date >= Date_add(Now(),interval - 12 month)
    //        GROUP BY DATE_FORMAT(po_date, "%m-%Y")) as sub'
    //        ))
    //            ->get();

            /*
            SELECT o.`product_id`,sum(o.qty),p.products_name,p.cat_id
    FROM `orderdetail` as o
    Join products as p on p.id=o.`product_id`
    GROUP By p.cat_id
            */
    //
    //        $stock = DB::table('orderdetail as o')
    //            ->selectRaw('o.`product_id`,sum(o.qty) As sum,p.products_name,p.cat_id,sc.cat_name AS cat_name')
    //            ->join('products as p', 'p.id', '=', 'o.product_id')
    //            ->join('stockcategory as sc', 'sc.id', '=', 'o.product_id')
    //            ->groupBy('p.cat_id')->get();
    //       /* dd($stock);*/
    //
    ////
    ////        $Purchasedata = (array)$monthlyPurchasedata[0];
    ////        $Purchasedatakeys   = array_keys($Purchasedata);
    ////        $Purchasedatavalues = array_values($Purchasedata);
    ////        $jsonPurchasedatakeys = json_encode($Purchasedatakeys);
    ////        $jsonPurchasedatavalues = json_encode($Purchasedatavalues);
    ////        $test = (array)$monthlySaledata[0];
    //        $lifeSpanArray = array();
    //
    //
    //        foreach ($test as $key => $val) {
    //            $month = $key;
    //            $sale = $val;
    //            $lifeSpanArray[] = array('month' => $month, 'salevalue' => $sale);
    //        }
    //        $testaray = json_encode($lifeSpanArray);

            $Counts['orders'] = Orders::count();
            $Counts['lcbank'] = LcBankModel::count();
            $Counts['groups'] = Groups::count();
            $Counts['ledgers'] = Ledgers::count();
            $Counts['entries'] = Entries::count();
            $Counts['branches'] = Branches::count();
            // $Counts['employees'] = Employees::count();
            $Counts['customers'] = Customers::count();

            $Candidate= CandidateForm::where('user_id',Auth::user()->id)->first();
            
            if($Candidate)
            {
                $Candidate->section= JobPost::where('id',$Candidate->job_post_id)->value('section');
                if($Candidate->status == 'Acknowledged')
                {

                     $data='NA';
                     $status='Acknowledged';

                }
                else
                {
                    $status= Schedule::where('candidate_form_id',$Candidate->id)->first();
                    $data= Schedule::where('candidate_form_id',$Candidate->id)->first();
                        if($data)
                        {
                            $data=$data;
                        }
                        else{
                            $data='NA';
                        }
                        if($status)
                        {
                        
                            if($status->status =='Approved')
                            {
                                 $status='Approved';
                            }
                            elseif($status->status =='Selected')
                            {
                                 $status='Selected';
                            }
                            elseif($status->status =='Interview-Pending')
                            {
                                 $status='Interview-Pending';
                            }
                            elseif($status->status =='Interview-Taken')
                            {
                                 $status='Interview-Taken';
                            }
                        }
                        else
                        {
                            $status_reject= CandidateForm::where('user_id',Auth::user()->id)->where('status','Rejected')->first();
                            if(!$status_reject)
                            {
                                $status='Pending';    
                            }
                            else
                            {
                                $status='Rejected'; 
                            }
                            
                        }                
                }

            }
            else
            {
                $status='NA';
                $data='NA';
            }

            return view('partials.dashboard',compact('Candidate','status','data'));
        }

    }
}
