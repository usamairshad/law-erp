<?php

namespace App\Http\Controllers\Notifications;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payroll\LeaveManagement;

class Notification_Controller extends Controller
{
    //

    public function leave_request_count_notification()
    {
    	# code...
    	$leave_count=LeaveManagement::where('status','Pending')->count();
    	return $leave_count;
    	
    	
    }
}
