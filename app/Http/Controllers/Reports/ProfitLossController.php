<?php

namespace App\Http\Controllers\Reports;
use App\Http\Controllers\Controller;
use App\Models\Admin\EntryItems;
use App\Models\Admin\Entries;
use App\Models\Admin\EntryTypes;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Ledgers;
use App\Models\Admin\Groups;

use Illuminate\Http\Request;

class ProfitLossController extends Controller
{
    public function index(){
        return view('reports.profit_loss.index');
    }
    public function print_report(Request $request){
        $type="";
        $type=$request->type;
        $texp_balance=0;
        $incomeData='';
        $expData='';
        $tincome_balance=0;
        if($request->post('date_range')) {
            $date_range=explode('-', trim($request->date_range));
            $start_date = strtr($date_range[0], '/', '-');
            $start_date=date('Y-m-d', strtotime($start_date));
            $end_date = strtr($date_range[1], '/', '-');
            $end_date = date('Y-m-d', strtotime($end_date));
        } else {
            $start_date = null;
            $end_date = null;
        }
        $tinc_balance=0;
        $GroupIncome=Groups::where('account_type_id', 4)->orderBy('number')->get();
        foreach ($GroupIncome as $income){
            if($income->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$income->level*30;
                $margin=$margin+20;
                $font=900-100*$income->level;
            }
            $incomeData.='<tr>';
            $incomeData.='<td colspan="2"><span style="margin-left:'.$margin.'">'.$income->name.'</span></td>';
            $incomeData.='</tr>';
            $Ledgers=Ledgers::where('group_id', $income->id)->get();
            foreach ($Ledgers as $Ledger){
                    $ob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                    $tinc_balance+=$ob;
                    $incomeData .= '<tr style="text-align: center">';
                    $incomeData.='<td align="right"> '.ucfirst($Ledger->name).'</td>';
                    $incomeData .= '<td align="right">'.CoreAccounts::dr_cr_balance($ob,2).'</td>';
                    $incomeData .= '</tr>';
            }
        }
        $incomeData.='
                    <tr class="bold-text bg-filled">
                        <td>Gross Incomes</td>
                        <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">'.CoreAccounts::dr_cr_balance($tinc_balance,2).'</td>
                    </tr>';
        //fetch all expenses groups
        $texp_balance=0;
        $exp_ob=0;
        $GroupExp=Groups::where('account_type_id', 3)->orderBy('number')->get();
        foreach ($GroupExp as $Exp){
            if($Exp->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Exp->level*30;
                $Lmargin=$margin+20;
                $font=900-100*$Exp->level;
            }
            $expData.='<tr>';
            $expData.='<td colspan="2"><span style="margin-left:'.$margin.'">'.$Exp->name.'</span></td>';
            $expData.='</tr>';
            $Ledgers=Ledgers::where('group_id', $Exp->id)->get();
            foreach ($Ledgers as $Ledger){
                $exp_ob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $texp_balance+=$exp_ob;
                    $expData .= '<tr style="text-align: center">';
                    $expData.='<td align="right"> '.ucfirst($Ledger->name).'</td>';
                    $expData .= '<td align="right">'.CoreAccounts::dr_cr_balance($exp_ob,2).'</td>';
                    $expData .= '</tr>';
            }
        }
        $expData.='
                <tr class="bold-text bg-filled">
                    <td>Gross Expenses</td>
                    <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">'.CoreAccounts::dr_cr_balance($texp_balance,2).'</td>
                </tr>';
        $net=($tinc_balance)+($texp_balance);
        $expData.='<tr class="bold-text">
                    <td>Net (Profit)/Loss</td>
                    <td align="right" style="border-top:double; border-bottom: double">'.CoreAccounts::dr_cr_balance($net,2).'</td>
                </tr>';
        if($type=='excel'){
            return self::excel_report($request->all());
        }else{
            return view('reports.profit_loss.print_report', compact('incomeData', 'expData', 'start_date','end_date'));
        }
    }
    public function excel_report($request){
        header('Content-Type: application/vnd.ms-excel charset=UTF-8');
        header('Content-disposition: attachment; filename='.rand().'.xls');
        $data='';
        if($request['date_range']) {
            $date_range=explode('-', trim($request['date_range']));
            $start_date = strtr($date_range[0], '/', '-');
            $start_date=date('Y-m-d', strtotime($start_date));
            $end_date = strtr($date_range[1], '/', '-');
            $end_date = date('Y-m-d', strtotime($end_date));
        } else {
            $start_date = null;
            $end_date = null;
        }
        $data.='<style>
                table{width: 100%;}
                td,th {
                    border: 0.1pt solid #ccc;
                }
                </style>';
        $expData='';
        $incomeData='';
        $tinc_balance=0;
        $data.=CoreAccounts::excel_header();
        $data.='<tr><th colspan="4">Profit & Loss AS on '.$start_date.' </th></tr>';
        $GroupIncome=Groups::where('account_type_id', 4)->orderBy('number')->get();
        foreach ($GroupIncome as $income){
            if($income->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$income->level*30;
                $margin=$margin+20;
                $font=900-100*$income->level;
            }
            $incomeData.='<tr>';
            $incomeData.='<th colspan="2"><span style="margin-left:'.$margin.'">'.$income->name.'</span></th>';
            $incomeData.='</tr>';
            $Ledgers=Ledgers::where('group_id', $income->id)->get();
            foreach ($Ledgers as $Ledger){
                $ob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $tinc_balance+=$ob;
                $incomeData .= '<tr style="text-align: center">';
                $incomeData.='<td align="right"> '.ucfirst($Ledger->name).'</td>';
                $incomeData .= '<td align="right">'.CoreAccounts::dr_cr_balance($ob,2).'</td>';
                $incomeData .= '</tr>';
            }
        }
        $incomeData.='
                    <tr class="bold-text bg-filled">
                        <td style="text-align: right">Gross Incomes</td>
                        <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">'.CoreAccounts::dr_cr_balance($tinc_balance,2).'</td>
                    </tr>';
        //fetch all expenses groups
        $texp_balance=0;
        $exp_ob=0;
        $GroupExp=Groups::where('account_type_id', 3)->orderBy('number')->get();
        foreach ($GroupExp as $Exp){
            if($Exp->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Exp->level*30;
                $Lmargin=$margin+20;
                $font=900-100*$Exp->level;
            }
            $expData.='<tr>';
            $expData.='<td colspan="2"><span style="margin-left:'.$margin.'">'.$Exp->name.'</span></td>';
            $expData.='</tr>';
            $Ledgers=Ledgers::where('group_id', $Exp->id)->get();
            foreach ($Ledgers as $Ledger){
                $exp_ob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $texp_balance+=$exp_ob;
                $expData .= '<tr style="text-align: center">';
                $expData.='<td align="right"> '.ucfirst($Ledger->name).'</td>';
                $expData .= '<td align="right">'.CoreAccounts::dr_cr_balance($exp_ob,2).'</td>';
                $expData .= '</tr>';
            }
        }
        $expData.='
                <tr class="bold-text bg-filled">
                    <td style="text-align: right">Gross Expenses</td>
                    <td style="text-align: right;">'.CoreAccounts::dr_cr_balance($texp_balance,2).'</td>
                </tr>';
        $net=($tinc_balance)+($texp_balance);
        $expData.='<tr class="bold-text">
                    <td style="text-align: right">Net (Profit)/Loss</td>
                    <td align="right" style="border-top:double black; border-bottom: double black">'.CoreAccounts::dr_cr_balance($net,2).'</td>
                </tr>';
        $data.=$incomeData.''.$expData;
        echo '<body style="border: 0.1pt solid #ccc"><table>'.$data.'</table></body>';
    }
}
