<?php

namespace App\Http\Controllers\Reports;
use App\Http\Controllers\Controller;
use App\Models\Admin\EntryItems;
use App\Models\Admin\Entries;
use App\Models\Admin\EntryTypes;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Ledgers;
use App\Models\Admin\Groups;
use DB;


use Illuminate\Http\Request;
use Svg\Tag\Group;

class BalanceSheetController extends Controller
{
    public static function index(){
        return view('reports.balance_sheet.index');
    }
    public function print_report(Request $request){
        $data=''; $string=""; $type="";
        //assets opening balance
        $aob=0; $taob=0;
        //liability opening balance
        $lob=0; $tlob=0;
        if($request->post('date_range')) {
            $date_range=explode('-', trim($request->date_range));
            $start_date = strtr($date_range[0], '/', '-');
            $start_date=date('Y-m-d', strtotime($start_date));
            $end_date = strtr($date_range[1], '/', '-');
            $end_date = date('Y-m-d', strtotime($end_date));
        } else {
            $start_date = null;
            $end_date = null;
        }
        $type=$request->type;
        $Groups = Groups::whereIn('account_type_id', array(1))->orderBy('number')->get();
        $tdr=0; $tcr=0;$level=0;
        foreach($Groups as $Group) {

            if($Group->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Group->level*30;
                $Lmargin=$margin+20;
                $font=900-100*$Group->level;
                $level=$Group->level*1;
            }
            $Ledgers=Ledgers::where('group_id', $Group->id)->groupBy('group_id')->get();
            foreach ($Ledgers as $Ledger){
                $aob=CoreAccounts::opening_balance($end_date, $Ledger->id);
//                $data.='<tr>';
//                $data.='<th colspan="2" style="text-align: center"> '.$Ledger->name.'</th>';
//                $data.='<td colspan="2" style="text-align: right">'.number_format($aob,2).'</td>';
//                $data.='</tr>';
                $taob+=$aob;
            }
            $data.='<tr>';
            $data.='<th colspan="2"> <span style="margin-left:'.$margin.'px; font-weight:'.$font.'">'.$Group->number.'-'.$Group->name.'</span></th>';
            $data.='</tr>';


        }
        $data.='<tr class="bold-text bg-filled">
                        <td style="text-align: right">Total Asset:</td>
                        <td colspan="2" style="text-align: right;" align="right">'.number_format($taob,2).'</td>
                    </tr>';
        $Groups = Groups::whereIn('account_type_id', array(2,5))->orderBy('number')->get();
        $tdr=0; $tcr=0;$level=0;
        foreach($Groups as $Group) {

            if($Group->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Group->level*30;
                $Lmargin=$margin+20;
                $font=900-100*$Group->level;
                $level=$Group->level*1;
            }

            $data.='<tr>';
            $data.='<th colspan="2"> <span style="margin-left:'.$margin.'px; font-weight:'.$font.'">'.$Group->number.'-'.$Group->name.'</span></th>';
            $data.='</tr>';
            $Ledgers=Ledgers::where('group_id', $Group->id)->groupBy('group_id')->get();
            foreach ($Ledgers as $Ledger){
                $lob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $tlob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $data.='<tr>';
                $data.='<th colspan="2" style="text-align: center"> '.$Ledger->name.'</th>';
                $data.='<td colspan="2" style="text-align: right">'.number_format($lob,2).'</td>';
                $data.='</tr>';
            }


        }
        $total_liability=0;
        $total_liability=$tlob+self::unapp_profit($end_date);
        $data.='<tr>';
        $data.='<th colspan="2" style="text-align: center"> UnAppropriate Profit</th>';
        $data.='<td colspan="2" style="text-align: right">'.number_format(abs(self::unapp_profit($end_date)),2).'</td>';
        $data.='</tr>';
        $data.='<tr class="bold-text bg-filled">
                        <td style="text-align: right">Total Liabilities & Owners Equity:</td>
                        <td colspan="2" style="text-align: right;" align="right">'.number_format(abs($total_liability),2).'</td>
                    </tr>';
        return view('reports.balance_sheet.print_report', compact('data', 'end_date'));
    }
    public static function unapp_profit($end_date=''){
        $ob=0; $tinc_balance=0;
        $GroupIncome=Groups::where('account_type_id', 4)->orderBy('number')->get();
        foreach ($GroupIncome as $income){
            $Ledgers=Ledgers::where('group_id', $income->id)->get();
            foreach ($Ledgers as $Ledger){
                $ob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $tinc_balance+=$ob;
            }
        }
        //==========================end income=================
        $texp_balance=0; $exp_ob=0;
        $GroupExp=Groups::where('account_type_id', 3)->orderBy('number')->get();
        foreach ($GroupExp as $Exp){
            if($Exp->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Exp->level*30;
                $Lmargin=$margin+20;
                $font=900-100*$Exp->level;
            }
            $Ledgers=Ledgers::where('group_id', $Exp->id)->get();
            foreach ($Ledgers as $Ledger){
                $exp_ob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $texp_balance+=$exp_ob;
            }
        }
        $net=($tinc_balance)+($texp_balance);
        return $net;
    }
    //tree balance sheet
    public function balance_sheet_tree(){
        $Groups=Groups::all();
        return view('reports.balance_sheet.balance_sheet_tree', compact('Groups'));
    }
    public function get_balance_sheet_tree(Request $request){
        $data='';
        $id=$request->id;
        $total=0;
        $obj = new CoreAccounts();
        $Groups = Groups::where('parent_id', $id)->get();
        if($Groups->isNotEmpty()) {
            foreach ($Groups as $Group) {
                $obj->val=0;
                $total = $obj->group_balances($Group->id);
                $data .= '
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" href="#' . $Group->number . '" data-toggle="collapse" onclick="get_tree(this, ' . $Group->id . ', ' . $Group->level . ');" val="' . $Group->number . '">' . $Group->number . '-' . $Group->name . ' (' . $total . ')</a>
                </div>
                <div class="accordion-body collapse" id="' . $Group->number . '"></div>
            </div>';
            }
        }else{
            $Groups = Groups::where('id', $id)->get();
            foreach ($Groups as $Group) {
                $obj->val=0;
                $total = $obj->group_balances($Group->id);
                $data .= '
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-parent="#' . $Group->number . '" data-toggle="collapse" href="#" onclick="get_tree(this, ' . $Group->id . ', ' . $Group->level . ');" val="' . $Group->number . '">' . $Group->number . '-' . $Group->name . ' (' . $total . ')</a>
                </div>
                <div class="accordion-body collapse" id="' . $Group->number . '"></div>
            </div>';
            }
        }
        return response()->json(['data' => $data]);
    }
}
