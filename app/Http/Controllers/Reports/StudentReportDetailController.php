<?php

namespace App\Http\Controllers\Reports;

use App\ActiveSessionSection;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\AssignClassSection;
use App\Models\Academics\BoardProgramClass;
use App\Models\Academics\Section;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function Complex\sec;

class StudentReportDetailController extends Controller
{
    public function index()
    {
        return view('studentreportdetail');
    }

    public function board_branches(Request $request){
        $records = ActiveSession::where('branch_id', $request->branch_id)->with('board')
            ->groupBy('board_id')
            ->get();
        \Log::info($request);
        return response()->json($records);
    }
    public function boards(Request $request){
        $records = ActiveSession::where('board_id', $request->board_id)->with('program')
            ->groupBy('program_id')
            ->get();
        return response()->json($records);
    }
    public function programs(Request $request){
        $records = ActiveSession::where('program_id', $request->program_id)->with('classes')
            ->groupBy('class_id')
            ->get();
        return response()->json($records);
    }
    public function classes(Request $request){

        $records = BoardProgramClass::where('class_id',$request->class_id)->groupBy('class_id')->get();
        foreach ($records as $record){
            $collections = AssignClassSection::where('board_program_class_id',$record->id)->groupBy('board_program_class_id')->get();
            foreach ($collections as $data)
            {
                $section = Section::where('id',$data->section_id)->groupBy('id')->get();
                return response()->json($section);
            }
        }


    }
    public function active_session(Request $request){
        $whereConditions = [
            ['branch_id',$request->branch],
            ['board_id', $request->board],
            ['program_id',$request->program],
            ['class_id',$request->class]
        ];
        $activeSessions = ActiveSession::where($whereConditions)->first();

           $data =   ActiveSessionSection::where('active_session_id',$activeSessions->id)->get();
           dd($data[0]['id']);
    }
}
