<?php

namespace App\Http\Controllers\Reports;
use App\Http\Controllers\Controller;
use App\Models\Admin\EntryItems;
use App\Models\Admin\Entries;
use App\Models\Admin\Branches;
use App\Models\Admin\EntryTypes;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Ledgers;
use App\Models\Admin\Groups;
use Illuminate\Http\Request;
use App\Helpers\GroupsTree;
use App\Helpers\AccountsList;
use App\Models\Admin\AccountTypes;


class TrialBalanceController extends Controller
{
   // public function index(){
    //    return view('reports.trial_balance.index');
   // }

    public function index()
    {
       

        // Get All Employees
    

        // Get All Branches
        $Branches = Branches::pluckActiveOnly();
       $Branches->prepend('Select a Branch', '');

        // Get All Departments
     //   $Departments = Departments::pluckActiveOnly();
     //   $Departments->prepend('Select a Department', '');

        // Get All Account Types
        $AccountTypes = AccountTypes::getActiveListDropdown(true);

        // Get All Entry Types
 //       $EntryTypes = EntryTypes::pluckActiveOnly();
  //      $EntryTypes->prepend('Select an Entry Type', '');

        // Get All Groups
        $Groups = GroupsTree::buildOptions(GroupsTree::buildTree(Groups::OrderBy('name', 'asc')->get()->toArray()), old('group_id'));

        return view('admin.account_reports.trial_balance.index', compact('Branches','AccountTypes','Groups'));
    }

    public function trial_balance_report(Request $request)
    {
         // print_r($request->all());exit;
          if($request->get('date_range')) {
            $date_range = explode(' - ', $request->get('date_range'));
            $start_date = date('Y-m-d', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1]));
        } else {
            $start_date = null;
            $end_date = null;
        }
        $accountlist = new AccountsList();
        $accountlist->only_opening = false;
        $accountlist->start_date = $start_date;
        $accountlist->end_date = $end_date;
        $accountlist->affects_gross = -1;
        $accountlist->filter = $request->all();
   
        $accountlist->start(($request->get('group_id')) ? $request->get('group_id') : 0);
     //echo'<pre>';print_r($accountlist);echo'</pre>';exit;
        $DefaultCurrency = Currencies::getDefaultCurrencty();
        $ReportData = $accountlist->generateLedgerStatement($accountlist);

        switch($request->get('medium_type')) {
            case 'web':
                return view('admin.account_reports.trial_balance.report', compact('accountlist', 'ReportData', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
            case 'print':
                return view('admin.account_reports.trial_balance.report', compact('accountlist', 'ReportData', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
            case 'excel':
                $this->trialBalanceExcel($accountlist, $DefaultCurrency, $start_date, $end_date);
                break;
            case 'pdf':
                $content = View::make('admin.account_reports.trial_balance.report', compact('accountlist', 'ReportData', 'start_date', 'end_date', 'DefaultCurrency'))->render();
                // Create an instance of the class:
                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
                // Write some HTML code:
                $mpdf->WriteHTML($content);
                // Output a PDF file directly to the browser
                $mpdf->Output('TrialBalanceReport.pdf','D');
                break;
            default:
                return view('admin.account_reports.trial_balance.report', compact('accountlist', 'ReportData', 'start_date', 'end_date', 'DefaultCurrency'));
                break;
        } 
    }

    public function get_trial_balance(Request $request){
        $tdr=0; $tcr=0;$tcb=0; $tob=0;
        $tcb_dr=0; $tcb_cr=0;
        $data=''; $string=""; $type="";
        $type=$request->type;
        if($request->post('date_range')) {
            $date_range=explode('-', trim($request->date_range));
            $start_date = strtr($date_range[0], '/', '-');
            $start_date=date('Y-m-d', strtotime($start_date));
            $end_date = strtr($date_range[1], '/', '-');
            $end_date = date('Y-m-d', strtotime($end_date));
        } else {
            $start_date = null;
            $end_date = null;
        }
        $Groups = Groups::orderBy('number')->get();
        foreach($Groups as $Group) {
            if($Group->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Group->level*10;
                $Lmargin=$margin+20;
                $font=900-100*$Group->level;
            }

            $data.='<tr>';
            $data.='<th colspan="5" style="text-align: left;"> <span style="margin-left:'.$margin.'px; font-weight:'.$font.'">'.$Group->number.'-'.$Group->name.'</span></th>';
            $data.='</tr>';
            $Ledgers=Ledgers::where('group_id', $Group->id)->get();
            foreach ($Ledgers as $Ledger){
                $sob=0; $sdr=0; $scr=0; $scb=0;
                $dr_cr=CoreAccounts::dr_cr_amount($start_date, $end_date, $Ledger->id);
                $ob=CoreAccounts::opening_balance($start_date, $Ledger->id);
                $balance=($ob)+($dr_cr[0])-($dr_cr[1]);
                $data.='<tr>';
                $data.='<td colspan="1" align="right"> '.ucfirst($Ledger->name).'</td>';
                $data.='<td align="center">'.CoreAccounts::dr_cr_balance($ob,2).'</td>';
                $data.='<td align="center">'.number_format($dr_cr[0], 2).'</td>';
                $data.='<td align="center"> '.number_format($dr_cr[1], 2).'</td>';
                $data.='<td align="center">'.CoreAccounts::dr_cr_balance($balance, 2).'</td>';
                $data.='</tr>';
                $tdr+=$dr_cr[0];
                $tcr+=$dr_cr[1];
                $tcb_dr=$tdr;
                $tcb_cr=$tcr;
                $tob+=$ob;
                $tcb+=$balance;
            }

        }
        $string.='
                      <tr class="last-th">
                        <th></th>
                        <th width="15%" align="center">Opening Balance</th>
                        <th width="10%" align="center">Debit</th>
                        <th width="10%" align="center">Credit </th>
                        <th width="12%" align="center">Closing Balance </th>
                    </tr>
                     ';
        $string.='<tr style="border-bottom: double;border-top: double">';
        $string.='<th style="text-align: right" colspan="1">Total:</th>';
        $string.='<th style="text-align: center;">'.CoreAccounts::dr_cr_balance($tob, 2).'</th>';
        $string.='<th style="text-align: center">'.number_format($tdr, 2).'</th>';
        $string.='<th style="text-align: center">'.number_format($tcr, 2).'</th>';
        $string.='<th style="text-align: center">'.CoreAccounts::dr_cr_balance($tcb, 2).'</th>';
        $string.='</tr>';
        $data.=$string;
        $content= view('reports.trial_balance.print_report', compact('data', 'start_date', 'end_date'));
        if($type=='excel'){
           return self::trial_balance_excel($request);
     }
        else{
            return $content;
        }
    }
    public function trial_balance_excel($request){
        header('Content-Type: application/vnd.ms-excel charset=UTF-8');
        header('Content-disposition: attachment; filename='.rand().'.xls');
        $tob_dr=0; $tob_cr=0;  $tdr=0; $tcr=0;$tcb=0; $tob=0;
        $tcb_dr=0; $tcb_cr=0;
        if($request->post('date_range')) {
            $date_range=explode('-', trim($request->date_range));
            $start_date = strtr($date_range[0], '/', '-');
            $start_date=date('Y-m-d', strtotime($start_date));
            $end_date = strtr($date_range[1], '/', '-');
            $end_date = date('Y-m-d', strtotime($end_date));
        } else {
            $start_date = null;
            $end_date = null;
        }
        $data=''; $string="";
        $data.='<style>
                table{width: 100%;}
                td,th {
                    border: 0.1pt solid #ccc;
                }
                </style>';
        $data.='<tr><th colspan="4">Trial Balance AS on '.$start_date.' </th></tr>';
        $data.='
              <tr class="last-th">
                <th>Account</th>
                <th align="center">Opening Balance</th>
                <th align="center">Debit</th>
                <th align="center">Credit </th>
                <th align="center">Closing Balance </th>
            </tr>';
        $Groups = Groups::orderBy('number')->get();
        foreach($Groups as $Group) {
            if($Group->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Group->level*10;
                $Lmargin=$margin+20;
                $font=900-100*$Group->level;
            }

            $data.='<tr>';
            $data.='<th colspan="3" style="text-align: left;"> <span style="margin-left:'.$margin.'px; font-weight:'.$font.'">'.$Group->number.'-'.$Group->name.'</span></th>';
            $data.='</tr>';
            $Ledgers=Ledgers::where('group_id', $Group->id)->get();
            foreach ($Ledgers as $Ledger){
                $dr_cr=CoreAccounts::dr_cr_amount($start_date, $end_date, $Ledger->id);
                $ob=CoreAccounts::opening_balance($start_date, $Ledger->id);
                $balance=($ob)+($dr_cr[0])-($dr_cr[1]);
                $data.='<tr>';
                $data.='<td colspan="1" align="right"> '.ucfirst($Ledger->name).'</td>';
                $data.='<td align="center">'.CoreAccounts::dr_cr_balance($ob,2).'</td>';
                $data.='<td align="center">'.number_format($dr_cr[0], 2).'</td>';
                $data.='<td align="center"> '.number_format($dr_cr[1], 2).'</td>';
                $data.='<td align="center">'.CoreAccounts::dr_cr_balance($balance, 2).'</td>';

                $data.='</tr>';
                $tdr+=$dr_cr[0];
                $tcr+=$dr_cr[1];
                $tcb_dr=$tdr;
                $tcb_cr=$tcr;
                $tob+=$ob;
                $tcb+=$balance;
            }

        }
        $string.='
              <tr class="last-th">
                <th></th>
                <th width="10%" align="center">Opening Balance</th>
                <th width="10%" align="center">Debit</th>
                <th width="10%" align="center">Credit </th>
                <th width="12%" align="center">Closing Balance </th>
            </tr>';
        $string.='<tr>';
        $string.='<th align="right" colspan="1">Total</th>';
        $string.='<th style="text-align: center;">'.CoreAccounts::dr_cr_balance($tob, 2).'</th>';
        $string.='<th style="text-align: center">'.number_format($tdr, 2).'</th>';
        $string.='<th style="text-align: center">'.number_format($tcr, 2).'</th>';
        $string.='<th style="text-align: center">'.CoreAccounts::dr_cr_balance($tcb, 2).'</th>';
        $string.='</tr>';
        $data.=$string;
        echo '<body style="border: 0.1pt solid #ccc"><table>'.$data.'</table></body>';
    }
}
