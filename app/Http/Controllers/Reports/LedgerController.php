<?php

namespace App\Http\Controllers\Reports;
use App\Http\Controllers\Controller;
use App\Models\Admin\EntryItems;
use App\Models\Admin\Entries;
use App\Models\Admin\EntryTypes;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Ledgers;
use App\Models\Admin\StaffSalaries;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Carbon\CarbonPeriod;
class LedgerController extends Controller
{
    public function index(){
        return view('reports.ledgers.index');
    }
    public function ledger_print(Request $request){
        
        $type = $request->search_type;
        $date_range=explode('-', trim($request->date_range));
        $start_date = strtr($date_range[0], '/', '-');
        $start_date=date('Y-m-d', strtotime($start_date));
        $end_date = strtr($date_range[1], '/', '-');
        $end_date = date('Y-m-d', strtotime($end_date));
        $balance=0.00; $ob=0; $tdr=0; $tcr=0;
        $ledgerID = $request->leadger_idd;

        $date=date_create($start_date);
        date_sub($date,date_interval_create_from_date_string("1 days"));
       $new= date_format($date,"Y-m-d");





        $array = array();
        $ledger_name=$ledgerID;
//also changes required in opening balance to get the data from salary and fee tables
$new_date = date("Y-m-d",strtotime('2010-01-01'));
        //Initialize the code to get the data from salaries and the fees tables and join with the ledger id
/*
it will give data in array format

*/
$Entrie = 0;
$Entries = 0;
    if($type == "Student")
    {
        $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
        ->where([['erp_entry_items.narration','=', $ledgerID],['erp_ledgers.account_type_id','!=', 4]])->whereBetween('voucher_date', [$start_date, $end_date])->get();
   
$Entries = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
->where([['erp_entry_items.narration','=', $ledgerID],['erp_ledgers.account_type_id','!=', 4]])->whereBetween('voucher_date', [''.$new_date.'', ''.$new.''])->get();
    }
    else
    if($type == "Staff")
    {
        $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
        ->where([['erp_entry_items.narration','=', $ledgerID],['erp_ledgers.account_type_id','!=', 3]])->whereBetween('voucher_date', [$start_date, $end_date])->get();
        $Entries = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
        ->where([['erp_entry_items.narration','=', $ledgerID],['erp_ledgers.account_type_id','!=', 3]])->whereBetween('voucher_date', [''.$new_date.'', ''.$new.''])->get();

  
    }
    else
    {
     $Entrie = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
     ->where('erp_ledgers.id','=', $ledgerID)->whereBetween('voucher_date', [$start_date, $end_date])->get();
     $Entries = EntryItems::join('erp_ledgers','erp_ledgers.id','=','erp_entry_items.ledger_id')
     ->where('erp_entry_items.ledger_id', $ledgerID)->whereBetween('voucher_date', [''.$new_date.'', ''.$new.''])->get();
    }
   

  
$dr=0; $cr=0; $ob=0;

foreach ($Entries as $Ent) {
    if ($Ent->dc == 'd') {
        $dr += $Ent['amount'];
    }
    if ($Ent->dc == 'c') {
        $cr += $Ent['amount'];
    }
}//foreach

$opening_balance=($ob)+($dr)-($cr);
$ob_str = '<td colspan="6" align="right">Opening Balance AS on ' . $start_date . '</td>
        <td align="right">'.CoreAccounts::dr_cr_balance($opening_balance).'</td>';
        
         foreach ($Entrie as $Ent) {
              

            $dr=0.00; $cr=0.00;
            $vn = Entries::where('id', $Ent->entry_id)->pluck('number');
            $vt = EntryTypes::where('id', $Ent->entry_type_id)->pluck('code');
            if ($Ent->dc == 'd') {
                $dr = $Ent->amount;
            } else {
                $cr = floatval($Ent->amount);

            }
            $ob+=($dr)-($cr);
            $balance=$opening_balance+$ob;
            $balance=number_format($balance, 2,'.', '');
            $array[] = array('voucher_date' => $Ent->voucher_date, 'number' => $vn[0],
                'vt' => $vt[0], 'narration' => $Ent->narration, 'dr_amount' => number_format($dr, 2), 'cr_amount' => number_format($cr,2), 'balance' => CoreAccounts::dr_cr_balance($balance, 2));
            $tdr+=$dr;
            $tcr+=$cr;
           // print_r();
        }
      
   // }
        $prBalance=CoreAccounts::dr_cr_balance($opening_balance);
    
        if(isset($request->type) && $request->type=='excel'){
            return self::excel_ledger($array, $start_date, CoreAccounts::dr_cr_balance(CoreAccounts::opening_balance($start_date, $ledgerID)));
        }else if($request->type=='print'){
            return view('reports.ledgers.print_ledger', compact('array', 'ledger_name', 'start_date', 'end_date', 'prBalance', 'tdr', 'tcr', 'balance'));
        }
        return response()->json(['data' => $array, 'ob' => $ob_str, 'total_dr'=>number_format($tdr, 2), 'total_cr'=>number_format($tcr, 2), 'balance'=>CoreAccounts::dr_cr_balance($balance, 2)]);
    }
    public function excel_ledger($array, $start_date="", $ob){
        header('Content-Type: application/vnd.ms-excel charset=UTF-8');
        header('Content-disposition: attachment; filename='.rand().'.xls');
        $data=''; $string="";
        $tdr=0; $tcr=0;
        $data.='<style>
                table{width: 100%;}
                td,th {
                    border: 0.1pt solid #ccc;
                }
                </style>';
        $data.='<tr></tr>';
        $data.='<tr><th colspan="6" align="center">Basic Ledger Report</th></tr>';
        $data.='<tr>
            <th width="10%" align="center">Date</th>
            <th width="5%" align="center">VN</th>
            <th width="12%" align="center">Voucher Type</th>
            <th width="40%">Descriptions</th>           
            <th width="10%" align="center">Debit </th>
            <th width="10%" align="center">Credit </th>
        </tr>';
        $data.='<tr>
            <td colspan="6" align="right">Opening Balance As on '.$start_date.'</td>
            <td style="text-align: right">'.$ob.'</td>
        </tr>';
        foreach ($array as $arr){
            $data.='<tr>
                    <td>'.$arr['voucher_date'].'</td>
                    <td>'.$arr['number'].'</td>
                    <td>'.$arr['vt'].'</td>
                    <td style="text-align: left">'.$arr['narration'].'</td>
    
                    <td>'. $arr['dr_amount'].'</td>
                    <td style="text-align: right">'.$arr['cr_amount'].'</td>
                    <td style="text-align: right">'.$arr['balance'].'</td>
                </tr>';
        }
        echo '<body style="border: 0.1pt solid #ccc"><table>'.$data.'</table></body>';
    }
}
