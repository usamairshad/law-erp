<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Report\StudentReport;
use App\Models\Academics\Student;

class StudentReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        $stu_reports = StudentReport::with('student')->get();
        return view('reports.student_report.index', compact('students','stu_reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request,[
            'student_id' => 'required',
            'grade' => 'required',
        ]);
        $id = $request->id;
        $inputs = $request->input();
        $inputs['name'] = Student::where('id',$inputs['student_id'])->value('first_name');
        //dd($inputs);
        if ($id==0 || $id==''){
            StudentReport::create($inputs);
            return redirect()->back()->with('success','Data Created successfully!');
        }else{
            StudentReport::where('id',$id)->update(['student_id'=>$request->student_id, 'grade'=>$request->grade, 'name'=>$inputs['name']]);
            return redirect()->back()->with('success','Data Updated successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StudentReport::where('id', $id)->delete();
        return redirect()->back()->with('deleted','Data Deleted successfully!');
    }
}
