<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Report\ReportGroup;
use App\Models\Report\ReportParameter;


class ReportParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $report_group = ReportGroup::all();
        $report_parameter = ReportParameter::with('reportgroup')->orderBy('id','desc')->get();
        //dd($report_parameter);
        return view('reports.report_parameter.index', compact('report_group', 'report_parameter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'range'=>'required',
            'reportGroup_id'=>'required',
        ]);
        $inputs = $request->input();
        $id = $request->id;
        if ($id==0 || $id==''){
        ReportParameter::create($inputs);
            return redirect()->back()->with('success','Data Created successfully!');
        }
        else{
           ReportParameter::where('id', $id)->update(['name'=>$request->name,'description'=>$request->description,'range'=>$request->range, 'reportgroup_id'=>$request->reportGroup_id]);
            return redirect()->back()->with('success','Data Updated successfully!');
        } 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
       ReportParameter::where('id', $id)->delete();
       return redirect()->back()->with('deleted','Data Deleted successfully!');

    }
}
