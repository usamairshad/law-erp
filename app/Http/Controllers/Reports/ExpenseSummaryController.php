<?php

namespace App\Http\Controllers\Reports;
use App\Http\Controllers\Controller;
use App\Models\Admin\EntryItems;
use App\Models\Admin\Entries;
use App\Models\Admin\EntryTypes;
use App\Helpers\CoreAccounts;
use App\Models\Admin\Ledgers;
use App\Models\Admin\Groups;

use Illuminate\Http\Request;

class ExpenseSummaryController extends Controller
{
    public function index(){
        return view('reports.expense_summary.index');
    }
    public function print_report(Request $request){
        $expData=''; $texp_balance=0;
        if($request->post('date_range')) {
            $date_range=explode('-', trim($request->date_range));
            $start_date = strtr($date_range[0], '/', '-');
            $start_date=date('Y-m-d', strtotime($start_date));
            $end_date = strtr($date_range[1], '/', '-');
            $end_date = date('Y-m-d', strtotime($end_date));
        } else {
            $start_date = null;
            $end_date = null;
        }
        $type="";
        $type=$request->type;
        $GroupExp=Groups::where('account_type_id', 3)->orderBy('number')->get();
        foreach ($GroupExp as $Exp){
            if($Exp->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Exp->level*30;
                $Lmargin=$margin+20;
                $font=900-100*$Exp->level;
            }
            $expData.='<tr>';
            $expData.='<td colspan="2"><span style="margin-left:'.$margin.'">'.$Exp->name.'</span></td>';
            $expData.='</tr>';
            $Ledgers=Ledgers::where('group_id', $Exp->id)->get();
            foreach ($Ledgers as $Ledger){
                $ob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $texp_balance+=$ob;
                    $expData .= '<tr style="text-align: center">';
                    $expData.='<td align="right"> '.ucfirst($Ledger->name).'</td>';
                    $expData .= '<td align="right">'.CoreAccounts::dr_cr_balance($ob,2).'</td>';
                    $expData .= '</tr>';
            }
        }
        $expData.='
                <tr class="bold-text bg-filled">
                    <td>Gross Expenses</td>
                    <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">'.CoreAccounts::dr_cr_balance($texp_balance, 2).'</td>
                </tr>';
        $content=view('reports.expense_summary.print_report', compact('expData', 'end_date'));
        if($type=='excel'){
            return self::excel_report($request->all());
        }else{
            return $content;
        }
    }
    public function excel_report($request){
        header('Content-Type: application/vnd.ms-excel charset=UTF-8');
        header('Content-disposition: attachment; filename='.rand().'.xls');
        $expData=''; $texp_balance=0;
        if($request['date_range']) {
            $date_range=explode('-', trim($request['date_range']));
            $start_date = strtr($date_range[0], '/', '-');
            $start_date=date('Y-m-d', strtotime($start_date));
            $end_date = strtr($date_range[1], '/', '-');
            $end_date = date('Y-m-d', strtotime($end_date));
        } else {
            $start_date = null;
            $end_date = null;
        }
        $expData.='<style>
        table{width: 100%;}
        td,th {
            border: 0.1pt solid #ccc;
        }
        </style>';
        $expData.='<tr><th colspan="4">Expense Summary AS on '.$start_date.' </th></tr>';
        $expData.=' <tr>
                    <th class="th-style">Expenses (Dr)</th>
                    <th class="th-style" width="20%" style="text-align: right;">Amount</th>
                </tr>';
        $GroupExp=Groups::where('account_type_id', 3)->orderBy('number')->get();
        foreach ($GroupExp as $Exp){
            if($Exp->level==1){
                $margin=0;
                $font="";
            }else{
                $margin=$Exp->level*30;
                $Lmargin=$margin+20;
                $font=900-100*$Exp->level;
            }
            $expData.='<tr>';
            $expData.='<td colspan="2"><span style="margin-left:'.$margin.'">'.$Exp->name.'</span></td>';
            $expData.='</tr>';
            $Ledgers=Ledgers::where('group_id', $Exp->id)->get();
            foreach ($Ledgers as $Ledger){
                $ob=CoreAccounts::opening_balance($end_date, $Ledger->id);
                $texp_balance+=$ob;
                $expData .= '<tr style="text-align: center">';
                $expData.='<td align="right"> '.ucfirst($Ledger->name).'</td>';
                $expData .= '<td align="right">'.CoreAccounts::dr_cr_balance($ob,2).'</td>';
                $expData .= '</tr>';
            }
        }
        $expData.='
                <tr class="bold-text bg-filled">
                    <td>Gross Expenses</td>
                    <td style="text-align: right; border-top: 1px solid black; border-bottom: 1px solid black;" align="right">'.CoreAccounts::dr_cr_balance($texp_balance, 2).'</td>
                </tr>';
        echo '<body style="border: 0.1pt solid #ccc"><table>'.$expData.'</table></body>';
    }
}
