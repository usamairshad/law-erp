<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Departments;
use App\Models\Admin\City;
use App\Models\Admin\Branches;
////
class AdminSettingController extends Controller
{
    public $successStatus = 200;
   
    public function departmentIndex()
    {

        $Departments = Departments::OrderBy('created_at','desc')->get();

        return response()->json([
            'Response Code:' => 200,
            'DateTime:' => date('Y-m-d h:i:s A'),
            'Total Records:' => count($Departments),
            'Departments' => $Departments
        ], $this->successStatus); 
    }

    public function cityIndex()
    {

        $City = City::OrderBy('created_at','desc')->get();

        return response()->json([
            'Response Code:' => 200,
            'DateTime:' => date('Y-m-d h:i:s A'),
            'Total Records:' => count($City),
            'Cities' => $City
        ], $this->successStatus); 
    }

    public function branchIndex()
    {

        $Branches = Branches::OrderBy('created_at','desc')->get();

        return response()->json([
            'Response Code:' => 200,
            'DateTime:' => date('Y-m-d h:i:s A'),
            'Total Records:' => count($Branches),
            'Branches' => $Branches
        ], $this->successStatus); 
    }
}
