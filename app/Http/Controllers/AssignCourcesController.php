<?php

namespace App\Http\Controllers;

use App\ActiveSessionSection;
use App\Models\Academics\ActiveSession;
use App\Models\Academics\ActiveSessionTeacher;
use App\Models\Academics\Course;
use App\Models\HRM\ClassTimeTable;
use Illuminate\Http\Request; 
use App\Models\HRM\TimeTable;
use Spatie\Permission\Models\Role;
use App\User;
use Illuminate\Support\Facades\Validator;
use DB;

class AssignCourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with("BranchDirectByUser", "staff")
            ->where('role_id',10)
            ->get();
            
        $courses = Course::all();

        $class_timetable_ids = ActiveSessionTeacher::pluck('class_timetable_id');
        $day='';
        $start_time='';
        $end_time='';
        if ($class_timetable_ids) {
          
            foreach ($class_timetable_ids as $key => $class_timetable_id) {
                $day = ClassTimeTable::where('id' , $class_timetable_id)->value('day');

                $time_table_id = ClassTimeTable::where('id', $class_timetable_id )->value('time_table_id');

               $start_time = TimeTable::where('id', $time_table_id)->value('start_time');

               $end_time = TimeTable::where('id', $time_table_id)->value('end_time');
            }
       
        }
        return view("hrm.AssignCources.index", compact('users', 'courses','day','start_time','end_time'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
       // dd($req);
       
        $coursesString = "";

        $messages = [
            "required" => 'Something went wrong'
        ];

        $rules = [
            
        ];

        $inputs = [
            "course_ids" => $req->courses,
            "active_session_section_id" => $req->active_session_section_id,
            "staff_id" => $req->staff_id,
            "class_timetable_id" => $req->class_timetable_id,
            "rates" => $req->rates,
            "rate_per_lecture"=>$req->rates/$req->days_in_month,
            "days_in_month" => $req->days_in_month

        ];

        $validator = Validator::make($inputs, $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }


        $temp=ActiveSessionTeacher::where('active_session_section_id',$req->active_session_section_id_usman)->where('course_ids',$req->course_ids)->first();

        if(!$temp)
        {
            $rate_per_lecture=$req->rates/$req->days_in_month;

            $timetable= ClassTimeTable::whereIn('id',$req['class_timetable_id'])->value('time_table_id');
                ActiveSessionTeacher::create([
                    "active_session_section_id" => $req->active_session_section_id_usman,
                    "staff_id" => $req->staff_id,
                    "course_ids" => $req->courses,
                    "rates" => $req->rates,
                    "days_in_month" => $req->days_in_month,
                    "class_timetable_id" => $timetable,
                    "rate_per_lecture"=> $rate_per_lecture
                ]);      

                foreach ($req['class_timetable_id'] as $key => $value) {
                    ClassTimeTable::where('id',$value)->where('active_session_section_id',$req->active_session_section_id_usman)->update([
                        'teacher_id'=>$req->staff_id
                    ]);
                }

            flash("Succesfully courses assign to teacher")->success()->important();
            return redirect()->back();
            
        }
        else
        {

            flash("Already Assinged")->warning()->important();
            return redirect()->back();
           
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function getActiveSessionSection($branch_id, $board_id, $program_id, $class_id)
    {
        \Log::info($branch_id);
        \Log::info($board_id);
        \Log::info($program_id);
        \Log::info($class_id);

        $activeSessionID = ActiveSession::where("branch_id", $branch_id)
            ->where("board_id", $board_id)
            ->where("program_id", $program_id)
            ->where("class_id", $class_id)
            ->where("status", "active")
            ->first();

        $activeSection = ActiveSessionSection::with('section')
            ->where("active_session_id", $activeSessionID->id)
            ->get();

        

        return $activeSection;
    }

    public function getActiveSessionSectionTimetable($activesessionsection_id)
    {   
       
        $data = ClassTimeTable::where('active_session_section_id' , $activesessionsection_id)->get();
        \Log::info($data);
        //return $data;
    }

    public function getActiveSessionSectionCourses($course_id, $branch_id,$board_id,$program_id,$class_id,$section_id)
    {   
        // \Log::info($course_id);
        // \Log::info($active_session_section_id_temp);
       
        //$data = ClassTimeTable::with('timetable')->where('active_session_section_id' , $active_session_section_id_temp)->where('subject_id' , $course_id)->get();
        $activeSessionID = ActiveSession::where("branch_id", $branch_id)
            ->where("board_id", $board_id)
            ->where("program_id", $program_id)
            ->where("class_id", $class_id)
            ->where("status", "active")
            ->first();
        \Log::info($branch_id);
        \Log::info($board_id);
        \Log::info($program_id);
        \Log::info($class_id);
        \Log::info($section_id);
        \Log::info($course_id);

        $activeSectionID = ActiveSessionSection::where("active_session_id", $activeSessionID->id)->where("section_id", $section_id)
            ->first();

       // \Log::info($activeSectionID);         

        $data = DB::table('class_time_tables')
            ->where('active_session_section_id' , $activeSectionID->id)->where('subject_id' , $course_id)->where('active_session_sections.id',$activeSectionID->id)
             ->join('active_session_sections', 'active_session_sections.id', '=', 'class_time_tables.active_session_section_id')
            ->join('time_tables', 'time_tables.id', '=', 'class_time_tables.time_table_id')
            ->select('class_time_tables.id', 'time_tables.title','active_session_sections.id as newActiveSessionSectionId')
            ->get();



            
        return $data;
    }
    public function getCoursesByStaff($id)
    {

        $activeSessionTeachers = ActiveSessionTeacher::with(
            "activesessionsection.activesession.branch",
            "activesessionsection.activesession.board",
            "activesessionsection.activesession.program",
            "activesessionsection.activesession.getClass",
            "activesessionsection.section"
        )
            ->where("staff_id", $id)
            ->wherehas("activesessionsection.activesession", function ($q) {
                $q->where("status", "active");
            })
            ->get()->toArray();



        $courseArray = [];

        foreach ($activeSessionTeachers as $key => $teacher) {

           \Log::info($teacher);
            $courseArray = explode(",", $teacher['course_ids']);

            $course = Course::whereIn("id", $courseArray)->get()->toArray();
            
            $activeSessionTeachers[$key]["courses"] = $course;
            $rates=$teacher['rates'];
            $activeSessionTeachers[$key]["rates"] = $rates;


            $activeSessionTeachers[$key]["rates"] = $rates;

          
        }
        
        return $activeSessionTeachers;
    }



    public function deleteActiveSessionTeacher($ASSID, $staff_id)
    {


        $obj = ActiveSessionTeacher::where("active_session_section_id", $ASSID)
            ->where("staff_id", $staff_id)->first();

        if (!empty($obj)) {
            $obj->delete();
        }

        flash("succesfully courses delete")->success()->important();
        return redirect()->back();
    }
    public function teacherCourseDetail()
    {   
      
            $data = DB::table('active_session_teachers')
            ->join('class_time_tables', 'active_session_teachers.class_timetable_id', '=', 'class_time_tables.id')
            ->join('time_tables', 'class_time_tables.time_table_id', '=', 'time_tables.id')
            ->select('active_session_teachers.staff_id', 'active_session_teachers.id','active_session_teachers.rates','active_session_teachers.days_in_month','class_time_tables.day','time_tables.start_time','time_tables.end_time','time_tables.title','time_tables.branch_id', 'class_time_tables.subject_id')
            ->get();

        return view("hrm.AssignCources.detail", compact('data'));
     

          
        //return $data;



    }
}
