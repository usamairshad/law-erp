<?php

namespace App\Http\Controllers\Asset;

use App\Models\Asset\Asset;
use App\User;
use App\Models\Asset\AssignAsset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class AssignAssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assign_assets = AssignAsset::with('asset')->get();
    
        return view('asset.assign-asset.index', compact('assign_assets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if ($request->hide_id == '' || $request->hide_id == 0){
            AssignAsset::create([
                'asset_id' => $request->asset,
                'user_id' => $request->assign_to,
                'assign_to'=>$request->assign_to,
                'quantity'=>$request->quantity,
                'role_id'=>$request->roles,
                'return_period'=>$request->return_period
            ]);
            $asset = Asset::find($request->asset);
            $asset->ware_house -= $request->quantity;
            $asset->save();
            return redirect()->back()->with('success','Data has been created');
        }
        else{
            AssignAsset::find($request->hide_id)->update([
                'asset_id' => $request->asset,
                'user_id' => $request->assign_to,
//                'room_id' => $request->asset,
                'assign_to'=>$request->assign_to,
                'quantity'=>$request->quantity,
                'role_id'=>$request->roles,
                'return_period'=>$request->return_period
                ]);
            $asset = Asset::find($request->asset);
            $asset->ware_house += $request->quantity;
            $asset->save();
            return redirect()->back()->with('success','Data has been updated');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function RoleRelatedUser(Request $request)
    {
        $data = User::where('role_id',$request->role_id)->get();
        return response()->json($data);
    }
}
