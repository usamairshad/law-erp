<?php

namespace App\Http\Controllers\Asset;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Asset\AssetType;
use Auth;

class AssetTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asset_types = AssetType::OrderBy('created_at','desc')->get();
        return view('asset.asset-type.index', compact('asset_types')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('asset.asset-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request, [
 
            'name' => 'required',
            'description' => 'required',
        ]);
        AssetType::create([
            'name'           => $request['name'],
            'description'    => $request['description'],
            'created_by'     => Auth::user()->id,    
        ]);

        flash('Record has been created successfully.')->success()->important();

        return redirect()->route('admin.asset_type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset_type = AssetType::find($id);
        return view('asset.asset-type.show', compact('asset_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset_types = AssetType::find($id);
        return view('asset.asset-type.edit', compact('asset_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
        {
        $this->validate($request, [
            'name' => 'required | max:10 | unique:erp_asset_types',
            'description' => 'required',
        ]);

        $asset_type = AssetType::findOrFail($id);
        $asset_type->update([
            'name' => $request['name'],
            'description' => $request['name'],
            'updated_by' => Auth::user()->id,
        ]);

        flash('Record has been updated successfully.')->success()->important();

        return redirect()->route('admin.asset_type.index');
    }

    public function active($id)
    {
        $asset_type = AssetType::findOrFail($id);
        $asset_type->update(['status' => 'Active']);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.asset_type.index');
    }

    /**
     * Inactivate FinancialYear from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {

        $asset_type = AssetType::findOrFail($id);
        $asset_type->update(['status' => 'InActive']);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.asset_type.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asset_type = AssetType::findOrFail($id);
        
        $asset_type->delete();

        flash('Record has been deleted successfully.')->success()->important();

        return redirect()->route('admin.asset_type.index');
    }
}
