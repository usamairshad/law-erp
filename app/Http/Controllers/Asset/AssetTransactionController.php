<?php

namespace App\Http\Controllers\Asset;

use App\Models\Asset\Asset;
use App\Models\Asset\AssetTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssetTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $records = AssetTransaction::with('asset')->get();
        return view('asset.asset-transaction.index',compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->button_value == 'Remove Quantity'){
            $this->validate($request,[
                'vendor' => 'required',
                'model' => 'required',
                'make_asset' => 'required',
                'quantity' => 'required',
            ]);
            AssetTransaction::create(['asset_id'=> $request->asset_id,
                'vendor'=>$request->vendor,
                'model'=>$request->model,
                'make_asset'=>$request->make_asset,
                'quantity'=>$request->quantity]);
            $asset = Asset::find($request->asset_id);
            $asset->quantity -= $request->quantity;
            $asset->save();

            return redirect()->back()->with('success','Quantity has been removed successfully!');
        }
        else{
            $this->validate($request,[
                'vendor' => 'required',
                'model' => 'required',
                'make_asset' => 'required',
                'quantity' => 'required',
            ]);
            AssetTransaction::create(['asset_id'=> $request->asset_id,
                'vendor'=>$request->vendor,
                'model'=>$request->model,
                'make_asset'=>$request->make_asset,
                'quantity'=>$request->quantity]);

            $asset = Asset::find($request->asset_id)->create([
                'quantity' => + $request->quantity,
                'ware_house' =>  $request->quantity,
            ]);
//            $asset->quantity += $request->quantity;
//            $asset->total_stock = $request->quantity;
//            $asset->stock_where_in_house = $request->quantity;
//            $asset->save();
            return redirect()->back()->with('success','Data has been created successfully!');


        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AssetTransaction::where('id',$id)->delete();
        return redirect()->back()->with('delete','Data has been deleted successfully!');
    }
    public function remove_quantity(Request $request){

        $asset = AssetTransaction::find($request->id);
        $asset->quantity -= $request->quantity;
        $asset->save();
        $asset = Asset::find($request->asset_id);
        $asset->quantity -= $request->quantity;
        $asset->save();
        return redirect()->back()->with('success','Quantity has been removed successfully!');
    }
}
