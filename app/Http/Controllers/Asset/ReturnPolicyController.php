<?php

namespace App\Http\Controllers\Asset;

use App\Models\Asset\ReturnPolicy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReturnPolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = ReturnPolicy::all();
        return view('asset.retun-policy.index',compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required',
            'description' => 'required',
        ]);
        $id=$request->id;
        if ($id==0 || $id==''){
            ReturnPolicy::create(['name'=>$request->name, 'description'=> $request->description]);
            return redirect()->back()->with('success','Data created successfully!');
        }

        else{
            ReturnPolicy::where('id',$id)->update(['name' => $request->name, 'description' =>
                $request->description]);
            return redirect()->back()->with('success','Data updated successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ReturnPolicy::where('id',$id)->delete();
        return redirect()->back()->with('deleted','Data Deleted successfully!');

    }
    public function status_inactive($id)
    {
        ReturnPolicy::where('id',$id)->update(['status' => 'InActive']);
        return redirect()->back()->with('success','Status Updated successfully!');
    }
    public function status_active($id)
    {
        ReturnPolicy::where('id',$id)->update(['status' => 'Active']);
        return redirect()->back()->with('success','Status Updated successfully!');
    }
}
