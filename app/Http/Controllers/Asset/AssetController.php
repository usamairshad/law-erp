<?php

namespace App\Http\Controllers\Asset;

use App\Models\Asset\Asset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Asset\AssetType;
use Auth;


class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $records =  Asset::all();
        return view('asset.asset.index',compact('records'));

//        $assets = Asset::OrderBy('created_at','desc')->get();
//        return view('asset.assets.index', compact('assets'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('asset.asset.create');
//        $asset_type= AssetType::pluck('name','id');
//        return view('asset.assets.create', compact('asset_type'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'name' => 'required',
            'model' => 'required',
            'description' => 'required',
            'asset_type_id' => 'required',
        ]);
      Asset::create(['name'=>$request->name, 'model'=>$request->model, 'asset_type_id'=>$request->asset_type_id,
                     'value'=>$request->value, 'description'=>$request->description ,'status'=>'Active',
          ]);
        return redirect()->route('admin.asset.index')->with('success','Data has been created successfully!');


//         $this->validate($request, [
//
//            'name' => 'required | max:10 | unique:assets',
//            'description' => 'required',
//            'value' => 'required',
//            'asset_type_id' => 'required',
//            'warranty' => 'required |min:1',
//            'expire_date' => 'required',
//            'quantity' => 'required',
//        ]);
//        Asset::create([
//            'name'               => $request['name'],
//            'description'        => $request['description'],
//            'value'              => $request['value'],
//            'asset_type_id'      => $request['asset_type_id'],
//            'warranty'           => $request['warranty'],
//            'expire_date'        => $request['expire_date'],
//            'quantity'           => $request['quantity'],
//            'created_by'         => Auth::user()->id,
//        ]);
//
//        flash('Record has been created successfully.')->success()->important();
//
//        return redirect()->route('admin.asset.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'model' => 'required',
            'description' => 'required',
            'asset_type_id' => 'required',
        ]);
        Asset::where('id',$id)->update(['name'=>$request->name,'model'=>$request->model, 'asset_type_id'=>$request->asset_type_id,
            'value'=>$request->value, 'description'=>$request->description ,'status'=>'Active',
        ]);
        return redirect()->route('admin.asset.index')->with('success','Data has been updates successfully!');
    }

    public function active($id)
    {
        $asset = Asset::findOrFail($id);
        $asset->update(['status' => 'Active']);

        flash('Record has been activated successfully.')->success()->important();

        return redirect()->route('admin.asset.index');
    }

    /**
     * Inactivate FinancialYear from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactive($id)
    {

        $asset = Asset::findOrFail($id);
        $asset->update(['status' => 'InActive']);

        flash('Record has been inactivated successfully.')->success()->important();

        return redirect()->route('admin.asset.index');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $records =  Asset::where('id',$id)->get();
        return view('asset.asset.edit',compact("records"));

//        $asset = Asset::find($id);
//
//        return view('assets.asset.edit', compact('asset'));

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asset = Asset::findOrFail($id);


        $asset->delete();
        return redirect()->back()->with('delete','Data has been deleted successfully!');

        
//        $asset->delete();
//
//        flash('Record has been deleted successfully.')->success()->important();
//
//        return redirect()->route('admin.asset.index');

    }
}
