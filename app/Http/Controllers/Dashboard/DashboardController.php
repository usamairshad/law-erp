<?php

namespace App\Http\Controllers\Dashboard;

use App\ActiveSessionSection;
use App\Helpers\Helper;
use App\Http\Controllers\LandManagement\BuildingManagementController;
use App\IdCard;
use App\Models\Academics\ActiveSession;
use App\Models\Admin\Ledgers;
use App\Models\Academics\ActiveSessionStudent;
use App\Models\Academics\Board;
use App\Models\Academics\BoardBranch;
use App\Models\Academics\BoardProgram;
use App\Models\Academics\Branch;
use App\Models\Academics\Company;
use App\Models\Academics\Course;
use App\Models\Academics\Program;
use App\Models\Academics\Staff;
use App\Models\Academics\Student;
use App\Models\Admin\Branches;
use App\Models\Agenda\Agenda;
use App\Models\HRM\Training;
use App\Models\HRM\Notifications;
use App\Models\HRM\LeaveRequests;
use App\Models\FeeCollection;
use App\Models\JobPostRequest;
use App\Models\JobPost;
use App\Models\HRM\Resign;
use App\Models\HRM\Termination;
use App\Models\LandManagement\Building;
use App\Roles;
use App\RoleUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class DashboardController extends Controller
{

    public function hr_dashboard()
    {
        $branch_count = Branches::count();
        $programs = Program::count();
        $boards = Board::count();
        $companies = Company::count();
        $buildings = Building::count();
        $staff  = Staff::count();
        $students = Student::count();
        $courses = Course::count();
        $agenda = Agenda::count();
        $warning = Notifications::count();
        $training = Training::count();
        $terminated = Agenda::count();
        $jobs = JobPostRequest::count();
        $candidates = JobPost::count();
        $resigned = Resign::count();
        $leaves = LeaveRequests::count();

        $sos_teacher = Staff::where('status','InActive')
                ->count();
                $sos_student = Student::where('status','InActive')
                ->count();
                $date = date("Y-m-d");
                return view('Dashboard.hr_head', compact('training','warning','terminated','jobs','candidates','resigned','leaves','agenda'));
    }
    public function index(){
        $branch_count = Branches::count();
        $programs = Program::count();
        $boards = Board::count();
        $companies = Company::count();
        $buildings = Building::count();
        $staff  = Staff::count();
        $students = Student::count();
        $courses = Course::count();
        $agenda = Agenda::count();
        $warning = Notifications::count();
        $training = Training::count();
        $terminated = Termination::count();
        $jobs = JobPostRequest::count();
        $candidates = Agenda::count();
        $resigned = Agenda::count();
        $leaves = LeaveRequests::count();

        $sos_teacher = Staff::where('status','InActive')
                ->count();
                $sos_student = Student::where('status','InActive')
                ->count();
                $date = date("Y-m-d");
        $defaulter = FeeCollection::where([['due_date','<',$date],['fee_status','=','1']])->count();
      
      
        $received=DashboardController::get_received_amount();
        foreach($received as $rec)
        {
            $received = $rec['received_amount'];
        }
      
        $total_received = $received;
        $received = DashboardController::conversion($received);
        $income=DashboardController::get_income_amount();
        foreach($income as $rec)
        {
            $income = $rec['income_amount'];
        }
        $total_income = $income;
       
        $income = DashboardController::conversion($income);
        $receivables = $total_income-$total_received;
    
        $receivables = DashboardController::conversion($receivables);
        $expense=DashboardController::get_expense_amount();

         $all_staff_count=DashboardController::getAllStaff();



        foreach($expense as $rec)
        {
            $expense = $rec['expense_amount'];
        }
        $total_expense = $total_income-$expense;
        $expense = DashboardController::conversion($expense);

        $total_expense = DashboardController::conversion($total_expense);
  



        $branch_wise_sos = [];
        $sos_title = [];
        $branches = Branches::where('company_id',1)->get();
        foreach ($branches as $key => $branch) {
         //   $sos_title[$key] = 
        }
        return view('Dashboard.dashboard', compact('training','warning','terminated','jobs','candidates','resigned','leaves','agenda','total_expense','branches','branch_count','defaulter'
            ,'programs','boards','companies',
            'buildings','staff', 'students', 'courses','sos_teacher','sos_student','received','income','receivables','expense','all_staff_count'));
    }
    public function conversion($n)
    {
      
        if($n != null)
        {
            // first strip any formatting;
            $n = (0+str_replace(",", "", $n));
    
            // is this a number?
            if (!is_numeric($n)) return false;
    
            // now filter it;
            if ($n > 1000000000000) return round(($n/1000000000000), 2).'T';
            elseif ($n > 1000000000) return round(($n/1000000000), 2).'B';
            elseif ($n > 1000000) return round(($n/1000000), 2).' M';
            elseif ($n > 1000) return round(($n/1000), 2).'K';
    
            return number_format($n);
        }
        
    }
    public function gm()
    {
        $branches = Branches::count();
        $programs = Program::count();
        $boards = Board::count();
        $companies = Company::count();
        $buildings = Building::count();
        $staff  = Staff::count();
        $students = Student::count();
        $courses = Course::count();
        $receivables = Ledgers::selectRaw('erp_ledgers.name')->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')->where('erp_ledgers.account_type_id','=','1')->get();
        $expenses = Ledgers::selectRaw('erp_ledgers.name')->join('erp_branches','erp_branches.id','=','erp_ledgers.branch_id')->where('erp_ledgers.account_type_id','=','3')->get();
        

        $branch_wise_sos = [];
        $sos_title = [];
        $branches = Branches::where('company_id',1)->get();
        foreach ($branches as $key => $branch) {
         //   $sos_title[$key] = 
        }
        return view('Dashboard.gm', compact('receivables','expenses'));
       // $branch_wise_walkin = [];
       /*  $branches_for_charts = Branches::where('company_id',1)->get();
        foreach ($branches_for_charts as $key => $branch) {

            //for sos
            $sos_student = Student::where('branch_id',$branch->id)
                            ->where('status','InActive')
                            ->count();
            $sos_teacher = Staff::where('branch_id',$branch->id)
                            ->where('status','InActive')
                            ->count();

            $branch_wise_sos[$key] = [$branch->name,$sos_student,$sos_teacher];

            //for walkin
            $walkin = DB::table('walk_in_registration')->where('branch_id',$branch->id)->count();
            
            $branch_wise_walkin[$key] = [$branch->name,$walkin];

        }
        $branch_wise_sos_json = json_encode($branch_wise_sos);




        return view('Dashboard.dashboard', compact('branches'
            ,'programs','boards','companies',
            'buildings','staff', 'students', 'courses','branch_wise_sos','branch_wise_walkin')); */
    }

    public  function get_income()
    {
        $income = Branches::SelectRaw('sum(amount) as a, erp_branches.name,erp_branches.id')
        ->join('erp_ledgers', 'erp_ledgers.branch_id', '=', 'erp_branches.id')
        ->join('erp_entry_items', 'erp_entry_items.ledger_id', '=', 'erp_ledgers.id')
        ->where([['account_type_id', '=', '4'], ['dc', '=', 'c' ],['erp_branches.company_id','=','1']])
        ->groupBy('erp_branches.id')->get();
      
        $array_branch = array();
        $array_income =array();
        $array_expense = array();
        $data_array = array();
        foreach($income as $i)
        {
            $name=$i->name;
            $inc =$i->a; 
            $expense_total =0;
            $expense = Branches::SelectRaw('sum(amount) as b, erp_branches.name')
            ->join('erp_ledgers', 'erp_ledgers.branch_id', '=', 'erp_branches.id')
            ->join('erp_entry_items', 'erp_entry_items.ledger_id', '=', 'erp_ledgers.id')
            ->where([['account_type_id', '=', '3'],['erp_branches.id', '=', $i->id], ['dc', '=', 'd' ]])
            ->groupBy('erp_branches.id')->get();
            foreach($expense as $e)
            {
                   $expense_total = $e->b; 
            }
            $array_branch[] = $name;
            $array_income[] = $inc;
            $array_expense[] = $expense_total;

        }
        $data_array = array(
            'branches'=>$array_branch,
            'income'=>$array_income,
            'expenses'=>$array_expense
        );
        return response()->json(['data' => $data_array]);
        
    
    }
    public  function get_income_two()
    {
        $income = Branches::SelectRaw('sum(amount) as a, erp_branches.name,erp_branches.id')
        ->join('erp_ledgers', 'erp_ledgers.branch_id', '=', 'erp_branches.id')
        ->join('erp_entry_items', 'erp_entry_items.ledger_id', '=', 'erp_ledgers.id')
        ->where([['account_type_id', '=', '4'], ['dc', '=', 'c' ],['erp_branches.company_id','=','2']])
        ->groupBy('erp_branches.id')->get();
      
        $array_branch = array();
        $array_income =array();
        $array_expense = array();
        $data_array = array();
        foreach($income as $i)
        {
            $name=$i->name;
            $inc =$i->a; 
            $expense_total =0;
            $expense = Branches::SelectRaw('sum(amount) as b, erp_branches.name')
            ->join('erp_ledgers', 'erp_ledgers.branch_id', '=', 'erp_branches.id')
            ->join('erp_entry_items', 'erp_entry_items.ledger_id', '=', 'erp_ledgers.id')
            ->where([['account_type_id', '=', '3'],['erp_branches.id', '=', $i->id], ['dc', '=', 'd' ]])
            ->groupBy('erp_branches.id')->get();
            foreach($expense as $e)
            {
                   $expense_total = $e->b; 
            }
            $array_branch[] = $name;
            $array_income[] = $inc;
            $array_expense[] = $expense_total;

        }
        $data_array = array(
            'branches'=>$array_branch,
            'income'=>$array_income,
            'expenses'=>$array_expense
        );
        return response()->json(['data' => $data_array]);
        
    
    }
    public  function get_income_three()
    {
        $income = Branches::SelectRaw('sum(amount) as a, erp_branches.name,erp_branches.id')
        ->join('erp_ledgers', 'erp_ledgers.branch_id', '=', 'erp_branches.id')
        ->join('erp_entry_items', 'erp_entry_items.ledger_id', '=', 'erp_ledgers.id')
        ->where([['account_type_id', '=', '4'], ['dc', '=', 'c' ],['erp_branches.company_id','=','3']])
        ->groupBy('erp_branches.id')->get();
      
        $array_branch = array();
        $array_income =array();
        $array_expense = array();
        $data_array = array();
        foreach($income as $i)
        {
            $name=$i->name;
            $inc =$i->a; 
            $expense_total =0;
            $expense = Branches::SelectRaw('sum(amount) as b, erp_branches.name')
            ->join('erp_ledgers', 'erp_ledgers.branch_id', '=', 'erp_branches.id')
            ->join('erp_entry_items', 'erp_entry_items.ledger_id', '=', 'erp_ledgers.id')
            ->where([['account_type_id', '=', '3'],['erp_branches.id', '=', $i->id], ['dc', '=', 'd' ]])
            ->groupBy('erp_branches.id')->get();
            foreach($expense as $e)
            {
                   $expense_total = $e->b; 
            }
            $array_branch[] = $name;
            $array_income[] = $inc;
            $array_expense[] = $expense_total;

        }
        $data_array = array(
            'branches'=>$array_branch,
            'income'=>$array_income,
            'expenses'=>$array_expense
        );
        return response()->json(['data' => $data_array]);
        
    
    }


    public  function get_payable()
    {
        $income = Branches::SelectRaw('erp_branches.name, erp_branches.id as bid')
        ->leftjoin('erp_ledgers', 'erp_ledgers.branch_id', '=', 'erp_branches.id')
        ->where('account_type_id', '=', '1')
        ->groupBy('erp_branches.id')->get();
      
        $array[] = 'Current Month';
        $amount[] = 'Amount';
        foreach($income as $i)
        {
            $name = $i->name;
            $get_receivable_amount = Ledgers::join('erp_entry_items','erp_entry_items.ledger_id','=','erp_ledgers.id')
            ->where([['account_type_id', '=', '5'],['erp_ledgers.branch_id', '=', $i->bid]])->get();
            $dr=0; $cr=0;

            foreach ( $get_receivable_amount as $Ent) {
               
                 if ($Ent->dc == 'd') {
                     $dr = $Ent->amount;
                 } else {
                     $cr = floatval($Ent->amount);
                   
     
                 }
                }
            $total = $cr-$dr;

            $array[] = $name;
                $amount[] = $total; 
        }
        $json_data = json_encode($array);
        $json_dataa = json_encode($amount);
       
        return response()->json(['data' => $json_data,'amount' => $json_dataa]);
        
    
    }
    public  function get_receivables_one()
    {
        $income = Branches::SelectRaw('erp_branches.name, erp_branches.id as bid')
        ->leftjoin('erp_ledgers', 'erp_ledgers.branch_id', '=', 'erp_branches.id')
        ->where('account_type_id', '=', '1')
        ->where('erp_branches.company_id','1')
        ->groupBy('erp_branches.id')->get();
   
        $array[] = 'Current Month';
        $amount[] = 'Amount';
        $array_branch = array();
        $array_receivables =array();
        $data_array = array();
        foreach($income as $i)
        {
            $name = $i->name;
            $get_receivable_amount = Ledgers::join('erp_entrry_iteems','erp_entry_items.ledger_id','=','erp_ledgers.id')
            ->where([['account_type_id', '=', '1'],['erp_ledgers.branch_id', '=', $i->bid]])->get();
            $dr=0; $cr=0;
            foreach ( $get_receivable_amount as $Ent) {
               
                 if ($Ent->dc == 'd') {
                     $dr = $Ent->amount;
                 } else {
                     $cr = floatval($Ent->amount);
             
     
                 }
                }
            $total = $dr-$cr;

            $array_branch[] = $name;
            $array_receivables[] = $total; 
        }
        $data_array = array(
            'branches'=>$array_branch,
            'receivables'=>$array_receivables,
        );
      
    return response()->json(['data' => $data_array]);
    
    }






    public function board_branches(Request $request){
        $records = ActiveSession::where('branch_id', $request->branch_id)->with('board')
            ->groupBy('board_id')
            ->get();
        
      return response()->json($records);
    }
    public function boards(Request $request){
        $records = ActiveSession::where('board_id', $request->board_id)->with('program')
            ->groupBy('program_id')
            ->get();
        return response()->json($records);
    }
    public function programs(Request $request){
        $records = ActiveSession::where('program_id', $request->program_id)->with('classes')
            ->groupBy('class_id')
            ->get();
        return response()->json($records);
    }
    public function total_companies(){
        $records = Company::all();
        return view('Dashboard.companies', compact('records'));
    }
    public function total_branches(){
        $records = Branches::all();
        return view('Dashboard.branches', compact('records'));

    }
    public function total_boards(){
        $records = Board::all();
        return view('Dashboard.boards', compact('records'));

    }
    public function total_buildings(){
        $records = Building::all();
        return view('Dashboard.buildings', compact('records'));

    }
    public function total_programs(){
        $records = Program::all();
        return view('Dashboard.programs', compact('records'));

    }
    public function total_staffs(){
        $records = Staff::all();
//        $users = DB::table('staff')
//            ->join('role_user', 'staff.user_id', '=', 'role_user.user_id')
//            ->join('roles', 'roles.id', '=', 'role_user.role_id')
//            ->select('staff.*', 'roles.name')
//            ->get();
//        dd($users);
        return view('Dashboard.staff', compact('records'));

    }
    public function total_students(){
        $records = Branches::all();
        return view('Dashboard.students', compact('records'));

    }
    public function total_courses(){
        $records = Course::all();
        return view('Dashboard.courses', compact('records'));

    }
    public function related_branch($id){
        $records = Branches::where('company_id',$id)->get();
        return view('Dashboard.branches', compact('records'));

    }
    public function related_program($id){
        $records = BoardProgram::where('board_id',$id)->with('program')->get();

    return view('Dashboard.programs', compact('records'));

        }
    public function related_board($id){
        //$records = BoardBranch::where('branch_id',$id)->with('board')->get();
        $records = ActiveSession::where('branch_id', $id)->with('board')
            ->groupBy('board_id')
            ->get();

        return view('Dashboard.boards', compact('records'));

    }
    public function relate_programs($id){
        $records = ActiveSession::where('program_id',$id)->groupBy('class_id')->get();

        return view('Dashboard.classes', compact('records'));

    }
    public function studentCount($id){

        $active_sessions = ActiveSession::where('class_id',$id)->get();

        foreach ($active_sessions as $a_s){
            $active_session_section = ActiveSessionSection::where('active_session_id',$a_s->id)->get();
            foreach ($active_session_section as $section){
                $count = ActiveSessionStudent::where('active_session_section_id',$section->id)->count();
                return response()->json($count);
            }


        }

    }

    public function get_received_amount()
    {
        $income = Ledgers::SelectRaw('sum(erp_entry_items.amount) as received_amount ')
        ->join('erp_entry_items','erp_entry_items.ledger_id','=','erp_ledgers.id')
        ->where('account_type_id', '=', '1')
        ->where('dc','=','c')
        ->get();
         return $income; 
        }
        public function get_income_amount()
        {
            $income = Ledgers::SelectRaw('sum(erp_entry_items.amount) as income_amount ')
            ->join('erp_entry_items','erp_entry_items.ledger_id','=','erp_ledgers.id')
            ->where('account_type_id', '=', '4')
            ->where('dc','=','c')
            ->get();
       
             return $income; 
            }

            public function get_expense_amount()
            {
                $income = Ledgers::SelectRaw('sum(erp_entry_items.amount) as expense_amount ')
                ->join('erp_entry_items','erp_entry_items.ledger_id','=','erp_ledgers.id')
                ->where('account_type_id', '=', '3')
                ->where('dc','=','d')
                ->get();
                 return $income; 
                }
                public function get_payable_amount()
                {
                    $income = Ledgers::SelectRaw('sum(erp_entry_items.amount) as payable_amount ')
                    ->join('erp_entry_items','erp_entry_items.ledger_id','=','erp_ledgers.id')
                    ->where('account_type_id', '=', '5')
                    ->where('dc','=','d')
                    ->get();
                     return $income; 
                    }
    public function filter_staff(Request $request){
    }


    public function getAllStaff()

    {
            $company_1 = Branch::SelectRaw('count(*) as total_staff, erp_branches.name as branch_name')
                    ->join('staff', 'staff.branch_id', '=', 'erp_branches.id')
                    ->groupBy('erp_branches.id')
                    ->get();
                    $array_branches =array();
        $array_staff = array();
        foreach($company_1 as $i)
        {
            $name = $i->branch_name;
            $staff = $i->total_staff;
           
           

            $array_branches[] = $name;
            $array_staff[] = $staff; 
        }
        $data_array = array(
            'branches'=>    $array_branches,
            'staff'=>  $array_staff,
        );
      
    return response()->json(['data' => $data_array]);
      
  
 
    }

    
    public function getAllStudents()

    {
            $company_1 = Branch::SelectRaw('count(*) as total_student, erp_branches.name as branch_name')
                    ->join('students', 'students.branch_id', '=', 'erp_branches.id')
                    ->where('students.status','Active')
                    ->groupBy('erp_branches.id')
                    ->get();
                    $array_branches =array();
        $array_staff = array();
        foreach($company_1 as $i)
        {
            $name = $i->branch_name;
            $staff = $i->total_student;
           
           

            $array_branches[] = $name;
            $array_staff[] = $staff; 
        }
        $data_array = array(
            'branches'=>    $array_branches,
            'students'=>  $array_staff,
        );
      
    return response()->json(['data' => $data_array]);
    }
    public function getAllSOS()

    {
            $company_1 = Branch::SelectRaw('count(*) as total_student, erp_branches.name as branch_name')
                    ->join('students', 'students.branch_id', '=', 'erp_branches.id')
                    ->where('students.status','inActive')
                    ->groupBy('erp_branches.id')
                    ->get();
                    $array_branches =array();
        $array_staff = array();
        foreach($company_1 as $i)
        {
            $name = $i->branch_name;
            $staff = $i->total_student;
           
           

            $array_branches[] = $name;
            $array_staff[] = $staff; 
        }
        $data_array = array(
            'branches'=>    $array_branches,
            'students'=>  $array_staff,
        );
      
    return response()->json(['data' => $data_array]);
    }

    public function getAllSOS_staff()

    {
            $company_1 = Branch::SelectRaw('count(*) as total_staff, erp_branches.name as branch_name')
            ->join('staff', 'staff.branch_id', '=', 'erp_branches.id')
                    ->where('staff.status','inActive')
                    ->groupBy('erp_branches.id')
                    ->get();
                    $array_branches =array();
        $array_staff = array();
        foreach($company_1 as $i)
        {
            $name = $i->branch_name;
            $staff = $i->total_staff;
           
           

            $array_branches[] = $name;
            $array_staff[] = $staff; 
        }
        $data_array = array(
            'branches'=>    $array_branches,
            'staff'=>  $array_staff,
        );
      
    return response()->json(['data' => $data_array]);
    }
    public function gettotal_Revenue()

    {
            $company_1 = Branch::SelectRaw('count(*) as total_staff, erp_branches.name as branch_name')
            ->join('staff', 'staff.branch_id', '=', 'erp_branches.id')
                    ->where('staff.status','inActive')
                    ->groupBy('erp_branches.id')
                    ->get();
                    $array_branches =array();
        $array_staff = array();
        foreach($company_1 as $i)
        {
            $name = $i->branch_name;
            $staff = $i->total_staff;
           
           

            $array_branches[] = $name;
            $array_staff[] = $staff; 
        }
        $data_array = array(
            'branches'=>    $array_branches,
            'staff'=>  $array_staff,
        );
      
    return response()->json(['data' => $data_array]);
    }

        }
