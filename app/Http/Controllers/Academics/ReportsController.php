<?php

namespace App\Http\Controllers\Academics;

use App\Helpers\AccountsList;
use App\Helpers\CoreAccounts;
use App\Helpers\GroupsTree;
use App\Helpers\LedgersTree;
use App\Models\Admin\AccountTypes;
use App\Models\Admin\Branches;
use App\Models\Admin\Currencies;
use App\Models\Admin\Departments;
use App\Models\Admin\Employees;
use App\Models\Admin\Entries;
use App\Models\Admin\EntryItems;
use App\Models\Admin\EntryTypes;
use App\Models\Admin\Groups;
use App\Models\Admin\StaffSalaries;
use App\Models\Admin\Students;
use App\Models\Admin\Ledgers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Admin\TaxSettings;
use App\Models\Academics\Staff;
use App\Models\Academics\Section;
//Excel Export Library
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use View;
use Config;
use Auth;
use DB;
use App\Models\Academics\Session;

class ReportsController extends Controller
{
   

    /*
     * ------------------------------------------------------
     * Ledger Statement Report End
     * ------------------------------------------------------
     */
    public function nominal_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.nominal.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }
    public function nominal_report_print(Request $request){
        
        //dd($request);
       
        $user = Auth::user();
        if($user !="")
        {
            // if ($request->company_id && $request->branch_id) {
            //     # code...
            // }
            // elseif($request->company_id && $request->branch_id && $request->boards)
            // {

            // }elseif($request->company_id && $request->branch_id && $request->boards && $request->programs)
            // {

            // }elseif($request->company_id && $request->branch_id && $request->boards && $request->programs && $request->classes)

            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date')
            ->get();

            if($request->type=='print'){
                return view('Reports.nominal.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }



     public function student_contact()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.student_contact.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function student_contact_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {
            // if ($request->company_id && $request->branch_id) {
            //     # code...
            // }
            // elseif($request->company_id && $request->branch_id && $request->boards)
            // {

            // }elseif($request->company_id && $request->branch_id && $request->boards && $request->programs)
            // {

            // }elseif($request->company_id && $request->branch_id && $request->boards && $request->programs && $request->classes)

            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.mobile_1')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.student_contact.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }




     public function student_address()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.student_address.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function student_address_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {
            // if ($request->company_id && $request->branch_id) {
            //     # code...
            // }
            // elseif($request->company_id && $request->branch_id && $request->boards)
            // {

            // }elseif($request->company_id && $request->branch_id && $request->boards && $request->programs)
            // {

            // }elseif($request->company_id && $request->branch_id && $request->boards && $request->programs && $request->classes)

            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.student_address.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }


         public function default_student_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.student_defaulters.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function default_student_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {
            
            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.student_defaulters.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }


    public function defaulter_amt_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.defaulter_amt_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function default_amt_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {


            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.defaulter_amt_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }


    public function summary_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.summary_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function summary_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {

            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.summary_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }


    public function concession_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.concession_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function concession_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {
            
            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.concession_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }



     public function head_wise_fee_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.head_wise_fee_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function head_wise_fee_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {

            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.head_wise_fee_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }


    public function head_wise_summary_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.head_wise_summary_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function head_wise_summary_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {
          
            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.head_wise_summary_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }




     public function head_wise_revenue_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.head_wise_revenue_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }
       public function head_wise_revenue_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {


            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.head_wise_revenue_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

     public function head_wise_initial_security_fee_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.head_wise_initial_security_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function head_wise_initial_security_fee_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {


            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.head_wise_initial_security_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }




    public function head_wise_lms_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.head_wise_lms_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function head_wise_lms_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {


            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.head_wise_lms_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }



   public function head_wise_annual_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.head_wise_annual_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function head_wise_annual_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {


            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.head_wise_annual_report.print',compact('student'));
            }
            else
            {
          return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }






    public function head_wise_partyfund_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.head_wise_partyfund_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function head_wise_partyfund_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {

            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.head_wise_partyfund_report_print_report.print',compact('student'));
            }
            else
            {
            return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }


        public function revenue_receivable_received_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
        return view('reports.revenue_receivable_received_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function revenue_receivable_received_report_print(Request $request){
        
      
       
        $user = Auth::user();
        if($user !="")
        {

            $input  = $request->all();
            $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            ->join('boards','boards.id','=','students.board_id')
            ->join('programs','programs.id','=','students.program_id')
            ->join('classes','classes.id','=','students.class_id')
             ->join('sections','sections.id','=','students.section_id')
            //->where('branch_id' , $request->branch_id)
            ->Where('board_id', $request->boards)
            //->orWhere('program_id', $request->programs)
            //->orWhere('class_id', $request->classes)
            ->orWhere('section_id', $request->sections)
            ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            ->get();

       
           
            if($request->type=='print'){
                return view('Reports.revenue_receivable_received_report.print',compact('student'));
            }
            else
            {
            return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }


    public function yearly_projection_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
            $sessions = Session::all();
       
        return view('reports.yearly_projection_report.index' , compact('sessions'));
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function yearly_projection_report_print(Request $request){
        
        
       
        $user = Auth::user();
        if($user !="")
        {

            $input  = $request->all();
            $student = DB::table('students')->get();
            // $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            // // ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            // // ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            // // ->join('boards','boards.id','=','students.board_id')
            // // ->join('programs','programs.id','=','students.program_id')
            // // ->join('classes','classes.id','=','students.class_id')
            // //  ->join('sections','sections.id','=','students.section_id')
            // //->where('branch_id' , $request->branch_id)
            // //->Where('board_id', $request->boards)
            // //->orWhere('program_id', $request->programs)
            // //->orWhere('class_id', $request->classes)
            // // ->orWhere('section_id', $request->sections)
            // // ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            // ->get();

       
           
            if($request->type=='print'){
                return view('Reports.yearly_projection_report.print',compact('student'));
            }
            else
            {
            return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

    public function admin_detail_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
            
        return view('reports.admin_detail_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function admin_detail_report_print(Request $request){
        
        
       
        $user = Auth::user();
        if($user !="")
        {

            $input  = $request->all();
            $student = DB::table('students')->get();
            // $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            // // ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            // // ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            // // ->join('boards','boards.id','=','students.board_id')
            // // ->join('programs','programs.id','=','students.program_id')
            // // ->join('classes','classes.id','=','students.class_id')
            // //  ->join('sections','sections.id','=','students.section_id')
            // //->where('branch_id' , $request->branch_id)
            // //->Where('board_id', $request->boards)
            // //->orWhere('program_id', $request->programs)
            // //->orWhere('class_id', $request->classes)
            // // ->orWhere('section_id', $request->sections)
            // // ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            // ->get();

       
           
            if($request->type=='print'){
                return view('Reports.admin_detail_report.print',compact('student'));
            }
            else
            {
            return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

    public function strength_comprision_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
            
        return view('reports.strength_comprision_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function strength_comprision_report_print(Request $request){
        
        
       
        $user = Auth::user();
        if($user !="")
        {   

            $input  = $request->all();
            $student = DB::table('students')->get();
            // $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            // // ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            // // ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            // // ->join('boards','boards.id','=','students.board_id')
            // // ->join('programs','programs.id','=','students.program_id')
            // // ->join('classes','classes.id','=','students.class_id')
            // //  ->join('sections','sections.id','=','students.section_id')
            // //->where('branch_id' , $request->branch_id)
            // //->Where('board_id', $request->boards)
            // //->orWhere('program_id', $request->programs)
            // //->orWhere('class_id', $request->classes)
            // // ->orWhere('section_id', $request->sections)
            // // ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            // ->get();

       
           
            if($request->type=='print'){
                return view('Reports.strength_comprision_report.print',compact('student'));
            }
            else
            {
            return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

    public function classwise_strength_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
            
        return view('reports.classwise_strength_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function classwise_strength_report_print(Request $request){
        
        
       
        $user = Auth::user();
        if($user !="")
        {   

            $input  = $request->all();
            $student = DB::table('students')->get();
            // $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            // // ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            // // ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            // // ->join('boards','boards.id','=','students.board_id')
            // // ->join('programs','programs.id','=','students.program_id')
            // // ->join('classes','classes.id','=','students.class_id')
            // //  ->join('sections','sections.id','=','students.section_id')
            // //->where('branch_id' , $request->branch_id)
            // //->Where('board_id', $request->boards)
            // //->orWhere('program_id', $request->programs)
            // //->orWhere('class_id', $request->classes)
            // // ->orWhere('section_id', $request->sections)
            // // ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            // ->get();

       
           
            if($request->type=='print'){
                return view('Reports.classwise_strength_report.print',compact('student'));
            }
            else
            {
            return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }
    public function classwise_revenue_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
            
        return view('reports.classwise_revenue_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function classwise_revenue_report_print(Request $request){
        
        
       
        $user = Auth::user();
        if($user !="")
        {   

            $input  = $request->all();
            $student = DB::table('students')->get();
            // $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            // // ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            // // ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            // // ->join('boards','boards.id','=','students.board_id')
            // // ->join('programs','programs.id','=','students.program_id')
            // // ->join('classes','classes.id','=','students.class_id')
            // //  ->join('sections','sections.id','=','students.section_id')
            // //->where('branch_id' , $request->branch_id)
            // //->Where('board_id', $request->boards)
            // //->orWhere('program_id', $request->programs)
            // //->orWhere('class_id', $request->classes)
            // // ->orWhere('section_id', $request->sections)
            // // ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            // ->get();

       
           
            if($request->type=='print'){
                return view('Reports.classwise_revenue_report.print',compact('student'));
            }
            else
            {
            return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }
    public function comparision_classwise_strength_report()
    {
        $user = Auth::user();
        
        if($user !="")
        {
            
        return view('reports.comparision_classwise_strength_report.index');
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

       public function comparision_classwise_strength_report_print(Request $request){
        
        
       
        $user = Auth::user();
        if($user !="")
        {   

            $input  = $request->all();
            $student = DB::table('students')->get();
            // $student =DB::table('students')->join('erp_branches','erp_branches.id','=','students.branch_id')
            // // ->join('erp_city','erp_city.id','=','erp_branches.city_id')
            // // ->join('erp_companies','erp_companies.id','=','erp_branches.company_id')
            // // ->join('boards','boards.id','=','students.board_id')
            // // ->join('programs','programs.id','=','students.program_id')
            // // ->join('classes','classes.id','=','students.class_id')
            // //  ->join('sections','sections.id','=','students.section_id')
            // //->where('branch_id' , $request->branch_id)
            // //->Where('board_id', $request->boards)
            // //->orWhere('program_id', $request->programs)
            // //->orWhere('class_id', $request->classes)
            // // ->orWhere('section_id', $request->sections)
            // // ->select('students.reg_no','erp_branches.name as branch_name', 'boards.name as board_name' , 'programs.name as program_name' , 'classes.name as class_name', 'sections.name as section_name' , 'students.first_name as first_name' , 'students.last_name as last_name', 'students.date_of_birth' , 'students.reg_date', 'students.address')
            // ->get();

       
           
            if($request->type=='print'){
                return view('Reports.classwise_revenue_report.print',compact('student'));
            }
            else
            {
            return response()->json(['data' => $student]);
            }
        }
        else
        {
            return redirect()->route('auth.login');
        }
    }

}