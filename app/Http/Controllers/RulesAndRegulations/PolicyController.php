<?php

namespace App\Http\Controllers\RulesAndRegulations;

use App\Models\RulesAndRegulations\CategoryPolicy;
use App\Models\RulesAndRegulations\CodeOfConduct;
use App\Models\RulesAndRegulations\Policy;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Mpdf\Tag\P;

class PolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $records = Policy::all();
        return view('RulesAndRegulations.policy', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'policy_name' => 'required',
            'policy_description' => 'required'
        ]);

        $id=$request->id;
        if ($id==0 || $id==''){

            Policy::create(['category_policy_id' => $request->category_policy_id,'policy_name' => $request->policy_name, 'policy_description'=>$request->policy_description]);
            return Redirect::back()->with('success','Data created successfully!');
        }
        else{
            Policy::where('id', $id)->update(['category_policy_id' => $request->category_policy_id,'policy_name' => $request->policy_name, 'policy_description'=>$request->policy_description]);
            return Redirect::back()->with('success','Data updated successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Policy::where('id',$id)->delete();
        return Redirect::back()->with('delete','Data Deleted successfully!');
    }
    public function completerolespolicy(){
        $policy_data = CategoryPolicy::with('codeofconduct','policy')->get();
        return view('RulesAndRegulations.completerulespolicies', compact('policy_data'));
    }
    public function relatedCodeofConduct($id){
        $policy = Policy::where('category_policy_id',$id)->first();
        $policy_category = CategoryPolicy::where('id',$id)->first();
        $conducts = CodeOfConduct::where('category_policy_id',$policy->category_policy_id)->first();
        return view('RulesAndRegulations.report',compact('policy','conducts','policy_category'));
//        foreach ($records as $record){
//            $policy_description = $record->policy_description;
//            $conducts = CodeOfConduct::where('category_policy_id',$record->category_policy_id)->get();
//            return view('RulesAndRegulations.report',compact('policy_description','conducts','policy_category'));
//
//        }
    }
}
