<?php

namespace App\Http\Controllers\RulesAndRegulations;

use App\Models\RulesAndRegulations\CodeOfConduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CodeOfConductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = CodeOfConduct::all();
        return view('RulesAndRegulations.codeofconduct', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code_of_conduct_name' => 'required',
            'code_of_conduct_description' => 'required'
        ]);

        $id=$request->id;
        if ($id==0 || $id==''){

            CodeOfConduct::create(['category_policy_id' => $request->category_policy_id,'codeofconduct_name' => $request->code_of_conduct_name, 'codeofconduct_description'=>$request->code_of_conduct_description]);
            return Redirect::back()->with('success','Data created successfully!');
        }
        else{
            CodeOfConduct::where('id', $id)->update(['category_policy_id' => $request->category_policy_id,'codeofconduct_name' => $request->code_of_conduct_name,
                'codeofconduct_description' => $request->code_of_conduct_description]);
            return Redirect::back()->with('success','Data updated successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CodeOfConduct::where('id',$id)->delete();
        return Redirect::back()->with('delete','Data Deleted successfully!');
    }
}
