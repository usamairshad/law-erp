<?php

namespace App\Http\Controllers\RulesAndRegulations;

use App\Models\RulesAndRegulations\CategoryPolicy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CategoryPolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $records = CategoryPolicy::all();
        return view('RulesAndRegulations.categorypolicy', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('create');
//        return view('RulesAndRegulations.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'categorypolicy_name' => 'required',
            'categorypolicy_description' => 'required'
        ]);

        $inputs = $request->input();
        $id=$request->id;
        if ($id==0 || $id==''){

            CategoryPolicy::create($inputs);
            return Redirect::back()->with('success','Data created successfully!');
        }
        else{
            CategoryPolicy::where('id', $id)->update(['categorypolicy_name' => $request->categorypolicy_name,
                'categorypolicy_description' => $request->categorypolicy_description]);
            return Redirect::back()->with('success','Data updated successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd('edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CategoryPolicy::where('id',$id)->delete();
        return Redirect::back()->with('delete','Data Deleted successfully!');
    }
}
