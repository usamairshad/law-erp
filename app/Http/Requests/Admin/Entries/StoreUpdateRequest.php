<?php
namespace App\Http\Requests\Admin\Entries;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'voucher_date' => 'required',
            'entry_type_id' => 'required|numeric',
            'branch_id' => 'required|numeric',
            'employee_id' => 'required|numeric',
            'department_id' => 'sometimes|nullable|numeric',
            'narration' => 'required',
            'dr_total' => 'required|numeric|min:1|same:cr_total',
            'cr_total' => 'required|numeric|min:1|same:dr_total',
            'diff_total' => 'required|numeric|min:0|max:0',
            'entry_items' => 'required',
        ];

        if($this->request->get('entry_items')) {
            $entry_items = $this->request->get('entry_items');
            foreach($entry_items['counter'] as $key => $val)
            {
                $rules['entry_items.ledger_id.'. $val] = 'required';
                $rules['entry_items.dr_amount.'. $val] = 'required_if:entry_items.cr_amount,0|numeric';
                $rules['entry_items.cr_amount.'. $val] = 'required_if:entry_items.dr_amount,0|numeric';
                $rules['entry_items.narration.'. $val] = 'required';
            }
        }

        return $rules;
    }
}
