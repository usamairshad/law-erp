<?php
namespace App\Http\Requests\Admin\FinancialYears;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');

        return [
            'name' => 'required|unique:financial_years,name,'. $id,
            'start_date' => 'required',
            'end_date' => 'required',
        ];
    }
}
