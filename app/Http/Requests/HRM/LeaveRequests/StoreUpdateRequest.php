<?php
namespace App\Http\Requests\HRM\LeaveRequests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'employee_id' => 'required',
            'leave_type_id' => 'required',
            'work_shift_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'total_days' => 'required|numeric|min:1',
        ];
    }
}
