<?php
namespace App\Http\Requests\HRM\PayRoll;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'year' => 'required',
            'month' => 'required',
            'basic_salary' => 'required',
            'sc_amount' => 'required',
            'house_rent' => 'required',
            'utilities' => 'required',
            'overtime' => 'required',
            'total_pf' => 'required',
            'leaves' => 'required',
            'short_leaves' => 'required',
            'half_leaves' => 'required',
            'sandwich_leaves' => 'required',
            'presents' => 'required',
            'allowed_leaves' => 'required',
            'deducted_amount' => 'required',
            'total_bonus' => 'required',
            'total_commission' => 'required',
            'gross_total' => 'required',
            'tax_percentage' => 'required',
            'tax_amount' => 'required',
            'total_after_tax' => 'required',
//            'paid_date' => 'required',
//            'cash_date' => 'required',
//            'salary_type' => 'required',
            'paid_amount' => 'required',
            'pending_amount' => 'required',
//            'payment_method' => 'required'
        ];
    }
}
