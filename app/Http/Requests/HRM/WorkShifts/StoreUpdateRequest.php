<?php
namespace App\Http\Requests\HRM\WorkShifts;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'hours_per_day' => 'required|numeric',
            'shift_start_time' => 'required',
            'shift_end_time' => 'required',
            'working_hours_per_day' => 'required',
            'break_hours_per_day' => 'required',
        ];
    }
}
