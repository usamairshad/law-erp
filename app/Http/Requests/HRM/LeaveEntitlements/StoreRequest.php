<?php
namespace App\Http\Requests\HRM\LeaveEntitlements;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'sometimes|numeric',
            'leave_type_id' => 'required|numeric',
            'no_of_days' => 'required|numeric',
            'days_used' => 'sometimes|numeric',
            'start_date' => 'required|date|date_format:Y-m-d',
            'end_date' => 'required|date|date_format:Y-m-d',
            'credited_date' => 'sometimes|date|date_format:Y-m-d',
        ];
    }
}
