<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Models\Marketing\SalePipelines;
use App\Helpers\Marketing;
class PerformanceSecurityCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performance Security Reminder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // any due payment security command

        $curr_date = date('Y-m-d');
        $SalePipelines = SalePipelines::salepipelinePaymentDate($curr_date);
        $curr_date = date('Y-m-d');
        $Securities = SalePipelines::salepipelineSecurityDate($curr_date);
        //  $SalePipeline = SalePipelines::salepipelineById($id);
        //   dd($SalePipelines);

        foreach ($Securities as $key => $val){

            $SalePipeline =  SalePipelines::findOrFail($val->id);
            $user_ids = Marketing::getMrkTreeOf($SalePipeline->assigned_to);
            $user_emails = Marketing::user_emails($user_ids);
            // dd($user_emails);
            if($SalePipeline && count($user_emails) > 0 ){

                $user_emails = $user_emails->toArray();
                // email added for assurity of ending of email
                array_push($user_emails, 'ahsan.ullah@netrootstech.com');

                Mail::send('marketing.salepipelines.securitymail', ['SalePipelines' => $SalePipeline->id,'Amount' =>$val->performance_security], function ($m) use ($user_emails) {
                    $m->from('info@erpnetroots.com', 'Your Reminder');

                    $m->to($user_emails)->subject('Your Reminder!');
                    //  echo 'sent';
                });
            }

        }



        foreach ($SalePipelines as $key => $val){

            $SalePipeline =  SalePipelines::findOrFail($val->id);
            $user_ids = Marketing::getMrkTreeOf($SalePipeline->assigned_to);
            $user_emails = Marketing::user_emails($user_ids);
            // dd($user_emails, $SalePipeline, $val->earnest_money_amount);
            if($SalePipeline && count($user_emails) > 0 ){

                $user_emails = $user_emails->toArray();
                // email added for assurity of ending of email
                array_push($user_emails, 'ahsan.ullah@netrootstech.com');

                Mail::send('marketing.salepipelines.cdrmail', ['SalePipelines' => $SalePipeline->id,'Amount' =>$val->earnest_money_amount], function ($m) use ($user_emails) {
                    $m->from('info@erpnetroots.com', 'Your Reminder');

                    $m->to($user_emails )
                        ->subject('Your Reminder!');
                    echo 'sent' ;
                });
            }

        }
//
        // if(count($SalePipelines) > 0 || count($Securities) > 0){

        //     Mail::send('marketing.salepipelines.mail', ['SalePipelines' => $SalePipelines,'Securities' => $Securities], function ($m) use ($SalePipelines,$Securities) {
        //         $m->from('info@erpnetroots.com', 'Your Reminder');

        //         $m->to('ahsan.ullah@netrootstech.com', 'Ahsan')->subject('Your Reminder!');
        //     });
        // }

        $this->info("Email sent succesfully ! ");
    }
}
