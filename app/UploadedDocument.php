<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadedDocument extends Model
{
    protected $fillable=[
        "group_event_id",
        "files_path",
    ];
}
