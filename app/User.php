<?php

namespace App;

use App\Models\Academics\Staff;
use App\Models\Admin\Branches;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Hash;
use App\Models\Admin\BranchUser;
use App\Models\HRM\Termination;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'erp_users';


    protected $fillable = ['name', 'branch_id', 'email', 'user_type','password', 'active', 'role_id', 'hook_id', 'remember_token', 'last_login_at', 'last_login_ip', 'lockout_time'];


    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {

        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }


   /*  public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    } */
    public function Branch()
    {
        return $this->belongsTo(BranchUser::class, 'id', 'user_id');
    }

    public function Employees()
    {
        return $this->hasOne('App\Models\HRM\Employees', 'user_id');
    }

    static public function emp_name($employee_id)
    {
        return self::where(['id' => $employee_id])->first()->name;
    }
    static public function getSpecificRoleUsers($name)
    {

        $role = Role::whereName($name)->value('id');

        $users_ids = DB::table('erp_model_has_roles')
            ->where('role_id', $role)
            ->pluck('model_id');

        $users = User::whereIn('id', $users_ids)->get();
        // dd($users_ids);
        return $users;
    }


    public function staff()
    {
        return $this->hasOne(Staff::class, 'user_id', 'id');
    }

    public function  upload_experience_letter()
    {
        return $this->belongsTo('App\Models\HRM\UploadExperienceLetter','user_id');
        //return $this->belongsTo('App\Models\HRM\Employees', 'user_id');
    }
    public function termination()
    {
        return $this->hasOne(Termination::class, "user_id");
    }


    public function BranchDirectByUser()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
    public function cards()
    {
        return $this->hasMany('App\Idcard');
    }
    public function role()
    {
        return $this->belongsTo('App\Roles','role_id','id');
    }
    public function roleuser()
    {
        return $this->belongsTo('App\Models\Admin\Role','role_id','id');
    }
    public function branchuser()
    {
        return $this->belongsTo('App\Models\Admin\Branches','branch_id');
    }
      public function assignAsset()
    {
        return $this->hasMany('App\Models\Asset\AssignAsset');
    }
    public static function users($id=0){
        $list='';
        $records=self::all('name','id');
        foreach ($records as $record){
            $list.='<option '.(($id==$record->id)?'selected':'').' value="'.$record->id.'">'.$record->name.'</option>';
        }

        return $list;
    }
    public function IdCards()
    {
        return $this->hasMany('App\Models\HRM\IdCards\IdCard','link_id','id');
    }
}
