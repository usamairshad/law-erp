<?php

namespace App;

use App\Models\Academics\ActiveSession;
use App\Models\Academics\Section;
use Illuminate\Database\Eloquent\Model;

class ActiveSessionSection extends Model
{

    public function active_session_student(){
        return  $this->hasMany('App\Models\Academics\ActiveSessionStudent');
    }


    protected $fillable = [

        "active_session_id",
        "section_id",

    ];


    public function section()
    {
        return $this->belongsTo(Section::class, "section_id");
    }
    public function activesession()
    {
        return $this->belongsTo(ActiveSession::class, "active_session_id");
    }

}
//