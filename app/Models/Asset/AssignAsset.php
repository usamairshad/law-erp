<?php

namespace App\Models\Asset;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AssignAsset extends Model
{
    protected $table = 'assign_assets';
    protected $fillable = ['asset_id', 'assign_to_user', 'quantity', 'return_period', 'user_id', 'room_id','role_id','assign_to'];

    public function asset(){
        return $this->belongsTo('App\Models\Asset\Asset','asset_id', 'id');
    }
      public function users(){
        return $this->belongsTo('App\User','assign_to', 'id');
    }
}
