<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class AssetType extends Model
{
    protected $fillable = ['name','description','status','created_by','updated_by'];

    protected $table = 'erp_asset_types';
    public static function assetsType($id=0){
        $list='';
        $records=self::all('name','id');
        foreach ($records as $record){
            $list.='<option '.(($id==$record->id)?'selected':'').' value="'.$record->id.'">'.$record->name.'</option>';
        }

        return $list;
    }
}
