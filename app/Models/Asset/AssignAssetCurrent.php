<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class AssignAssetCurrent extends Model
{
    protected $fillable = ['staff_id', 'asset_id', 'return', 'return_policy_id', 'quantity',];
}
