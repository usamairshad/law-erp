<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class AssetInventoryTransaction extends Model
{
     protected $fillable = ['asset_id','vendor_name', 'quantity', 'date', 'unit_price', 'check',
                            'warranty'];
}
