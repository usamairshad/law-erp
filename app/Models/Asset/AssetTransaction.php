<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class AssetTransaction extends Model
{
    protected $fillable = ['asset_id', 'vendor', 'model', 'make_asset', 'quantity'];

    public function asset(){
        return $this->belongsTo('App\Models\Asset\Asset','asset_id','id');
    }
}
