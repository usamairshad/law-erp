<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{

    protected $fillable = ['name', 'model', 'asset_type_id', 'value', 'quantity', 'description', 'status','ware_house'];
    public static function assets($id=0){
        $list='';
        $records=self::all('name','id');
        foreach ($records as $record){
            $list.='<option '.(($id==$record->id)?'selected':'').' value="'.$record->id.'">'.$record->name.'</option>';
        }

        return $list;
    }
    public function assetTransaction(){
        return $this->hasMany('App\Models\Asset\AssetTransaction');
    }
    public function assignasset(){
        return $this->hasMany('App\Models\Asset\AssignAsset');
    }

}
