<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class ReturnPolicy extends Model
{
    protected $fillable = ['name', 'description'];
}
