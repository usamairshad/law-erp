<?php

namespace App\Models\Asset;

use Illuminate\Database\Eloquent\Model;

class AssignReturnAsset extends Model
{
    protected $fillable = ['asset_id', 'date', 'in', 'out', 'assign_object', 'return'];
}
