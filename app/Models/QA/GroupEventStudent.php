<?php

namespace App\Models\QA;

use App\Models\Academics\Student;
use App\Models\QA\GroupEvent;
use Illuminate\Database\Eloquent\Model;

class GroupEventStudent extends Model
{
    protected $fillable = [
        "group_event_id",
        "student_id"
    ];


    public function student()
    {
        return $this->belongsTo(Student::class, "student_id");
    }


    public function group()
    {
        return $this->belongsTo(GroupEvent::class, "group_event_id");
    }
}
