<?php

namespace App\Models\QA;

use App\UploadedDocument;
use Illuminate\Database\Eloquent\Model;

class GroupEvent extends Model
{
    protected $fillable = [
        "category"
    ];

    public function groupstudentevent()
    {
        return $this->hasMany("App\Models\QA\GroupEventStudent", "group_event_id");
    }



    public function uploadeddocument()
    {
        return $this->hasMany(UploadedDocument::class, "group_event_id");
    }
}
