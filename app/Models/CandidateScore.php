<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CandidateScore extends Model
{
    protected $fillable = ['candidate_form_id','schedule_id','relevant_knowledge','skills','communication','relevant_experience','confidence'
    ];

    protected $table = 'erp_candidate_score';
}
