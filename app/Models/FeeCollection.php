<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeeCollection extends Model
{
//
    protected $table = 'fee_collections';
    protected $fillable = ['created_by', 'last_updated_by', 'students_id', 'session_id', 'branch_id', 'fee_masters_id', 'fee_structure_code','fee_term_id','tax_percentage','discount_type','bill_no', 'due_date', 'paid_date', 'discount', 'fine', 'paid_amount','total_amount', 'payment_mode', 'note', 'fee_status', 'fc_bill_no', 'fc_id'];

    public function feeMasters()
    {
        return $this->belongsTo(FeeMaster::class, 'fee_masters_id');
    }

    public function students()
    {
        return $this->belongsTo(Student::class, 'students_id');
    }
    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
    public function feeStructures()
    {
        return $this->hasMany(FeeMaster::class, 'fee_structure_code','fee_structure_code');
    }
}
