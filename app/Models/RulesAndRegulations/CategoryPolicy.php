<?php

namespace App\Models\RulesAndRegulations;

use Illuminate\Database\Eloquent\Model;

class CategoryPolicy extends Model
{
    protected $table = 'erp_category_policies';
    protected $fillable = ['categorypolicy_name', 'categorypolicy_description', 'created_by', 'updated_by', 'deleted_by'];
    public function codeofconduct(){
        return $this->hasMany('App\Models\RulesAndRegulations\CodeOfConduct');
    }
    public function policy(){
        return $this->hasMany('App\Models\RulesAndRegulations\Policy');
    }
    public static function categorypolicy($id=0){
        $list='';
        $records=self::all('categorypolicy_name','id');
        foreach ($records as $record){
            $list.='<option '.(($id==$record->id)?'selected':'').' value="'.$record->id.'">'.$record->categorypolicy_name.'</option>';
        }

        return $list;
    }
}
