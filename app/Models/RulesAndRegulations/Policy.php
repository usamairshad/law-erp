<?php

namespace App\Models\RulesAndRegulations;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $table = 'erp_policies';
    protected $fillable = ['category_policy_id', 'policy_name', 'policy_description','created_by', 'updated_by', 'deleted_by'];
    public function categorypolicy(){
        return $this->belongsTo('App\Models\RulesAndRegulations\CategoryPolicy','category_policy_id', 'id');
    }
}