<?php

namespace App\Models\RulesAndRegulations;

use Illuminate\Database\Eloquent\Model;

class CodeOfConduct extends Model
{
    protected $table = 'erp_code_of_conducts';
    protected $fillable = ['category_policy_id', 'codeofconduct_name', 'codeofconduct_description','created_by', 'updated_by', 'deleted_by'];
    public function categorypolicy(){
        return $this->belongsTo('App\Models\RulesAndRegulations\CategoryPolicy','category_policy_id', 'id');
    }
}
