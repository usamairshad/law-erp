<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class ReportGroup extends Model
{
    protected $fillable = ['name','description'];
}
