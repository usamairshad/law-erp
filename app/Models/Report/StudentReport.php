<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class StudentReport extends Model
{
    protected $fillable = ['student_id','grade','name'];

    public function student(){
    	return $this->belongsTo('App\Models\Academics\Student');
    }
}
