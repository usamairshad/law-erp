<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class ReportParameter extends Model
{
    protected $fillable = ['name','description','reportGroup_id','range'];

    public function reportgroup(){
    	return $this->belongsTo('App\Models\Report\ReportGroup','reportGroup_id','id');
    }
}
