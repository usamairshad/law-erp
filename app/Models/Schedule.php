<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ['candidate_form_id','job_post_id','test_time','test_venue','interview_time','interview_venue','branch_id','test_doc','interview_doc','status','created_by','updated_by','deleted_by','deleted_at'];

    protected $table = 'schedule';

    public  function candidate(){
        return $this->belongsTo('App\Models\HRM\CandidateForm','candidate_form_id');
    }
}
