<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class LeaveManagement extends Model
{
    protected $table = 'leavemanagements';
    protected $fillable = [
        'staff_id','leave_type','start_date','end_date','no_of_leaves','status','active_session_teacher_id','approver_id','approver_role', 'course_id','leave_date'
    ];
}
