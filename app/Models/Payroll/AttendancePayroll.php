<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class AttendancePayroll extends Model
{
    protected $table = 'teacher_attendance_payroll';
    protected $fillable = [
        'active_session_teacher_id','staff_id', 'class_Date', 'class_time', 'current_time' ,'day','attendance', 'attendance_type','course_id','month','year','branch_id',
    ];
}
