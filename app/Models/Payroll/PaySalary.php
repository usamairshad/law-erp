<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class PaySalary extends Model
{
    protected $table = 'erp_staff_salaries';
    protected $fillable = [
       'month_id','year_id','leaves','ledger_id', 'description', 'net_salary', 'date', 'staff_id','medical_allowance','house_rent_allowance','conveyance_allowance','training','loan_amount','remaining_amount','pay_loan','epf_amount','eobi_amount','provident_fund','tax_amount','branch_id','name','gross_salary','basic_salary','leaves_deduction','gross_before_adj','total_epf','total_eobi','total_tax','total_salary','sheet_code'];
}
