<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
//
    protected $table = 'erp_daily_attendance';
    protected $fillable = ['user_id', 'branch_id', 'time_in', 'time_out', 'date', 'status','remarks'];



}