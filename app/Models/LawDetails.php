<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LawDetails extends Model
{
    protected $table = 'erp_law_rates';
    protected $fillable = ['law_id','type','percentage','amount_limit','status'];
   
}
