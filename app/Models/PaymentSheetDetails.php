<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentSheetDetails extends Model
{
    protected $table = 'erp_payment_sheet_details';
    protected $fillable = ['amount_after','amount_before','advance_appears','sheet_id','remarks','status','advance','tax_rate',
     'law_id','amount','cheque_amount','ledger_id','vendor_id','mode','inv_amount','pra','rent_id'];
   
}
