<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\EmployeeAttendance;
use DB;

class EmployeesOvertime extends Model
{
    protected $table = 'erp_hrm_employee_overtime';

    protected $fillable = ['user_id','allowed_hours','for_date','allowed_by','approved_by',
            'work_status','status','created_by','updated_by','deleted_by','created_at','updated_at'
            ,'deleted_at'];

    public function employee(){
        return $this->belongsTo('App\Models\HRM\Employees','user_id');
    }

    static public function getOvertime($user_id, $month, $year){
        $start = EmployeeAttendance::getFirstDayOfMonth($month, $year);
        $end = EmployeeAttendance::getLastDayOfMonth($month, $year);
        return self::where(['status' => 1])->where(['user_id' => $user_id])->whereBetween('for_date', [$start, $end])->get();
    }

}
