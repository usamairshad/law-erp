<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class EmployeeDocumentTypes extends Model
{

    protected $fillable = ['id', 'name', 'created_at', 'updated_at','created_by', 'updated_by'];
                   
    protected $table = 'erp_hrm_employee_document_types';
    
    public function employee_documents(){
        return $this->hasMany('App\Models\HRM\EmployeeDocuments','document_type');
    }
    
    static public function getDocumentNameByDocumentId($doc_id){
        return self::where('id',$doc_id)->get()->pluck('name')->first();
    }
    
}
