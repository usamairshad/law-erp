<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class TimetableCategory extends Model
{
    protected $fillable = ['name', 'description','created_by', 'updated_by'];
    protected $table = 'timetable_categories';
}
