<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organizations extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'gst', 'ntn', 'phone', 'fax', 'email',
        'status', 'created_by', 'updated_by'. 'deleted_by'
    ];

    protected $table = 'erp_hrm_organizations';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
}
