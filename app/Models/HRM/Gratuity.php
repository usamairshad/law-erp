<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gratuity extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'gratuity_amount', 'years', 'basic','paid_on' ,
        'remarks',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_gratuity';


    public function emp_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','user_id','user_id');
    }

    static public  function employeeGratuityDetailByUserId($user_id){
        $Gratuity  = Gratuity::selectRaw('*')
            ->where(['status' => 1])
            ->where(['user_id' => $user_id])
            ->first();
        return $Gratuity;
    }




}
