<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvancePayroll extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','month','year','salary_type','comments'];

    protected $table = 'erp_hrm_advance_payrolls';

}
