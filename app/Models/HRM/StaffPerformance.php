<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class StaffPerformance extends Model
{
     protected $fillable = [

        "user_id",
        "lecture_deliver",
        "confidence",
        "punctuality",
        "moral_values",
        "student_feedback",
        "month",
        "date",
        "status",
        "created_by",
        "updated_by",
        "deleted_by",
        "remarks",

    ];
}
