<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = ['session_id','created_by','branch_id','title','status'];
    protected $table = 'exams';
}
