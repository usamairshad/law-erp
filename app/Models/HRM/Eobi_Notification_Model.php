<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Eobi_Notification_Model extends Model
{
    //
    protected $fillable = ['name','description','date','amount','company_share'];
    protected $table = 'eobi_notifications';
}
