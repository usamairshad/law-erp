<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class EventModel extends Model
{
    protected $fillable = ['branch_id','title','start','end'];
    protected $table = 'event_models';
}
