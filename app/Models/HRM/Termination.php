<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Termination extends Model
{
    protected $fillable = [
        "user_id",
        "reason",
        "warnings",
    ];
}
