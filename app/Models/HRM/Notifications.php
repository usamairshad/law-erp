<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
	
    //
    protected $fillable = ['sid', 'rid', 'title', 'reason', 'justification','type'];
    protected $table = 'notifications';
}
