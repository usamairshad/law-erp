<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesEducationDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'employee_id','edu_level', 'edu_institute', 'edu_specialization', 'edu_year', 'edu_score', 'edu_start_date', 'edu_end_date', 'created_by', 'updated_by'. 'deleted_by'
    ];

    protected $table = 'erp_hrm_employees_education_detail';

}
