<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class ClassTimeTable extends Model
{
    protected $fillable = ['time_table_id','active_session_section_id','subject_id','day','created_by','updated_by'];

    protected $table = 'class_time_tables';

    public function timetable()
    {
        return $this->belongsTo("App\Models\HRM\Timetable", 'time_table_id', "id");
    }
}