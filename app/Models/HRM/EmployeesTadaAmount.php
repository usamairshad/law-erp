<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesTadaAmount extends Model
{
    use SoftDeletes;

    protected $fillable = ['job_title_id', 'tada_amount', 'year','user_id','for_date',
        'allowed_by','approved_by',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_employees_tada_amount';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    public function job_name()
    {
        return $this->belongsTo('App\Models\HRM\JobTitle','job_title_id');
    }

    public function employee_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','user_id', 'user_id');
    }

    public function allowed_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','allowed_by', 'user_id');
    }
    public function approve_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','approved_by', 'user_id');
    }

}
