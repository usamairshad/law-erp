<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Lab extends Model
{
    protected $fillable = ['name','description','status','created_by','updated_by'];
    protected $table = 'labs';
}
