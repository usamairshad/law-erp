<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class AttendanceMachine extends Model
{

    protected $connection = 'sqlsrv';


    protected $fillable = ['USERID','CHECKTIME', 'CHECKTYPE', 'VERIFYCODE', 'SENSORID',
        'Memoinfo', 'sn', 'UserExtFmt','is_machine','branch_id','status','user_id','date_time','type'];

    protected $table = 'erp_CHECKINOUT';

    static function syncAttendance(){
        $employees = Employees::where(['status' => 1])->pluck('user_id','device_id');
        $employees = json_decode(json_encode($employees),true);

//        $last_time=EmployeeAttendance::where('is_machine','=',1)->orderBy('id','DESC')->limit(1)->first()->date_time;
        $last_time = EmployeeAttendance::where('is_machine','=',1)->orderBy('date_time','desc')->limit(1)->first();
//dd($last_time);
        if(!isset($last_time)){
            $last_time = '2019-07-23 06:00:00';

        }else{

            $last_time =    $last_time['date_time'];
        }

        $url = "203.128.30.39/pdoatt/api/?time=".str_replace(' ','%20',$last_time);
//        $url = "110.36.217.188/tuv/api/";
        $apiData = self::CallAPI('GET',$url);
        $attendanceReport = json_decode($apiData,true);

//dd($attendanceReport);
//        dd($attendanceReport['response_desc']);
        $add_record =  array();
        if($attendanceReport!=null){
            if(isset($attendanceReport['response_desc'])){

                foreach($attendanceReport['response_desc'] as $key => $response){

                    $code = (int)$response['code'];
                    if(array_key_exists($code, $employees)){
                        $user_id = $employees[$code];
                        $data=[];
                        $data['branch_id']=1;
                        $data['is_machine']=1;
                        $data['status']=1;
    //                    $data['user_id']=$user_id->user_id;
                        $data['user_id']=$user_id;
                        $data['date_time']= date('Y-m-d H:i:s', strtotime($response['CHECKTIME']));
                        $data['type']=$response['CHECKTYPE'];
                        $data['m_user_id']=$response['USERID'];
                        $data['m_verify_code']=$response['VERIFYCODE'];
                        $data['ma_sensor_id']=$response['SENSORID'];
                        $data['m_memo_info']=$response['Memoinfo'];
                        $data['m_work_code']=$response['WorkCode'];
                        $data['m_sn']=$response['sn'];
                        $data['m_user_ext_fmt']=$response['UserExtFmt'];

                        if( $data['type']=='O'){
                            $data['checkout_time']=$data['date_time'];

                        }else{
                            $data['checkin_time']=$data['date_time'];

                        }

                        array_push($add_record, $data);
                    }

//                    $eAttedance=EmployeeAttendance::create($data);
                }

                $result= null;
                DB::beginTransaction();
                foreach (array_chunk($add_record,1000) as $t) {

                    $result = EmployeeAttendance::insert($t);
                }

                if (!$result)
                {
                    DB::rollback();
                }

                DB::commit();

                return true;
            }
            else{
                return false;
            }
        }else{
            return false;
        }

    }

    public static function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}
