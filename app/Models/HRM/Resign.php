<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Resign extends Model
{
    protected $fillable = [

        "user_id",
        "valid",
        "join_date",
        "notice_period",
        "reason",
        "experience_letter",
        "approved_by_ceo",
        "approved_by_hr_head",
        "class",
        "subject",  
        "last_day_of_work",


    ];


    public function user()
    {
        return $this->belongsTo("App\User", "user_id");
    }

}
