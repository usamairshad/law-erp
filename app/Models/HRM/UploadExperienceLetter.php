<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class UploadExperienceLetter extends Model
{
        protected $fillable = [

        'user_id',
        "name",
        "reg_no",
        "designation",
        "branch_id",
        "class",
        "subject",
        "home_address",
        "contact",
        "join_date",
        "last_day_of_work",
        "experiene_letter_file",
        "clearance_file",
    ];

}
