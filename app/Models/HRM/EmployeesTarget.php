<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesTarget extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'type','year','category','product_type','class_type','product_id','brand',
        'status', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_hrm_employees_target';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    static public function pluckMyTargets($user_id) {
        return self::where(['status' => 1, 'created_by' => $user_id])->OrderBy('name','asc')->pluck('name','id');
    }

    public function target_creator() {
        return $this->belongsTo('App\Models\HRM\Employees', 'created_by','user_id');
    }

    public function product_name() {
        return $this->belongsTo('App\Models\Admin\ProductsModel', 'product_id');
    }

    public function brand_name() {
        return $this->belongsTo('App\Models\Admin\SuppliersModel', 'brand');
    }

    public function categ_name() {
        return $this->belongsTo('App\Models\Admin\StockCategoryModel', 'category');
    }



}
