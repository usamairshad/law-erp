<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Holidays extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'holiday_date', 'is_recurring', 'holiday_length', 'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_holidays';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    static public function holidayBetween($month, $year) {
        $holidays  = Holidays::selectRaw('hrm_holidays.holiday_date')
            ->where(['status' => 1])
            ->whereYear('holiday_date', $year)
            ->whereMonth('holiday_date' , $month)
            ->get();
        return $holidays;
    }

    static public function monthHoliday($month, $year) {
        $holidays  = self::where(['status' => 1])
            ->whereYear('holiday_date', $year)
            ->whereMonth('holiday_date' , $month)
            ->pluck('holiday_date');
        return $holidays;
    }
}
