<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeResources extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'product_id', 'serial_no', 'grant_date', 'return_date','condition' ,'worth',
        'remarks',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_employees_resources';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    public function emp_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','user_id','user_id');
    }

    public function product_name()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }

    public function asset_name()
    {
        return $this->belongsTo('App\Models\Admin\FixedAssetsModel','product_id');
    }
}
