<?php

namespace App\Models\HRM;

use App\Models\Admin\Branches;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $fillable = [
        "venue",
        "category",
        "department",
        "designation",
        "branch_id",
        "description",
        "date",
        "time",
        "training_by",
        "no_of_days",
        "training_charges",
        "training_type",
        "created_by",
        "updated_by",




    ];

    public function branch()
    {
        return $this->belongsTo("App\Models\Admin\Branches", "branch_id");
    }
}
