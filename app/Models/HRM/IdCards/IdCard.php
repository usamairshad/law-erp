<?php

namespace App\Models\HRM\IdCards;

use Illuminate\Database\Eloquent\Model;

class IdCard extends Model
{
    protected $table = 'idcards';
    protected $fillable = ['link_type', 'link_id', 'status', 'issue_date','expiry_date', 'approved_at', 'approved_by'];
    public function user()
    {
        return $this->belongsTo('App\User','link_id');
    }

}
