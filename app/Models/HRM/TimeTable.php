<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class TimeTable extends Model
{
    protected $fillable = ['timetable_category_id','branch_id','title','start_time','end_time','created_by','updated_by'];
    protected $table = 'time_tables';

   
    public function branch()
    {
        return $this->belongsTo('App\Models\Admin\Branches', 'branch_id');
    }
}
