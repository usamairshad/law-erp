<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesSuggestion extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'heading', 'suggestion','comments','ratings',
       'display_status', 'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_employees_suggestion';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('heading','asc')->pluck('heading','id');
    }

    public function user_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','user_id','user_id');
    }
}
