<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesSalaryDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'employee_id','basic_salary', 'account_number', 'bank_name', 'created_by', 'updated_by'. 'deleted_by'
    ];

    protected $table = 'erp_hrm_employees_salary_detail';

}
