<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeavePeriods extends Model
{
    use SoftDeletes;

    protected $fillable = ['start_day', 'start_month', 'end_day', 'end_month', 'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_leave_periods';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
}
