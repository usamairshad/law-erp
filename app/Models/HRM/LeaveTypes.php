<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveTypes extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'permitted_days', 'condition', 'allowed_number', 'allowed_type', 'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_leave_types';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
}
