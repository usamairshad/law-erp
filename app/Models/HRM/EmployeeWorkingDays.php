<?php

namespace App\Models\HRM;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class EmployeeWorkingDays extends Model
{
    protected $fillable = ['user_id', 'date', 'type_id', 'created_by', 'updated_by', 'created_at', 'updated_at'];

    protected $table = 'erp_hrm_employee_working_days';

    static public function checkExistence($user_id, $date){
        return self::orderBy('created_at', 'desc')->where('user_id', $user_id)->where('date', 'like', '%'.$date.'%')->get()->first();
    }

    static public function parseAttendanceArrays($EmployeeWorkingDays){
        //converting Array to Hierarchical Order
        $employee_keys = Array();
        foreach(array_keys($EmployeeWorkingDays) as $employee){
            $temp_dates = Array();
            foreach(array_keys($EmployeeWorkingDays[$employee]) as $date){
                $employee_keys[$employee][$date]['office_in'] = $EmployeeWorkingDays[$employee][$date]['office_in'];
                $employee_keys[$employee][$date]['office_out'] = $EmployeeWorkingDays[$employee][$date]['office_out'];
                $employee_keys[$employee][$date]['worked_hours'] = $EmployeeWorkingDays[$employee][$date]['worked_hours'];
                $employee_keys[$employee][$date]['attendance_type'] = $EmployeeWorkingDays[$employee][$date] ['attendance_type'];
            }
            if(!empty($temp_dates))
                $employee_keys[$employee] = $temp_dates;
        }
        //Converting Hierarchical to Single Collection
        $user_ids = Array();
        $dates = Array();
        $collections = Array();
        $count = 0;
        foreach(array_keys($employee_keys) as $employee){
            array_push($user_ids, $employee);
            foreach(array_keys($employee_keys[$employee]) as $date){
                if(!in_array($date, $dates))
                    array_push($dates, $date);
                $collection = [
                    'id' => ++$count,
                    'user_id' => $employee,
                    'date' => $date,
                    'office_in' => $employee_keys[$employee][$date]['office_in'],
                    'office_out' => $employee_keys[$employee][$date]['office_out'],
                    'worked_hours' => $employee_keys[$employee][$date]['worked_hours'],
                    'attendance_type' => $employee_keys[$employee][$date]['attendance_type']
                ];
                array_push($collections, $collection);
            }
        }

        $data = ['employee_ids' => $user_ids, 'dates' => $dates, 'employee_wise' => $employee_keys, 'collection_wise' => $collections];

        return $data;
    }
    
}
