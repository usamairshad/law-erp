<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payroll extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','account_number','bank_id','basic_salary','sc_amount',
        'half_leaves', 'sandwich_leaves', 'wages', 'total_after_tax',
        'house_rent','utilities','overtime','total_pf','total_bonus','total_commission','tax_percentage','tax_amount',
        'leaves','short_leaves','allowed_leaves','presents','month','year','paid_date','cash_date',
        'salary_type','paid_amount','pending_amount','deducted_amount','gross_total','paid_by','payment_method','comments',
        'status','created_by','updated_by','deleted_by','created_at','updated_at', 'deleted_at'];

    protected $table = 'erp_hrm_pay_roll';


    static public function payrollByUserId($user_id){
        return self::where(['status' => 1])->where(['deleted_at' => null])->where(['user_id' => $user_id])->get();
    }

    static public function payrollByDate($from_date){
        return self::where(['status' => 1])->where(['deleted_at' => null])->where(['paid_date' => $from_date])->get();
    }

    static public function payrollByUserAndDate($user_id, $from_date){
        return self::where(['status' => 1])->where(['deleted_at' => null])->where(['user_id' => $user_id])->where(['paid_date' => $from_date])->get();
    }

    static public function payrollByDatesAndUser($from_date, $to_date, $user_id){
        return self::where(['status' => 1])->where(['deleted_at' => null])->where(['user_id' => $user_id])->whereBetween('paid_date', array($from_date, $to_date))->get();
    }

    public function emp_name() {
        return $this->belongsTo('App\Models\HRM\Employees', 'user_id','user_id');
    }

}
