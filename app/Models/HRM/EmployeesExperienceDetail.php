<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesExperienceDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'employee_id','pre_company', 'pre_job_title', 'skills_experiance', 'skill_title', 'pre_job_date_from', 'pre_job_date_to', 'created_by', 'updated_by'. 'deleted_by'
    ];

    protected $table = 'erp_hrm_employees_work_experience';

}
