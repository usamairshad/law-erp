<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class LabTimeTable extends Model
{
    protected $fillable = ['time_table_id','date','created_by','lab_id'];
    protected $table = 'lab_time_table';
}
