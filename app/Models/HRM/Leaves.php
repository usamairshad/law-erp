<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leaves extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'leave_request_id', 'employee_id', 'leave_type_id', 'work_shift_id',
        'leave_date', 'start_time', 'end_time', 'shift', 'hours',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_leaves';

    /**
     * Get the employee that the leave belongs to.
     */

    static public function approvedHolidays($month, $year,$emp_id) {
        return self::where(['status' => 1])
            ->where(['employee_id' => $emp_id])
            ->where(['shift' => 'full'])
            ->whereYear('leave_date', $year)
            ->whereMonth('leave_date' , $month)
            ->OrderBy('leave_date','asc')->pluck('leave_date');

    }

    static public function approvedHalfDays($month, $year,$emp_id) {
        return self::where(['status' => 1])
            ->where(['employee_id' => $emp_id])
            ->where(['shift' => 'half'])
            ->whereYear('leave_date', $year)
            ->whereMonth('leave_date' , $month)
            ->OrderBy('leave_date','asc')->pluck('leave_date');

    }

    static public function employee_leave($emp_id,$from_date, $to_date) {
        return self::where(['status' => 1])
            ->where(['employee_id' => $emp_id])
            ->whereBetween('leave_date', [$from_date, $to_date])
            ->get();


    }


    static public  function employeeLeaves( $user_id, $start_date, $end_date){
        $EmployeeAttendance  = self::selectRaw('hrm_leaves.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_leaves.employee_id','=','hrm_employees.user_id')
            ->whereIn('hrm_leaves.leave_status_id', array(1,2,3,4))
            ->where('hrm_leaves.employee_id' , $user_id)
            ->whereRaw("hrm_leaves.leave_date BETWEEN '".$start_date. "' AND '". $end_date ."' ")
            ->get();
        return $EmployeeAttendance;
    }


    static public  function allReqByBranch($branch_id, $user_id){
        $EmployeeAttendance  = self::selectRaw('hrm_leaves.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_leaves.employee_id','=','hrm_employees.user_id')
//            ->where(['hrm_leaves.leave_status_id' => 3])
//            ->where('hrm_leaves.status','<>', 1)
            ->whereIn('hrm_leaves.status',[2,3])
            ->whereIn('hrm_employees.branch_id', $branch_id)
            ->orWhere('hrm_leaves.employee_id' , $user_id)
            ->get();
        return $EmployeeAttendance;
    }


    static public  function ReqByUser($user_id){
        $EmployeeAttendance  = self::selectRaw('hrm_leaves.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_leaves.employee_id','=','hrm_employees.user_id')
//            ->where(['hrm_leaves.leave_status_id' => 3])
//            ->where('hrm_leaves.status','<>', 1)
            ->whereIn('hrm_leaves.status',[2,3])
            ->where('hrm_leaves.employee_id' , $user_id)
            ->get();
        return $EmployeeAttendance;
    }

    static public  function allLeaveRequests(){
        $leaveRequest  = self::selectRaw('erp_hrm_leaves.*,staff.first_name')
            ->join('staff','erp_hrm_leaves.employee_id','=','staff.user_id')
           // ->where(['erp_hrm_leaves.leave_status_id' => 3])
          //  ->where('erp_hrm_leaves.status','<>', 1)
            ->whereIn('erp_hrm_leaves.status',[2,3])
            ->get();

        return $leaveRequest;
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\HRM\Employees', 'employee_id','user_id');
    }

    /**
     * Get the work shift that the leave belongs to.
     */
    public function work_shift()
    {
        return $this->belongsTo('App\Models\HRM\WorkShifts', 'work_shift_id');
    }

    /**
     * Get the leave type that the leave belongs to.
     */
    public function leave_type()
    {
        return $this->belongsTo('App\Models\HRM\LeaveTypes', 'leave_type_id');
    }

    /**
     * Get the leave request that the leave belongs to.
     */
    public function leave_request()
    {
        return $this->belongsTo('App\Models\HRM\LeaveRequests', 'leave_request_id');
    }
    public function leave_status()
    {
        return $this->belongsTo('App\Models\HRM\LeaveStatuses', 'leave_status_id');
    }



}
