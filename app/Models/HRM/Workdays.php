<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workdays extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'date', 'workday_length', 'applicable_to', 'status'];

    protected $table = 'erp_workdays';

}
