<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesJobDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'employee_id','job_title', 'job_category', 'department_id','employee_status','date_of_joining','probation_period','contract_start_date','contract_end_date','notice_period','date_of_resign','report_to','country_id',
        'region_id','branch_id','territory_id', 'created_by', 'updated_by'. 'deleted_by'
    ];

    protected $table = 'erp_hrm_employees_job_detail';

}
