<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkShifts extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'hours_per_day', 'shift_start_time', 'shift_end_time',
        'working_hours_per_day', 'break_start_time', 'break_end_time', 'break_hours_per_day',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_work_shifts';

    public function attendance_settings(){
        return $this->hasMany('App\Models\HRM\AttendanceSettings','shift_id');
    }

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    static public function getShiftById($shift_id){
        return self::find($shift_id)->where(['status' => '1'])->get()->first();
    }

    static public function getFirstWorkShift() {
        return self::where(['status' => 1])->OrderBy('name','asc')->first();
    }

}
