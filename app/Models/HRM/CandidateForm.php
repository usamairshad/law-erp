<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class CandidateForm extends Model
{
    protected $fillable = ['job_post_id','name','gender','education','experience','expected_salary','cv','cover_letter','phone','email','user_id','expected_joining','status','created_by','deleted_by','updated_by','deleted_at'
    ];

    protected $table = 'candidate_form';

    public function getName($id)
    {
        return $this->name;
    }

    public  function jobpost(){
        return $this->belongsTo('App\Models\JobPost','job_post_id');
    }
}
