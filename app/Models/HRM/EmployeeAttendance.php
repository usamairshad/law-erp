<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\HRM\EmployeeWorkingDays;
use DB;

class EmployeeAttendance extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','branch_id','checkin_time','checkout_time','date_time','type','status','approved_by',
        'm_user_id','m_verify_code','ma_sensor_id','m_memo_info','m_work_code','m_sn','m_user_ext_fmt','is_machine',
        'created_by','updated_by','deleted_by','created_at','updated_at', 'deleted_at'];

    protected $table = 'erp_hrm_employee_attendance';

    public function approved_by_name(){
        return $this->belongsTo('App\Models\HRM\Employees','approved_by','user_id');
    }
    static public  function attendanceDetailById($att_id){
        $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name,branches.name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->join('branches','hrm_employee_attendance.branch_id','=','branches.id')
            ->where(['hrm_employee_attendance.status' => 1])
            ->where(['hrm_employee_attendance.id' => $att_id])
            ->first();
        return $EmployeeAttendance;
    }

    static public  function notApprovedReqByBranch($branch_id){
        $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->where(['hrm_employee_attendance.status' => 3])
            ->whereIn('hrm_employee_attendance.branch_id' , $branch_id)
            ->get();
        return $EmployeeAttendance;
    }

    static public  function approvedReqByBranch($branch_id){
        $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->where(['hrm_employee_attendance.status' => 2])
            ->where(['hrm_employees.branch_id' => $branch_id])
            ->get();
        return $EmployeeAttendance;
    }

    static public  function ReqByUserIds($user_id){
        $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->where(['hrm_employee_attendance.status' => 2])
            ->whereIn('hrm_employees.user_id' , $user_id)
            ->get();
        return $EmployeeAttendance;
    }

    static public  function allReqByBranch($branch_id, $user_id){
        $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->where('hrm_employee_attendance.status' ,'<>', 1)
            ->whereIn('hrm_employees.branch_id', $branch_id)
            ->orWhere('hrm_employee_attendance.user_id' , $user_id)
            ->get();
        return $EmployeeAttendance;
    }

    static public  function allReq(){
        $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->where('hrm_employee_attendance.status' ,'<>', 1)
            ->get();
        return $EmployeeAttendance;
    }


    // static public  function attendanceByUserId($user_id){
    //     $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name,branches.name')
    //         ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
    //         ->join('branches','hrm_employee_attendance.branch_id','=','branches.id')
    //         ->where(['hrm_employee_attendance.user_id' => $user_id])
    //         ->where(['hrm_employee_attendance.status' => 1])
    //         ->orderBy('date_time','asc')->get();
    //     return $EmployeeAttendance;
    // }

    static public  function requestByUserId($user_id){

        $EmployeeAttendance  = self::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->where('hrm_employee_attendance.user_id', $user_id)
            ->where('hrm_employee_attendance.status' ,'!=', '1')->get();

        return $EmployeeAttendance;
    }

    static public  function attendanceByUserId($user_id){
        $EmployeeAttendance  = self::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name')
                ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
                ->where('hrm_employee_attendance.user_id', $user_id)
                ->where(['hrm_employee_attendance.status' => 1])
                ->orderBy('date_time','asc')->get();
        return $EmployeeAttendance;
    }

    static public function attendanceByUserAndDate($user, $date){
        return DB::select("SELECT * from hrm_employee_attendance WHERE user_id = '$user' AND date(date_time) = '$date' AND deleted_at IS NULL");
    }

    // static public  function attendanceByDates($from, $to){
    //     $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name,branches.name')
    //         ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
    //         ->join('branches','hrm_employee_attendance.branch_id','=','branches.id')
    //         ->where(['hrm_employee_attendance.status' => 1])
    //         ->whereBetween('hrm_employee_attendance.date_time', array($from, $to))
    //         ->orderBy('date_time')->get();
    //     return $EmployeeAttendance;
    // }

    static public  function attendanceByDates($from, $to){
        $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name,branches.name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->join('branches','hrm_employee_attendance.branch_id','=','branches.id')
            ->where(['hrm_employee_attendance.status' => 1])
            ->whereBetween('hrm_employee_attendance.date_time', array($from, $to))
            ->orderBy('date_time')->get();
        return $EmployeeAttendance;
    }

    // static public  function attendanceByDate($from){
    //     $EmployeeAttendance  = EmployeeAttendance::selectRaw('hrm_employee_attendance.*,hrm_employees.first_name,branches.name')
    //         ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
    //         ->join('branches','hrm_employee_attendance.branch_id','=','branches.id')
    //         ->where(['hrm_employee_attendance.status' => 1])
    //         ->where('hrm_employee_attendance.date_time', 'like', $from.'%')
    //         ->orderBy('date_time')->get();
    //     return $EmployeeAttendance;
    // }

    static public  function attendanceByDate($from){
        return self::selectRaw('hrm_employee_attendance.*, hrm_employees.first_name')
                ->join('hrm_employees', 'hrm_employee_attendance.user_id', '=', 'hrm_employees.user_id')
                ->where('hrm_employee_attendance.date_time', 'like', $from.'%')
                ->where(['hrm_employee_attendance.status' => 1])
                ->orderBy('date_time')->get();
    }

    // static public  function attendanceByDatesAndUser($user_id,$from, $to){
    //     $EmployeeAttendance  = EmployeeAttendance::orderBy('date_time','asc')->selectRaw('hrm_employee_attendance.*,hrm_employees.first_name,branches.name')
    //         ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
    //         ->join('branches','hrm_employee_attendance.branch_id','=','branches.id')
    //         ->where(['hrm_employee_attendance.status' => 1])
    //         ->where(['hrm_employee_attendance.user_id' => $user_id])
    //         ->whereBetween('hrm_employee_attendance.date_time', array($from, $to))
    //         ->get();
    //     return $EmployeeAttendance;
    // }

    static public  function attendanceByDatesAndUser($user_id, $from, $to){
        $EmployeeAttendance  = EmployeeAttendance::orderBy('date_time','asc')
            ->selectRaw('hrm_employee_attendance.*,hrm_employees.first_name')
            ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
            ->where('hrm_employee_attendance.status', 1)
            ->where('hrm_employee_attendance.user_id', $user_id)
            ->whereBetween('hrm_employee_attendance.date_time', array($from, $to))
            ->get();
        return $EmployeeAttendance;
    }

    static public function pluckUserIdsFromAttendance(){ 
        return self::groupBy('user_id')->where('deleted_at',null)->pluck('user_id');
    }

    static public  function attendanceDatesByUserId($user_id,$from, $to){
        return db::select("select distinct date(date_time) 'date' from hrm_employee_attendance where user_id = '$user_id' AND deleted_at IS NULL AND date_time BETWEEN '$from' AND '$to'");
    }

    static public function attendanceDatesByUser($user_id){
        return db::select("select distinct date(date_time) 'date' from hrm_employee_attendance where user_id = '$user_id' and deleted_at IS NULL");
    }

    static public function attendenceByUserId($user, $date){
        return self::orderBy('date_time','asc')->where('deleted_at',null)->where('user_id',$user)->get();
    }

    // static public function attendenceByUserAndDate($user,$date){
    //     $EmployeeAttendance  = EmployeeAttendance::orderBy('date_time','asc')->selectRaw('hrm_employee_attendance.*,hrm_employees.first_name,branches.name')
    //     ->join('hrm_employees','hrm_employee_attendance.user_id','=','hrm_employees.user_id')
    //     ->join('branches','hrm_employee_attendance.branch_id','=','branches.id')
    //     ->where(['hrm_employee_attendance.status' => 1])
    //     ->where(['hrm_employee_attendance.user_id' => $user])
    //     ->where('date_time','like',$date.'%')->orWhere('date_time',$date)
    //     ->get();
    //     return $EmployeeAttendance;
    // }

    static public function attendenceByUserAndDate($user, $date){
        return self::selectRaw('hrm_employee_attendance.*, hrm_employees.first_name')
                ->join('hrm_employees', 'hrm_employees.user_id', '=', 'hrm_employee_attendance.user_id')
                ->where('hrm_employee_attendance.user_id', $user)
                ->where('hrm_employee_attendance.date_time', 'like', '%'.$date.'%')
                ->where('hrm_employee_attendance.status', 1)
                ->orderBy('date_time')->get();
    }

    static public function firstInByUserAndDate($user_id, $date){
        return self::orderBy('date_time','asc')->where(['user_id' => $user_id])
                    ->where('date_time','like','%'.$date.'%')->where(['status' => '1'])
                    ->where(['deleted_at' => null])->where(['type' => 'I'])
                    ->get()->pluck('date_time')->first();
    }

    static public function LastOutByUserAndDate($user_id, $date){
        return self::orderBy('date_time','desc')->where(['user_id' => $user_id])
                    ->where('date_time','like','%'.$date.'%')->where(['status' => '1'])
                    ->where(['deleted_at' => null])->where(['type' => 'O'])
                    ->get()->pluck('date_time')->first();
    }

    static public function evaluateFaultedAttendance($employee_id,$from_date,$to_date){
        $employee_attendance_status = Array();
        foreach(EmployeeAttendance::attendanceDatesByUserId($employee_id, $from_date, $to_date) as $employee_date){
            $count = 0; $count_in = 0; $count_out = 0;
            
            foreach(EmployeeAttendance::attendenceByUserAndDate($employee_id,$employee_date->date) as $checktype){
                if($count % 2 == 0 && $checktype->type != 'I'){
                    if(!in_array($employee_id.'+'.$employee_date->date,$employee_attendance_status))
                        array_push($employee_attendance_status, $employee_id.'+'.$employee_date->date);    
                }
            
                elseif($count % 2 == 0 && $checktype->type == 'I')
                    $count_in++;

                elseif($count % 2 == 1 && $checktype->type != 'O'){
                    if(!in_array($employee_id.'+'.$employee_date->date,$employee_attendance_status))
                        array_push($employee_attendance_status, $employee_id.'+'.$employee_date->date);    
                }

                elseif($count % 2 == 1 && $checktype->type == 'O')
                    $count_out++;  

                $count++;
            }
            //If IN and OUT count Is Not Equal
            if($count_in != $count_out){
                if(!in_array($employee_id.'+'.$employee_date->date,$employee_attendance_status))
                    array_push($employee_attendance_status, $employee_id.'+'.$employee_date->date);   
            }
        }

        //Giving shape to contents in array
        for($i=0;$i<count($employee_attendance_status); $i++)
            $employee_attendance_status[$i] = explode('+',$employee_attendance_status[$i]);

        return $employee_attendance_status;
    }

    static public function evaluateFaultedAttendanceByUserAndDate($employee_id,$date){
        $employee_attendance_status = Array();
        $count = 0; $count_in = 0; $count_out = 0;
            foreach(EmployeeAttendance::attendenceByUserAndDate($employee_id,$date) as $checktype){
                if($count % 2 == 0 && $checktype->type != 'I'){
                    if(!in_array($employee_id.'+'.$date,$employee_attendance_status))
                        array_push($employee_attendance_status, $employee_id.'+'.$date);    
                }
            
                elseif($count % 2 == 0 && $checktype->type == 'I')
                    $count_in++;

                elseif($count % 2 == 1 && $checktype->type != 'O'){
                    if(!in_array($employee_id.'+'.$date,$employee_attendance_status))
                        array_push($employee_attendance_status, $employee_id.'+'.$date);    
                }

                elseif($count % 2 == 1 && $checktype->type == 'O')
                    $count_out++;  

                $count++;
            }
            //If IN and OUT count Is Not Equal
            if($count_in != $count_out){
                if(!in_array($employee_id.'+'.$date,$employee_attendance_status))
                    array_push($employee_attendance_status, $employee_id.'+'.$date);   
            }

        //Giving shape to contents in array
        for($i=0 ; $i<count($employee_attendance_status) ; $i++)
            $employee_attendance_status[$i] = explode('+',$employee_attendance_status[$i]);

        return $employee_attendance_status;
    }

    static public function evaluateFaultedAttendanceByUser($employee_id){
        $employee_attendance_status = Array();
        foreach(EmployeeAttendance::attendanceDatesByUser($employee_id) as $date){
            $count = 0; $count_in = 0; $count_out = 0;
            foreach(EmployeeAttendance::attendanceByUserAndDate($employee_id, $date->date) as $checktype){
                if($count % 2 == 0 && $checktype->type != 'I'){
                    if(!in_array($employee_id.'+'.$date->date,$employee_attendance_status))
                        array_push($employee_attendance_status, $employee_id.'+'.$date->date);    
                }
            
                elseif($count % 2 == 0 && $checktype->type == 'I')
                    $count_in++;

                elseif($count % 2 == 1 && $checktype->type != 'O'){
                    if(!in_array($employee_id.'+'.$date->date,$employee_attendance_status))
                        array_push($employee_attendance_status, $employee_id.'+'.$date->date);    
                }

                elseif($count % 2 == 1 && $checktype->type == 'O')
                    $count_out++;  

                $count++;
            }
            //If IN and OUT count Is Not Equal
            if($count_in != $count_out){
                if(!in_array($employee_id.'+'.$date->date,$employee_attendance_status))
                    array_push($employee_attendance_status, $employee_id.'+'.$date->date);
            }
        }

        //Giving shape to contents in array
        for($i=0 ; $i<count($employee_attendance_status) ; $i++)
        $employee_attendance_status[$i] = explode('+',$employee_attendance_status[$i]);

        return $employee_attendance_status;
    }

    static public function evaluateFaultedAttendanceByDate($from){
        $employee_attendance_status = Array();
        foreach(EmployeeAttendance::pluckUserIdsFromAttendance() as $user_id){
            $count = 0; $count_in = 0; $count_out = 0;
            foreach(EmployeeAttendance::attendanceByUserAndDate($user_id ,$from) as $checktype){
                if($count % 2 == 0 && $checktype->type != 'I'){
                    if(!in_array($user_id.'+'.$from,$employee_attendance_status))
                        array_push($employee_attendance_status, $user_id.'+'.$from);    
                }
            
                elseif($count % 2 == 0 && $checktype->type == 'I')
                    $count_in++;

                elseif($count % 2 == 1 && $checktype->type != 'O'){
                    if(!in_array($user_id.'+'.$from,$employee_attendance_status))
                        array_push($employee_attendance_status, $user_id.'+'.$from);    
                }

                elseif($count % 2 == 1 && $checktype->type == 'O')
                    $count_out++;  

                $count++;
            }
            //If IN and OUT count Is Not Equal
            if($count_in != $count_out){
                if(!in_array($user_id.'+'.$from,$employee_attendance_status))
                    array_push($employee_attendance_status, $user_id.'+'.$from);
            }
        }
        //Giving shape to contents in array
        for($i=0 ; $i<count($employee_attendance_status) ; $i++)
        $employee_attendance_status[$i] = explode('+',$employee_attendance_status[$i]);

        return $employee_attendance_status;
    }

    static public function workingHoursOfPreviousMonth(){
        $start = new \Carbon\Carbon('first day of last month');
        $start->startOfMonth();
        $end = new \Carbon\Carbon('last day of last month');
        $end->endOfMonth();
        $EmployeeHours = Array();
        foreach(\App\Models\HRM\Employees::pluckActiveWithID() as $employee){
            //Extracting Employee Ids Forcefully :D
            $employee_id = trim(explode('-', $employee)[0]);
            $EmployeeHours[$employee_id] = EmployeeAttendance::evaluateAttendanceDaysByUserIdAndDates($employee_id, $start, $end);
        }
        return $EmployeeHours;
    }

    static public function workingHoursOfGivenMonth($month, $year){
        $start = EmployeeAttendance::getFirstDayOfMonth($month, $year);
        $end = EmployeeAttendance::getLastDayOfMonth($month, $year);
        $EmployeeHours = Array();
        foreach(\App\Models\HRM\Employees::pluckActiveWithID() as $employee){
            //Extracting Employee Ids Forcefully :D
            $employee_id = trim(explode('-', $employee)[0]);
            $EmployeeHours[$employee_id] = EmployeeAttendance::evaluateAttendanceDaysByUserIdAndDates($employee_id, $start, $end);
        }
        return $EmployeeHours;
    }

    static public function evaluateAttendanceDaysByUserIdAndDates($user_id, $from, $to){
        $FaultedDates = collect(EmployeeAttendance::evaluateFaultedAttendance( $user_id, $from, $to ))->pluck('1');
        $TempDates = EmployeeAttendance::attendanceDatesByUserId( $user_id, $from, $to );
        $Dates = Array();
        foreach($TempDates as $date){
            array_push($Dates, $date->date);
        }
        $Dates = collect($Dates);
        $ValidDates = $Dates->diff($FaultedDates);

        $Hours = Array();
        foreach($ValidDates as $date){
            $Hours[$date] = EmployeeAttendance::getWorkHoursByUserAndDate($user_id, $date);
        }

        return $Hours;

    }

    static public function getWorkHoursByUserAndDate($user_id, $date){

        $DayAttendance = EmployeeAttendance::attendenceByUserAndDate($user_id, $date);
        $Durations = Array();
        $data = Array();

        if(count($DayAttendance) > 0){
            //Finding Out Working Hours Of A Single Day
            foreach($DayAttendance as $attendance){

                $Attendance = Array();
                if($attendance->type == 'I'){
                    $date_time = explode(' ',$attendance->date_time);
                    $in_time = $date_time[1];
                }
                else if($attendance->type == 'O'){

                    $date_time = explode(' ',$attendance->date_time);
                    $Duration = \Carbon\Carbon::parse($date_time[1])->diffInSeconds(\Carbon\Carbon::parse($in_time));
                    array_push($Durations, $Duration);
                    $in_time = '';
                }
            }

            //Matching In/Out Timings with Shift Timings //[0] For Date [1] For Time
            $data['office_in'] = explode(' ',EmployeeAttendance::firstInByUserAndDate($user_id, $date))[1];
            $data['office_out'] = explode(' ',EmployeeAttendance::LastOutByUserAndDate($user_id, $date))[1];
            $data['worked_hours'] = EmployeeAttendance::convertTimeToFloat(gmdate("H:i:s", array_sum($Durations)));
            //A database Check would be implied in future here
            if(!empty(EmployeeWorkingDays::checkExistence($user_id, $date)))
                $data['attendance_type'] = EmployeeWorkingDays::checkExistence($user_id, $date)->type_id;
            else
                $data['attendance_type'] = EmployeeAttendance::getAttendanceType($user_id, $data['office_in'], $data['office_out']);
        }

        return $data;
    }

    static public function getAttendanceType($user_id, $in_time, $out_time){ 

        $Employee = \App\Models\HRM\Employees::getEmployeeByUserId($user_id);
        $employee_shift = \App\Models\HRM\WorkShifts::getShiftById($Employee->shift_id);
        $shift_id = $employee_shift->id;
        $shift_start_time = $employee_shift->shift_start_time;
        $shift_end_time = $employee_shift->shift_end_time;
        $working_hours_per_day = $employee_shift->working_hours_per_day;

        foreach($employee_shift->attendance_settings as $attendance_setting){
            $range['in_lower'] = date("H:i:s", strtotime($shift_start_time) - ($attendance_setting->in_start * 60));
            $range['in_upper'] = date("H:i:s", strtotime($shift_start_time) + ($attendance_setting->in_end * 60));
            $range['out_lower'] = date("H:i:s", strtotime($shift_end_time) - ($attendance_setting->out_start * 60));
            $range['out_upper'] = date("H:i:s", strtotime($shift_end_time) + ($attendance_setting->out_end * 60));
            if(strtotime($range['in_lower']) <= strtotime($in_time) && strtotime($in_time) <= strtotime($range['in_upper']) && strtotime($range['out_lower']) <= strtotime($out_time) && strtotime($out_time) <= strtotime($range['out_upper']))
                return $attendance_setting->attendance_type;
        }
        return 3;
    }

    static public function convertTimeToFloat($time){
        $parts = explode(':', $time);
        return $parts[0] + floor(($parts[1]/60)*100) / 100;
    }

    static public function getFirstDayOfMonth($month, $year){
        $current_month = \Carbon\Carbon::now()->month;
        $difference = $current_month - $month;
        $start = \Carbon\Carbon::now()->startOfMonth()->modify('-'.$difference.' month')->toDateString();
        $start = str_replace(\Carbon\Carbon::now()->year, '20'.$year, $start);

        return $start;
    }

    static public function getLastDayOfMonth($month, $year){
        $current_month = \Carbon\Carbon::now()->month;
        $difference = $current_month - $month;
        $end = \Carbon\Carbon::now()->startOfMonth()->modify('-'.$difference.' month')->endOfMonth()->toDateString();
        $end = str_replace(\Carbon\Carbon::now()->year, '20'.$year, $end);

        return $end;
    }

    public function employee() {
        return $this->belongsTo('App\Models\HRM\Employees', 'user_id', 'user_id');
    }

}
