<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Branches;


class StaffTransferBranch extends Model
{
    protected $fillable = [

        "user_id",
        "staff_status",
        "branch_id",

    ];

    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
}
