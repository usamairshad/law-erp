<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesTada extends Model
{
    use SoftDeletes;

    protected $fillable = ['job_title_id', 'tada_amount', 'year',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_employees_tada';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    public function job_name()
    {
        return $this->belongsTo('App\Models\HRM\JobTitle','job_title_id');
    }
}
