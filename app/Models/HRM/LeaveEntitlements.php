<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveEntitlements extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'employee_id', 'no_of_days', 'days_used', 'start_date', 'end_date', 'credited_date', 'leave_type_id',
        'status', 'created_by', 'updated_by'. 'deleted_by'
    ];

    protected $table = 'erp_hrm_leave_entitlements';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
}
