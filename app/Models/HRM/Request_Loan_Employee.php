<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Request_Loan_Employee extends Model
{
    //
	protected $fillable=['type','amount','installments','monthly_installment','reason','remarks','approved_by_ceo','approved_by_hr','created_at','updated_at'];
    protected $table='request_loan_employee';
}
