<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttendanceSettings extends Model
{
    use SoftDeletes;

    protected $fillable = ['shift_id', 'in_start', 'in_end', 'in_range', 'out_start','out_end' ,'out_range',
        'attendance_type',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    public function shift_name()
    {
        return $this->belongsTo('App\Models\HRM\WorkShifts','shift_id');
    }

    

}
