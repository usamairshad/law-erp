<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveRequests extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'employee_id', 'leave_type_id', 'work_shift_id', 'start_date', 'end_date', 'total_days', 'partial_days',
        'applied_date', 'comments','leave_status',
        'single_duration', 'single_shift', 'single_hours_start', 'single_hours_end', 'single_hours_duration',
        'all_days_duration', 'all_days_shift', 'all_days_hours_start', 'all_days_hours_end', 'all_days_hours_duration',
        'starting_duration', 'starting_shift', 'starting_hours_start', 'starting_hours_end', 'starting_hours_duration',
        'ending_duration', 'ending_shift', 'ending_hours_start', 'ending_hours_end', 'ending_hours_duration',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_leave_requests';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    /**
     * Get the employee that the leave request belongs to.
     */
   // public function employee()
   // {
     //   return $this->belongsTo('App\Models\HRM\Employees', 'employee_id','user_id');
        //return $this->belongsTo('App\Models\HRM\Employees', 'user_id');
   // }

    /**
     * Get the employee that the leave request belongs to.
     */
    public function leave_statuses()
    {
        return $this->belongsTo('App\Models\HRM\LeaveStatuses', 'leave_status','id');

    }

    /**
     * Get the work shift that the leave request belongs to.
     */
    public function work_shift()
    {
        return $this->belongsTo('App\Models\HRM\WorkShifts', 'work_shift_id');
    }

    /**
     * Get the work shift that the leave request belongs to.
     */
    public function leave_type()
    {
        return $this->belongsTo('App\Models\HRM\LeaveTypes', 'leave_type_id');
    }

    /**
     * Get the work shift that the leave request belongs to.
     */
    public function leaves()
    {
        return $this->hasMany('App\Models\HRM\Leaves', 'leave_request_id');
    }
}
