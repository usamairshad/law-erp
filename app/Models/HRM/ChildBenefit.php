<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class ChildBenefit extends Model
{
	use SoftDeletes;

    protected $fillable = ['student_id', 'teacher_id', 'percentage', 'status'];

    protected $table = 'erp_child_benefit';

}
