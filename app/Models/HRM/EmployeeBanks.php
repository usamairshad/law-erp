<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeBanks extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'bank_id', 'title', 'account_no', 'iban','branch' ,'branch_code',
        'type',
        'status', 'created_by', 'updated_by','deleted_by'];

    protected $table = 'erp_hrm_employee_bank_accounts';


    public function emp_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','user_id','user_id');
    }

    public function bank_name()
    {
        return $this->belongsTo('App\Models\Admin\BanksModel','bank_id');
    }
}
