<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeKpis extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'name','year','value', 'weight',
        'acheived', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_hrm_emp_kpis';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    public function user_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','user_id', 'user_id');
    }

    public function creator_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','created_by', 'user_id');
    }

}
