<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class EobiEpfModel extends Model
{
    //
    protected $fillable = ['staff_id','branch_id','company_id','amount','status','month','year'];
    protected $table = 'eobi_epf_register';
}
