<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class ExamTimeTable extends Model
{
    protected $fillable = ['time_table_id','date','created_by','exam_id'];
    protected $table = 'exam_time_tables';
}
