<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignTarget extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'target_id','value', 'weight',
        'status', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_hrm_target_assigned';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    static public function getUserTarget($user_id, $target_id) {
        return self::where(['target_id' => $target_id ,'user_id'=> $user_id ,'status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    static public function getMyTeamTargets($user_ids) {
        return self::whereIn('user_id', $user_ids)->OrderBy('user_id','asc')->get();
    }

    public function user_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','user_id', 'user_id');
    }

    public function creator_name()
    {
        return $this->belongsTo('App\Models\HRM\Employees','created_by', 'user_id');
    }

    public function target_name()
    {
        return $this->belongsTo('App\Models\HRM\EmployeesTarget','target_id');
    }
}
