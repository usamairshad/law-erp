<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeDocuments extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'user_id', 'document_type', 'file_location', 'created_at','created_by'
                     ,'updated_at', 'updated_by', 'deleted_at', 'deleted_by'];
                   
    protected $table = 'erp_hrm_employee_documents';      
    
    public function employee(){
        return $this->belongsTo('App\Models\HRM\EmployeeDocuments','user_id');
    }

    public function employee_document_type(){
        return $this->belongsTo('App\Models\HRM\EmployeeDocumentTypes','document_type');
    }

}
