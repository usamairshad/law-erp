<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobTitle extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','department_id', 'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_job_title';

    public function employees(){
        return $this->hasMany('App\Models\HRM\Employees','job_title');
    }

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    static public function pluckActiveMrkOnly() {
        return self::where(['status' => 1])->where(['department_id' => 4])->OrderBy('name','asc')->pluck('name','id');
    }
}
