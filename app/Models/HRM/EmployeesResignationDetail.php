<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesResignationDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'employee_id','termination_reason', 'date_of_resign',  'created_by', 'updated_by'. 'deleted_by'
    ];

    protected $table = 'erp_hrm_employees_resignation_detail';

}
