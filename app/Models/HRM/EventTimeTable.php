<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class EventTimeTable extends Model
{
    protected $fillable = ['time_table_id','event_id','date','created_by','updated_by'];
    protected $table = 'event_time_tables';
}
