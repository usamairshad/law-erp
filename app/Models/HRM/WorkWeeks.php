<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkWeeks extends Model
{
    use SoftDeletes;

    protected $fillable = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun', 'working_days', 'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_work_weeks';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    static public function getWorkingDays(){
        return self::findOrFail(1);
    }
}
