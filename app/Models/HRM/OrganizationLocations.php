<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationLocations extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'is_default', 'organization_id', 'street_1', 'street_2',
        'city', 'zip_code', 'province', 'phone', 'fax', 'country', 'notes',
        'status', 'created_by', 'updated_by'. 'deleted_by'];

    protected $table = 'erp_hrm_organization_locations';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
}
