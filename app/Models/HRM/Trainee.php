<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Trainee extends Model
{
    protected $fillable = [

        "staff_id",
        "training_id",
        "avalibility",
        "remarks",
        "acknowledge",


    ];

    protected $table = 'trainees';


    public function training()
    {
        return $this->belongsTo("App\Models\HRM\Training", "training_id", "id");
    }


    public function user()
    {
        return $this->belongsTo("App\User", "staff_id", "id");
    }
}
