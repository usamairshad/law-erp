<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['id', 'branch_id', 'member_type', 'member_id', 'title','file'
                     ,'description','status','created_by', 'updated_by', 'deleted_by'];        
    protected $table = 'documents';
}
