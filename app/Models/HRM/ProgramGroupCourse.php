<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class ProgramGroupCourse extends Model
{
    protected $fillable = ['program_group_id','course_id'];

    protected $table = 'program_group_courses';
}
