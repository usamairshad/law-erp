<?php 
namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = [ 'created_at','updated_at','name', 'display_name', 'description'];

    public static function roleList($id = 0)
    {
        $list = '';
        $roles = self::orderBy('name', 'asc')->get(['name', 'id']);
        foreach ($roles as $role) {
            $list .= '<option ' . (($id == $role->id) ? 'selected' : '') . ' value="' . $role->id . '">' . $role->name . '</option>';
        }
        return $list;
    }
    public function user()
    {
        return $this->hasMany('App\Models\User');
    }
}