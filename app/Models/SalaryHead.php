<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryHead extends Model
{
    protected $table = 'erp_salary_head';
    protected $fillable = ['id','rec_group_id','inc_group_id','salary_head'];
   
}
