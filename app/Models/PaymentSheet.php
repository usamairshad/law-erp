<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentSheet extends Model
{
    protected $table = 'erp_payment_sheet';
    protected $fillable = ['sheet_type','sheet_code','prepared_by','ledger_head_id','city_id','company_id'];
   
}
