<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeeHead extends Model
{
    protected $table = 'erp_fee_heads';
    protected $fillable = ['created_by','branch_id', 'last_updated_by','program_id', 'dividable','fee_section_id','fee_head_desc','fee_head_title', 'slug', 'status'];
    public function tuFeeStructure()
    {
        return $this->belongsTo(FeeStructure::class,'id', 'fee_head_id');
    }
}
