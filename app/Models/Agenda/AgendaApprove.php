<?php

namespace App\Models\Agenda;

use Illuminate\Database\Eloquent\Model;

class AgendaApprove extends Model
{
    protected $table = 'approve_agendas';
    protected $fillable = ['agenda_request_id', 'date', 'time', 'in_campus', 'location', 'description'];
}
