<?php

namespace App\Models\UtilityBills;

use Illuminate\Database\Eloquent\Model;

class UtilityBills extends Model
{
    protected $table = "utilitybills";
    protected $fillable = ['branch_id', 'month', 'meter_no', 'nature','bill_amount',
                          'with_holding_tax', 'total_bill', 'last_month_bill', 'increase_decease_bill',
                            'reason', 'status'];
    public function branch(){
        return $this->belongsTo('App\Models\Admin\Branches','branch_id','id');
    }
}
