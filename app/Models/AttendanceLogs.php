<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceLogs extends Model
{
//
    protected $table = 'erp_attendance_logs';
    protected $fillable = ['device_id', 'branch_id', 'user_id', 'status', 'date', 'time'];



}