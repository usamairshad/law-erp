<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Laws extends Model
{
    protected $table = 'erp_laws';
    protected $fillable = ['law_section','description','group_id'];
   
}
