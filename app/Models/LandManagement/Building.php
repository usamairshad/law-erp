<?php

namespace App\Models\LandManagement;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = 'erp_buildings';
    protected $fillable = ['aggreement_date','basic_rent','branch_id','agreement_end_date',
    'security', 'tenure', 'increment'];
    public function payeelists()
    {
        return $this->belongsTo('App\Models\LandManagement\PayeeList');
    }
//    public function payees_amount()
//    {
//        return $this->hasMany('App\Payees_amount');
//    }
}
