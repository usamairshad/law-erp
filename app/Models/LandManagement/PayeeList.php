<?php

namespace App\Models\LandManagement;

use App\Models\Admin\Branches;
use Illuminate\Database\Eloquent\Model;

class PayeeList extends Model
{
    protected $table = 'erp_payee_lists';

    protected $fillable = ['payee_name', 'vendor_type', 'branch_id', 'phone', 'email', 'address'];
    public function branches()
    {
        return $this->belongsTo('App\Models\Admin\Branches', 'branch_id', 'id');
    }
    public function buildings()
    {
        return $this->hasMany('App\Models\LandManagement\Building');
    }
//    public function payees_amount()
//    {
//        return $this->hasMany('App\Payees_amount');
//    }

    public static function allpayees($id=0){
        $list='';
        $payees=self::all('payee_name','id');
        foreach ($payees as $payee){
            $list.='<option '.(($id==$payee->id)?'selected':'').' value="'.$payee->id.'">'.$payee->payee_name.'</option>';
        }

        return $list;
    }
}
