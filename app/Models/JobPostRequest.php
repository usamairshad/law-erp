<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPostRequest extends Model
{
   protected $fillable = ['branch_id','section','education','experience','subject_id','gender','program_id','status','created_by','updated_by','deleted_by','deleted_at','expected_joining','description','location','skill','job_title','job_category'
    ];

    protected $table = 'job_post_request';
    
    public  function branch(){
        return $this->belongsTo('App\Models\Academics\Branch','branch_id');
    }
    
}
