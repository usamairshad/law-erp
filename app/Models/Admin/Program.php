<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Program extends Model
{
    protected $table = 'erp_program';
    protected $fillable = ['created_by', 'last_updated_by', 'branch_id', 'session_id', 'term_id', 'program', 'status', 'code', 'board_id',
        'start_month', 'end_month', 'duration', 'grading_id', 'basic_fee', 'fee_type_id'

    ];

    public function subject() {
        return $this->belongsToMany(SubjectDetail::class, 'category_id');
    }
    public function subjectcategory() {
        return $this->belongsTo(SubjectCategoryModel::class, 'category_id');
    }

    public function subprogram() {
        return $this->belongsToMany(ProgramTerm::class, 'program_term');
    }
    public function Startmonth() {
        return $this->belongsTo(Month::class, 'start_month');
    }
    public function Endmonth() {
        return $this->belongsTo(Month::class, 'end_month');
    }

    public function fee_type()
    {
        return $this->belongsTo(FeeType::class);
    }

    public function programInfo()
    {
        return $this->hasMany(ProgramTerm::class, 'program_id', 'id');
    }
    public function gradingType()
    {
        return $this->hasOne(GradingType::class, 'id','grading_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
    public function session()
    {
        return $this->belongsTo(Sessions::class, 'session_id');
    }
    public function board()
    {
        return $this->belongsTo(Board::class, 'board_id');
    }
}
