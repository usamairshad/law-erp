<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AcademicYear extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['name', 'start_month', 'end_month', 'start_year', 'end_year', 'status', 'created_by', 'updated_by', 'deleted_by','session_id'];

    protected $table = 'erp_academic_year';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
    public  function branch(){
        return $this->belongsTo('App\Models\Admin\Branches','branch_name');
    }
}
