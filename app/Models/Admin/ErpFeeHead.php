<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ErpFeeHead extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['title','fee_section_id','group_id','description','status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_fee_head';

    public  function feeSection(){
        return $this->belongsTo('App\Models\Admin\FeeSection');
    }
    public function  program(){
        return $this->belongsTo('App\Models\Admin\Program');
    }
}
