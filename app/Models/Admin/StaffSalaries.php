<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class StaffSalaries extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'month_id','year_id', 'leaves','ledger_id', 'description', 'Amount', 'deduction', 'net_salary' , 'date', 'staff_id','deleted_at','medical_allowance','house_rent_allowance','conveyance_allowance','training_all','eobi','provident_fund','tax_amount','loan_amount','remaining_amount','pay_loan','salary_slip_id'
    ];

    protected $table = 'erp_staff_salaries';
}
