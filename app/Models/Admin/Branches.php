<?php

namespace App\Models\Admin;

use App\Models\LandManagement\PayeeList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Branches extends Model
{
    use SoftDeletes;

    //
    protected $fillable = ['name', 'city_id', 'company_id', 'country_id', 'branch_code', 'phone', 'fax', 'cell', 'address', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_branches';

    public static function allBranches()
    {

        $Branches  = Branches::selectRaw('id,name,region_id')
            ->where(['status' => 1])
            ->get();
        return $Branches;
    }

    static public  function branchDetailById($id)
    {
        $Branch  = Branches::selectRaw('id,name')
            ->where(['status' => 1])
            ->where(['id' => $id])
            ->first();
        return $Branch;
    }

    public static function BranchesByRegion($id)
    {
        $Branches  = Branches::selectRaw('id,name,region_id')
            ->where(['status' => 1])
            ->where(['region_id' => $id])
            ->get();
        return $Branches;
    }

    public static function allActiveBranches()
    {
        $Branches  = Branches::selectRaw('id,name,region_id')
            ->where(['status' => 1])

            ->get();
        return $Branches;
    }

    static public function pluckActiveOnly()
    {
        return self::where(['status' => 1])->OrderBy('name', 'asc')->pluck('name', 'id', 'region_id');
    }

    static public function pluckRegionBranches($region_id)
    {
        return self::where(['status' => 1, 'region_id' => $region_id])->OrderBy('name', 'asc')->pluck('name', 'id');
    }
    static public function pluckAllBranches()
    {
        return self::where(['status' => 1])->OrderBy('name', 'asc')->pluck('name', 'id');
    }
    static public function pluckRegionBranchesIds($region_id)
    {
        return self::where(['status' => 1, 'region_id' => $region_id])->OrderBy('name', 'asc')->pluck('id');
    }
    public  function country()
    {
        return $this->belongsTo('App\Models\Admin\Country', 'country_id');
    }
    public  function company()
    {
        return $this->belongsTo('App\Models\Admin\Company', 'region_id');
    }
    public  function city()
    {
        return $this->belongsTo('App\Models\Admin\City', 'city_id');
    }

    public function getName($id)
    {
        return $this->name;
    }
    public static function branchList($id = 0)
    {
        $list = '';
        $branches = self::where('status', 1)->orderBy('name', 'asc')->get(['name', 'id']);
        foreach ($branches as $branch) {
            $list .= '<option ' . (($id == $branch->id) ? 'selected' : '') . ' value="' . $branch->id . '">' . $branch->name . '</option>';
        }
        return $list;
    }

    public function payeelist()
    {
        return $this->belongsTo(PayeeList::class);
    }
    public function student()
    {
        return $this->hasMany(Students::class,'branch_id','id');
    }
    public function cities()
    {
        return $this->belongsTo('App\Models\Admin\City','city_id','id');
    }
    public function utiltybills(){
        return $this->hasMany('App\Models\UtilityBills\UtilityBills');
    }
}
