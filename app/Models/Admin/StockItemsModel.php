<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockItemsModel extends Model
{
    use SoftDeletes;

    protected $guarded =['id'];

    protected $table = 'erp_stockitems';

    public static function stockitemsall(){
        $StockItems  = StockItemsModel::selectRaw('stockitems.*, suppliers.sup_name, stockcategory.cat_name')
            ->Join('suppliers','stockitems.sup_id','=','suppliers.id')
            ->Join('stockcategory','stockitems.cat_id','=','stockcategory.id')->OrderBy('created_at','desc')
            ->get();
        return $StockItems;
    }

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('st_name','asc')->pluck('st_name','id');
    }

    public function products(){
        return $this->hasMany('App\Models\Admin\ProductsModel','st_name');
    }
    public function stockProducts(){
        return $this->belongsTo('App\Models\Admin\ProductsModel','st_name');
    }
    public function unitsModel()
    {
        return $this->belongsTo('App\Models\Admin\UnitsModel','sale_unit');
    }
    public function branches()
    {
        return $this->belongsTo('App\Models\Admin\Warehouse','branch_id');
    }
    public static function stockProduct($name){

        $StockItems  = StockItemsModel::selectRaw('stockitems.*, products.products_name')
            ->Join('products','stockitems.st_name','=','products.id')
            ->where('products_name', 'like', '%' .$name . '%')
            ->OrderBy('created_at','desc')
            ->get();
        return $StockItems;
    }
    public static function inventoryProduct($name,$warehouse){

        $StockItems  = StockItemsModel::selectRaw('stockitems.*, products.products_name')
            ->Join('products','stockitems.st_name','=','products.id')
            ->where('products_name', 'like', '%' .$name . '%')
            ->where('branch_id',$warehouse)
            ->OrderBy('created_at','desc')
            ->get();
        return $StockItems;
    }
    public static function issueProduct($name){

        $StockItems  = StockItemsModel::selectRaw('stockitems.*, products.products_name')
            ->Join('products','stockitems.st_name','=','products.id')
            ->where('products_name', 'like', '%' .$name . '%')
            ->OrderBy('created_at','desc')
            ->get();
        return $StockItems;
    }
    public static function categoryByname(){

        return StockCategoryModel::OrderBy('cat_name','asc')->get()->pluck('cat_name','id');
    }
    public static function brandByname(){

        return SuppliersModel::OrderBy('sup_name','asc')->get()->pluck('sup_name','id');
    }

    public static function getproductBycatBrand($sup_id , $cat_id){
        $StockItems  = StockItemsModel::selectRaw('stockitems.*, products.*')
            ->Join('products','stockitems.st_name','=','products.id')
            ->where('suplier_id',$sup_id)
            ->where('cat_id',$cat_id)
            ->where('closing_stock','>',0)
            ->get();
           // dd($StockItems);
        return $StockItems;
      
    }

   
    public static function getproductBycatReport($sup_id , $cat_id){
        $StockItems  = StockItemsModel::selectRaw('stockitems.*, products.*')
            ->Join('products','stockitems.st_name','=','products.id')
            ->where('suplier_id',$sup_id)
            ->where('cat_id',$cat_id)
            ->where('closing_stock','>',0)
            ->get();
           // dd($StockItems);
        return $StockItems;
        //return StockItemsModel::where('sup_id',$sup_id)->where('cat_id',$cat_id)->get();
    }
    public static function getproductByBrand($sup_id ){
        $StockItems  = StockItemsModel::selectRaw('stockitems.*, products.*')
            ->Join('products','stockitems.st_name','=','products.id')
            ->where('suplier_id',$sup_id)
            ->where('closing_stock','>',0)
            ->get();
        return $StockItems;
        //return StockItemsModel::where('sup_id',$sup_id)->get();
    }
    public static function getproductBycat($cat_id){
        $StockItems  = StockItemsModel::selectRaw('stockitems.*, products.*')
            ->Join('products','stockitems.st_name','=','products.id')
            ->where('cat_id',$cat_id)
            ->where('closing_stock','>',0)
            ->get();
        return $StockItems;
        //return StockItemsModel::where('cat_id',$cat_id)->get();
    }

    public static function updateStockAfterInvoice($stockitem_id, $quantity){
        $closing_stock = self::where(['id' => $stockitem_id])->pluck('closing_stock')->first();
        self::where(['id' => $stockitem_id])->update(['closing_stock' => $closing_stock - $quantity]);
        $closing_stock -= $quantity;

        $purchase_stock = self::where(['id' => $stockitem_id])->pluck('purchase_stock')->first();
        $movement_stock = $purchase_stock - $closing_stock;
        $unit_price = self::where(['id' => $stockitem_id])->pluck('st_unit_price')->first();
        
        self::where(['id' => $stockitem_id])->update(['moments_stock' => $movement_stock]);
        self::where(['id' => $stockitem_id])->update(['stock_value' => $unit_price * $closing_stock]);
    }

}
