<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class TaxSettings extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['tax_class','law_id', 'tax_type', 'fix_amount', 'tax_percent', 'start_range', 'end_range', 'year', 'status', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_tax_settings';

    static public function getAnnualTaxByBasicSalary($basic_salary){
        $basic_annual_salary = $basic_salary * 12;
        return self::where(['status' => '1'])->where('start_range', '<=', $basic_annual_salary)->where('end_range', '>=', $basic_annual_salary)->get();
    }
    static public function getTaxSlab($basic_salary){
        $basic_annual_salary = (int)$basic_salary * 12;
        // dd($basic_salary);
        $tax_slab = self::where(['status' => '1'])->where('start_range', '<=', $basic_annual_salary)->where('end_range', '>=', $basic_annual_salary)->first();
        $fix_amount = $tax_slab->fix_amount;
        $surplus_amount = (int)$basic_annual_salary - ((int)$tax_slab->fix_amount + 1);
        $surplus_tax = ($surplus_amount * (int)$tax_slab->tax_percent /100) ;
        return (int)(($fix_amount + $surplus_tax)/ 12);


    }

}
