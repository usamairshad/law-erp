<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Eobi extends Model
{
    protected $fillable = ['cnic','role_id','user_id','date_of_reg', 'year','contribution','deduction','balance'];

    public function staff()
    {
        return $this->belongsTo("App\Models\Academics\Staff", 'staff_id', "id");
    }
    public function role()
    {
        return $this->belongsTo("App\Models\Role", 'role_id', "id");
    }
    public function user(){
    	return $this->belongsTo("App\User");
    }
}

?>