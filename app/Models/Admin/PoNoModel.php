<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PoNoModel extends Model
{
    use SoftDeletes;
    protected $fillable = ['po_no','po_date','brand_id'];

    protected $table = 'erp_ponumber';

    public  function suppliersModel(){
        return $this->belongsTo('App\Models\Admin\SuppliersModel','brand_id');
    }
}
