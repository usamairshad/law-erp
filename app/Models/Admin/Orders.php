<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class Orders extends Model
{
    //Orders Table
    use SoftDeletes;

    protected $guarded = ['id'];


    protected $table = 'erp_orders';

    public function orderdetail()
    {
        return $this->hasMany('App\Models\Admin\OrderDetailModel','perchas_id');
    }
    public  function suppliersModel(){
        return $this->belongsTo('App\Models\Admin\SuppliersModel','sup_id');
    }
    public  function branches(){
        return $this->belongsTo('App\Models\Admin\Branches','branch_id');
    }
    public function products()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }
}
