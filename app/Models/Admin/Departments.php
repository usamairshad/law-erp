<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\Branches;
use Auth;

class Departments extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['name', 'branch_id', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_departments';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
    public  function branch(){
        return $this->belongsTo('App\Models\Admin\Branches','branch_id');
    }
}
