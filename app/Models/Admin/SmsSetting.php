<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SmsSetting extends Model
{
	protected $table = 'sms_settings';
    protected $fillable = ['created_at', 'updated_at', 'identity', 'logo', 'link', 'config', 'status'];}
