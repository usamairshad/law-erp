<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LcDutyModel extends Model
{
    //
    use SoftDeletes;
    protected $guarded = ['id'];

    protected $table = 'erp_lcduty';

    public  function dutyModel(){
        return $this->belongsTo('App\Models\Admin\DutyModel','duties');
    }
    public  function LcModel(){
        return $this->belongsTo('App\Models\Admin\LcBankModel','ledgerLc_id');
    }
    public static function getDutiesByLcNo($lc_no){
        return self::select('*',sum('duties'))->where(['deleted_at' => NULL])->where(['lc_no' => $lc_no])->get();
        }
    public static function getDuties($id,$cid){
        $duties = array(
            'insurance'=>0,
            'bank_charges'=>0,
            'freight'=>0,
            'profit_invest'=>0,
            'murahaba_profit'=>0,
            'mvmnt'=>0,
            'duty'=>0,
        );
        $LcDutyModel = LcDutyModel::selectRaw('sum(lcduty.amount)As amt,duty.id,duty.type')
            ->Join('duty','lcduty.duties','=','duty.id')
            ->where('lcduty.ledgerLc_id', '=',$id)
            ->where('lcduty.Comercial_id', '=',$cid)
            ->where('lcduty.status', '=',1)
            ->where('duty.costing', '=',1)
            ->groupBy('duty.type')
            ->get();
        //dd($LcDutyModel);
        if(count($LcDutyModel) > 0){
            foreach($LcDutyModel as $lcduty){
                if(isset($lcduty->type) && $lcduty->type == 1){
                    $duties['insurance'] = $lcduty->amt;
                }
                if(isset($lcduty->type) && $lcduty->type == 2){
                    $duties['bank_charges'] = $lcduty->amt;
                }
                if(isset($lcduty->type) && $lcduty->type == 3){
                    $duties['freight'] = $lcduty->amt;
                }
                if(isset($lcduty->type) && $lcduty->type == 4){
                    $duties['profit_invest'] = $lcduty->amt;
                }
                if(isset($lcduty->type) && $lcduty->type == 5){
                    $duties['murahaba_profit'] = $lcduty->amt;
                }
                if(isset($lcduty->type) && $lcduty->type == 6){
                    $duties['mvmnt'] = $lcduty->amt;
                }
                if(isset($lcduty->type) && $lcduty->type == 7){
                    $duties['duty'] = $lcduty->amt;
                }
        }
        }
        return $duties;
    }
    public static function getInsuranceDuties($id){
        $duties = array(
            'insurance'=>0,
            'bank_charges'=>0,
        );
        $LcDutyModel = LcDutyModel::selectRaw('sum(lcduty.amount)As amt,duty.id,duty.type')
            ->Join('duty','lcduty.duties','=','duty.id')
            ->where('lcduty.ledgerLc_id', '=',$id)
            ->where('lcduty.status', '=',1)
            ->where('duty.costing', '=',1)
            ->groupBy('duty.type')
            ->get();
        //dd($LcDutyModel);
        if(count($LcDutyModel) > 0){
            foreach($LcDutyModel as $lcduty){
                if(isset($lcduty->type) && $lcduty->type == 1){
                    $duties['insurance'] = $lcduty->amt;
                }
                if(isset($lcduty->type) && $lcduty->type == 2){
                    $duties['bank_charges'] = $lcduty->amt;
                }
            }
        }
        return $duties;
    }
}
