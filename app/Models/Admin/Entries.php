<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Entries extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'number', 'voucher_date', 'cheque_no', 'cheque_date', 'invoice_no', 'invoice_date',
        'cdr_no', 'cdr_date', 'bdr_no', 'bdr_date', 'bank_name', 'bank_branch', 'drawn_date',
        'entry_type_id', 'employee_id', 'branch_id', 'department_id','suppliers_id',
        'narration', 'remarks', 'dr_total', 'cr_total',
        'status', 'created_by', 'updated_by', 'deleted_by'
    ];

    protected $table = 'erp_entries';
}
