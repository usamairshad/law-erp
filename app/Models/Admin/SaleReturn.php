<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SaleReturn extends Model
{

    protected $table = "erp_sale_return";

    protected $fillable = ['invoice_id', 'return_date', 'description','status', 'created_by', 'updated_by', 'updated_at','created_at', 'deleted_at'];

    public function invoice(){
        return $this->belongsTo('App\Models\Admin\SalesInvoiceDetailModel','invoice_id');
    }
    public function returned_by (){
        return $this->belongsTo('App\Models\HRM\Employees','created_by','user_id');
    }
}
