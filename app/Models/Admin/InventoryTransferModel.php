<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryTransferModel extends Model
{
    use SoftDeletes;

    protected $fillable =[
        'branch_from',
        'branch_to',
        'sup_id',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $table = 'erp_inventorytransfer';

    public function products()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','productid');
    }
    public function suppliersModel()
    {
        return $this->belongsTo('App\Models\Admin\SuppliersModel','sup_id');
    }
    public function stockCategoryModel()
    {
        return $this->belongsTo('App\Models\Admin\StockCategoryModel','cat_id');
    }
    public function branches()
    {
        return $this->belongsTo('App\Models\Admin\Warehouse','branch_from');
    }
    public function branchesto()
    {
        return $this->belongsTo('App\Models\Admin\Warehouse','branch_to');
    }
}
