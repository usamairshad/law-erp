<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Config;


class Students extends Model
{
    protected $fillable = ['created_by', 'last_updated_by','students_id', 'branch_id', 'program_id', 'session_id', 'term_id', 'reg_no', 'reg_date','father_cnic','mother_cnic', 'university_reg','class_id','section_id',
        'faculty','semester', 'section_name', 'academic_status', 'first_name', 'middle_name', 'last_name','user_id', 
        'date_of_birth', 'gender', 'blood_group', 'nationality', 'mother_tongue', 'email', 'extra_info', 'sos_date',
        'student_image','status','proficiency','language_speak', 'parent_id','english_examination', 'board_id','academic_group_id','home_phone','mobile_1','mobile_2','state','country','city','address','temp_address','previous_school','previous_class','student_image'];
    public static $classId;
    public function getSecurityFeeAttribute()
    {
    $fessHead = FeeHead::where('branch_id',$this->branch_id)->where('slug','Security-Fee')->first();
        if($fessHead)
        return $fessHead->tuFeeStructure->fee_amount;
        return 'N/A';
    }
    public function getDiscountAttribute()
    {
     $activeSession = Sessions::where('no_of_years',1)->where('status','Active')->value('id');
     $discount = FeeMaster::where('students_id',$this->id)->where('fee_discount_percentage','>',0)->select('fee_discount_percentage')->get();

    if(!$discount->isEmpty()) {
            $totalHeads = $discount->count();
            $disSum = $discount->sum('fee_discount_percentage');
            $totalDisInPer = $this->averDiscPerHead($totalHeads,$disSum);
            return round($totalDisInPer,Config::get('constants.roundPoints.decimal'));
        }
        return 0;
    }
    public function averDiscPerHead($totalHeads,$tPercentage){
       return $tPercentage/$totalHeads;
}
    public function getBrHeadAttribute()
    {
        $brHead = User::where('branch_id',$this->branch_id)->where('role_id',11)->first();
        if($brHead)
        return $brHead->name;
        return 'N/A';
    }
    public function address()
    {
        return $this->hasOne(Addressinfo::class,'students_id', 'id');
    }
    public function feesMasterHead()
    {
        return $this->hasMany(FeeMaster::class,'students_id');
    }
    public function program()
    {
        return $this->hasOne(Program::class,'id', 'program_id');
    }
    public function term()
    {
        return $this->hasOne(Term::class,'id', 'term_id');
    }
    public function academicStatus()
    {
        return $this->hasOne(StudentStatus::class,'id', 'academic_status');
    }

    public function parents()
    {
        return $this->hasOne(ParentDetail::class, 'students_id', 'id');
    }

    public function guardian()
    {
        return $this->hasOne(StudentGuardian::class, 'students_id', 'id');
    }

   /* public function guardian()
    {
        return $this->belongsTo(StudentGuardian::class);
    }*/
   public  static  function asignClassId($id){
       self::$classId = $id;
   }
   public function studentPrograms(){
       return $this->belongsTo(StudentEnrollment::class, 'program_id','program_id');
   }
    public function studentClass()
    {
        return $this->belongsTo(StudentEnrollment::class, 'id','student_id')->where('section_id',self::$classId);
    }
    public function discountedStudent()
    {
        $active_session = session('active_session');
        return $this->belongsTo(FeeMaster::class, 'id','students_id')->where('session_id',$active_session)->where('fee_discount_percentage','>',0);
    }
    public function class()
    {
        return $this->belongsTo(Classes::class, 'class_id');
    }
    
    public function basic_fee()
    {
        return $this->belongsTo(StudentFeeSetting::class, 'student_id', 'id');
    }
    public function section()
    {
        return $this->belongsTo(FacultySemester::class, 'section_id');
    }
    public function subject()
    {
        return $this->belongsTo(SubjectDetail::class, 'subject_id');
    }
    public function academicInfo()
    {
        return $this->hasMany(AcademicInfo::class, 'students_id', 'id');
    }
    public function branches()
    {
        return $this->belongsTo(Branches::class);
    }
    public function feeMaster()
    {
        return $this->belongsTo(FeeMasterBasic::class, 'student_id', 'id');
    }

    public function feeCollect()
    {
        return $this->hasMany(FeeCollection::class, 'students_id', 'id');
    }


    public function markLedger()
    {
        return $this->hasMany(ExamMarkLedger::class, 'students_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
    public function session()
    {
        return $this->belongsTo(Sessions::class, 'session_id');
    }
    public function faculty()
    {
        return $this->hasOne(Faculty::class,'program', 'program_id');
    }
    public function activeSessionStudent()
    {
        return $this->belongsTo(ActiveSessionStudent::class, 'id', 'student_id');
    }
}
