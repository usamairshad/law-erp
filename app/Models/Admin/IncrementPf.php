<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class IncrementPf extends Model
{
    //

    protected $fillable = ['reason','role_id','user_id','basic', 'pf_previous','pf_new','New_salary_increment','Amount','increment_date'];

    public function staff()
    {
        return $this->belongsTo("App\Models\Academics\Staff", 'user_id', "id");
    }
    public function role()
    {
        return $this->belongsTo("App\Models\Role", 'role_id', "id");
    }
    public function user(){
    	return $this->belongsTo("App\User");
    }
}
