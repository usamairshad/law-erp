<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currencies extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'name',
        'code',
        'symbol',
        'rate',
        'is_default',
        'created_by',
        'updated_by'.
        'deleted_by'
    ];

    protected $table = 'erp_currencies';

    static function getDefaultCurrencty() {
        return self::where(['is_default' => 1])->first();
    }
}
