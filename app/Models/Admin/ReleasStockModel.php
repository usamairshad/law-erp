<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ReleasStockModel extends Model
{
    protected $fillable =[
        'lc_id',
        'product_id',
        'rel_qty',
        'import_date',
    ];

    protected $table = 'erp_releasestock';

    public  function productsModel(){
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }
//    public  function lcModel(){
//        return $this->belongsTo('App\Models\Admin\LcBankModel','lc_id');
//    }
    public  function cinvoiceModel(){
        return $this->belongsTo('App\Models\Admin\ComercialInvoiceModel','lc_id');
    }
}
