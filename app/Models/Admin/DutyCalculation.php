<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class DutyCalculation extends Model
{
    protected $fillable = ['duty_id','percentage','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_duty_calculation';

    public  function dutyName(){
        return $this->belongsTo('App\Models\Admin\DutyModel','duty_id');
    }
}
