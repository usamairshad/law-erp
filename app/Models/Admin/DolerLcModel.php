<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DolerLcModel extends Model
{
    //
    use SoftDeletes;
    //
    protected $guarded = ['id'];

    protected $table = 'erp_lcdoler';
}
