<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\CountryModel;
class FeeSection extends Model
{

    protected $fillable = ['name','description', 'created_by', 'updated_by'];

    protected $table = 'erp_fee_section';


}
