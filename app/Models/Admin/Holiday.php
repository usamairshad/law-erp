<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable = ['holiday_name', 'holiday_date', 'created_by', 'updated_by'];

    protected $table = 'erp_holidays';
}
