<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\Models\Admin\CountryModel;

class Regions extends Model
{
    //
    protected $fillable = ['name','country_id','created_by', 'updated_by'];

    protected $table = 'erp_regions';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    static public function getRegionDetailById($region_id) {
        return self::where(['status' => 1, 'id' => $region_id])->first();
    }

    public  function country(){
        return $this->belongsTo('App\Models\Admin\CountryModel','country_id');
    }
}
