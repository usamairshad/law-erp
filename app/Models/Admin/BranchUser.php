<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class BranchUser extends Model
{
	protected $table = 'erp_branch_users';

    protected $fillable = ['user_id','branch_id','created_by','updated_by','deleted_by'];

    public  function branch(){
        return $this->belongsTo('App\Models\Admin\Branches','branch_id');
    }
}
