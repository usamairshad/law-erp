<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComercialInvoiceModel extends Model
{
    //
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $table = 'erp_c_invoice';

    public  function lCmodel(){
        return $this->belongsTo('App\Models\Admin\LcBankModel','lcno');
    }
    public function performaStockModel()
    {
        return $this->hasMany('App\Models\Admin\PerformaStockModel','ci_id');
    }
    public static function get_order_detail($id){
        $Orders  = Orders::selectRaw('orders.id,orders.perfomra_no, orderdetail.*')
            ->Join('orderdetail','orders.id','=','orderdetail.perchas_id')
            ->where('orders.id', '=',$id)
            ->get();
        return $Orders;
    }

}
