<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsIssueDetailModel extends Model
{
    use SoftDeletes;

    protected $fillable =[
        'gissue_id',
        'product_id',
        'quantity',
        'created_by',
    ];
    protected $table = 'erp_goodsissudetail';

    public function products()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }
}
