<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class SuppliersModel extends Model
{
    //Suppliers Table
    use SoftDeletes;

    protected $fillable =
        [
            'sup_name',
            'sup_address',
            'sup_country',
            'sup_city',
            'sup_contact',
            'sup_email',
            'sup_mobile',
            'status',
            //'sup_company',
            'created_by',
            'updated_by',
            'deleted_by',
            'sup_website',
            'sup_type',
            'sup_bank_account'
        ];

    protected $table = 'erp_suppliers';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('sup_name','asc')->pluck('sup_name','id');
    }
    static public function pluckForeignOnly() {
        return self::where(['sup_type' => 2])->OrderBy('sup_name','asc')->pluck('sup_name','id');
    }
    static public function pluckLocalOnly() {
        return self::where(['sup_type' => 1])->OrderBy('sup_name','asc')->pluck('sup_name','id');
    }
}
