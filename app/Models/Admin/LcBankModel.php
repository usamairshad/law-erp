<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\CoreAccounts;
use Auth;

class LcBankModel extends Model
{   
    use SoftDeletes;

    protected $guarded = ['id'];
    //
//    protected $fillable = ['transaction_type','group_id','pf_no','lc_no','bnk_id','lc_amt','lc_status','open_date','dollar_rate',
//        'bank_charges','ten_margin','bank_commission','lc_expiry_date','2wenty_margin','total_margin',
//        'ammend_charges','today_doller','total_lc_amount','description','document_date','bl_no','document_received',
//        'container_status'  ];
    protected $table = 'erp_lcbank';

//    static public function pluckActiveOnly() {
//        return self::where(['lc_status' => 3 ])->lists('id','transaction_type','lc_no','lc_amt');
//    }
    public  function banksModel(){
        return $this->belongsTo('App\Models\Admin\BanksModel','bnk_id');
    }
    public  function suppliersModel(){
        return $this->belongsTo('App\Models\Admin\SuppliersModel','bnk_id');
    }
    public  function orders(){
        return $this->belongsTo('App\Models\Admin\Orders','pf_no');
    }
    public  function performaStocks(){
        return $this->hasMany('App\Models\Admin\PerformaStockModel','lc_no');
    }
    public  function productsModel(){
        return $this->hasMany('App\Models\Admin\ProductsModel','lc_no');
    }
    public static function getLcBanks(){
        $lcs = self::select('id','lc_no', 'lc_amt', 'lc_expiry_date')->get();
        $concat_lcs = Array();
        foreach($lcs as $lc){
            $temp_string = $lc->lc_no;
            $temp_string .= ' ('.$lc->lc_amt.') ';
            $temp_string .= '('.$lc->lc_expiry_date.')';
            $concat_lcs[$lc->id] = $temp_string;
        }
        return $concat_lcs;
    }

    public static function doubleOrder($name){
        $lcs = self::select('id','pf_no')->where('id',$name)->first();
        if(stristr($lcs->pf_no,',')){
            $order_no = explode(',',$lcs->pf_no);
        }else{
            $order_no[] = $lcs->pf_no;
        }
        $OrdersModel  = Orders::selectRaw('id,po_date,sup_id')
            ->whereIn('id', $order_no)
            ->get();
        return $OrdersModel;
    }
    public static function order($pfno){
        if(stristr($pfno,',')){
            $order_no = explode(',',$pfno);
        }else{
            $order_no[] = $pfno;
        }
        $OrdersModel  = Orders::selectRaw('GROUP_CONCAT(po_number) as po_number')
            ->whereIn('id', $order_no)
            ->first();
        $po_numbers = $OrdersModel->po_number;
       // echo '<pre>';print_r($OrdersModel->po_number);echo'</pre>';exit;
        //$prfn = implode(',',$request['pf_no']);
        return $po_numbers;
    }

    

}

