<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class CalculateMargin extends Model
{
    use SoftDeletes;
    protected $table = 'erp_calculatepaymargin';
    protected $guarded = ['id'];

    public static function updateMargin($data){
        $entry = CalculateMargin::insert($data);
    }
}
