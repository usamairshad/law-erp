<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Company extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name','ntn','fax','phone','logo','address','status', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_companies';

    public function createdBy()
    {
    	return $this->belongsTo('App\User','created_by');
    }
    public function updatedBy()
    {
    	return $this->belongsTo('App\User','updated_by');
    }
    public function branches()
    {
        return $this->hasMany('App\Models\Admin\Branches');
    }
    public static function CompanyList($id = 0)
    {
        $list = '';
        $branches = self::where('status', 1)->orderBy('name', 'asc')->get(['name', 'id']);
        foreach ($branches as $branch) {
            $list .= '<option ' . (($id == $branch->id) ? 'selected' : '') . ' value="' . $branch->id . '">' . $branch->name . '</option>';
        }
        return $list;
    }
}
