<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ChildBenefitConfig extends Model
{
    protected $fillable = ['child_name','discount'];
}
