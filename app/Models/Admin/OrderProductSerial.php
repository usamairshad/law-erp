<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProductSerial extends Model
{  
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $table = 'erp_order_product_serial';

    protected $fillable = ['order_id', 'product_id','serial_no', 'installation_date','warrenty','expires_at','created_by', 'updated_by', 'deleted_by','created_at','updated_at', 'deleted_at'];

    public function productsModel()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel', 'product_id');
    }

}
