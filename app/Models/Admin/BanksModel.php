<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BanksModel extends Model
{
    //Banks Table
    use SoftDeletes;

    protected $fillable = ['bank_name','created_by', 'updated_by','deleted_by'];

    protected $table = 'erp_banks';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('bank_name','asc')->pluck('bank_name','id');
    }

    static public function getBankNameById($bank_id){
        return self::where('id', $bank_id)->pluck('bank_name')->first();
    }
}
