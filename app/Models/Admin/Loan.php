<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = ['amount','role_id','user_id','type', 'paid_status','return_date','remaining','salary_deduct'];

    public function staff()
    {
        return $this->belongsTo("App\Models\Academics\Staff", 'staff_id', "id");
    }
    public function role()
    {
        return $this->belongsTo("App\Models\Role", 'role_id', "id");
    }
    public function user(){
    	return $this->belongsTo("App\User");
    }
}
