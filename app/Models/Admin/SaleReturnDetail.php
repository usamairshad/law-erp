<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SaleReturnDetail extends Model
{

    protected $table = "erp_sale_return_detail";

    protected $fillable = ['sale_return_id', 'product_id', 'original_qty','return_qty','return_serial',
        'status', 'created_by', 'updated_by', 'updated_at','created_at', 'deleted_at'];

    public function productsModel()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }
}
