<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ChildBenefitStudent extends Model
{
    protected $fillable = ['staff_id','student_id','child_benefit_config_id'];

    public function staff()
    {
        return $this->belongsTo("App\Models\Academics\Staff", 'staff_id', "id");
    }
    public function student()
    {
        return $this->belongsTo("App\Models\Academics\Student", 'student_id', "id");
    }
    public function childbenefitconf()
    {
        return $this->belongsTo("App\Models\Admin\ChildBenefitConfig", 'child_benefit_config_id', "id");
    }
}
