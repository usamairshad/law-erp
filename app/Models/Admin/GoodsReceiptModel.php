<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsReceiptModel extends Model
{
    use SoftDeletes;

    protected $fillable =[
        'branch_to',
        'branch_from',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $table = 'erp_goodreceipt';

    public function branchTo()
    {
        return $this->belongsTo('App\Models\Admin\Branches','branch_to');
    }
    public function branchFrom()
    {
        return $this->belongsTo('App\Models\Admin\Branches','branch_from');
    }
}
