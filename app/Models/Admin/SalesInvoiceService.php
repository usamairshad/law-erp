<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Marketing\Databanks;
use App\Models\Marketing\SalePipelines;
use App\Models\Marketing\PipelineProducts;

class SalesInvoiceService extends Model
{

    protected $table = "erp_invoice_services";

    protected $fillable = ['salesinvoice_id', 'description', 'rate', 'amount', 'status', 'created_by', 'updated_by', 'deleted_by', 'deleted_at'];






    static public function getAllSaleKeys(){
        $databanks = SalePipelines::select('id','name')->where(['confirm_status' => 1])->get();
        $ordernos = Array();
        foreach($databanks as $databank){
            $ordernos[$databank->id] = strtoupper(substr(str_replace('-', '', str_replace(' ','',$databank->name)), 0, 4)).'-'.$databank->id;
        }
        $ordernos[''] = 'Select an Order No';
        return $ordernos;
    }

    static public function getInvoiceDate($order_id){
        return SalePipelines::where(['id' => $order_id])->get()->pluck('qoute_date')->first();
    }

    static public function getDueDate($order_id){
        return SalePipelines::where(['id' => $order_id])->where(['deleted_at' => NULL])->get()->pluck('expected_closing_date')->first();
    }

    static public function getCustomerIdByOrderId($order_id){
        return SalePipelines::where(['id' => $order_id])->where(['deleted_at' => NULL])->get()->pluck('databank_id')->first();
    }
    public function CustomersModel(){
        //return $this->belongsTo('App\Models\Admin\Customers','customer_id');
        return $this->belongsTo('App\Models\Marketing\Databanks','customer_id');
    }

}
