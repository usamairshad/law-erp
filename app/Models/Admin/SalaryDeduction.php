<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class SalaryDeduction extends Model
{
    protected $fillable = ['deduction','amount','type'];
}
