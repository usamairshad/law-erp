<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = ['branch_id','device_no','ip_address' , 'port','created_by', 'updated_by'];

    protected $table = 'devices';
}
