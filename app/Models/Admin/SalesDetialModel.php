<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class SalesDetialModel extends Model
{
    //

    //salesdetial Table

    use SoftDeletes;

    protected $fillable =
        [
            'sal_id',
            'sal_st_name',
            'sal_st_unit_price',
            'sale_quantity',
            'sale_amount',
            'status',
        ];


    protected $table = 'erp_salesdetial';
}
