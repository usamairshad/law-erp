<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FixedAssetsModel extends Model
{
    use SoftDeletes;

    protected $guarded =['id' ];


    protected $table = 'erp_fixedassets';

    public function branches()
    {
        return $this->belongsTo('App\Models\Admin\Branches','branch');
    }
    public function suppliersModel()
    {
        return $this->belongsTo('App\Models\Admin\SuppliersModel','customer_name');
    }

//
}
