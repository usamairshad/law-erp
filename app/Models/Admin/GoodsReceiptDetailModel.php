<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsReceiptDetailModel extends Model
{
    use SoftDeletes;

    protected $fillable =[
        'gr_detail_id',
        'product_id',
        'quantity',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $table = 'erp_goodsreceiptdetail';

    public function products()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }
}
