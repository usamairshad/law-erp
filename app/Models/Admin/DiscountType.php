<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class DiscountType extends Model
{
    protected $table = 'discount_types';
    protected $fillable = ['name', 'description', 'percentage','status','created_by','updated_by','deleted_by'];
    }
