<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Country extends Model
{
    protected $fillable = ['name', 'created_by', 'updated_by'];

    protected $table = 'erp_country';
}
