<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetailsModel extends Model
{
    protected $table = "erp_invoice_details";

    protected $fillable = ['salesinvoice_id','no_of_prints', 'type' , 'product_id','serialNo','print_name','total_count', 'quantity', 'return_qty' ,'unit_price', 'total_price','sale_price','tax_amount','tax_percent',
        'discount','discount_type','discount_amount','misc_amount','net_income', 'created_by', 'updated_by', 'deleted_by', 'deleted_at'];

    public function productsModel()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }
}
