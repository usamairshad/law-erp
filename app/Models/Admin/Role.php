<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    public static function roleuser($id = 0)
    {
        $list = '';
        $roles = self::orderBy('name', 'asc')->get(['name', 'id']);
        foreach ($roles as $role) {
            $list .= '<option ' . (($id == $role->id) ? 'selected' : '') . ' value="' . $role->id . '">' . $role->name . '</option>';
        }
        return $list;
    }
    public function user()
    {
        return $this->hasMany('App\User');
    }
}
