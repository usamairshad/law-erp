<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethods extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['name', 'payment_type', 'status', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_payment_methods';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
}
