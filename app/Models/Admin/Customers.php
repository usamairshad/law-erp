<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class Customers extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'name', 'contact_person','databank_id', 'title_1', 'title_2', 'address_1', 'address_2', 'zip_code',
        'phone_1', 'phone_2', 'fax_1', 'fax_2', 'mobile_1', 'mobile_2', 'email', 'website',
        'cnic', 'gst', 'ntn', 'reference_person', 'credit_limit', 'credit_term_days', 'discount',
        'bank_name', 'bank_ac_number', 'employee_id', 'branch_id', 'status', 'created_by', 'updated_by', 'deleted_by'
    ];

    protected $table = 'erp_customers';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }
}
