<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankDetailModel extends Model
{
    //
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $table = 'erp_bankdetail';


    public  function banksModel(){
        return $this->belongsTo('App\Models\Admin\BanksModel','bank_id');
    }
    public  function emplyeeModel(){
        return $this->belongsTo('App\Models\HRM\Employees','group_id');
    }
}
