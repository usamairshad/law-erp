<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsIssueModel extends Model
{
    use SoftDeletes;

    protected $fillable =[
        'sup_id',
        'branch_id',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $table = 'erp_goodsissue';

    public function products()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','productid');
    }
    public function suppliersModel()
    {
        return $this->belongsTo('App\Models\Admin\SuppliersModel','sup_id');
    }
    public function stockCategoryModel()
    {
        return $this->belongsTo('App\Models\Admin\StockCategoryModel','cat_id');
    }
    public function branches()
    {
        return $this->belongsTo('App\Models\Admin\Warehouse','branch_id');
    }
}
