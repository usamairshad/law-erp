<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PaymentSetting extends Model
{
	protected $table = 'payment_settings';
     protected $fillable = ['created_at', 'updated_at', 'identity', 'logo', 'link', 'config', 'status'];
}
