<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class BankLoan extends Model
{
     protected $fillable = ['bank_id', 'amount', 'markup', 'installment_plan', 'deduction_each_month', 'kabis', 'bank_intrest'];

    protected $table = 'erp_bank_loan';
}
