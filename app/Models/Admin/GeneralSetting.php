<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    protected $fillable = ['created_by', 'branch_id', 'last_updated_by', 'institute', 'salogan', 'address','phone','email','website', 'favicon', 'email', 'logo',
    'print_header', 'print_footer', 'facebook', 'twitter', 'linkedIn', 'youtube', 'googlePlus',
    'instagram', 'whatsApp', 'skype', 'pinterest','wordpress', 'status'];
   
}
