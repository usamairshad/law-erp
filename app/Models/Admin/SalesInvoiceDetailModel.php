<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Marketing\Databanks;
use App\Models\Marketing\SalePipelines;
use App\Models\Marketing\PipelineProducts;

class SalesInvoiceDetailModel extends Model
{

    protected $table = "erp_salesinvoice";

    protected $fillable = ['invoice_to','invoice_dept','sales_person', 'customer_id', 'invoice_no','quote_id','invoice_type' ,'invoice_date', 'due_date', 'rr_no', 'po_no','delivery_type','status','is_disc','disc_value','disc_type','disc_amount', 'total_after_disc','is_taxable','tax_percent','is_service','service_value','service_type',
        'installation_date','warrenty','expires_at','overall_discount','service_amount','misc_amount', 'total_discount','total_tax','service_tax','total_after_service_tax',
        'gross_total','net_income',
        'services_amount','products_amount','services_tax','service_tax_amount','products_tax', 'amount', 'tax_amount', 'total_amount','discount','payable_amount', 'remaining_amount', 'paid_amount','confirm_status', 'created_by', 'updated_by', 'deleted_by', 'deleted_at'];


    static public function getAllSaleKeys(){
        $databanks = SalePipelines::select('id','name')->where(['confirm_status' => 1])->get();
        $ordernos = Array();
        foreach($databanks as $databank){
            $ordernos[$databank->id] = strtoupper(substr(str_replace('-', '', str_replace(' ','',$databank->name)), 0, 4)).'-'.$databank->id;
        }
        $ordernos[''] = 'Select an Order No';
        return $ordernos;
    }

    static public function getInvoiceDate($order_id){
        return SalePipelines::where(['id' => $order_id])->get()->pluck('qoute_date')->first();
    }

    static public function getDueDate($order_id){
        return SalePipelines::where(['id' => $order_id])->where(['deleted_at' => NULL])->get()->pluck('expected_closing_date')->first();
    }

    static public function getCustomerIdByOrderId($order_id){
        return SalePipelines::where(['id' => $order_id])->where(['deleted_at' => NULL])->get()->pluck('databank_id')->first();
    }


    static public function customerTotalCredit($customer_id){
        return self::where(['customer_id' => $customer_id,'confirm_status' =>'1' ])->sum('remaining_amount');
    }
    public function CustomersModel(){

        // return $this->belongsTo('App\Models\Marketing\Databanks','customer_id');
       return $this->belongsTo('App\Models\Admin\Customers','customer_id');
    }

    public function BranchName(){
        return $this->belongsTo('App\Models\Admin\Branches','customer_id');
    }
    public function DealerName(){
        return $this->belongsTo('App\Models\Admin\DealerModel','customer_id');
    }

    public function createdBy(){
        return $this->belongsTo('App\User','created_by');
    }


    public static function engrYTDReport(  $user_ids, $quote_ids, $brand  ,$date_from, $date_to ){

        $quote_ids = count($quote_ids) > 0 ? implode(',', $quote_ids) : 0;
        $brand = isset($brand) ? $brand : 0;
        $date_to = Date('Y-m-d', strtotime("+1 days"));
        $SalePipelines  = SalesInvoiceDetailModel::selectRaw('salesinvoice.*,
        invoice_details.product_id as product_id, invoice_details.quantity product_quantity, invoice_details.unit_price unit_price, invoice_details.net_income net_income')

            ->join('invoice_details','invoice_details.salesinvoice_id','=','salesinvoice.id')
            ->join('products','products.id','=','invoice_details.product_id')

            ->where('salesinvoice.confirm_status', '1')

            ->where('products.cat_id','<>', '1')
            ->whereRaw("salesinvoice.invoice_date BETWEEN '".$date_from. "' AND '". $date_to ."' ")


            ->where(function ($query) use ($quote_ids){
                $query->whereRaw( "salesinvoice.quote_id IN ( ".
                    $quote_ids.")  OR '".
                    $quote_ids."' = 0");
            })

            ->where(function ($query) use ($brand){
                $query->whereRaw( "products.suplier_id = ".
                    $brand."  OR '".
                    $brand."' = 0");
            })

            ->OrderBy('salesinvoice.id','desc')
            ->get();
        return $SalePipelines;
    }


}
