<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetailModel extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $table = 'erp_orderdetail';

    public function order()
    {
        return $this->belongsTo('App\Models\Admin\Orders', 'perchas_id');
    }
    public function productsModel()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel', 'product_id');
    }
    public function unitsModel()
    {
        return $this->belongsTo('App\Models\Admin\UnitsModel','unit_name');
    }
    public static function getOrderQty($orderid,$productid){
        $OrderDetailModel = OrderDetailModel::selectRaw('qty')
            ->where('perchas_id', '=',$orderid)
            ->where('product_id', '=',$productid)
            ->first();
        if(count($OrderDetailModel)>0){
           $qty =  $OrderDetailModel->qty;
        }else{
            $qty =  0;
        }
        return $qty;
    }

}
