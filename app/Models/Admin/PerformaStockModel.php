<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformaStockModel extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

//    protected $fillable = ['lc_no','product_name','product_model','total_qty','rel_qty','balance_qty','amount','total_amount',
//        'amount_rs','margin','total_murahba','murahba_profit','net_murahba','cost_per_unit','balance_murahba',
//        'add_inventory','insurance','bank_charges','freight','profit_investment','mvmnt_bndg','Duty','f_total_amount',
//        'af_cp_unit','balance_lc'];

    protected $table = 'erp_performastock';

    public  function productsModel(){
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_name');
    }


}
