<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\CountryModel;
use App\Models\Admin\Regions;
use App\Models\Admin\City;
use Auth;

class Territory extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['name','country_id','city_id','region_id', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_territory';

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('name','asc')->pluck('name','id');
    }

    public static function allTerritory(){
        $Territory  = Territory::selectRaw('id,name,branch_id')
            ->where(['status' => 1])
            ->get();
        return $Territory;
    }

    static public function getTerritoryDetailById($territory_id) {
        return self::where(['status' => 1, 'id' => $territory_id])->first();
    }
    public  function country(){
        return $this->belongsTo('App\Models\Admin\CountryModel','country_id');
    }
    public  function region(){
        return $this->belongsTo('App\Models\Admin\Regions','region_id');
    }
    public  function city(){
        return $this->belongsTo('App\Models\Admin\City','city_id');
    }
}
