<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\Country;
use App\Models\Admin\Regions;
class City extends Model
{

    protected $fillable = ['name','country_id', 'created_by', 'updated_by'];

    protected $table = 'erp_city';


    public  function country(){
        return $this->belongsTo('App\Models\Admin\Country','country_id');
    }
    public function branches()
    {
        return $this->hasMany('App\Models\Admin\Branches');
    }
    public static function CityList($id = 0)
    {
        $list = '';
        $branches = self::where('status', 1)->orderBy('name', 'asc')->get(['name', 'id']);
        foreach ($branches as $branch) {
            $list .= '<option ' . (($id == $branch->id) ? 'selected' : '') . ' value="' . $branch->id . '">' . $branch->name . '</option>';
        }
        return $list;
    }
}
