<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class InventoryTransferDetailModel extends Model
{
    use SoftDeletes;

    protected $fillable =[
        'i_transfer_id',
        'product_id',
        'transfer_qty',
        'created_by',
        'updated_by',
    ];
    protected $table = 'erp_inventorytransferdetail';

    public function suppliersModel()
    {
        return $this->belongsTo('App\Models\Admin\SuppliersModel','sup_id');
    }
    public function products()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }
}
