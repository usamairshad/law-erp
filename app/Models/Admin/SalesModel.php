<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class SalesModel extends Model
{
    //sales Table
    use SoftDeletes;

    protected $fillable =
        [
            'group_id',
            'sal_no',
            'sal_date',
            'valid_upto',
            'customer_id',
            'branch_id',
            'employee_id',
            //'stock_item_id',
            //'product_id',
            'sup_id',
            'description',
            'remarks',
            'discount',
            'tax',
            'final_amount',
            'total_amount',
            'total_balance',
            'gst',
            'shipping_charges',
            'status',
            'created_by',
            'updated_by',
            'deleted_by',
            
        ];

    protected $table = 'erp_sales';

    //Get All Sale Data
    public static function AllSales(){
        $Sales  = SalesModel::selectRaw('sales.*, customers.name as cus_name')->Join('customers','sales.customer_id','=','customers.id')->get();
        return $Sales;
    }

    //Get All Sale Data
    public static function salesdetail( $id){
        $SalesDetail  = SalesModel::selectRaw('sales.*, customers.name as cus_name')
            ->where(['id' => $id])
            ->Join('customers','sales.customer_id','=','customers.id')->get();
        //dd($SalesDetail);
        return $SalesDetail;
    }

    //Get Last Sale Id
    public static function Salno(){
        $salno = '';

        $Salno = DB::table('sales')->OrderBy('id','desc')->limit(1)->get();
        return $Salno;
    }

    static public function pluckActiveOnly() {
        return self::where(['status' => 1])->OrderBy('sal_no','asc')->pluck('sal_no','id');
    }
}
