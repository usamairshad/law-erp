<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class GrBufferModel extends Model
{

    protected $table = 'erp_grbuffer';

    
    protected $fillable =[
        'c_invoice',
        'product_id',
        'rel_qty',
        'segrigate_qty',
        'warehouse_id',
        'import_date',
        'unit_price'
    ];

    public function products()
    {
        return $this->belongsTo('App\Models\Admin\ProductsModel','product_id');
    }
}
