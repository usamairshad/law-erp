<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = ['name','status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'boards';
    public function activesession()
    {
        return $this->hasMany('App\Models\Academics\ActiveSession','board_id','id');
    }
    public function branchboard()
    {
        return $this->hasMany('App\Models\Academics\BoardBranch');
    }

    public function getName()
    {
        return $this->name;
    }
}
