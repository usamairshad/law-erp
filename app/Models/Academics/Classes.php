<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $fillable = ['name', 'description', 'status', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'classes';

    public function activesession()
    {
        return $this->hasMany('App\Models\Academics\ActiveSession');
    }

}
