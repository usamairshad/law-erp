<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
 	protected $fillable = ['title', 'start_year', 'active_status','end_year', 'status','created_by', 'updated_by', 'deleted_by'];

 	protected $table = 'sessions';
}
