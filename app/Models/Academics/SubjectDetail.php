<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class SubjectDetail extends Model
{
    protected $table = 'subject_details';
     protected $fillable = ['created_by', 'last_updated_by','program_id', 'term_id', 'branch_id', 'session_id', 'title', 'code', 'full_mark_theory', 'pass_mark_theory',
        'full_mark_practical', 'pass_mark_practical', 'credit_hour', 'sub_type', 'class_type', 'staff_id',
        'description', 'status'];

    public function subject(){
        return $this->belongsTo(Subject::class);
    }

    public function semester()
    {
        return $this->belongsToMany(Semester::class);
    }
    public function staff_name(){
        return $this->belongsTo('App\Models\Academics\Staff','staff_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
    public function session()
    {
        return $this->belongsTo(Sessions::class, 'session_id');
    }
}
