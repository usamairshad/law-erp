<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class StudentEnrollment extends Model
{
     protected $table = 'student_enrollments';
    protected $fillable = [
        'branch_id','board_id','student_id','session_id','program_id','term_id','section_id','subject_id'
    ];
}
