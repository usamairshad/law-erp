<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class TeacherClass extends Model
{
    protected $table = 'teacher_classes';
    protected $fillable = ['user_id', 'class_id'];
}
