<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{

    protected $fillable = ['name','city_id','company_id','city_name','country_id','branch_code','phone','fax','cell','address', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_branches';
    public function activesession()
    {
        return $this->hasMany('App\Models\Academics\ActiveSession');
    }
    public function branchboard()
    {
        return $this->hasMany('App\Models\Academics\BoardBranch');
    }
    
}
