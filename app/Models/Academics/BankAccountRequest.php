<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class BankAccountRequest extends Model
{
    //
    protected $fillable = ['staff_id','first_name','last_name','cnic','father_name','joining_date','designation','branch','salary','form'];
    protected $table = 'bank_account_request';
}
