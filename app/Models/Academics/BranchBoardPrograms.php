<?php

namespace App\Models\Academics;

use App\Models\Admin\Branches;
use Illuminate\Database\Eloquent\Model;

class BranchBoardPrograms extends Model
{
    protected $fillable = ['branch_id', 'board_id', 'program_id'];

    protected $table = 'branch_board_program';



    public function branch()
    {
        return $this->belongsTo(Branches::class, "branch_id");
    }

    public function board()
    {
        return $this->belongsTo(Board::class, "board_id");
    }

    public function program()
    {
        return $this->belongsTo(Program::class, "program_id");
    }
}
