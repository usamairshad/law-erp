<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class AcademicsGroupCourse extends Model
{
    protected $fillable = ['academics_group_id','course_id'];
	protected $table = 'academics_group_courses';
}
