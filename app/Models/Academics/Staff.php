<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\Branches;

class Staff extends Model
{
    protected $table = 'staff';
    protected $fillable = [
        'eobi_amount','provident_amount','medical_allowances','conveyance','house_rent','EOBI','provident_fund','filer_type','ntn','created_by', 'last_updated_by', 'reg_no', 'branch_id', 'join_date', 'designation', 'first_name',  'middle_name', 'last_name','working_hours',
        'father_name', 'mother_name', 'date_of_birth', 'gender', 'blood_group', 'nationality', 'mother_tongue', 'address', 'country', 'city',
        'temp_address', 'home_phone', 'mobile_1', 'mobile_2', 'email', 'qualification',
        'experience', 'other_info', 'staff_image', 'skill', 'cnic', 'status', 'job_status', 'password', 'salary','basic_salary', 'staff_type', 'user_id', 'salary_per_hour','probation','end_day_contract','start_day_contract','account_title','account_number','bank_branch','bank_account','tax_percentage','epf','eobi'
    ];
    // public function payrollMaster()
    // {
    //     return $this->hasMany(PayrollMaster::class, 'staff_id');
    // }

    // public function paySalary()
    // {
    //     return $this->hasMany(SalaryPay::class, 'staff_id');
    // }
    static public  function employeeDetailByUserId($user_id){
        $Employees  = Staff::where(['user_id' => $user_id])
            ->first();
           // dd($Employees);
        return $Employees;
    }
    static public function pluckActiveOnly() {
        return self::OrderBy('first_name','asc')->get()->pluck('first_name','user_id');
    }
    static public function pluckActiveWithID() {
        return self::where(['status' => 1])->OrderBy('first_name','asc')->get()->pluck('first_name','user_id');
    }
    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }


    public function user()
    {
        return $this->belongsTo("App\User", 'user_id');
    }

    public function stafftransferbranch()
    {
        return $this->hasOne("App\Models\HRM\StaffTransferBranch", 'user_id', "user_id");
    }

    public function role()
    {
        return $this->hasOne("App\Models\ModelHasRoles", 'model_id', "user_id");
    }

    public function activesession()
    {
        return $this->hasMany("App\Models\Academics\ActiveSession", 'branch_id', "branch_id");
    }
    public function users()
    {
        return $this->belongsTo('App\User');
    }


}
