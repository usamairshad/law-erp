<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class ProgramTerm extends Model
{
    protected $fillable = ['program_id','term_id'];

	protected $table = 'program_terms';
}
