<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class CourseGroup extends Model
{
    protected $fillable = ['program_group_id','name','status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'course_groups';

    public function programGroup(){
        return $this->belongsTo('App\Models\Academics\ProgramGroup','program_group_id');
    }
}
