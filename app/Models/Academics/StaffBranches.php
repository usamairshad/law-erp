<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class StaffBranches extends Model
{
    //
    protected $fillable = ['staff_id','branch_id'];
    protected $table = 'staff_branches';
}
	