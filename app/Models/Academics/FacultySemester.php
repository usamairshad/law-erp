<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacultySemester extends BaseModel
{
//    use SoftDeletes;
    protected $fillable = ['branch_id','faculty_id', 'program_id', 'term_id', 'session_id', 'class_teacher', 'section_name', 'student_strength', 'slug','status','created_by','last_updated_by'];

    protected $table = 'faculty_semesters';


    public function gradingType()
    {
        return $this->hasOne(GradingType::class, 'id','grading_id');
    }
    public function classTeacher(){
        return $this->belongsTo('App\Models\Staff','class_teacher');
    }
    public function program(){
        return $this->belongsTo('App\Models\Program','program_id');
    }
    public function class(){
        return $this->belongsTo('App\Models\Faculty','faculty_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
    
    public function session()
    {
        return $this->belongsTo(Sessions::class, 'session_id');
    }

    public function getSection()
    {
        return $this->section_name;
    }
}
