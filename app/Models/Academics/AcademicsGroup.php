<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class AcademicsGroup extends Model
{
    protected $fillable = ['program_id','name'];
	protected $table = 'academics_groups';

	public function getName()
    {
    	return $this->name;
    }
    public function getProgram()
    {
    	return $this->belongsTo('App\Models\Academics\Program','program_id');
    }
}
