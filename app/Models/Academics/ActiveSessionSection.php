<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class ActiveSessionSection extends Model
{
      protected $fillable = ['active_session_id', 'section_id'];
    protected $table = 'active_session_sections';



    public function section()
    {
        return $this->belongsTo(Section::class, "section_id");
    }
    public function activesession()
    {
        return $this->belongsTo(ActiveSession::class, "active_session_id");
    }



}
