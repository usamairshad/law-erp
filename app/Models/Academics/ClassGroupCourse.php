<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class ClassGroupCourse extends Model
{
    protected $fillable = ['class_group_id','course_id'];

    protected $table = 'class_group_courses';
}
