<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = ['name','description','program','session_id','status','years','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'programs';
    public function activesession()
    {
        return $this->hasMany('App\Models\Academics\ActiveSession');
    }
}
