<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacultySubjects extends BaseModel
{

    protected $fillable = ['facultysemester_id', 'faculty_id', 'program_id', 'session_id', 'term_id','section_id','subjects','staff_id'];

    protected $table = 'faculty_subjects';

    public function staff(){
        return $this->belongsTo('App\Models\Staff','staff_id');
    }
    public function subject(){
        return $this->belongsTo('App\Models\SubjectDetail','subjects');
    }
    public function sectionName(){
        return $this->belongsTo('App\Models\FacultySemester','section_id');
    }
    public function classes(){
        return $this->belongsTo('App\Models\Faculty','faculty_id');
    }
    public function program(){
        return $this->belongsTo('App\Models\Program','program_id');
    }
    public function term(){
        return $this->belongsTo('App\Models\Term','term_id');
    }

}
