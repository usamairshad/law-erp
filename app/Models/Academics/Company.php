<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name','address','description','status', 'created_by', 'updated_by', 'deleted_by'];

    protected $table = 'erp_companies';
}
