<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;
use App\Models\Academics\Program;
use App\Models\Academics\Board;
use App\Models\Academics\BoardProgramClass;

class BPC_Term extends Model
{
    protected $fillable = ['bpc_id','term_id','updated_by','created_by'];

	protected $table = 'bpc_term';

	public function getClassaa()
    {
    	return $this->belongsTo('App\Models\Academics\Classes','class_id');
    }
    public function getPrograaam()
    {
    	return $this->belongsTo('App\Models\Academics\Program','program_id');
    }
    public function getBoaraad()
    {
    	return $this->belongsTo('App\Models\Academics\Board','board_id');
    }
    public function getBoardProgramClass()
    {
    	return $this->belongsTo('App\Models\Academics\BoardProgramClass','bpc_id');
    }
}
