<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable = ['name','status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'buildings';
}
