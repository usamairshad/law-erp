<?php

namespace App\Models\Academics;

use App\Models\Admin\Branches;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder\Class_;

class ActiveSession extends Model
{

    protected $fillable = ['title', 'session_id', 'board_id', 'branch_id', 'program_id', 'class_id', 'status'];
    protected $table = 'active_sessions';


    public  function classes()
    {
        return $this->belongsTo('App\Models\Academics\Classes', 'class_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(Branches::class, "branch_id");
    }


    public function board()
    {
        return $this->belongsTo(Board::class, "board_id");
    }


    public function program()
    {
        return $this->belongsTo(Program::class, "program_id");
    }


    public function getClass()
    {
        return $this->belongsTo(Classes::class, "class_id");
    }

    public function activesessionteacher()
    {
        return $this->hasMany(ActiveSessionTeacher::class, "active_session_id", "id");
    }


    public function session()
    {
        return $this->belongsTo(Session::class, "session_id");
    }
}
