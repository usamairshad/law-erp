<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class AssignActiveSessionCourseLab extends Model
{
    protected $fillable = ['active_session_id','course_id','description','lab_id','created_by', 'updated_by', 'deleted_by','created_at','updated_at'];

    protected $table = 'assign_active_session_course_lab';
}
