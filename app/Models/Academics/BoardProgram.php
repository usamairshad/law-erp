<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;
use App\Models\Academics\Program;
use App\Models\Academics\Board;

class BoardProgram extends Model
{
    protected $fillable = ['board_id', 'program_id',"class_id"];

    protected $table = 'board_programs';

    public function board()
    {
        return $this->belongsTo('App\Models\Academics\Board', 'board_id');
    }
    public function program()
    {
        return $this->belongsTo('App\Models\Academics\Program', 'program_id');
    }


    
}
