<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class ProgramGroup extends Model
{
    protected $fillable = ['board_program_id','name'];

    protected $table = 'program_group';

    public function boardProgram(){
        return $this->belongsTo('App\Models\Academics\BoardProgram','board_program_id');
    }
}
