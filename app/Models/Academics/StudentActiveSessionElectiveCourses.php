<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class StudentActiveSessionElectiveCourses extends Model
{
    protected $fillable = ['active_session_student_id','course_id'];

    protected $table = 'std_active_s_elecive';
}
