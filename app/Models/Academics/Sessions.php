<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
    protected $table = 'session';

    protected $fillable = ['created_by', 'last_updated_by', 'title', 'start_date', 'end_date', 'status','branch_id'];

    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }

}
