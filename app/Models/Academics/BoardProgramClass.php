<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class BoardProgramClass extends Model
{
    protected $fillable = ['board_id','program_id','class_id'];

	protected $table = 'board_program_class';

	public function getClass()
    {
    	return $this->belongsTo('App\Models\Academics\Classes','class_id');
    }
    public function getProgram()
    {
    	return $this->belongsTo('App\Models\Academics\Program','program_id');
    }
    public function getBoard()
    {
    	return $this->belongsTo('App\Models\Academics\Board','board_id');
    }
    
}
