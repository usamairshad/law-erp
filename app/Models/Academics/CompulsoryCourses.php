<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class CompulsoryCourses extends Model
{
    protected $fillable = ['program_id','course_id'];

    protected $table = 'compulsory_courses';
}
