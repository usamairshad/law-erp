<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class ClassGroup extends Model
{
     protected $fillable = ['course_group_id','class_id','name','status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'class_groups';

    public function courseGroup(){
        return $this->belongsTo('App\Models\Academics\CourseGroup','course_group_id');
    }
}
