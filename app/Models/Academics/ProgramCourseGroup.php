<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class ProgramCourseGroup extends Model
{
    protected $fillable = ['program_id','course_group_id'];

    protected $table = 'program_course_groups';
}
