<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['name','description','status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'sections';

    public function getName()
    {
    	return $this->name;
    }
}
