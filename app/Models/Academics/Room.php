<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
     protected $fillable = ['building_id','name','status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'rooms';
}
