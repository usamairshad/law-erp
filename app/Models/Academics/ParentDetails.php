<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class ParentDetails extends Model
{
    protected $fillable = ['student_id','father_first_name','father_middle_name','father_last_name','father_occupation','father_email','father_mobile_1','father_cnic', 'mother_first_name', 'mother_middle_name','mother_last_name','mother_cnic','mother_occupation','mother_mobile_1','mother_email','guardian_is','guardian_first_name', 'guardian_middle_name','guardian_last_name','guardian_occupation','guardian_address','guardian_residence_number','guardian_mobile_1','guardian_cnic','guardian_email','guardian_relation','filer_info','status','parent_image'];
    
     protected $table = 'parent_details';

    public function students()
    {
        return $this->belongsTo(Student::class, 'id');
    }}





}
