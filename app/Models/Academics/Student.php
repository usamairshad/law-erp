<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $fillable = ['created_by', 'last_updated_by','transfer_id', 'branch_id','board_id', 'program_id', 'session_id', 'term_id', 'reg_no', 'reg_date','father_cnic','mother_cnic', 'university_reg','class_id',
        'faculty','semester', 'section_name', 'academic_status', 'first_name', 'middle_name', 'last_name',
        'date_of_birth', 'gender', 'blood_group', 'nationality', 'mother_tongue', 'email', 'extra_info', 'sos_date',
        'student_image','status','proficiency','language_speak', 'parent_id','english_examination','walkin_reg_no' , 'admission_status','user_id'];


    public function board()
    {
        return $this->belongsTo(Board::class, "board_id");
    }

    public function program()
    {
        return $this->belongsTo(Program::class, "program_id");
    }

    public function getClass()
    {
        return $this->belongsTo(Classes::class, "class_id");
    }

    public function Section()
    {
        return $this->belongsTo(Section::class, "section_id");
    }
}
