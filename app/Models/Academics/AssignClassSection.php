<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class AssignClassSection extends Model
{
    protected $fillable = ['board_program_class_id','section_id','status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'assign_class_section';

    public function getBoardProgramClass()
    {
    	return $this->belongsTo('App\Models\Academics\BoardProgramClass','board_program_class_id');
    }
}
