<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class CourseGroupCourse extends Model
{
    protected $fillable = ['course_group_id','course_id'];

    protected $table = 'course_group_courses';
}
