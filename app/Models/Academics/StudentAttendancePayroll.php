<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class StudentAttendancePayroll extends Model
{
    protected $fillable =[
    	'active_session_student_id',
        'student_id',
        'class_date',
        'class_time',
        'attendance_type',
        'day',
        'attendance'
    ];
    protected $table = 'student_attendance_payroll';
}
