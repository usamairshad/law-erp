<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;
use App\Models\Academics\FacultySemester;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Faculty extends Model
{
    // use SoftDeletes;
    protected $table = 'faculties';
    protected $fillable = ['created_by','deleted_by', 'last_updated_by', 'branch_id', 'session_id', 'program', 'faculty', 'slug', 'status'];

    public function semester() {
        return $this->belongsToMany(Semester::class);
    }
    public function programs(){
        return $this->belongsTo('App\Models\Program','program');
    }
    public function sectionInfo()
    {
        return $this->hasMany(FacultySemester::class, 'faculty_id', 'id');
    }
    public function staff(){
        return $this->belongsTo('App\Models\Staff','staff_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
    public function session()
    {
        return $this->belongsTo(Sessions::class, 'session_id');
    }

    public function getName()
    {
        $name = $this->faculty;
        return $name;
    }
}
