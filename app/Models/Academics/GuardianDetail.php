<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class GuardianDetail extends Model
{
	protected $table = 'guardian_details';
    protected $fillable = ['student_id','first_name', 'middle_name', 'last_name', 'cnic', 'address', 'contact_no', 'nationality',  'tax_info', 'passport',
        'is_guardian'];
}
