<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Syllabus extends Model
{
    protected $fillable = ['program_id', 'branch_id', 'section_id', 'session_id', 'syllabus_head_id', 'subject_id','description','file', 'status', 'created_by'];

    protected $table = 'syllabi';

    public function program()
    {
        return $this->hasOne(Program::class,'id', 'program_id');
    }
    public function section()
    {
        return $this->hasOne(FacultySemester::class,'id', 'section_id');
    }
    public function subject()
    {
        return $this->belongsTo(SubjectDetail::class, 'subject_id');
    }
    public function syllabusHead()
    {
        return $this->belongsTo(syllabusHead::class, 'syllabus_head_id');
    }
    public function session()
    {
        return $this->belongsTo(Sessions::class, 'session_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branches::class, 'branch_id');
    }
}
