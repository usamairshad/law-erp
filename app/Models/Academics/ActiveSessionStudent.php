<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class ActiveSessionStudent extends Model
{
    protected $fillable = ['active_session_id','student_id','course_group_id','program_course_group_id'];
	protected $table = 'active_session_students';
    public function active_session_section(){
        return  $this->belongsTo('App\ActiveSessionSection');
    }
}
