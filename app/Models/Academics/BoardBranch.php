<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class BoardBranch extends Model
{
    protected $fillable = ['board_id', 'branch_id'];
    protected $table = 'board_branches';

    public function branch()
    {
        return $this->belongsTo('App\Models\Academics\Branch');
    }

    public function board()
    {
        return $this->belongsTo('App\Models\Academics\Board');


    }
}
