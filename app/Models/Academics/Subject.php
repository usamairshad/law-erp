<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;
use App\Models\Academics\Staff;

class Subject extends Model
{
    protected $fillable = ['id','class', 'created_by'];

  	public $table = 'subject_details';

    public function semester()
    {
        return $this->belongsToMany(Semester::class);
    }
    public function staff_name(){
        return $this->belongsTo('App\Models\Academics\Staff','staff_id');
    }

    public function getName()
    {
    	return $this->title;
    }
}
