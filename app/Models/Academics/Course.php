<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name','description','subject_code', 'credit_hours' , 'program_id', 'type' , 'actual_fee' , 'subject_fee' , 'subject_optional_code' , 'status','created_by', 'updated_by', 'deleted_by'];

    protected $table = 'courses';

    public function getName()
    {
    	return $this->name;
    }
}
