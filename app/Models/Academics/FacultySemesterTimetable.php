<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FacultySemesterTimetable extends BaseModel
{
    protected $fillable = ['branch_id','faculty_semester_id', 'subject_id', 'title', 'start', 'end', 'start_time', 'end_time', 'room', 'color','description','status','created_by','last_updated_by'];

    protected $table = 'faculty_semester_timetables';
}