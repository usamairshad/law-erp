<?php

namespace App\Models\Academics;

use App\ActiveSessionSection;

use Illuminate\Database\Eloquent\Model;

class ActiveSessionTeacher extends Model
{
    protected $fillable = ['active_session_section_id', 'staff_id', 'course_ids','rates','rate_per_lecture','days_in_month','class_timetable_id'];
    protected $table = 'active_session_teachers';




    public function activesessionsection()
    {
        return $this->belongsTo(ActiveSessionSection::class, "active_session_section_id");
    }

    public function course()
    {
        return $this->belongsTo(Course::class, "course_ids");
    }

    public function section()
    {
        return $this->belongsTo(Section::class, "section_id");
    }


}
