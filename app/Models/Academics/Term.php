<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = ['academic_year_id','start_month','start_year','end_month','end_year','name','status','created_by', 'updated_by', 'deleted_by','description'];

    protected $table = 'terms';

    public function getName()
    {
    	return $this->name;
    }
}
