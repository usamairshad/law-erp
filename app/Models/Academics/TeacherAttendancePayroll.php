<?php

namespace App\Models\Academics;

use Illuminate\Database\Eloquent\Model;

class TeacherAttendancePayroll extends Model
{
    protected $fillable =[
        'active_session_teacher_id',
        'staff_id',
        'class_Date',
        'class_time',
        'day',
        'attendance_type',
        'course_id',
        'month',
        'year',
        'branch_id'
    ];
    protected $table = 'teacher_attendance_payroll';
}
