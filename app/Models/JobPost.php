<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPost extends Model
{
    protected $fillable = ['job_post_request_id','job_title','description','location','section','education','experience','skill','branch_id','expected_joining','public_link','status','created_by','updated_by','deleted_by','deleted_at'
    ];

    protected $table = 'job_post';
    public  function jobpostrequest(){
        return $this->belongsTo('App\Models\JobPostRequest','job_post_request_id');
    }
}
