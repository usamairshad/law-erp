<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanInstallment extends Model
{
    protected $table = 'erp_loan_plans';
    protected $fillable = ['loan_installment','tenure','extra_pay'];
   
}
