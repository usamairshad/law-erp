<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recruitment_Feedback extends Model
{
    //
    protected $fillable = ['candidate_form_id','job_post_id','status','category','confidence','communication','knowledge','remarks','created_at','updated_at'];


    protected $table = 'recruitment_feedback';
}
