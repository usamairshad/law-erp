<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $table = 'erp_loans';
    protected $fillable = ['loan_type','description'];
   
}
