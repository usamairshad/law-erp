<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'erp_vendor';
    protected $fillable = ['type','category','updated_by','vendor_name','cnic','ntn','salestaxno','email','contact','addresss'];
   
}
