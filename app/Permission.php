<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	public $table = "erp_permissions";
    protected $fillable = ['group', 'name', 'display_name', 'description'];
}
