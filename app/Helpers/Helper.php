<?php

namespace App\Helpers;

use App\Models\Academics\ActiveSession;
use App\Models\Academics\ActiveSessionStudent;
use App\Models\Academics\ActiveSessionTeacher;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use App\Models\Academics\Branch;
use App\Models\Academics\Company;
use App\Models\Academics\Board;
use App\Models\Academics\Program;
use App\Models\Academics\Classes;
use App\Models\Academics\Session;
use App\Models\Academics\Staff;
use App\Models\Academics\Course;
use App\Models\Academics\Lab;
use App\Models\Admin\Country; 
use App\Models\Academics\StudentAttendancePayroll;
use App\Models\Academics\TeacherAttendancePayroll;
use App\Models\Admin\City;
use App\Models\Admin\Branches;
use App\Models\Academics\Student;

//use App\Models\Academics\ActiveSession;
use App\Models\HRM\Termination;
use App\Models\HRM\EventModel;
use App\Models\HRM\Exam;
//use App\User; 
use App\Models\HRM\TimeTable;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\Role;
use App\Roles;
use Carbon\carbon;


class Helper
{





    public static function attendance_sheet_details($branch_id = null,$board_id= null,$program_id= null, $class_id= null, $section_id= null, $start_date= "2021-03-24", $end_date= "2021-03-25", $type="student", $type_id=6)
    {
        # code...


        $from = date($start_date);
        $to = date($end_date);  
        $date=$start_date;
        if($type=="student")

        {
        
            $present_attendance=StudentAttendancePayroll::where('student_id',$type_id)
            ->where('attendance',"present")
            ->whereDate('class_Date',$date)  
            ->count();


            $absent_attendance=StudentAttendancePayroll::where('student_id',$type_id)
            ->where('attendance',"absent")
            ->whereDate('class_Date',$date) 
            ->count();
            
            $leave_attendance=StudentAttendancePayroll::where('student_id',$type_id)
            ->where('attendance',"leave")
            ->whereDate('class_Date',$date) 
            ->count();
            
            $short_leave_attendance=StudentAttendancePayroll::where('student_id',$type_id)
            ->where('attendance',"short_leave")
            ->whereDate('class_Date',$date) 
            ->count();
            
            return compact('present_attendance','absent_attendance','leave_attendance','short_leave_attendance');
        }
        if($type=="teacher")
        {

            $attendent_attendance=TeacherAttendancePayroll::where('staff_id',$type_id)
            ->where('attendance',1)
            ->whereDate('class_Date',$date) 
            ->count();

            $missed_attendance=TeacherAttendancePayroll::where('staff_id',$type_id)
            ->where('attendance',0)
            ->whereDate('class_Date',$date) 
            ->count();

            return compact('attendent_attendance','missed_attendance');

        }
        


        if($type=="principal")
        {

            

            // $users = DB::table('students')
            // ->select('students.first_name')
            // ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            // ->where('branch_id',$type_id)

            // ->where('student_attendance_payroll.attendance',"leave") 
            // ->groupBy('students.id')

            // ->count('students.id')  
            
            // ->get();


            $date=$start_date;



            $std_present_attendance = Student::selectRaw('students.first_name, count(students.id) as presents ')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$type_id)
            ->where('student_attendance_payroll.attendance',"present") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $std_absent_attendance = Student::selectRaw('students.first_name, count(students.id) as absents')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$type_id)
            ->where('student_attendance_payroll.attendance',"absent") 
            ->whereBetween('class_Date', [$from, $to])
            ->groupBy('students.id')  
            ->get();




            $std_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$type_id)
            ->where('student_attendance_payroll.attendance',"leave") 
            ->whereBetween('class_Date', [$from, $to])
            ->groupBy('students.id')  
            ->get();




            $std_short_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as short_leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$type_id)
            ->where('student_attendance_payroll.attendance',"short_leave") 
            ->whereBetween('class_Date', [$from, $to])
            ->groupBy('students.id')  
            ->get();



            $tchr_present_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as presents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->where('staff.branch_id',$type_id)
            ->where('teacher_attendance_payroll.attendance',1) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();

            

            $tchr_absent_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as absents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->where('staff.branch_id',$type_id)
            ->where('teacher_attendance_payroll.attendance',0) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();   


            return compact('std_present_attendance','std_absent_attendance','std_leave_attendance','std_short_leave_attendance','tchr_present_attendance','tchr_absent_attendance');

            

        }

        
        


        if ($branch_id != "null" && $branch_id != null && ($board_id=="null" || $board_id==null)) 
        {
            # code...


            $date=$start_date;
            $std_present_attendance = Student::selectRaw('students.first_name, count(students.id) as presents ')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('student_attendance_payroll.attendance',"present") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $std_absent_attendance = Student::selectRaw('students.first_name, count(students.id) as absents')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('student_attendance_payroll.attendance',"absent") 
            ->whereDate('class_Date',$date) 
            ->groupBy('students.id')  
            ->get();




            $std_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('student_attendance_payroll.attendance',"leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();




            $std_short_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as short_leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('student_attendance_payroll.attendance',"short_leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $tchr_present_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as presents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->where('staff.branch_id',$branch_id)
            ->where('teacher_attendance_payroll.attendance',1) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();

            

            $tchr_absent_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as absents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->where('staff.branch_id',$branch_id)
            ->where('teacher_attendance_payroll.attendance',0) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();   


            return compact('std_present_attendance','std_absent_attendance','std_leave_attendance','std_short_leave_attendance','tchr_present_attendance','tchr_absent_attendance');




        }







        if ($branch_id != "null" && $branch_id != null && $board_id !="null" && $board_id !=null && ($program_id=="null" || $program_id==null)) 
        {
            # code...
            

            $date=$start_date;
            $std_present_attendance = Student::selectRaw('students.first_name, count(students.id) as presents ')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('student_attendance_payroll.attendance',"present") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $std_absent_attendance = Student::selectRaw('students.first_name, count(students.id) as absents')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('student_attendance_payroll.attendance',"absent") 
            ->whereDate('class_Date',$date) 
            ->groupBy('students.id')  
            ->get();




            $std_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('student_attendance_payroll.attendance',"leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();




            $std_short_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as short_leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('student_attendance_payroll.attendance',"short_leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $tchr_present_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as presents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->join('active_sessions', 'staff.branch_id', '=', 'active_sessions.branch_id')
            ->where('staff.branch_id',$branch_id)
            ->where('active_sessions.board_id',$board_id)            
            ->where('teacher_attendance_payroll.attendance',1) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();

            

            $tchr_absent_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as absents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->join('active_sessions', 'staff.branch_id', '=', 'active_sessions.branch_id')
            ->where('staff.branch_id',$branch_id)
            ->where('active_sessions.board_id',$board_id) 
            ->where('teacher_attendance_payroll.attendance',0) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();   


            return compact('std_present_attendance','std_absent_attendance','std_leave_attendance','std_short_leave_attendance','tchr_present_attendance','tchr_absent_attendance');




        }










        if ($branch_id != "null" && $branch_id != null && $board_id !="null" && $board_id !=null && $program_id !="null" && $program_id !=null && ($class_id=="null" || $class_id==null)) 
        {
            # code...
            

            $date=$start_date;
            $std_present_attendance = Student::selectRaw('students.first_name, count(students.id) as presents ')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('student_attendance_payroll.attendance',"present") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $std_absent_attendance = Student::selectRaw('students.first_name, count(students.id) as absents')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('student_attendance_payroll.attendance',"absent") 
            ->whereDate('class_Date',$date) 
            ->groupBy('students.id')  
            ->get();




            $std_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('student_attendance_payroll.attendance',"leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();




            $std_short_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as short_leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('student_attendance_payroll.attendance',"short_leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $tchr_present_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as presents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->join('active_sessions', 'staff.branch_id', '=', 'active_sessions.branch_id')
            ->where('staff.branch_id',$branch_id)
            ->where('active_sessions.board_id',$board_id)
            ->where('active_sessions.program_id',$program_id)            
            ->where('teacher_attendance_payroll.attendance',1) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();

            

            $tchr_absent_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as absents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->join('active_sessions', 'staff.branch_id', '=', 'active_sessions.branch_id')
            ->where('staff.branch_id',$branch_id)
            ->where('active_sessions.board_id',$board_id) 
            ->where('active_sessions.program_id',$program_id)  
            ->where('teacher_attendance_payroll.attendance',0) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();   


            return compact('std_present_attendance','std_absent_attendance','std_leave_attendance','std_short_leave_attendance','tchr_present_attendance','tchr_absent_attendance');




        }












        if ($branch_id != "null" && $branch_id != null && $board_id !="null" && $board_id !=null && $program_id !="null" && $program_id !=null && $class_id!="null" && $class_id!=null && ($section_id=="null" || $section_id==null)) 
        {
            # code...
            

            $date=$start_date;
            $std_present_attendance = Student::selectRaw('students.first_name, count(students.id) as presents ')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('class_id',$class_id)
            ->where('student_attendance_payroll.attendance',"present") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $std_absent_attendance = Student::selectRaw('students.first_name, count(students.id) as absents')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('class_id',$class_id)
            ->where('student_attendance_payroll.attendance',"absent") 
            ->whereDate('class_Date',$date) 
            ->groupBy('students.id')  
            ->get();




            $std_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('class_id',$class_id)
            ->where('student_attendance_payroll.attendance',"leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();




            $std_short_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as short_leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('class_id',$class_id)
            ->where('student_attendance_payroll.attendance',"short_leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $tchr_present_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as presents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->join('active_sessions', 'staff.branch_id', '=', 'active_sessions.branch_id')
            ->where('staff.branch_id',$branch_id)
            ->where('active_sessions.board_id',$board_id)
            ->where('active_sessions.program_id',$program_id)
            ->where('active_sessions.class_id',$class_id)            
            ->where('teacher_attendance_payroll.attendance',1) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();

            

            $tchr_absent_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as absents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->join('active_sessions', 'staff.branch_id', '=', 'active_sessions.branch_id')
            ->where('staff.branch_id',$branch_id)
            ->where('active_sessions.board_id',$board_id) 
            ->where('active_sessions.program_id',$program_id)
            ->where('active_sessions.class_id',$class_id)  
            ->where('teacher_attendance_payroll.attendance',0) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();   


            return compact('std_present_attendance','std_absent_attendance','std_leave_attendance','std_short_leave_attendance','tchr_present_attendance','tchr_absent_attendance');




        }











        if ($branch_id != "null" && $branch_id != null && $board_id !="null" && $board_id !=null && $program_id !="null" && $program_id !=null && $class_id!="null" && $class_id!=null && $section_id !="null" && $section_id!=null) 
        {
            # code...
            

            $date=$start_date;
            $std_present_attendance = Student::selectRaw('students.first_name, count(students.id) as presents ')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('class_id',$class_id)
            ->where('section_id',$section_id)
            ->where('student_attendance_payroll.attendance',"present") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $std_absent_attendance = Student::selectRaw('students.first_name, count(students.id) as absents')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('class_id',$class_id)
            ->where('section_id',$section_id)
            ->where('student_attendance_payroll.attendance',"absent") 
            ->whereDate('class_Date',$date) 
            ->groupBy('students.id')  
            ->get();




            $std_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('class_id',$class_id)
            ->where('section_id',$section_id)
            ->where('student_attendance_payroll.attendance',"leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();




            $std_short_leave_attendance = Student::selectRaw('students.first_name, count(students.id) as short_leaves')
            ->join('student_attendance_payroll', 'students.id', '=', 'student_attendance_payroll.student_id')
            ->where('branch_id',$branch_id)
            ->where('board_id',$board_id)
            ->where('program_id',$program_id)
            ->where('class_id',$class_id)
            ->where('section_id',$section_id)
            ->where('student_attendance_payroll.attendance',"short_leave") 
            ->whereDate('class_Date',$date)  
            ->groupBy('students.id')  
            ->get();



            $tchr_present_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as presents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->join('active_sessions', 'staff.branch_id', '=', 'active_sessions.branch_id')
            ->where('staff.branch_id',$branch_id)
            ->where('active_sessions.board_id',$board_id)
            ->where('active_sessions.program_id',$program_id)
            ->where('active_sessions.class_id',$class_id)
                        
            ->where('teacher_attendance_payroll.attendance',1) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();

            

            $tchr_absent_attendance = Staff::selectRaw('staff.first_name, count(staff.id) as absents')
            ->join('teacher_attendance_payroll', 'staff.id', '=', 'teacher_attendance_payroll.staff_id')
            ->join('active_sessions', 'staff.branch_id', '=', 'active_sessions.branch_id')
            ->where('staff.branch_id',$branch_id)
            ->where('active_sessions.board_id',$board_id) 
            ->where('active_sessions.program_id',$program_id)
            ->where('active_sessions.class_id',$class_id)
              
            ->where('teacher_attendance_payroll.attendance',0) 
            ->whereDate('class_Date',$date)  
            ->groupBy('staff.id')  
            ->get();   


            return compact('std_present_attendance','std_absent_attendance','std_leave_attendance','std_short_leave_attendance','tchr_present_attendance','tchr_absent_attendance');




        }





        
    }


    //Lab helper
    
    public static function roleIdToName($role_id,$user_type)
    {
        if($user_type == 'erp')
        {
            $data=Roles::where('id',$role_id)->value('name');
            return $data;
        }
        elseif ($user_type == 'academics') {
            $data=Role::where('id',$role_id)->value('name');
            return $data;
        }
        else
        {
            return '';
        }
        
    }


    public static function FunctionName($value='')
    {
          # code...
    }







    public static function getAttendanceDetail($branch_id = null,$board_id= null,$program_id= null, $class_id= null, $section_id= null, $start_date= null, $end_date= null)
    {
        if($branch_id && $start_date && $end_date)
        {
            $get_students = Student::where('branch_id' , $branch_id)->get();


       foreach ($get_students as $key => $get_student) {
           
           $student_attendance = StudentAttendancePayroll::where('student_id' , $get_student->id)->where('class_date' , '>' , $start_date)->where('class_date' , '<' , $end_date)
           ->get();   
        }           return $student_attendance;     
        
        }elseif ($branch_id && $board_id && $start_date && $end_date) {
            $get_students = Student::where('branch_id' , $branch_id)->where('board_id' , $board_id)->get();       
       
            foreach ($get_students as $key => $get_student) {
           
            $student_attendance = StudentAttendancePayroll::where('student_id' , $get_student->id)->where('class_date' , '>' , $start_date)->where('class_date' , '<' , $end_date)
           ->get();
   
        }      return $student_attendance;   
        }elseif($branch_id && $board_id && $program_id && $start_date && $end_date){
            $get_students = Student::where('branch_id' , $branch_id)->where('board_id' , $board_id)->where('program_id' , $program_id)->get();       
       
            foreach ($get_students as $key => $get_student) {
           
            $student_attendance = StudentAttendancePayroll::where('student_id' , $get_student->id)->where('class_date' , '>' , $start_date)->where('class_date' , '<' , $end_date)
           ->get();
        } return $student_attendance;
        }
        elseif($branch_id && $board_id && $program_id && $class_id && $start_date && $end_date)
        {
            $get_students = Student::where('branch_id' , $branch_id)->where('board_id' , $board_id)->where('program_id' , $program_id)->where('class_id' , $class_id)->get();       
       
            foreach ($get_students as $key => $get_student) {
           
            $student_attendance = StudentAttendancePayroll::where('student_id' , $get_student->id)->where('class_date' , '>' , $start_date)->where('class_date' , '<' , $end_date)
           ->get();
            } return $student_attendance;
        }elseif($branch_id && $board_id && $program_id && $class_id && $section_id &&  $start_date && $end_date){
          
            $get_students = Student::where('branch_id' , $branch_id)->where('board_id' , $board_id)->where('program_id' , $program_id)->where('class_id' , $class_id)->where('section_id' , $section_id)->get();       
       
            foreach ($get_students as $key => $get_student) {
           
            $student_attendance = StudentAttendancePayroll::where('student_id' , $get_student->id)->where('class_date' , '>' , $start_date)->where('class_date' , '<' , $end_date)
           ->get(); 
             } return $student_attendance;
        }
        else
        {
            return 0;
        }
    }
  // public static function getAttendanceDetail($branch_id,$board_id,$program_id, $class_id, $section_id $start_date $end_date)
  //   {
  //       if($branch_id == 'erp')
  //       {
  //           $data=Roles::where('id',$role_id)->value('name');
  //           return $data;
  //       }
  //       elseif ($user_type == 'academics') {
  //           $data=Role::where('id',$role_id)->value('name');
  //           return $data;
  //       }
  //       else
  //       {
  //           return '';
  //       }
        
  //   } 


    public static function labIdToName($id)
    {
        $name = Lab::where('id', $id)->value('name');
        return $name;
    }
    public static function subjectIdToName($id)
    {
        $name = Course::where('id', $id)->value('name');
        return $name;
    }
    public static function activeSessionIdToBoard($id)
    {
        $b_id = ActiveSession::where('id', $id)->value('board_id');
        $name = Board::where('id', $b_id)->value('name');
        return $name;
    }
    public static function activeSessionIdToProgram($id)
    {
        $p_id = ActiveSession::where('id', $id)->value('program_id');
        $name = Program::where('id', $p_id)->value('name');
        return $name;
    }
    public static function companyIdToName($id)
    {
        $name = Company::where('id', $id)->value('name');
        return $name;
    }
    public static function branchIdToName($id)
    {
        $name = Branch::where('id', $id)->value('name');
        return $name;
    }
    public static function branchIdToCode($id)
    {
        $name = Branch::where('id', $id)->value('branch_code');
        return $name;
    }
    public static function boardIdToName($id)
    {
        $name = Board::where('id', $id)->value('name');
        return $name;
    }
    public static function programIdToName($id)
    {
        $name = Program::where('id', $id)->value('name');
        return $name;
    }
    public static function classIdToName($id)
    {
        $name = Classes::where('id', $id)->value('name');
        return $name;
    }
    public static function countryIdToName($id)
    {
        $name = Country::where('id', $id)->value('name');
        return $name;
    }
    public static function cityIdToName($id)
    {
        $name = City::where('id', $id)->value('name');
        return $name;
    }
    public static function userIdToName($id)
    {
        $name = User::where('id', $id)->value('name');
        return $name;
    }
    public static function sessionIdToName($id)
    {
        $name = Session::where('id', $id)->value('title');
        return $name;
    }

    //    public static function getStudentCount($id)
    //    {
    //        $activesessions = ActiveSession::where('class_id',$id)->groupBy('id')->get();
    //        foreach ($activesessions as $session){
    //                    $active_session_sections =DB::table('active_session_sections')
    //                    ->join('active_session_students','active_session_sections.id', '=', 'active_session_students.active_session_id')
    //                    ->select('active_session_students.id')
    //                    ->get();
    //                    dd($active_session_sections);
    //        }
    //    }

    public static function getTermination($user_id)
    {
        $obj = Termination::where('user_id', $user_id)->first();
        return $obj;
    }

    public static function getStaffIDByUserID($user_id)
    {
        $obj = Staff::where('user_id', $user_id)->first();
        return $obj;
    }
    public static function timetableIdToTitle($id)
    {
        $name = TimeTable::where('id', $id)->value('title');
        return $name;
    }

    public static function getStaffIDToFirstName($id)
    {
        $obj = Staff::where('id', $id)->value('first_name');
        return $obj;
    }
    public static function getStaffIDToRegNo($id)
    {
        $obj = Staff::where('id', $id)->value('reg_no');
        return $obj;
    }
    public static function eventIdToName($id)
    {
        $name = EventModel::where('id', $id)->value('title');
        return $name;
    }
    public static function examIdToName($id)
    {
        $name = Exam::where('id', $id)->value('title');
        return $name;
    }


    public static function checkAssignCourses($staff_id)
    {

        $assignCourses = ActiveSessionTeacher::where("staff_id", $staff_id)->get();

        if ($assignCourses->isEmpty()) {

            return true;
        }

        return false;
    }
    public static function teacherIdToName($id)
    {
        $name = User::where('id', $id)->value('name');
        return $name;
    
    }

    public static function studentIdToFirstName($id)
    {
        $name = Student::where('id', $id)->value('first_name');
        return $name;
    
    }
       public static function studentIdToLastName($id)
    {
        $name = Student::where('id', $id)->value('last_name');
        return $name;
    
    }
    public static function studentIdToBranchName($id)
    {
        $branch_id = Student::where('id', $id)->value('branch_id');
        $data = Branches::where('id' , $branch_id)->value('name');
        return $data;
    
    }
    public static function userIdToCityName($id)
    {
        $data = Staff::where('user_id', $id)->value('city');
       
        return $data;
    
    }
    public static function userIdToBranchName($id)
    {
        $branch_id = User::where('id', $id)->value('branch_id');
        $data = Branches::where('id' , $branch_id)->value('name');
        return $data;
    
    }
    public static function studentIdToBoardName($id)
    {
        $board_id = Student::where('id', $id)->value('board_id');
        $data = Board::where('id' , $board_id)->value('name');
        return $data;
    
    }
    public static function userIdToStaffRegNo($id)
    {
        $data = Staff::where('user_id', $id)->value('reg_no');
        return $data;
    
    }
    public static function studentIdToProgramName($id)
    {
        $program_id = Student::where('id', $id)->value('program_id');
        $data = Program::where('id' , $program_id)->value('name');
        return $data;
    
    }
    public static function studentIdToTermName($id)
    {
        $term_id = Student::where('id', $id)->value('term_id');
        $data = Term::where('id' , $term_id)->value('name');
        return $name;
    
    }
    public static function studentIdToAcademicGroup($id)
    {
        $academic_group_id  = Student::where('id', $id)->value('academic_group_id');
        $data = AcademicsGroup::where('id' , $academic_group_id)->value('name');
        return $data;
    
    }
    public static function studentIdToClassName($id)
    {
        $class_id = Student::where('id', $id)->value('class_id');
        $data = Classes::where('id' , $class_id)->value('name');
        return $data;
    
    }
    public static function studentIdToSection($id)
    {
        $class_id = Student::where('id', $id)->value('class_id');
        $data = Classes::where('id' , $class_id)->value('name');
        return $data;
    
    }
      public static function userIdToRole($id)
    {
        $role_id  = User::where('id', $id)->value('role_id');
        $data = Roles::where('id' , $role_id)->value('name');
        return $data;
    
    }

      public static function regNoToStaffId($id)
    {
      $data = Staff::where('reg_no' , $id)->value('id');

      return $data;

}

      public static function rolesIdToName($id)
    {
        $data = Roles::where('id' , $id)->value('name');
        return $data;

    
    }

      public static function vendorIdToName($id)
    {
        $data = DB::table('erp_payee_lists')->where('id' , $id)->value('payee_name');
        return $data;

    
    }
}

