<?php

namespace App;

use App\Models\LandManagement\Building;
use App\Models\LandManagement\PayeeList;
use Illuminate\Database\Eloquent\Model;

class Payees_amount extends Model
{
    protected $table = 'erp_payees_amounts';
    protected $fillable = ['payee_id','building_id','payee_amount'];
    function payees(){
        return $this->belongsTo(PayeeList::class, 'payee_id','id');

    }
    function building(){
        return $this->belongsTo(Building::class, 'building_id','id');

    }

}

