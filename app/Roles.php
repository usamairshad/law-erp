<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends \Spatie\Permission\Models\Role
{
    protected $table = 'erp_roles';
    protected $fillable = ['name', 'display_name', 'description'];
    public static function roleList($id = 0)
    {
        $list = '';
        $roles = self::orderBy('name', 'asc')->get(['name', 'id']);
        foreach ($roles as $role) {
            $list .= '<option ' . (($id == $role->id) ? 'selected' : '') . ' value="' . $role->id . '">' . $role->name . '</option>';
        }
        return $list;
    }
//    public function users(){
//        return $this->hasMany('App\User','role_id');
//    }
    public function staff_relation()
    {
        return $this->hasMany('App\Models\Academics\Staff','user_id','id');
    }
    public function user()
    {
        return $this->hasMany('App\User');
    }
}

