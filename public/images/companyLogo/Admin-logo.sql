-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2021 at 07:40 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erpnetroots_rootsschool`
--

-- --------------------------------------------------------

--
-- Table structure for table `academics_groups`
--

CREATE TABLE `academics_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `academics_groups`
--

INSERT INTO `academics_groups` (`id`, `program_id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 5, 'GROUP  1', NULL, '2021-02-26 09:58:19', '2021-02-26 09:58:19'),
(2, 5, 'GROUP 2', NULL, '2021-02-26 09:58:19', '2021-02-26 09:58:19'),
(3, 1, 'EYP  GROUP', NULL, '2021-03-01 09:48:04', '2021-03-01 09:48:04'),
(4, 2, 'PYP GROUP', NULL, '2021-03-01 09:48:04', '2021-03-01 09:48:04'),
(5, 3, 'IGCSE - 1', NULL, '2021-03-02 06:15:09', '2021-03-02 06:15:09'),
(6, 3, 'IGCSE  2', NULL, '2021-03-02 06:15:27', '2021-03-02 06:15:27'),
(7, 3, 'IGCSE 3', NULL, '2021-03-02 06:15:44', '2021-03-02 06:15:44'),
(8, 3, 'IGCSE 4', NULL, '2021-03-02 06:15:56', '2021-03-02 06:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `academics_group_courses`
--

CREATE TABLE `academics_group_courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `academics_group_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `academics_group_courses`
--

INSERT INTO `academics_group_courses` (`id`, `academics_group_id`, `course_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, '2021-02-26 09:58:59', '2021-02-26 09:58:59'),
(2, 1, 2, NULL, '2021-02-26 09:58:59', '2021-02-26 09:58:59'),
(3, 1, 3, NULL, '2021-02-26 09:58:59', '2021-02-26 09:58:59'),
(4, 1, 10, NULL, '2021-02-26 09:58:59', '2021-02-26 09:58:59'),
(5, 1, 22, NULL, '2021-02-26 09:58:59', '2021-02-26 09:58:59'),
(6, 1, 23, NULL, '2021-02-26 09:58:59', '2021-02-26 09:58:59'),
(7, 2, 3, NULL, '2021-02-26 10:00:15', '2021-02-26 10:00:15'),
(8, 2, 16, NULL, '2021-02-26 10:00:15', '2021-02-26 10:00:15'),
(9, 2, 22, NULL, '2021-02-26 10:00:15', '2021-02-26 10:00:15'),
(10, 2, 23, NULL, '2021-02-26 10:00:15', '2021-02-26 10:00:15'),
(11, 2, 24, NULL, '2021-02-26 10:00:15', '2021-02-26 10:00:15'),
(12, 3, 2, NULL, '2021-03-01 09:48:20', '2021-03-01 09:48:20'),
(13, 3, 3, NULL, '2021-03-01 09:48:20', '2021-03-01 09:48:20'),
(14, 3, 4, NULL, '2021-03-01 09:48:20', '2021-03-01 09:48:20'),
(15, 3, 5, NULL, '2021-03-01 09:48:20', '2021-03-01 09:48:20'),
(16, 3, 11, NULL, '2021-03-01 09:48:20', '2021-03-01 09:48:20'),
(17, 4, 7, NULL, '2021-03-01 09:48:48', '2021-03-01 09:48:48'),
(18, 4, 8, NULL, '2021-03-01 09:48:48', '2021-03-01 09:48:48'),
(19, 4, 9, NULL, '2021-03-01 09:48:48', '2021-03-01 09:48:48'),
(20, 4, 10, NULL, '2021-03-01 09:48:48', '2021-03-01 09:48:48'),
(21, 4, 11, NULL, '2021-03-01 09:48:48', '2021-03-01 09:48:48'),
(22, 5, 2, NULL, '2021-03-02 06:17:05', '2021-03-02 06:17:05'),
(23, 5, 3, NULL, '2021-03-02 06:17:05', '2021-03-02 06:17:05'),
(24, 5, 10, NULL, '2021-03-02 06:17:05', '2021-03-02 06:17:05'),
(25, 5, 22, NULL, '2021-03-02 06:17:05', '2021-03-02 06:17:05'),
(26, 5, 23, NULL, '2021-03-02 06:17:05', '2021-03-02 06:17:05'),
(27, 5, 24, NULL, '2021-03-02 06:17:05', '2021-03-02 06:17:05');

-- --------------------------------------------------------

--
-- Table structure for table `active_sessions`
--

CREATE TABLE `active_sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `board_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `active_sessions`
--

INSERT INTO `active_sessions` (`id`, `title`, `session_id`, `board_id`, `branch_id`, `program_id`, `class_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2021-2022-IB-EYP-1', 1, 1, 1, 1, 1, 'Active', NULL, NULL, NULL),
(2, '2021-2022-IB-EYP-2', 1, 1, 3, 1, 2, 'Active', NULL, NULL, NULL),
(3, '2021-2022-IB-EYP-3', 1, 1, 3, 1, 3, 'Active', NULL, NULL, NULL),
(4, '2021-2022-IB-PYP-1', 1, 1, 3, 2, 4, 'Active', NULL, NULL, NULL),
(5, '2021-2022-IB-PYP-2', 1, 1, 3, 2, 5, 'Active', NULL, NULL, NULL),
(6, '2021-2022-IB-PYP-3', 1, 1, 3, 2, 6, 'Active', NULL, NULL, NULL),
(7, '2021-2022-IB-PYP-4', 1, 1, 3, 2, 7, 'Active', NULL, NULL, NULL),
(8, '2021-2022-IB-PYP-5', 1, 1, 3, 2, 8, 'Active', NULL, NULL, NULL),
(9, '2021-2022-IB-PYP-6', 1, 1, 3, 2, 9, 'Active', NULL, NULL, NULL),
(10, '2021-2022-IB-IGCSE-1', 1, 1, 3, 3, 10, 'Active', NULL, NULL, NULL),
(11, '2021-2022-IB-IGCSE-2', 1, 1, 3, 3, 11, 'Active', NULL, NULL, NULL),
(12, '2021-2023-IB-IGCSE-3', 2, 1, 3, 3, 12, 'Active', NULL, NULL, NULL),
(13, '2021-2023-IB-IGCSE-4', 2, 1, 3, 3, 13, 'Active', NULL, NULL, NULL),
(14, '2021-2023-IB-AS ', 2, 1, 3, 5, 14, 'Active', NULL, NULL, NULL),
(15, '2021-2023-IB-A2 ', 2, 1, 3, 5, 15, 'Active', NULL, NULL, NULL),
(16, '2021-2022-IB-EYP-1', 1, 1, 4, 1, 1, 'Active', NULL, NULL, NULL),
(17, '2021-2022-IB-EYP-2', 1, 1, 4, 1, 2, 'Active', NULL, NULL, NULL),
(18, '2021-2022-IB-EYP-3', 1, 1, 4, 1, 3, 'Active', NULL, NULL, NULL),
(19, '2021-2022-IB-PYP-1', 1, 1, 4, 2, 4, 'Active', NULL, NULL, NULL),
(20, '2021-2022-IB-PYP-2', 1, 1, 4, 2, 5, 'Active', NULL, NULL, NULL),
(21, '2021-2022-IB-PYP-3', 1, 1, 4, 2, 6, 'Active', NULL, NULL, NULL),
(22, '2021-2022-IB-PYP-4', 1, 1, 4, 2, 7, 'Active', NULL, NULL, NULL),
(23, '2021-2022-IB-PYP-5', 1, 1, 4, 2, 8, 'Active', NULL, NULL, NULL),
(24, '2021-2022-IB-PYP-6', 1, 1, 4, 2, 9, 'Active', NULL, NULL, NULL),
(25, '2021-2022-IB-IGCSE-1', 1, 1, 4, 3, 10, 'Active', NULL, NULL, NULL),
(26, '2021-2022-IB-IGCSE-2', 1, 1, 4, 3, 11, 'Active', NULL, NULL, NULL),
(27, '2021-2023-IB-IGCSE-3', 2, 1, 4, 3, 12, 'Active', NULL, NULL, NULL),
(28, '2021-2023-IB-IGCSE-4', 2, 1, 4, 3, 13, 'Active', NULL, NULL, NULL),
(29, '2021-2023-IB-AS ', 2, 1, 4, 5, 14, 'Active', NULL, NULL, NULL),
(30, '2021-2023-IB-A2 ', 2, 1, 4, 5, 15, 'Active', NULL, NULL, NULL),
(31, '2021-2022-IB-EYP-1', 1, 1, 9, 1, 1, 'Active', NULL, NULL, NULL),
(32, '2021-2022-IB-EYP-2', 1, 1, 9, 1, 2, 'Active', NULL, NULL, NULL),
(33, '2021-2022-IB-EYP-3', 1, 1, 9, 1, 3, 'Active', NULL, NULL, NULL),
(34, '2021-2022-IB-PYP-1', 1, 1, 9, 2, 4, 'Active', NULL, NULL, NULL),
(35, '2021-2022-IB-PYP-2', 1, 1, 9, 2, 5, 'Active', NULL, NULL, NULL),
(36, '2021-2022-IB-PYP-3', 1, 1, 9, 2, 6, 'Active', NULL, NULL, NULL),
(37, '2021-2022-IB-PYP-4', 1, 1, 9, 2, 7, 'Active', NULL, NULL, NULL),
(38, '2021-2022-IB-PYP-5', 1, 1, 9, 2, 8, 'Active', NULL, NULL, NULL),
(39, '2021-2022-IB-PYP-6', 1, 1, 9, 2, 9, 'Active', NULL, NULL, NULL),
(40, '2021-2022-IB-IGCSE-1', 1, 1, 9, 3, 10, 'Active', NULL, NULL, NULL),
(41, '2021-2022-IB-IGCSE-2', 1, 1, 9, 3, 11, 'Active', NULL, NULL, NULL),
(42, '2021-2023-IB-IGCSE-3', 2, 1, 9, 3, 12, 'Active', NULL, NULL, NULL),
(43, '2021-2023-IB-IGCSE-4', 2, 1, 9, 3, 13, 'Active', NULL, NULL, NULL),
(44, '2021-2023-IB-AS ', 2, 1, 9, 5, 14, 'Active', NULL, NULL, NULL),
(45, '2021-2023-IB-A2 ', 2, 1, 9, 5, 15, 'Active', NULL, NULL, NULL),
(46, '2021-2023-IB-IGCSE-3', 2, 1, 9, 7, 12, 'Active', NULL, NULL, NULL),
(47, '2021-2023-IB-IGCSE-4', 2, 1, 9, 7, 13, 'Active', NULL, NULL, NULL),
(48, '2021-2023-IB-AS ', 2, 1, 9, 7, 14, 'Active', NULL, NULL, NULL),
(49, '2021-2023-IB-A2 ', 2, 1, 9, 7, 15, 'Active', NULL, NULL, NULL),
(50, '2021-2022- SessionINTERNATIONAL BACCALAUREATEO LEVELAS  LEVEL', 1, 1, 1, 4, 14, 'Active', NULL, '2021-03-01 07:15:39', '2021-03-01 07:15:39');

-- --------------------------------------------------------

--
-- Table structure for table `active_session_sections`
--

CREATE TABLE `active_session_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `active_session_id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `active_session_sections`
--

INSERT INTO `active_session_sections` (`id`, `active_session_id`, `section_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 16, 2, NULL, NULL),
(3, 31, 3, NULL, NULL),
(4, 31, 3, NULL, NULL),
(5, 2, 1, NULL, NULL),
(6, 17, 2, NULL, NULL),
(7, 32, 3, NULL, NULL),
(8, 32, 3, NULL, NULL),
(9, 3, 1, NULL, NULL),
(10, 18, 2, NULL, NULL),
(11, 18, 3, NULL, NULL),
(12, 33, 1, NULL, NULL),
(13, 4, 1, NULL, NULL),
(14, 19, 2, NULL, NULL),
(15, 34, 1, NULL, NULL),
(16, 5, 1, NULL, NULL),
(17, 20, 2, NULL, NULL),
(18, 35, 3, NULL, NULL),
(19, 35, 2, NULL, NULL),
(20, 6, 1, NULL, NULL),
(21, 21, 2, NULL, NULL),
(22, 36, 4, NULL, NULL),
(23, 7, 2, NULL, NULL),
(24, 22, 1, NULL, NULL),
(25, 37, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `active_session_students`
--

CREATE TABLE `active_session_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `active_session_section_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `academics_group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `active_session_students`
--

INSERT INTO `active_session_students` (`id`, `active_session_section_id`, `student_id`, `academics_group_id`, `created_at`, `updated_at`) VALUES
(1, 1, 7, 1, '2021-02-28 11:19:32', '2021-02-28 11:19:32'),
(2, 1, 11, 1, '2021-03-01 05:13:11', '2021-03-01 05:13:11'),
(3, 1, 14, 1, '2021-03-01 05:45:16', '2021-03-01 05:45:16'),
(4, 1, 15, 1, '2021-03-01 05:53:22', '2021-03-01 05:53:22'),
(5, 1, 17, 1, '2021-03-01 06:19:06', '2021-03-01 06:19:06'),
(6, 1, 18, 3, '2021-03-01 09:55:44', '2021-03-01 09:55:44'),
(7, 1, 19, 3, '2021-03-01 10:39:44', '2021-03-01 10:39:44'),
(8, 1, 20, 3, '2021-03-01 11:11:31', '2021-03-01 11:11:31'),
(9, 1, 21, 1, '2021-03-01 11:30:37', '2021-03-01 11:30:37'),
(10, 1, 23, 3, '2021-03-02 06:52:09', '2021-03-02 06:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `active_session_teachers`
--

CREATE TABLE `active_session_teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `active_session_section_id` int(10) UNSIGNED NOT NULL,
  `staff_id` int(10) UNSIGNED NOT NULL,
  `course_ids` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `active_session_teachers`
--

INSERT INTO `active_session_teachers` (`id`, `active_session_section_id`, `staff_id`, `course_ids`, `created_at`, `updated_at`) VALUES
(2, 1, 10, '1', '2021-03-01 09:14:52', '2021-03-01 09:14:52'),
(3, 1, 12, '1', '2021-03-02 10:06:03', '2021-03-02 10:06:03'),
(4, 1, 12, '2', '2021-03-02 10:06:03', '2021-03-02 10:06:03'),
(5, 1, 12, '3', '2021-03-02 10:06:03', '2021-03-02 10:06:03'),
(6, 1, 12, '4', '2021-03-02 10:06:03', '2021-03-02 10:06:03'),
(7, 1, 12, '5', '2021-03-02 10:06:03', '2021-03-02 10:06:03'),
(8, 1, 12, '6', '2021-03-02 10:06:03', '2021-03-02 10:06:03'),
(9, 1, 12, '10', '2021-03-02 10:06:03', '2021-03-02 10:06:03'),
(10, 1, 12, '11', '2021-03-02 10:06:03', '2021-03-02 10:06:03'),
(16, 5, 13, '1', '2021-03-02 12:21:43', '2021-03-02 12:21:43'),
(17, 5, 13, '2', '2021-03-02 12:21:43', '2021-03-02 12:21:43'),
(18, 5, 13, '4', '2021-03-02 12:21:43', '2021-03-02 12:21:43'),
(19, 5, 13, '5', '2021-03-02 12:21:43', '2021-03-02 12:21:43'),
(20, 5, 15, '1', '2021-03-02 12:25:52', '2021-03-02 12:25:52'),
(21, 5, 15, '2', '2021-03-02 12:25:52', '2021-03-02 12:25:52'),
(22, 5, 15, '4', '2021-03-02 12:25:52', '2021-03-02 12:25:52'),
(23, 5, 15, '6', '2021-03-02 12:25:52', '2021-03-02 12:25:52'),
(24, 9, 15, '8', '2021-03-02 12:31:33', '2021-03-02 12:31:33'),
(25, 9, 15, '9', '2021-03-02 12:31:33', '2021-03-02 12:31:33');

-- --------------------------------------------------------

--
-- Table structure for table `agendas`
--

CREATE TABLE `agendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agendas`
--

INSERT INTO `agendas` (`id`, `title`, `subject`, `description`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Examination   Meeting', 'Timetable  for pyp  class  should be   at  9 am', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'pending', '2021-02-25 09:06:31', '2021-02-25 09:06:31');

-- --------------------------------------------------------

--
-- Table structure for table `approve_agendas`
--

CREATE TABLE `approve_agendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_campus` int(11) NOT NULL,
  `agenda_request_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset_type_id` int(10) UNSIGNED NOT NULL,
  `value` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warranty` int(11) NOT NULL,
  `expire_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `status` enum('Active','InActive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assign_active_session_course_lab`
--

CREATE TABLE `assign_active_session_course_lab` (
  `id` int(10) UNSIGNED NOT NULL,
  `active_session_id` int(10) UNSIGNED DEFAULT NULL,
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `lab_id` int(10) UNSIGNED DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assign_active_session_course_lab`
--

INSERT INTO `assign_active_session_course_lab` (`id`, `active_session_id`, `course_id`, `lab_id`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 23, 1, 'Lab  Class  will be      9: 30 in Lab  Classes.', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 07:15:43', '2021-02-25 07:15:43'),
(2, NULL, 23, 1, NULL, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:50:05', '2021-02-26 07:50:05'),
(3, 1, 6, 1, 'demo', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 13:25:07', '2021-02-26 13:25:07'),
(4, 4, 10, 1, 'demo', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 13:26:46', '2021-02-26 13:26:46'),
(5, 12, 23, 1, 'Class  will be  at 9:00 am', 'Active', NULL, NULL, NULL, NULL, '2021-02-28 00:02:20', '2021-02-28 00:02:20'),
(6, 10, 24, 3, 'class  will be  9:30', 'Active', NULL, NULL, NULL, NULL, '2021-03-01 07:06:48', '2021-03-01 07:06:48');

-- --------------------------------------------------------

--
-- Table structure for table `assign_class_section`
--

CREATE TABLE `assign_class_section` (
  `id` int(10) UNSIGNED NOT NULL,
  `board_program_class_id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assign_class_section`
--

INSERT INTO `assign_class_section` (`id`, `board_program_class_id`, `section_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, '2021-02-26 07:43:46', '2021-02-26 07:43:46'),
(2, 1, 2, NULL, '2021-02-26 07:44:00', '2021-02-26 07:44:00'),
(3, 1, 3, NULL, '2021-02-26 07:44:14', '2021-02-26 07:44:14'),
(4, 4, 1, NULL, '2021-02-26 07:44:30', '2021-02-26 07:44:30'),
(5, 4, 1, NULL, '2021-02-26 07:45:14', '2021-02-26 07:45:14'),
(6, 5, 1, NULL, '2021-02-26 07:46:28', '2021-02-26 07:46:28'),
(7, 24, 1, NULL, '2021-02-26 07:46:54', '2021-02-26 07:46:54'),
(8, 30, 1, NULL, '2021-02-26 07:47:10', '2021-02-26 07:47:10');

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `attendees_type` int(11) NOT NULL,
  `link_id` int(10) UNSIGNED NOT NULL,
  `years_id` int(10) UNSIGNED NOT NULL,
  `months_id` int(10) UNSIGNED NOT NULL,
  `day_1` int(11) NOT NULL DEFAULT 0,
  `day_2` int(11) NOT NULL DEFAULT 0,
  `day_3` int(11) NOT NULL DEFAULT 0,
  `day_4` int(11) NOT NULL DEFAULT 0,
  `day_5` int(11) NOT NULL DEFAULT 0,
  `day_6` int(11) NOT NULL DEFAULT 0,
  `day_7` int(11) NOT NULL DEFAULT 0,
  `day_8` int(11) NOT NULL DEFAULT 0,
  `day_9` int(11) NOT NULL DEFAULT 0,
  `day_10` int(11) NOT NULL DEFAULT 0,
  `day_11` int(11) NOT NULL DEFAULT 0,
  `day_12` int(11) NOT NULL DEFAULT 0,
  `day_13` int(11) NOT NULL DEFAULT 0,
  `day_14` int(11) NOT NULL DEFAULT 0,
  `day_15` int(11) NOT NULL DEFAULT 0,
  `day_16` int(11) NOT NULL DEFAULT 0,
  `day_17` int(11) NOT NULL DEFAULT 0,
  `day_18` int(11) NOT NULL DEFAULT 0,
  `day_19` int(11) NOT NULL DEFAULT 0,
  `day_20` int(11) NOT NULL DEFAULT 0,
  `day_21` int(11) NOT NULL DEFAULT 0,
  `day_22` int(11) NOT NULL DEFAULT 0,
  `day_23` int(11) NOT NULL DEFAULT 0,
  `day_24` int(11) NOT NULL DEFAULT 0,
  `day_25` int(11) NOT NULL DEFAULT 0,
  `day_26` int(11) NOT NULL DEFAULT 0,
  `day_27` int(11) NOT NULL DEFAULT 0,
  `day_28` int(11) NOT NULL DEFAULT 0,
  `day_29` int(11) NOT NULL DEFAULT 0,
  `day_30` int(11) NOT NULL DEFAULT 0,
  `day_31` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendance_masters`
--

CREATE TABLE `attendance_masters` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `year` int(10) UNSIGNED NOT NULL,
  `month` int(10) UNSIGNED NOT NULL,
  `day_in_month` int(11) NOT NULL,
  `holiday` int(11) NOT NULL,
  `open` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendance_statuses`
--

CREATE TABLE `attendance_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_class` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boards`
--

INSERT INTO `boards` (`id`, `name`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'INTERNATIONAL BACCALAUREATE', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:15:34', '2021-02-26 07:15:34'),
(2, 'NON IB', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:16:24', '2021-02-26 07:16:24'),
(3, 'LAHORE  BOARD', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:17:17', '2021-02-26 07:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `board_branches`
--

CREATE TABLE `board_branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `board_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `board_programs`
--

CREATE TABLE `board_programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `board_id` int(10) UNSIGNED NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_programs`
--

INSERT INTO `board_programs` (`id`, `board_id`, `program_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, '2021-02-26 07:15:34', '2021-02-26 07:15:34'),
(2, 1, 2, NULL, '2021-02-26 07:15:34', '2021-02-26 07:15:34'),
(3, 1, 3, NULL, '2021-02-26 07:15:34', '2021-02-26 07:15:34'),
(4, 1, 4, NULL, '2021-02-26 07:15:34', '2021-02-26 07:15:34'),
(5, 1, 5, NULL, '2021-02-26 07:15:34', '2021-02-26 07:15:34'),
(6, 2, 6, NULL, '2021-02-26 07:16:24', '2021-02-26 07:16:24'),
(7, 3, 7, NULL, '2021-02-26 07:17:18', '2021-02-26 07:17:18'),
(8, 3, 8, NULL, '2021-02-26 07:17:18', '2021-02-26 07:17:18'),
(9, 3, 9, NULL, '2021-02-26 07:17:18', '2021-02-26 07:17:18'),
(10, 3, 10, NULL, '2021-02-26 07:17:18', '2021-02-26 07:17:18');

-- --------------------------------------------------------

--
-- Table structure for table `board_program_class`
--

CREATE TABLE `board_program_class` (
  `id` int(10) UNSIGNED NOT NULL,
  `board_id` int(10) UNSIGNED NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_program_class`
--

INSERT INTO `board_program_class` (`id`, `board_id`, `program_id`, `class_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, '2021-02-26 07:31:34', '2021-02-26 07:31:34'),
(2, 1, 1, 2, NULL, '2021-02-26 07:31:34', '2021-02-26 07:31:34'),
(3, 1, 1, 3, NULL, '2021-02-26 07:31:34', '2021-02-26 07:31:34'),
(4, 1, 2, 4, NULL, '2021-02-26 07:31:58', '2021-02-26 07:31:58'),
(5, 1, 2, 5, NULL, '2021-02-26 07:31:58', '2021-02-26 07:31:58'),
(6, 1, 2, 6, NULL, '2021-02-26 07:31:58', '2021-02-26 07:31:58'),
(7, 1, 2, 7, NULL, '2021-02-26 07:31:58', '2021-02-26 07:31:58'),
(8, 1, 2, 8, NULL, '2021-02-26 07:31:58', '2021-02-26 07:31:58'),
(9, 1, 2, 9, NULL, '2021-02-26 07:31:58', '2021-02-26 07:31:58'),
(10, 3, 7, 24, NULL, '2021-02-26 07:32:55', '2021-02-26 07:32:55'),
(11, 3, 7, 25, NULL, '2021-02-26 07:32:55', '2021-02-26 07:32:55'),
(12, 3, 7, 26, NULL, '2021-02-26 07:32:55', '2021-02-26 07:32:55'),
(13, 3, 7, 27, NULL, '2021-02-26 07:32:55', '2021-02-26 07:32:55'),
(14, 3, 7, 28, NULL, '2021-02-26 07:32:55', '2021-02-26 07:32:55'),
(15, 3, 7, 29, NULL, '2021-02-26 07:32:55', '2021-02-26 07:32:55'),
(16, 3, 9, 16, NULL, '2021-02-26 07:33:36', '2021-02-26 07:33:36'),
(17, 3, 9, 17, NULL, '2021-02-26 07:33:36', '2021-02-26 07:33:36'),
(18, 3, 10, 18, NULL, '2021-02-26 07:34:15', '2021-02-26 07:34:15'),
(19, 3, 10, 19, NULL, '2021-02-26 07:34:15', '2021-02-26 07:34:15'),
(20, 3, 10, 20, NULL, '2021-02-26 07:34:15', '2021-02-26 07:34:15'),
(21, 3, 10, 21, NULL, '2021-02-26 07:34:15', '2021-02-26 07:34:15'),
(22, 3, 10, 22, NULL, '2021-02-26 07:34:15', '2021-02-26 07:34:15'),
(23, 3, 10, 23, NULL, '2021-02-26 07:34:15', '2021-02-26 07:34:15'),
(24, 1, 3, 10, NULL, '2021-02-26 07:37:55', '2021-02-26 07:37:55'),
(25, 1, 3, 11, NULL, '2021-02-26 07:37:55', '2021-02-26 07:37:55'),
(26, 1, 3, 12, NULL, '2021-02-26 07:37:55', '2021-02-26 07:37:55'),
(27, 1, 3, 13, NULL, '2021-02-26 07:37:55', '2021-02-26 07:37:55'),
(28, 1, 5, 14, NULL, '2021-02-26 07:39:00', '2021-02-26 07:39:00'),
(29, 1, 5, 15, NULL, '2021-02-26 07:39:00', '2021-02-26 07:39:00'),
(30, 1, 4, 12, NULL, '2021-02-26 07:42:25', '2021-02-26 07:42:25'),
(31, 1, 4, 13, NULL, '2021-02-26 07:42:25', '2021-02-26 07:42:25'),
(32, 2, 6, 32, NULL, '2021-02-26 10:31:47', '2021-02-26 10:31:47'),
(33, 2, 6, 33, NULL, '2021-02-26 10:31:47', '2021-02-26 10:31:47'),
(34, 2, 6, 34, NULL, '2021-02-26 10:31:47', '2021-02-26 10:31:47');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `book_masters_id` int(10) UNSIGNED NOT NULL,
  `book_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_status` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `created_at`, `updated_at`, `created_by`, `last_updated_by`, `book_masters_id`, `book_code`, `book_status`) VALUES
(1, '2021-02-24 10:06:12', '2021-03-01 11:48:27', 1, NULL, 1, '0011', 1),
(2, '2021-02-24 10:06:12', '2021-02-24 10:06:12', 1, NULL, 1, '0012', 1),
(3, '2021-02-24 10:06:12', '2021-02-24 10:06:12', 1, NULL, 1, '0013', 1),
(4, '2021-02-24 10:06:12', '2021-02-24 10:06:12', 1, NULL, 1, '0014', 1),
(7, '2021-03-02 07:14:03', '2021-03-02 07:17:05', 1, NULL, 3, '34561', 1),
(8, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '34562', 1),
(9, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '34563', 1),
(10, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '34564', 1),
(11, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '34565', 1),
(12, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '34566', 1),
(13, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '34567', 1),
(14, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '34568', 1),
(15, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '34569', 1),
(16, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345610', 1),
(17, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345611', 1),
(18, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345612', 1),
(19, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345613', 1),
(20, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345614', 1),
(21, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345615', 1),
(22, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345616', 1),
(23, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345617', 1),
(24, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345618', 1),
(25, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345619', 1),
(26, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345620', 1),
(27, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345621', 1),
(28, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345622', 1),
(29, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345623', 1),
(30, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345624', 1),
(31, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345625', 1),
(32, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345626', 1),
(33, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345627', 1),
(34, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345628', 1),
(35, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345629', 1),
(36, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL, 3, '345630', 1),
(47, '2021-03-02 07:26:33', '2021-03-02 07:34:05', 1, NULL, 4, '125111', 2),
(48, '2021-03-02 07:26:33', '2021-03-02 07:26:33', 1, NULL, 4, '125112', 1),
(49, '2021-03-02 07:26:33', '2021-03-02 07:26:33', 1, NULL, 4, '125113', 1),
(50, '2021-03-02 07:26:33', '2021-03-02 07:26:33', 1, NULL, 4, '125114', 1),
(51, '2021-03-02 07:26:33', '2021-03-02 07:26:33', 1, NULL, 4, '125115', 1);

-- --------------------------------------------------------

--
-- Table structure for table `book_categories`
--

CREATE TABLE `book_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_categories`
--

INSERT INTO `book_categories` (`id`, `title`, `slug`, `status`, `created_at`, `updated_at`, `created_by`, `last_updated_by`) VALUES
(3, 'PAKISTAN  HISTORY', 'PAKISTAN-HISTORY', 1, '2021-03-02 07:08:18', '2021-03-02 07:08:18', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_issues`
--

CREATE TABLE `book_issues` (
  `id` int(10) UNSIGNED NOT NULL,
  `member_id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `issued_on` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `return_date` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_issues`
--

INSERT INTO `book_issues` (`id`, `member_id`, `book_id`, `issued_on`, `due_date`, `return_date`, `status`, `created_at`, `updated_at`, `created_by`, `last_updated_by`) VALUES
(1, 13, 1, '2021-03-01 16:48:06', '2021-03-06 16:48:06', '2021-03-01 16:48:27', 1, '2021-03-01 11:48:06', '2021-03-01 11:48:27', 1, NULL),
(2, 20, 7, '2021-03-02 12:16:46', '2021-03-07 12:16:46', '2021-03-02 12:17:05', 1, '2021-03-02 07:16:46', '2021-03-02 07:17:05', 1, NULL),
(3, 8, 47, '2021-03-02 12:34:05', '2021-03-07 12:34:05', NULL, 1, '2021-03-02 07:34:05', '2021-03-02 07:34:05', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_masters`
--

CREATE TABLE `book_masters` (
  `id` int(10) UNSIGNED NOT NULL,
  `isbn_number` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editor` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edition` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edition_year` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_year` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `series` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rack_location` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_pages` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_masters`
--

INSERT INTO `book_masters` (`id`, `isbn_number`, `code`, `title`, `sub_title`, `image`, `language`, `editor`, `categories`, `edition`, `edition_year`, `publisher`, `publish_year`, `series`, `author`, `rack_location`, `price`, `total_pages`, `source`, `notes`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, '1234567890', '001', 'oliver twist', NULL, '4643.jpg', 'English', NULL, '2', '7th', '2018', 'Oxford', '2018', NULL, 'charles dikens', '25', '150', '994', NULL, NULL, 1, '2021-02-24 10:06:12', '2021-02-24 10:06:12', 1, NULL),
(3, '9781538142042', '3456', 'The Battle for Pakistan', 'The Bitter US Friendship and a Tough Neighbourhood', '7532.jpg', 'ENGLISH', NULL, '3', '2ND', '2018', 'Rowman & Littlefield', '2019', 'NIL', 'Shuja Nawaz', '1-1', '30', '428', 'INTERNET', 'NIL', 1, '2021-03-02 07:14:03', '2021-03-02 07:14:03', 1, NULL),
(4, '9781983847547', '1251', 'The Art Of Thinking In Systems', 'Improve Your Logic', '7605.jpg', 'ENGLISH', NULL, '0', 'NEW', '2018', 'Createspace Independent Publishing Platform', '2019', 'web series', 'Steven Schuster', '1-8', '30', '180', 'Internet', 'NIL', 1, '2021-03-02 07:26:33', '2021-03-02 07:26:33', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book_status`
--

CREATE TABLE `book_status` (
  `id` int(11) NOT NULL,
  `title` varchar(191) NOT NULL,
  `display_class` varchar(191) DEFAULT NULL,
  `status` varchar(191) DEFAULT NULL,
  `created_by` timestamp NULL DEFAULT NULL,
  `last_updated_by` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_status`
--

INSERT INTO `book_status` (`id`, `title`, `display_class`, `status`, `created_by`, `last_updated_by`) VALUES
(1, 'available', 'demo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branch_board_program`
--

CREATE TABLE `branch_board_program` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `board_id` int(10) UNSIGNED NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branch_board_program`
--

INSERT INTO `branch_board_program` (`id`, `branch_id`, `board_id`, `program_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 11, 1, 1, NULL, '2021-02-26 10:04:51', '2021-02-26 10:04:51'),
(2, 11, 1, 2, NULL, '2021-02-26 10:04:51', '2021-02-26 10:04:51'),
(3, 11, 1, 3, NULL, '2021-02-26 10:04:51', '2021-02-26 10:04:51'),
(4, 11, 1, 4, NULL, '2021-02-26 10:04:51', '2021-02-26 10:04:51'),
(5, 11, 1, 5, NULL, '2021-02-26 10:04:51', '2021-02-26 10:04:51'),
(6, 11, 2, 6, NULL, '2021-02-26 10:05:14', '2021-02-26 10:05:14'),
(7, 12, 1, 1, NULL, '2021-02-26 10:05:36', '2021-02-26 10:05:36'),
(8, 12, 1, 2, NULL, '2021-02-26 10:05:36', '2021-02-26 10:05:36'),
(9, 12, 1, 3, NULL, '2021-02-26 10:05:36', '2021-02-26 10:05:36'),
(10, 12, 1, 4, NULL, '2021-02-26 10:05:36', '2021-02-26 10:05:36'),
(11, 12, 1, 5, NULL, '2021-02-26 10:05:36', '2021-02-26 10:05:36'),
(12, 13, 1, 1, NULL, '2021-02-26 10:06:01', '2021-02-26 10:06:01'),
(13, 13, 1, 2, NULL, '2021-02-26 10:06:01', '2021-02-26 10:06:01'),
(14, 13, 1, 3, NULL, '2021-02-26 10:06:01', '2021-02-26 10:06:01'),
(15, 13, 1, 4, NULL, '2021-02-26 10:06:01', '2021-02-26 10:06:01'),
(16, 13, 1, 5, NULL, '2021-02-26 10:06:01', '2021-02-26 10:06:01'),
(17, 14, 1, 1, NULL, '2021-02-26 10:06:26', '2021-02-26 10:06:26'),
(18, 14, 1, 2, NULL, '2021-02-26 10:06:26', '2021-02-26 10:06:26'),
(19, 14, 1, 3, NULL, '2021-02-26 10:06:26', '2021-02-26 10:06:26'),
(20, 14, 1, 4, NULL, '2021-02-26 10:06:26', '2021-02-26 10:06:26'),
(21, 14, 1, 5, NULL, '2021-02-26 10:06:26', '2021-02-26 10:06:26'),
(22, 15, 1, 1, NULL, '2021-02-26 10:07:47', '2021-02-26 10:07:47'),
(23, 15, 1, 2, NULL, '2021-02-26 10:07:47', '2021-02-26 10:07:47'),
(24, 15, 1, 3, NULL, '2021-02-26 10:07:47', '2021-02-26 10:07:47'),
(25, 15, 1, 4, NULL, '2021-02-26 10:07:47', '2021-02-26 10:07:47'),
(26, 15, 1, 5, NULL, '2021-02-26 10:07:47', '2021-02-26 10:07:47'),
(27, 16, 1, 1, NULL, '2021-02-26 10:08:03', '2021-02-26 10:08:03'),
(28, 16, 1, 2, NULL, '2021-02-26 10:08:03', '2021-02-26 10:08:03'),
(29, 16, 1, 3, NULL, '2021-02-26 10:08:03', '2021-02-26 10:08:03'),
(30, 16, 1, 4, NULL, '2021-02-26 10:08:03', '2021-02-26 10:08:03'),
(31, 16, 1, 5, NULL, '2021-02-26 10:08:03', '2021-02-26 10:08:03'),
(32, 1, 1, 1, NULL, '2021-02-26 10:08:35', '2021-02-26 10:08:35'),
(33, 1, 1, 2, NULL, '2021-02-26 10:08:35', '2021-02-26 10:08:35'),
(34, 1, 1, 3, NULL, '2021-02-26 10:08:35', '2021-02-26 10:08:35'),
(35, 1, 1, 4, NULL, '2021-02-26 10:08:35', '2021-02-26 10:08:35'),
(36, 1, 1, 5, NULL, '2021-02-26 10:08:35', '2021-02-26 10:08:35'),
(37, 2, 1, 1, NULL, '2021-02-26 10:08:55', '2021-02-26 10:08:55'),
(38, 2, 1, 2, NULL, '2021-02-26 10:08:55', '2021-02-26 10:08:55'),
(39, 2, 1, 3, NULL, '2021-02-26 10:08:55', '2021-02-26 10:08:55'),
(40, 2, 1, 4, NULL, '2021-02-26 10:08:55', '2021-02-26 10:08:55'),
(41, 2, 1, 5, NULL, '2021-02-26 10:08:55', '2021-02-26 10:08:55'),
(42, 3, 1, 1, NULL, '2021-02-26 10:09:20', '2021-02-26 10:09:20'),
(43, 3, 1, 2, NULL, '2021-02-26 10:09:20', '2021-02-26 10:09:20'),
(44, 3, 1, 3, NULL, '2021-02-26 10:09:20', '2021-02-26 10:09:20'),
(45, 3, 1, 4, NULL, '2021-02-26 10:09:20', '2021-02-26 10:09:20'),
(46, 3, 1, 5, NULL, '2021-02-26 10:09:20', '2021-02-26 10:09:20'),
(47, 3, 2, 6, NULL, '2021-02-26 10:09:28', '2021-02-26 10:09:28'),
(48, 4, 1, 1, NULL, '2021-02-26 10:09:55', '2021-02-26 10:09:55'),
(49, 4, 1, 2, NULL, '2021-02-26 10:09:55', '2021-02-26 10:09:55'),
(50, 4, 1, 3, NULL, '2021-02-26 10:09:55', '2021-02-26 10:09:55'),
(51, 4, 1, 4, NULL, '2021-02-26 10:09:55', '2021-02-26 10:09:55'),
(52, 4, 1, 5, NULL, '2021-02-26 10:09:55', '2021-02-26 10:09:55'),
(53, 4, 2, 6, NULL, '2021-02-26 10:10:08', '2021-02-26 10:10:08'),
(54, 5, 1, 1, NULL, '2021-02-26 10:10:36', '2021-02-26 10:10:36'),
(55, 5, 1, 2, NULL, '2021-02-26 10:10:36', '2021-02-26 10:10:36'),
(56, 5, 1, 3, NULL, '2021-02-26 10:10:36', '2021-02-26 10:10:36'),
(57, 5, 1, 4, NULL, '2021-02-26 10:10:36', '2021-02-26 10:10:36'),
(58, 5, 1, 5, NULL, '2021-02-26 10:10:36', '2021-02-26 10:10:36'),
(59, 5, 2, 6, NULL, '2021-02-26 10:10:47', '2021-02-26 10:10:47'),
(60, 6, 1, 1, NULL, '2021-02-26 10:11:16', '2021-02-26 10:11:16'),
(61, 6, 1, 2, NULL, '2021-02-26 10:11:16', '2021-02-26 10:11:16'),
(62, 6, 1, 3, NULL, '2021-02-26 10:11:16', '2021-02-26 10:11:16'),
(63, 6, 1, 4, NULL, '2021-02-26 10:11:16', '2021-02-26 10:11:16'),
(64, 6, 1, 5, NULL, '2021-02-26 10:11:16', '2021-02-26 10:11:16'),
(65, 7, 1, 1, NULL, '2021-02-26 10:14:21', '2021-02-26 10:14:21'),
(66, 7, 1, 2, NULL, '2021-02-26 10:14:21', '2021-02-26 10:14:21'),
(67, 7, 1, 3, NULL, '2021-02-26 10:14:21', '2021-02-26 10:14:21'),
(68, 7, 1, 4, NULL, '2021-02-26 10:14:21', '2021-02-26 10:14:21'),
(69, 7, 1, 5, NULL, '2021-02-26 10:14:21', '2021-02-26 10:14:21'),
(70, 8, 1, 1, NULL, '2021-02-26 10:15:02', '2021-02-26 10:15:02'),
(71, 8, 1, 2, NULL, '2021-02-26 10:15:02', '2021-02-26 10:15:02'),
(72, 8, 1, 3, NULL, '2021-02-26 10:15:02', '2021-02-26 10:15:02'),
(73, 8, 1, 4, NULL, '2021-02-26 10:15:02', '2021-02-26 10:15:02'),
(74, 8, 1, 5, NULL, '2021-02-26 10:15:02', '2021-02-26 10:15:02'),
(75, 8, 1, 1, NULL, '2021-02-26 10:15:21', '2021-02-26 10:15:21'),
(76, 8, 1, 2, NULL, '2021-02-26 10:15:21', '2021-02-26 10:15:21'),
(77, 8, 1, 3, NULL, '2021-02-26 10:15:21', '2021-02-26 10:15:21'),
(78, 8, 1, 4, NULL, '2021-02-26 10:15:21', '2021-02-26 10:15:21'),
(79, 8, 1, 5, NULL, '2021-02-26 10:15:21', '2021-02-26 10:15:21'),
(80, 9, 1, 1, NULL, '2021-02-26 10:15:51', '2021-02-26 10:15:51'),
(81, 9, 1, 2, NULL, '2021-02-26 10:15:51', '2021-02-26 10:15:51'),
(82, 9, 1, 3, NULL, '2021-02-26 10:15:51', '2021-02-26 10:15:51'),
(83, 9, 1, 4, NULL, '2021-02-26 10:15:51', '2021-02-26 10:15:51'),
(84, 9, 1, 5, NULL, '2021-02-26 10:15:51', '2021-02-26 10:15:51'),
(85, 10, 1, 1, NULL, '2021-02-26 10:16:08', '2021-02-26 10:16:08'),
(86, 10, 1, 2, NULL, '2021-02-26 10:16:08', '2021-02-26 10:16:08'),
(87, 10, 1, 3, NULL, '2021-02-26 10:16:08', '2021-02-26 10:16:08'),
(88, 10, 1, 4, NULL, '2021-02-26 10:16:08', '2021-02-26 10:16:08'),
(89, 10, 1, 5, NULL, '2021-02-26 10:16:08', '2021-02-26 10:16:08'),
(90, 4, 1, 1, NULL, '2021-02-26 10:10:08', '2021-02-26 10:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `candidate_form`
--

CREATE TABLE `candidate_form` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_post_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expected_salary` int(11) NOT NULL,
  `cv` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_letter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domicile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_size_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `degree` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awards` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommendation_letter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience_letter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expected_joining` date NOT NULL,
  `status` enum('Pending','Accepted','Rejected','Selected','Interview-Taken','Not-Hired','Acknowledged') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidate_form`
--

INSERT INTO `candidate_form` (`id`, `job_post_id`, `name`, `education`, `experience`, `gender`, `expected_salary`, `cv`, `cover_letter`, `phone`, `email`, `cnic`, `domicile`, `passport`, `passport_size_photo`, `degree`, `awards`, `recommendation_letter`, `experience_letter`, `user_id`, `expected_joining`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Ayiza Ahmed', 'MS  Computer Science', '5 Years', 'female', 50000, '/public/images/uploadDocuments/Ayiza Ahmed.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '03234141653', 'ayiza001@gmail.com', 'Ayiza Ahmed-cnic.jpg', 'Ayiza Ahmed-domicile.jpg', 'Ayiza Ahmed-passport.jpg', 'Ayiza Ahmed-passport_size_photo.jpg', 'Ayiza Ahmed-degree.jpg', 'Ayiza Ahmed-awards.JPG', 'Ayiza Ahmed-recommendation_letter.jpg', 'Ayiza Ahmed-experience_letter.jpg', '15', '2021-02-25', 'Acknowledged', NULL, NULL, NULL, '2021-02-23 10:45:06', '2021-02-23 10:51:52', NULL),
(2, 2, 'M  Aslam', 'Master in  English literature', '5  Years', 'male', 50000, '/public/images/uploadDocuments/M  Aslam.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '03424243243243', 'aslam@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '18', '2021-02-26', 'Selected', NULL, NULL, NULL, '2021-02-25 10:00:22', '2021-02-25 10:01:48', NULL),
(3, 1, 'demo', 'BS', '3', 'male', 34444, '/public/images/uploadDocuments/demo.csv', 'asdasdas', '4444444', 'hr_head@rootsivy.edu.pkee', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '39', '2021-02-16', 'Selected', NULL, NULL, NULL, '2021-02-28 10:22:19', '2021-02-28 10:23:29', NULL),
(4, 3, 'Fizza', 'MS  Mathematics', '5 Years', 'female', 50000, '/public/images/uploadDocuments/Fizza.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '03234242651', 'fizza@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '71', '2021-03-02', 'Selected', NULL, NULL, NULL, '2021-03-01 07:03:55', '2021-03-01 07:04:58', NULL),
(5, 4, 'AHMED KHAN', 'MS  MATHEMATICS', '5 YEARS', 'female', 50000, '/public/images/uploadDocuments/AHMED KHAN.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '02435234152', 'ahmed1999@gmail.com', 'AHMED KHAN-cnic.jpg', 'AHMED KHAN-domicile.jpg', 'AHMED KHAN-passport.jpg', 'AHMED KHAN-passport_size_photo.jpg', 'AHMED KHAN-degree.jpg', 'AHMED KHAN-awards.jpg', 'AHMED KHAN-recommendation_letter.jpg', 'AHMED KHAN-experience_letter.jpg', '73', '2021-03-01', 'Acknowledged', NULL, NULL, NULL, '2021-03-01 09:20:16', '2021-03-01 09:24:46', NULL),
(6, 5, 'Zarwah khan', 'MS  Mathematics', '5 Years', 'female', 500000, '/public/images/uploadDocuments/Zarwah khan.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '03234242657', 'zarwah188@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '74', '2021-03-03', 'Selected', NULL, NULL, NULL, '2021-03-01 09:32:03', '2021-03-01 09:33:06', NULL),
(7, 1, 'Moiza Ahmed Khan', 'MS  Computer Science', '5 Years', 'female', 50000, '/public/images/uploadDocuments/Moiza Ahmed Khan.jpg', 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and ', '03234242666', 'moizaahmed@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '98', '2021-03-17', 'Pending', NULL, NULL, NULL, '2021-03-03 06:11:36', '2021-03-03 06:11:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `name`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'EY 1', 'EYP', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:19:36'),
(2, 'EY 2', 'EYP', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:19:36'),
(3, 'EY 3', 'EYP', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:26:08'),
(4, 'PYP 1', '1-6', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:19:36'),
(5, 'PYP 2', '1-6', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:19:36'),
(6, 'PYP 3', '1-6', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:19:36'),
(7, 'PYP 4', '1-6', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:19:36'),
(8, 'PYP  5', '1-6', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:19:36'),
(9, 'PYP 6', '1-6', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:19:36', '2021-02-26 07:19:36'),
(10, 'IGCSE 1', '6-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:20:51', '2021-02-26 07:20:51'),
(11, 'IGCSE  2', '6-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:20:51', '2021-02-26 07:20:51'),
(12, 'IGCSE   3', '6- 8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:20:51', '2021-02-26 07:20:51'),
(13, 'IGCSE   4', '6-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:20:51', '2021-02-26 07:20:51'),
(14, 'AS  LEVEL', '11-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:22:27', '2021-02-26 07:22:27'),
(15, 'A2  LEVEL', '11-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:22:27', '2021-02-26 07:22:27'),
(16, '9TH', '9-10', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:25:35', '2021-02-26 07:25:35'),
(17, '10TH', '9-10', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:25:35', '2021-02-26 07:25:35'),
(18, 'FSC -1  Med', '11-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:25:35', '2021-02-26 07:25:35'),
(19, 'FSC -2  Med', '11-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:25:35', '2021-02-26 07:25:35'),
(20, 'ICS -1', '11-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:25:35', '2021-02-26 07:25:35'),
(21, 'ICS-2', '11-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:25:35', '2021-02-26 07:25:35'),
(22, 'FSC -1 ENG', '11-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:25:35', '2021-02-26 07:25:35'),
(23, 'FSC -2   ENG', '11-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:25:35', '2021-02-26 07:25:35'),
(24, 'GRADE    1', '1- 8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:28:23', '2021-02-26 07:28:23'),
(25, 'GRADE    2', '1-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:28:23', '2021-02-26 07:28:23'),
(26, 'GRADE  3', '1-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:28:23', '2021-02-26 07:29:49'),
(27, 'GRADE    4', '1-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:28:23', '2021-02-26 07:28:23'),
(28, 'GRADE    5', '1-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:28:23', '2021-02-26 07:28:23'),
(29, 'GRADE    6', '1-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:28:23', '2021-02-26 07:28:23'),
(30, 'GRADE    7', '1-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:28:23', '2021-02-26 07:28:23'),
(31, 'GRADE    8', '1-8', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:28:23', '2021-02-26 07:28:23'),
(32, 'PLAY  GROUP', '0-1', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 10:30:27', '2021-02-26 10:30:27'),
(33, 'JR   MONTESSORI', '1-2', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 10:30:27', '2021-02-26 10:30:27'),
(34, 'ADV  MONTESSORI', '1-3', 'Active', NULL, NULL, NULL, NULL, '2021-02-26 10:30:27', '2021-02-26 10:30:27');

-- --------------------------------------------------------

--
-- Table structure for table `class_time_tables`
--

CREATE TABLE `class_time_tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `time_table_id` int(10) UNSIGNED NOT NULL,
  `active_session_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `member_type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` int(10) UNSIGNED NOT NULL,
  `behavior` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `performance` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_activity` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `branch_id`, `member_type`, `member_id`, `behavior`, `performance`, `other_activity`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 4, 'student', 9, 'CVCJVHJDFH', 'SDSJDHJKSA', 'SDJSAHDJKSA', 1, 1, NULL, '2021-03-01 11:46:23', '2021-03-01 11:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `compulsory_courses`
--

CREATE TABLE `compulsory_courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `compulsory_courses`
--

INSERT INTO `compulsory_courses` (`id`, `program_id`, `course_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, '2021-02-25 12:13:03', '2021-02-25 12:13:03'),
(2, 1, 2, NULL, '2021-02-25 12:13:03', '2021-02-25 12:13:03'),
(3, 1, 3, NULL, '2021-02-25 12:13:03', '2021-02-25 12:13:03'),
(4, 1, 10, NULL, '2021-02-25 12:13:03', '2021-02-25 12:13:03'),
(5, 1, 22, NULL, '2021-02-25 12:13:03', '2021-02-25 12:13:03'),
(6, 1, 23, NULL, '2021-02-25 12:13:03', '2021-02-25 12:13:03'),
(7, 2, 1, NULL, '2021-02-25 12:13:46', '2021-02-25 12:13:46'),
(8, 2, 2, NULL, '2021-02-25 12:13:46', '2021-02-25 12:13:46'),
(9, 2, 3, NULL, '2021-02-25 12:13:46', '2021-02-25 12:13:46'),
(10, 2, 10, NULL, '2021-02-25 12:13:46', '2021-02-25 12:13:46'),
(11, 3, 1, NULL, '2021-02-25 12:14:44', '2021-02-25 12:14:44'),
(12, 3, 2, NULL, '2021-02-25 12:14:44', '2021-02-25 12:14:44'),
(13, 3, 3, NULL, '2021-02-25 12:14:44', '2021-02-25 12:14:44'),
(14, 3, 10, NULL, '2021-02-25 12:14:44', '2021-02-25 12:14:44'),
(15, 4, 1, NULL, '2021-02-25 12:15:28', '2021-02-25 12:15:28'),
(16, 4, 2, NULL, '2021-02-25 12:15:28', '2021-02-25 12:15:28'),
(17, 4, 3, NULL, '2021-02-25 12:15:28', '2021-02-25 12:15:28'),
(18, 4, 16, NULL, '2021-02-25 12:15:28', '2021-02-25 12:15:28'),
(19, 5, 1, NULL, '2021-02-25 12:16:25', '2021-02-25 12:16:25'),
(20, 5, 2, NULL, '2021-02-25 12:16:25', '2021-02-25 12:16:25'),
(21, 5, 3, NULL, '2021-02-25 12:16:25', '2021-02-25 12:16:25'),
(22, 5, 10, NULL, '2021-02-25 12:16:25', '2021-02-25 12:16:25'),
(23, 6, 1, NULL, '2021-02-25 12:16:54', '2021-02-25 12:16:54'),
(24, 6, 2, NULL, '2021-02-25 12:16:54', '2021-02-25 12:16:54'),
(25, 6, 3, NULL, '2021-02-25 12:16:54', '2021-02-25 12:16:54'),
(26, 6, 20, NULL, '2021-02-25 12:16:54', '2021-02-25 12:16:54'),
(27, 7, 1, NULL, '2021-02-25 12:17:23', '2021-02-25 12:17:23'),
(28, 7, 2, NULL, '2021-02-25 12:17:23', '2021-02-25 12:17:23'),
(29, 7, 3, NULL, '2021-02-25 12:17:23', '2021-02-25 12:17:23'),
(30, 7, 10, NULL, '2021-02-25 12:17:23', '2021-02-25 12:17:23'),
(31, 8, 1, NULL, '2021-02-25 12:17:46', '2021-02-25 12:17:46'),
(32, 8, 2, NULL, '2021-02-25 12:17:46', '2021-02-25 12:17:46'),
(33, 8, 3, NULL, '2021-02-25 12:17:46', '2021-02-25 12:17:46'),
(34, 8, 10, NULL, '2021-02-25 12:17:46', '2021-02-25 12:17:46'),
(35, 9, 1, NULL, '2021-02-26 10:18:59', '2021-02-26 10:18:59'),
(36, 9, 2, NULL, '2021-02-26 10:18:59', '2021-02-26 10:18:59'),
(37, 9, 3, NULL, '2021-02-26 10:18:59', '2021-02-26 10:18:59'),
(38, 9, 10, NULL, '2021-02-26 10:18:59', '2021-02-26 10:18:59'),
(39, 10, 1, NULL, '2021-02-26 10:20:29', '2021-02-26 10:20:29'),
(40, 10, 2, NULL, '2021-02-26 10:20:29', '2021-02-26 10:20:29'),
(41, 10, 3, NULL, '2021-02-26 10:20:29', '2021-02-26 10:20:29'),
(42, 10, 10, NULL, '2021-02-26 10:20:29', '2021-02-26 10:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Mathematics', 'EYP 1-3', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:41:58', '2021-02-25 05:49:54'),
(2, 'English', 'EYP 1-3', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:42:21', '2021-02-25 05:50:41'),
(3, 'Urdu', 'EYP 1-3', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:42:34', '2021-02-25 05:51:36'),
(4, 'General Knowledge (Knowledge of World)', 'EYP 1-3', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:43:15', '2021-02-25 05:50:17'),
(5, 'Expressive Art & Design', 'EYP 1-3', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:44:26', '2021-02-25 05:50:06'),
(6, 'Islamiyat (Oral)', 'EYP 1-3', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:44:26', '2021-02-25 05:50:27'),
(7, 'Mathematics', 'PYP 1-5', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:49:32', '2021-02-25 05:49:32'),
(8, 'English', 'PYP 1-5', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:49:32', '2021-02-25 05:49:32'),
(9, 'Urdu', 'PYP 1-5', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:49:32', '2021-02-25 05:49:32'),
(10, 'Islamiyat', 'PYP 1-5', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:49:32', '2021-02-25 05:49:32'),
(11, 'UOI (Science + Social Studies)', 'PYP 1-5', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:49:32', '2021-02-25 05:49:32'),
(12, 'ICT', 'PYP 1-5', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:49:32', '2021-02-25 05:49:32'),
(13, 'English', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(14, 'Mathematics', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(15, 'Urdu', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(16, 'Islamiyat', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(17, 'Geography', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(18, 'History', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(19, 'ICT', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(20, 'Science', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(21, 'ART', 'PYP 6', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:53:54', '2021-02-25 05:53:54'),
(22, 'Chemistry', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(23, 'Physics', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(24, 'Biology', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(25, 'Accounting', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(26, 'Business', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(27, 'Economics', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(28, 'Urdu', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(29, 'Mathematics', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(30, 'English Literature', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(31, 'English Language', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(32, 'Sociology', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(33, 'Psychology', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(34, 'Computer Science', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(35, 'World History', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(36, 'Art & design', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(37, 'Media Studies', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16'),
(38, 'Law', 'O & A level', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 05:59:16', '2021-02-25 05:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `curriculum`
--

CREATE TABLE `curriculum` (
  `id` int(11) NOT NULL,
  `title` varchar(191) NOT NULL,
  `syllabus_id` int(11) DEFAULT NULL,
  `term_id` int(11) DEFAULT NULL,
  `description` int(191) DEFAULT NULL,
  `file` varchar(191) DEFAULT NULL,
  `status` varchar(191) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `curriculum`
--

INSERT INTO `curriculum` (`id`, `title`, `syllabus_id`, `term_id`, `description`, `file`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 'sadasd', 1, 1, 0, 'C:\\xampp\\tmp\\php4898.tmp', NULL, 12, NULL, '2021-02-16 14:24:20', '2021-02-16 14:24:20'),
(2, 'erp_recruitment_manage', 3, 1, 0, '4806.sql', NULL, 11, NULL, '2021-02-17 10:12:38', '2021-02-17 10:12:38'),
(3, 'EXAM  SYLLABUS', 10, 1, 0, '8542.jpeg', NULL, 1, NULL, '2021-02-27 23:37:53', '2021-02-27 23:37:53'),
(4, 'MID  TERM   SYLLABUS', 11, 1, 0, '9526.jpeg', NULL, 1, NULL, '2021-02-28 18:29:31', '2021-02-28 18:29:31'),
(5, 'Supply', 13, 2, 0, '6006.csv', NULL, 1, NULL, '2021-03-01 06:12:18', '2021-03-01 06:12:18');

-- --------------------------------------------------------

--
-- Table structure for table `discount_types`
--

CREATE TABLE `discount_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` int(11) NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount_types`
--

INSERT INTO `discount_types` (`id`, `name`, `description`, `percentage`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`) VALUES
(1, 'Sports base', 'Person having background of sports alongwith certificates and medals can apply', 25, 'Active', 10, NULL, NULL, '2021-02-23 06:32:17', '2021-02-23 06:32:17'),
(2, 'Sibling  Discount', 'Kinship', 50, 'Active', 10, NULL, NULL, '2021-02-24 05:28:32', '2021-02-24 05:28:32');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `member_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `branch_id`, `member_type`, `member_id`, `title`, `file`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'Coordinator', 17, 'Resume', 'Roots Logo Final.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 1, 17, NULL, NULL, '2021-02-25 10:02:33', '2021-02-25 10:02:33'),
(4, 1, 'Coordinator', 19, 'Resume', 'public/images/uploadDocuments/1614256461.jpeg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 1, 19, NULL, NULL, '2021-02-25 12:34:21', '2021-02-25 12:34:21'),
(5, 2, 'Teacher', 21, 'Resume', 'public/images/uploadDocuments/1614342986.jpeg', 'NIL', 1, 21, NULL, NULL, '2021-02-26 12:36:26', '2021-02-26 12:36:26'),
(6, 1, 'Teacher', 91, 'Resume', 'public/images/uploadDocuments/1614678802.jpeg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 1, 91, NULL, NULL, '2021-03-02 09:53:22', '2021-03-02 09:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `program_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `active_session_id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `created_by`, `last_updated_by`, `branch_id`, `program_id`, `class_id`, `section_id`, `active_session_id`, `title`, `description`, `file`, `status`, `created_at`, `updated_at`) VALUES
(14, 1, NULL, 4, 1, 1, 1, 1, 'Active Session Title', 'Des', '9564_active-session-title.docx', 0, '2021-02-16 09:04:37', '2021-02-16 09:04:37'),
(15, 1, NULL, 4, 1, 1, 1, 1, 'Zain Doc', 'Des', '6709_zain-doc.docx', 0, '2021-02-16 09:36:49', '2021-02-16 09:36:49'),
(16, 1, NULL, 4, 1, 1, 1, 1, 'Active Session Title3', 'desssss', '6250_active-session-title3.docx', 0, '2021-02-16 11:04:15', '2021-02-16 11:04:15'),
(17, 1, NULL, 4, 1, 1, 1, 1, 'usman', 'Des', '5695_usman.docx', 0, '2021-02-16 12:05:19', '2021-02-16 12:05:19'),
(18, 1, NULL, 4, 1, 1, 1, 16, 'QUIZ SYLLABUS', 'Nil', '5558_quiz-syllabus.jpg', 0, '2021-02-26 12:54:20', '2021-02-26 12:54:20'),
(19, 1, NULL, 4, 1, 1, 1, 16, 'Assignment  Testing', 'nil', '5911_assignment-testing.jpg', 0, '2021-02-26 12:58:24', '2021-02-26 12:58:24'),
(20, 1, NULL, 4, 1, 1, 1, 16, 'test001', 'Des', '7461_test001.docx', 0, '2021-02-28 11:42:11', '2021-02-28 11:42:11'),
(21, 1, NULL, 4, 1, 1, 1, 16, 'CLASS  COURSE', NULL, '8961_class-course.jpg', 0, '2021-03-01 11:40:52', '2021-03-01 11:40:52');

-- --------------------------------------------------------

--
-- Table structure for table `erp_academic_year`
--

CREATE TABLE `erp_academic_year` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_academic_year`
--

INSERT INTO `erp_academic_year` (`id`, `session_id`, `name`, `start_month`, `end_month`, `start_year`, `end_year`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'S-21-22-Y-21-22', 'August', 'December', '2021', '2022', 1, 10, 10, NULL, '2021-02-25 07:25:50', '2021-02-26 10:00:26', NULL),
(2, 2, 'S-21-23-Y-21-22', 'August', 'June', '2021', '2022', 1, 10, 10, NULL, '2021-02-25 07:35:34', '2021-02-25 07:35:34', NULL),
(3, 2, 'S-21-23-Y-22-23', 'August', 'June', '2022', '2023', 1, 10, 10, NULL, '2021-02-25 07:36:55', '2021-02-25 07:36:55', NULL),
(4, 3, 'S-21-25-Y-21-22', 'August', 'June', '2021', '2022', 1, 10, 10, NULL, '2021-02-25 07:37:59', '2021-02-25 07:37:59', NULL),
(5, 3, 'S-21-25-Y-22-23', 'August', 'June', '2022', '2023', 1, 10, 10, NULL, '2021-02-25 07:39:22', '2021-02-25 07:39:22', NULL),
(6, 3, 'S-21-25-Y-23-24', 'August', 'June', '2023', '2024', 1, 10, 10, NULL, '2021-02-25 07:41:28', '2021-02-25 07:41:28', NULL),
(7, 3, 'S-21-25-Y-24-25', 'August', 'June', '2024', '2025', 1, 10, 10, NULL, '2021-02-25 07:42:48', '2021-02-25 07:42:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_account_types`
--

CREATE TABLE `erp_account_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_account_types`
--

INSERT INTO `erp_account_types` (`id`, `name`, `code`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Assets', 'A', 1, 1, 1, NULL, '2021-02-23 05:39:49', '2021-02-23 05:39:49', NULL),
(2, 'Equity', 'P', 0, 1, 1, NULL, '2021-02-23 05:39:49', '2021-02-23 05:39:49', NULL),
(3, 'Expenses', 'E', 1, 1, 1, NULL, '2021-02-23 05:39:49', '2021-02-23 05:39:49', NULL),
(4, 'Income', 'I', 1, 1, 1, NULL, '2021-02-23 05:39:49', '2021-02-23 05:39:49', NULL),
(5, 'Liabilities', 'L', 1, 1, 1, NULL, '2021-02-23 05:39:49', '2021-02-23 05:39:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_asset_types`
--

CREATE TABLE `erp_asset_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_attendances`
--

CREATE TABLE `erp_attendances` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `attendees_type` int(11) NOT NULL,
  `link_id` int(10) UNSIGNED NOT NULL,
  `years_id` int(10) UNSIGNED NOT NULL,
  `months_id` int(10) UNSIGNED NOT NULL,
  `day_1` int(11) NOT NULL DEFAULT 0,
  `day_2` int(11) NOT NULL DEFAULT 0,
  `day_3` int(11) NOT NULL DEFAULT 0,
  `day_4` int(11) NOT NULL DEFAULT 0,
  `day_5` int(11) NOT NULL DEFAULT 0,
  `day_6` int(11) NOT NULL DEFAULT 0,
  `day_7` int(11) NOT NULL DEFAULT 0,
  `day_8` int(11) NOT NULL DEFAULT 0,
  `day_9` int(11) NOT NULL DEFAULT 0,
  `day_10` int(11) NOT NULL DEFAULT 0,
  `day_11` int(11) NOT NULL DEFAULT 0,
  `day_12` int(11) NOT NULL DEFAULT 0,
  `day_13` int(11) NOT NULL DEFAULT 0,
  `day_14` int(11) NOT NULL DEFAULT 0,
  `day_15` int(11) NOT NULL DEFAULT 0,
  `day_16` int(11) NOT NULL DEFAULT 0,
  `day_17` int(11) NOT NULL DEFAULT 0,
  `day_18` int(11) NOT NULL DEFAULT 0,
  `day_19` int(11) NOT NULL DEFAULT 0,
  `day_20` int(11) NOT NULL DEFAULT 0,
  `day_21` int(11) NOT NULL DEFAULT 0,
  `day_22` int(11) NOT NULL DEFAULT 0,
  `day_23` int(11) NOT NULL DEFAULT 0,
  `day_24` int(11) NOT NULL DEFAULT 0,
  `day_25` int(11) NOT NULL DEFAULT 0,
  `day_26` int(11) NOT NULL DEFAULT 0,
  `day_27` int(11) NOT NULL DEFAULT 0,
  `day_28` int(11) NOT NULL DEFAULT 0,
  `day_29` int(11) NOT NULL DEFAULT 0,
  `day_30` int(11) NOT NULL DEFAULT 0,
  `day_31` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_attendance_masters`
--

CREATE TABLE `erp_attendance_masters` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `year` int(10) UNSIGNED NOT NULL,
  `month` int(10) UNSIGNED NOT NULL,
  `day_in_month` int(11) NOT NULL,
  `holiday` int(11) NOT NULL,
  `open` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_attendance_statuses`
--

CREATE TABLE `erp_attendance_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_class` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_bankdetail`
--

CREATE TABLE `erp_bankdetail` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `accountType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_type` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `account_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_no` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_banks`
--

CREATE TABLE `erp_banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_branches`
--

CREATE TABLE `erp_branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_name` int(10) UNSIGNED DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `branch_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` int(10) UNSIGNED DEFAULT NULL,
  `cell` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_branches`
--

INSERT INTO `erp_branches` (`id`, `company_id`, `name`, `city_name`, `city_id`, `country_id`, `branch_code`, `phone`, `fax`, `cell`, `address`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'DHA-5', NULL, 1, 1, '23', '223-12312', 2222, '3123-12312', 'demo', 1, 1, NULL, NULL, '2021-03-03 14:53:12', '2021-03-03 14:53:12', NULL),
(2, 2, 'demo', NULL, 1, 1, '33', '212-12', 212, '212', 'dasdasd', 1, 1, NULL, NULL, '2021-03-03 14:55:55', '2021-03-03 14:55:55', NULL),
(3, 3, 'part 1', NULL, 1, 1, '22', '222', 2222, '2222-2', 'dasdasd', 1, 1, NULL, NULL, '2021-03-03 15:07:03', '2021-03-03 15:07:03', NULL),
(4, 2, 'Davis road', NULL, 1, 1, '345', '343-45344444', 35345343, '4534-534534', 'address1', 1, 1, NULL, NULL, '2021-03-04 05:07:59', '2021-03-04 05:07:59', NULL),
(5, 4, 'DAV|iS Road curemd', NULL, 1, 1, '162', '344-44444444', 23443243, '3453-4534534', 'ASDASD', 1, 1, NULL, NULL, '2021-03-04 05:09:31', '2021-03-04 05:09:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_branch_users`
--

CREATE TABLE `erp_branch_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_branch_users`
--

INSERT INTO `erp_branch_users` (`id`, `user_id`, `branch_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 3, NULL, 3, NULL, '2021-02-23 05:40:44', '2021-02-23 05:40:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_buildings`
--

CREATE TABLE `erp_buildings` (
  `id` int(10) UNSIGNED NOT NULL,
  `aggreement_date` date DEFAULT NULL,
  `land_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_mail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` int(11) DEFAULT NULL,
  `cnic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_rent` int(11) DEFAULT NULL,
  `security` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tenure` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `increment` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_candidate_score`
--

CREATE TABLE `erp_candidate_score` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_form_id` int(10) UNSIGNED NOT NULL,
  `schedule_id` int(10) UNSIGNED NOT NULL,
  `relevant_knowledge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skills` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `communication` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relevant_experience` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confidence` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_candidate_score`
--

INSERT INTO `erp_candidate_score` (`id`, `candidate_form_id`, `schedule_id`, `relevant_knowledge`, `skills`, `communication`, `relevant_experience`, `confidence`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', NULL, NULL, NULL, '2021-02-23 10:46:12', '2021-02-23 10:46:12', NULL),
(2, 2, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', NULL, NULL, NULL, '2021-02-25 10:01:39', '2021-02-25 10:01:39', NULL),
(3, 3, 3, '2', '2', '2', '2', '2', NULL, NULL, NULL, '2021-02-28 10:23:24', '2021-02-28 10:23:24', NULL),
(4, 4, 4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', NULL, NULL, NULL, '2021-03-01 07:04:53', '2021-03-01 07:04:53', NULL),
(5, 5, 5, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', NULL, NULL, NULL, '2021-03-01 09:22:19', '2021-03-01 09:22:19', NULL),
(6, 5, 5, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', NULL, NULL, NULL, '2021-03-01 09:22:26', '2021-03-01 09:22:26', NULL),
(7, 6, 6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', NULL, NULL, NULL, '2021-03-01 09:32:58', '2021-03-01 09:32:58', NULL),
(8, 6, 6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', NULL, NULL, NULL, '2021-03-01 09:33:01', '2021-03-01 09:33:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_category_policies`
--

CREATE TABLE `erp_category_policies` (
  `id` int(10) UNSIGNED NOT NULL,
  `categorypolicy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categorypolicy_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_category_policies`
--

INSERT INTO `erp_category_policies` (`id`, `categorypolicy_name`, `categorypolicy_description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'Basic  Expectations', 'Respect People and Properly', NULL, NULL, NULL, '2021-02-24 06:32:22', '2021-02-24 06:36:33', NULL),
(6, 'Late comings', 'anyoe who is 15 minutes late from the office will be getting deduction on daily basis.', NULL, NULL, NULL, '2021-02-24 06:37:18', '2021-02-24 06:37:18', NULL),
(7, 'Moral ethics', 'Applicable on all the teachers', NULL, NULL, NULL, '2021-02-24 06:40:18', '2021-02-24 06:40:18', NULL),
(8, 'Attendance  and  Leaves', 'Late Arrivals', NULL, NULL, NULL, '2021-02-24 06:49:40', '2021-02-24 06:49:50', NULL),
(9, 'Student Attendance  and  Leave', 'Late  arrivals   need  to  pay   fine', NULL, NULL, NULL, '2021-02-26 11:12:07', '2021-02-26 11:12:07', NULL),
(10, '1', 'a', NULL, NULL, NULL, '2021-02-26 11:26:25', '2021-02-26 11:26:25', NULL),
(11, 'ABC', '123', NULL, NULL, NULL, '2021-02-26 11:29:02', '2021-02-26 11:29:02', NULL),
(12, 'Employee  Code of Conduct  1', '1. It is also a best practice to have policies on standards of conduct, drug and alcohol abuse, disciplinary action, confidentiality, conflicts of interest, and workplace violence.', NULL, NULL, NULL, '2021-02-26 11:31:58', '2021-02-26 11:31:58', NULL),
(13, 'Respect People and Property', 'Respect  People', NULL, NULL, NULL, '2021-03-01 10:15:12', '2021-03-01 10:15:12', NULL),
(14, 'Employee  Code  of  Conduct', 'Ethical and  Moral  Behavior', NULL, NULL, NULL, '2021-03-01 10:17:25', '2021-03-01 10:17:25', NULL),
(15, 'Employee Code of Conduct', 'It is also a best practice to have policies on standards of conduct, drug and alcohol abuse, disciplinary action, confidentiality, conflicts of interest, and workplace violence.', NULL, NULL, NULL, '2021-03-01 10:20:02', '2021-03-01 10:20:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_child_benefit`
--

CREATE TABLE `erp_child_benefit` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `percentage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_city`
--

CREATE TABLE `erp_city` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_city`
--

INSERT INTO `erp_city` (`id`, `name`, `country_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Lahore', 1, 1, '2021-02-23 05:39:32', '2021-02-23 05:39:32', NULL),
(2, 'Faisalabad', 1, 1, '2021-02-23 05:39:32', '2021-02-23 05:39:32', NULL),
(3, 'Bahawalpur', 1, 1, '2021-02-23 05:39:32', '2021-02-23 05:39:32', NULL),
(4, 'Islamabad', 1, 1, '2021-02-23 05:39:32', '2021-02-23 05:39:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_code_of_conducts`
--

CREATE TABLE `erp_code_of_conducts` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_policy_id` int(10) UNSIGNED NOT NULL,
  `codeofconduct_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codeofconduct_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_code_of_conducts`
--

INSERT INTO `erp_code_of_conducts` (`id`, `category_policy_id`, `codeofconduct_name`, `codeofconduct_description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 5, 'Respect People and Property', 'Students are to be respectful in attitude and manner to people and property.', NULL, NULL, NULL, '2021-02-24 06:32:49', '2021-02-24 06:32:49', NULL),
(6, 7, 'Teachers ethics', 'This Code contains rules of conduct and ethics to be observed so as to maintain the integrity, dignity and nobility of the teaching profession.', NULL, NULL, NULL, '2021-02-24 06:39:21', '2021-02-24 06:44:57', NULL),
(7, 8, 'Apply  for  Leave', 'Absence from school must be supported by a medical certificate. Excuse from school may be granted on compassionate grounds and on a case-by-case basis. All medical certificates or letters of ', NULL, NULL, NULL, '2021-02-24 06:51:04', '2021-02-24 06:51:04', NULL),
(8, 8, 'Leave  without    Application', '500   Fine    per   late  and   absent   without  leave', NULL, NULL, NULL, '2021-02-26 11:14:17', '2021-02-26 11:14:17', NULL),
(9, 6, 'Late  arrivals  will   be  charges  fine', 'It is also a best practice to have policies on standards of conduct, drug and alcohol abuse, disciplinary action, confidentiality, conflicts of interest, and workplace violence.', NULL, NULL, NULL, '2021-02-26 11:16:54', '2021-02-26 11:16:54', NULL),
(10, 10, '2', 'b', NULL, NULL, NULL, '2021-02-26 11:26:49', '2021-02-26 11:26:49', NULL),
(11, 11, 'CDE', '456', NULL, NULL, NULL, '2021-02-26 11:29:23', '2021-02-26 11:29:23', NULL),
(12, 12, 'Ethical Rules', '2  It is also a best practice to have policies on standards of conduct, drug and alcohol abuse, disciplinary action, confidentiality, conflicts of interest, and workplace violence.', NULL, NULL, NULL, '2021-02-26 11:32:54', '2021-02-26 11:32:54', NULL),
(13, 5, 'Ethical Rules', 'It is also a best practice to have policies on standards of conduct, drug and alcohol abuse, disciplinary action, confidentiality, conflicts of interest, and workplace violence.', NULL, NULL, NULL, '2021-03-01 10:18:21', '2021-03-01 10:18:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_companies`
--

CREATE TABLE `erp_companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ntn` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_type` enum('fixed','percentage') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_companies`
--

INSERT INTO `erp_companies` (`id`, `name`, `description`, `ntn`, `gst`, `gst_type`, `vat`, `phone`, `fax`, `address`, `logo`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Roots IVY', 'demo', '1111', NULL, NULL, NULL, '111-111', '111', 'demo', 'Admin-logo.sql', 1, NULL, NULL, NULL, NULL, '2021-03-03 14:52:36', '2021-03-03 14:52:36'),
(2, 'Demo Company', 'asdas', '111', NULL, NULL, NULL, '121-3', '2222', 'dsda', 'Admin-logo.sql', 1, NULL, NULL, NULL, NULL, '2021-03-03 14:55:40', '2021-03-03 14:55:40'),
(3, 'new', 'asdasd', '1212', NULL, NULL, NULL, '121-21', '21212', 'asdasd', 'Admin-logo.sql', 1, NULL, NULL, NULL, NULL, '2021-03-03 15:06:38', '2021-03-03 15:06:38'),
(4, 'DemoABC', 'demo', '1111', NULL, NULL, NULL, '873-949324', '334324324', 'asdasd', 'Admin-logo.csv', 1, NULL, NULL, NULL, NULL, '2021-03-04 05:06:02', '2021-03-04 05:06:02');

-- --------------------------------------------------------

--
-- Table structure for table `erp_country`
--

CREATE TABLE `erp_country` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_country`
--

INSERT INTO `erp_country` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2345', 1, '2021-02-23 05:39:31', '2021-02-23 06:39:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_currencies`
--

CREATE TABLE `erp_currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(11,2) NOT NULL,
  `is_default` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_currencies`
--

INSERT INTO `erp_currencies` (`id`, `name`, `code`, `symbol`, `rate`, `is_default`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Pakistani Rupees', 'PKR', 'Rs.', '1.00', 1, 1, 1, 1, NULL, '2021-02-23 05:40:34', '2021-02-23 05:40:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_customers`
--

CREATE TABLE `erp_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `databank_id` int(10) UNSIGNED DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_2` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax_1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax_2` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_1` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_2` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cnic` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ntn` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_limit` int(10) UNSIGNED DEFAULT NULL,
  `credit_term_days` smallint(5) UNSIGNED DEFAULT NULL,
  `discount` smallint(5) UNSIGNED DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_ac_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_id` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_c_invoice`
--

CREATE TABLE `erp_c_invoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `ci_no` int(11) DEFAULT NULL,
  `eta` date DEFAULT NULL,
  `atp` date DEFAULT NULL,
  `etd` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lcno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mos` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usd_amt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `today_doller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lc_expiry_date` date DEFAULT NULL,
  `total_days` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_date` date DEFAULT NULL,
  `document_received` int(11) DEFAULT NULL,
  `bl_no` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `container_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `margin_rat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `murahba_profit_rate` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_databank_comment`
--

CREATE TABLE `erp_databank_comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_departments`
--

CREATE TABLE `erp_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_name` int(10) UNSIGNED DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_duty_calculation`
--

CREATE TABLE `erp_duty_calculation` (
  `id` int(10) UNSIGNED NOT NULL,
  `duty_id` int(10) UNSIGNED DEFAULT NULL,
  `percentage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_entries`
--

CREATE TABLE `erp_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher_date` date NOT NULL,
  `cheque_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `cdr_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdr_date` date DEFAULT NULL,
  `bdr_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bdr_date` date DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_branch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drawn_date` date DEFAULT NULL,
  `dr_total` decimal(11,2) NOT NULL DEFAULT 0.00,
  `cr_total` decimal(11,2) NOT NULL DEFAULT 0.00,
  `narration` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry_type_id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `customers_id` int(10) UNSIGNED DEFAULT NULL,
  `stockitems_id` int(10) UNSIGNED DEFAULT NULL,
  `suppliers_id` int(10) UNSIGNED DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_entry_items`
--

CREATE TABLE `erp_entry_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `entry_type_id` int(10) UNSIGNED NOT NULL,
  `entry_id` int(10) UNSIGNED NOT NULL,
  `ledger_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_date` date NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `dc` enum('d','c') COLLATE utf8mb4_unicode_ci NOT NULL,
  `narration` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_entry_types`
--

CREATE TABLE `erp_entry_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_entry_types`
--

INSERT INTO `erp_entry_types` (`id`, `name`, `code`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Journal Voucher', 'GJV', 1, 1, 1, NULL, '2021-02-23 05:40:43', '2021-02-23 05:40:43', NULL),
(2, 'Cash Receipt Voucher', 'CRV', 1, 1, 1, NULL, '2021-02-23 05:40:43', '2021-02-23 05:40:43', NULL),
(3, 'Cash Payment Voucher', 'CPV', 1, 1, 1, NULL, '2021-02-23 05:40:43', '2021-02-23 05:40:43', NULL),
(4, 'Bank Receipt Voucher', 'BRV', 1, 1, 1, NULL, '2021-02-23 05:40:43', '2021-02-23 05:40:43', NULL),
(5, 'Bank Payment Voucher', 'BPV', 1, 1, 1, NULL, '2021-02-23 05:40:43', '2021-02-23 05:40:43', NULL),
(6, 'Fee Voucher', 'FV', 1, 1, 1, NULL, '2021-02-23 05:40:43', '2021-02-23 05:40:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_fee_head`
--

CREATE TABLE `erp_fee_head` (
  `id` int(10) UNSIGNED NOT NULL,
  `fee_section_id` int(10) UNSIGNED DEFAULT NULL,
  `rec_parent_group_id` int(10) UNSIGNED NOT NULL,
  `inc_parent_group_id` int(10) UNSIGNED NOT NULL,
  `rec_group_id` int(10) UNSIGNED NOT NULL,
  `inc_group_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_fee_head`
--

INSERT INTO `erp_fee_head` (`id`, `fee_section_id`, `rec_parent_group_id`, `inc_parent_group_id`, `rec_group_id`, `inc_group_id`, `title`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 17, 120, 20, 124, 'Registration Fee', '(only at the time of admission)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(2, 1, 17, 120, 136, 127, 'Admission Fee', '(only at the time of admission)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(3, 1, 17, 34, 137, 35, 'Security Fee', '(only at the time of admission)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(4, 1, 17, 120, 138, 128, 'RIVY Montessori Bag', '(only at the time of admission)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(5, 1, 17, 120, 139, 129, 'RIVY Prospectus', '(only at the time of admission)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(6, 2, 17, 12, 19, 14, 'IB Fee', '(Yearly)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(7, 2, 17, 120, 141, 131, 'RIVY ID Card', '(Yearly)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(8, 3, 17, 12, 18, 13, 'Tuition Fee', '(Aug-may)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(9, 3, 17, 120, 142, 132, 'LMS Fee', '(Aug-may)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(10, 4, 17, 120, 143, 135, 'Late Payment', '(Aug-may)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(11, 4, 17, 125, 144, 126, 'External Fee British Council', '(Aug-may)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(12, 4, 17, 125, 145, 134, 'External Fee SAT', '(Aug-may)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(13, 3, 17, 120, 146, 133, 'Lab Fee', '(Aug-may)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL),
(14, 3, 17, 120, 140, 130, 'RIVY Year Book - RIVY News', '(Aug-may)', 1, NULL, NULL, NULL, '2021-02-08 12:57:47', '2021-02-08 12:57:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_fee_section`
--

CREATE TABLE `erp_fee_section` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_fee_section`
--

INSERT INTO `erp_fee_section` (`id`, `name`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'One Time Fee', '(At the time of Admission)', 1, NULL, NULL, NULL, '2021-02-08 12:57:44', '2021-02-08 12:57:44', NULL),
(2, 'Once in Year', '(Once in year)', 1, NULL, NULL, NULL, '2021-02-08 12:57:44', '2021-02-08 12:57:44', NULL),
(3, 'One Academic Fee', '(Once in year)', 1, NULL, NULL, NULL, '2021-02-08 12:57:44', '2021-02-08 12:57:44', NULL),
(4, 'Other Fee', '(Once in year)', 1, NULL, NULL, NULL, '2021-02-08 12:57:44', '2021-02-08 12:57:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_financial_years`
--

CREATE TABLE `erp_financial_years` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_financial_years`
--

INSERT INTO `erp_financial_years` (`id`, `name`, `start_date`, `end_date`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2020', '2020-01-01', '2020-12-31', 1, 1, 1, NULL, '2021-02-23 05:40:16', '2021-02-23 05:40:16', NULL),
(2, '2021', '2021-01-01', '2021-12-31', 1, 1, 1, NULL, '2021-02-23 05:40:16', '2021-02-23 05:40:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_fixedassets`
--

CREATE TABLE `erp_fixedassets` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_is` int(11) DEFAULT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `branch` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `po_number` int(11) DEFAULT NULL,
  `serial_number` int(11) DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products_date` datetime NOT NULL,
  `warranty_expires` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_goodreceipt`
--

CREATE TABLE `erp_goodreceipt` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_to` int(11) NOT NULL,
  `branch_from` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_goodsissudetail`
--

CREATE TABLE `erp_goodsissudetail` (
  `id` int(10) UNSIGNED NOT NULL,
  `gissue_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_goodsissue`
--

CREATE TABLE `erp_goodsissue` (
  `id` int(10) UNSIGNED NOT NULL,
  `sup_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_goodsreceiptdetail`
--

CREATE TABLE `erp_goodsreceiptdetail` (
  `id` int(10) UNSIGNED NOT NULL,
  `gr_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_gratuity`
--

CREATE TABLE `erp_gratuity` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `gratuity_amount` int(10) UNSIGNED DEFAULT NULL,
  `years` int(10) UNSIGNED DEFAULT NULL,
  `basic` int(10) UNSIGNED NOT NULL,
  `paid_on` date DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `remarks` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_groups`
--

CREATE TABLE `erp_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `account_type_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `parent_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_groups`
--

INSERT INTO `erp_groups` (`id`, `name`, `number`, `code`, `level`, `parent_id`, `account_type_id`, `status`, `parent_type`, `company_id`, `city_id`, `branch_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Assets', '1', 'ASSETS', 1, 0, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:31:42', '2018-02-16 13:31:42', NULL),
(2, 'Liabilities & Owners Equity', '2', 'LIABILITIES+OWNER+Q', 1, 0, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:31:42', '2018-02-16 13:31:42', NULL),
(3, 'Incomes', '3', 'INCOMES', 1, 0, 4, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:31:42', '2018-02-16 13:31:42', NULL),
(4, 'Expenses', '4', 'EXPENSES', 1, 0, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:31:42', '2018-02-16 13:31:42', NULL),
(5, 'Owner\'s Capital & Reserves', '2-01', 'Share Capital and Reserves', 2, 2, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 12:55:02', '2019-11-09 23:14:15', NULL),
(6, 'Issued Subscribed and Paid  Up Capital', '2-01-001', 'Authorized Capital', 3, 5, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 12:55:29', '2019-09-21 08:56:57', NULL),
(7, 'Reserves', '2-01-002', 'Reserves', 3, 5, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:13:18', '2018-02-16 13:13:18', NULL),
(8, 'Current Liabilites', '2-03', 'Current Liabilites', 2, 2, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:21:18', '2018-02-16 13:21:18', NULL),
(10, 'Other Payables', '2-03-001', 'Trade and Other Payables', 4, 8, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:21:51', '2020-12-02 13:24:40', NULL),
(11, 'Accrued Liablities', '2-03-002', 'Accrued Liablities', 4, 8, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:22:11', '2019-11-09 22:53:51', NULL),
(12, 'Revenue & other Income', '3-01', 'Revenue & other Income', 2, 3, 4, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-16 13:24:42', '2018-02-16 13:24:42', NULL),
(15, 'Non Current Assets', '1-01', 'Non Current Assets', 2, 1, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-17 17:09:27', '2019-11-07 00:40:30', NULL),
(16, 'Current Assets', '1-02', 'Current Assets', 2, 1, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-17 17:15:29', '2018-02-17 17:15:29', NULL),
(17, 'Receiveables', '1-02-002', 'Trade Debts', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-17 17:32:21', '2019-10-08 18:22:13', NULL),
(21, 'Other Receivables', '1-02-003', 'Misc Advances / Receivables', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-17 17:34:26', '2019-10-08 18:25:45', NULL),
(22, 'Cash and Bank Balance', '1-02-008', 'Cash and Bank Balance', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-17 17:36:28', '2018-02-17 17:36:28', NULL),
(23, 'Cash in hand', '1-02-008-0001', 'Cash in hand', 4, 22, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-17 17:36:48', '2018-02-17 17:36:48', NULL),
(24, 'Petty Cash', '1-02-008-0002', 'Petty Cash', 4, 22, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-17 17:37:08', '2018-02-17 17:37:08', NULL),
(25, 'Bank Balance', '1-02-008-0003', 'Bank Balance', 4, 22, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-02-17 17:37:32', '2018-02-17 17:37:32', NULL),
(26, 'Stock', '1-02-001', 'STOCK', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2018-10-03 10:13:24', '2018-10-03 10:13:24', NULL),
(27, 'Cost of Sales', '4-01', '', 2, 4, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-07-30 10:25:25', '2019-11-05 21:37:45', NULL),
(28, 'Insurance Claim Receivables', '1-02-003-0001', '', 4, 21, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-08-25 16:03:58', '2019-11-09 19:58:35', NULL),
(29, 'PaidUp Share Capital', '2-01-001-0001', '', 4, 6, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-09-21 08:57:27', '2019-09-21 08:57:27', NULL),
(30, 'Bonus Shares Capital', '2-01-001-0002', '', 4, 6, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-09-21 08:58:41', '2019-09-21 08:58:41', NULL),
(31, 'Revenue Reserves', '2-01-002-0001', '', 4, 7, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-09-21 12:04:02', '2019-09-21 12:04:02', NULL),
(32, 'Revaluation Reserves', '2-01-002-0002', '', 4, 7, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-09-21 12:05:16', '2019-09-21 12:05:16', NULL),
(33, 'Capital Reserves', '2-01-002-0003', '', 4, 7, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-09-21 12:06:42', '2019-09-21 12:06:42', NULL),
(34, 'Non Current Liabilities', '2-02', '', 2, 2, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-09-21 12:08:45', '2019-09-21 12:08:45', NULL),
(35, 'Security Fee', '2-02-001', '', 3, 34, 5, 1, '3', NULL, NULL, NULL, 1, 1, NULL, '2019-09-21 12:09:22', '2020-08-27 10:55:57', NULL),
(36, 'Operating Expenses', '4-02', '', 2, 4, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-10-06 10:39:49', '2019-10-06 10:39:49', NULL),
(37, 'Advance Taxes', '2-03-003', '', 3, 8, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-10-06 10:39:49', '2019-10-06 10:39:49', NULL),
(38, 'Vehicle Running & Maintenance', '4-02-001', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 01:55:16', '2019-11-06 01:55:16', NULL),
(39, 'Admin & Accounts Petrol - Vehicles', '4-02-001-0001', '', 4, 38, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 01:55:53', '2019-11-06 01:55:53', NULL),
(40, 'Repair Maintenance - Vehicles', '4-02-001-0002', '', 4, 38, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 01:56:25', '2019-11-06 01:56:25', NULL),
(41, 'Vehicles -  Cars', '4-02-001-0001-00001', '', 5, 39, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 01:57:26', '2019-11-06 01:57:26', NULL),
(42, 'Vehicles -  Cars', '4-02-001-0002-00001', '', 5, 40, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 01:59:14', '2019-11-06 01:59:14', NULL),
(43, 'Office Utilities', '4-02-003', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:00:44', '2019-11-06 02:00:44', NULL),
(44, 'Utilities Expenses', '4-02-003-0001', '', 4, 43, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:01:24', '2019-11-06 02:01:24', NULL),
(45, 'Printing and Stationary', '4-02-004', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:02:24', '2019-11-06 02:02:24', NULL),
(46, 'Rent, Rates & Taxes', '4-02-005', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:03:07', '2019-11-06 02:03:07', NULL),
(47, 'Rent', '4-02-005-0001', '', 4, 46, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:03:39', '2019-11-06 02:03:39', NULL),
(48, 'Property Tax', '4-02-005-0002', '', 4, 46, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:04:00', '2019-11-06 02:04:00', NULL),
(49, 'Postage and Courier Charges', '4-02-006', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:04:25', '2019-11-06 02:04:25', NULL),
(50, 'Office Entertainment', '4-02-007', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:05:02', '2019-11-06 02:05:02', NULL),
(51, 'Annual Subscription', '4-02-008', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:06:42', '2019-11-06 02:06:42', NULL),
(52, 'Disturbance Allowance', '4-02-009', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:07:11', '2019-11-06 02:07:11', NULL),
(53, 'Insurance Charges', '4-02-010', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:07:35', '2019-11-06 02:07:35', NULL),
(54, 'Assets-Insurance', '4-02-010-0001', '', 4, 53, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:08:02', '2019-11-06 02:08:02', NULL),
(55, 'Other-Insurance', '4-02-010-0002', '', 4, 53, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:08:25', '2019-11-06 02:08:25', NULL),
(56, 'Bank Charges', '4-02-011', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:09:54', '2019-11-06 02:09:54', NULL),
(57, 'Local Bank Charges', '4-02-011-0001', '', 4, 56, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 02:10:17', '2019-11-06 02:10:17', NULL),
(58, 'Taxation', '4-04', '', 2, 4, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 20:10:00', '2019-11-06 20:10:00', NULL),
(59, 'Current Year Taxation', '4-04-001', '', 3, 58, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 20:10:41', '2019-11-06 20:10:41', NULL),
(60, 'Prior Year Taxation', '4-04-002', '', 3, 58, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 20:11:02', '2019-11-06 20:11:02', NULL),
(61, 'Refunds Due from Government Authorities', '1-02-003-0002', '', 4, 21, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-06 20:26:32', '2019-11-06 20:26:32', NULL),
(62, 'Land & Building -Cost', '1-01-001', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:41:21', '2019-11-07 00:41:21', NULL),
(63, 'Land', '1-01-001-0001', '', 4, 62, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:42:04', '2019-11-07 00:42:04', NULL),
(64, 'Building', '1-01-001-0002', '', 4, 62, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:42:25', '2019-11-07 00:42:25', NULL),
(65, 'Office Equipment\'s', '1-01-002', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:43:09', '2019-11-07 00:43:09', NULL),
(66, 'Furniture & Fixture', '1-01-003', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:43:44', '2019-11-07 00:43:44', NULL),
(67, 'Computers & Accessories', '1-01-004', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:44:04', '2019-11-07 00:44:04', NULL),
(68, 'Electric Equipment\'s', '1-01-005', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:44:22', '2019-11-07 00:44:22', NULL),
(69, 'Plant and Machinery', '1-01-006', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:44:50', '2019-11-07 00:44:50', NULL),
(70, 'Telephone Exchange & Installations', '1-01-007', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:45:07', '2019-11-07 00:45:07', NULL),
(71, 'Vehicles', '1-01-008', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:45:26', '2019-11-07 00:45:26', NULL),
(72, 'Arm And Ammunition', '1-01-009', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:45:45', '2019-11-07 00:45:45', NULL),
(73, 'Assets - Accumulated Depreciation', '1-01-010', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:46:49', '2019-11-07 00:46:49', NULL),
(74, 'Building - Accumulated Depreciation', '1-01-010-0001', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:47:26', '2019-11-07 00:47:26', NULL),
(75, 'Office Equipment\'s - Accumulated Depreciation', '1-01-010-0002', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:47:55', '2019-11-07 00:47:55', NULL),
(76, 'Furniture & Fixture - Accumulated Depreciation', '1-01-010-0003', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:48:15', '2019-11-07 00:48:15', NULL),
(77, 'Computers & Accessories - Accumulated Depreciation', '1-01-010-0004', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:48:30', '2019-11-07 00:48:30', NULL),
(78, 'Electric Equipment\'s - Accumulated Depreciation', '1-01-010-0005', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:48:53', '2019-11-07 00:48:53', NULL),
(79, 'Plant and Machinery - Accumulated Depreciation', '1-01-010-0006', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:49:10', '2019-11-07 00:49:10', NULL),
(80, 'Telephone Exchange & Installations - Accumulated Depreciation', '1-01-010-0007', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:49:28', '2019-11-07 00:49:28', NULL),
(81, 'Vehicles - Accumulated Depreciation', '1-01-010-0008', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:49:45', '2019-11-07 00:49:45', NULL),
(82, 'Arm And Ammunition - Accumulated Depreciation', '1-01-010-0009', '', 4, 73, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:50:03', '2019-11-07 00:50:03', NULL),
(83, 'Intangible Assets', '1-01-012', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:58:28', '2019-11-07 00:58:28', NULL),
(84, 'Software\'s', '1-01-012-0001', '', 4, 83, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:59:05', '2019-11-07 00:59:05', NULL),
(85, 'Long Term Investments', '1-01-013', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:59:27', '2019-11-07 00:59:27', NULL),
(86, 'Investments in Shares', '1-01-013-0001', '', 4, 85, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 00:59:49', '2019-11-07 00:59:49', NULL),
(87, 'Long Term Deposits', '1-01-014', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 01:00:12', '2019-11-07 01:00:12', NULL),
(88, 'Term Deposit Receipts', '1-01-014-0001', '', 4, 87, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 01:00:31', '2019-11-07 01:00:31', NULL),
(89, 'Short Term Deposits', '1-01-011', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 01:01:18', '2019-11-07 01:01:18', NULL),
(90, 'Shot Term Deposit Receipts', '1-01-011-0001', '', 4, 89, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 01:01:48', '2019-11-07 01:01:48', NULL),
(91, 'Rent', '1-01-015', '', 3, 15, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-07 01:02:57', '2019-11-07 01:02:57', NULL),
(92, 'Loan & Advances', '1-02-004', '', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 20:04:49', '2019-11-09 20:04:49', NULL),
(93, 'Advances To Director', '1-02-004-0001', '', 4, 92, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 20:06:18', '2019-11-09 20:06:18', NULL),
(94, 'Loan/Advances To Staff', '1-02-004-0002', '', 4, 92, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 20:06:48', '2019-11-09 20:06:48', NULL),
(95, 'Trade Debts & Short term Pre payments', '1-02-006', '', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 20:15:46', '2019-11-09 20:15:46', NULL),
(96, 'Pre paid Insurance', '1-02-006-0001', '', 4, 95, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 20:17:24', '2019-11-09 20:17:24', NULL),
(97, 'Pre Paid Lease Rentals', '1-02-006-0002', '', 4, 95, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 20:17:51', '2019-11-09 20:17:51', NULL),
(98, 'Other Financial Assets', '1-02-007', '', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 20:18:36', '2019-11-09 20:18:36', NULL),
(99, 'Certificate of Deposits', '1-02-007-0001', '', 4, 98, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 20:26:00', '2019-11-09 20:26:00', NULL),
(100, 'Directors Remuneration payable', '2-03-002-0001', '', 5, 11, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 23:03:48', '2019-11-09 23:03:48', NULL),
(101, 'Salaries & Wages Payable', '2-03-002-0002', '', 5, 11, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 23:04:28', '2019-11-09 23:04:28', NULL),
(102, 'Utilities Payable', '2-03-002-0003', '', 5, 11, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 23:04:51', '2019-11-09 23:04:51', NULL),
(103, 'Other Payable', '2-03-002-0004', '', 5, 11, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 23:05:14', '2019-11-09 23:05:14', NULL),
(104, 'Fee & Subscriptions Payable', '2-03-002-0005', '', 5, 11, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 23:06:39', '2019-11-09 23:06:39', NULL),
(105, 'Others', '2-03-002-0006', '', 5, 11, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-11-09 23:07:06', '2020-08-25 09:51:43', NULL),
(106, 'Local Stock', '1-02-001-0001', '', 4, 26, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-25 19:35:31', '2019-12-25 19:35:31', NULL),
(107, 'Vehicles - Motor Cars', '1-01-008-0001', '', 4, 71, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-26 17:25:31', '2019-12-26 17:25:31', NULL),
(108, 'Guns', '1-01-009-0001', '', 4, 72, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-26 17:26:52', '2019-12-26 17:26:52', NULL),
(109, 'Pistol', '1-01-009-0002', '', 4, 72, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-26 17:27:14', '2019-12-26 17:27:14', NULL),
(110, 'Insurance Claims Receivables', '1-02-003-0003', '', 4, 21, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-26 17:33:23', '2019-12-26 17:33:23', NULL),
(111, 'Loan to Employees', '1-02-004-0002-00001', '', 5, 94, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-26 17:41:34', '2019-12-26 17:41:34', NULL),
(112, 'Advances to Employees', '1-02-004-0002-00002', '', 5, 94, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-26 17:42:18', '2019-12-26 17:42:18', NULL),
(113, 'Advances to Workers, Contractors & Others', '1-02-004-0002-00003', '', 5, 94, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-26 17:43:02', '2019-12-26 17:43:02', NULL),
(114, 'Office Rent', '4-02-012', '', 3, 36, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-27 20:43:36', '2019-12-27 20:43:36', NULL),
(115, 'Internet Bill', '4-02-003-0002', '', 4, 43, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-30 14:06:38', '2019-12-30 14:06:38', NULL),
(116, 'Mobile Bills', '4-02-003-0003', '', 4, 43, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-30 14:06:54', '2019-12-30 14:06:54', NULL),
(117, 'Scratch Cards Expenses', '4-02-003-0004', '', 4, 43, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-30 14:07:27', '2019-12-30 14:07:27', NULL),
(118, 'Electricity Charges', '4-02-003-0005', '', 4, 43, 3, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2019-12-30 14:12:05', '2019-12-30 14:12:05', NULL),
(120, 'Other Revenue', '3-01-003', '', 3, 12, 4, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-03-10 06:03:08', '2020-08-23 06:50:10', NULL),
(121, 'Security Deposit', '1-02-009', '', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-08-19 06:26:20', '2020-08-19 06:26:20', NULL),
(122, 'Advance Tax', '1-02-005', '', 3, 16, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-08-19 06:31:08', '2020-08-19 06:31:08', NULL),
(123, 'Withholding Tax Payable', '2-03-002-0007', '', 5, 11, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-08-20 07:17:35', '2020-08-25 09:52:04', NULL),
(124, 'Registration Fee', '3-01-003-0001', '', 4, 120, 4, 1, '1', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 09:41:30', '2020-08-25 09:41:30', NULL),
(125, 'External Fee', '3-01-004', '', 3, 12, 4, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 09:43:37', '2020-08-25 09:43:37', NULL),
(126, 'External Fee British Council', '3-01-004-0001', '', 4, 125, 4, 1, '11', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 09:45:03', '2020-08-25 09:45:03', NULL),
(127, 'Admission Fee', '3-01-003-0002', '', 4, 120, 4, 1, '2', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 10:54:58', '2020-08-25 10:54:58', NULL),
(128, 'RIVY Montessori Bag', '3-01-003-0003', '', 4, 120, 4, 1, '4', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 10:58:55', '2020-08-25 10:58:55', NULL),
(129, 'RIVY Prospectus', '3-01-003-0004', '', 4, 120, 4, 1, '5', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 11:00:57', '2020-08-25 11:00:57', NULL),
(130, 'RIVY Year Book - RIVY News', '3-01-003-0005', '', 4, 120, 4, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 11:05:21', '2020-08-25 11:05:21', NULL),
(131, 'RIVY ID Card', '3-01-003-0006', '', 4, 120, 4, 1, '7', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 11:07:33', '2020-08-25 11:07:33', NULL),
(132, 'LMS Fee', '3-01-003-0007', '', 4, 120, 4, 1, '9', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 11:08:38', '2020-08-25 11:08:38', NULL),
(133, 'Lab Fee', '3-01-003-0008', '', 4, 120, 4, 1, '13', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 12:06:19', '2020-08-25 12:06:19', NULL),
(134, 'External Fee SAT', '3-01-004-0002', '', 4, 125, 4, 1, '12', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 12:43:57', '2020-08-25 12:43:57', NULL),
(135, 'Late Payment', '3-01-003-0009', '', 4, 120, 4, 1, '10', NULL, NULL, NULL, 1, 1, NULL, '2020-08-25 13:12:38', '2020-08-25 13:12:38', NULL),
(136, 'Admission Fee', '1-02-002-0004', '', 4, 17, 1, 1, '2', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 05:49:54', '2020-08-26 05:56:20', NULL),
(137, 'Security Fee', '1-02-002-0005', '', 4, 17, 1, 1, '3', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 05:58:22', '2020-08-26 05:58:22', NULL),
(138, 'RIVY Montessori Bag', '1-02-002-0006', '', 4, 17, 1, 1, '4', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 06:00:25', '2020-08-26 06:00:25', NULL),
(139, 'RIVY Prospectus', '1-02-002-0007', '', 4, 17, 1, 1, '5', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 06:01:50', '2020-08-26 06:01:50', NULL),
(140, 'RIVY Year Book - RIVY News', '1-02-002-0008', '', 4, 17, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 06:03:01', '2020-08-26 06:03:01', NULL),
(141, 'RIVY ID Card', '1-02-002-0009', '', 4, 17, 1, 1, '7', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 06:04:02', '2020-08-26 06:04:02', NULL),
(142, 'LMS Fee', '1-02-002-0010', '', 4, 17, 1, 1, '9', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 06:04:54', '2020-08-26 06:04:54', NULL),
(143, 'Late Payment', '1-02-002-0011', '', 4, 17, 1, 1, '10', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 06:06:23', '2020-08-26 06:06:23', NULL),
(144, 'External Fee British Council', '1-02-002-0012', '', 4, 17, 1, 1, '11', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 06:07:14', '2020-08-26 06:07:14', NULL),
(145, 'External Fee SAT', '1-02-002-0013', '', 4, 17, 1, 1, '12', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 06:07:50', '2020-08-26 06:07:50', NULL),
(146, 'Lab Fee', '1-02-002-0014', '', 4, 17, 1, 1, '13', NULL, NULL, NULL, 1, 1, NULL, '2020-08-26 10:25:32', '2020-08-26 10:25:32', NULL),
(147, 'Miscellaneous', '3-01-005', '', 3, 12, 4, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-12-02 13:30:25', '2020-12-02 13:30:25', NULL),
(148, 'Tax 236i Receiveable', '1-02-005-0001', '', 4, 122, 1, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-12-02 13:30:25', '2020-12-02 13:30:25', NULL),
(149, 'Tax 236i Payable', '2-03-003-0001', '', 4, 37, 5, 1, '', NULL, NULL, NULL, 1, 1, NULL, '2020-12-02 13:30:25', '2020-12-02 13:30:25', NULL);
-- --------------------------------------------------------

--
-- Table structure for table `erp_inventorytransfer`
--

CREATE TABLE `erp_inventorytransfer` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_from` int(11) NOT NULL,
  `branch_to` int(11) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_inventorytransferdetail`
--

CREATE TABLE `erp_inventorytransferdetail` (
  `id` int(10) UNSIGNED NOT NULL,
  `i_transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `transfer_qty` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_invoice_details`
--

CREATE TABLE `erp_invoice_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_prints` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salesinvoice_id` int(10) UNSIGNED NOT NULL,
  `serialNo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_count` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `return_qty` int(11) NOT NULL DEFAULT 0,
  `unit_price` bigint(20) NOT NULL,
  `sale_price` bigint(20) NOT NULL,
  `tax_amount` bigint(20) NOT NULL,
  `tax_percent` bigint(20) NOT NULL,
  `discount` bigint(20) DEFAULT NULL,
  `discount_type` int(11) DEFAULT NULL,
  `discount_amount` bigint(20) DEFAULT NULL,
  `misc_amount` bigint(20) DEFAULT NULL,
  `net_income` bigint(20) DEFAULT NULL,
  `total_price` bigint(20) NOT NULL,
  `line_total` bigint(20) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_invoice_services`
--

CREATE TABLE `erp_invoice_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `salesinvoice_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` bigint(20) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_lcbank`
--

CREATE TABLE `erp_lcbank` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_type` int(11) DEFAULT NULL,
  `pf_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bnk_id` int(11) DEFAULT NULL,
  `lc_amt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lc_status` int(11) DEFAULT NULL,
  `open_date` datetime DEFAULT NULL,
  `dollar_rate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `today_doller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_lc_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lc_expiry_date` date DEFAULT NULL,
  `document_date` datetime DEFAULT NULL,
  `bl_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `murahabah_profit` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_days` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_received` int(11) DEFAULT NULL,
  `container_status` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lc_type` int(11) DEFAULT NULL,
  `delivery_term` int(11) DEFAULT NULL,
  `margin_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `murahabah_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `murahabah_rate` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `margin_rate` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_lcdoler`
--

CREATE TABLE `erp_lcdoler` (
  `id` int(10) UNSIGNED NOT NULL,
  `lc_id` int(11) DEFAULT NULL,
  `rate` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_lcduty`
--

CREATE TABLE `erp_lcduty` (
  `id` int(10) UNSIGNED NOT NULL,
  `ledgerLc_id` int(11) DEFAULT NULL,
  `Comercial_id` int(11) DEFAULT NULL,
  `duties` int(11) DEFAULT NULL,
  `amount` decimal(20,2) DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_ledgers`
--

CREATE TABLE `erp_ledgers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `group_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opening_balance` int(10) UNSIGNED DEFAULT 0,
  `closing_balance` int(10) UNSIGNED DEFAULT 0,
  `balance_type` enum('c','d') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'c',
  `account_type_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `parent_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_common` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_ledgers`
--

INSERT INTO `erp_ledgers` (`id`, `name`, `number`, `code`, `group_id`, `group_number`, `opening_balance`, `closing_balance`, `balance_type`, `account_type_id`, `status`, `branch_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`, `parent_type`, `is_common`) VALUES
(1, 'Dummy', '1-02-002-0001-000001-0001', NULL, 18, '1-02-002-0001-000001', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(2, 'Dummy', '1-02-002-0001-000002-0001', NULL, 19, '1-02-002-0001-000002', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(3, 'Dummy', '1-02-002-0001-000003-0001', NULL, 20, '1-02-002-0001-000003', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(4, 'Dummy', '1-02-002-0001-000004-0001', NULL, 136, '1-02-002-0001-000004', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(5, 'Dummy', '1-02-002-0001-000005-0001', NULL, 137, '1-02-002-0001-000005', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(6, 'Dummy', '1-02-002-0001-000006-0001', NULL, 138, '1-02-002-0001-000006', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(7, 'Dummy', '1-02-002-0001-000007-0001', NULL, 139, '1-02-002-0001-000007', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(8, 'Dummy', '1-02-002-0001-000008-0001', NULL, 140, '1-02-002-0001-000008', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(9, 'Dummy', '1-02-002-0001-000009-0001', NULL, 141, '1-02-002-0001-000009', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(10, 'Dummy', '1-02-002-0001-0010-0001', NULL, 142, '1-02-002-0001-0010', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(11, 'Dummy', '1-02-002-0001-0011-0001', NULL, 143, '1-02-002-0001-0011', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(12, 'Dummy', '1-02-002-0001-0012-0001', NULL, 144, '1-02-002-0001-0012', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(13, 'Dummy', '1-02-002-0001-0013-0001', NULL, 145, '1-02-002-0001-0013', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL),
(14, 'Dummy', '1-02-002-0001-0014-0001', NULL, 146, '1-02-002-0001-0014', 0, 0, 'd', 1, 1, NULL, 1, NULL, NULL, '2021-03-03 13:07:50', '2021-03-03 13:07:50', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_model_has_permissions`
--

CREATE TABLE `erp_model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_model_has_permissions`
--

INSERT INTO `erp_model_has_permissions` (`permission_id`, `model_id`, `model_type`) VALUES
(14, 5, 'App\\User'),
(21, 5, 'App\\User'),
(36, 4, 'App\\User'),
(56, 5, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `erp_model_has_roles`
--

CREATE TABLE `erp_model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_model_has_roles`
--

INSERT INTO `erp_model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\User'),
(2, 10, 'App\\User'),
(3, 2, 'App\\User'),
(4, 3, 'App\\User'),
(5, 4, 'App\\User'),
(5, 13, 'App\\User'),
(6, 5, 'App\\User'),
(6, 20, 'App\\User'),
(7, 6, 'App\\User'),
(8, 7, 'App\\User'),
(9, 8, 'App\\User'),
(10, 9, 'App\\User'),
(11, 11, 'App\\User'),
(11, 12, 'App\\User'),
(11, 14, 'App\\User'),
(11, 72, 'App\\User'),
(11, 91, 'App\\User'),
(11, 94, 'App\\User'),
(11, 95, 'App\\User'),
(11, 97, 'App\\User'),
(12, 17, 'App\\User'),
(12, 21, 'App\\User'),
(13, 15, 'App\\User'),
(13, 18, 'App\\User'),
(13, 19, 'App\\User'),
(13, 39, 'App\\User'),
(13, 56, 'App\\User'),
(13, 71, 'App\\User'),
(13, 73, 'App\\User'),
(13, 74, 'App\\User'),
(13, 77, 'App\\User'),
(13, 98, 'App\\User'),
(16, 16, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `erp_orderdetail`
--

CREATE TABLE `erp_orderdetail` (
  `id` int(10) UNSIGNED NOT NULL,
  `perchas_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `unit_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `remaining_qty` int(11) DEFAULT NULL,
  `unit_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_of_origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_orders`
--

CREATE TABLE `erp_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `po_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `sup_id` int(11) DEFAULT NULL,
  `perfomra_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale_order_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `soruce` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sal_date` date DEFAULT NULL,
  `valid_upto` date DEFAULT NULL,
  `total_order_amt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `payment_terms` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `confirm_status` int(11) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_order_product_serial`
--

CREATE TABLE `erp_order_product_serial` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `installation_date` date DEFAULT NULL,
  `warrenty` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires_at` date DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_password_resets`
--

CREATE TABLE `erp_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_payees_amounts`
--

CREATE TABLE `erp_payees_amounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `building_id` int(10) UNSIGNED DEFAULT NULL,
  `payee_id` int(10) UNSIGNED DEFAULT NULL,
  `payee_amount` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_payee_lists`
--

CREATE TABLE `erp_payee_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `payee_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_payment_methods`
--

CREATE TABLE `erp_payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` tinyint(3) UNSIGNED NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_performastock`
--

CREATE TABLE `erp_performastock` (
  `id` int(10) UNSIGNED NOT NULL,
  `ci_id` int(11) DEFAULT NULL,
  `peroforma_no` int(11) DEFAULT NULL,
  `product_name` int(11) DEFAULT NULL,
  `total_qty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rel_qty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balanace_qty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_rs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost_per_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insurance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_charges` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `freight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profit_investment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `murahba_profit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mvmnt_bndg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Duty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `f_total_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costing_u_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costing_do_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_permissions`
--

CREATE TABLE `erp_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_group` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_permissions`
--

INSERT INTO `erp_permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `title`, `main_group`, `parent_id`, `status`) VALUES
(1, 'erp_roles_manage', 'web', '2021-02-23 05:39:28', '2021-02-23 05:39:28', 'Roles', 1, 0, 1),
(2, 'erp_roles_create', 'web', '2021-02-23 05:39:28', '2021-02-23 05:39:28', 'Create', 0, 1, 1),
(3, 'erp_roles_edit', 'web', '2021-02-23 05:39:28', '2021-02-23 05:39:28', 'Edit', 0, 1, 1),
(4, 'erp_roles_active', 'web', '2021-02-23 05:39:28', '2021-02-23 05:39:28', 'Activate', 0, 1, 1),
(5, 'erp_roles_inactive', 'web', '2021-02-23 05:39:28', '2021-02-23 05:39:28', 'Inactivate', 0, 1, 1),
(6, 'erp_roles_destroy', 'web', '2021-02-23 05:39:28', '2021-02-23 05:39:28', 'Delete', 0, 1, 1),
(7, 'erp_permissions_manage', 'web', '2021-02-23 05:39:29', '2021-02-23 05:39:29', 'Permissions', 1, 0, 1),
(8, 'erp_permissions_create', 'web', '2021-02-23 05:39:29', '2021-02-23 05:39:29', 'Create', 0, 7, 1),
(9, 'erp_permissions_edit', 'web', '2021-02-23 05:39:29', '2021-02-23 05:39:29', 'Edit', 0, 7, 1),
(10, 'erp_permissions_active', 'web', '2021-02-23 05:39:29', '2021-02-23 05:39:29', 'Activate', 0, 7, 1),
(11, 'erp_permissions_inactive', 'web', '2021-02-23 05:39:29', '2021-02-23 05:39:29', 'Inactivate', 0, 7, 1),
(12, 'erp_permissions_destroy', 'web', '2021-02-23 05:39:29', '2021-02-23 05:39:29', 'Delete', 0, 7, 1),
(13, 'erp_permissions_mass_destroy', 'web', '2021-02-23 05:39:29', '2021-02-23 05:39:29', 'Bulk Delete', 0, 7, 1),
(14, 'erp_countries_manage', 'web', '2021-02-23 05:39:30', '2021-02-23 05:39:30', 'Countries', 1, 0, 1),
(15, 'erp_countries_create', 'web', '2021-02-23 05:39:30', '2021-02-23 05:39:30', 'Create', 0, 14, 1),
(16, 'erp_countries_edit', 'web', '2021-02-23 05:39:30', '2021-02-23 05:39:30', 'Edit', 0, 14, 1),
(17, 'erp_countries_destroy', 'web', '2021-02-23 05:39:30', '2021-02-23 05:39:30', 'Delete', 0, 14, 1),
(18, 'erp_countries_detail', 'web', '2021-02-23 05:39:30', '2021-02-23 05:39:30', 'Detail', 0, 14, 1),
(19, 'erp_countries_active', 'web', '2021-02-23 05:39:30', '2021-02-23 05:39:30', 'Activate', 0, 14, 1),
(20, 'erp_countries_inactive', 'web', '2021-02-23 05:39:30', '2021-02-23 05:39:30', 'Inactivate', 0, 14, 1),
(21, 'erp_cities_manage', 'web', '2021-02-23 05:39:32', '2021-02-23 05:39:32', 'Cities', 1, 0, 1),
(22, 'erp_cities_create', 'web', '2021-02-23 05:39:32', '2021-02-23 05:39:32', 'Create', 0, 21, 1),
(23, 'erp_cities_edit', 'web', '2021-02-23 05:39:32', '2021-02-23 05:39:32', 'Edit', 0, 21, 1),
(24, 'erp_cities_destroy', 'web', '2021-02-23 05:39:32', '2021-02-23 05:39:32', 'Delete', 0, 21, 1),
(25, 'erp_cities_detail', 'web', '2021-02-23 05:39:32', '2021-02-23 05:39:32', 'Detail', 0, 21, 1),
(26, 'erp_cities_active', 'web', '2021-02-23 05:39:32', '2021-02-23 05:39:32', 'Activate', 0, 21, 1),
(27, 'erp_cities_inactive', 'web', '2021-02-23 05:39:32', '2021-02-23 05:39:32', 'Inactivate', 0, 21, 1),
(28, 'erp_companies_manage', 'web', '2021-02-23 05:39:32', '2021-02-23 05:39:32', 'Companies', 1, 0, 1),
(29, 'erp_companies_create', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 'Create', 0, 28, 1),
(30, 'erp_companies_edit', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 'Edit', 0, 28, 1),
(31, 'erp_companies_destroy', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 'Delete', 0, 28, 1),
(32, 'erp_companies_detail', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 'Detail', 0, 28, 1),
(33, 'erp_companies_active', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 'Activate', 0, 28, 1),
(34, 'erp_companies_inactive', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 'Inactivate', 0, 28, 1),
(35, 'erp_school_setting', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 'School Setting', 0, 28, 1),
(36, 'erp_hired_hr', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 'Hired HR', 0, 28, 1),
(37, 'erp_users_manage', 'web', '2021-02-23 05:39:35', '2021-02-23 05:39:35', 'Users', 1, 0, 1),
(38, 'erp_users_create', 'web', '2021-02-23 05:39:35', '2021-02-23 05:39:35', 'Create', 0, 37, 1),
(39, 'erp_users_edit', 'web', '2021-02-23 05:39:35', '2021-02-23 05:39:35', 'Edit', 0, 37, 1),
(40, 'erp_users_active', 'web', '2021-02-23 05:39:35', '2021-02-23 05:39:35', 'Activate', 0, 37, 1),
(41, 'erp_users_inactive', 'web', '2021-02-23 05:39:35', '2021-02-23 05:39:35', 'Inactivate', 0, 37, 1),
(42, 'erp_users_destroy', 'web', '2021-02-23 05:39:35', '2021-02-23 05:39:35', 'Delete', 0, 37, 1),
(43, 'erp_users_detail', 'web', '2021-02-23 05:39:35', '2021-02-23 05:39:35', 'Detail', 0, 37, 1),
(44, 'erp_account_types_manage', 'web', '2021-02-23 05:39:39', '2021-02-23 05:39:39', 'Account Types', 1, 0, 1),
(45, 'erp_account_types_create', 'web', '2021-02-23 05:39:39', '2021-02-23 05:39:39', 'Create', 0, 44, 1),
(46, 'erp_account_types_edit', 'web', '2021-02-23 05:39:39', '2021-02-23 05:39:39', 'Edit', 0, 44, 1),
(47, 'erp_account_types_active', 'web', '2021-02-23 05:39:39', '2021-02-23 05:39:39', 'Activate', 0, 44, 1),
(48, 'erp_account_types_inactive', 'web', '2021-02-23 05:39:39', '2021-02-23 05:39:39', 'Inactivate', 0, 44, 1),
(49, 'erp_account_types_destroy', 'web', '2021-02-23 05:39:39', '2021-02-23 05:39:39', 'Delete', 0, 44, 1),
(50, 'erp_account_types_detail', 'web', '2021-02-23 05:39:39', '2021-02-23 05:39:39', 'Detail', 0, 44, 1),
(51, 'erp_branches_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'erp_branches', 1, 0, 1),
(52, 'erp_branches_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Create', 0, 51, 1),
(53, 'erp_branches_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Edit', 0, 51, 1),
(54, 'erp_branches_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Delete', 0, 51, 1),
(55, 'erp_branches_detail', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Detail', 0, 51, 1),
(56, 'erp_users_management_dfit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'User Managment for DFIT', 0, 51, 1),
(57, 'erp_hire_hr', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Hire HR', 0, 51, 1),
(58, 'erp_branchhr_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Manage', 0, 51, 1),
(59, 'erp_hr_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Manage', 0, 51, 1),
(60, 'erp_branchhr_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Create Hire HR', 0, 51, 1),
(61, 'erp_branchhr_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Edit Hire HR', 0, 51, 1),
(62, 'erp_branchhr_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Delete Hire HR', 0, 51, 1),
(63, 'erp_branchhr_inactive', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Inactive Hire HR', 0, 51, 1),
(64, 'erp_branchhr_detail', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Detail Hire HR', 0, 51, 1),
(65, 'erp_branchhr_active', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Hire HR Active', 0, 51, 1),
(66, 'erp_document_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Document Manage', 0, 51, 1),
(67, 'erp_document_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Document Create', 0, 51, 1),
(68, 'erp_document_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Document Edit', 0, 51, 1),
(69, 'erp_document_active', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Document Active', 0, 51, 1),
(70, 'erp_document_inactive', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Document Inactive', 0, 51, 1),
(71, 'erp_document_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Document Destroy', 0, 51, 1),
(72, 'erp_user_staff_profile', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'User Profile', 0, 51, 1),
(73, 'erp_discount_type_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Discount Type Manage', 0, 51, 1),
(74, 'erp_discount_type_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Discount Type Create', 0, 51, 1),
(75, 'erp_discount_type_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Discount Type Edit', 0, 51, 1),
(76, 'erp_discount_type_active', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Discount Type Active', 0, 51, 1),
(77, 'erp_discount_type_inactive', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Discount Type Inactive', 0, 51, 1),
(78, 'erp_discount_type_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Discount Type Destroy', 0, 51, 1),
(79, 'erp_session_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Session Manage', 0, 51, 1),
(80, 'erp_session_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Session Create', 0, 51, 1),
(81, 'erp_session_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Session Edit', 0, 51, 1),
(82, 'erp_session_active', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Session Active', 0, 51, 1),
(83, 'erp_session_inactive', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Session Inactive', 0, 51, 1),
(84, 'erp_session_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Session Destroy', 0, 51, 1),
(85, 'erp_term_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Term Manage', 0, 51, 1),
(86, 'erp_term_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Term Create', 0, 51, 1),
(87, 'erp_term_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Term Edit', 0, 51, 1),
(88, 'erp_term_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Term Destroy', 0, 51, 1),
(89, 'erp_term_active', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Term Active', 0, 51, 1),
(90, 'erp_term_inactive', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Term InActive', 0, 51, 1),
(91, 'erp_time_table_category_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Time Table Category Manage', 0, 51, 1),
(92, 'erp_time_table_category_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Time Table Category Create', 0, 51, 1),
(93, 'erp_time_table_category_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Time Table Category Edit', 0, 51, 1),
(94, 'erp_time_table_category_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Time Table Category Destroy', 0, 51, 1),
(95, 'erp_time_table_category_active', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Time Table Category Active', 0, 51, 1),
(96, 'erp_time_table_category_inactive', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Time Table Category InActive', 0, 51, 1),
(97, 'erp_timetable_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Timetable Manage', 0, 51, 1),
(98, 'erp_timetable_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Timetable Create', 0, 51, 1),
(99, 'erp_timetable_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Timetable Edit', 0, 51, 1),
(100, 'erp_timetable_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Timetable Destroy', 0, 51, 1),
(101, 'erp_timetable_active', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Timetable Active', 0, 51, 1),
(102, 'erp_timetable_inactive', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Timetable InActive', 0, 51, 1),
(103, 'erp_class_time_table_manage', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Class Time Table Manage', 0, 51, 1),
(104, 'erp_class_time_table_create', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Class Time Table Create', 0, 51, 1),
(105, 'erp_class_time_table_edit', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Class Time Table Edit', 0, 51, 1),
(106, 'erp_class_time_table_destroy', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Class Time Table Destroy', 0, 51, 1),
(107, 'erp_class_time_table_active', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Class Time Table Active', 0, 51, 1),
(108, 'erp_class_time_table_inactive', 'web', '2021-02-23 05:39:49', '2021-02-23 05:39:49', 'Class Time Table InActive', 0, 51, 1),
(109, 'erp_financial_years_manage', 'web', '2021-02-23 05:40:03', '2021-02-23 05:40:03', 'Financial Years', 1, 0, 1),
(110, 'erp_financial_years_create', 'web', '2021-02-23 05:40:03', '2021-02-23 05:40:03', 'Create', 0, 109, 1),
(111, 'erp_financial_years_edit', 'web', '2021-02-23 05:40:03', '2021-02-23 05:40:03', 'Edit', 0, 109, 1),
(112, 'erp_financial_years_active', 'web', '2021-02-23 05:40:03', '2021-02-23 05:40:03', 'Activate', 0, 109, 1),
(113, 'erp_financial_years_inactive', 'web', '2021-02-23 05:40:03', '2021-02-23 05:40:03', 'Inactivate', 0, 109, 1),
(114, 'erp_financial_years_destroy', 'web', '2021-02-23 05:40:03', '2021-02-23 05:40:03', 'Delete', 0, 109, 1),
(115, 'erp_financial_years_detail', 'web', '2021-02-23 05:40:03', '2021-02-23 05:40:03', 'Detail', 0, 109, 1),
(116, 'erp_report_manage', 'web', '2021-02-23 05:40:19', '2021-02-23 05:40:19', 'Reports', 1, 0, 1),
(117, 'erp_account_reports_manage', 'web', '2021-02-23 05:40:20', '2021-02-23 05:40:20', 'Account Reports', 0, 116, 1),
(118, 'erp_account_reports_trial_balance', 'web', '2021-02-23 05:40:20', '2021-02-23 05:40:20', 'Trial Balance', 0, 116, 1),
(119, 'erp_account_reports_ledger_statement', 'web', '2021-02-23 05:40:20', '2021-02-23 05:40:20', 'Ledger Statement', 0, 116, 1),
(120, 'erp_account_reports_balance_sheet', 'web', '2021-02-23 05:40:20', '2021-02-23 05:40:20', 'Balance Sheet Report', 0, 116, 1),
(121, 'erp_account_reports_profit_loss', 'web', '2021-02-23 05:40:20', '2021-02-23 05:40:20', 'Profit and Loss Reports', 0, 116, 1),
(122, 'erp_account_reports_expense_summary', 'web', '2021-02-23 05:40:20', '2021-02-23 05:40:20', 'Expense Summary', 0, 116, 1),
(123, 'erp_fee_head_manage', 'web', '2021-02-23 05:40:30', '2021-02-23 05:40:30', 'Fee Head', 1, 0, 1),
(124, 'erp_fee_head_create', 'web', '2021-02-23 05:40:30', '2021-02-23 05:40:30', 'Create', 0, 123, 1),
(125, 'erp_fee_head_edit', 'web', '2021-02-23 05:40:30', '2021-02-23 05:40:30', 'Edit', 0, 123, 1),
(126, 'erp_fee_head_destroy', 'web', '2021-02-23 05:40:30', '2021-02-23 05:40:30', 'Delete', 0, 123, 1),
(127, 'erp_fee_head_detail', 'web', '2021-02-23 05:40:30', '2021-02-23 05:40:30', 'Detail', 0, 123, 1),
(128, 'erp_fee_head_active', 'web', '2021-02-23 05:40:30', '2021-02-23 05:40:30', 'Activate', 0, 123, 1),
(129, 'erp_fee_head_inactive', 'web', '2021-02-23 05:40:30', '2021-02-23 05:40:30', 'Inactivate', 0, 123, 1),
(130, 'erp_currencies_manage', 'web', '2021-02-23 05:40:32', '2021-02-23 05:40:32', 'Currencies', 1, 0, 1),
(131, 'erp_currencies_create', 'web', '2021-02-23 05:40:33', '2021-02-23 05:40:33', 'Create', 0, 130, 1),
(132, 'erp_currencies_edit', 'web', '2021-02-23 05:40:33', '2021-02-23 05:40:33', 'Edit', 0, 130, 1),
(133, 'erp_currencies_destroy', 'web', '2021-02-23 05:40:33', '2021-02-23 05:40:33', 'Delete', 0, 130, 1),
(134, 'erp_currencies_detail', 'web', '2021-02-23 05:40:33', '2021-02-23 05:40:33', 'Detail', 0, 130, 1),
(135, 'erp_currency_activate_inactivate', 'web', '2021-02-23 05:40:33', '2021-02-23 05:40:33', 'Activate/Inactivate', 0, 130, 1),
(136, 'erp_banks_manage', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'erp_banks', 1, 0, 1),
(137, 'erp_banks_create', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'Create', 0, 136, 1),
(138, 'erp_banks_edit', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'Edit', 0, 136, 1),
(139, 'erp_banks_destroy', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'Delete', 0, 136, 1),
(140, 'erp_banks_detail', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'Detail', 0, 136, 1),
(141, 'erp_banks_detail', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'Detail', 0, 136, 1),
(142, 'erp_banks_detail', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'Detail', 0, 136, 1),
(143, 'erp_banks_detail', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'Detail', 0, 136, 1),
(144, 'erp_banks_detail', 'web', '2021-02-23 05:40:34', '2021-02-23 05:40:34', 'Detail', 0, 136, 1),
(145, 'erp_banksdeatil_manage', 'web', '2021-02-23 05:40:36', '2021-02-23 05:40:36', 'erp_banksDetail', 1, 0, 1),
(146, 'erp_banksdeatil_create', 'web', '2021-02-23 05:40:36', '2021-02-23 05:40:36', 'Create', 0, 145, 1),
(147, 'erp_banksdeatil_edit', 'web', '2021-02-23 05:40:36', '2021-02-23 05:40:36', 'Edit', 0, 145, 1),
(148, 'erp_banksdeatil_destroy', 'web', '2021-02-23 05:40:36', '2021-02-23 05:40:36', 'Delete', 0, 145, 1),
(149, 'erp_banksdeatil_detail', 'web', '2021-02-23 05:40:36', '2021-02-23 05:40:36', 'Detail', 0, 145, 1),
(150, 'erp_groups_manage', 'web', '2021-02-23 05:40:38', '2021-02-23 05:40:38', 'Groups', 1, 0, 1),
(151, 'erp_groups_create', 'web', '2021-02-23 05:40:38', '2021-02-23 05:40:38', 'Create', 0, 150, 1),
(152, 'erp_groups_edit', 'web', '2021-02-23 05:40:38', '2021-02-23 05:40:38', 'Edit', 0, 150, 1),
(153, 'erp_groups_destroy', 'web', '2021-02-23 05:40:38', '2021-02-23 05:40:38', 'Delete', 0, 150, 1),
(154, 'erp_groups_mass_destroy', 'web', '2021-02-23 05:40:38', '2021-02-23 05:40:38', 'Bulk Delete', 0, 150, 1),
(155, 'erp_ledgers_manage', 'web', '2021-02-23 05:40:39', '2021-02-23 05:40:39', 'Ledgers', 1, 0, 1),
(156, 'erp_ledgers_create', 'web', '2021-02-23 05:40:39', '2021-02-23 05:40:39', 'Create', 0, 155, 1),
(157, 'erp_ledgers_edit', 'web', '2021-02-23 05:40:39', '2021-02-23 05:40:39', 'Edit', 0, 155, 1),
(158, 'erp_ledgers_destroy', 'web', '2021-02-23 05:40:39', '2021-02-23 05:40:39', 'Delete', 0, 155, 1),
(159, 'erp_ledgers_mass_destroy', 'web', '2021-02-23 05:40:39', '2021-02-23 05:40:39', 'Bulk Delete', 0, 155, 1),
(160, 'erp_entries_manage', 'web', '2021-02-23 05:40:41', '2021-02-23 05:40:41', 'Entries', 1, 0, 1),
(161, 'erp_entries_create', 'web', '2021-02-23 05:40:41', '2021-02-23 05:40:41', 'Create', 0, 160, 1),
(162, 'erp_entries_edit', 'web', '2021-02-23 05:40:41', '2021-02-23 05:40:41', 'Edit', 0, 160, 1),
(163, 'erp_entries_active', 'web', '2021-02-23 05:40:41', '2021-02-23 05:40:41', 'Activate', 0, 160, 1),
(164, 'erp_entries_inactive', 'web', '2021-02-23 05:40:41', '2021-02-23 05:40:41', 'Inactivate', 0, 160, 1),
(165, 'erp_entries_destroy', 'web', '2021-02-23 05:40:41', '2021-02-23 05:40:41', 'Delete', 0, 160, 1),
(166, 'erp_entries_mass_destroy', 'web', '2021-02-23 05:40:41', '2021-02-23 05:40:41', 'Bulk Delete', 0, 160, 1),
(167, 'settings_manage', 'web', '2021-02-23 05:40:43', '2021-02-23 05:40:43', 'Settings', 1, 0, 1),
(168, 'settings_create', 'web', '2021-02-23 05:40:43', '2021-02-23 05:40:43', 'Create', 0, 167, 1),
(169, 'settings_edit', 'web', '2021-02-23 05:40:43', '2021-02-23 05:40:43', 'Edit', 0, 167, 1),
(170, 'settings_destroy', 'web', '2021-02-23 05:40:43', '2021-02-23 05:40:43', 'Delete', 0, 167, 1),
(171, 'erp_recruitment_manage', 'web', '2021-02-23 05:40:45', '2021-02-23 05:40:45', 'Recruitment', 1, 0, 1),
(172, 'erp_job_post_request_create', 'web', '2021-02-23 05:40:45', '2021-02-23 05:40:45', 'Job Post Request Create', 0, 171, 1),
(173, 'erp_job_post_request_edit', 'web', '2021-02-23 05:40:45', '2021-02-23 05:40:45', 'Job Post Request Edit', 0, 171, 1),
(174, 'erp_job_post_create', 'web', '2021-02-23 05:40:45', '2021-02-23 05:40:45', 'Job Post Create', 0, 171, 1),
(175, 'erp_interview_scheduling_create', 'web', '2021-02-23 05:40:45', '2021-02-23 05:40:45', 'Interview Scheduling', 0, 171, 1),
(176, 'erp_interview_rescheduling_create', 'web', '2021-02-23 05:40:45', '2021-02-23 05:40:45', 'Interview ReScheduling', 0, 171, 1),
(177, 'erp_hired_candidate', 'web', '2021-02-23 05:40:45', '2021-02-23 05:40:45', 'Enter Hired Candidate', 0, 171, 1),
(178, 'erp_employees_manage', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Employees', 1, 0, 1),
(179, 'erp_employees_create', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Create', 0, 178, 1),
(180, 'erp_employees_edit', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Edit', 0, 178, 1),
(181, 'erp_employees_active', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Activate', 0, 178, 1),
(182, 'erp_employees_inactive', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Inactivate', 0, 178, 1),
(183, 'erp_employees_destroy', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Delete', 0, 178, 1),
(184, 'erp_employees_mass_destroy', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Bulk Delete', 0, 178, 1),
(185, 'erp_staff_manage', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Staff', 1, 0, 1),
(186, 'erp_staff_create', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Create', 0, 185, 1),
(187, 'erp_staff_edit', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Edit', 0, 185, 1),
(188, 'erp_staff_active', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Activate', 0, 185, 1),
(189, 'erp_staff_inactive', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Inactivate', 0, 185, 1),
(190, 'erp_staff_destroy', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Delete', 0, 185, 1),
(191, 'erp_staff_mass_destroy', 'web', '2021-02-23 05:40:46', '2021-02-23 05:40:46', 'Bulk Delete', 0, 185, 1),
(192, 'erp_academic_years_manage', 'web', '2021-02-23 05:41:08', '2021-02-23 05:41:08', 'Academic Years', 1, 0, 1),
(193, 'erp_academic_years_create', 'web', '2021-02-23 05:41:08', '2021-02-23 05:41:08', 'Create', 0, 192, 1),
(194, 'erp_academic_years_edit', 'web', '2021-02-23 05:41:08', '2021-02-23 05:41:08', 'Edit', 0, 192, 1),
(195, 'erp_academic_years_active', 'web', '2021-02-23 05:41:08', '2021-02-23 05:41:08', 'Activate', 0, 192, 1),
(196, 'erp_academic_years_inactive', 'web', '2021-02-23 05:41:08', '2021-02-23 05:41:08', 'Inactivate', 0, 192, 1),
(197, 'erp_academic_years_destroy', 'web', '2021-02-23 05:41:08', '2021-02-23 05:41:08', 'Delete', 0, 192, 1),
(198, 'erp_academic_years_detail', 'web', '2021-02-23 05:41:08', '2021-02-23 05:41:08', 'Detail', 0, 192, 1),
(199, 'erp_training_manage', 'web', '2021-02-23 05:41:10', '2021-02-23 05:41:10', 'Training', 1, 0, 1),
(200, 'erp_training_create', 'web', '2021-02-23 05:41:10', '2021-02-23 05:41:10', 'Create', 0, 199, 1),
(201, 'erp_training_edit', 'web', '2021-02-23 05:41:11', '2021-02-23 05:41:11', 'Edit', 0, 199, 1),
(202, 'erp_training_destroy', 'web', '2021-02-23 05:41:11', '2021-02-23 05:41:11', 'Delete', 0, 199, 1),
(203, 'erp_training_activate', 'web', '2021-02-23 05:41:12', '2021-02-23 05:41:12', 'Activate', 0, 199, 1),
(204, 'erp_training_Inactivate', 'web', '2021-02-23 05:41:12', '2021-02-23 05:41:12', 'Inactivate', 0, 199, 1),
(205, 'erp_resign_manage', 'web', '2021-02-23 05:41:13', '2021-02-23 05:41:13', 'Resign', 1, 0, 1),
(206, 'erp_resign_destroy', 'web', '2021-02-23 05:41:13', '2021-02-23 05:41:13', 'Delete', 0, 205, 1),
(207, 'erp_resign_activate', 'web', '2021-02-23 05:41:13', '2021-02-23 05:41:13', 'Activate', 0, 205, 1),
(208, 'erp_resign_Inactivate', 'web', '2021-02-23 05:41:14', '2021-02-23 05:41:14', 'Inactivate', 0, 205, 1),
(209, 'erp_group_event_manage', 'web', '2021-02-23 05:41:18', '2021-02-23 05:41:18', 'Group Event', 1, 0, 1),
(210, 'erp_group_event_create', 'web', '2021-02-23 05:41:18', '2021-02-23 05:41:18', 'Create', 0, 209, 1),
(211, 'erp_group_event_edit', 'web', '2021-02-23 05:41:20', '2021-02-23 05:41:20', 'Edit', 0, 209, 1),
(212, 'erp_group_event_destroy', 'web', '2021-02-23 05:41:20', '2021-02-23 05:41:20', 'Delete', 0, 209, 1),
(213, 'erp_group_event_activate', 'web', '2021-02-23 05:41:21', '2021-02-23 05:41:21', 'Activate', 0, 209, 1),
(214, 'erp_group_event_Inactivate', 'web', '2021-02-23 05:41:21', '2021-02-23 05:41:21', 'Inactivate', 0, 209, 1);

-- --------------------------------------------------------

--
-- Table structure for table `erp_permission_role`
--

CREATE TABLE `erp_permission_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_policies`
--

CREATE TABLE `erp_policies` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_policy_id` int(10) UNSIGNED NOT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_policies`
--

INSERT INTO `erp_policies` (`id`, `category_policy_id`, `policy_name`, `policy_description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 5, 'Respect People and Property', 'Students are to be respectful in attitude and manner to people and property.', NULL, NULL, NULL, '2021-02-24 06:33:07', '2021-02-24 06:33:07', NULL),
(6, 7, 'staff ethics', 'This is applicable on all the staff membrs.', NULL, NULL, NULL, '2021-02-24 06:42:51', '2021-02-24 06:42:51', NULL),
(7, 8, 'Apply  for  Absence', 'Parents need to apply for leave of absence for their child from the Vice-Principal to leave or absent themselves from school for urgent private matters', NULL, NULL, NULL, '2021-02-24 06:52:28', '2021-02-24 06:52:28', NULL),
(8, 6, 'Late   Arrivals  will mark  absent', 'late  comings  will   mark    as  absent', NULL, NULL, NULL, '2021-02-26 11:15:26', '2021-02-26 11:15:26', NULL),
(9, 6, '3', 'c', NULL, NULL, NULL, '2021-02-26 11:27:30', '2021-02-26 11:27:30', NULL),
(10, 6, '2', '3', NULL, NULL, NULL, '2021-02-26 11:28:11', '2021-02-26 11:28:11', NULL),
(11, 7, 'Employee  Leave  Req', '3 It is also a best practice to have policies on standards of conduct, drug and alcohol', NULL, NULL, NULL, '2021-02-26 11:33:55', '2021-02-26 11:33:55', NULL),
(12, 13, 'Moral Rules and  Policy', 'Students are to be respectful in attitude and manner to people and property.', NULL, NULL, NULL, '2021-03-01 10:16:29', '2021-03-01 10:16:29', NULL),
(13, 5, 'Unscheduled Absence', 'Students are to attend all academic programmes, co-curricular activities, school functions and other school related programmes.', NULL, NULL, NULL, '2021-03-01 10:19:02', '2021-03-01 10:19:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_ponumber`
--

CREATE TABLE `erp_ponumber` (
  `id` int(10) UNSIGNED NOT NULL,
  `po_no` int(11) NOT NULL,
  `po_date` datetime NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_program`
--

CREATE TABLE `erp_program` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortcode` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_regions`
--

CREATE TABLE `erp_regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_name` int(10) UNSIGNED DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_releasestock`
--

CREATE TABLE `erp_releasestock` (
  `id` int(10) UNSIGNED NOT NULL,
  `lc_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `rel_qty` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `import_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_roles`
--

CREATE TABLE `erp_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_roles`
--

INSERT INTO `erp_roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `status`) VALUES
(1, 'administrator', 'web', '2021-02-23 05:39:28', '2021-02-23 05:39:28', 1),
(2, 'DFIT_head', 'web', '2021-02-23 05:39:33', '2021-02-23 05:39:33', 1),
(3, 'Accountant', 'web', '2021-02-23 05:39:37', '2021-02-23 05:39:37', 1),
(4, 'HR_head', 'web', '2021-02-23 05:40:44', '2021-02-23 05:40:44', 1),
(5, 'HR', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(6, 'DFIT', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(7, 'Principal_head', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(8, 'Principal', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(9, 'QA_head', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(10, 'QA', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(11, 'Teacher', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(12, 'Coordinator', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(13, 'Candidate', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(14, 'Admission', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(15, 'GM Finance', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1),
(16, 'Supporting Staff', 'web', '2021-02-23 05:40:47', '2021-02-23 05:40:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `erp_role_has_permissions`
--

CREATE TABLE `erp_role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_role_has_permissions`
--

INSERT INTO `erp_role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(17, 1),
(17, 2),
(18, 1),
(18, 2),
(19, 1),
(19, 2),
(20, 1),
(20, 2),
(21, 1),
(21, 2),
(22, 1),
(22, 2),
(23, 1),
(23, 2),
(24, 1),
(24, 2),
(25, 1),
(25, 2),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 2),
(37, 1),
(37, 4),
(37, 5),
(38, 1),
(38, 4),
(38, 5),
(39, 1),
(39, 4),
(39, 5),
(40, 1),
(40, 4),
(40, 5),
(41, 1),
(41, 4),
(41, 5),
(42, 1),
(42, 4),
(42, 5),
(43, 1),
(43, 4),
(43, 5),
(44, 1),
(44, 3),
(45, 1),
(45, 3),
(46, 1),
(46, 3),
(47, 1),
(47, 3),
(48, 1),
(48, 3),
(49, 1),
(49, 3),
(50, 1),
(50, 3),
(51, 1),
(51, 2),
(52, 1),
(52, 2),
(53, 1),
(53, 2),
(54, 1),
(54, 2),
(55, 1),
(55, 2),
(57, 4),
(58, 4),
(59, 4),
(59, 5),
(60, 4),
(61, 4),
(62, 4),
(63, 4),
(64, 4),
(65, 4),
(66, 4),
(67, 8),
(67, 11),
(67, 12),
(68, 8),
(68, 11),
(68, 12),
(69, 8),
(69, 11),
(69, 12),
(70, 8),
(70, 11),
(70, 12),
(71, 8),
(71, 11),
(71, 12),
(72, 8),
(72, 11),
(72, 12),
(73, 2),
(74, 2),
(75, 2),
(76, 2),
(77, 2),
(78, 2),
(79, 1),
(79, 2),
(79, 9),
(80, 1),
(80, 2),
(80, 9),
(81, 1),
(81, 2),
(81, 9),
(82, 1),
(82, 2),
(82, 9),
(83, 1),
(83, 2),
(83, 9),
(84, 1),
(84, 2),
(84, 9),
(85, 2),
(86, 2),
(87, 2),
(88, 2),
(89, 2),
(90, 2),
(91, 2),
(92, 2),
(93, 2),
(94, 2),
(95, 2),
(96, 2),
(97, 2),
(98, 2),
(99, 2),
(100, 2),
(101, 2),
(102, 2),
(103, 2),
(104, 2),
(105, 2),
(106, 2),
(107, 2),
(108, 2),
(109, 1),
(109, 3),
(110, 1),
(110, 3),
(111, 1),
(111, 3),
(112, 1),
(112, 3),
(113, 1),
(113, 3),
(114, 1),
(114, 3),
(115, 1),
(115, 3),
(116, 1),
(116, 3),
(117, 1),
(117, 3),
(118, 1),
(118, 3),
(119, 1),
(119, 3),
(120, 1),
(120, 3),
(121, 1),
(121, 3),
(122, 1),
(122, 3),
(123, 1),
(123, 3),
(124, 1),
(124, 3),
(125, 1),
(125, 3),
(126, 1),
(126, 3),
(127, 1),
(127, 3),
(128, 1),
(128, 3),
(129, 1),
(129, 3),
(130, 1),
(130, 3),
(131, 1),
(131, 3),
(132, 1),
(132, 3),
(133, 1),
(133, 3),
(134, 1),
(134, 3),
(135, 1),
(135, 3),
(136, 1),
(136, 3),
(137, 1),
(137, 3),
(138, 1),
(138, 3),
(139, 1),
(139, 3),
(140, 1),
(140, 3),
(145, 1),
(145, 3),
(146, 1),
(146, 3),
(147, 1),
(147, 3),
(148, 1),
(148, 3),
(149, 1),
(149, 3),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(155, 3),
(156, 1),
(156, 3),
(157, 1),
(157, 3),
(158, 1),
(158, 3),
(159, 1),
(159, 3),
(160, 1),
(160, 3),
(161, 1),
(161, 3),
(162, 1),
(162, 3),
(163, 1),
(163, 3),
(164, 1),
(164, 3),
(165, 1),
(165, 3),
(166, 1),
(166, 3),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(171, 4),
(171, 5),
(172, 1),
(172, 4),
(172, 5),
(173, 1),
(173, 4),
(173, 5),
(174, 1),
(174, 4),
(175, 1),
(175, 4),
(176, 1),
(176, 4),
(177, 4),
(178, 4),
(179, 4),
(180, 4),
(181, 4),
(182, 4),
(183, 4),
(184, 4),
(185, 4),
(186, 4),
(187, 4),
(188, 4),
(189, 4),
(190, 4),
(191, 4),
(192, 1),
(192, 2),
(193, 1),
(193, 2),
(194, 1),
(194, 2),
(195, 1),
(195, 2),
(196, 1),
(196, 2),
(197, 1),
(197, 2),
(198, 1),
(198, 2),
(199, 4),
(199, 5),
(200, 4),
(200, 5),
(201, 4),
(201, 5),
(202, 4),
(202, 5),
(203, 4),
(203, 5),
(204, 4),
(204, 5),
(205, 4),
(205, 5),
(206, 4),
(206, 5),
(207, 4),
(207, 5),
(208, 4),
(208, 5),
(209, 9),
(210, 9),
(211, 9),
(212, 9),
(213, 9),
(214, 9);

-- --------------------------------------------------------

--
-- Table structure for table `erp_sales`
--

CREATE TABLE `erp_sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `sal_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sal_date` datetime NOT NULL,
  `valid_upto` datetime NOT NULL,
  `customer_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `tax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `final_amount` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `total_balance` int(11) NOT NULL,
  `gst` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_charges` int(11) NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_salesdetial`
--

CREATE TABLE `erp_salesdetial` (
  `id` int(10) UNSIGNED NOT NULL,
  `sal_id` int(11) NOT NULL,
  `sal_st_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sal_st_unit_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale_quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_salesinvoice`
--

CREATE TABLE `erp_salesinvoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_to` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_dept` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quote_id` int(10) UNSIGNED DEFAULT NULL,
  `sales_person` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'taxable',
  `invoice_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `installation_date` date DEFAULT NULL,
  `warrenty` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires_at` date DEFAULT NULL,
  `rr_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_type` smallint(6) DEFAULT NULL,
  `services_amount` bigint(20) DEFAULT NULL,
  `products_amount` bigint(20) DEFAULT NULL,
  `services_tax` bigint(20) DEFAULT NULL,
  `products_tax` bigint(20) DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `tax_amount` bigint(20) DEFAULT NULL,
  `total_amount` bigint(20) DEFAULT NULL,
  `remaining_amount` bigint(20) DEFAULT NULL,
  `paid_amount` bigint(20) DEFAULT NULL,
  `discount` bigint(20) DEFAULT NULL,
  `payable_amount` bigint(20) DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `confirm_status` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `is_disc` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `disc_value` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `disc_type` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `disc_amount` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `total_after_disc` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `is_taxable` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `tax_percent` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `is_service` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `service_value` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `service_type` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `service_tax_amount` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `misc_amount` int(10) UNSIGNED DEFAULT NULL,
  `total_discount` int(10) UNSIGNED DEFAULT NULL,
  `total_tax` int(10) UNSIGNED DEFAULT NULL,
  `service_tax` int(10) UNSIGNED DEFAULT NULL,
  `total_after_service_tax` int(10) UNSIGNED DEFAULT NULL,
  `gross_total` int(10) UNSIGNED DEFAULT NULL,
  `net_income` int(10) UNSIGNED DEFAULT NULL,
  `overall_discount` int(10) UNSIGNED DEFAULT NULL,
  `service_amount` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sale_return`
--

CREATE TABLE `erp_sale_return` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `return_date` date DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sale_return_detail`
--

CREATE TABLE `erp_sale_return_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `sale_return_id` int(10) UNSIGNED NOT NULL,
  `original_qty` int(10) UNSIGNED NOT NULL,
  `return_qty` int(10) UNSIGNED NOT NULL,
  `return_serial` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_settings`
--

CREATE TABLE `erp_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_settings`
--

INSERT INTO `erp_settings` (`id`, `name`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Currency Symbol', 'PKR', 1, 1, 1, NULL, '2021-02-23 05:40:44', '2021-02-23 05:40:44', NULL),
(2, 'Currency Format', '###,###.##', 1, 1, 1, NULL, '2021-02-23 05:40:44', '2021-02-23 05:40:44', NULL),
(3, 'Date Format', 'd-M-Y|dd-M-yy', 1, 1, 1, NULL, '2021-02-23 05:40:44', '2021-02-23 05:40:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_stockitems`
--

CREATE TABLE `erp_stockitems` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `st_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_unit_price` int(11) DEFAULT NULL,
  `opening_stock` int(11) DEFAULT NULL,
  `purchase_stock` int(11) DEFAULT NULL,
  `closing_stock` int(11) DEFAULT NULL,
  `moments_stock` int(11) DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_suppliers`
--

CREATE TABLE `erp_suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `sup_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sup_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sup_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sup_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sup_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sup_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sup_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sup_website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sup_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sup_bank_account` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_tax_settings`
--

CREATE TABLE `erp_tax_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_class` int(10) UNSIGNED NOT NULL,
  `tax_type` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `fix_amount` double(12,2) DEFAULT 0.00,
  `tax_percent` double(12,2) DEFAULT 0.00,
  `start_range` double(12,2) DEFAULT 0.00,
  `end_range` double(12,2) DEFAULT 0.00,
  `year` int(10) UNSIGNED DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_territory`
--

CREATE TABLE `erp_territory` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_name` int(10) UNSIGNED DEFAULT NULL,
  `region_name` int(10) UNSIGNED DEFAULT NULL,
  `country_name` int(10) UNSIGNED DEFAULT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_users`
--

CREATE TABLE `erp_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `hook_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lockout_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `erp_users`
--

INSERT INTO `erp_users` (`id`, `branch_id`, `role_id`, `hook_id`, `name`, `email`, `password`, `remember_token`, `last_login_at`, `last_login_ip`, `lockout_time`, `active`, `created_at`, `updated_at`) VALUES
(1, 4, 1, NULL, 'Admin', 'admin@rootsivy.edu.pk', '$2y$10$sIn6v4Pex.hsKmGnOPGJhul4koAQ51ZVJlbza9HyKehD9Dl../6lS', 'gnGDX0m94D5ArYQFtayqykEPO5JaQRTj36xNnX3vwEfO1q06PsMZ3VElC9Ga', '2021-03-04 09:41:03', '127.0.0.1', NULL, 1, '2021-02-23 05:39:35', '2021-03-04 04:41:03'),
(2, 4, NULL, NULL, 'Accountant', 'accountant@rootsivy.edu.pk', '$2y$10$CMgcqMPg.A6Fb/lYDBkDqe0YIziwhzPFYbUj9kDWLxb85LXF1UBEa', 'vixuiRXHVTyzxupEsCqteTeC3Hip5Jf6LHtZPj7kvnTQs6pQatKgN02WSwzR', NULL, NULL, NULL, 1, '2021-02-23 05:39:37', '2021-02-23 05:39:37'),
(3, 4, NULL, NULL, 'HR head', 'hr_head@rootsivy.edu.pk', '$2y$10$xHoEcVlFBQsD.E..b.6xRep2dFQjaE2CjGqIktrxLfkFxtpxVzuuq', 'KTURvum2Tnq4ixJbmghcmK6f6WS7y2hDtrRhYEyW9SMvrpzT93dpU5emyvpk', '2021-03-02 10:56:16', '202.142.153.118', NULL, 1, '2021-02-23 05:40:44', '2021-03-02 05:56:16'),
(5, 4, NULL, NULL, 'DFIT', 'dfit@rootsivy.edu.pk', '$2y$10$QZ/Qkhb8o1/a1Y7tODIve.qTVZA1kPqCfJmXmJ8OXAZ8zjYp.yTMy', NULL, NULL, NULL, NULL, 1, '2021-02-23 05:40:49', '2021-02-23 05:40:49'),
(6, 4, NULL, NULL, 'Principal head', 'principal_head@rootsivy.edu.pk', '$2y$10$PdyXyIgZFs5FxySXbYmXdOW5UD0fXUeqbVMy7HEgG3oZ8jbejBdtW', NULL, NULL, NULL, NULL, 1, '2021-02-23 05:40:49', '2021-02-23 05:40:49'),
(7, 4, NULL, NULL, 'Principal', 'principal@rootsivy.edu.pk', '$2y$10$RNZkk6nUodsYrZtEOD7Veez9P3ybEdOWXF9rbM.BzNQWiOwSmR5mS', NULL, NULL, NULL, NULL, 1, '2021-02-23 05:40:49', '2021-02-23 05:40:49'),
(9, 4, NULL, NULL, 'QA', 'qa@rootsivy.edu.pk', '$2y$10$goVYq8dJf.l49E28XuPPle6OlRlQsQxmc6rPX6DKmcF744lP8J1BW', NULL, NULL, NULL, NULL, 1, '2021-02-23 05:40:49', '2021-02-23 05:40:49'),
(10, 4, NULL, NULL, 'DFIT head', 'dfit_head@rootsivy.edu.pk', '$2y$10$2Ljzyrt83xtYL4Givmdyf.FAJF0Uk0/m9MZNtPnX6mwZqc8YpeyM.', 'PxEzVbT7Rtme4rmibloI2Ye8CAd6Yl7E3dhmiebO18frZv525rsn8cmOvl74', '2021-03-01 11:08:00', '202.142.153.118', NULL, 1, '2021-02-23 05:40:50', '2021-03-01 06:08:00'),
(11, 15, NULL, NULL, 'Moahmmed', 'mahmed@gmail.com', '$2y$10$l2tzne8mZ9pg7IfBazuZNOc6xAXPOVUuLd9cvHsrU8lNOMZA4wII6', '7hkBP8TRnqOXohjGD3TrYdta7iZenaNjjJ0b26998y4PF1wwm7anXNIaQ2ov', NULL, NULL, NULL, 0, '2021-02-23 06:58:44', '2021-02-26 10:48:42'),
(12, 7, NULL, NULL, 'Ali', 'xyz@gmail.com', '$2y$10$ytAPVsZPAlCgpGoJbOvsgeTf39gAXXHk7cca/Cge/RXc0I2IcNhyq', 'Zv4uRE8qc3bxtGwfrMBOOCqCbpqzNFKQopveWlZahWb2baWXf2H1MGfaWVsz', NULL, NULL, NULL, 1, '2021-02-23 07:39:38', '2021-02-26 10:47:28'),
(13, 3, NULL, NULL, 'Zack William', 'zack.william@gmail.com', '$2y$10$73K.2.Bt6efZJUugs2zfyOxl75wjjyaYuc8T/CD7n7Wl3ueJaadKu', NULL, NULL, NULL, NULL, 1, '2021-02-23 07:47:57', '2021-02-23 07:47:57'),
(14, 3, NULL, NULL, 'Mohammed', 'uzair1@gmail.com', '$2y$10$iZw6T.JivgFL.94uPpruLeYHZU2kp1hrTMBTfSNgRLZnYmq/uvRdi', 'MZVpvXLQRDKtGHpQXuIgMHCh663sY7ERYPhwQXuqZNdAzEEcLSUtZfPuy1n9', NULL, NULL, NULL, 1, '2021-02-23 08:02:42', '2021-02-26 10:47:10'),
(15, NULL, NULL, NULL, 'Ayiza Ahmed', 'ayiza001@gmail.com', '$2y$10$JrMWDC2Khc6ptOIZQnMjk.9DXQl1C0GTG7ljDC/v0XdS3/J21oGUu', 'm5eGKS7bVP587ziEJxFyqLHaJ9pJ2QrvZvNXEAlyPhJip7qlKBglN86DFNGJ', NULL, NULL, NULL, 1, '2021-02-23 10:45:06', '2021-02-23 10:45:06'),
(16, 2, NULL, NULL, 'Muazna', 'muazna@gmail.com', '$2y$10$j7tdwfppllBD4CeV8sEG4OwE34b/DFqBHz8BdtWFLWmAWeksHsHAu', 'Berm5bkd3MmmxbHrjpTdrDkTPsrjiJkvGSrmfzH9TIhWUGclorlJBXN5uHuM', NULL, NULL, NULL, 1, '2021-02-24 05:48:25', '2021-02-26 10:47:19'),
(17, 2, NULL, NULL, 'Mohammed', 'uzair101@gmail.com', '$2y$10$.RwPghplsBGPgCYBJwOBwOLxi6iQ25Ob2/Vdp7yASM85ggyCkn9OO', 'Ai7anW1EB4IroWakEt3WwEbua2VRyZ0FYAxFmmRJLZd8uy6wMME1YdenEuZT', '2021-03-02 12:43:12', '119.160.65.139', NULL, 1, '2021-02-25 09:20:48', '2021-03-02 07:43:12'),
(18, NULL, NULL, NULL, 'M  Aslam', 'aslam@gmail.com', '$2y$10$5U4VxNthL6iXWJvbp6mVVuZnn7sRYvSAGTqUXcOiW8d/.7lXVUgwG', NULL, NULL, NULL, NULL, 1, '2021-02-25 10:00:22', '2021-02-25 10:00:22'),
(19, 3, NULL, NULL, 'M', 'ali111@gmail.com', '$2y$10$vpYvSCTumP9oS3SI8x/n5uGxIRlr.H5ZmBKpX6CKK17fgpx2X2Zwq', 'VLImwp8RaYmaIy9VsDWtDYe39NC8ynO1On4B3s0OiHxP5e2HF2uezu98RlmZ', NULL, NULL, NULL, 1, '2021-02-25 12:30:41', '2021-02-26 10:47:14'),
(20, 1, NULL, NULL, 'Mohammed', 'umairahmed@gmail.com', '$2y$10$KRxZwqXBC.349/dS/odCyuTbxcH7ruEq5yjjH9znUCbfKd.M4DI.q', NULL, NULL, NULL, NULL, 0, '2021-02-26 05:23:04', '2021-02-26 10:48:58'),
(21, 3, NULL, NULL, 'AYIZA', 'ayiza@gmail.com', '$2y$10$3uUXC2wFw5o.Ntgh8fNar.5AKZdMkob1we9QH4hMS4g/taXMPtWjq', 'xI8IaqD4JRH8vIQrcQnAuf0F2Y5SDehgvF0YgC0hSBpoINk5f5qKghtEwbpv', NULL, NULL, NULL, 1, '2021-02-26 12:35:02', '2021-02-26 12:40:33'),
(22, NULL, NULL, NULL, 'JUNAID', 'junaid110@gmail.com', '$2y$10$AR.l.rVtK617KpBKFBn6gOEf.CkQZE.cLmGHzhs6ALOBYV13m0yn2', NULL, NULL, NULL, NULL, 0, '2021-02-26 12:48:12', '2021-02-26 12:48:12'),
(24, NULL, NULL, NULL, 'WALEED', 'student1001@gmail.com', '$2y$10$.5G6Z0JRmJnV7lKEioR3meJw97a8JtkUVmsf3t5ZRS8roDNGWZiby', NULL, NULL, NULL, NULL, 0, '2021-02-26 12:57:33', '2021-02-26 12:57:33'),
(25, NULL, NULL, 24, 'DEMO', 'demo1001@gmail.com', '$2y$10$KzReVN/yqIuKDpz3gFCvFeqOeF5Kc2.Rt0LVYe.2OGN1dkstMzWWy', NULL, NULL, NULL, NULL, 0, '2021-02-26 12:57:33', '2021-02-26 12:57:33'),
(26, NULL, NULL, NULL, 'ALI', 'ali1001@gmail.com', '$2y$10$zJkNT0rL1LnHtDXda2yvKO2QlbW3TLQ05pwsE3WpPEavO8pPvR4g6', NULL, NULL, NULL, NULL, 0, '2021-02-26 13:14:32', '2021-02-26 13:14:32'),
(27, NULL, NULL, 26, 'DEMO', 'demo1011@gmail.com', '$2y$10$ZWUvHa0EIp3K0tqofEHe/OplaKpcWHwOdHE/fJBQ1sC4DqU3m4g8S', NULL, NULL, NULL, NULL, 0, '2021-02-26 13:14:32', '2021-02-26 13:14:32'),
(28, NULL, NULL, NULL, 'DEMO', 'demo111@admin.com', '$2y$10$01RQ1qTxJBywQhbg44Ohx.4S2KTQMZiJX.118fvQYFTkt.z6.PTnK', NULL, NULL, NULL, NULL, 0, '2021-02-26 13:21:43', '2021-02-26 13:21:43'),
(29, NULL, NULL, 28, 'QWERTY', 'qwerty00001@gmail.com', '$2y$10$0Z/OBq13Dbe/1r/W9.KYlOoSJGl.Twk9PoLVAY8L3FDecqPIqKh.m', NULL, NULL, NULL, NULL, 0, '2021-02-26 13:21:43', '2021-02-26 13:21:43'),
(30, NULL, NULL, NULL, 'DEMO1', 'admin1111@rootsivy.edu.pk', '$2y$10$PK6UJdQ4JeVf55TL0ZR2A.Iz.czClyu18e.4conTPUh/qlggqhEBa', NULL, NULL, NULL, NULL, 0, '2021-02-26 13:31:29', '2021-02-26 13:31:29'),
(31, NULL, NULL, 30, 'DEMO11', 'father11@gmail.com', '$2y$10$Zi1Wkml4hptTp4JaDOdf4u8suv2xqtCz.TWf3NZ.A8AfIJNQtj0Vu', NULL, NULL, NULL, NULL, 0, '2021-02-26 13:31:29', '2021-02-26 13:31:29'),
(33, NULL, NULL, NULL, 'WALEED2', 'waleed2@gmail.com', '$2y$10$uLCo8ECGupPSezCLiqeT9.m797ZP68qknfB9OmDVcY7aVAcxS1sL6', NULL, '2021-03-01 14:44:05', '202.142.153.118', NULL, 0, '2021-02-26 13:44:43', '2021-03-01 09:44:05'),
(34, NULL, NULL, 33, 'SDASD', 'asd1001@gmail.com', '$2y$10$EgjZ.HjoDnqKO6EcN/wU1eW/YISoMe50QRX/rq65bwklsAaqg9ivi', NULL, NULL, NULL, NULL, 0, '2021-02-26 13:44:43', '2021-02-26 13:44:43'),
(36, NULL, NULL, NULL, 'DEMO', 'asad1001@rootsivy.edu.pk', '$2y$10$FZBEPPSGKih0NaQVNkgunuTLgglrwys.Uzwdon4TSDpHx/bcF.pJ6', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:06:02', '2021-02-28 10:06:02'),
(37, NULL, NULL, NULL, 'WALEED10', 'waleeed1@rootsivy.edu.pk', '$2y$10$fldM8HTAYwj01N.f55jn2eEFIUwdiSZ7kfslolYdJGxXUAiJNxkAC', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:16:59', '2021-02-28 10:16:59'),
(38, NULL, NULL, 37, 'DEMO', 'mother100000@gmail.com', '$2y$10$KPMdSXvXjK41Qsg0FT3KUu2.iDeRvbf3isJTk/A35r6Xyebatk25.', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:16:59', '2021-02-28 10:16:59'),
(39, NULL, NULL, NULL, 'demo', 'hr_head@rootsivy.edu.pkee', '$2y$10$1Wm3Y2UR1f9.FR9y14Qf5u1F1WvC8gh4pwXpDZkwjTO5nxJBFKav.', NULL, NULL, NULL, NULL, 1, '2021-02-28 10:22:19', '2021-02-28 10:22:19'),
(40, NULL, NULL, NULL, 'DEMO100', 'admin1001@rootsivy.edu.pk', '$2y$10$NY2xxNW7N0VJT2rDBsU67.nslw0otAmzFCU4PZvdOhXmxXqZyd2my', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:25:33', '2021-02-28 10:25:33'),
(41, NULL, NULL, 40, 'DEMO', 'other@gmail.com', '$2y$10$85lU55qIKlOFvpa/2ANAqum11JoBmaG2UsVrlGTsI/2l88pNxbmbO', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:25:33', '2021-02-28 10:25:33'),
(42, NULL, NULL, NULL, 'DEMO00', 'studentnew@rootsivy.edu.pk', '$2y$10$sbNOKRJ8DsAILJB8ZMdhROO3EY25nunr42jwNDT9AFTbC3qhMcgN6', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:35:31', '2021-02-28 10:35:31'),
(43, NULL, NULL, 42, 'HINA', 'mother000000@gmail.com', '$2y$10$DPCly.ePOjg.IrUjJUko0eGSWJel5EUY.DF/JpfnTsgc3Tjoi5Bxq', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:35:31', '2021-02-28 10:35:31'),
(44, NULL, NULL, NULL, 'DEMO090', 'asdfgh@rootsivy.edu.pk', '$2y$10$.qDIbVrtNUjTe0Rqd2qmzu/77Vt3Nx1w5ghiFrni33CGguPKIvXQW', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:38:39', '2021-02-28 10:38:39'),
(45, NULL, NULL, 44, 'DEMO', 'mother11998@gmail.com', '$2y$10$zvKxs8Irg9fpeFSzG.esku8/5.NzULIfbNNZCEb0Kbt3UjonQLHyG', NULL, NULL, NULL, NULL, 0, '2021-02-28 10:38:39', '2021-02-28 10:38:39'),
(46, NULL, NULL, NULL, 'JJ', 'VDV@rootsivy.edu.pk', '$2y$10$ijCWPAXYxjvoJ.5G0vgnY.urFeKcO7ZyrGNat8M7FBTZw5yaWuvam', NULL, NULL, NULL, NULL, 0, '2021-02-28 11:05:03', '2021-02-28 11:05:03'),
(47, NULL, NULL, 46, 'DEMO', 'mother001@gmail.com', '$2y$10$splF0By3L52Z83c6sHjrA.vH0UxHU9Uqi/jPLw7ikI6jGLqTMu3i2', NULL, NULL, NULL, NULL, 0, '2021-02-28 11:05:03', '2021-02-28 11:05:03'),
(48, NULL, NULL, NULL, 'KKK1', 'KK1@gmail.com', '$2y$10$7tTgYsX24hrxxUeMOXGeAe1koZLd3jmHVGHnkrSy1q3Vf9LjUefp6', NULL, NULL, NULL, NULL, 0, '2021-02-28 11:19:32', '2021-02-28 11:19:32'),
(49, NULL, NULL, 48, 'DEMO', 'kk3@gmail.com', '$2y$10$uzP/MtonZryp2PoopB3noeNd.g0d4bzEgbo6LXxfGbDnaH6NgxGge', NULL, NULL, NULL, NULL, 0, '2021-02-28 11:19:32', '2021-02-28 11:19:32'),
(50, NULL, NULL, NULL, 'MOIZA', 'moiza@gmail.com', '$2y$10$efF2c0yEP/cIMr6yd9opQ.lQvC0SavDalGzAB1wWIzgWeFqZ7i/u2', NULL, NULL, NULL, NULL, 0, '2021-02-28 16:55:37', '2021-02-28 16:55:37'),
(52, NULL, NULL, NULL, 'FATIMA', 'fatima@gmail.com', '$2y$10$HELDhQXPEqpWSUoCPuL.4.QOGiMJ1d3ThMUQhuFZpaX0v1M044fha', NULL, NULL, NULL, NULL, 0, '2021-02-28 17:35:32', '2021-02-28 17:35:32'),
(53, NULL, NULL, 52, 'MOHAMMED', 'MASIF@GMAIL.COM', '$2y$10$4kNrDQCWnRn4VYptH06mn.xnNcH4tFdK1YR6g4eXw64mXA9vyO2W.', NULL, NULL, NULL, NULL, 0, '2021-02-28 17:35:32', '2021-02-28 17:35:32'),
(54, NULL, NULL, NULL, 'AREEJ', 'fatima101@gmail.com', '$2y$10$0n7pwiqKtvR2mVK6avcWKeGL7lnGm8OaaWtX4e8ePfEvnHKyEu9J6', NULL, '2021-02-28 23:38:57', '119.160.103.98', NULL, 0, '2021-02-28 18:20:42', '2021-02-28 18:38:57'),
(55, NULL, NULL, 54, 'M', 'MASHRAF@GMAI.COM', '$2y$10$IPbOIgqdEqC4rKrBwxG0leguuLtcPDx96jhXaqFv46IBZrhfxTjNu', NULL, NULL, NULL, NULL, 0, '2021-02-28 18:20:42', '2021-02-28 18:20:42'),
(56, 2, NULL, NULL, 'ZARA', 'zara101@gmail.com', '$2y$10$STDxFT17s5ZFpKOBIIm5e.I0zUmmPBQXyXtnUUY45xgDLS2JB3dpi', NULL, '2021-03-01 14:47:34', '202.142.153.118', NULL, 1, '2021-02-28 18:47:26', '2021-03-01 09:47:34'),
(57, NULL, NULL, NULL, 'DEMO202', 'zain001@netroots.com', '$2y$10$HIrT1zgyC.ec78pj4ns6ae036S5YBkVjXWS3JWe5EzbKTJ7vbuFPi', NULL, NULL, NULL, NULL, 0, '2021-03-01 05:13:11', '2021-03-01 05:13:11'),
(58, NULL, NULL, 57, 'HINA', 'father0900@gmail.com', '$2y$10$B0o4vNfKenup3Ha.CZIDZeLxvS66/hEh.paR6KgO7a6./C8cNpj7q', NULL, '2021-03-01 10:16:07', '202.142.153.118', NULL, 0, '2021-03-01 05:13:11', '2021-03-01 05:16:07'),
(59, NULL, NULL, NULL, 'AYESHA', 'fatima1994@gmail.com', '$2y$10$VxvzRxzdytW5fqfdjsHQRODzkoG0XAgLLPMz3Kx9EyUT6vVFsDWu2', NULL, NULL, NULL, NULL, 0, '2021-03-01 05:25:22', '2021-03-01 05:25:22'),
(60, NULL, NULL, 59, 'MOHAMMED', 'ahmed1119@gmail.com', '$2y$10$YMXCUZu0uHdvcB9rdF..feCeaFA1Qsaht7IAXb8xAdumAhTjznIcu', NULL, NULL, NULL, NULL, 0, '2021-03-01 05:25:22', '2021-03-01 05:25:22'),
(61, NULL, NULL, NULL, 'MOHAMMED', 'masif1994@gmail.com', '$2y$10$u7iZxBWFU/56rC8MGrUNMuvBVfKUwX6DpqZQ4CRj3TaC73BH3hAvu', NULL, NULL, NULL, NULL, 0, '2021-03-01 05:36:47', '2021-03-01 05:36:47'),
(62, NULL, NULL, 61, 'MOHAMMED', 'ashraf1998@gamil.com', '$2y$10$V/FFV7lU/gq/4ct.tssANuQtFt//I8QrgNutvPl.nxea71DtZBIQ6', NULL, NULL, NULL, NULL, 0, '2021-03-01 05:36:47', '2021-03-01 05:36:47'),
(63, NULL, NULL, NULL, 'MUAZNA', 'muazna1999@gmail.com', '$2y$10$2djSoFgdh0wNWRXqB9t38O5U1AWAbz6ASlMRHqBylKq6wpzHog3Um', 'aktW1DKDV6SuWqgJIyhMNb6Vuki9rRQI2iwZ3gxTr2H4YEPGC9haT1huLip4', '2021-03-01 10:46:50', '202.142.153.118', NULL, 0, '2021-03-01 05:45:16', '2021-03-01 05:46:50'),
(64, NULL, NULL, 63, 'AHMED', 'axyz@gmail.com', '$2y$10$sesX2cTyQcoISdD5Ks/MZ.zNUIrTf2KXCUQflGZs0SFlXOF.tAqqm', NULL, NULL, NULL, NULL, 0, '2021-03-01 05:45:16', '2021-03-01 05:45:16'),
(65, NULL, NULL, NULL, 'ABRAR', 'ibrar@gmail.com', '$2y$10$HFsiXV9zZizak9RmowtQ3O.GaAtTSpXwEMORBQHeCombvzGttBzL2', NULL, NULL, NULL, NULL, 0, '2021-03-01 05:53:22', '2021-03-01 05:53:22'),
(66, NULL, NULL, 65, 'HINA', 'guardians1@gmail.com', '$2y$10$O1HChrg4vAVPM0qS8J7NUOxAwRAmReDmPWXGu3ilfrenIY37nidW6', NULL, '2021-03-01 10:53:39', '202.142.153.118', NULL, 0, '2021-03-01 05:53:22', '2021-03-01 05:53:39'),
(67, NULL, NULL, NULL, 'WALEED11', 'waleeed11@gmail.com', '$2y$10$oaaDxX2KXo49O4v0Vrdv0u0hhBhwDuEkjDarpb3bpLeG0NbMckuNy', NULL, NULL, NULL, NULL, 0, '2021-03-01 06:12:13', '2021-03-01 06:12:13'),
(68, NULL, NULL, 67, 'HINA1', 'guardians100@gmail.com', '$2y$10$E8GiShbVuSYmHndeWyveFeHIsz7oV6OltSjLtJ9DbJMvbpNcg4eEe', NULL, NULL, NULL, NULL, 0, '2021-03-01 06:12:13', '2021-03-01 06:12:13'),
(69, NULL, NULL, NULL, 'BBB', 'bb@gmail.com', '$2y$10$66ZcUJhJimu02IOjZlMgde.hyQOhfSXGcC41iGDsUuUUOrY0sg10u', NULL, NULL, NULL, NULL, 0, '2021-03-01 06:19:06', '2021-03-01 06:19:06'),
(70, NULL, NULL, 69, 'BRENNAN', 'guardians2@gmail.com', '$2y$10$mOzrG99eKqNKbZYRcx/fT.LwE3fdFMzPsQD9dZ/HXpHwU6lr4XBHG', NULL, NULL, NULL, NULL, 0, '2021-03-01 06:19:06', '2021-03-01 06:19:06'),
(71, NULL, NULL, NULL, 'Fizza', 'fizza@gmail.com', '$2y$10$fkfpIlaa63ods72HbfKHWuxf3Yy39c1xx8P7n6CwD76qLfJ3j0nES', 'jjKquDoIHJD35Mgu4EhVjLV2ZzhPar1FhM5hrDXBitSmRZyT8w6jl4DgTilS', NULL, NULL, NULL, 1, '2021-03-01 07:03:55', '2021-03-01 07:03:55'),
(72, 1, NULL, NULL, '12321313', 'shahrukhlodhi5@gmail.com', '$2y$10$HKuDw0IJTkkvJELUVoLKYOdYTzTc/u/eidSFHUdDxSmXCTlZA49Bi', 'L8HuynFo2mSGo4Y5vhgLDYPsnTnmubEd8uqZyGGhcOpdv4eSOrxbkiPeujhC', '2021-03-01 16:29:02', '202.142.153.118', NULL, 1, '2021-03-01 08:07:51', '2021-03-01 11:29:02'),
(73, NULL, NULL, NULL, 'AHMED KHAN', 'ahmed1999@gmail.com', '$2y$10$pHxfrPcd5V9o2p.G0BOWhODxDw0AZvVTJ0tLbmb.CUy.erdQ1nRI2', 'X60MKXvf3xxcKRj4CicNXmCXjYgDBhZsZexVMcEUiWBnHS8v8bFZpl8IHyYC', NULL, NULL, NULL, 1, '2021-03-01 09:20:16', '2021-03-01 09:20:16'),
(74, NULL, NULL, NULL, 'Zarwah khan', 'zarwah188@gmail.com', '$2y$10$7y8S1KXRaGCD3F0PTxO2X.EGLlScACFFrDOU5QRSfz3QFzifIf/bW', NULL, NULL, NULL, NULL, 1, '2021-03-01 09:32:03', '2021-03-01 09:32:03'),
(75, NULL, NULL, NULL, 'SARA', 'saraahmed@gmail.com', '$2y$10$blcHJTruQysYvXjnCQr2AemzWr/8EYHImPM3zrrS/jdEtbH1lSLEe', NULL, '2021-03-01 15:01:16', '202.142.153.118', NULL, 0, '2021-03-01 09:55:44', '2021-03-01 10:01:16'),
(76, NULL, NULL, 75, 'MOHAMMED', 'ashraf1888@gmail.com', '$2y$10$GJt2UIaszmiM/nKvtjwt/ee2fECTFgpw3CyNCHSCcErhGtcG.9.Ou', NULL, NULL, NULL, NULL, 0, '2021-03-01 09:55:44', '2021-03-01 09:55:44'),
(77, 2, NULL, NULL, 'UZAIR', 'uzair1999@gmail.com', '$2y$10$zDjv26WtzVwO06gJ0L7O9e6hHda8UFnoQdWrs54IwLuhnvnY0l2Vu', 'ECLA30JUFFmfx2v7RnkduyMSoaN0Nn43R2bvNMCwidIVwdKcnQgdbFf425Le', NULL, NULL, NULL, 1, '2021-03-01 10:06:06', '2021-03-02 06:29:02'),
(78, NULL, NULL, NULL, 'MOHAMMED', 'ahmedasif111@gmail.com', '$2y$10$ujSetfyBGk7tTfNdUdVO.up6Ugf0dgRfmDrXicAZ4piiJi/W2/ghG', NULL, NULL, NULL, NULL, 0, '2021-03-01 10:39:44', '2021-03-01 10:39:44'),
(79, NULL, NULL, 78, 'ASIF', 'ABCD121@GMAIL.COM', '$2y$10$ikeQATqM1YNQY5RhS5Vu6OFoT8T3eNMUakRVakZHoLmfkxz/WLFvK', 'XAMeRfSvgY4iPPeaFU7XYDqLRYrwOX4XfInoIpe9bdfYjQztfHMugjwkAIrl', '2021-03-01 15:54:14', '202.142.153.118', NULL, 0, '2021-03-01 10:39:44', '2021-03-01 10:54:14'),
(80, NULL, NULL, NULL, 'MOAZ', 'moaz@gmail.com', '$2y$10$JcG7/yOCkjVQrFgNhenVdO2lTRgKqCQ9FBc.brrobXWmYtW.hDs12', NULL, NULL, NULL, NULL, 0, '2021-03-01 11:11:31', '2021-03-01 11:11:31'),
(81, NULL, NULL, 80, 'MOHAMMED', 'AHMED1996@GMAIL.COM', '$2y$10$xQAfW1WA4odvlSLWaqSfY.//9mNOXH9nK4ZWp.bKdxPlRxdVcZqyq', NULL, '2021-03-01 16:13:46', '202.142.153.118', NULL, 0, '2021-03-01 11:11:31', '2021-03-01 11:13:46'),
(82, NULL, NULL, NULL, 'ALI', 'student6666@gmail.com', '$2y$10$ekS2peXy5A0qPIsL9uWDc..wxzfEn2D.MIGr6BxgQ1dnG3sR1q0Mu', NULL, NULL, NULL, NULL, 0, '2021-03-01 11:30:37', '2021-03-01 11:30:37'),
(83, NULL, NULL, 82, 'BRENNAN', 'guardians666@gmail.com', '$2y$10$9Ey9glZolZkigVcmcKuMDeXmhSwb/1HwPu4WrEWCFLtHZNKGCpbee', 'wxtfmyZIYhwKmSe5vZ8iFY4Cgb3O5umCcWrUAEuFKRNZM75nv6EykRX8cQxF', '2021-03-01 16:31:23', '202.142.153.118', NULL, 0, '2021-03-01 11:30:37', '2021-03-01 11:31:23'),
(84, 13, 14, NULL, 'Admission  Manager', 'admission@gmail.com', '$2y$10$Wq4QEvnM0fkjjear65wMH.9vkggNO1Mh0P2EwBdlaEyQ48X/f7vIy', 'A6ST9SAN4ruwQtKcu5YkFuX9eAZJ8nKrcIrkTMPcJ6EDylCFsL6BVN9XKUwJ', '2021-03-01 17:06:52', '202.142.153.118', NULL, 1, '2021-03-01 11:51:56', '2021-03-01 12:06:52'),
(85, 11, 14, NULL, 'Uzair Khan', 'admission101@gmail.com', '$2y$10$jlGpvWV780C33FIlaS2fje2THOIeZX6FWWogU52Yh/Pm0i34etP1C', NULL, NULL, NULL, NULL, 0, '2021-03-01 12:14:30', '2021-03-01 12:14:30'),
(87, NULL, NULL, NULL, 'MUAZNA', 'muazna101@gmail.com', '$2y$10$aJA1RV9MoMfZ.bUlJlFflO7BL.NkMz4dbNkReqikm/Ez2xS7ajjHG', NULL, NULL, NULL, NULL, 0, '2021-03-02 06:44:57', '2021-03-02 06:44:57'),
(88, NULL, NULL, 87, 'UMAIR', 'axyz101@gmail.com', '$2y$10$Ql8MxKIw2yn2oTpJz/UX4ewowU9ht/sV9/0ENiTSbW/fj22L6dvmy', NULL, NULL, NULL, NULL, 0, '2021-03-02 06:44:57', '2021-03-02 06:44:57'),
(89, NULL, NULL, NULL, 'ALI', 'aliahmed@gmail.com', '$2y$10$5MQRr9UvXO1A/Pp.bcm7l.qqjv1uaS55u6JmJ6VoVhr3rWwrCAebC', NULL, NULL, NULL, NULL, 0, '2021-03-02 06:52:09', '2021-03-02 06:52:09'),
(90, NULL, NULL, 89, 'FUGIAT', 'abc1234@gmail.com', '$2y$10$Fcf/o1ByIsgODuFC85SFl.V7MhDLTGiSHMt4poZBNMEKiky1xBwPS', NULL, NULL, NULL, NULL, 0, '2021-03-02 06:52:09', '2021-03-02 06:52:09'),
(91, 1, NULL, NULL, 'Zarwah Ahmed', 'zarwah1990@gmail.com', '$2y$10$j.Uz5aWN81B8RMwWK1KSSeDhACqxBILtqK4z6TaOrO/g/Rn4rb9ZS', 'Cul5IgFCjKe7uBNkxf4tBHgvQLPY0AOmGq1yTJSzYKf070XTL0KfbFt7oi4U', NULL, NULL, NULL, 1, '2021-03-02 09:51:19', '2021-03-02 09:51:19'),
(94, 3, NULL, NULL, 'test', 'test@test.com', '$2y$10$OkCk/uEo4BWXHmyGsPrV1uNrpHegbZAPGvGMnfL9KqPjsAE0St.Qm', NULL, NULL, NULL, NULL, 1, '2021-03-02 12:11:43', '2021-03-02 12:11:43'),
(95, 3, NULL, NULL, 'test', 'test1@gmail.com', '$2y$10$GypxyfFLOoxJ2Uj0prGAIeySdKGtdnPQ5LIoMpWE0vLGON77fGEx.', NULL, NULL, NULL, NULL, 1, '2021-03-02 12:23:44', '2021-03-02 12:23:44'),
(97, 3, NULL, NULL, 'test1', 'test221@gmail.com', '$2y$10$/tubgloETLbiLUb41HXRvOJcDhOPp8AB0tsj2cd.kMK4NVMFv1D52', NULL, NULL, NULL, NULL, 1, '2021-03-02 12:25:03', '2021-03-02 12:25:03'),
(98, NULL, NULL, NULL, 'Moiza Ahmed Khan', 'moizaahmed@gmail.com', '$2y$10$x75laYAl/9/ZRzR0T/kU3ul23sjycpK6zLwS7u4RKhfiPdK2Hv0SO', 'ja5AbC6O3XBQZyLcJQJTnt1lE8AF4nh90nrtMylJaqU3SwTEXMXxLFjCG645', NULL, NULL, NULL, 1, '2021-03-03 06:11:37', '2021-03-03 06:11:37');

-- --------------------------------------------------------

--
-- Table structure for table `erp_workdays`
--

CREATE TABLE `erp_workdays` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `workday_length` smallint(5) UNSIGNED NOT NULL,
  `applicable_to` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_models`
--

CREATE TABLE `event_models` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_time_tables`
--

CREATE TABLE `event_time_tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `time_table_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `exam_time_tables`
--

CREATE TABLE `exam_time_tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `time_table_id` int(10) UNSIGNED NOT NULL,
  `exam_id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE `faculties` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `program` int(10) UNSIGNED DEFAULT NULL,
  `faculty` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`id`, `branch_id`, `session_id`, `program`, `faculty`, `slug`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 7, 3, 1, 'EARLY YEAR 1', 'Early-Year-1', 1, 6, 6, '2020-07-16 18:27:12', '2020-07-16 18:29:49'),
(2, 7, 3, 2, 'EARLY YEAR 2', 'EARLY-YEAR-2', 1, 6, NULL, '2020-07-16 18:27:19', '2020-07-16 18:27:19'),
(3, 7, 3, 3, 'EARLY YEAR 3', 'EARLY-YEAR-3', 1, 6, NULL, '2020-07-16 18:27:26', '2020-07-16 18:27:26'),
(4, 7, 3, 4, 'PYP 1', 'PYP-1', 1, 6, NULL, '2020-07-16 18:27:35', '2020-07-16 18:27:35'),
(5, 7, 3, 5, 'PYP 2', 'PYP-2', 1, 6, NULL, '2020-07-16 18:27:43', '2020-07-16 18:27:43'),
(6, 7, 3, 6, 'PYP 3', 'PYP-3', 1, 6, NULL, '2020-07-16 18:27:52', '2020-07-16 18:27:52'),
(7, 7, 3, 7, 'PYP 4', 'PYP-4', 1, 6, NULL, '2020-07-16 18:28:00', '2020-07-16 18:28:00'),
(8, 7, 3, 8, 'PYP 5', 'PYP-5', 1, 6, NULL, '2020-07-16 18:28:10', '2020-07-16 18:28:10'),
(9, 7, 3, 9, 'PYP 6', 'PYP-6', 1, 6, NULL, '2020-07-16 18:28:19', '2020-07-16 18:28:19'),
(10, 7, 3, 10, 'IGCSE-I', 'IGCSE-I', 1, 6, NULL, '2020-07-16 18:28:44', '2020-07-16 18:28:44'),
(11, 7, 3, 11, 'IGCSE-II', 'IGCSE-II', 1, 6, NULL, '2020-07-16 18:28:54', '2020-07-16 18:28:54'),
(12, 7, 3, 12, 'IGCSE-III', 'IGCSE-III', 1, 6, NULL, '2020-07-16 18:29:06', '2020-07-16 18:29:06'),
(13, 7, 3, 13, 'IGCSE-IV', 'IGCSE-IV', 1, 6, NULL, '2020-07-16 18:29:14', '2020-07-16 18:29:14'),
(14, 7, 3, 18, 'A-I', 'A-I', 1, 6, NULL, '2020-07-16 18:30:05', '2020-07-16 18:30:05'),
(15, 7, 3, 19, 'A-II', 'A-II', 1, 6, NULL, '2020-07-16 18:30:15', '2020-07-16 18:30:15');

-- --------------------------------------------------------

--
-- Table structure for table `fee_collections`
--

CREATE TABLE `fee_collections` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(20) DEFAULT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `students_id` int(10) UNSIGNED NOT NULL,
  `fee_masters_id` int(10) UNSIGNED NOT NULL,
  `fee_structure_code` int(11) NOT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee_term_id` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `paid_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_amount` float DEFAULT 0,
  `discount` int(11) DEFAULT 0,
  `discount_type` int(11) NOT NULL DEFAULT 1,
  `fine` float DEFAULT 0,
  `tax_percentage` float NOT NULL DEFAULT 0,
  `total_amount` float DEFAULT 0,
  `payment_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee_status` tinyint(1) NOT NULL DEFAULT 1,
  `fc_id` int(10) DEFAULT NULL,
  `fc_bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` float NOT NULL DEFAULT 0,
  `all_paid` int(11) NOT NULL DEFAULT 0,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_collections`
--

INSERT INTO `fee_collections` (`id`, `session_id`, `branch_id`, `students_id`, `fee_masters_id`, `fee_structure_code`, `bill_no`, `fee_term_id`, `due_date`, `paid_date`, `paid_amount`, `discount`, `discount_type`, `fine`, `tax_percentage`, `total_amount`, `payment_mode`, `note`, `fee_status`, `fc_id`, `fc_bill_no`, `balance`, `all_paid`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 33, 0, 10000015, '10000001', 57, '2020-08-10', NULL, 0, 0, 1, 0, 5, 231665, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46'),
(2, 1, 4, 33, 0, 10000015, '10000002', 58, '2020-10-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46'),
(3, 1, 4, 33, 0, 10000015, '10000003', 59, '2020-12-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46'),
(4, 1, 4, 33, 0, 10000015, '10000004', 60, '2021-02-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46'),
(5, 1, 4, 36, 0, 10000016, '10000005', 61, '2020-09-10', NULL, 0, 0, 1, 0, 5, 343996, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20'),
(6, 1, 4, 36, 0, 10000016, '10000006', 62, '2020-11-10', NULL, 0, 0, 1, 0, 5, 126282, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20'),
(7, 1, 4, 36, 0, 10000016, '10000007', 63, '2021-01-10', NULL, 0, 0, 1, 0, 5, 126282, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20'),
(8, 1, 4, 36, 0, 10000016, '10000008', 64, '2021-04-10', NULL, 0, 0, 1, 0, 5, 126282, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20'),
(13, 1, 4, 10, 0, 10000006, '10000013', 21, '2020-09-10', NULL, 0, 0, 1, 0, 5, 232989, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25'),
(14, 1, 4, 10, 0, 10000006, '10000014', 22, '2020-11-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25'),
(15, 1, 4, 10, 0, 10000006, '10000015', 23, '2021-01-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25'),
(16, 1, 4, 10, 0, 10000006, '10000016', 24, '2021-04-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25'),
(17, 1, 4, 16, 0, 10000008, '10000017', 29, '2020-09-10', NULL, 0, 0, 1, 0, 5, 232989, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02'),
(18, 1, 4, 16, 0, 10000008, '10000018', 30, '2020-11-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02'),
(19, 1, 4, 16, 0, 10000008, '10000019', 31, '2021-01-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02'),
(20, 1, 4, 16, 0, 10000008, '10000020', 32, '2021-04-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02'),
(21, 1, 4, 53, 0, 10000014, '10000021', 53, '2020-08-10', NULL, 0, 0, 1, 0, 5, 231665, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17'),
(22, 1, 4, 53, 0, 10000014, '10000022', 54, '2020-10-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17'),
(23, 1, 4, 53, 0, 10000014, '10000023', 55, '2020-12-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17'),
(24, 1, 4, 53, 0, 10000014, '10000024', 56, '2021-02-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17'),
(25, 1, 4, 51, 0, 10000014, '10000025', 53, '2020-08-10', NULL, 0, 0, 1, 0, 5, 231665, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16'),
(26, 1, 4, 51, 0, 10000014, '10000026', 54, '2020-10-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16'),
(27, 1, 4, 51, 0, 10000014, '10000027', 55, '2020-12-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16'),
(28, 1, 4, 51, 0, 10000014, '10000028', 56, '2021-02-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16'),
(29, 1, 4, 101, 0, 10000012, '10000029', 45, '2020-08-10', NULL, 0, 0, 1, 0, 5, 231665, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48'),
(30, 1, 4, 101, 0, 10000012, '10000030', 46, '2020-10-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48'),
(31, 1, 4, 101, 0, 10000012, '10000031', 47, '2020-12-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48'),
(32, 1, 4, 101, 0, 10000012, '10000032', 48, '2021-02-10', NULL, 0, 0, 1, 0, 5, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48'),
(41, 1, 4, 104, 0, 10000020, '10000033', 13, '2020-09-10', NULL, 0, 0, 1, 0, 0, 232989, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-09 12:08:40', '2020-10-09 12:08:40'),
(42, 1, 4, 104, 0, 10000020, '10000034', 14, '2020-11-10', NULL, 0, 0, 1, 0, 0, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-09 12:08:40', '2020-10-09 12:08:40'),
(43, 1, 4, 104, 0, 10000020, '10000035', 15, '2021-01-10', NULL, 0, 0, 1, 0, 0, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-09 12:08:40', '2020-10-09 12:08:40'),
(44, 1, 4, 104, 0, 10000020, '10000036', 16, '2021-04-10', NULL, 0, 0, 1, 0, 0, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-09 12:08:40', '2020-10-09 12:08:40'),
(49, 1, 7, 105, 0, 10000011, '10000037', 41, '2020-09-10', NULL, 0, 0, 1, 0, 0, 232989, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-12 10:50:49', '2020-10-12 10:50:49'),
(50, 1, 7, 105, 0, 10000011, '10000038', 42, '2020-11-10', NULL, 0, 0, 1, 0, 0, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-12 10:50:49', '2020-10-12 10:50:49'),
(51, 1, 7, 105, 0, 10000011, '10000039', 43, '2021-01-10', NULL, 0, 0, 1, 0, 0, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-12 10:50:49', '2020-10-12 10:50:49'),
(52, 1, 7, 105, 0, 10000011, '10000040', 44, '2021-04-10', NULL, 0, 0, 1, 0, 0, 120875, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-12 10:50:49', '2020-10-12 10:50:49'),
(57, 1, 7, 106, 0, 10000012, '10000045', 45, '2020-08-10', NULL, 0, 0, 1, 0, 0, 174983, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58'),
(58, 1, 7, 106, 0, 10000012, '10000046', 46, '2020-10-10', NULL, 0, 0, 1, 0, 0, 84612.5, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58'),
(59, 1, 7, 106, 0, 10000012, '10000047', 47, '2020-12-10', NULL, 0, 0, 1, 0, 0, 84612.5, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58'),
(60, 1, 7, 106, 0, 10000012, '10000048', 48, '2021-02-10', NULL, 0, 0, 1, 0, 0, 84612.5, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58'),
(65, 1, 4, 19, 0, 10000009, '10000049', 33, '2020-09-10', NULL, 0, 0, 2, 0, 0, 125489, NULL, NULL, 1, NULL, NULL, 0, 0, 6, 6, '2020-10-12 17:38:24', '2020-10-12 17:41:14'),
(66, 1, 4, 19, 0, 10000009, '10000050', 34, '2020-11-10', NULL, 0, 0, 2, 0, 0, 24175, NULL, NULL, 1, NULL, NULL, 0, 0, 6, 6, '2020-10-12 17:38:24', '2020-10-12 17:41:14'),
(67, 1, 4, 19, 0, 10000009, '10000051', 35, '2021-01-10', NULL, 0, 0, 2, 0, 0, 24175, NULL, NULL, 1, NULL, NULL, 0, 0, 6, 6, '2020-10-12 17:38:24', '2020-10-12 17:41:14'),
(68, 1, 4, 19, 0, 10000009, '10000052', 36, '2021-04-10', NULL, 0, 0, 2, 0, 0, 24175, NULL, NULL, 1, NULL, NULL, 0, 0, 6, 6, '2020-10-12 17:38:24', '2020-10-12 17:41:14'),
(73, 1, 7, 6, 0, 10000020, '10000053', 13, '2020-09-10', '2020-10-14', 193871, 0, 1, 0, 5, 184639, 'Cash', 'Fee Paid', 3, NULL, NULL, 0, 0, 0, NULL, '2020-10-13 04:55:04', '2020-10-13 05:00:16'),
(74, 1, 7, 6, 0, 10000020, '10000054', 14, '2020-11-10', NULL, 0, 0, 1, 0, 5, 72525, NULL, 'Installments Done', 0, NULL, NULL, 0, 0, 67, 67, '2020-10-13 04:55:04', '2020-10-13 04:59:58'),
(75, 1, 7, 6, 0, 10000020, '10000055', 15, '2021-01-10', NULL, 0, 0, 1, 0, 5, 72525, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04'),
(76, 1, 7, 6, 0, 10000020, '10000056', 16, '2021-04-10', NULL, 0, 0, 1, 0, 5, 72525, NULL, NULL, 1, NULL, NULL, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04'),
(77, 1, 7, 6, 0, 10000020, '10000057', 14, '2020-10-01', '2020-09-07', 38076, 0, 1, 0, 5, 36262.5, 'cash', 'MIS File Import', 3, 74, '10000054', 0, 0, 67, 67, '2020-10-13 04:59:58', '2020-10-13 05:37:49'),
(78, 1, 7, 6, 0, 10000020, '10000058', 14, '2020-10-23', NULL, 0, 0, 1, 0, 5, 36262.5, NULL, 'Installment 2', 1, 74, '10000054', 0, 0, 67, NULL, '2020-10-13 04:59:58', '2020-10-13 04:59:58'),
(79, 1, 7, 107, 0, 10000021, '10000059', 17, '2020-09-10', NULL, 0, 0, 1, 0, 0, 98400, NULL, 'Installments Done', 0, NULL, NULL, 0, 0, 4, 4, '2020-10-13 08:20:55', '2020-10-13 08:29:14'),
(80, 1, 7, 107, 0, 10000021, '10000060', 18, '2020-11-10', NULL, 0, 0, 1, 0, 0, 72525, NULL, NULL, 1, NULL, NULL, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55'),
(81, 1, 7, 107, 0, 10000021, '10000061', 19, '2021-01-10', NULL, 0, 0, 1, 0, 0, 72525, NULL, NULL, 1, NULL, NULL, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55'),
(82, 1, 7, 107, 0, 10000021, '10000062', 20, '2021-04-10', NULL, 0, 0, 1, 0, 0, 72525, NULL, NULL, 1, NULL, NULL, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55'),
(83, 1, 7, 107, 0, 10000021, '10000063', 17, '2020-09-10', '2020-10-06', 48400, 0, 1, 0, 0, 48400, 'Cash', NULL, 3, 79, '10000059', 0, 0, 0, NULL, '2020-10-13 08:29:14', '2020-10-13 08:47:33'),
(84, 1, 7, 107, 0, 10000021, '10000064', 17, '2020-10-10', '2020-10-08', 50000, 0, 1, 0, 0, 50000, 'Cash', NULL, 3, 79, '10000059', 0, 0, 0, NULL, '2020-10-13 08:29:14', '2020-10-13 08:50:17'),
(85, 1, 7, 108, 0, 10000016, '10000065', 61, '2020-09-10', '2020-10-07', 141918, 0, 1, 0, 0, 141918, 'Cash', NULL, 3, NULL, NULL, 0, 0, 0, NULL, '2020-10-13 09:11:06', '2020-10-13 09:12:54'),
(86, 1, 7, 108, 0, 10000016, '10000066', 62, '2020-11-10', NULL, 0, 0, 1, 0, 0, 0, NULL, NULL, 1, NULL, NULL, 0, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06'),
(87, 1, 7, 108, 0, 10000016, '10000067', 63, '2021-01-10', NULL, 0, 0, 1, 0, 0, 0, NULL, NULL, 1, NULL, NULL, 0, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06'),
(88, 1, 7, 108, 0, 10000016, '10000068', 64, '2021-04-10', NULL, 0, 0, 1, 0, 0, 0, NULL, NULL, 1, NULL, NULL, 0, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06'),
(89, 1, 4, 4, 0, 10000019, '10000069', 73, '2020-09-10', NULL, 0, 0, 1, 0, 5, 424996, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29'),
(90, 1, 4, 4, 0, 10000019, '10000070', 74, '2020-11-10', NULL, 0, 0, 1, 0, 5, 24999.8, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29'),
(91, 1, 4, 4, 0, 10000019, '10000071', 75, '2021-01-10', NULL, 0, 0, 1, 0, 5, 24999.8, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29'),
(92, 1, 4, 4, 0, 10000019, '10000072', 76, '2021-04-10', NULL, 0, 0, 1, 0, 5, 24999.8, NULL, NULL, 1, NULL, NULL, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29'),
(93, 1, 4, 2, 0, 10000030, '10000073', 13, '2020-09-10', NULL, 0, 0, 1, 0, 0, 10000, NULL, NULL, 1, NULL, NULL, 0, 0, 1, NULL, '2021-02-26 13:48:44', '2021-02-26 13:48:44');

-- --------------------------------------------------------

--
-- Table structure for table `fee_heads`
--

CREATE TABLE `fee_heads` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `program_id` int(10) UNSIGNED DEFAULT NULL,
  `fee_section_id` int(10) UNSIGNED DEFAULT NULL,
  `erp_fees_head_id` int(11) NOT NULL,
  `fee_head_desc` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dividable` int(10) UNSIGNED DEFAULT NULL,
  `fee_head_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_heads`
--

INSERT INTO `fee_heads` (`id`, `branch_id`, `session_id`, `program_id`, `fee_section_id`, `erp_fees_head_id`, `fee_head_desc`, `dividable`, `fee_head_title`, `slug`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(207, 7, 1, 1, 1, 1, 'Only at the time of admission', 0, 'Admission Fee', 'Admission-Fee', 1, 70, 70, '2020-10-01 00:19:51', '2020-10-14 07:49:14'),
(206, 4, 1, 1, 1, 0, 'Only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 70, 70, '2020-10-01 00:19:21', '2020-10-14 07:49:31'),
(8, 7, 1, 1, 2, 1, 'Yearly', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-07-23 16:03:00', '2020-07-23 16:03:00'),
(198, 7, 1, 1, 5, 2, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, NULL, '2020-09-23 04:19:01', '2020-09-23 04:19:01'),
(10, 7, 1, 0, 5, 2, NULL, 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-07-23 16:03:38', '2020-09-23 04:12:07'),
(11, 7, 1, 2, 1, 3, 'Only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-07-23 15:35:50', '2020-07-23 15:35:50'),
(12, 7, 1, 2, 1, 4, 'Only at the time of admission', 0, 'Admission Fee', 'Admission-Fee', 1, 6, NULL, '2020-07-23 15:36:07', '2020-07-23 15:36:07'),
(13, 7, 1, 2, 1, 5, 'Only at the time of admission & Refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-07-23 15:36:26', '2020-07-23 15:36:26'),
(15, 7, 1, 2, 2, 5, 'Yearly', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-07-23 15:38:00', '2020-07-23 15:38:00'),
(17, 7, 1, 2, 2, 6, 'Yearly', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-07-23 16:03:00', '2020-07-23 16:03:00'),
(18, 7, 1, 2, 3, 7, 'Aug  - May', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-07-23 16:03:20', '2020-09-03 22:51:30'),
(19, 4, 1, 2, 3, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, NULL, '2020-07-23 16:03:38', '2020-07-23 16:03:38'),
(155, 7, 1, 10, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-02 20:04:40', '2020-09-02 20:04:40'),
(153, 7, 1, 10, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-02 20:03:50', '2020-09-02 20:03:50'),
(158, 7, 1, 11, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-02 20:08:15', '2020-09-02 20:08:15'),
(151, 7, 1, 10, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-02 20:02:42', '2020-09-02 20:02:42'),
(150, 7, 1, 10, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-02 20:02:08', '2020-09-02 20:02:08'),
(149, 7, 1, 10, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-02 20:01:46', '2020-09-02 20:01:46'),
(76, 7, 1, 19, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-01 00:06:10', '2020-09-01 00:06:10'),
(75, 7, 1, 0, 2, 0, '(A-2) yearly', 0, 'Annual Resource Charges (Part 1)', 'Annual-Resource-Charges-(Part-1)', 1, 6, 6, '2020-09-01 00:05:42', '2020-09-17 02:21:11'),
(71, 7, 1, 19, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-01 00:03:51', '2020-09-01 00:03:51'),
(70, 7, 1, 19, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-01 00:03:12', '2020-09-01 00:03:12'),
(39, 7, 1, 3, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-08-31 20:34:12', '2020-08-31 20:34:12'),
(40, 7, 1, 3, 1, 0, 'Only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-08-31 20:38:41', '2020-08-31 20:38:41'),
(41, 7, 1, 3, 1, 0, 'Only at the time of admission & Refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-08-31 20:39:48', '2020-08-31 20:39:48'),
(43, 7, 1, 3, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, 6, '2020-08-31 20:43:27', '2020-08-31 21:15:46'),
(45, 7, 1, 3, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, 6, '2020-08-31 20:45:40', '2020-08-31 21:20:30'),
(46, 7, 1, 3, 3, 0, 'Aug  - May', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-08-31 20:47:27', '2020-09-03 22:52:04'),
(48, 7, 1, 3, 3, 0, 'Semester Fee', 0, 'LMS Fee', 'LMS-Fee', 1, 6, NULL, '2020-08-31 20:54:15', '2020-08-31 20:54:15'),
(49, 7, 1, 18, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-08-31 23:18:13', '2020-08-31 23:18:13'),
(50, 7, 1, 0, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-08-31 23:19:02', '2020-08-31 23:19:02'),
(51, 7, 1, 18, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-08-31 23:19:48', '2020-08-31 23:19:48'),
(52, 7, 1, 18, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-08-31 23:20:59', '2020-08-31 23:20:59'),
(55, 7, 1, 18, 2, 0, '(A-1) yearly', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, 6, '2020-08-31 23:27:13', '2020-09-02 21:56:55'),
(57, 7, 1, 18, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-08-31 23:28:38', '2020-08-31 23:28:38'),
(58, 7, 1, 18, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-08-31 23:29:45', '2020-08-31 23:29:45'),
(60, 7, 1, 18, 3, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-08-31 23:36:54', '2020-09-01 01:29:59'),
(61, 7, 1, 18, 3, 0, 'Semester Fee', 0, 'Lab Fee', 'Lab-Fee', 1, 6, NULL, '2020-08-31 23:40:59', '2020-08-31 23:40:59'),
(62, 7, 1, 18, 3, 0, 'Semester Fee', 0, 'LMS Fee', 'LMS-Fee', 1, 6, NULL, '2020-08-31 23:41:47', '2020-08-31 23:41:47'),
(77, 7, 1, 19, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-01 00:06:47', '2020-09-01 00:06:47'),
(78, 7, 1, 19, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-01 00:07:23', '2020-09-01 01:30:33'),
(74, 7, 1, 0, 2, 0, '(A-1) yearly', 0, 'Annual Resource Charges (Part 2)', 'Annual-Resource-Charges-(Part-2)', 1, 6, 6, '2020-09-01 00:05:15', '2020-09-17 02:24:21'),
(69, 7, 1, 19, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-01 00:02:39', '2020-09-01 00:02:39'),
(79, 7, 1, 19, 5, 0, 'Academic Fee', 0, 'Lab Fee', 'Lab-Fee', 1, 6, 6, '2020-09-01 00:07:50', '2020-09-01 00:40:54'),
(80, 7, 1, 19, 5, 0, 'Academic Fee', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-01 00:08:17', '2020-09-01 00:41:31'),
(81, 7, 1, 4, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-01 00:52:54', '2020-09-01 00:52:54'),
(82, 7, 1, 4, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-01 00:53:33', '2020-09-01 00:53:33'),
(83, 7, 1, 4, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-01 00:54:20', '2020-09-01 00:54:20'),
(86, 7, 1, 4, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-01 00:57:37', '2020-09-01 00:57:37'),
(88, 7, 1, 4, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-01 00:59:11', '2020-09-01 00:59:11'),
(89, 7, 1, 4, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-01 01:00:10', '2020-09-01 01:00:10'),
(90, 7, 1, 4, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-01 01:02:27', '2020-09-03 22:52:37'),
(91, 7, 1, 4, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-01 01:06:27', '2020-09-01 01:52:58'),
(92, 7, 1, 5, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-01 01:32:08', '2020-09-01 01:32:08'),
(93, 7, 1, 5, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-01 01:32:45', '2020-09-01 01:32:45'),
(94, 7, 1, 5, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-01 01:33:21', '2020-09-01 01:33:21'),
(97, 7, 1, 5, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, 6, '2020-09-01 01:46:09', '2020-09-01 01:47:15'),
(99, 7, 1, 5, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-01 01:48:36', '2020-09-01 01:48:36'),
(100, 7, 1, 5, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-01 01:49:04', '2020-09-01 01:49:04'),
(101, 7, 1, 5, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-01 01:49:39', '2020-09-03 22:53:03'),
(102, 7, 1, 5, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-01 01:50:29', '2020-09-01 01:52:04'),
(103, 7, 1, 6, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-01 01:59:03', '2020-09-01 01:59:03'),
(104, 7, 1, 6, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-01 01:59:46', '2020-09-01 01:59:46'),
(105, 7, 1, 6, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-01 02:00:11', '2020-09-01 02:00:11'),
(108, 7, 1, 6, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-01 02:03:51', '2020-09-01 02:03:51'),
(110, 7, 1, 6, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-01 02:05:08', '2020-09-01 02:05:08'),
(111, 7, 1, 6, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, 6, '2020-09-01 02:05:48', '2020-09-01 02:06:50'),
(112, 7, 1, 6, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-01 02:07:41', '2020-09-03 22:53:51'),
(113, 7, 1, 6, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-01 02:09:00', '2020-09-01 02:09:47'),
(114, 7, 1, 7, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-01 02:33:23', '2020-09-01 02:33:23'),
(115, 7, 1, 7, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-01 02:33:58', '2020-09-01 02:33:58'),
(116, 7, 1, 7, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-01 02:34:58', '2020-09-01 02:34:58'),
(119, 7, 1, 7, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-01 02:38:00', '2020-09-01 02:38:00'),
(121, 7, 1, 7, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-01 02:39:51', '2020-09-01 02:39:51'),
(122, 7, 1, 7, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-01 02:40:53', '2020-09-01 02:40:53'),
(123, 7, 1, 7, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-01 02:42:08', '2020-09-03 22:54:33'),
(124, 7, 1, 7, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-01 02:42:40', '2020-09-01 02:43:26'),
(125, 7, 1, 8, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-01 19:56:49', '2020-09-01 19:56:49'),
(126, 7, 1, 8, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-01 19:57:14', '2020-09-01 19:57:14'),
(127, 7, 1, 8, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-01 19:57:42', '2020-09-01 19:57:42'),
(130, 7, 1, 8, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-01 19:59:15', '2020-09-01 19:59:15'),
(132, 7, 1, 8, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-01 20:00:58', '2020-09-01 20:00:58'),
(133, 7, 1, 8, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-01 20:01:25', '2020-09-01 20:01:25'),
(134, 7, 1, 8, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-01 20:02:11', '2020-09-03 22:54:37'),
(135, 7, 1, 8, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-01 20:02:37', '2020-09-01 20:04:59'),
(136, 7, 1, 9, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-01 20:05:37', '2020-09-01 20:05:37'),
(137, 7, 1, 9, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-01 20:06:02', '2020-09-01 20:06:02'),
(138, 7, 1, 9, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-01 20:06:37', '2020-09-01 20:06:37'),
(141, 7, 1, 9, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-01 20:08:18', '2020-09-01 20:08:18'),
(143, 7, 1, 9, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-01 20:26:14', '2020-09-01 20:26:14'),
(145, 7, 1, 9, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-01 20:28:10', '2020-09-01 20:28:10'),
(146, 7, 1, 9, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-01 20:28:45', '2020-09-03 22:54:41'),
(147, 7, 1, 9, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-01 20:29:22', '2020-09-01 20:31:19'),
(156, 7, 1, 10, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-02 20:05:18', '2020-09-03 22:54:44'),
(157, 7, 1, 10, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-02 20:05:42', '2020-09-02 20:41:10'),
(159, 7, 1, 11, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-02 20:08:43', '2020-09-02 20:08:43'),
(160, 7, 1, 11, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-02 20:09:09', '2020-09-02 20:09:09'),
(162, 7, 1, 11, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-02 20:10:11', '2020-09-02 20:10:11'),
(164, 7, 1, 11, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-02 20:11:40', '2020-09-02 20:11:40'),
(165, 7, 1, 11, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-02 20:12:20', '2020-09-02 20:43:41'),
(166, 7, 1, 11, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-02 20:12:50', '2020-09-02 20:43:07'),
(167, 7, 1, 12, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-02 20:14:32', '2020-09-02 20:14:32'),
(168, 7, 1, 12, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-02 20:14:59', '2020-09-02 20:14:59'),
(169, 7, 1, 12, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-02 20:15:59', '2020-09-02 20:15:59'),
(171, 7, 1, 12, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-02 20:17:35', '2020-09-02 20:17:35'),
(173, 7, 1, 12, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-02 20:18:32', '2020-09-02 20:18:32'),
(174, 7, 1, 12, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-02 20:19:18', '2020-09-02 20:45:18'),
(175, 7, 1, 12, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-02 20:19:44', '2020-09-02 20:45:55'),
(176, 7, 1, 13, 1, 0, 'only at the time of admission', 0, 'Registration Fee', 'Registration-Fee', 1, 6, NULL, '2020-09-02 20:23:23', '2020-09-02 20:23:23'),
(177, 7, 1, 13, 1, 0, 'only at the time of admission', 0, 'Admission fee', 'Admission-fee', 1, 6, NULL, '2020-09-02 20:23:48', '2020-09-02 20:23:48'),
(178, 7, 1, 13, 1, 0, 'Only at the time of admission & refundable', 0, 'Security Fee', 'Security-Fee', 1, 6, NULL, '2020-09-02 20:24:40', '2020-09-02 20:24:40'),
(180, 7, 1, 13, 2, 0, 'Once in a Year', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 6, NULL, '2020-09-02 20:25:49', '2020-09-02 20:25:49'),
(182, 7, 1, 13, 2, 0, 'Once in a Year', 0, 'RIVY ID Card', 'RIVY-ID-Card', 1, 6, NULL, '2020-09-02 20:26:56', '2020-09-02 20:26:56'),
(183, 7, 1, 13, 5, 0, '(Aug-may)', 1, 'Tuition Fee', 'Tuition-Fee', 1, 6, 6, '2020-09-02 20:27:26', '2020-09-02 20:48:02'),
(184, 7, 1, 13, 5, 0, '', 0, 'LMS Fee', 'LMS-Fee', 1, 6, 6, '2020-09-02 20:27:55', '2020-09-02 20:47:24'),
(186, 7, 1, 10, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-02 20:39:44', '2020-09-02 20:39:44'),
(188, 7, 1, 11, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-02 20:42:22', '2020-09-02 20:42:22'),
(190, 7, 1, 12, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-02 20:44:46', '2020-09-02 20:44:46'),
(192, 7, 1, 13, 2, 0, 'Once in a Year', 0, 'IB Fee', 'IB-Fee', 1, 6, NULL, '2020-09-02 20:46:51', '2020-09-02 20:46:51'),
(208, 7, 1, 0, 1, 0, 'Refundable', 0, 'Security Fee', 'Security-Fee', 1, 70, 70, '2020-10-01 00:20:13', '2020-10-01 00:20:35'),
(194, 7, 1, 0, 2, 0, 'Yearly', 0, 'Security Fee', 'Security-Fee', 1, 14, 70, '2020-09-16 01:31:22', '2020-10-01 01:03:20'),
(199, 7, 1, 1, 5, 0, 'Once in a Year', 0, 'LMS Fee', 'LMS-Fee', 1, 6, NULL, '2020-09-23 04:20:21', '2020-09-23 04:20:21'),
(209, 7, 1, 1, 1, 0, 'Only at the time of admission & Refundable', 0, 'Security Fee', 'Security-Fee', 1, 70, 70, '2020-10-01 01:07:34', '2020-10-14 07:49:49'),
(210, 7, 1, 1, 1, 0, 'Yearly', 0, 'Annual Resource Charges', 'Annual-Resource-Charges', 1, 70, NULL, '2020-10-14 07:44:11', '2020-10-14 07:44:11'),
(211, 2, 1, 2, 1, 0, 'ONE TAME', 1, '1000', '1000', 1, 1, NULL, '2021-02-11 11:35:19', '2021-02-11 11:35:19'),
(212, 2, 1, 2, 2, 0, 'one-tame', 0, 'one-tame', 'one-tame', 1, 1, NULL, '2021-02-11 11:37:10', '2021-02-11 11:37:10'),
(213, 4, NULL, 1, 5, 0, 'ONE TAME', 1, '1000', '1000', 1, 1, NULL, '2021-02-25 10:50:49', '2021-02-25 10:50:49'),
(214, 4, NULL, 1, 40, 0, NULL, 0, 'Registration F', 'Registration-F', 1, 1, NULL, '2021-02-26 07:13:43', '2021-02-26 07:13:43'),
(215, 4, NULL, 1, 40, 0, 'Registration Fee', 0, 'Registration Fee', 'Registration-Fee', 1, 1, NULL, '2021-02-26 07:13:47', '2021-02-26 07:13:47'),
(216, 1, NULL, 1, 1, 0, 'one  time  Fee', 1, 'Admission Fee', 'Admission-Fee', 1, 1, NULL, '2021-03-01 11:42:59', '2021-03-01 11:42:59'),
(217, 7, NULL, 1, 1, 0, 'One Time  Payment', 1, 'Registration Fee', 'Registration-Fee', 1, 1, NULL, '2021-03-01 12:39:59', '2021-03-01 12:39:59'),
(218, 1, NULL, 1, 1, 0, 'at  the  time  of  Admission', 0, 'Security  Fee', 'Security-Fee', 1, 1, NULL, '2021-03-02 08:03:31', '2021-03-02 08:03:31'),
(219, 1, NULL, 1, 1, 0, 'at  the  time  of  Admission', 1, 'admission Fee', 'admission-Fee', 1, 1, NULL, '2021-03-02 08:04:26', '2021-03-02 08:04:26'),
(220, 1, NULL, 1, 1, 0, 'Only  at  the   time  of  Admission', 1, 'Security  Fee', 'Security-Fee', 1, 1, NULL, '2021-03-02 08:05:06', '2021-03-02 08:05:06'),
(221, 1, NULL, 1, 1, 0, 'Only  at  the   time  of  Admission', 1, 'Security  Fee', 'Security-Fee', 1, 1, NULL, '2021-03-02 08:05:08', '2021-03-02 08:05:08'),
(222, 1, NULL, 1, 1, 0, 'Only  at  the   time  of  Admission', 1, 'Security  Fee', 'Security-Fee', 1, 1, NULL, '2021-03-02 08:05:24', '2021-03-02 08:05:24');

-- --------------------------------------------------------

--
-- Table structure for table `fee_masters`
--

CREATE TABLE `fee_masters` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(20) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `program_id` int(11) NOT NULL,
  `students_id` int(10) UNSIGNED NOT NULL,
  `fee_head` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee_structure_code` int(11) NOT NULL,
  `fee_amount` int(11) NOT NULL,
  `fee_head_term` int(11) DEFAULT 1,
  `fee_discount_percentage` float NOT NULL DEFAULT 0,
  `discount_type` float NOT NULL DEFAULT 1,
  `fee_discount_comment` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scholarship` int(11) DEFAULT 0,
  `scholarship_no_of_a` int(11) DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fee_master_basic_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_masters`
--

INSERT INTO `fee_masters` (`id`, `session_id`, `branch_id`, `program_id`, `students_id`, `fee_head`, `fee_structure_code`, `fee_amount`, `fee_head_term`, `fee_discount_percentage`, `discount_type`, `fee_discount_comment`, `scholarship`, `scholarship_no_of_a`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`, `fee_master_basic_id`) VALUES
(1, 1, 4, 13, 33, '176', 10000015, 22050, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 0),
(2, 1, 4, 13, 33, '177', 10000015, 30870, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 0),
(3, 1, 4, 13, 33, '178', 10000015, 30870, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 0),
(4, 1, 4, 13, 33, '180', 10000015, 15000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 0),
(5, 1, 4, 13, 33, '182', 10000015, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 0),
(6, 1, 4, 13, 33, '192', 10000015, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 0),
(7, 1, 4, 13, 33, '183', 10000015, 483500, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 0),
(8, 1, 4, 13, 33, '184', 10000015, 12000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 0),
(9, 1, 4, 18, 36, '49', 10000016, 22050, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(10, 1, 4, 18, 36, '51', 10000016, 37898, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(11, 1, 4, 18, 36, '52', 10000016, 37898, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(12, 1, 4, 18, 36, '55', 10000016, 33000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(13, 1, 4, 18, 36, '57', 10000016, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(14, 1, 4, 18, 36, '58', 10000016, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(16, 1, 4, 18, 36, '60', 10000016, 505128, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(17, 1, 4, 18, 36, '61', 10000016, 59868, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(18, 1, 4, 18, 36, '62', 10000016, 12000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:52:20', '2020-10-08 11:52:20', 0),
(19, 1, 4, 2, 5, '11', 10000020, 17350, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:53:15', '2020-10-08 11:53:15', 0),
(20, 1, 4, 2, 5, '12', 10000020, 36382, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:53:15', '2020-10-08 11:53:15', 0),
(21, 1, 4, 2, 5, '13', 10000020, 36382, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:53:15', '2020-10-08 11:53:15', 0),
(22, 1, 4, 2, 5, '15', 10000020, 10000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:53:15', '2020-10-08 11:53:15', 0),
(23, 1, 4, 2, 5, '17', 10000020, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:53:15', '2020-10-08 11:53:15', 0),
(24, 1, 4, 2, 5, '18', 10000020, 483500, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:53:15', '2020-10-08 11:53:15', 0),
(25, 1, 4, 2, 5, '19', 10000020, 12000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:53:15', '2020-10-08 11:53:15', 0),
(26, 1, 4, 4, 10, '81', 10000006, 17350, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 0),
(27, 1, 4, 4, 10, '82', 10000006, 36382, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 0),
(28, 1, 4, 4, 10, '83', 10000006, 36382, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 0),
(29, 1, 4, 4, 10, '86', 10000006, 10000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 0),
(30, 1, 4, 4, 10, '88', 10000006, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 0),
(31, 1, 4, 4, 10, '89', 10000006, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 0),
(32, 1, 4, 4, 10, '90', 10000006, 483500, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 0),
(33, 1, 4, 4, 10, '91', 10000006, 12000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 0),
(34, 1, 4, 6, 16, '103', 10000008, 17350, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 0),
(35, 1, 4, 6, 16, '104', 10000008, 36382, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 0),
(36, 1, 4, 6, 16, '105', 10000008, 36382, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 0),
(37, 1, 4, 6, 16, '108', 10000008, 10000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 0),
(38, 1, 4, 6, 16, '110', 10000008, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 0),
(39, 1, 4, 6, 16, '111', 10000008, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 0),
(40, 1, 4, 6, 16, '112', 10000008, 483500, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 0),
(41, 1, 4, 6, 16, '113', 10000008, 12000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 0),
(42, 1, 4, 12, 53, '167', 10000014, 22050, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 0),
(43, 1, 4, 12, 53, '168', 10000014, 30870, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 0),
(44, 1, 4, 12, 53, '169', 10000014, 30870, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 0),
(45, 1, 4, 12, 53, '171', 10000014, 15000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 0),
(46, 1, 4, 12, 53, '173', 10000014, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 0),
(47, 1, 4, 12, 53, '190', 10000014, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 0),
(48, 1, 4, 12, 53, '174', 10000014, 483500, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 0),
(49, 1, 4, 12, 53, '175', 10000014, 12000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 0),
(50, 1, 4, 12, 51, '167', 10000014, 22050, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 0),
(51, 1, 4, 12, 51, '168', 10000014, 30870, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 0),
(52, 1, 4, 12, 51, '169', 10000014, 30870, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 0),
(53, 1, 4, 12, 51, '171', 10000014, 15000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 0),
(54, 1, 4, 12, 51, '173', 10000014, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 0),
(55, 1, 4, 12, 51, '190', 10000014, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 0),
(56, 1, 4, 12, 51, '174', 10000014, 483500, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 0),
(57, 1, 4, 12, 51, '175', 10000014, 12000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 0),
(58, 1, 4, 10, 101, '149', 10000012, 22050, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 0),
(59, 1, 4, 10, 101, '150', 10000012, 30870, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 0),
(60, 1, 4, 10, 101, '151', 10000012, 30870, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 0),
(61, 1, 4, 10, 101, '153', 10000012, 15000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 0),
(62, 1, 4, 10, 101, '155', 10000012, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 0),
(63, 1, 4, 10, 101, '186', 10000012, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 0),
(64, 1, 4, 10, 101, '156', 10000012, 483500, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 0),
(65, 1, 4, 10, 101, '157', 10000012, 12000, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 0),
(66, 1, 4, 2, 104, '11', 10000020, 17350, NULL, 0, 1, 'null', 0, 0, 1, 70, 70, '2020-10-09 06:21:05', '2020-10-09 12:08:40', 0),
(67, 1, 4, 2, 104, '12', 10000020, 36382, NULL, 0, 1, 'null', 0, 0, 1, 70, 70, '2020-10-09 06:21:05', '2020-10-09 12:08:40', 0),
(68, 1, 4, 2, 104, '13', 10000020, 36382, NULL, 0, 1, 'null', 0, 0, 1, 70, 70, '2020-10-09 06:21:05', '2020-10-09 12:08:40', 0),
(69, 1, 4, 2, 104, '15', 10000020, 10000, NULL, 0, 1, 'null', 0, 0, 1, 70, 70, '2020-10-09 06:21:05', '2020-10-09 12:08:40', 0),
(70, 1, 4, 2, 104, '17', 10000020, 0, NULL, 0, 1, 'null', 0, 0, 1, 70, 70, '2020-10-09 06:21:05', '2020-10-09 12:08:40', 0),
(71, 1, 4, 2, 104, '18', 10000020, 483500, 1, 0, 1, 'null', 0, 0, 1, 70, 70, '2020-10-09 06:21:05', '2020-10-09 12:08:40', 0),
(72, 1, 4, 2, 104, '19', 10000020, 12000, NULL, 0, 1, 'null', 0, 0, 1, 70, 70, '2020-10-09 06:21:05', '2020-10-09 12:08:40', 0),
(73, 1, 7, 9, 105, '136', 10000011, 17350, 1, 100, 1, 'disc', 0, 0, 1, 67, 67, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 0),
(74, 1, 7, 9, 105, '137', 10000011, 36382, 1, 0, 1, 'null', 0, 0, 1, 67, 67, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 0),
(75, 1, 7, 9, 105, '138', 10000011, 36382, 1, 0, 1, 'null', 0, 0, 1, 67, 67, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 0),
(76, 1, 7, 9, 105, '141', 10000011, 10000, 1, 0, 1, 'null', 0, 0, 1, 67, 67, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 0),
(77, 1, 7, 9, 105, '143', 10000011, 0, 1, 0, 1, 'null', 0, 0, 1, 67, 67, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 0),
(78, 1, 7, 9, 105, '145', 10000011, 0, 1, 0, 1, 'null', 0, 0, 1, 67, 67, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 0),
(79, 1, 7, 9, 105, '146', 10000011, 483500, 1, 0, 1, 'null', 0, 0, 1, 67, 67, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 0),
(80, 1, 7, 9, 105, '147', 10000011, 12000, 1, 0, 1, 'null', 0, 0, 1, 67, 67, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 0),
(88, 1, 7, 10, 106, '149', 10000012, 22050, NULL, 30, 1, 'dic', 0, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 0),
(89, 1, 7, 10, 106, '150', 10000012, 30870, NULL, 20, 1, 'disc', 0, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 0),
(90, 1, 7, 10, 106, '151', 10000012, 30870, NULL, 15, 1, 'disc', 0, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 0),
(91, 1, 7, 10, 106, '153', 10000012, 15000, NULL, 20, 1, 'disc', 0, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 0),
(92, 1, 7, 10, 106, '155', 10000012, 0, NULL, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 0),
(93, 1, 7, 10, 106, '186', 10000012, 0, NULL, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 0),
(94, 1, 7, 10, 106, '156', 10000012, 483500, 1, 30, 1, 'disc', 0, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 0),
(95, 1, 7, 10, 106, '157', 10000012, 12000, NULL, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 0),
(104, 1, 4, 7, 19, '114', 10000009, 17350, NULL, 0, 2, NULL, 0, 0, 0, 6, NULL, '2020-10-12 17:38:24', '2020-10-12 17:38:24', 0),
(105, 1, 4, 7, 19, '115', 10000009, 36382, NULL, 0, 2, NULL, 0, 0, 0, 6, NULL, '2020-10-12 17:38:24', '2020-10-12 17:38:24', 0),
(106, 1, 4, 7, 19, '116', 10000009, 36382, NULL, 0, 2, NULL, 0, 0, 0, 6, NULL, '2020-10-12 17:38:24', '2020-10-12 17:38:24', 0),
(107, 1, 4, 7, 19, '119', 10000009, 10000, NULL, 0, 2, NULL, 0, 0, 0, 6, NULL, '2020-10-12 17:38:24', '2020-10-12 17:38:24', 0),
(108, 1, 4, 7, 19, '121', 10000009, 0, NULL, 0, 2, NULL, 0, 0, 0, 6, NULL, '2020-10-12 17:38:24', '2020-10-12 17:38:24', 0),
(109, 1, 4, 7, 19, '122', 10000009, 0, NULL, 0, 2, NULL, 0, 0, 0, 6, NULL, '2020-10-12 17:38:24', '2020-10-12 17:38:24', 0),
(110, 1, 4, 7, 19, '123', 10000009, 483500, 1, 80, 2, 'Tuition Fee', 0, 0, 0, 6, NULL, '2020-10-12 17:38:24', '2020-10-12 17:38:24', 0),
(111, 1, 4, 7, 19, '124', 10000009, 1200, NULL, 0, 2, NULL, 0, 0, 0, 6, NULL, '2020-10-12 17:38:24', '2020-10-12 17:38:24', 0),
(119, 1, 7, 2, 6, '11', 10000020, 17350, 1, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04', 0),
(120, 1, 7, 2, 6, '12', 10000020, 36382, 1, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04', 0),
(121, 1, 7, 2, 6, '13', 10000020, 36382, 1, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04', 0),
(122, 1, 7, 2, 6, '15', 10000020, 10000, 1, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04', 0),
(123, 1, 7, 2, 6, '17', 10000020, 0, 1, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04', 0),
(124, 1, 7, 2, 6, '18', 10000020, 483500, 1, 40, 1, 'Tuition Fee', 0, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04', 0),
(125, 1, 7, 2, 6, '19', 10000020, 12000, 1, 0, 1, NULL, 0, 0, 0, 67, NULL, '2020-10-13 04:55:04', '2020-10-13 04:55:04', 0),
(126, 1, 7, 3, 107, '39', 10000021, 17350, NULL, 50, 1, 'Approved by Pr', 0, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55', 0),
(127, 1, 7, 3, 107, '40', 10000021, 36382, NULL, 100, 1, 'Approved by Pr', 0, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55', 0),
(128, 1, 7, 3, 107, '41', 10000021, 36382, NULL, 100, 1, 'Approved by Pr', 0, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55', 0),
(129, 1, 7, 3, 107, '43', 10000021, 10000, NULL, 0, 1, NULL, 0, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55', 0),
(130, 1, 7, 3, 107, '45', 10000021, 0, NULL, 0, 1, NULL, 0, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55', 0),
(131, 1, 7, 3, 107, '46', 10000021, 483500, 1, 40, 1, 'Approved by Pr', 0, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55', 0),
(132, 1, 7, 3, 107, '48', 10000021, 12000, NULL, 40, 1, 'Approved by Pr', 0, 0, 0, 4, NULL, '2020-10-13 08:20:55', '2020-10-13 08:20:55', 0),
(133, 1, 7, 18, 108, '49', 10000016, 22050, 1, 0, 1, NULL, 0, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(134, 1, 7, 18, 108, '51', 10000016, 37898, 1, 100, 1, 'Scholarship', 1, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(135, 1, 7, 18, 108, '52', 10000016, 37898, 1, 100, 1, 'Scholarship', 1, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(136, 1, 7, 18, 108, '55', 10000016, 33000, 1, 0, 1, NULL, 0, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(137, 1, 7, 18, 108, '57', 10000016, 0, 1, 0, 1, NULL, 0, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(138, 1, 7, 18, 108, '58', 10000016, 0, 1, 0, 1, NULL, 0, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(140, 1, 7, 18, 108, '60', 10000016, 505128, 1, 100, 1, 'Scholarship', 1, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(141, 1, 7, 18, 108, '61', 10000016, 59868, 1, 0, 1, NULL, 0, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(142, 1, 7, 18, 108, '62', 10000016, 12000, 1, 0, 1, NULL, 0, 1, 0, 4, NULL, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 0),
(143, 1, 4, 1, 4, '206', 10000019, 99999, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29', 0),
(144, 1, 4, 1, 4, '207', 10000019, 99999, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29', 0),
(145, 1, 4, 1, 4, '209', 10000019, 0, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29', 0),
(146, 1, 4, 1, 4, '8', 10000019, 99999, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29', 0),
(147, 1, 4, 1, 4, '198', 10000019, 99999, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29', 0),
(148, 1, 4, 1, 4, '199', 10000019, 99999, 1, 0, 1, NULL, 0, 0, 0, 70, NULL, '2020-10-15 05:27:29', '2020-10-15 05:27:29', 0),
(149, 1, 4, 1, 2, '206', 10000030, 1000, 1, 0, 1, 'ssdas', 0, 0, 0, 1, NULL, '2021-02-26 13:48:44', '2021-02-26 13:48:44', 0),
(150, 1, 4, 1, 2, '213', 10000030, 2000, 1, 0, 1, 'asdasd', 0, 0, 0, 1, NULL, '2021-02-26 13:48:44', '2021-02-26 13:48:44', 0),
(151, 1, 4, 1, 2, '214', 10000030, 3000, 1, 0, 1, 'asdasd', 0, 0, 0, 1, NULL, '2021-02-26 13:48:44', '2021-02-26 13:48:44', 0),
(152, 1, 4, 1, 2, '215', 10000030, 4000, 1, 0, 1, 'asdasd', 0, 0, 0, 1, NULL, '2021-02-26 13:48:44', '2021-02-26 13:48:44', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fee_master_basics`
--

CREATE TABLE `fee_master_basics` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(20) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `program_id` int(10) UNSIGNED DEFAULT NULL,
  `installment_id` int(10) UNSIGNED DEFAULT NULL,
  `fee_structure_code` int(11) NOT NULL,
  `fee_type` int(10) UNSIGNED DEFAULT NULL,
  `bill_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_total` int(11) DEFAULT NULL,
  `term_total` int(11) DEFAULT NULL,
  `discount_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_after_discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_type` int(11) NOT NULL DEFAULT 1,
  `special_discount_comment` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_discount_type` int(11) DEFAULT 0,
  `scholarship` int(11) DEFAULT 0,
  `scholarship_no_of_a` int(11) DEFAULT 0,
  `voucher_date` date DEFAULT NULL,
  `fee_due_date_start` date DEFAULT NULL,
  `fee_due_date_end` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `ceo_approval` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_master_basics`
--

INSERT INTO `fee_master_basics` (`id`, `session_id`, `branch_id`, `student_id`, `program_id`, `installment_id`, `fee_structure_code`, `fee_type`, `bill_no`, `basic_fee`, `total_fee`, `session_total`, `term_total`, `discount_fee`, `total_after_discount`, `discount_type`, `special_discount_comment`, `other_discount_type`, `scholarship`, `scholarship_no_of_a`, `voucher_date`, `fee_due_date_start`, `fee_due_date_end`, `status`, `ceo_approval`, `created_at`, `updated_at`, `created_by`, `last_updated_by`) VALUES
(1, 1, 4, 33, 13, 0, 10000015, 0, NULL, '0', '594290', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-08 11:51:46', '2020-10-08 11:51:46', 70, NULL),
(2, 1, 4, 36, 18, 0, 10000016, 0, NULL, '0', '722842', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-08 11:52:19', '2020-10-08 11:52:19', 70, NULL),
(3, 1, 4, 5, 2, 0, 10000020, 0, NULL, '0', '595614', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-08 11:53:15', '2020-10-08 11:53:15', 70, NULL),
(4, 1, 4, 10, 4, 0, 10000006, 0, NULL, '0', '595614', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-08 11:54:25', '2020-10-08 11:54:25', 70, NULL),
(5, 1, 4, 16, 6, 0, 10000008, 0, NULL, '0', '595614', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-08 11:55:02', '2020-10-08 11:55:02', 70, NULL),
(6, 1, 4, 53, 12, 0, 10000014, 0, NULL, '0', '594290', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-08 11:58:17', '2020-10-08 11:58:17', 70, NULL),
(7, 1, 4, 51, 12, 0, 10000014, 0, NULL, '0', '594290', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-08 11:59:16', '2020-10-08 11:59:16', 70, NULL),
(8, 1, 4, 101, 10, 0, 10000012, 0, NULL, '0', '594290', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-08 12:07:48', '2020-10-08 12:07:48', 70, NULL),
(9, 1, 4, 104, 2, 0, 10000020, 0, NULL, '0', '595614', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 1, NULL, '2020-10-09 06:21:05', '2020-10-09 12:08:40', 70, 70),
(10, 1, 7, 105, 9, 0, 10000011, 0, NULL, '0', '595614', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 1, NULL, '2020-10-12 08:24:09', '2020-10-12 10:50:49', 67, 67),
(12, 1, 7, 106, 10, 0, 10000012, 0, NULL, '0', '594290', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, '2020-10-12 11:14:58', '2020-10-12 11:14:58', 67, NULL),
(14, 1, 4, 19, 7, 0, 10000009, 0, NULL, '0', '584814', NULL, 0, '0', '0', 2, 'Parents Financial Issue', 0, 0, 0, NULL, NULL, NULL, 0, 1, '2020-10-12 17:38:24', '2020-10-12 17:41:14', 6, 6),
(16, 1, 7, 6, 2, 0, 10000020, 0, NULL, '0', '595614', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, 1, '2020-10-13 04:55:04', '2020-10-13 04:55:04', 67, NULL),
(17, 1, 7, 107, 3, 0, 10000021, 0, NULL, '0', '595614', NULL, 0, '0', '0', 1, NULL, 2, 0, 0, NULL, NULL, NULL, 0, 1, '2020-10-13 08:20:55', '2020-10-13 08:20:55', 4, NULL),
(18, 1, 7, 108, 18, 0, 10000016, 0, NULL, '0', '722842', NULL, 0, '0', '0', 1, NULL, 0, 1, 1, NULL, NULL, NULL, 0, 1, '2020-10-13 09:11:06', '2020-10-13 09:11:06', 4, NULL),
(19, 1, 4, 4, 1, 0, 10000019, 0, NULL, '0', '499995', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 0, 1, '2020-10-15 05:27:29', '2020-10-15 05:27:29', 70, NULL),
(20, 1, 4, 2, 1, 0, 10000030, 0, NULL, '0', '10000', NULL, 0, '0', '0', 1, NULL, 0, 0, 0, NULL, NULL, NULL, 1, 1, '2021-02-26 13:11:47', '2021-02-26 13:50:55', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fee_scholarship_defination`
--

CREATE TABLE `fee_scholarship_defination` (
  `id` int(11) NOT NULL,
  `session_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `no_of_a` int(11) NOT NULL,
  `term_percentage` float DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `last_updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee_scholarship_defination`
--

INSERT INTO `fee_scholarship_defination` (`id`, `session_id`, `branch_id`, `program_id`, `title`, `no_of_a`, `term_percentage`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 7, 18, '8 As', 8, 0, 1, 70, 70, '2020-09-21 07:23:59', '2020-09-21 08:07:31'),
(2, 1, 7, 18, '7 As', 7, 0, 1, 70, 70, '2020-09-21 07:37:18', '2020-09-21 08:16:48'),
(3, 1, 7, 18, '6 As', 6, 0, 1, 70, 70, '2020-09-21 07:38:55', '2020-09-21 08:17:10'),
(4, 1, 7, 18, '5 As', 5, 0, 1, 70, 70, '2020-09-21 07:39:46', '2020-09-21 08:17:26'),
(5, 1, 7, 19, '3 As', 3, 0, 1, 70, 70, '2020-09-21 07:41:16', '2020-09-21 08:17:39'),
(6, 1, 7, 19, '2 As', 2, 0, 1, 70, 70, '2020-09-21 07:42:23', '2020-09-21 08:18:06'),
(7, 1, 4, 18, '8 A', 8, 0, 1, 70, NULL, '2020-09-30 03:02:20', '2020-09-30 03:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `fee_scholarship_details`
--

CREATE TABLE `fee_scholarship_details` (
  `id` int(11) NOT NULL,
  `fsd_id` int(11) NOT NULL,
  `fee_head_id` int(11) NOT NULL,
  `discount_percentage` float NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee_scholarship_details`
--

INSERT INTO `fee_scholarship_details` (`id`, `fsd_id`, `fee_head_id`, `discount_percentage`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 51, 100, 1, 70, NULL, '2020-09-21 07:24:43', '2020-09-21 07:24:43'),
(2, 1, 52, 100, 1, 70, NULL, '2020-09-21 07:24:52', '2020-09-21 07:24:52'),
(3, 1, 60, 100, 1, 70, NULL, '2020-09-21 07:25:05', '2020-09-21 07:25:05'),
(4, 2, 60, 75, 1, 70, NULL, '2020-09-21 07:37:54', '2020-09-21 07:37:54'),
(5, 2, 51, 50, 1, 70, NULL, '2020-09-21 07:38:11', '2020-09-21 07:38:11'),
(6, 2, 52, 50, 1, 70, NULL, '2020-09-21 07:38:17', '2020-09-21 07:38:17'),
(7, 3, 60, 50, 1, 70, NULL, '2020-09-21 07:39:09', '2020-09-21 07:39:09'),
(8, 3, 51, 50, 1, 70, NULL, '2020-09-21 07:39:18', '2020-09-21 07:39:18'),
(9, 3, 52, 50, 1, 70, NULL, '2020-09-21 07:39:24', '2020-09-21 07:39:24'),
(10, 4, 60, 25, 1, 70, NULL, '2020-09-21 07:40:05', '2020-09-21 07:40:05'),
(11, 4, 51, 50, 1, 70, NULL, '2020-09-21 07:40:19', '2020-09-21 07:40:19'),
(12, 4, 52, 50, 1, 70, NULL, '2020-09-21 07:40:26', '2020-09-21 07:40:26'),
(13, 5, 78, 100, 1, 70, NULL, '2020-09-21 07:41:38', '2020-09-21 07:41:38'),
(14, 5, 71, 100, 1, 70, NULL, '2020-09-21 07:41:52', '2020-09-21 07:41:52'),
(15, 5, 70, 100, 1, 70, NULL, '2020-09-21 07:41:59', '2020-09-21 07:41:59'),
(16, 6, 78, 75, 1, 70, NULL, '2020-09-21 07:42:40', '2020-09-21 07:42:40'),
(17, 6, 71, 50, 1, 70, NULL, '2020-09-21 07:42:47', '2020-09-21 07:42:47'),
(18, 6, 70, 50, 1, 70, NULL, '2020-09-21 07:43:03', '2020-09-21 07:43:03'),
(19, 7, 49, 100, 1, 70, NULL, '2020-09-30 03:02:32', '2020-09-30 03:02:32'),
(20, 7, 51, 100, 1, 70, NULL, '2020-09-30 03:02:42', '2020-09-30 03:02:42'),
(21, 7, 60, 100, 1, 70, NULL, '2020-09-30 03:02:59', '2020-09-30 03:02:59');

-- --------------------------------------------------------

--
-- Table structure for table `fee_sections`
--

CREATE TABLE `fee_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_sections`
--

INSERT INTO `fee_sections` (`id`, `branch_id`, `title`, `slug`, `type`, `status`, `created_at`, `updated_at`, `created_by`, `last_updated_by`) VALUES
(1, 7, 'One Time Fee (At the time of Admission)', 'One Time Fee', 1, '1', NULL, NULL, 0, NULL),
(2, 7, 'Once in Year', 'Once in Year', 1, '1', NULL, NULL, 0, NULL),
(3, 7, 'Semester Fee', 'Semester Fee', 1, '1', NULL, NULL, 0, NULL),
(5, 7, 'Fee at Glance (One Academic Fee)', 'Fee-at-Glance-(One-Academic-Fee)', 1, '1', '2020-09-01 00:38:24', '2020-10-14 07:51:10', 6, 70),
(6, 7, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:01:33', '2020-10-01 00:01:33', 70, NULL),
(44, 1, 'One  Time  Fee (At the  Time  of Addmission)', 'One-Time-Fee-(At-the-Time-of-Addmission)', 1, '1', '2021-03-02 08:01:15', '2021-03-02 08:01:15', 1, NULL),
(8, 1, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:02:04', '2020-10-01 00:02:04', 70, NULL),
(9, 1, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:02:18', '2020-10-01 00:02:18', 70, NULL),
(10, 2, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:02:30', '2020-10-01 00:02:30', 70, NULL),
(11, 2, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:03:29', '2020-10-01 00:03:29', 70, NULL),
(12, 2, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:03:42', '2020-10-01 00:03:42', 70, NULL),
(13, 3, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:03:58', '2020-10-01 00:03:58', 70, NULL),
(14, 3, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:04:13', '2020-10-01 00:04:13', 70, NULL),
(15, 3, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:04:27', '2020-10-01 00:04:27', 70, NULL),
(16, 4, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:04:52', '2020-10-01 00:04:52', 70, NULL),
(17, 4, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:05:05', '2020-10-01 00:05:05', 70, NULL),
(18, 4, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:05:21', '2020-10-01 00:05:21', 70, NULL),
(19, 5, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:05:37', '2020-10-01 00:05:37', 70, NULL),
(20, 5, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:05:53', '2020-10-01 00:05:53', 70, NULL),
(21, 5, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:06:14', '2020-10-01 00:06:14', 70, NULL),
(22, 6, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:06:28', '2020-10-01 00:06:28', 70, NULL),
(23, 6, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:07:25', '2020-10-01 00:07:25', 70, NULL),
(24, 6, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:07:44', '2020-10-01 00:07:44', 70, NULL),
(25, 8, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:08:16', '2020-10-01 00:08:16', 70, NULL),
(26, 8, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:08:37', '2020-10-01 00:08:37', 70, NULL),
(27, 8, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:08:48', '2020-10-01 00:08:48', 70, NULL),
(28, 9, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:09:06', '2020-10-01 00:09:06', 70, NULL),
(29, 9, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:09:15', '2020-10-01 00:09:15', 70, NULL),
(30, 9, 'Term Fee', 'Term-Fee', 1, '1', '2020-10-01 00:09:30', '2020-10-01 00:09:30', 70, NULL),
(31, 10, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:09:42', '2020-10-01 00:09:42', 70, NULL),
(32, 10, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:09:54', '2020-10-01 00:09:54', 70, NULL),
(33, 10, 'Semester Fee', 'Semester-Fee', 1, '1', '2020-10-01 00:10:26', '2020-10-01 00:10:26', 70, NULL),
(34, 11, 'Semester Fee', 'Semester-Fee', 1, '1', '2020-10-01 00:10:33', '2020-10-01 00:10:33', 70, NULL),
(35, 11, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:10:43', '2020-10-01 00:10:43', 70, NULL),
(36, 11, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:10:55', '2020-10-01 00:10:55', 70, NULL),
(37, 12, 'One Time Fee (At the time of Admission)', 'One-Time-Fee-(At-the-time-of-Admission)', 1, '1', '2020-10-01 00:11:05', '2020-10-01 00:11:05', 70, NULL),
(38, 12, 'Once in Year', 'Once-in-Year', 1, '1', '2020-10-01 00:11:26', '2020-10-01 00:11:26', 70, NULL),
(39, 0, 'Semester Fee', 'Semester-Fee', 1, '1', '2020-10-01 00:11:40', '2021-03-01 15:15:12', 70, 17),
(40, 4, 'One Time FEE', 'One-Time-FEE', 1, '1', '2021-02-26 07:12:21', '2021-02-26 07:12:21', 1, NULL),
(41, 1, 'One Time  Fee', 'One-Time-Fee', 1, '1', '2021-03-01 11:58:23', '2021-03-01 11:58:23', 84, NULL),
(42, 7, 'One  Time  Fee  (at the  time  of Admission)', 'One-Time-Fee-(at-the-time-of-Admission)', 1, '1', '2021-03-01 12:39:13', '2021-03-01 12:39:13', 1, NULL),
(43, 1, 'one  Time Fee', 'one-Time-Fee', 1, '1', '2021-03-02 07:42:04', '2021-03-02 07:42:04', 1, NULL),
(45, 1, 'Once in a  Year', 'Once-in-a-Year', 1, '1', '2021-03-02 08:01:47', '2021-03-02 08:01:47', 1, NULL),
(46, 1, 'Fee at Glance', 'Fee-at-Glance', 1, '1', '2021-03-02 08:02:07', '2021-03-02 08:02:07', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fee_structures`
--

CREATE TABLE `fee_structures` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED DEFAULT NULL,
  `fee_structure_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee_head_id` int(10) UNSIGNED NOT NULL,
  `fee_amount` double(8,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_structures`
--

INSERT INTO `fee_structures` (`id`, `created_at`, `updated_at`, `branch_id`, `session_id`, `program_id`, `term_id`, `fee_structure_code`, `fee_head_id`, `fee_amount`, `status`, `last_updated_by`) VALUES
(244, NULL, NULL, 7, 1, 3, NULL, '10000021', 44, 0.00, 1, 6),
(243, NULL, NULL, 4, 1, 3, NULL, '10000021', 43, 10000.00, 1, 6),
(242, NULL, NULL, 4, 1, 3, NULL, '10000021', 202, 0.00, 1, 6),
(241, NULL, NULL, 7, 1, 3, NULL, '10000021', 42, 0.00, 1, 6),
(240, NULL, NULL, 7, 1, 3, NULL, '10000021', 41, 36382.00, 1, 6),
(239, NULL, NULL, 7, 1, 3, NULL, '10000021', 40, 36382.00, 1, 6),
(238, NULL, NULL, 7, 1, 3, NULL, '10000021', 39, 17350.00, 1, 6),
(237, NULL, NULL, 7, 1, 2, NULL, '10000020', 19, 12000.00, 1, 6),
(236, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000020', 18, 555.00, 1, 1),
(235, NULL, NULL, 7, 1, 2, NULL, '10000020', 201, 0.00, 1, 6),
(234, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000020', 17, 123456.00, 1, 1),
(233, NULL, NULL, 7, 1, 2, NULL, '10000020', 16, 0.00, 1, 6),
(232, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000020', 15, 4654.00, 1, 1),
(231, NULL, NULL, 7, 1, 2, NULL, '10000020', 200, 0.00, 1, 6),
(230, NULL, NULL, 7, 1, 2, NULL, '10000020', 47, 0.00, 1, 6),
(229, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000020', 13, 36382.00, 1, 1),
(228, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000020', 12, 36382.00, 1, 1),
(227, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000020', 11, 17350.00, 1, 1),
(47, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 81, 17350.00, 1, 6),
(48, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 82, 36382.00, 1, 6),
(49, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 83, 36382.00, 1, 6),
(50, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 84, 0.00, 1, 6),
(51, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 85, 0.00, 1, 6),
(52, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 88, 0.00, 1, 6),
(53, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 89, 0.00, 1, 6),
(54, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 86, 10000.00, 1, 6),
(55, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 87, 0.00, 1, 6),
(56, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 90, 483500.00, 1, 6),
(57, NULL, '2020-09-02 01:48:00', 7, 1, 4, 0, '10000006', 91, 12000.00, 1, 6),
(58, NULL, '2020-09-09 01:13:02', 7, 1, 5, 0, '10000007', 95, 0.00, 1, 6),
(59, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 92, 17350.00, 1, 6),
(60, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 93, 36382.00, 1, 6),
(61, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 94, 36382.00, 1, 6),
(62, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 96, 0.00, 1, 6),
(63, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 99, 0.00, 1, 6),
(64, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 100, 0.00, 1, 6),
(65, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 97, 10000.00, 1, 6),
(66, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 98, 0.00, 1, 6),
(67, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 101, 483500.00, 1, 6),
(68, NULL, '2020-09-09 01:13:03', 7, 1, 5, 0, '10000007', 102, 12000.00, 1, 6),
(69, NULL, '2020-09-02 02:51:56', 7, 1, 6, 0, '10000008', 103, 17350.00, 1, 6),
(70, NULL, '2020-09-02 02:51:56', 7, 1, 6, 0, '10000008', 104, 36382.00, 1, 6),
(71, NULL, '2020-09-02 02:51:56', 7, 1, 6, 0, '10000008', 105, 36382.00, 1, 6),
(72, NULL, '2020-09-02 02:51:56', 7, 1, 6, 0, '10000008', 106, 0.00, 1, 6),
(73, NULL, '2020-09-02 02:51:56', 7, 1, 6, 0, '10000008', 107, 0.00, 1, 6),
(74, NULL, '2020-09-02 02:51:56', 7, 1, 6, 0, '10000008', 110, 0.00, 1, 6),
(75, NULL, '2020-09-02 02:51:56', 7, 1, 6, 0, '10000008', 111, 0.00, 1, 6),
(76, NULL, '2020-09-02 02:51:57', 7, 1, 6, 0, '10000008', 108, 10000.00, 1, 6),
(77, NULL, '2020-09-02 02:51:57', 7, 1, 6, 0, '10000008', 109, 0.00, 1, 6),
(78, NULL, '2020-09-02 02:51:57', 7, 1, 6, 0, '10000008', 112, 483500.00, 1, 6),
(79, NULL, '2020-09-02 02:51:57', 7, 1, 6, 0, '10000008', 113, 12000.00, 1, 6),
(80, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 114, 17350.00, 1, 6),
(81, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 115, 36382.00, 1, 6),
(82, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 116, 36382.00, 1, 6),
(83, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 117, 0.00, 1, 6),
(84, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 118, 0.00, 1, 6),
(85, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 121, 0.00, 1, 6),
(86, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 122, 0.00, 1, 6),
(87, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 119, 10000.00, 1, 6),
(88, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 120, 0.00, 1, 6),
(89, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 123, 483500.00, 1, 6),
(90, NULL, '2020-09-02 02:55:18', 7, 1, 7, 0, '10000009', 124, 1200.00, 1, 6),
(91, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 125, 17350.00, 1, 6),
(92, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 126, 36382.00, 1, 6),
(93, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 127, 36382.00, 1, 6),
(94, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 128, 0.00, 1, 6),
(95, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 129, 0.00, 1, 6),
(96, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 132, 0.00, 1, 6),
(97, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 133, 0.00, 1, 6),
(98, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 130, 10000.00, 1, 6),
(99, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 131, 0.00, 1, 6),
(100, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 134, 483500.00, 1, 6),
(101, NULL, '2020-09-03 04:07:26', 7, 1, 8, 0, '10000010', 135, 12000.00, 1, 6),
(102, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 136, 17350.00, 1, 6),
(103, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 137, 36382.00, 1, 6),
(104, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 138, 36382.00, 1, 6),
(105, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 139, 0.00, 1, 6),
(106, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 140, 0.00, 1, 6),
(107, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 143, 0.00, 1, 6),
(108, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 145, 0.00, 1, 6),
(109, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 141, 10000.00, 1, 6),
(110, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 142, 0.00, 1, 6),
(111, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 146, 483500.00, 1, 6),
(112, NULL, '2020-09-02 02:59:08', 7, 1, 9, 0, '10000011', 147, 12000.00, 1, 6),
(113, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 185, 0.00, 1, 6),
(114, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 152, 0.00, 1, 6),
(115, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 151, 30870.00, 1, 6),
(116, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 150, 30870.00, 1, 6),
(117, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 149, 22050.00, 1, 6),
(118, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 155, 0.00, 1, 6),
(119, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 154, 0.00, 1, 6),
(120, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 153, 15000.00, 1, 6),
(121, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 186, 0.00, 1, 6),
(122, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 156, 483500.00, 1, 6),
(123, NULL, '2020-09-03 02:01:39', 7, 1, 10, 0, '10000012', 157, 12000.00, 1, 6),
(124, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 158, 22050.00, 1, 6),
(125, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 159, 30870.00, 1, 6),
(126, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 160, 30870.00, 1, 6),
(127, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 187, 0.00, 1, 6),
(128, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 161, 0.00, 1, 6),
(129, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 188, 0.00, 1, 6),
(130, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 162, 15000.00, 1, 6),
(131, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 163, 0.00, 1, 6),
(132, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 164, 0.00, 1, 6),
(133, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 165, 483500.00, 1, 6),
(134, NULL, '2020-09-03 02:04:17', 7, 1, 11, 0, '10000013', 166, 12000.00, 1, 6),
(135, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 167, 22050.00, 1, 6),
(136, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 168, 30870.00, 1, 6),
(137, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 169, 30870.00, 1, 6),
(138, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 189, 0.00, 1, 6),
(139, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 170, 0.00, 1, 6),
(140, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 190, 0.00, 1, 6),
(141, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 171, 15000.00, 1, 6),
(142, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 172, 0.00, 1, 6),
(143, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 173, 0.00, 1, 6),
(144, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 174, 483500.00, 1, 6),
(145, NULL, '2020-09-03 02:06:01', 7, 1, 12, 0, '10000014', 175, 12000.00, 1, 6),
(146, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 176, 22050.00, 1, 6),
(147, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 177, 30870.00, 1, 6),
(148, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 178, 30870.00, 1, 6),
(149, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 191, 0.00, 1, 6),
(150, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 179, 0.00, 1, 6),
(151, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 192, 0.00, 1, 6),
(152, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 180, 15000.00, 1, 6),
(153, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 181, 0.00, 1, 6),
(154, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 182, 0.00, 1, 6),
(155, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 183, 483500.00, 1, 6),
(156, NULL, '2020-09-03 02:08:02', 7, 1, 13, 0, '10000015', 184, 12000.00, 1, 6),
(157, NULL, '2020-09-03 02:50:37', 7, 1, 18, 0, '10000016', 49, 22050.00, 1, 6),
(158, NULL, '2020-09-03 02:50:37', 7, 1, 18, 0, '10000016', 51, 37898.00, 1, 6),
(159, NULL, '2020-09-03 02:50:37', 7, 1, 18, 0, '10000016', 52, 37898.00, 1, 6),
(160, NULL, '2020-09-03 02:50:37', 7, 1, 18, 0, '10000016', 53, 0.00, 1, 6),
(161, NULL, '2020-09-03 02:50:38', 7, 1, 18, 0, '10000016', 54, 0.00, 1, 6),
(162, NULL, '2020-09-03 02:50:38', 7, 1, 18, 0, '10000016', 57, 0.00, 1, 6),
(163, NULL, '2020-09-03 02:50:38', 7, 1, 18, 0, '10000016', 58, 0.00, 1, 6),
(164, NULL, '2020-09-03 02:50:38', 7, 1, 18, 0, '10000016', 55, 33000.00, 1, 6),
(165, NULL, '2020-09-03 02:50:38', 7, 1, 18, 0, '10000016', 59, 15000.00, 1, 6),
(166, NULL, '2020-09-03 02:50:38', 7, 1, 18, 0, '10000016', 60, 505128.00, 1, 6),
(167, NULL, '2020-09-03 02:50:38', 7, 1, 18, 0, '10000016', 61, 59868.00, 1, 6),
(168, NULL, '2020-09-03 02:50:38', 7, 1, 18, 0, '10000016', 62, 12000.00, 1, 6),
(169, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 73, 0.00, 1, 6),
(170, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 71, 37898.00, 1, 6),
(171, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 69, 22050.00, 1, 6),
(172, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 72, 0.00, 1, 6),
(173, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 70, 37898.00, 1, 6),
(174, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 76, 0.00, 1, 6),
(175, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 74, 33000.00, 1, 6),
(176, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 75, 15000.00, 1, 6),
(177, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 77, 0.00, 1, 6),
(178, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 79, 59868.00, 1, 6),
(179, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 80, 12000.00, 1, 6),
(180, NULL, '2020-09-03 02:53:47', 7, 1, 19, 0, '10000017', 78, 505128.00, 1, 6),
(181, NULL, NULL, 7, 1, 13, NULL, '10000018', 176, 12000.00, 1, 14),
(182, NULL, NULL, 7, 1, 13, NULL, '10000018', 177, 454654.00, 1, 14),
(183, NULL, NULL, 7, 1, 13, NULL, '10000018', 178, 546546.00, 1, 14),
(184, NULL, NULL, 7, 1, 13, NULL, '10000018', 179, 8889.00, 1, 14),
(185, NULL, NULL, 7, 1, 13, NULL, '10000018', 191, 5654.00, 1, 14),
(186, NULL, NULL, 7, 1, 13, NULL, '10000018', 180, 448.00, 1, 14),
(187, NULL, NULL, 7, 1, 13, NULL, '10000018', 181, 668.00, 1, 14),
(188, NULL, NULL, 7, 1, 13, NULL, '10000018', 182, 6546.00, 1, 14),
(189, NULL, NULL, 7, 1, 13, NULL, '10000018', 192, 885.00, 1, 14),
(190, NULL, NULL, 7, 1, 13, NULL, '10000018', 183, 5558.00, 1, 14),
(191, NULL, NULL, 7, 1, 13, NULL, '10000018', 184, 6669.00, 1, 14),
(249, NULL, NULL, 7, 1, 3, NULL, '10000021', 48, 12000.00, 1, 6),
(248, NULL, NULL, 7, 1, 3, NULL, '10000021', 46, 483500.00, 1, 6),
(247, NULL, NULL, 7, 1, 3, NULL, '10000021', 204, 0.00, 1, 6),
(246, NULL, NULL, 7, 1, 3, NULL, '10000021', 203, 0.00, 1, 6),
(245, NULL, NULL, 7, 1, 3, NULL, '10000021', 45, 0.00, 1, 6),
(226, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000019', 199, 6000.00, 1, 1),
(225, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000019', 198, 99999.00, 1, 1),
(224, NULL, '2020-10-01 01:02:41', 7, 1, 1, NULL, '10000019', 194, 0.00, 1, 70),
(223, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000019', 8, 99999.00, 1, 1),
(222, NULL, NULL, 7, 1, 1, NULL, '10000019', 7, 0.00, 1, 6),
(221, NULL, NULL, 7, 1, 1, NULL, '10000019', 6, 10000.00, 1, 6),
(220, NULL, NULL, 7, 1, 1, NULL, '10000019', 193, 0.00, 1, 6),
(219, NULL, NULL, 7, 1, 1, NULL, '10000019', 148, 17350.00, 1, 6),
(218, NULL, NULL, 7, 1, 1, NULL, '10000019', 4, 0.00, 1, 6),
(217, NULL, NULL, 7, 1, 1, NULL, '10000019', 3, 36382.00, 1, 6),
(216, NULL, NULL, 7, 1, 1, NULL, '10000019', 2, 36382.00, 1, 6),
(250, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000022', 199, 6000.00, 1, 1),
(251, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000022', 198, 99999.00, 1, 1),
(252, NULL, '2020-10-01 01:02:41', 7, 1, 1, NULL, '10000022', 194, 0.00, 1, 70),
(253, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000022', 8, 99999.00, 1, 1),
(254, NULL, NULL, 7, 1, 1, NULL, '10000022', 7, 0.00, 1, NULL),
(255, NULL, NULL, 7, 1, 1, NULL, '10000022', 6, 10000.00, 1, NULL),
(256, NULL, NULL, 7, 1, 1, NULL, '10000022', 193, 0.00, 1, NULL),
(257, NULL, NULL, 7, 1, 1, NULL, '10000022', 148, 17350.00, 1, NULL),
(258, NULL, NULL, 7, 1, 1, NULL, '10000022', 4, 0.00, 1, NULL),
(259, NULL, NULL, 7, 1, 1, NULL, '10000022', 3, 36382.00, 1, NULL),
(260, NULL, NULL, 7, 1, 1, NULL, '10000022', 2, 36382.00, 1, NULL),
(261, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000023', 11, 17350.00, 1, 1),
(262, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000023', 12, 36382.00, 1, 1),
(263, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000023', 13, 36382.00, 1, 1),
(264, NULL, NULL, 7, 1, 2, NULL, '10000023', 47, 588.00, 1, 70),
(265, NULL, NULL, 7, 1, 2, NULL, '10000023', 200, 998.00, 1, 70),
(266, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000023', 15, 4654.00, 1, 1),
(267, NULL, NULL, 7, 1, 2, NULL, '10000023', 16, 1616.00, 1, 70),
(268, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000023', 17, 123456.00, 1, 1),
(269, NULL, NULL, 7, 1, 2, NULL, '10000023', 201, 6464.00, 1, 70),
(270, NULL, '2021-03-02 07:53:51', 7, 1, 2, NULL, '10000023', 18, 555.00, 1, 1),
(271, NULL, NULL, 7, 1, 2, NULL, '10000023', 19, 464.00, 1, 70),
(272, NULL, '2020-10-06 08:19:35', 7, 1, 1, NULL, '10000024', 206, 99999.00, 1, 70),
(273, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000024', 207, 36382.00, 1, 1),
(274, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000025', 209, 0.00, 1, 1),
(275, NULL, NULL, 7, 1, 1, NULL, '10000026', 206, 17350.00, 1, 70),
(276, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000026', 207, 36382.00, 1, 1),
(277, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000026', 209, 0.00, 1, 1),
(278, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000026', 210, 10500.00, 1, 1),
(279, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000026', 8, 99999.00, 1, 1),
(280, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000026', 198, 99999.00, 1, 1),
(281, NULL, '2021-03-01 12:40:27', 7, 1, 1, NULL, '10000026', 199, 6000.00, 1, 1),
(282, NULL, NULL, 7, 1, 4, NULL, '10000027', 81, 17350.00, 1, 70),
(283, NULL, NULL, 7, 1, 4, NULL, '10000027', 82, 36382.00, 1, 70),
(284, NULL, NULL, 7, 1, 4, NULL, '10000027', 83, 36382.00, 1, 70),
(285, NULL, NULL, 7, 1, 4, NULL, '10000027', 86, 10500.00, 1, 70),
(286, NULL, NULL, 7, 1, 4, NULL, '10000027', 88, 0.00, 1, 70),
(287, NULL, NULL, 7, 1, 4, NULL, '10000027', 89, 630.00, 1, 70),
(288, NULL, NULL, 7, 1, 4, NULL, '10000027', 90, 495090.00, 1, 70),
(289, NULL, NULL, 7, 1, 4, NULL, '10000027', 91, 6000.00, 1, 70),
(290, NULL, NULL, 7, 1, 10, NULL, '10000028', 149, 22050.00, 1, 70),
(291, NULL, NULL, 7, 1, 10, NULL, '10000028', 150, 30870.00, 1, 70),
(292, NULL, NULL, 7, 1, 10, NULL, '10000028', 151, 30870.00, 1, 70),
(293, NULL, NULL, 7, 1, 10, NULL, '10000028', 153, 15750.00, 1, 70),
(294, NULL, NULL, 7, 1, 10, NULL, '10000028', 155, 630.00, 1, 70),
(295, NULL, NULL, 7, 1, 10, NULL, '10000028', 186, 0.00, 1, 70),
(296, NULL, NULL, 7, 1, 10, NULL, '10000028', 156, 486965.00, 1, 70),
(297, NULL, NULL, 7, 1, 10, NULL, '10000028', 157, 6000.00, 1, 70),
(298, NULL, NULL, 7, 1, 18, NULL, '10000029', 49, 22050.00, 1, 70),
(299, NULL, NULL, 7, 1, 18, NULL, '10000029', 51, 37898.00, 1, 70),
(300, NULL, NULL, 7, 1, 18, NULL, '10000029', 52, 37898.00, 1, 70),
(301, NULL, NULL, 7, 1, 18, NULL, '10000029', 55, 33000.00, 1, 70),
(302, NULL, NULL, 7, 1, 18, NULL, '10000029', 57, 0.00, 1, 70),
(303, NULL, NULL, 7, 1, 18, NULL, '10000029', 58, 630.00, 1, 70),
(304, NULL, NULL, 7, 1, 18, NULL, '10000029', 60, 515705.00, 1, 70),
(305, NULL, NULL, 7, 1, 18, NULL, '10000029', 61, 59868.00, 1, 70),
(306, NULL, NULL, 4, 1, 18, NULL, '10000029', 62, 6000.00, 1, 70),
(309, NULL, '2021-03-02 07:58:44', 4, 1, 1, NULL, '10000030', 206, 1200.00, 1, 1),
(310, NULL, '2021-03-02 07:58:44', 4, 1, 1, NULL, '10000030', 213, 1500.00, 1, 1),
(311, NULL, '2021-03-02 07:58:44', 4, 1, 1, NULL, '10000030', 214, 2300.00, 1, 1),
(312, NULL, '2021-03-02 07:58:44', 4, 1, 1, NULL, '10000030', 215, 2110.00, 1, 1),
(313, NULL, '2021-03-02 08:06:25', 1, 1, 1, NULL, '10000031', 216, 10000.00, 1, 1),
(314, NULL, NULL, 7, 1, 1, NULL, '10000032', 217, 1000.00, 1, 1),
(315, NULL, NULL, 1, 1, 1, NULL, '10000033', 218, 1000.00, 1, 1),
(316, NULL, NULL, 1, 1, 1, NULL, '10000033', 219, 1000.00, 1, 1),
(317, NULL, NULL, 1, 1, 1, NULL, '10000033', 220, 10000.00, 1, 1),
(318, NULL, NULL, 1, 1, 1, NULL, '10000033', 221, 10000.00, 1, 1),
(319, NULL, NULL, 1, 1, 1, NULL, '10000033', 222, 100000.00, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fee_terms`
--

CREATE TABLE `fee_terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `term_number` int(10) UNSIGNED NOT NULL,
  `term_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term_date` date DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fee_types`
--

CREATE TABLE `fee_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `institute` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salogan` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_header` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_footer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedIn` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `googlePlus` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsApp` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skype` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterest` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wordpress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_events`
--

CREATE TABLE `group_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archive` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_event_students`
--

CREATE TABLE `group_event_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_event_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `guardian_details`
--

CREATE TABLE `guardian_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guardian_relation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guardian_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guardian_occupation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_info` enum('Filer','Non_Filer') COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_guardian` tinyint(1) NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guardian_details`
--

INSERT INTO `guardian_details` (`id`, `user_id`, `first_name`, `middle_name`, `last_name`, `cnic`, `guardian_relation`, `guardian_email`, `guardian_occupation`, `address`, `contact_no`, `nationality`, `tax_info`, `passport`, `is_guardian`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'DEMO', 'KIRBY', 'DEMMO', '11111111111', 'MOTHER', 'other@gmail.com', 'OCCUPATION', 'ADDRESS', '0323445567', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-02-28 10:25:33', '2021-02-28 10:25:33'),
(2, 1, 'HINA', 'ALI', 'BOYD', '2345678909876', 'MOTHER', 'mother000000@gmail.com', 'OCCUPATION', 'ADDRESS', '0323445567', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-02-28 10:35:31', '2021-02-28 10:35:31'),
(3, 1, 'DEMO', 'KIRBY', 'BOYD', '3434356788899', 'MOTHER', 'mother11998@gmail.com', 'OCCUPATION', 'ADASDASDASD', '0323445567', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-02-28 10:38:39', '2021-02-28 10:38:39'),
(4, 1, 'DEMO', 'KIRBY', 'DEMMO', '3434356788899', 'MOTHER', 'mother001@gmail.com', 'OCCUPATION', 'ADDRESS', '03332121234', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-02-28 11:05:03', '2021-02-28 11:05:03'),
(5, 1, 'DEMO', 'ALI', 'DEMMO', '3434356788899', 'MOTHER', 'kk3@gmail.com', 'OCCUPATION', 'SADASDAS', '0323445567', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-02-28 11:19:32', '2021-02-28 11:19:32'),
(6, 1, 'MOHAMMED', 'ASIF', 'KHAN', '236521651', 'FATHER', 'MASIF@GMAIL.COM', 'REITIRED', 'W56WRTASFAG', '212176157', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-02-28 17:35:32', '2021-02-28 17:35:32'),
(7, 1, 'M', 'ASHRAF', 'AHMED', '52371267', 'FATHER', 'MASHRAF@GMAI.COM', 'REITRED', 'WEGUEGWSG', 'WQ3621352361', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-02-28 18:20:42', '2021-02-28 18:20:42'),
(8, 1, 'HINA', 'KIRBY', 'BOYD', '2345678909876', 'FATHER', 'father0900@gmail.com', 'OCCUPATION', 'ERERERER', '0323445567', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 05:13:11', '2021-03-01 05:13:11'),
(9, 1, 'MOHAMMED', 'AHMED', 'KHAN', '3540401833422', 'FATHER', 'ahmed1119@gmail.com', 'TEACHER', 'NIL', '0302134255', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 05:25:22', '2021-03-01 05:25:22'),
(10, 1, 'MOHAMMED', 'ASHRAF', 'AHMED', '3540401833422', 'FATHER', 'ashraf1998@gamil.com', 'RETIRED', 'CXNBSAEY8237892UWUJKXNCKXMC    CJHBSDH', '03334455655', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 05:36:47', '2021-03-01 05:36:47'),
(11, 1, 'AHMED', 'ASHRAF', 'XYZ', '3540401833455', 'FATHER', 'axyz@gmail.com', 'RETIRED', 'NIL', '032355664667', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 05:45:16', '2021-03-01 05:45:16'),
(12, 1, 'HINA', 'ALI', 'KHAN', '3434356788899', 'MOTHER', 'guardians1@gmail.com', 'OCCUPATION', 'ADDRESS', '0323445567', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 05:53:22', '2021-03-01 05:53:22'),
(13, 1, 'BRENNAN', 'AHMAD', 'ASDASD', '1111111111111', 'FATHER', 'guardians2@gmail.com', 'OCCUPATION', 'SADASDAS', '0323445567', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 06:19:06', '2021-03-01 06:19:06'),
(14, 76, 'MOHAMMED', 'ASHRAF', 'XYZ', '3540401933421', 'FATHER', 'ashraf1888@gmail.com', 'RETIRED', 'NIL', '03334343655', '', 'Filer', '', 0, 3, NULL, NULL, NULL, '2021-03-01 09:55:44', '2021-03-01 09:55:44'),
(15, 79, 'ASIF', 'AHMED', 'BILAL', '354040193345651', 'FATHER', 'ABCD121@GMAIL.COM', 'TEACHER', 'NIL', '03335567782', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 10:39:44', '2021-03-01 10:39:44'),
(16, 81, 'MOHAMMED', 'ADEEL', 'AHMED', '3540401833422', 'FATHER', 'AHMED1996@GMAIL.COM', 'RETIRED', 'NIL', '0324214241234', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 11:11:31', '2021-03-01 11:11:31'),
(17, 83, 'BRENNAN', 'KIRBY MACK', 'DEMMO', '2345678909876', 'FATHER', 'guardians666@gmail.com', 'OCCUPATION', 'SADASDAS', '0323445567', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-01 11:30:37', '2021-03-01 11:30:37'),
(18, 88, 'UMAIR', 'AHMED', 'CONSECTETUR ADIPISCING', '35404019364752', 'FATHER', 'axyz101@gmail.com', 'TEACHER', 'UMAIR101@GMAIL.COM', '03234242651', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-02 06:44:57', '2021-03-02 06:44:57'),
(19, 90, 'FUGIAT', 'FUGIAT', 'FUGIAT', '354040193342', 'FATHER', 'abc1234@gmail.com', 'DOCTOR', 'NIL', '03334242667', '', 'Filer', '', 0, 1, NULL, NULL, NULL, '2021-03-02 06:52:09', '2021-03-02 06:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `idcards`
--

CREATE TABLE `idcards` (
  `id` int(10) UNSIGNED NOT NULL,
  `link_type` int(11) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` int(11) DEFAULT NULL,
  `expiry_date` int(11) DEFAULT NULL,
  `approve_at` int(11) DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `idcards`
--

INSERT INTO `idcards` (`id`, `link_type`, `link_id`, `status`, `issue_date`, `expiry_date`, `approve_at`, `approve_by`, `created_at`, `updated_at`) VALUES
(1, 2, 14, 'approve', 2021, 2021, 2021, 3, '2021-02-23 09:07:19', '2021-02-23 09:15:44'),
(2, 2, 14, 'approve', 2021, 2021, 2021, 3, '2021-02-23 09:07:27', '2021-02-23 09:15:52'),
(3, 2, 12, 'approve', 2021, 2021, 2021, 3, '2021-02-23 09:49:06', '2021-02-23 09:54:39'),
(4, 2, 16, 'approve', 2021, 2021, 2021, 3, '2021-02-24 05:52:44', '2021-02-24 05:53:08'),
(5, 2, 16, 'approve', 2021, 2021, 2021, 2, '2021-02-24 06:08:31', '2021-02-24 06:54:20'),
(6, 2, 17, 'approve', 2021, 2021, 2021, 3, '2021-02-25 10:02:40', '2021-02-25 12:50:18'),
(7, 2, 17, 'approve', 2021, 2021, 2021, 3, '2021-02-25 12:48:59', '2021-02-25 12:50:21'),
(8, 2, 17, 'approve', 2021, 2021, 2021, 3, '2021-02-25 13:21:45', '2021-02-26 05:01:47'),
(9, 2, 11, 'approve', 2021, 2021, 2021, 10, '2021-02-26 05:28:50', '2021-02-26 06:39:27'),
(10, 2, 21, 'approve', 2021, 2021, 2021, 3, '2021-02-26 12:36:03', '2021-02-26 12:37:00'),
(11, 2, 91, 'pending', 2021, 2021, NULL, NULL, '2021-03-02 09:53:26', '2021-03-02 09:53:26');

-- --------------------------------------------------------

--
-- Table structure for table `job_post`
--

CREATE TABLE `job_post` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_post_request_id` int(10) UNSIGNED NOT NULL,
  `section` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `experience` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expected_joining` date NOT NULL,
  `public_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_post`
--

INSERT INTO `job_post` (`id`, `job_post_request_id`, `section`, `education`, `skill`, `branch_id`, `experience`, `expected_joining`, `public_link`, `job_title`, `description`, `location`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Junior', 'MS  Mathematics', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 4, '5', '2021-02-24', 'http://rootsschool.erpnetroots.com/job-post/1', 'Teacher  Required  for  EYP Computer Science', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 'DHA   Phase  5   ISB', 1, 3, 3, NULL, '2021-02-23 10:42:45', '2021-02-23 10:58:24', NULL),
(2, 2, 'Junior', 'Masters   English Literature', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 4, '5  Year', '2021-02-26', 'http://rootsschool.erpnetroots.com/job-post/2', 'Teacher  required  for PYP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 'IVY Campus  Lahore', 1, 3, 3, NULL, '2021-02-25 09:57:56', '2021-02-25 09:57:56', NULL),
(3, 3, 'Senior', 'MS  Mathematics', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 4, '5 Years', '2021-03-01', 'http://rootsschool.erpnetroots.com/job-post/3', 'Teacher  required  foR PYP', 'Nil', 'IVY HEAD  Office', 1, 3, 3, NULL, '2021-03-01 07:02:06', '2021-03-01 07:02:06', NULL),
(4, 4, 'Junior', 'MS  MATHEMATICS', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 4, '5  Years', '2021-03-03', 'http://rootsschool.erpnetroots.com/job-post/4', 'Teacher  required  for   PYP', 'xvcvcgshxdvsghvsd', 'ivy  Head  office', 1, 3, 3, NULL, '2021-03-01 09:18:11', '2021-03-01 09:18:11', NULL),
(5, 5, 'Junior', 'MS  English', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 4, '5 Years', '2021-03-03', 'http://rootsschool.erpnetroots.com/job-post/5', 'Teacher  Required  for  EYP  English', 'erwreq3q3621t6ysdfghxzvch', 'IVY  Head  office', 1, 3, 3, NULL, '2021-03-01 09:30:12', '2021-03-01 09:30:12', NULL),
(6, 3, 'Senior', 'MS   Mathematics', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 4, '5 Years', '2021-03-03', 'http://rootsschool.erpnetroots.com/job-post/6', 'Teacher required for PYP', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 'Lahore', 1, 3, 3, NULL, '2021-03-03 06:09:02', '2021-03-03 06:09:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_post_request`
--

CREATE TABLE `job_post_request` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `section` enum('junior','senior') COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `education` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expected_joining` date NOT NULL,
  `job_status` enum('Permanent','Full Time','Part Time','Visiting') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_post_request`
--

INSERT INTO `job_post_request` (`id`, `branch_id`, `subject_id`, `section`, `experience`, `gender`, `program_id`, `education`, `expected_joining`, `job_status`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, 'junior', '5', 'female', 1, 'MS   Mathematics', '2021-02-24', 'Permanent', 1, 3, NULL, NULL, '2021-02-23 10:42:07', '2021-02-23 10:42:07', NULL),
(2, 4, 2, 'junior', '5  Years', 'female', 2, 'MS  English', '2021-02-26', 'Permanent', 1, 3, NULL, NULL, '2021-02-25 09:56:24', '2021-02-25 09:56:24', NULL),
(3, 4, 1, 'junior', '5   Years', 'female', 1, 'MS Mathematics', '2021-03-31', 'Permanent', 1, 3, NULL, NULL, '2021-03-01 07:00:07', '2021-03-01 07:00:07', NULL),
(4, 4, 2, 'junior', '5 Years', 'male', 2, 'MS ENGLISH', '2021-03-02', 'Permanent', 1, 3, NULL, NULL, '2021-03-01 09:16:25', '2021-03-01 09:16:25', NULL),
(5, 4, 2, 'junior', '5  Years', 'male', 1, 'MS  English  Literature', '2021-03-02', 'Permanent', 1, 3, NULL, NULL, '2021-03-01 09:27:20', '2021-03-01 09:27:20', NULL),
(6, 4, 2, 'junior', '5  Years', 'male', 1, 'MS  English literature', '2021-03-04', 'Permanent', 1, 3, NULL, NULL, '2021-03-03 06:06:27', '2021-03-03 06:06:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `labs`
--

CREATE TABLE `labs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `labs`
--

INSERT INTO `labs` (`id`, `name`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'PHYSICS LAB', '9-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-23 06:38:29', '2021-02-26 07:48:05'),
(2, 'CHEMISTRY  LAB', '9-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-23 06:38:29', '2021-02-26 07:48:23'),
(3, 'BIOLOGY  LAB', '9-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-23 06:38:29', '2021-02-26 07:48:55'),
(4, 'COMPUTER LAB', '9-12', 'Active', NULL, NULL, NULL, NULL, '2021-02-23 06:38:29', '2021-02-26 07:48:39');

-- --------------------------------------------------------

--
-- Table structure for table `lab_time_tables`
--

CREATE TABLE `lab_time_tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `time_table_id` int(10) UNSIGNED NOT NULL,
  `lab_id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lab_time_tables`
--

INSERT INTO `lab_time_tables` (`id`, `time_table_id`, `lab_id`, `date`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-02-26', NULL, NULL, '2021-02-23 12:16:46', '2021-02-23 12:16:46');

-- --------------------------------------------------------

--
-- Table structure for table `library_circulation`
--

CREATE TABLE `library_circulation` (
  `id` int(11) NOT NULL,
  `user_type` varchar(191) NOT NULL,
  `slug` int(11) NOT NULL,
  `code_prefix` varchar(191) NOT NULL,
  `issue_limit_days` int(11) NOT NULL,
  `issue_limit_books` int(11) NOT NULL,
  `fine_per_day` int(11) NOT NULL,
  `status` varchar(191) NOT NULL,
  `created_by` timestamp NULL DEFAULT NULL,
  `last_updated_by` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library_circulation`
--

INSERT INTO `library_circulation` (`id`, `user_type`, `slug`, `code_prefix`, `issue_limit_days`, `issue_limit_books`, `fine_per_day`, `status`, `created_by`, `last_updated_by`) VALUES
(1, 'student', 22, 'asdasdas', 5, 5, 50, 'active', NULL, NULL),
(2, 'staff', 22, 'asdasdas', 5, 5, 50, 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `library_members`
--

CREATE TABLE `library_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `user_type` int(10) UNSIGNED NOT NULL,
  `member_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `library_members`
--

INSERT INTO `library_members` (`id`, `created_at`, `updated_at`, `created_by`, `last_updated_by`, `branch_id`, `user_type`, `member_id`, `status`) VALUES
(1, '2021-03-01 11:47:46', '2021-03-01 11:47:46', 1, NULL, 4, 1, 13, 1),
(2, '2021-03-02 07:16:18', '2021-03-02 07:16:18', 1, NULL, 4, 1, 20, 1),
(3, '2021-03-02 07:29:29', '2021-03-02 07:29:29', 1, NULL, 4, 2, 8, 1),
(4, '2021-03-02 09:38:26', '2021-03-02 09:38:26', 1, NULL, 4, 1, 9, 1),
(5, '2021-03-02 09:40:12', '2021-03-02 09:40:12', 1, NULL, 4, 1, 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_detail`
--

CREATE TABLE `login_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `start_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login_details`
--

CREATE TABLE `login_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `start_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login_details`
--

INSERT INTO `login_details` (`id`, `user_id`, `start_time`, `end_time`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-03-03 12:23:16', '', '1', '2021-03-03 07:23:16', '2021-03-03 07:23:16'),
(2, 1, '2021-03-03 12:33:15', '', '1', '2021-03-03 07:33:15', '2021-03-03 07:33:15'),
(3, 1, '2021-03-03 12:38:49', '', '1', '2021-03-03 07:38:49', '2021-03-03 07:38:49'),
(4, 1, '2021-03-03 12:43:04', '', '1', '2021-03-03 07:43:04', '2021-03-03 07:43:04'),
(5, 1, '2021-03-03 12:46:14', '', '1', '2021-03-03 07:46:14', '2021-03-03 07:46:14'),
(6, 1, '2021-03-03 12:48:56', '', '1', '2021-03-03 07:48:56', '2021-03-03 07:48:56'),
(7, 1, '2021-03-03 04:50:07', '', '1', '2021-03-03 11:50:07', '2021-03-03 11:50:07'),
(8, 1, '2021-03-03 04:52:57', '', '1', '2021-03-03 11:52:57', '2021-03-03 11:52:57'),
(9, 1, '2021-03-03 05:39:18', '', '1', '2021-03-03 12:39:18', '2021-03-03 12:39:18'),
(10, 1, '2021-03-03 05:50:22', '', '1', '2021-03-03 12:50:22', '2021-03-03 12:50:22'),
(11, 1, '2021-03-03 05:54:28', '', '1', '2021-03-03 12:54:28', '2021-03-03 12:54:28'),
(12, 1, '2021-03-04 09:20:24', '', '1', '2021-03-04 04:20:24', '2021-03-04 04:20:24'),
(13, 1, '2021-03-04 09:24:48', '', '1', '2021-03-04 04:24:48', '2021-03-04 04:24:48'),
(14, 1, '2021-03-04 09:27:51', '', '1', '2021-03-04 04:27:51', '2021-03-04 04:27:51'),
(15, 1, '2021-03-04 09:35:18', '', '1', '2021-03-04 04:35:18', '2021-03-04 04:35:18'),
(16, 1, '2021-03-04 09:35:51', '', '1', '2021-03-04 04:35:51', '2021-03-04 04:35:51'),
(17, 1, '2021-03-04 09:41:03', '', '1', '2021-03-04 04:41:03', '2021-03-04 04:41:03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `member_type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `display_group` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `branch_id`, `title`, `message`, `publish_date`, `end_date`, `display_group`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 7, 'Orientation', '<p>Tomorrow is the orientation of new students.&nbsp;</p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '8', 1, 4, NULL, '2020-09-24 02:24:01', '2020-09-24 02:24:01'),
(2, 7, 'New teacher\'s introduction', '<p>we are having new teachers on board and tomorrow we will have an introductory session.&nbsp;</p>', '2020-09-24 00:00:00', '2020-09-25 00:00:00', '8', 1, 4, NULL, '2020-09-24 02:25:21', '2020-09-24 02:25:21'),
(3, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '2', 1, 4, NULL, '2020-09-24 05:49:26', '2020-09-24 05:49:26'),
(4, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '3', 1, 4, NULL, '2020-09-24 05:50:06', '2020-09-24 05:50:06'),
(5, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '4', 1, 4, NULL, '2020-09-24 05:50:25', '2020-09-24 05:50:25'),
(6, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '5', 1, 4, NULL, '2020-09-24 05:51:06', '2020-09-24 05:51:06'),
(7, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '6', 1, 4, NULL, '2020-09-24 05:51:29', '2020-09-24 05:51:29'),
(8, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '7', 1, 4, NULL, '2020-09-24 05:52:23', '2020-09-24 05:52:23'),
(9, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '9', 1, 4, NULL, '2020-09-24 05:52:57', '2020-09-24 05:52:57'),
(10, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '10', 1, 4, NULL, '2020-09-24 05:53:34', '2020-09-24 05:53:34'),
(11, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '11', 1, 4, NULL, '2020-09-24 05:53:55', '2020-09-24 05:53:55'),
(12, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '12', 1, 4, NULL, '2020-09-24 05:54:25', '2020-09-24 05:54:25'),
(13, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '13', 1, 4, NULL, '2020-09-24 05:55:07', '2020-09-24 05:55:07'),
(14, 7, 'workshop', '<p>attend the workshop.&nbsp;<br></p>', '2020-09-24 00:00:00', '2020-09-26 00:00:00', '15', 1, 4, NULL, '2020-09-24 05:55:29', '2020-09-24 05:55:29');

-- --------------------------------------------------------

--
-- Table structure for table `parent_details`
--

CREATE TABLE `parent_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `students_id` int(10) UNSIGNED NOT NULL,
  `father_first_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_middle_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_last_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_occupation` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_email` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_mobile_1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_cnic` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_middle_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_last_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_cnic` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_occupation` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_mobile_1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_is` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_first_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_middle_name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_occupation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_residence_number` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_mobile_1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_cnic` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_email` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_relation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filer_info` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parent_details`
--

INSERT INTO `parent_details` (`id`, `created_by`, `last_updated_by`, `students_id`, `father_first_name`, `father_middle_name`, `father_last_name`, `father_occupation`, `father_email`, `father_mobile_1`, `father_cnic`, `mother_first_name`, `mother_middle_name`, `mother_last_name`, `mother_cnic`, `mother_occupation`, `mother_mobile_1`, `mother_email`, `parent_image`, `guardian_is`, `guardian_first_name`, `guardian_middle_name`, `guardian_last_name`, `guardian_occupation`, `guardian_address`, `guardian_residence_number`, `guardian_mobile_1`, `guardian_cnic`, `guardian_email`, `guardian_relation`, `filer_info`, `status`, `created_at`, `updated_at`) VALUES
(6, NULL, NULL, 3, 'FATHER NAME', 'NEVADA', 'DEMO', 'DEMO', 'usamairshad1000', '03235665566', '6543212345677', 'DEMO', 'ALI', 'KHAN', '3434356788899', 'OCCUPATION', '0323445567', 'mother100@gmail.com', '/tmp/phphLibtL', 'other_guardian', 'DEMO', 'KIRBY', 'DEMMO', 'OCCUPATION', 'ADDRESS', '03334545567', '0323445567', '11111111111', 'other@gmail.com', 'MOTHER', 1, 1, '2021-02-28 10:25:33', '2021-02-28 10:25:33'),
(7, NULL, NULL, 4, 'DEMO', 'NEVADA', 'DEMO', 'DEMO', 'asad111111@netr', '03235665566', '6543212345677', 'DEMO', 'KIRBY MACK', 'DEMMO', '1111111111111', 'OCCUPATION', '0323445567', 'mother0000@gmail.com', '/tmp/phpFuZmEr', 'other_guardian', 'HINA', 'ALI', 'BOYD', 'OCCUPATION', 'ADDRESS', '03234567898', '0323445567', '2345678909876', 'mother000000@gm', 'MOTHER', 1, 1, '2021-02-28 10:35:31', '2021-02-28 10:35:31'),
(8, NULL, NULL, 5, 'FATHER', 'NEVADA', 'DEMO', 'EMPLOYEE', 'bbb1@netroots.c', '03235665566', '6543212345677', 'DEMO', 'KIRBY', 'DEMMO1', '3434356788899', 'OCCUPATION', '0323445567', 'mother111@gmail.com', '/tmp/phpqihFjf', 'other_guardian', 'DEMO', 'KIRBY', 'BOYD', 'OCCUPATION', 'ADASDASDASD', '03334545567', '0323445567', '3434356788899', 'mother11998@gma', 'MOTHER', 1, 1, '2021-02-28 10:38:39', '2021-02-28 10:38:39'),
(9, NULL, NULL, 6, 'FATHER NAME', 'NEVADA', 'DEMO', 'DEMO', 'bccc@netroots.c', '03235665566', '6543212345677', 'DEMO', 'KIRBY', 'DEMMO', '3434343434343', 'RTRTRT', '0323445567', 'mother1211@gmail.com', '/tmp/phpxoMgp5', 'other_guardian', 'DEMO', 'KIRBY', 'DEMMO', 'OCCUPATION', 'ADDRESS', '03334545567', '03332121234', '3434356788899', 'mother001@gmail', 'MOTHER', 1, 1, '2021-02-28 11:05:03', '2021-02-28 11:05:03'),
(10, NULL, NULL, 7, 'KK2', 'KK2', 'KK2', 'EMPLOYEE', 'kkk2@netroots.c', '03235665566', '2222222222222', 'DEMO', 'ALI', 'KHAN', '3434356788899', 'OCCUPATION', '0323445567', 'bbc2@gmail.com', '/tmp/php7fUvIY', 'other_guardian', 'DEMO', 'ALI', 'DEMMO', 'OCCUPATION', 'SADASDAS', '03334545567', '0323445567', '3434356788899', 'kk3@gmail.com', 'MOTHER', 1, 1, '2021-02-28 11:19:32', '2021-02-28 11:19:32'),
(11, NULL, NULL, 9, 'MOHAMMED', 'ASIF', 'KHAN', 'REITIRED', 'asif@gmail.com', '0323445454', '3540401933422', 'MS', 'SNBANAS', 'SBHHJSB', '3652652636171', 'TEACHER', '03234455651', 'abcxyz@gmail.com', '/tmp/phpM5K4n0', 'father_as_guard', 'MOHAMMED', 'ASIF', 'KHAN', 'REITIRED', 'W56WRTASFAG', '5321435621', '212176157', '236521651', 'MASIF@GMAIL.COM', 'FATHER', 1, 1, '2021-02-28 17:35:32', '2021-02-28 17:35:32'),
(12, NULL, NULL, 10, 'M', 'ASHRAF', 'AHMED', 'REITRED', 'ashraf@gmail.co', '024345425652', '2321565136126', 'ETWQUETWUQ', 'WQEQYTWEUQT', 'WETQWYETQ', '7666666666654', '341T74612SDBAS', '0323123456', 'abcx@gmail.com', '/tmp/phpmTLJ7s', 'father_as_guard', 'M', 'ASHRAF', 'AHMED', 'REITRED', 'WEGUEGWSG', '2365276WGA', 'WQ3621352361', '52371267', 'MASHRAF@GMAI.CO', 'FATHER', 1, 1, '2021-02-28 18:20:42', '2021-02-28 18:20:42'),
(13, NULL, NULL, 11, 'DEMO', 'NEVADA', 'DEMO', 'DEMO', 'father565@gmail', '03235665566', '6543212345677', 'HUMA', 'ALI', 'BOYD', '3434343434343', 'EMPLOYEE', '0323445567', 'mother1101@gmail.com', '/tmp/phpH1WZGc', 'other_guardian', 'HINA', 'KIRBY', 'BOYD', 'OCCUPATION', 'ERERERER', '03334545567', '0323445567', '2345678909876', 'father0900@gmai', 'FATHER', 1, 1, '2021-03-01 05:13:11', '2021-03-01 05:13:11'),
(14, NULL, NULL, 12, 'MOHAMMED', 'AHMED', 'KHAN', 'TEACHER', 'ahmed2000@gmail', '03332545233', '3540401833422', 'SDGUDGHJSBH', 'HSYUEHYUDSH', 'WEYWEUHX', '3540401933422', 'TEACHER', '03025108858', 'ahmed1999@gmail.com', '/tmp/phpsUeL15', 'father_as_guard', 'MOHAMMED', 'AHMED', 'KHAN', 'TEACHER', 'NIL', '0353', '0302134255', '3540401833422', 'ahmed1119@gmail', 'FATHER', 1, 1, '2021-03-01 05:25:22', '2021-03-01 05:25:22'),
(15, NULL, NULL, 13, 'MOHAMMED', 'ASHRAF', 'AHMED', 'RETIRED', 'ashraf111@gmail', '023423243154', '3540401833421', 'AMEERA', 'ASIF', 'AHMED', '3540401833466', 'TEACHING', '03324242655', 'abcx@gmail.com', '/tmp/phprlWDBK', 'father_as_guard', 'MOHAMMED', 'ASHRAF', 'AHMED', 'RETIRED', 'CXNBSAEY8237892UWUJKXNCKXMC    CJHBSDH', '24325', '03334455655', '3540401833422', 'ashraf1998@gami', 'FATHER', 1, 1, '2021-03-01 05:36:47', '2021-03-01 05:36:47'),
(16, NULL, NULL, 14, 'AHMED', 'ASHRAF', 'XYZ', 'RETIRED', 'axyzashraf2gmai', '03234343655', '3540402954634', 'MAIRA', 'XCBXVCG', 'CFVDTYFGVFDC', '3540401933442', 'TEACHER', '03234242566', 'maira@gmail.com', '/tmp/phpnqT4OL', 'father_as_guard', 'AHMED', 'ASHRAF', 'XYZ', 'RETIRED', 'NIL', '36454', '032355664667', '3540401833455', 'axyz@gmail.com', 'FATHER', 1, 1, '2021-03-01 05:45:16', '2021-03-01 05:45:16'),
(17, NULL, NULL, 15, 'DEMO', 'ALI', 'KHAN', 'EMPLOYEE', 'father100011@ne', '032233232323', '5454545444554', 'HINA', 'KIRBY', 'BOYD', '3434356788899', 'EMPLOYEE', '0323445567', 'mother100011@gmail.com', '/tmp/phpO9u3id', 'other_guardian', 'HINA', 'ALI', 'KHAN', 'OCCUPATION', 'ADDRESS', '03234567898', '0323445567', '3434356788899', 'guardians1@gmai', 'MOTHER', 1, 1, '2021-03-01 05:53:22', '2021-03-01 05:53:22'),
(18, NULL, NULL, 17, 'FATHER', 'ALI', 'KHAN', 'EPLOYEE', 'father4444@gmai', '03235665566', '5454545444554', 'HINA', 'ALI', 'KHAN', '5454544554455', 'EMPLOYEE', '0323445567', 'bij@gmail.com', '/tmp/phpSoFxn6', 'other_guardian', 'BRENNAN', 'AHMAD', 'ASDASD', 'OCCUPATION', 'SADASDAS', '03334545567', '0323445567', '1111111111111', 'guardians2@gmai', 'FATHER', 1, 1, '2021-03-01 06:19:06', '2021-03-01 06:19:06'),
(19, NULL, NULL, 18, 'MOHAMMED', 'ASHRAF', 'XYZ', 'RETIRED', 'abcde@gmail.com', '03334545655', '3540401988324', 'MISBAH', 'AHMED', 'KHAN', '3540401933422', 'TEACHER', '04213413412341', 'misbah@gmail.com', '/tmp/phpc5ReBV', 'father_as_guard', 'MOHAMMED', 'ASHRAF', 'XYZ', 'RETIRED', 'NIL', '35463', '03334343655', '3540401933421', 'ashraf1888@gmai', 'FATHER', 1, 1, '2021-03-01 09:55:44', '2021-03-01 09:55:44'),
(20, NULL, NULL, 19, 'ASIF', 'AHMED', 'BILAL', 'TEACHER', 'bilal@gmail.com', '0123412413254', '3540401933424', 'SAIRA', 'SGAJGBXHA', 'SDSGADV', '354040233455', 'NIL', '03334242655', 'saira100@gmail.com', '/tmp/php4Dt0VY', 'father_as_guard', 'ASIF', 'AHMED', 'BILAL', 'TEACHER', 'NIL', '43235623', '03335567782', '354040193345651', 'ABCD121@GMAIL.C', 'FATHER', 1, 1, '2021-03-01 10:39:44', '2021-03-01 10:39:44'),
(21, NULL, NULL, 20, 'MOHAMMED', 'ADEEL', 'AHMED', 'RETIRED', 'adeel1999@gmail', '03234242655', '3540401833424', 'AROOJ', 'AHMED', 'ABC', '2543652143562', 'TEACHER', '05234', 'abc@gmail.com', '/tmp/phpnioz53', 'father_as_guard', 'MOHAMMED', 'ADEEL', 'AHMED', 'RETIRED', 'NIL', '4213', '0324214241234', '3540401833422', 'AHMED1996@GMAIL', 'FATHER', 1, 1, '2021-03-01 11:11:31', '2021-03-01 11:11:31'),
(22, NULL, NULL, 21, 'CYNTHIA', 'NEVADA', 'DEMO', 'DEMO', 'father666@netro', '032233232323', '5454545444554', 'DEMO', 'KIRBY MACK', 'DEMMO', '3434356788899', 'OCCUPATION', '0323445567', 'mother666@gmail.com', '/tmp/php6zCA5d', 'other_guardian', 'BRENNAN', 'KIRBY MACK', 'DEMMO', 'OCCUPATION', 'SADASDAS', '03334545567', '0323445567', '2345678909876', 'guardians666@gm', 'FATHER', 1, 1, '2021-03-01 11:30:37', '2021-03-01 11:30:37'),
(23, NULL, NULL, 22, 'UMAIR', 'AHMED', 'CONSECTETUR', 'TEACHER', 'umairahmed@gmai', '03334545651', '3540401933422', 'CONSECTETUR', 'CONSECTETUR', 'CONSECTETUF', '3540401933421', 'TEACHER', '03324242655', 'abcse@gmail.com', '/tmp/phpQq44DC', 'father_as_guard', 'UMAIR', 'AHMED', 'CONSECTETUR ADIPISCING', 'TEACHER', 'UMAIR101@GMAIL.COM', '3540401933422', '03234242651', '35404019364752', 'axyz101@gmail.c', 'FATHER', 1, 1, '2021-03-02 06:44:57', '2021-03-02 06:44:57'),
(24, NULL, NULL, 23, 'FUGIAT', 'FUGIAT', 'FUGIAT', 'DOCTOR', 'abc123abc@gmail', '03334242666', '3540401933422', 'PERSPICIATIS', 'PERSPICIATIS', 'PERSPICIATIS', '3540401213541', 'TEACHER', '03334242655', 'perspiciatis@gmail.com', '/tmp/phpYG0lZr', 'father_as_guard', 'FUGIAT', 'FUGIAT', 'FUGIAT', 'DOCTOR', 'NIL', '35404', '03334242667', '354040193342', 'abc1234@gmail.c', 'FATHER', 1, 1, '2021-03-02 06:52:09', '2021-03-02 06:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `past_paper`
--

CREATE TABLE `past_paper` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `active_session_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `year` varchar(191) DEFAULT NULL,
  `document_type` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_settings`
--

CREATE TABLE `payment_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `identity` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `group`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Menu', 'expand-action-menu', 'Expand Nav Menu', 'Expand Nav Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(2, 'Menu', 'admin-control', 'Admin Control', 'Admin Control Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(3, 'Menu', 'general', 'General', 'General Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(4, 'Menu', 'dashboard', 'Dashboard', 'Dashboard Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(5, 'Menu', 'student-staff', 'Student-Staff', 'Student-Staff Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(6, 'Menu', 'account', 'Account', 'Account Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(7, 'Menu', 'library', 'Library', 'Libaray Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(8, 'Menu', 'attendance', 'Attendance', 'Attendance Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(9, 'Menu', 'examination', 'Examination', 'Examination Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(10, 'Menu', 'academics specific', 'Academics Specific', 'Academics Specific Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(11, 'Menu', 'report', 'Report', 'Report Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(12, 'Menu', 'alert-center', 'Alert Center', 'Alert Center Menu', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(13, 'School Settings', 'school-settings-index', 'School Settings', 'School Settings Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(14, 'Role', 'role-index', 'Index', 'Role Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(15, 'Role', 'role-add', 'Add', 'Role Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(16, 'Role', 'role-view', 'View', 'View Role', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(17, 'Role', 'role-edit', 'Edit', 'Edit Role', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(18, 'Role', 'role-delete', 'Delete', 'Delete Role', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(19, 'User', 'user-index', 'Index', 'User Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(20, 'User', 'user-add', 'Add', 'User Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(21, 'User', 'user-edit', 'Edit', 'Edit User', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(22, 'User', 'user-delete', 'Delete', 'Delete User', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(23, 'User', 'user-active', 'Active', 'Active User', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(24, 'User', 'user-in-active', 'In-Active', 'In-Active User', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(25, 'User', 'user-bulk-action', 'Bulk(Active,InActive,Delete)', 'User Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(26, 'General Setting', 'general-setting-index', 'Index', 'General Setting Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(27, 'General Setting', 'general-setting-add', 'Add', 'General Setting Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(28, 'General Setting', 'general-setting-edit', 'Edit', 'Edit General Setting', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(29, 'Alert Setting', 'alert-setting-index', 'Index', 'Alert Setting Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(30, 'Alert Setting', 'alert-setting-add', 'Add', 'Alert Setting Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(31, 'Alert Setting', 'alert-setting-edit', 'Edit', 'Edit Alert Setting', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(32, 'SMS Setting', 'sms-setting-index', 'Index', 'SMS Setting Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(33, 'SMS Setting', 'sms-setting-add', 'Add', 'SMS Setting Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(34, 'SMS Setting', 'sms-setting-edit', 'Edit', 'Edit SMS Setting', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(35, 'SMS Setting', 'sms-setting-active', 'Active', 'Active SMS', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(36, 'SMS Setting', 'sms-setting-in-active', 'In-Active', 'In-Active SMS', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(37, 'Email Setting', 'email-setting-index', 'Index', 'Email Setting Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(38, 'Email Setting', 'email-setting-add', 'Add', 'Email Setting Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(39, 'Email Setting', 'email-setting-edit', 'Edit', 'Edit Email Setting', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(40, 'Email Setting', 'email-setting-status-change', 'Status', 'Change Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(41, 'Payment Gateway Setting', 'payment-gateway-setting-index', 'Index', 'Payment Gateway Setting Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(42, 'Payment Gateway Setting', 'payment-gateway-setting-add', 'Add', 'Payment Gateway Setting Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(43, 'Payment Gateway Setting', 'payment-gateway-setting-edit', 'Edit', 'Edit Payment Gateway Setting', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(44, 'Payment Gateway Setting', 'payment-gateway-active', 'Active', 'Active Payment Gateway', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(45, 'Payment Gateway Setting', 'payment-gateway-in-active', 'In-Active', 'In-Active Payment Gateway', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(46, 'Student', 'student-index', 'Index', 'Student Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(47, 'Student', 'student-registration', 'Registration', 'Student Registration', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(48, 'Student', 'student-view', 'View', 'View Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(49, 'Student', 'student-edit', 'Edit', 'Edit Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(50, 'Student', 'student-delete', 'Delete', 'Delete Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(51, 'Student', 'student-active', 'Active', 'Active Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(52, 'Student', 'student-in-active', 'In-Active', 'In-Active Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(53, 'Student', 'student-bulk-action', 'Bulk(Active,InActive,Delete)', 'Student Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(54, 'Student', 'student-delete-academic-info', 'Delete Academic Info', 'Student Delete Academic Info', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(55, 'Student', 'student-transfer', 'Transfer', 'Transfer Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(56, 'Student Document', 'student-document-index', 'Index', 'Student Document Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(57, 'Student Document', 'student-document-add', 'Add', 'Student Document Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(58, 'Student Document', 'student-document-edit', 'Edit', 'Edit Student Document', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(59, 'Student Document', 'student-document-delete', 'Delete', 'Delete Student Document', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(60, 'Student Document', 'student-document-active', 'Active', 'Active Student Document', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(61, 'Student Document', 'student-document-in-active', 'In-Active', 'In-Active Student Document', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(62, 'Student Document', 'student-document-bulk-action', 'Bulk(Active,InActive,Delete)', 'Student Document Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(63, 'Student Note', 'student-note-index', 'Index', 'Student Note Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(64, 'Student Note', 'student-note-add', 'Add', 'Student Note Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(65, 'Student Note', 'student-note-edit', 'Edit', 'Edit Student Note', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(66, 'Student Note', 'student-note-delete', 'Delete', 'Delete Student Note', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(67, 'Student Note', 'student-note-active', 'Active', 'Active Student Note', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(68, 'Student Note', 'student-note-in-active', 'In-Active', 'In-Active Student Note', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(69, 'Student Note', 'student-note-bulk-action', 'Bulk(Active,InActive,Delete)', 'Student Note Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(70, 'Student Comment', 'student-comment-index', 'Index', 'Student Comment Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(71, 'Student Comment', 'student-comment-add', 'Add', 'Student Comment Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(72, 'Student Comment', 'student-comment-edit', 'Edit', 'Edit Student Comment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(73, 'Student Comment', 'student-comment-delete', 'Delete', 'Delete Student Comment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(74, 'Student Comment', 'student-comment-active', 'Active', 'Active Student Comment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(75, 'Student Comment', 'student-comment-in-active', 'In-Active', 'In-Active Student Comment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(76, 'Student Comment', 'student-comment-bulk-action', 'Bulk(Active,InActive,Delete)', 'Bulk Action Student Comment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(77, 'Staff', 'staff-index', 'Index', 'Staff Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(78, 'Staff', 'staff-add', 'Add', 'Staff Registration', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(79, 'Staff', 'staff-view', 'View', 'View Staff', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(80, 'Staff', 'staff-edit', 'Edit', 'Edit Staff', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(81, 'Staff', 'staff-delete', 'Delete', 'Delete Staff', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(82, 'Staff', 'staff-active', 'Active', 'Active Staff', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(83, 'Staff', 'staff-in-active', 'In-Active', 'In-Active Staff', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(84, 'Staff', 'staff-bulk-action', 'Bulk(Active,InActive,Delete)', 'Staff Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(85, 'Staff Document', 'staff-document-index', 'Index', 'Staff Document Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(86, 'Staff Document', 'staff-document-add', 'Add', 'Staff Document Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(87, 'Staff Document', 'staff-document-edit', 'Edit', 'Edit Staff Document', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(88, 'Staff Document', 'staff-document-delete', 'Delete', 'Delete Staff Document', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(89, 'Staff Document', 'staff-document-active', 'Active', 'Active Staff Document', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(90, 'Staff Document', 'staff-document-in-active', 'In-Active', 'In-Active Staff Document', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(91, 'Staff Document', 'staff-document-bulk-action', 'Bulk(Active,InActive,Delete)', 'Staff Document Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(92, 'Staff Note', 'staff-note-index', 'Index', 'Staff Note Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(93, 'Staff Note', 'staff-note-add', 'Add', 'Staff Note Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(94, 'Staff Note', 'staff-note-edit', 'Edit', 'Edit Staff Note', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(95, 'Staff Note', 'staff-note-delete', 'Delete', 'Delete Staff Note', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(96, 'Staff Note', 'staff-note-active', 'Active', 'Active Staff Note', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(97, 'Staff Note', 'staff-note-in-active', 'In-Active', 'In-Active Staff Note', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(98, 'Staff Note', 'staff-note-bulk-action', 'Bulk(Active,InActive,Delete)', 'Staff Note Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(99, 'Staff Designation', 'staff-designation-index', 'Index', 'Staff Designation Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(100, 'Staff Designation', 'staff-designation-add', 'Add', 'Staff Designation Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(101, 'Staff Designation', 'staff-designation-edit', 'Edit', 'Edit Staff Designation', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(102, 'Staff Designation', 'staff-designation-delete', 'Delete', 'Delete Staff Designation', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(103, 'Staff Designation', 'staff-designation-active', 'Active', 'Active Staff Designation', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(104, 'Staff Designation', 'staff-designation-in-active', 'In-Active', 'In-Active Staff Designation', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(105, 'Staff Designation', 'staff-designation-bulk-action', 'Bulk(Active,InActive,Delete)', 'Staff Designation Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(106, 'Fees', 'fees-index', 'Index', 'Student Fees Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(107, 'Fees', 'fees-balance', 'Balance', 'Fees Balance', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(108, 'Installment', 'installments-index', 'Index', 'Index Installment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(109, 'Installment', 'installments-add', 'Add', 'Add Installment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(110, 'Installment', 'installments-edit', 'Edit', 'Edit Installment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(111, 'Installment', 'installments-delete', 'Delete', 'Delete Installment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(112, 'Installment', 'installments-active', 'Active', 'Active Installment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(113, 'Installment', 'installments-in-active', 'In-Active', 'In-Active Installment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(114, 'Installment', 'installments-bulk-action', 'Bulk Action', 'Installment Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(115, 'Fees Head', 'fees-head-index', 'Index', 'Fees Head Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(116, 'Fees Head', 'fees-head-add', 'Add', 'Fees Head Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(117, 'Fees Head', 'fees-head-edit', 'Edit', 'Edit Fees Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(118, 'Fees Head', 'fees-head-delete', 'Delete', 'Delete Fees Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(119, 'Fees Head', 'fees-head-active', 'Active', 'Active Fees Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(120, 'Fees Head', 'fees-head-in-active', 'In-Active', 'In-Active Fees Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(121, 'Fees Head', 'fees-head-bulk-action', 'Bulk(Active,InActive,Delete)', 'Fees Head Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(122, 'Fees Process Type', 'fees-type-index', 'Index', 'Fees Process Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(123, 'Fees Process Type', 'fees-type-add', 'Add', 'Fees Process Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(124, 'Fees Process Type', 'fees-type-edit', 'Edit', 'Edit Fees Process', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(125, 'Fees Process Type', 'fees-type-delete', 'Delete', 'Delete Fees Process', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(126, 'Fees Process Type', 'fees-type-active', 'Active', 'Active Fees Process', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(127, 'Fees Process Type', 'fees-type-in-active', 'In-Active', 'In-ActiveFees Process', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(128, 'Fees Process Type', 'fees-type-bulk-action', 'Bulk(Active,InActive,Delete)', 'Fees Process Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(129, 'Fees Master', 'fees-master-index', 'Index', 'Fees Master Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(130, 'Fees Master', 'fees-master-add', 'Add', 'Fees Master Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(131, 'Fees Master', 'fees-master-edit', 'Edit', 'Edit Fees Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(132, 'Fees Master', 'fees-master-view', 'View', 'View Fees Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(133, 'Fees Master', 'fees-master-delete', 'Delete', 'Delete Fees Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(134, 'Fees Master', 'fees-master-active', 'Active', 'Active Fees Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(135, 'Fees Master', 'fees-master-in-active', 'In-Active', 'In-Active Fees Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(136, 'Fees Master', 'fees-master-bulk-action', 'Bulk(Active,InActive,Delete)', 'Fees Master Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(137, 'Fees Collection', 'fees-collection-index', 'Index', 'Fees Collection Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(138, 'Fees Collection', 'fees-collection-add', 'Add', 'Fees Collection Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(139, 'Fees Collection', 'fees-collection-view', 'View', 'View Fees Collection', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(140, 'Fees Collection', 'fees-collection-delete', 'Delete', 'Delete Fees Collection', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(141, 'Fees Collection', 'fees-collection-reconciliation', 'Reconciliation', 'Reconciliation Fees Collection', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(142, 'Fees Collection', 'fees-collection-reconciliation-post', 'Reconciliation Post', 'Reconciliation Post Fees Collection', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(143, 'Fees Collection', 'fees-collection-view-detail', 'View Detail', 'View Detail Fees Collection', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(144, 'Fees Collection', 'fees-collection-voucher-add', 'Add Voucher', 'Add Fee Voucher', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(145, 'Payroll', 'payroll-index', 'Index', 'Staff Payroll Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(146, 'Payroll', 'payroll-balance', 'Balance', 'Payroll Balance', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(147, 'Payroll Head', 'payroll-head-index', 'Index', 'Payroll Head Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(148, 'Payroll Head', 'payroll-head-add', 'Add', 'Payroll Head Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(149, 'Payroll Head', 'payroll-head-edit', 'Edit', 'Edit Payroll Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(150, 'Payroll Head', 'payroll-head-delete', 'Delete', 'Delete Payroll Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(151, 'Payroll Head', 'payroll-head-active', 'Active', 'Active Payroll Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(152, 'Payroll Head', 'payroll-head-in-active', 'In-Active', 'In-Active Payroll Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(153, 'Payroll Head', 'payroll-head-bulk-action', 'Bulk(Active,InActive,Delete)', 'Payroll Head Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(154, 'Payroll Master', 'payroll-master-index', 'Index', 'Payroll Master Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(155, 'Payroll Master', 'payroll-master-add', 'Add', 'Payroll Master Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(156, 'Payroll Master', 'payroll-master-edit', 'Edit', 'Edit Payroll Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(157, 'Payroll Master', 'payroll-master-delete', 'Delete', 'Delete Payroll Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(158, 'Payroll Master', 'payroll-master-active', 'Active', 'Active Payroll Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(159, 'Payroll Master', 'payroll-master-in-active', 'In-Active', 'In-Active Payroll Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(160, 'Payroll Master', 'payroll-master-bulk-action', 'Bulk(Active,InActive,Delete)', 'Payroll Master Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(161, 'Salary Pay', 'salary-payment-index', 'Index', 'Salary Pay Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(162, 'Salary Pay', 'salary-payment-add', 'Add', 'Salary Pay Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(163, 'Salary Pay', 'salary-payment-view', 'View', 'View Salary Pay', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(164, 'Salary Pay', 'salary-payment-delete', 'Delete', 'Delete Salary Pay', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(165, 'Transaction Head', 'transaction-head-index', 'Index', 'Transaction Head Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(166, 'Transaction Head', 'transaction-head-add', 'Add', 'Transaction Head Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(167, 'Transaction Head', 'transaction-head-edit', 'Edit', 'Edit Transaction Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(168, 'Transaction Head', 'transaction-head-delete', 'Delete', 'Delete Transaction Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(169, 'Transaction Head', 'transaction-head-active', 'Active', 'Active Transaction Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(170, 'Transaction Head', 'transaction-head-in-active', 'In-Active', 'In-Active Transaction Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(171, 'Transaction Head', 'transaction-head-bulk-action', 'Bulk(Active,InActive,Delete)', 'Payroll Head Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(172, 'Transaction', 'transaction-index', 'Index', 'Transaction Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(173, 'Transaction', 'transaction-add', 'Add', 'Transaction Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(174, 'Transaction', 'transaction-edit', 'Edit', 'Edit Transaction', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(175, 'Transaction', 'transaction-delete', 'Delete', 'Delete Transaction', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(176, 'Transaction', 'transaction-active', 'Active', 'Active Transaction', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(177, 'Transaction', 'transaction-in-active', 'In-Active', 'In-Active Transaction', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(178, 'Transaction', 'transaction-bulk-action', 'Bulk(Active,InActive,Delete)', 'Transaction Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(179, 'Fees Structure', 'fees-structure-index', 'Index', 'Fees Structure Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(180, 'Fees Structure', 'fees-structure-add', 'Add', 'Fees Structure Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(181, 'Fees Structure', 'fees-structure-edit', 'Edit', 'Edit Fees Structure', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(182, 'Fees Structure', 'fees-structure-delete', 'Delete', 'Delete Fees Structure', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(183, 'Fees Structure', 'fees-structure-active', 'Active', 'Active Fees Structure', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(184, 'Fees Structure', 'fees-structure-in-active', 'In-Active', 'In-Active Fees Structure', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(185, 'Fees Structure', 'fees-structure-bulk-action', 'Bulk(Active,InActive,Delete)', 'Fees Structure Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(186, 'Group Members', 'group-member-index', 'Index', 'Group Members Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(187, 'Group Members', 'group-member-add', 'Add', 'Group Members Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(188, 'Group Members', 'group-member-edit', 'Edit', 'Edit Group Members', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(189, 'Group Members', 'group-member-delete', 'Delete', 'Delete Group Members', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(190, 'Group Members', 'group-member-active', 'Active', 'Active Group Members', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(191, 'Group Members', 'group-member-in-active', 'In-Active', 'In-Active Group Members', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(192, 'Group Members', 'group-member-bulk-action', 'Bulk(Active,InActive,Delete)', 'Group Members Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(193, 'Group Members', 'group-member-staff', 'Group Staff', 'Group Members Staff', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(194, 'Group Members', 'group-member-staff-view', 'Group Staff View', 'Group Members Staff View', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(195, 'Group Members', 'group-member-student', 'Group Student', 'Group Members Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(196, 'Group Members', 'group-member-student-view', 'Group Student View', 'Group Members Student View', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(197, 'Account Print', 'fee-print-master', 'Master Slip', 'Fee Master Slip', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(198, 'Account Print', 'fee-print-collection', 'Collection', 'Print Fee Collection', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(199, 'Account Print', 'fee-print-today-receipt', 'Today Short Receipt', 'Print Today Short Receipt', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(200, 'Account Print', 'fee-print-today-detail-receipt', 'Today Detail Receipt', 'Print Today Detail Receip', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(201, 'Account Print', 'fee-print-student-ledger', 'Student Ledger', 'Print Student Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(202, 'Account Print', 'fee-print-student-due', 'Due Short Slip', 'Print Due Short Slip', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(203, 'Account Print', 'fee-print-student-due-detail', 'Due Detail Slip', 'Print Due Detail Slip', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(204, 'Account Print', 'fee-print-bulk-due-slip', 'Bulk Due Short Slip', 'Print Bulk Due Short Slip', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(205, 'Account Print', 'fee-print-bulk-due-detail-slip', 'Bulk Due Detail SLip', 'Print Bulk Due Detail SLip', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(206, 'Library', 'library-index', 'Index', 'Library Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(207, 'Library', 'library-book-issue', 'Book Issue', 'Book Issue', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(208, 'Library', 'library-book-return', 'Book Return', 'Return Book', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(209, 'Library', 'library-return-over', 'Return Period Over', 'Return Period Over', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(210, 'Library', 'library-issue-history', 'History', 'Issue History', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(211, 'Book', 'book-index', 'Index', 'Book Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(212, 'Book', 'book-add', 'Add', 'Book Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(213, 'Book', 'book-edit', 'Edit', 'Edit Book', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(214, 'Book', 'book-view', 'View', 'View Book', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(215, 'Book', 'book-delete', 'Delete', 'Delete Book', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(216, 'Book', 'book-active', 'Active', 'Active Book', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(217, 'Book', 'book-in-active', 'In-Active', 'In-Active Book', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(218, 'Book', 'book-bulk-action', 'Bulk(Active,InActive,Delete)', 'Book Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(219, 'Book', 'book-add-copies', 'Add Book Copies', 'Add Book Copies', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(220, 'Book', 'book-status', 'Status', 'Book Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(221, 'Book', 'book-bulk-copies-delete', 'Delete Bulk Copies', 'Delete Bulk Copes', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(222, 'Book Category', 'book-category-index', 'Index', 'Book Category Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(223, 'Book Category', 'book-category-add', 'Add', 'Book Category Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(224, 'Book Category', 'book-category-edit', 'Edit', 'Edit Book Category', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(225, 'Book Category', 'book-category-delete', 'Delete', 'Delete Book Category', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(226, 'Book Category', 'book-category-active', 'Active', 'Active Book Category', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(227, 'Book Category', 'book-category-in-active', 'In-Active', 'In-Active Book Category', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(228, 'Book Category', 'book-category-bulk-action', 'Bulk(Active,InActive,Delete)', 'Book Category Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(229, 'Library Circulation', 'library-circulation-index', 'Index', 'Library Circulation Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(230, 'Library Circulation', 'library-circulation-add', 'Add', 'Library Circulation Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(231, 'Library Circulation', 'library-circulation-edit', 'Edit', 'Edit Library Circulation', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(232, 'Library Circulation', 'library-circulation-delete', 'Delete', 'Delete Library Circulation', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(233, 'Library Circulation', 'library-circulation-active', 'Active', 'Active Library Circulation', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(234, 'Library Circulation', 'library-circulation-in-active', 'In-Active', 'In-Active Library Circulation', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(235, 'Library Circulation', 'library-circulation-bulk-action', 'Bulk(Active,InActive,Delete)', 'Library Circulation Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(236, 'Library Member', 'library-member-index', 'Index', 'Library Member Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(237, 'Library Member', 'library-member-add', 'Add', 'Library Member Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(238, 'Library Member', 'library-member-edit', 'Edit', 'Edit Library Member', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(239, 'Library Member', 'library-member-delete', 'Delete', 'Delete Library Member', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(240, 'Library Member', 'library-member-active', 'Active', 'Active Library Member', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(241, 'Library Member', 'library-member-in-active', 'In-Active', 'In-Active Library Member', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(242, 'Library Member', 'library-member-bulk-action', 'Bulk(Active,InActive,Delete)', 'Library Member Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(243, 'Library Member', 'library-member-staff', 'Staff Index', 'Library Member Staff', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(244, 'Library Member', 'library-member-staff-view', 'Staff View', 'Library Member Staff', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(245, 'Library Member', 'library-member-student', 'Student Index', 'Library Member Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(246, 'Library Member', 'library-member-student-view', 'Student View', 'Library Member Student', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(247, 'Attendance Master', 'attendance-master-index', 'Index', 'Attendance Master Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(248, 'Attendance Master', 'attendance-master-add', 'Add', 'Attendance Master Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(249, 'Attendance Master', 'attendance-master-edit', 'Edit', 'Edit Attendance Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(250, 'Attendance Master', 'attendance-master-delete', 'Delete', 'Delete Attendance Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(251, 'Attendance Master', 'attendance-master-active', 'Active', 'Active Attendance Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(252, 'Attendance Master', 'attendance-master-in-active', 'In-Active', 'In-Active Attendance Master', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(253, 'Attendance Master', 'attendance-master-bulk-action', 'Bulk(Active,InActive,Delete)', 'Attendance Master Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(254, 'Student Attendance', 'student-attendance-index', 'Index', 'Student Attendance Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(255, 'Student Attendance', 'student-attendance-add', 'Add', 'Student Attendance Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(256, 'Student Attendance', 'student-attendance-delete', 'Delete', 'Delete Student Attendance', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(257, 'Student Attendance', 'student-attendance-bulk-action', 'Bulk(Active,InActive,Delete)', 'Student Attendance Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(258, 'Staff Attendance', 'staff-attendance-index', 'Index', 'Staff Attendance Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(259, 'Staff Attendance', 'staff-attendance-add', 'Add', 'Staff Attendance Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(260, 'Staff Attendance', 'staff-attendance-delete', 'Delete', 'Delete Staff Attendance', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(261, 'Staff Attendance', 'staff-attendance-bulk-action', 'Bulk(Active,InActive,Delete)', 'Staff Attendance Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(262, 'Exam', 'exam-index', 'Index', 'Exam Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(263, 'Exam', 'exam-add', 'Add', 'Exam Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(264, 'Exam', 'exam-edit', 'Edit', 'Edit Exam', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(265, 'Exam', 'exam-delete', 'Delete', 'Delete Exam', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(266, 'Exam', 'exam-active', 'Active', 'Active Exam', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(267, 'Exam', 'exam-in-active', 'In-Active', 'In-Active Exam', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(268, 'Exam', 'exam-bulk-action', 'Bulk(Active,InActive,Delete)', 'Exam Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(269, 'Exam', 'exam-admit-card', 'Admit Card', 'Exam Admit Card', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(270, 'Exam', 'exam-exam-routine', 'Routin/Schedule', 'Exam Routine/Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(271, 'Exam', 'exam-mark-sheet', 'MarkSheet', 'Exam Mark Sheet', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(272, 'Exam', 'exam-result-publish', 'Result Publish', 'Exam Result Publish', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(273, 'Exam', 'exam-result-un-publish', 'Result UnPublish', 'Exam Result UnPublish', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(274, 'Exam Schedule', 'exam-schedule-index', 'Index', 'Exam Schedule Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(275, 'Exam Schedule', 'exam-schedule-add', 'Add', 'Exam Schedule Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(276, 'Exam Schedule', 'exam-schedule-edit', 'Edit', 'Edit Exam Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(277, 'Exam Schedule', 'exam-schedule-delete', 'Delete', 'Delete Exam Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(278, 'Exam Schedule', 'exam-schedule-active', 'Active', 'Active Exam Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(279, 'Exam Schedule', 'exam-schedule-in-active', 'In-Active', 'In-Active Exam Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(280, 'Exam Schedule', 'exam-schedule-view', 'View', 'View Exam Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(281, 'Exam Mark Ledger', 'exam-mark-ledger-index', 'Index', 'Exam Mark Ledger Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(282, 'Exam Mark Ledger', 'exam-mark-ledger-add', 'Add', 'Exam Mark Ledger Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(283, 'Exam Mark Ledger', 'exam-mark-ledger-edit', 'Edit', 'Edit Exam Mark Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(284, 'Exam Mark Ledger', 'exam-mark-ledger-delete', 'Delete', 'Delete Exam Mark Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(285, 'Exam Mark Ledger', 'exam-mark-ledger-active', 'Active', 'Active Exam Mark Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(286, 'Exam Mark Ledger', 'exam-mark-ledger-in-active', 'In-Active', 'In-Active Exam Mark Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(287, 'Exam Print', 'exam-print-admitcard', 'Admit Card', 'Exam Admit Card', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(288, 'Exam Print', 'exam-print-routine', 'Routine/Schedule', 'Exam Routine/Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(289, 'Exam Print', 'exam-print-mark-sheet', 'Mark/Grade Sheet', 'Exam Mark/Grade Sheet', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(290, 'Report', 'student-report', 'Student', 'Student Report', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(291, 'Report', 'admission-report', 'Admission', 'Admission Report', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(292, 'Report', 'branch-report', 'Branch', 'Branch Report', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(293, 'Report', 'staff-report', 'Staff Reports', 'Staff Report', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(294, 'Report', 'academics-reports', 'Academics Reports', 'Academics Report', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(295, 'Report', 'fee-reports', 'Fee', 'Fee Report', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(296, 'Report', 'classinfo-index', 'Class Info Index', 'Class Info Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(297, 'Report', 'classinfo-detail', 'Class Info Detail', 'Class Info Detail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(298, 'Report', 'learner-profile-index', 'Learner Profile Index', 'Learner Profile Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(299, 'Report', 'learner-profile-add', 'Learner Profile ADD', 'Add Learner Profile', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(300, 'Report', 'learner-profile-active', 'Learner Profile Active', 'Active Learner Profile', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(301, 'Report', 'learner-profile-inactive', 'Learner Profile In-Active', 'In-Active Learner Profile', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(302, 'Notice', 'notice-index', 'Index', 'Notice Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(303, 'Notice', 'notice-add', 'Add', 'Notice Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(304, 'Notice', 'notice-edit', 'Edit', 'Edit Notice', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(305, 'Notice', 'notice-delete', 'Delete', 'Delete Notice', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(306, 'Class Notice', 'classnotice-add', 'Add', 'Notice Class Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(307, 'Class Notice', 'classnotice-edit', 'Edit', 'Edit Class Notice', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(308, 'Class Notice', 'classnotice-delete', 'Delete', 'Delete Class Notice', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(309, 'SMS/E-Mail', 'sms-email-index', 'Index', 'SMS/E-Mail Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(310, 'SMS/E-Mail', 'sms-email-delete', 'Delete', 'SMS/E-Mail Delete', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(311, 'SMS/E-Mail', 'sms-email-bulk-action', 'Bulk(Active,InActive,Delete)', 'Bulk SMS/E-Mail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(312, 'SMS/E-Mail', 'sms-email-create', 'Create', 'Create SMS/E-Mail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(313, 'SMS/E-Mail', 'sms-email-send', 'Send', 'Send SMS/E-Mail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(314, 'SMS/E-Mail', 'sms-email-student-guardian-send', 'Student & Guardian', 'Student & Guardian SMS/E-Mail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(315, 'SMS/E-Mail', 'sms-email-staff-send', 'Staff', 'Staff SMS/E-Mail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(316, 'SMS/E-Mail', 'sms-email-individual-send', 'Individual', 'Individual SMS/E-Mail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(317, 'SMS/E-Mail', 'sms-email-fee-receipt', 'Fee Receipt', 'Fee ReceiptSMS/E-Mail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(318, 'SMS/E-Mail', 'sms-email-due-reminder', 'Due Reminder', 'Due Reminder SMS/E-Mail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(319, 'SMS/E-Mail', 'sms-email-book-return-reminder', 'Book Return Reminder', 'Book Return Reminder', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(320, 'Student Status', 'student-status-index', 'Index', 'Student Status Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(321, 'Student Status', 'student-status-add', 'Add', 'Student Status Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(322, 'Student Status', 'student-status-edit', 'Edit', 'Edit Student Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(323, 'Student Status', 'student-status-delete', 'Delete', 'Delete Student Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(324, 'Student Status', 'student-status-active', 'Active', 'Active Student Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(325, 'Student Status', 'student-status-in-active', 'In-Active', 'In-Active Student Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(326, 'Student Status', 'student-status-bulk-action', 'Bulk(Active,InActive,Delete)', 'Student Status Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(327, 'Attendance Status', 'attendance-status-index', 'Index', 'Attendance Status Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(328, 'Attendance Status', 'attendance-status-add', 'Add', 'Attendance Status Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(329, 'Attendance Status', 'attendance-status-edit', 'Edit', 'Edit Attendance Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(330, 'Attendance Status', 'attendance-status-delete', 'Delete', 'Delete Attendance Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(331, 'Attendance Status', 'attendance-status-active', 'Active', 'Active Attendance Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(332, 'Attendance Status', 'attendance-status-in-active', 'In-Active', 'In-Active Attendance Status', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(333, 'Attendance Status', 'attendance-status-bulk-action', 'Bulk(Active,InActive,Delete)', 'Attendance Status Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(334, 'Year', 'year-index', 'Index', 'Year Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(335, 'Year', 'year-add', 'Add', 'Year Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(336, 'Year', 'year-edit', 'Edit', 'Edit Year', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(337, 'Year', 'year-delete', 'Delete', 'Delete Year', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(338, 'Year', 'year-active', 'Active', 'Active Year', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(339, 'Year', 'year-in-active', 'In-Active', 'In-Active Year', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(340, 'Year', 'year-bulk-action', 'Bulk(Active,InActive,Delete)', 'Year Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(341, 'Year', 'years-active-status', 'Make Active', 'Year Make Active', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(342, 'Year-Group', 'years-group-index', 'Year Group Active', 'Year Group Make Active', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(343, 'Month', 'month-index', 'Index', 'Month Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(344, 'Month', 'month-add', 'Add', 'Month Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(345, 'Month', 'month-edit', 'Edit', 'Edit Month', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(346, 'Month', 'month-delete', 'Delete', 'Delete Month', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(347, 'Month', 'month-active', 'Active', 'Active Month', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(348, 'Month', 'month-in-active', 'In-Active', 'In-Active Month', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(349, 'Month', 'month-bulk-action', 'Bulk(Active,InActive,Delete)', 'Month Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(350, 'Day', 'day-index', 'Index', 'Day Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(351, 'Day', 'day-add', 'Add', 'Day Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(352, 'Day', 'day-edit', 'Edit', 'Edit Day', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(353, 'Day', 'day-delete', 'Delete', 'Delete Day', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(354, 'Day', 'day-active', 'Active', 'Active Day', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(355, 'Day', 'day-in-active', 'In-Active', 'In-Active Day', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(356, 'Day', 'day-bulk-action', 'Bulk(Active,InActive,Delete)', 'Day Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(357, 'Board', 'board-index', 'Index', 'Board Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(358, 'Board', 'board-add', 'Add', 'Board Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(359, 'Board', 'board-edit', 'Edit', 'Edit Board', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(360, 'Board', 'board-delete', 'Delete', 'Delete Board', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(361, 'Board', 'board-active', 'Active', 'Active Board', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(362, 'Board', 'board-in-active', 'In-Active', 'In-Active Board', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(363, 'Term', 'term-index', 'Index', 'Term Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(364, 'Term', 'term-add', 'Add', 'Term Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(365, 'Term', 'term-edit', 'Edit', 'Edit Term', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(366, 'Term', 'term-delete', 'Delete', 'Delete Term', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(367, 'Term', 'term-active', 'Active', 'Active Term', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(368, 'Term', 'term-in-active', 'In-Active', 'In-Active Term', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(369, 'Term', 'term-bulk-action', 'Bulk(Active,InActive,Delete)', 'Term Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(370, 'Grading', 'grading-index', 'Index', 'Grading Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(371, 'Grading', 'grading-add', 'Add', 'Grading Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(372, 'Grading', 'grading-edit', 'Edit', 'Edit Grading', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(373, 'Grading', 'grading-delete', 'Delete', 'Delete Grading', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(374, 'Grading', 'grading-active', 'Active', 'Active Grading', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(375, 'Grading', 'grading-in-active', 'In-Active', 'In-Active Grading', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(376, 'Grading', 'grading-bulk-action', 'Bulk(Active,InActive,Delete)', 'Grading Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(377, 'Program', 'program-index', 'Index', 'Program Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(378, 'Program', 'program-add', 'Add', 'Program Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(379, 'Program', 'program-edit', 'Edit', 'Edit Program', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(380, 'Program', 'program-delete', 'Delete', 'Delete Program', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(381, 'Program', 'program-active', 'Active', 'Active Program', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(382, 'Program', 'program-in-active', 'In-Active', 'In-Active Program', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(383, 'Program', 'program-bulk-action', 'Bulk(Active,InActive,Delete)', 'Program Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(384, 'Subject / Course', 'subject-index', 'Index', 'Subject / Course Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(385, 'Subject / Course', 'subject-add', 'Add', 'Subject / Course Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(386, 'Subject / Course', 'subject-edit', 'Edit', 'Edit Subject / Course', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(387, 'Subject / Course', 'subject-delete', 'Delete', 'Delete Subject / Course', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(388, 'Subject / Course', 'subject-active', 'Active', 'Active Subject / Course', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(389, 'Subject / Course', 'subject-in-active', 'In-Active', 'In-Active Subject / Course', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(390, 'Subject / Course', 'subject-bulk-action', 'Bulk(Active,InActive,Delete)', 'Subject / Course Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(391, 'Class', 'faculty-index', 'Index', 'Class Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(392, 'Class', 'faculty-add', 'Add', 'Class Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(393, 'Class', 'faculty-edit', 'Edit', 'Edit Class', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(394, 'Class', 'faculty-delete', 'Delete', 'Delete Class', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(395, 'Class', 'faculty-active', 'Active', 'Active Class', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(396, 'Class', 'faculty-in-active', 'In-Active', 'In-Active Class', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(397, 'Class', 'faculty-bulk-action', 'Bulk(Active,InActive,Delete)', 'Class Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(398, 'Section', 'facultysemester-index', 'Index', 'Section Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(399, 'Section', 'facultysemester-add', 'Add', 'Section Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(400, 'Section', 'facultysemester-edit', 'Edit', 'Edit Section', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(401, 'Section', 'facultysemester-delete', 'Delete', 'Delete Section', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(402, 'Section', 'facultysemester-active', 'Active', 'Active Section', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(403, 'Section', 'facultysemester-in-active', 'In-Active', 'In-Active Section', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(404, 'Section', 'facultysemester-bulk-action', 'Bulk(Active,InActive,Delete)', 'Section Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(405, 'Syllabus', 'syllabus-index', 'Index', 'Syllabus Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(406, 'Syllabus', 'syllabus-add', 'Add', 'Syllabus Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(407, 'Syllabus', 'syllabus-edit', 'Edit', 'Edit Syllabus', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(408, 'Syllabus', 'syllabus-delete', 'Delete', 'Delete Syllabus', '2020-09-23 03:22:14', '2020-09-23 03:22:14');
INSERT INTO `permissions` (`id`, `group`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(409, 'Syllabus', 'syllabus-active', 'Active', 'Active Syllabus', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(410, 'Syllabus', 'syllabus-in-active', 'In-Active', 'In-Active Syllabus', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(411, 'Syllabus', 'syllabus-bulk-action', 'Bulk(Active,InActive,Delete)', 'Syllabus Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(412, 'Syllabus Head', 'syllabus-head-index', 'Index', 'Syllabus Head Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(413, 'Syllabus Head', 'syllabus-head-add', 'Add', 'Syllabus Head Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(414, 'Syllabus Head', 'syllabus-head-edit', 'Edit', 'Edit Syllabus Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(415, 'Syllabus Head', 'syllabus-head-delete', 'Delete', 'Delete Syllabus Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(416, 'Syllabus Head', 'syllabus-head-active', 'Active', 'Active Syllabus Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(417, 'Syllabus Head', 'syllabus-head-in-active', 'In-Active', 'In-Active Syllabus Head', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(418, 'Syllabus Head', 'syllabus-head-bulk-action', 'Bulk(Active,InActive,Delete)', 'Syllabus Head Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(419, 'Work Plan', 'workplan-index', 'Index', 'Work Plan Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(420, 'Work Plan', 'workplan-add', 'Add', 'Work Plan Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(421, 'Work Plan', 'workplan-edit', 'Edit', 'Edit Work Plan', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(422, 'Work Plan', 'workplan-delete', 'Delete', 'Delete Work Plan', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(423, 'Work Plan', 'workplan-active', 'Active', 'Active Work Plan', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(424, 'Work Plan', 'workplan-in-active', 'In-Active', 'In-Active Work Plan', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(425, 'Work Plan', 'workplan-detail', 'Detail', 'Work Plan Detail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(426, 'Assignment', 'assignment-index', 'Index', 'Assignment Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(427, 'Assignment', 'assignment-add', 'Add', 'Assignment Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(428, 'Assignment', 'assignment-edit', 'Edit', 'Edit Assignment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(429, 'Assignment', 'assignment-view', 'View', 'View Assignment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(430, 'Assignment', 'assignment-delete', 'Delete', 'Delete Assignment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(431, 'Assignment', 'assignment-active', 'Active', 'Active Assignment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(432, 'Assignment', 'assignment-in-active', 'In-Active', 'In-Active Assignment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(433, 'Assignment', 'assignment-bulk-action', 'Bulk(Active,InActive,Delete)', 'Assignment Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(434, 'Assignment Answer', 'assignment-answer-view', 'View', 'Assignment Answer View', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(435, 'Assignment Answer', 'assignment-answer-approve', 'Approve', 'Approve Assignment Answer', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(436, 'Assignment Answer', 'assignment-answer-reject', 'Reject', 'Reject Assignment Answer', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(437, 'Assignment Answer', 'assignment-answer-delete', 'Delete', 'Delete Assignment Answer', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(438, 'Assignment Answer', 'assignment-answer-bulk-action', 'Bulk(Approve, Reject,Delete)', 'Assignment Answer Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(439, 'E-Course', 'download-index', 'Index', 'E-Course Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(440, 'E-Course', 'download-add', 'Add', 'E-Course Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(441, 'E-Course', 'download-edit', 'Edit', 'Edit E-Course', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(442, 'E-Course', 'download-delete', 'Delete', 'Delete E-Course', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(443, 'E-Course', 'download-active', 'Active', 'Active E-Course', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(444, 'E-Course', 'download-in-active', 'In-Active', 'In-Active E-Course', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(445, 'E-Course', 'download-bulk-action', 'Bulk(Active,InActive,Delete)', 'E-Course Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(446, 'Diary', 'diary-index', 'Index', 'Diary Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(447, 'Diary', 'diary-add', 'Add', 'Diary Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(448, 'Diary', 'diary-edit', 'Edit', 'Edit Diary', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(449, 'Diary', 'diary-view', 'View', 'View Diary', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(450, 'Diary', 'diary-delete', 'Delete', 'Delete Diary', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(451, 'Diary', 'diary-active', 'Active', 'Active Diary', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(452, 'Diary', 'diary-in-active', 'In-Active', 'In-Active Diary', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(453, 'Diary', 'diary-bulk-action', 'Bulk(Active,InActive,Delete)', 'Diary Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(454, 'Past Papers', 'pastpaper-index', 'Index', 'Past Papers Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(455, 'Past Papers', 'pastpaper-add', 'Add', 'Past Papers Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(456, 'Past Papers', 'pastpaper-edit', 'Edit', 'Edit Past Papers', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(457, 'Past Papers', 'pastpaper-delete', 'Delete', 'Delete Past Papers', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(458, 'Past Papers', 'pastpaper-active', 'Active', 'Active Past Papers', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(459, 'Past Papers', 'pastpaper-in-active', 'In-Active', 'In-Active Past Papers', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(460, 'Past Papers', 'pastpaper-bulk-action', 'Bulk(Active,InActive,Delete)', 'Past Papers Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(461, 'Quiz', 'quiz-index', 'Index', 'Quiz Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(462, 'Quiz', 'quiz-add', 'Add', 'Quiz Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(463, 'Quiz', 'quiz-edit', 'Edit', 'Edit Quiz', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(464, 'Quiz', 'quiz-delete', 'Delete', 'Delete Quiz', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(465, 'Quiz', 'quiz-active', 'Active', 'Active Quiz', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(466, 'Quiz', 'quiz-in-active', 'In-Active', 'In-Active Quiz', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(467, 'Quiz', 'quiz-bulk-action', 'Bulk(Active,InActive,Delete)', 'Quiz Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(468, 'Quiz', 'quiz-mark-sheet', 'MarkSheet', 'Quiz Mark Sheet', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(469, 'Quiz', 'quiz-quiz-routine', 'Routine', 'Quiz Routine', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(470, 'Quiz', 'quiz-result-publish', 'Result Publish', 'Quiz Result Publish', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(471, 'Quiz', 'quiz-result-un-publish', 'Result UnPublish', 'Quiz Result UnPublish', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(472, 'Quizz', 'all-quizz', 'All Quizzes', 'All Quizzes', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(473, 'Quiz Schedule', 'quiz-schedule-index', 'Index', 'Quiz Schedule Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(474, 'Quiz Schedule', 'quiz-schedule-add', 'Add', 'Quiz Schedule Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(475, 'Quiz Schedule', 'quiz-schedule-edit', 'Edit', 'Edit Quiz Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(476, 'Exam Schedule', 'quiz-schedule-delete', 'Delete', 'Delete Exam Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(477, 'Quiz Schedule', 'quiz-schedule-active', 'Active', 'Active Quiz Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(478, 'Quiz Schedule', 'quiz-schedule-in-active', 'In-Active', 'In-Active Quiz Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(479, 'Quiz Schedule', 'quiz-schedule-view', 'View', 'View Quiz Schedule', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(480, 'Quizz Published', 'quiz-schedule-published', 'Published', 'Quizz Published', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(481, 'Quizz UnPublished', 'quiz-schedule-unpublished', 'UnPublished', 'Quizz UnPublished', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(482, 'Quiz Mark Ledger', 'quiz-mark-ledger-index', 'Index', 'Quiz Mark Ledger Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(483, 'Quiz Mark Ledger', 'quiz-mark-ledger-add', 'Add', 'Quiz Mark Ledger Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(484, 'Quiz Mark Ledger', 'quiz-mark-ledger-edit', 'Edit', 'Edit Quiz Mark Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(485, 'Exam Mark Ledger', 'quiz-mark-ledger-delete', 'Delete', 'Delete Exam Mark Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(486, 'Quiz Mark Ledger', 'quiz-mark-ledger-active', 'Active', 'Active Quiz Mark Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(487, 'Quiz Mark Ledger', 'quiz-mark-ledger-in-active', 'In-Active', 'In-Active Quiz Mark Ledger', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(488, 'Quiz Print', 'quiz-print-mark-sheet', 'Mark Sheet', 'Quiz Mark Sheet', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(489, 'Home Room', 'homeroom-index', 'Index', 'Home Room Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(490, 'Home Room', 'homeroom-add', 'Add', 'Home Room Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(491, 'Home Room', 'homeroom-edit', 'Edit', 'Edit Home Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(492, 'Home Room', 'homeroom-delete', 'Delete', 'Delete Home Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(493, 'Home Room', 'homeroom-active', 'Active', 'Active Home Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(494, 'Home Room', 'homeroom-in-active', 'In-Active', 'In-Active Home Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(495, 'Home Room', 'homeroom-bulk-action', 'Bulk(Active,InActive,Delete)', 'Home Room Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(496, 'Groups', 'group-index', 'Index', 'Groups Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(497, 'Groups', 'group-add', 'Add', 'Groups Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(498, 'Groups', 'group-edit', 'Edit', 'Edit Groups', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(499, 'Groups', 'group-detail', 'Detail', 'Groups Detail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(500, 'Groups', 'group-event-detail', 'Event Details', 'Groups Event Detail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(501, 'Groups', 'group-delete', 'Delete', 'Delete Groups', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(502, 'Groups', 'group-active', 'Active', 'Active Groups', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(503, 'Groups', 'group-in-active', 'In-Active', 'In-Active Groups', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(504, 'Groups', 'group-bulk-action', 'Bulk(Active,InActive,Delete)', 'Groups Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(505, 'Groups', 'group-student-active', 'Groups Student Active', 'Groups Student Active', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(506, 'Groups', 'group-student-in-active', 'Groups Student InActive', 'Groups Student InActive', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(507, 'Groups', 'group-event', 'Groups Event', 'Groups Event', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(508, 'Groups', 'group-event-comment-store', 'Groups Event Store Comment', 'Groups Event Store Comment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(509, 'Groups', 'group-event-comments', 'Groups Event Comment', 'Groups Event Comment', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(510, 'Groups', 'group-event-filedetail', 'Groups Event File Detail', 'Groups Event File Detail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(511, 'Groups', 'group-event-todoform', 'Groups Event Todo', 'Groups Event Todo', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(512, 'Groups', 'group-event-todostore', 'Groups Event Add Todo', 'Groups Event Add Todo', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(513, 'Calendar', 'calender-index', 'Index', 'Calendar Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(514, 'Calendar', 'calender-create', 'Add', 'Calendar Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(515, 'Calendar', 'calender-update', 'Edit', 'EditCalendar', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(516, 'Calendar', 'calender-destroy', 'Delete', 'Delete Event Calendar', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(517, 'Event Calendar', 'eventcalender-index', 'Index', 'Event Calendar Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(518, 'Event Calendar', 'eventcalender-create', 'Add', 'Event Calendar Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(519, 'Event Calendar', 'eventcalender-update', 'Edit', 'Edit Event Calendar', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(520, 'Event Calendar', 'eventcalender-destroy', 'Delete', 'Delete Event Calendar', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(521, 'Class Calendar', 'maincalendar-index', 'Index', 'Event Calendar Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(522, 'Class Calendar', 'maincalendar-create', 'Add', 'Event Calendar Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(523, 'Class Calendar', 'maincalendar-update', 'Edit', 'Edit Event Calendar', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(524, 'Class Calendar', 'maincalendar-destroy', 'Delete', 'Delete Event Calendar', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(525, 'Main Calendar Quiz', 'maincalendar-quiz-index', 'Index', 'Main Calendar Quiz Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(526, 'Main Calendar Quiz', 'maincalendar-quiz-create', 'Add', 'Main Calendar Quiz Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(527, 'Main Calendar Quiz', 'maincalendar-quiz-update', 'Edit', 'Edit Main Calendar Quiz', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(528, 'Main Calendar Quiz', 'maincalendar-quiz-destroy', 'Delete', 'Delete Main Calendar Quiz', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(529, 'Extra Curricular', 'ecurricular-index', 'Index', 'Extra Curricular Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(530, 'Extra Curricular', 'ecurricular-create', 'Add', 'Extra Curricular Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(531, 'Extra Curricular', 'ecurricular-update', 'Edit', 'Edit Extra Curricular', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(532, 'Extra Curricular', 'ecurricular-destroy', 'Delete', 'Delete Extra Curricular', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(533, 'Portfolio Timeline', 'portfoliotimeline-index', 'Index', 'Extra Portfolio Timeline', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(534, 'Portfolio Timeline', 'portfoliotimeline-detail', 'Detail', 'Portfolio Timeline Detail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(535, 'Portfolio Timeline', 'portfoliotimeline-table', 'Index', 'View Portfolio Timeline', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(536, 'Portfolio Timeline', 'portfoliotimeline-student-detail', 'Detail', 'Student Portfolio Detail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(537, 'Portfolio Timeline', 'portfoliotimeline-view', 'View Detail', 'Student Portfolio View Detail', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(538, 'Exam Guide', 'examguide-index', 'Index', 'Exam Guide Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(539, 'Exam Guide', 'examguide-add', 'Add', 'Exam Guide Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(540, 'Exam Guide', 'examguide-edit', 'Edit', 'Edit Exam Guide', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(541, 'Exam Guide', 'examguide-delete', 'Delete', 'Delete Exam Guide', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(542, 'Exam Guide', 'examguide-active', 'Active', 'Active Exam Guide', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(543, 'Exam Guide', 'examguide-in-active', 'In-Active', 'In-Active Exam Guide', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(544, 'Exam Guide', 'examguide-bulk-action', 'Bulk(Active,InActive,Delete)', 'Exam Guide Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(545, 'User Permissions', 'user_permissions-index', 'Index', 'User Permission Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(546, 'User Permissions', 'user_permissions-add', 'Add', 'User Permissions Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(547, 'User Permissions', 'user_permissions-edit', 'Edit', 'Edit User Permissions', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(548, 'Session', 'session-index', 'Index', 'Session Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(549, 'Session', 'session-add', 'Add', 'Session Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(550, 'Session', 'session-edit', 'Edit', 'Edit Session', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(551, 'Session', 'session-delete', 'Delete', 'Delete Session', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(552, 'Session', 'session-active', 'Active', 'Active Session', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(553, 'Session', 'session-in-active', 'In-Active', 'In-Active Session', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(554, 'Session', 'session-bulk-action', 'Bulk(Active,InActive,Delete)', 'Session Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(555, 'Session', 'session-active-status', 'Make Active', 'Session Make Active', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(556, 'Financial Year', 'financial-year-index', 'Index', 'Financial Year Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(557, 'Financial Year', 'financial-year-add', 'Add', 'Financial Year Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(558, 'Financial Year', 'financial-year-edit', 'Edit', 'Edit Financial Year', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(559, 'Financial Year', 'financial-year-delete', 'Delete', 'Delete Financial Year', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(560, 'Financial Year', 'financial-year-active', 'Active', 'Active Financial Year', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(561, 'Financial Year', 'financial-year-in-active', 'In-Active', 'In-Active Financial Year', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(562, 'Financial Year', 'financial-year-bulk-action', 'Bulk(Active,InActive,Delete)', 'Financial Year Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(563, 'Financial Year', 'financial-year-active-status', 'Make Active', 'Financial Year Make Active', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(564, 'Student Attendance', 'student-attendance-report', 'Student attendance report', 'Student attendance report', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(565, 'Room', 'room-index', 'Index', 'Room Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(566, 'Room', 'room-add', 'Add', 'Room Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(567, 'Room', 'room-edit', 'Edit', 'Edit Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(568, 'Room', 'room-delete', 'Delete', 'Delete Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(569, 'Room', 'room-active', 'Active', 'Active Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(570, 'Room', 'room-in-active', 'In-Active', 'In-Active Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(571, 'Room', 'room-bulk-action', 'Bulk(Active,InActive,Delete)', 'Room Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(572, 'Building', 'building-index', 'Index', 'Building Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(573, 'Building', 'building-add', 'Add', 'Building Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(574, 'Building', 'building-edit', 'Edit', 'Edit Building', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(575, 'Building', 'building-delete', 'Delete', 'Delete Building', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(576, 'Building', 'building-active', 'Active', 'Active Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(577, 'Building', 'building-in-active', 'In-Active', 'In-Active Room', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(578, 'Building', 'building-bulk-action', 'Bulk(Active,InActive,Delete)', 'Building Bulk Action', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(579, 'TimeTable', 'timetable-index', 'Index', 'TimeTable Index', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(580, 'TimeTable', 'timetable-add', 'Add', 'TimeTable Add', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(581, 'TimeTable', 'timetable-edit', 'Edit', 'Edit TimeTable', '2020-09-23 03:22:14', '2020-09-23 03:22:14'),
(582, 'TimeTable', 'timetable-delete', 'Delete', 'Delete TimeTable', '2020-09-23 03:22:14', '2020-09-23 03:22:14');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(0, 0),
(1, 1),
(1, 5),
(1, 7),
(1, 11),
(1, 13),
(1, 15),
(2, 1),
(2, 5),
(2, 6),
(2, 7),
(2, 11),
(2, 13),
(2, 15),
(3, 1),
(3, 5),
(3, 6),
(3, 7),
(3, 10),
(3, 11),
(3, 13),
(3, 15),
(4, 1),
(4, 5),
(4, 6),
(4, 7),
(4, 10),
(4, 11),
(4, 13),
(4, 15),
(5, 1),
(5, 5),
(5, 6),
(5, 7),
(5, 11),
(5, 13),
(5, 15),
(6, 1),
(6, 5),
(6, 11),
(6, 12),
(6, 15),
(7, 1),
(7, 5),
(7, 11),
(7, 15),
(8, 1),
(8, 5),
(8, 6),
(8, 7),
(8, 10),
(8, 11),
(8, 13),
(8, 15),
(9, 1),
(9, 5),
(9, 6),
(9, 7),
(9, 10),
(9, 11),
(9, 13),
(9, 15),
(10, 1),
(10, 5),
(10, 6),
(10, 7),
(10, 10),
(10, 11),
(10, 13),
(10, 15),
(11, 1),
(11, 5),
(11, 6),
(11, 11),
(11, 12),
(11, 13),
(11, 15),
(12, 1),
(12, 5),
(12, 6),
(12, 7),
(12, 10),
(12, 11),
(12, 12),
(12, 13),
(12, 15),
(13, 1),
(13, 6),
(13, 11),
(13, 13),
(13, 15),
(14, 1),
(14, 6),
(14, 11),
(14, 13),
(14, 15),
(15, 1),
(15, 6),
(15, 11),
(15, 13),
(15, 15),
(16, 1),
(16, 6),
(16, 11),
(16, 13),
(16, 15),
(17, 1),
(17, 6),
(17, 11),
(17, 13),
(17, 15),
(18, 1),
(18, 6),
(18, 11),
(18, 13),
(18, 15),
(19, 1),
(19, 6),
(19, 11),
(19, 13),
(19, 15),
(20, 1),
(20, 6),
(20, 11),
(20, 13),
(20, 15),
(21, 1),
(21, 6),
(21, 11),
(21, 13),
(21, 15),
(22, 1),
(22, 6),
(22, 11),
(22, 13),
(22, 15),
(23, 1),
(23, 6),
(23, 11),
(23, 13),
(23, 15),
(24, 1),
(24, 6),
(24, 11),
(24, 13),
(24, 15),
(25, 1),
(25, 6),
(25, 11),
(25, 13),
(25, 15),
(26, 1),
(26, 6),
(26, 11),
(26, 13),
(26, 15),
(27, 1),
(27, 6),
(27, 11),
(27, 13),
(27, 15),
(28, 1),
(28, 6),
(28, 11),
(28, 13),
(28, 15),
(29, 1),
(29, 6),
(29, 11),
(29, 13),
(29, 15),
(30, 1),
(30, 6),
(30, 11),
(30, 13),
(30, 15),
(31, 1),
(31, 6),
(31, 11),
(31, 13),
(31, 15),
(32, 1),
(32, 6),
(32, 11),
(32, 13),
(32, 15),
(33, 1),
(33, 6),
(33, 11),
(33, 13),
(33, 15),
(34, 1),
(34, 6),
(34, 11),
(34, 13),
(34, 15),
(35, 1),
(35, 6),
(35, 11),
(35, 13),
(35, 15),
(36, 1),
(36, 6),
(36, 11),
(36, 13),
(36, 15),
(37, 1),
(37, 6),
(37, 11),
(37, 13),
(37, 15),
(38, 1),
(38, 6),
(38, 11),
(38, 13),
(38, 15),
(39, 1),
(39, 6),
(39, 11),
(39, 13),
(39, 15),
(40, 1),
(40, 6),
(40, 11),
(40, 13),
(40, 15),
(41, 1),
(41, 6),
(41, 11),
(41, 13),
(41, 15),
(42, 1),
(42, 6),
(42, 11),
(42, 13),
(42, 15),
(43, 1),
(43, 6),
(43, 11),
(43, 13),
(43, 15),
(44, 1),
(44, 6),
(44, 11),
(44, 13),
(44, 15),
(45, 1),
(45, 5),
(45, 6),
(45, 7),
(45, 10),
(45, 11),
(45, 13),
(45, 15),
(46, 1),
(46, 5),
(46, 11),
(46, 13),
(46, 15),
(47, 1),
(47, 5),
(47, 6),
(47, 10),
(47, 11),
(47, 13),
(47, 15),
(48, 1),
(48, 5),
(48, 11),
(48, 13),
(48, 15),
(49, 1),
(49, 5),
(49, 11),
(49, 13),
(49, 15),
(50, 1),
(50, 5),
(50, 6),
(50, 11),
(50, 13),
(50, 15),
(51, 1),
(51, 5),
(51, 6),
(51, 11),
(51, 13),
(51, 15),
(52, 1),
(52, 5),
(52, 11),
(52, 13),
(52, 15),
(53, 1),
(53, 5),
(53, 11),
(53, 13),
(53, 15),
(54, 1),
(54, 5),
(54, 11),
(54, 13),
(54, 15),
(55, 1),
(55, 5),
(55, 10),
(55, 11),
(55, 13),
(55, 15),
(56, 1),
(56, 5),
(56, 11),
(56, 13),
(56, 15),
(57, 1),
(57, 5),
(57, 11),
(57, 13),
(57, 15),
(58, 1),
(58, 5),
(58, 11),
(58, 13),
(58, 15),
(59, 1),
(59, 5),
(59, 11),
(59, 13),
(59, 15),
(60, 1),
(60, 5),
(60, 11),
(60, 13),
(60, 15),
(61, 1),
(61, 5),
(61, 11),
(61, 13),
(61, 15),
(62, 1),
(62, 5),
(62, 6),
(62, 10),
(62, 11),
(62, 13),
(62, 15),
(63, 1),
(63, 5),
(63, 6),
(63, 10),
(63, 11),
(63, 13),
(63, 15),
(64, 1),
(64, 5),
(64, 6),
(64, 10),
(64, 11),
(64, 13),
(64, 15),
(65, 1),
(65, 5),
(65, 6),
(65, 10),
(65, 11),
(65, 13),
(65, 15),
(66, 1),
(66, 5),
(66, 6),
(66, 10),
(66, 11),
(66, 13),
(66, 15),
(67, 1),
(67, 5),
(67, 6),
(67, 10),
(67, 11),
(67, 13),
(67, 15),
(68, 1),
(68, 5),
(68, 6),
(68, 10),
(68, 11),
(68, 13),
(68, 15),
(69, 1),
(69, 6),
(69, 7),
(69, 10),
(69, 11),
(69, 13),
(69, 15),
(70, 1),
(70, 6),
(70, 7),
(70, 10),
(70, 11),
(70, 13),
(70, 15),
(71, 1),
(71, 6),
(71, 7),
(71, 10),
(71, 11),
(71, 13),
(71, 15),
(72, 1),
(72, 6),
(72, 7),
(72, 10),
(72, 11),
(72, 13),
(72, 15),
(73, 1),
(73, 6),
(73, 10),
(73, 11),
(73, 13),
(73, 15),
(74, 1),
(74, 6),
(74, 10),
(74, 11),
(74, 13),
(74, 15),
(75, 1),
(75, 6),
(75, 7),
(75, 10),
(75, 11),
(75, 13),
(75, 15),
(76, 1),
(76, 5),
(76, 11),
(76, 13),
(76, 15),
(77, 1),
(77, 5),
(77, 6),
(77, 11),
(77, 13),
(77, 15),
(78, 1),
(78, 5),
(78, 11),
(78, 13),
(78, 15),
(79, 1),
(79, 5),
(79, 11),
(79, 13),
(79, 15),
(80, 1),
(80, 5),
(80, 11),
(80, 13),
(80, 15),
(81, 1),
(81, 5),
(81, 11),
(81, 13),
(81, 15),
(82, 1),
(82, 5),
(82, 11),
(82, 13),
(82, 15),
(83, 1),
(83, 5),
(83, 7),
(83, 10),
(83, 11),
(83, 13),
(83, 15),
(84, 1),
(84, 5),
(84, 7),
(84, 10),
(84, 11),
(84, 13),
(84, 15),
(85, 1),
(85, 5),
(85, 10),
(85, 11),
(85, 13),
(85, 15),
(86, 1),
(86, 5),
(86, 10),
(86, 11),
(86, 13),
(86, 15),
(87, 1),
(87, 5),
(87, 10),
(87, 11),
(87, 13),
(87, 15),
(88, 1),
(88, 5),
(88, 10),
(88, 11),
(88, 13),
(88, 15),
(89, 1),
(89, 5),
(89, 10),
(89, 11),
(89, 13),
(89, 15),
(90, 1),
(90, 5),
(90, 6),
(90, 7),
(90, 10),
(90, 11),
(90, 13),
(90, 15),
(91, 1),
(91, 5),
(91, 6),
(91, 10),
(91, 11),
(91, 13),
(91, 15),
(92, 1),
(92, 5),
(92, 6),
(92, 10),
(92, 11),
(92, 13),
(92, 15),
(93, 1),
(93, 5),
(93, 6),
(93, 10),
(93, 11),
(93, 13),
(93, 15),
(94, 1),
(94, 5),
(94, 6),
(94, 10),
(94, 11),
(94, 13),
(94, 15),
(95, 1),
(95, 5),
(95, 6),
(95, 10),
(95, 11),
(95, 13),
(95, 15),
(96, 1),
(96, 5),
(96, 6),
(96, 10),
(96, 11),
(96, 13),
(96, 15),
(97, 1),
(97, 5),
(97, 11),
(97, 13),
(97, 15),
(98, 1),
(98, 5),
(98, 11),
(98, 13),
(98, 15),
(99, 1),
(99, 5),
(99, 11),
(99, 13),
(99, 15),
(100, 1),
(100, 5),
(100, 11),
(100, 13),
(100, 15),
(101, 1),
(101, 5),
(101, 11),
(101, 13),
(101, 15),
(102, 1),
(102, 5),
(102, 11),
(102, 13),
(102, 15),
(103, 1),
(103, 5),
(103, 11),
(103, 13),
(103, 15),
(104, 1),
(104, 5),
(104, 11),
(104, 13),
(104, 15),
(105, 1),
(105, 11),
(105, 12),
(105, 15),
(106, 1),
(106, 11),
(106, 12),
(106, 15),
(107, 1),
(107, 11),
(107, 12),
(107, 15),
(108, 1),
(108, 11),
(108, 12),
(108, 15),
(109, 1),
(109, 11),
(109, 12),
(109, 15),
(110, 1),
(110, 11),
(110, 12),
(110, 15),
(111, 1),
(111, 11),
(111, 12),
(111, 15),
(112, 1),
(112, 11),
(112, 12),
(112, 15),
(113, 1),
(113, 11),
(113, 12),
(113, 15),
(114, 1),
(114, 11),
(114, 12),
(114, 15),
(115, 1),
(115, 11),
(115, 12),
(115, 15),
(116, 1),
(116, 11),
(116, 12),
(116, 15),
(117, 1),
(117, 11),
(117, 12),
(117, 15),
(118, 1),
(118, 11),
(118, 12),
(118, 15),
(119, 1),
(119, 11),
(119, 12),
(119, 15),
(120, 1),
(120, 11),
(120, 12),
(120, 15),
(121, 1),
(121, 11),
(121, 12),
(121, 15),
(122, 1),
(122, 11),
(122, 12),
(122, 15),
(123, 1),
(123, 11),
(123, 12),
(123, 15),
(124, 1),
(124, 11),
(124, 12),
(124, 15),
(125, 1),
(125, 11),
(125, 12),
(125, 15),
(126, 1),
(126, 11),
(126, 12),
(126, 15),
(127, 1),
(127, 11),
(127, 12),
(127, 15),
(128, 1),
(128, 11),
(128, 12),
(128, 15),
(129, 1),
(129, 11),
(129, 12),
(129, 15),
(130, 1),
(130, 11),
(130, 12),
(130, 15),
(131, 1),
(131, 11),
(131, 12),
(131, 15),
(132, 1),
(132, 11),
(132, 12),
(132, 15),
(133, 1),
(133, 11),
(133, 12),
(133, 15),
(134, 1),
(134, 11),
(134, 12),
(134, 15),
(135, 1),
(135, 11),
(135, 12),
(135, 15),
(136, 1),
(136, 11),
(136, 12),
(136, 15),
(137, 1),
(137, 11),
(137, 12),
(137, 15),
(138, 1),
(138, 11),
(138, 12),
(138, 15),
(139, 1),
(139, 11),
(139, 12),
(139, 15),
(140, 1),
(140, 11),
(140, 12),
(140, 15),
(141, 1),
(141, 11),
(141, 12),
(141, 15),
(142, 1),
(142, 11),
(142, 12),
(142, 15),
(143, 1),
(143, 11),
(143, 12),
(143, 15),
(144, 1),
(144, 11),
(144, 15),
(145, 1),
(145, 11),
(145, 15),
(146, 1),
(146, 11),
(146, 15),
(147, 1),
(147, 11),
(147, 15),
(148, 1),
(148, 11),
(148, 15),
(149, 1),
(149, 11),
(149, 15),
(150, 1),
(150, 11),
(150, 15),
(151, 1),
(151, 11),
(151, 15),
(152, 1),
(152, 11),
(152, 15),
(153, 1),
(153, 11),
(153, 15),
(154, 1),
(154, 11),
(154, 15),
(155, 1),
(155, 11),
(155, 15),
(156, 1),
(156, 11),
(156, 15),
(157, 1),
(157, 11),
(157, 15),
(158, 1),
(158, 11),
(158, 15),
(159, 1),
(159, 11),
(159, 15),
(160, 1),
(160, 11),
(160, 15),
(161, 1),
(161, 11),
(161, 15),
(162, 1),
(162, 11),
(162, 15),
(163, 1),
(163, 11),
(163, 15),
(164, 1),
(164, 11),
(164, 15),
(165, 1),
(165, 11),
(165, 15),
(166, 1),
(166, 11),
(166, 15),
(167, 1),
(167, 11),
(167, 15),
(168, 1),
(168, 11),
(168, 15),
(169, 1),
(169, 11),
(169, 15),
(170, 1),
(170, 11),
(170, 15),
(171, 1),
(171, 11),
(171, 15),
(172, 1),
(172, 11),
(172, 15),
(173, 1),
(173, 11),
(173, 15),
(174, 1),
(174, 11),
(174, 15),
(175, 1),
(175, 11),
(175, 15),
(176, 1),
(176, 11),
(176, 15),
(177, 1),
(177, 11),
(177, 15),
(178, 1),
(178, 11),
(178, 12),
(178, 15),
(179, 1),
(179, 11),
(179, 12),
(179, 15),
(180, 1),
(180, 11),
(180, 12),
(180, 15),
(181, 1),
(181, 11),
(181, 12),
(181, 15),
(182, 1),
(182, 11),
(182, 12),
(182, 15),
(183, 1),
(183, 11),
(183, 12),
(183, 15),
(184, 1),
(184, 11),
(184, 12),
(184, 15),
(185, 1),
(185, 10),
(185, 11),
(185, 13),
(185, 15),
(186, 1),
(186, 10),
(186, 11),
(186, 13),
(186, 15),
(187, 1),
(187, 10),
(187, 11),
(187, 13),
(187, 15),
(188, 1),
(188, 10),
(188, 11),
(188, 13),
(188, 15),
(189, 1),
(189, 10),
(189, 11),
(189, 13),
(189, 15),
(190, 1),
(190, 10),
(190, 11),
(190, 13),
(190, 15),
(191, 1),
(191, 10),
(191, 11),
(191, 13),
(191, 15),
(192, 1),
(192, 10),
(192, 11),
(192, 13),
(192, 15),
(193, 1),
(193, 10),
(193, 11),
(193, 13),
(193, 15),
(194, 1),
(194, 10),
(194, 11),
(194, 13),
(194, 15),
(195, 1),
(195, 10),
(195, 11),
(195, 13),
(195, 15),
(196, 1),
(196, 11),
(196, 12),
(196, 13),
(196, 15),
(197, 1),
(197, 11),
(197, 12),
(197, 13),
(197, 15),
(198, 1),
(198, 11),
(198, 12),
(198, 13),
(198, 15),
(199, 1),
(199, 11),
(199, 12),
(199, 13),
(199, 15),
(200, 1),
(200, 11),
(200, 12),
(200, 13),
(200, 15),
(201, 1),
(201, 11),
(201, 12),
(201, 13),
(201, 15),
(202, 1),
(202, 11),
(202, 12),
(202, 13),
(202, 15),
(203, 1),
(203, 11),
(203, 12),
(203, 13),
(203, 15),
(204, 1),
(204, 11),
(204, 12),
(204, 13),
(204, 15),
(205, 1),
(205, 5),
(205, 11),
(205, 12),
(205, 13),
(205, 15),
(206, 1),
(206, 5),
(206, 11),
(206, 13),
(206, 15),
(207, 1),
(207, 5),
(207, 11),
(207, 13),
(207, 15),
(208, 1),
(208, 5),
(208, 11),
(208, 13),
(208, 15),
(209, 1),
(209, 5),
(209, 11),
(209, 13),
(209, 15),
(210, 1),
(210, 5),
(210, 11),
(210, 13),
(210, 15),
(211, 1),
(211, 5),
(211, 6),
(211, 7),
(211, 11),
(211, 13),
(211, 15),
(212, 1),
(212, 5),
(212, 6),
(212, 7),
(212, 11),
(212, 13),
(212, 15),
(213, 1),
(213, 5),
(213, 6),
(213, 7),
(213, 11),
(213, 13),
(213, 15),
(214, 1),
(214, 5),
(214, 6),
(214, 7),
(214, 11),
(214, 13),
(214, 15),
(215, 1),
(215, 5),
(215, 6),
(215, 11),
(215, 13),
(215, 15),
(216, 1),
(216, 5),
(216, 6),
(216, 11),
(216, 13),
(216, 15),
(217, 1),
(217, 5),
(217, 6),
(217, 11),
(217, 13),
(217, 15),
(218, 1),
(218, 5),
(218, 6),
(218, 7),
(218, 10),
(218, 11),
(218, 13),
(218, 15),
(219, 1),
(219, 5),
(219, 6),
(219, 7),
(219, 10),
(219, 11),
(219, 13),
(219, 15),
(220, 1),
(220, 5),
(220, 6),
(220, 7),
(220, 10),
(220, 11),
(220, 13),
(220, 15),
(221, 1),
(221, 5),
(221, 6),
(221, 7),
(221, 10),
(221, 11),
(221, 13),
(221, 15),
(222, 1),
(222, 5),
(222, 6),
(222, 11),
(222, 13),
(222, 15),
(223, 1),
(223, 5),
(223, 11),
(223, 13),
(223, 15),
(224, 1),
(224, 5),
(224, 11),
(224, 13),
(224, 15),
(225, 1),
(225, 5),
(225, 11),
(225, 13),
(225, 15),
(226, 1),
(226, 5),
(226, 6),
(226, 7),
(226, 10),
(226, 11),
(226, 13),
(226, 15),
(227, 1),
(227, 5),
(227, 6),
(227, 7),
(227, 10),
(227, 11),
(227, 13),
(227, 15),
(228, 1),
(228, 5),
(228, 6),
(228, 7),
(228, 10),
(228, 11),
(228, 13),
(228, 15),
(229, 1),
(229, 5),
(229, 6),
(229, 7),
(229, 10),
(229, 11),
(229, 13),
(229, 15),
(230, 1),
(230, 5),
(230, 6),
(230, 7),
(230, 11),
(230, 13),
(230, 15),
(231, 1),
(231, 5),
(231, 6),
(231, 7),
(231, 11),
(231, 13),
(231, 15),
(232, 1),
(232, 5),
(232, 6),
(232, 11),
(232, 13),
(232, 15),
(233, 1),
(233, 5),
(233, 6),
(233, 7),
(233, 10),
(233, 11),
(233, 13),
(233, 15),
(234, 1),
(234, 5),
(234, 6),
(234, 7),
(234, 10),
(234, 11),
(234, 13),
(234, 15),
(235, 1),
(235, 5),
(235, 6),
(235, 7),
(235, 10),
(235, 11),
(235, 13),
(235, 15),
(236, 1),
(236, 5),
(236, 6),
(236, 11),
(236, 13),
(236, 15),
(237, 1),
(237, 5),
(237, 6),
(237, 11),
(237, 13),
(237, 15),
(238, 1),
(238, 5),
(238, 6),
(238, 7),
(238, 10),
(238, 11),
(238, 13),
(238, 15),
(239, 1),
(239, 5),
(239, 6),
(239, 7),
(239, 10),
(239, 11),
(239, 13),
(239, 15),
(240, 1),
(240, 5),
(240, 6),
(240, 7),
(240, 10),
(240, 11),
(240, 13),
(240, 15),
(241, 1),
(241, 5),
(241, 6),
(241, 7),
(241, 10),
(241, 11),
(241, 13),
(241, 15),
(242, 1),
(242, 5),
(242, 6),
(242, 10),
(242, 11),
(242, 13),
(242, 15),
(243, 1),
(243, 5),
(243, 6),
(243, 10),
(243, 11),
(243, 13),
(243, 15),
(244, 1),
(244, 5),
(244, 6),
(244, 7),
(244, 10),
(244, 11),
(244, 13),
(244, 15),
(245, 1),
(245, 5),
(245, 6),
(245, 7),
(245, 10),
(245, 11),
(245, 13),
(245, 15),
(246, 1),
(246, 5),
(246, 6),
(246, 7),
(246, 10),
(246, 11),
(246, 13),
(246, 15),
(247, 1),
(247, 6),
(247, 7),
(247, 10),
(247, 11),
(247, 13),
(247, 15),
(248, 1),
(248, 6),
(248, 10),
(248, 11),
(248, 13),
(248, 15),
(249, 1),
(249, 6),
(249, 10),
(249, 11),
(249, 13),
(249, 15),
(250, 1),
(250, 6),
(250, 7),
(250, 10),
(250, 11),
(250, 13),
(250, 15),
(251, 1),
(251, 6),
(251, 7),
(251, 10),
(251, 11),
(251, 13),
(251, 15),
(252, 1),
(252, 6),
(252, 7),
(252, 10),
(252, 11),
(252, 13),
(252, 15),
(253, 1),
(253, 6),
(253, 10),
(253, 11),
(253, 13),
(253, 15),
(254, 1),
(254, 6),
(254, 10),
(254, 11),
(254, 13),
(254, 15),
(255, 1),
(255, 6),
(255, 10),
(255, 11),
(255, 13),
(255, 15),
(256, 1),
(256, 6),
(256, 10),
(256, 11),
(256, 13),
(256, 15),
(257, 1),
(257, 10),
(257, 11),
(257, 13),
(257, 15),
(258, 1),
(258, 11),
(258, 13),
(258, 15),
(259, 1),
(259, 11),
(259, 13),
(259, 15),
(260, 1),
(260, 11),
(260, 13),
(260, 15),
(261, 1),
(261, 6),
(261, 7),
(261, 10),
(261, 11),
(261, 13),
(261, 15),
(262, 1),
(262, 6),
(262, 7),
(262, 10),
(262, 11),
(262, 13),
(262, 15),
(263, 1),
(263, 6),
(263, 10),
(263, 11),
(263, 13),
(263, 15),
(264, 1),
(264, 6),
(264, 10),
(264, 11),
(264, 13),
(264, 15),
(265, 1),
(265, 6),
(265, 10),
(265, 11),
(265, 13),
(265, 15),
(266, 1),
(266, 6),
(266, 10),
(266, 11),
(266, 13),
(266, 15),
(267, 1),
(267, 6),
(267, 10),
(267, 11),
(267, 13),
(267, 15),
(268, 1),
(268, 6),
(268, 10),
(268, 11),
(268, 13),
(268, 15),
(269, 1),
(269, 6),
(269, 10),
(269, 11),
(269, 13),
(269, 15),
(270, 1),
(270, 6),
(270, 10),
(270, 11),
(270, 13),
(270, 15),
(271, 1),
(271, 6),
(271, 10),
(271, 11),
(271, 13),
(271, 15),
(272, 1),
(272, 6),
(272, 10),
(272, 11),
(272, 13),
(272, 15),
(273, 1),
(273, 6),
(273, 10),
(273, 11),
(273, 13),
(273, 15),
(274, 1),
(274, 6),
(274, 10),
(274, 11),
(274, 13),
(274, 15),
(275, 1),
(275, 6),
(275, 10),
(275, 11),
(275, 13),
(275, 15),
(276, 1),
(276, 6),
(276, 10),
(276, 11),
(276, 13),
(276, 15),
(277, 1),
(277, 6),
(277, 10),
(277, 11),
(277, 13),
(277, 15),
(278, 1),
(278, 6),
(278, 10),
(278, 11),
(278, 13),
(278, 15),
(279, 1),
(279, 6),
(279, 10),
(279, 11),
(279, 13),
(279, 15),
(280, 1),
(280, 6),
(280, 10),
(280, 11),
(280, 13),
(280, 15),
(281, 1),
(281, 6),
(281, 10),
(281, 11),
(281, 13),
(281, 15),
(282, 1),
(282, 6),
(282, 10),
(282, 11),
(282, 13),
(282, 15),
(283, 1),
(283, 6),
(283, 10),
(283, 11),
(283, 13),
(283, 15),
(284, 1),
(284, 6),
(284, 10),
(284, 11),
(284, 13),
(284, 15),
(285, 1),
(285, 6),
(285, 10),
(285, 11),
(285, 13),
(285, 15),
(286, 1),
(286, 6),
(286, 10),
(286, 11),
(286, 13),
(286, 15),
(287, 1),
(287, 6),
(287, 10),
(287, 11),
(287, 13),
(287, 15),
(288, 1),
(288, 6),
(288, 10),
(288, 11),
(288, 13),
(288, 15),
(289, 1),
(289, 6),
(289, 11),
(289, 13),
(289, 15),
(290, 1),
(290, 6),
(290, 11),
(290, 13),
(290, 15),
(291, 1),
(291, 6),
(291, 11),
(291, 13),
(291, 15),
(292, 1),
(292, 11),
(292, 13),
(292, 15),
(293, 1),
(293, 6),
(293, 11),
(293, 13),
(293, 15),
(294, 1),
(294, 6),
(294, 11),
(294, 13),
(294, 15),
(295, 1),
(295, 11),
(295, 12),
(295, 13),
(295, 15),
(296, 1),
(296, 6),
(296, 11),
(296, 13),
(296, 15),
(297, 1),
(297, 6),
(297, 11),
(297, 13),
(297, 15),
(298, 1),
(298, 6),
(298, 11),
(298, 13),
(298, 15),
(299, 1),
(299, 6),
(299, 11),
(299, 13),
(299, 15),
(300, 1),
(300, 6),
(300, 11),
(300, 13),
(300, 15),
(301, 1),
(301, 6),
(301, 10),
(301, 11),
(301, 13),
(301, 15),
(302, 1),
(302, 6),
(302, 10),
(302, 11),
(302, 13),
(302, 15),
(303, 1),
(303, 6),
(303, 7),
(303, 10),
(303, 11),
(303, 13),
(303, 15),
(304, 1),
(304, 6),
(304, 11),
(304, 13),
(304, 15),
(305, 1),
(305, 6),
(305, 10),
(305, 11),
(305, 12),
(305, 13),
(305, 15),
(306, 1),
(306, 6),
(306, 10),
(306, 11),
(306, 12),
(306, 13),
(306, 15),
(307, 1),
(307, 6),
(307, 10),
(307, 11),
(307, 12),
(307, 13),
(307, 15),
(308, 1),
(308, 6),
(308, 11),
(308, 12),
(308, 13),
(308, 15),
(309, 1),
(309, 6),
(309, 7),
(309, 10),
(309, 11),
(309, 12),
(309, 13),
(309, 15),
(310, 1),
(310, 6),
(310, 11),
(310, 12),
(310, 13),
(310, 15),
(311, 1),
(311, 6),
(311, 11),
(311, 12),
(311, 13),
(311, 15),
(312, 1),
(312, 6),
(312, 11),
(312, 12),
(312, 13),
(312, 15),
(313, 1),
(313, 6),
(313, 11),
(313, 12),
(313, 13),
(313, 15),
(314, 1),
(314, 6),
(314, 11),
(314, 12),
(314, 13),
(314, 15),
(315, 1),
(315, 6),
(315, 11),
(315, 12),
(315, 13),
(315, 15),
(316, 1),
(316, 6),
(316, 7),
(316, 10),
(316, 11),
(316, 12),
(316, 13),
(316, 15),
(317, 1),
(317, 6),
(317, 11),
(317, 12),
(317, 13),
(317, 15),
(318, 1),
(318, 6),
(318, 11),
(318, 12),
(318, 13),
(318, 15),
(319, 1),
(319, 6),
(319, 11),
(319, 13),
(319, 15),
(320, 1),
(320, 6),
(320, 11),
(320, 13),
(320, 15),
(321, 1),
(321, 6),
(321, 11),
(321, 13),
(321, 15),
(322, 1),
(322, 6),
(322, 11),
(322, 13),
(322, 15),
(323, 1),
(323, 6),
(323, 7),
(323, 11),
(323, 13),
(323, 15),
(324, 1),
(324, 6),
(324, 11),
(324, 13),
(324, 15),
(325, 1),
(325, 6),
(325, 11),
(325, 13),
(325, 15),
(326, 1),
(326, 6),
(326, 11),
(326, 13),
(326, 15),
(327, 1),
(327, 6),
(327, 11),
(327, 13),
(327, 15),
(328, 1),
(328, 6),
(328, 11),
(328, 13),
(328, 15),
(329, 1),
(329, 6),
(329, 11),
(329, 13),
(329, 15),
(330, 1),
(330, 6),
(330, 7),
(330, 10),
(330, 11),
(330, 13),
(330, 15),
(331, 1),
(331, 11),
(331, 13),
(331, 15),
(332, 1),
(332, 11),
(332, 13),
(332, 15),
(333, 1),
(333, 11),
(333, 13),
(333, 15),
(334, 1),
(334, 11),
(334, 13),
(334, 15),
(335, 1),
(335, 11),
(335, 13),
(335, 15),
(336, 1),
(336, 11),
(336, 13),
(336, 15),
(337, 1),
(337, 7),
(337, 10),
(337, 11),
(337, 13),
(337, 15),
(338, 1),
(338, 11),
(338, 13),
(338, 15),
(339, 1),
(339, 11),
(339, 13),
(339, 15),
(340, 1),
(340, 11),
(340, 13),
(340, 15),
(341, 1),
(341, 7),
(341, 11),
(341, 13),
(341, 15),
(342, 1),
(342, 11),
(342, 13),
(342, 15),
(343, 1),
(343, 11),
(343, 13),
(343, 15),
(344, 1),
(344, 7),
(344, 10),
(344, 11),
(344, 13),
(344, 15),
(345, 1),
(345, 11),
(345, 13),
(345, 15),
(346, 1),
(346, 11),
(346, 13),
(346, 15),
(347, 1),
(347, 11),
(347, 13),
(347, 15),
(348, 1),
(348, 11),
(348, 13),
(348, 15),
(349, 1),
(349, 11),
(349, 13),
(349, 15),
(350, 1),
(350, 11),
(350, 13),
(350, 15),
(351, 1),
(351, 7),
(351, 10),
(351, 11),
(351, 13),
(351, 15),
(352, 1),
(352, 7),
(352, 10),
(352, 11),
(352, 13),
(352, 15),
(353, 1),
(353, 6),
(353, 7),
(353, 10),
(353, 11),
(353, 13),
(353, 15),
(354, 1),
(354, 7),
(354, 10),
(354, 11),
(354, 13),
(354, 15),
(355, 1),
(355, 11),
(355, 13),
(355, 15),
(356, 1),
(356, 11),
(356, 13),
(356, 15),
(357, 1),
(357, 11),
(357, 13),
(357, 15),
(358, 1),
(358, 7),
(358, 10),
(358, 11),
(358, 13),
(358, 15),
(359, 1),
(359, 7),
(359, 11),
(359, 13),
(359, 15),
(360, 1),
(360, 7),
(360, 11),
(360, 13),
(360, 15),
(361, 1),
(361, 7),
(361, 11),
(361, 13),
(361, 15),
(362, 1),
(362, 11),
(362, 13),
(362, 15),
(363, 1),
(363, 11),
(363, 13),
(363, 15),
(364, 1),
(364, 11),
(364, 13),
(364, 15),
(365, 1),
(365, 7),
(365, 11),
(365, 13),
(365, 15),
(366, 1),
(366, 7),
(366, 11),
(366, 13),
(366, 15),
(367, 1),
(367, 7),
(367, 11),
(367, 13),
(367, 15),
(368, 1),
(368, 7),
(368, 11),
(368, 13),
(368, 15),
(369, 1),
(369, 11),
(369, 13),
(369, 15),
(370, 1),
(370, 11),
(370, 13),
(370, 15),
(371, 1),
(371, 7),
(371, 11),
(371, 13),
(371, 15),
(372, 1),
(372, 7),
(372, 10),
(372, 11),
(372, 13),
(372, 15),
(373, 1),
(373, 7),
(373, 10),
(373, 11),
(373, 13),
(373, 15),
(374, 1),
(374, 7),
(374, 10),
(374, 11),
(374, 13),
(374, 15),
(375, 1),
(375, 7),
(375, 11),
(375, 13),
(375, 15),
(376, 1),
(376, 7),
(376, 11),
(376, 13),
(376, 15),
(377, 1),
(377, 11),
(377, 13),
(377, 15),
(378, 1),
(378, 11),
(378, 13),
(378, 15),
(379, 1),
(379, 11),
(379, 13),
(379, 15),
(380, 1),
(380, 7),
(380, 11),
(380, 13),
(380, 15),
(381, 1),
(381, 7),
(381, 11),
(381, 13),
(381, 15),
(382, 1),
(382, 7),
(382, 11),
(382, 13),
(382, 15),
(383, 1),
(383, 7),
(383, 11),
(383, 13),
(383, 15),
(384, 1),
(384, 11),
(384, 13),
(384, 15),
(385, 1),
(385, 11),
(385, 13),
(385, 15),
(386, 1),
(386, 11),
(386, 13),
(386, 15),
(387, 1),
(387, 11),
(387, 13),
(387, 15),
(388, 1),
(388, 11),
(388, 13),
(388, 15),
(389, 1),
(389, 11),
(389, 13),
(389, 15),
(390, 1),
(390, 11),
(390, 13),
(390, 15),
(391, 1),
(391, 11),
(391, 13),
(391, 15),
(392, 1),
(392, 7),
(392, 11),
(392, 13),
(392, 15),
(393, 1),
(393, 7),
(393, 11),
(393, 13),
(393, 15),
(394, 1),
(394, 7),
(394, 11),
(394, 13),
(394, 15),
(395, 1),
(395, 7),
(395, 11),
(395, 13),
(395, 15),
(396, 1),
(396, 7),
(396, 11),
(396, 13),
(396, 15),
(397, 1),
(397, 11),
(397, 13),
(397, 15),
(398, 1),
(398, 11),
(398, 13),
(398, 15),
(399, 1),
(399, 11),
(399, 13),
(399, 15),
(400, 1),
(400, 11),
(400, 13),
(400, 15),
(401, 1),
(401, 6),
(401, 11),
(401, 13),
(401, 15),
(402, 1),
(402, 6),
(402, 11),
(402, 13),
(402, 15),
(403, 1),
(403, 6),
(403, 10),
(403, 11),
(403, 13),
(403, 15),
(404, 1),
(404, 6),
(404, 10),
(404, 11),
(404, 13),
(404, 15),
(405, 1),
(405, 6),
(405, 10),
(405, 11),
(405, 13),
(405, 15),
(406, 1),
(406, 6),
(406, 10),
(406, 11),
(406, 13),
(406, 15),
(407, 1),
(407, 6),
(407, 7),
(407, 10),
(407, 11),
(407, 13),
(407, 15),
(408, 1),
(408, 6),
(408, 7),
(408, 10),
(408, 11),
(408, 13),
(408, 15),
(409, 1),
(409, 6),
(409, 7),
(409, 10),
(409, 11),
(409, 13),
(409, 15),
(410, 1),
(410, 6),
(410, 7),
(410, 10),
(410, 11),
(410, 13),
(410, 15),
(411, 1),
(411, 6),
(411, 11),
(411, 13),
(411, 15),
(412, 1),
(412, 6),
(412, 11),
(412, 13),
(412, 15),
(413, 1),
(413, 6),
(413, 10),
(413, 11),
(413, 13),
(413, 15),
(414, 1),
(414, 6),
(414, 7),
(414, 10),
(414, 11),
(414, 13),
(414, 15),
(415, 1),
(415, 6),
(415, 11),
(415, 13),
(415, 15),
(416, 1),
(416, 6),
(416, 11),
(416, 13),
(416, 15),
(417, 1),
(417, 6),
(417, 7),
(417, 10),
(417, 11),
(417, 13),
(417, 15),
(418, 1),
(418, 6),
(418, 7),
(418, 10),
(418, 11),
(418, 13),
(418, 15),
(419, 1),
(419, 6),
(419, 7),
(419, 10),
(419, 11),
(419, 13),
(419, 15),
(420, 1),
(420, 6),
(420, 10),
(420, 11),
(420, 13),
(420, 15),
(421, 1),
(421, 6),
(421, 10),
(421, 11),
(421, 13),
(421, 15),
(422, 1),
(422, 6),
(422, 10),
(422, 11),
(422, 13),
(422, 15),
(423, 1),
(423, 6),
(423, 7),
(423, 10),
(423, 11),
(423, 13),
(423, 15),
(424, 1),
(424, 6),
(424, 7),
(424, 10),
(424, 11),
(424, 13),
(424, 15),
(425, 1),
(425, 6),
(425, 7),
(425, 10),
(425, 11),
(425, 13),
(425, 15),
(426, 1),
(426, 6),
(426, 10),
(426, 11),
(426, 13),
(426, 15),
(427, 1),
(427, 6),
(427, 10),
(427, 11),
(427, 13),
(427, 15),
(428, 1),
(428, 6),
(428, 10),
(428, 11),
(428, 13),
(428, 15),
(429, 1),
(429, 6),
(429, 7),
(429, 10),
(429, 11),
(429, 13),
(429, 15),
(430, 1),
(430, 6),
(430, 10),
(430, 11),
(430, 13),
(430, 15),
(431, 1),
(431, 6),
(431, 10),
(431, 11),
(431, 13),
(431, 15),
(432, 1),
(432, 6),
(432, 10),
(432, 11),
(432, 13),
(432, 15),
(433, 1),
(433, 6),
(433, 10),
(433, 11),
(433, 13),
(433, 15),
(434, 1),
(434, 6),
(434, 10),
(434, 11),
(434, 13),
(434, 15),
(435, 1),
(435, 6),
(435, 10),
(435, 11),
(435, 13),
(435, 15),
(436, 1),
(436, 6),
(436, 10),
(436, 11),
(436, 13),
(436, 15),
(437, 1),
(437, 6),
(437, 10),
(437, 11),
(437, 13),
(437, 15),
(438, 1),
(438, 6),
(438, 10),
(438, 11),
(438, 13),
(438, 15),
(439, 1),
(439, 6),
(439, 10),
(439, 11),
(439, 13),
(439, 15),
(440, 1),
(440, 6),
(440, 10),
(440, 11),
(440, 13),
(440, 15),
(441, 1),
(441, 6),
(441, 10),
(441, 11),
(441, 13),
(441, 15),
(442, 1),
(442, 6),
(442, 10),
(442, 11),
(442, 13),
(442, 15),
(443, 1),
(443, 6),
(443, 10),
(443, 11),
(443, 13),
(443, 15),
(444, 1),
(444, 6),
(444, 10),
(444, 11),
(444, 13),
(444, 15),
(445, 1),
(445, 6),
(445, 10),
(445, 11),
(445, 13),
(445, 15),
(446, 6),
(446, 10),
(446, 11),
(446, 13),
(446, 15),
(447, 6),
(447, 10),
(447, 11),
(447, 13),
(447, 15),
(448, 6),
(448, 10),
(448, 11),
(448, 13),
(448, 15),
(449, 6),
(449, 10),
(449, 11),
(449, 13),
(449, 15),
(450, 6),
(450, 10),
(450, 11),
(450, 13),
(450, 15),
(451, 6),
(451, 7),
(451, 10),
(451, 11),
(451, 13),
(451, 15),
(452, 6),
(452, 7),
(452, 10),
(452, 11),
(452, 13),
(452, 15),
(453, 6),
(453, 7),
(453, 10),
(453, 11),
(453, 13),
(453, 15),
(454, 6),
(454, 7),
(454, 10),
(454, 11),
(454, 13),
(454, 15),
(455, 6),
(455, 7),
(455, 10),
(455, 11),
(455, 13),
(455, 15),
(456, 6),
(456, 7),
(456, 10),
(456, 11),
(456, 13),
(456, 15),
(457, 6),
(457, 7),
(457, 10),
(457, 11),
(457, 13),
(457, 15),
(458, 6),
(458, 7),
(458, 10),
(458, 11),
(458, 13),
(458, 15),
(459, 6),
(459, 7),
(459, 10),
(459, 11),
(459, 13),
(459, 15),
(460, 6),
(460, 7),
(460, 10),
(460, 11),
(460, 13),
(460, 15),
(461, 6),
(461, 7),
(461, 10),
(461, 11),
(461, 13),
(461, 15),
(462, 6),
(462, 7),
(462, 10),
(462, 11),
(462, 13),
(462, 15),
(463, 6),
(463, 7),
(463, 10),
(463, 11),
(463, 13),
(463, 15),
(464, 6),
(464, 7),
(464, 10),
(464, 11),
(464, 13),
(464, 15),
(465, 6),
(465, 7),
(465, 10),
(465, 11),
(465, 13),
(465, 15),
(466, 6),
(466, 7),
(466, 10),
(466, 11),
(466, 13),
(466, 15),
(467, 6),
(467, 10),
(467, 11),
(467, 13),
(467, 15),
(468, 6),
(468, 10),
(468, 11),
(468, 13),
(468, 15),
(469, 6),
(469, 10),
(469, 11),
(469, 13),
(469, 15),
(470, 6),
(470, 10),
(470, 11),
(470, 13),
(470, 15),
(471, 6),
(471, 10),
(471, 11),
(471, 13),
(471, 15),
(472, 6),
(472, 10),
(472, 11),
(472, 13),
(472, 15),
(473, 6),
(473, 10),
(473, 11),
(473, 13),
(473, 15),
(474, 6),
(474, 10),
(474, 11),
(474, 13),
(474, 15),
(475, 6),
(475, 10),
(475, 11),
(475, 13),
(475, 15),
(476, 6),
(476, 10),
(476, 11),
(476, 13),
(476, 15),
(477, 6),
(477, 10),
(477, 11),
(477, 13),
(477, 15),
(478, 6),
(478, 11),
(478, 13),
(478, 15),
(479, 6),
(479, 7),
(479, 11),
(479, 13),
(479, 15),
(480, 6),
(480, 7),
(480, 10),
(480, 11),
(480, 13),
(480, 15),
(481, 6),
(481, 10),
(481, 11),
(481, 13),
(481, 15),
(482, 6),
(482, 10),
(482, 11),
(482, 13),
(482, 15),
(483, 6),
(483, 10),
(483, 11),
(483, 13),
(483, 15),
(484, 6),
(484, 10),
(484, 11),
(484, 13),
(484, 15),
(485, 6),
(485, 10),
(485, 11),
(485, 13),
(485, 15),
(486, 10),
(486, 11),
(486, 13),
(486, 15),
(487, 6),
(487, 11),
(487, 13),
(487, 15),
(488, 6),
(488, 11),
(488, 13),
(488, 15),
(489, 6),
(489, 11),
(489, 13),
(489, 15),
(490, 6),
(490, 11),
(490, 13),
(490, 15),
(491, 6),
(491, 11),
(491, 13),
(491, 15),
(492, 6),
(492, 11),
(492, 13),
(492, 15),
(493, 6),
(493, 11),
(493, 13),
(493, 15),
(494, 7),
(494, 10),
(494, 11),
(494, 13),
(494, 15),
(495, 7),
(495, 10),
(495, 11),
(495, 13),
(495, 15),
(496, 7),
(496, 10),
(496, 11),
(496, 13),
(496, 15),
(497, 7),
(497, 10),
(497, 11),
(497, 13),
(497, 15),
(498, 7),
(498, 10),
(498, 11),
(498, 13),
(498, 15),
(499, 7),
(499, 10),
(499, 11),
(499, 13),
(499, 15),
(500, 7),
(500, 10),
(500, 11),
(500, 13),
(500, 15),
(501, 7),
(501, 10),
(501, 11),
(501, 13),
(501, 15),
(502, 7),
(502, 10),
(502, 11),
(502, 13),
(502, 15),
(503, 7),
(503, 10),
(503, 11),
(503, 13),
(503, 15),
(504, 7),
(504, 10),
(504, 11),
(504, 13),
(504, 15),
(505, 7),
(505, 10),
(505, 11),
(505, 13),
(505, 15),
(506, 6),
(506, 7),
(506, 10),
(506, 11),
(506, 13),
(506, 15),
(507, 6),
(507, 7),
(507, 10),
(507, 11),
(507, 13),
(507, 15),
(508, 6),
(508, 7),
(508, 10),
(508, 11),
(508, 13),
(508, 15),
(509, 6),
(509, 7),
(509, 10),
(509, 11),
(509, 13),
(509, 15),
(510, 6),
(510, 7),
(510, 10),
(510, 11),
(510, 13),
(510, 15),
(511, 6),
(511, 10),
(511, 11),
(511, 13),
(511, 15),
(512, 6),
(512, 10),
(512, 11),
(512, 13),
(512, 15),
(513, 6),
(513, 10),
(513, 11),
(513, 13),
(513, 15),
(514, 6),
(514, 10),
(514, 11),
(514, 13),
(514, 15),
(515, 6),
(515, 10),
(515, 11),
(515, 13),
(515, 15),
(516, 6),
(516, 10),
(516, 11),
(516, 13),
(516, 15),
(517, 6),
(517, 10),
(517, 11),
(517, 13),
(517, 15),
(518, 6),
(518, 10),
(518, 11),
(518, 13),
(518, 15),
(519, 6),
(519, 10),
(519, 11),
(519, 13),
(519, 15),
(520, 6),
(520, 10),
(520, 11),
(520, 13),
(520, 15),
(521, 6),
(521, 10),
(521, 11),
(521, 13),
(521, 15),
(522, 6),
(522, 10),
(522, 11),
(522, 13),
(522, 15),
(523, 6),
(523, 10),
(523, 11),
(523, 13),
(523, 15),
(524, 6),
(524, 10),
(524, 11),
(524, 13),
(524, 15),
(525, 6),
(525, 10),
(525, 11),
(525, 13),
(525, 15),
(526, 10),
(526, 11),
(526, 13),
(526, 15),
(527, 10),
(527, 11),
(527, 13),
(527, 15),
(528, 10),
(528, 11),
(528, 13),
(528, 15),
(529, 10),
(529, 11),
(529, 13),
(529, 15),
(530, 10),
(530, 11),
(530, 13),
(530, 15),
(531, 6),
(531, 10),
(531, 11),
(531, 13),
(531, 15),
(532, 6),
(532, 10),
(532, 11),
(532, 13),
(532, 15),
(533, 6),
(533, 10),
(533, 11),
(533, 13),
(533, 15),
(534, 6),
(534, 10),
(534, 11),
(534, 13),
(534, 15),
(535, 6),
(535, 10),
(535, 11),
(535, 13),
(535, 15),
(536, 6),
(536, 10),
(536, 11),
(536, 13),
(536, 15),
(537, 6),
(537, 10),
(537, 11),
(537, 13),
(537, 15),
(538, 6),
(538, 10),
(538, 11),
(538, 13),
(538, 15),
(539, 6),
(539, 10),
(539, 11),
(539, 13),
(539, 15),
(540, 6),
(540, 10),
(540, 11),
(540, 13),
(540, 15),
(541, 6),
(541, 10),
(541, 11),
(541, 13),
(541, 15),
(542, 6),
(542, 10),
(542, 11),
(542, 13),
(542, 15),
(543, 11),
(543, 13),
(543, 15),
(544, 11),
(544, 13),
(544, 15),
(545, 11),
(545, 13),
(545, 15),
(546, 11),
(546, 13),
(546, 15),
(547, 11),
(547, 13),
(547, 15),
(548, 11),
(548, 13),
(548, 15),
(549, 11),
(549, 13),
(549, 15),
(550, 11),
(550, 13),
(550, 15),
(551, 11),
(551, 13),
(551, 15),
(552, 11),
(552, 13),
(552, 15),
(553, 11),
(553, 13),
(553, 15),
(554, 11),
(554, 13),
(554, 15),
(555, 11),
(555, 13),
(555, 15),
(556, 11),
(556, 12),
(556, 13),
(556, 15),
(557, 11),
(557, 13),
(557, 15),
(558, 11),
(558, 13),
(558, 15),
(559, 11),
(559, 13),
(559, 15),
(560, 11),
(560, 13),
(560, 15),
(561, 11),
(561, 13),
(561, 15),
(562, 11),
(562, 15),
(563, 11),
(563, 15),
(564, 6),
(564, 7),
(564, 10),
(564, 11),
(564, 15),
(565, 11),
(565, 15),
(566, 11),
(566, 15),
(567, 11),
(567, 15),
(568, 11),
(568, 15),
(569, 11),
(569, 15),
(570, 11),
(570, 15),
(571, 11),
(571, 15),
(572, 11),
(572, 15),
(573, 11),
(573, 15),
(574, 11),
(574, 15),
(575, 11),
(575, 15),
(576, 11),
(576, 15),
(577, 11),
(577, 15),
(578, 11),
(578, 15),
(579, 6),
(579, 7),
(579, 11),
(580, 6),
(580, 7),
(580, 11),
(581, 6),
(581, 7),
(581, 11),
(582, 6),
(582, 7),
(582, 11),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 2),
(50, 2),
(51, 2),
(52, 2),
(53, 2),
(54, 2),
(55, 2),
(56, 2),
(57, 2),
(58, 2),
(59, 2),
(60, 2),
(61, 2),
(62, 2),
(63, 2),
(64, 2),
(65, 2),
(66, 2),
(67, 2),
(68, 2),
(69, 2),
(70, 2),
(71, 2),
(72, 2),
(73, 2),
(74, 2),
(75, 2),
(76, 2),
(77, 2),
(78, 2),
(79, 2),
(80, 2),
(81, 2),
(82, 2),
(83, 2),
(84, 2),
(85, 2),
(86, 2),
(87, 2),
(88, 2),
(89, 2),
(90, 2),
(91, 2),
(92, 2),
(93, 2),
(94, 2),
(95, 2),
(96, 2),
(97, 2),
(98, 2),
(99, 2),
(100, 2),
(101, 2),
(102, 2),
(103, 2),
(104, 2),
(105, 2),
(145, 2),
(146, 2),
(147, 2),
(148, 2),
(149, 2),
(150, 2),
(151, 2),
(152, 2),
(153, 2),
(154, 2),
(155, 2),
(156, 2),
(157, 2),
(158, 2),
(159, 2),
(160, 2),
(161, 2),
(162, 2),
(163, 2),
(164, 2),
(165, 2),
(166, 2),
(167, 2),
(168, 2),
(169, 2),
(170, 2),
(171, 2),
(172, 2),
(173, 2),
(174, 2),
(175, 2),
(176, 2),
(177, 2),
(178, 2),
(179, 2),
(180, 2),
(181, 2),
(182, 2),
(183, 2),
(184, 2),
(185, 2),
(186, 2),
(187, 2),
(188, 2),
(189, 2),
(190, 2),
(191, 2),
(192, 2),
(193, 2),
(194, 2),
(195, 2),
(196, 2),
(197, 2),
(198, 2),
(199, 2),
(200, 2),
(201, 2),
(202, 2),
(203, 2),
(204, 2),
(205, 2),
(206, 2),
(207, 2),
(208, 2),
(209, 2),
(210, 2),
(211, 2),
(212, 2),
(213, 2),
(214, 2),
(215, 2),
(216, 2),
(217, 2),
(218, 2),
(219, 2),
(220, 2),
(221, 2),
(222, 2),
(223, 2),
(224, 2),
(225, 2),
(226, 2),
(227, 2),
(228, 2),
(229, 2),
(230, 2),
(231, 2),
(232, 2),
(233, 2),
(234, 2),
(235, 2),
(236, 2),
(237, 2),
(238, 2),
(239, 2),
(240, 2),
(241, 2),
(242, 2),
(243, 2),
(244, 2),
(245, 2),
(246, 2),
(247, 2),
(248, 2),
(249, 2),
(250, 2),
(251, 2),
(252, 2),
(253, 2),
(254, 2),
(255, 2),
(256, 2),
(257, 2),
(258, 2),
(259, 2),
(260, 2),
(261, 2),
(262, 2),
(263, 2),
(264, 2),
(265, 2),
(266, 2),
(267, 2),
(268, 2),
(269, 2),
(270, 2),
(271, 2),
(272, 2),
(273, 2),
(274, 2),
(275, 2),
(276, 2),
(277, 2),
(278, 2),
(279, 2),
(280, 2),
(476, 2),
(281, 2),
(282, 2),
(283, 2),
(284, 2),
(285, 2),
(286, 2),
(485, 2),
(287, 2),
(288, 2),
(289, 2),
(290, 2),
(291, 2),
(292, 2),
(293, 2),
(294, 2),
(295, 2),
(296, 2),
(297, 2),
(298, 2),
(299, 2),
(300, 2),
(301, 2),
(302, 2),
(303, 2),
(304, 2),
(305, 2),
(306, 2),
(307, 2),
(308, 2),
(309, 2),
(310, 2),
(311, 2),
(312, 2),
(313, 2),
(314, 2),
(315, 2),
(316, 2),
(317, 2),
(318, 2),
(319, 2),
(320, 2),
(321, 2),
(322, 2),
(323, 2),
(324, 2),
(325, 2),
(326, 2),
(327, 2),
(328, 2),
(329, 2),
(330, 2),
(331, 2),
(332, 2),
(333, 2),
(334, 2),
(335, 2),
(336, 2),
(337, 2),
(338, 2),
(339, 2),
(340, 2),
(341, 2),
(342, 2),
(343, 2),
(344, 2),
(345, 2),
(346, 2),
(347, 2),
(348, 2),
(349, 2),
(350, 2),
(351, 2),
(352, 2),
(353, 2),
(354, 2),
(355, 2),
(356, 2),
(357, 2),
(358, 2),
(359, 2),
(360, 2),
(361, 2),
(362, 2),
(363, 2),
(364, 2),
(365, 2),
(366, 2),
(367, 2),
(368, 2),
(369, 2),
(370, 2),
(371, 2),
(372, 2),
(373, 2),
(374, 2),
(375, 2),
(376, 2),
(377, 2),
(378, 2),
(379, 2),
(380, 2),
(381, 2),
(382, 2),
(383, 2),
(384, 2),
(385, 2),
(386, 2),
(387, 2),
(388, 2),
(389, 2),
(390, 2),
(391, 2),
(392, 2),
(393, 2),
(394, 2),
(395, 2),
(396, 2),
(397, 2),
(398, 2),
(399, 2),
(400, 2),
(401, 2),
(402, 2),
(403, 2),
(404, 2),
(405, 2),
(406, 2),
(407, 2),
(408, 2),
(409, 2),
(410, 2),
(411, 2),
(412, 2),
(413, 2),
(414, 2),
(415, 2),
(416, 2),
(417, 2),
(418, 2),
(419, 2),
(420, 2),
(421, 2),
(422, 2),
(423, 2),
(424, 2),
(425, 2),
(426, 2),
(427, 2),
(428, 2),
(429, 2),
(430, 2),
(431, 2),
(432, 2),
(433, 2),
(434, 2),
(435, 2),
(436, 2),
(437, 2),
(438, 2),
(439, 2),
(440, 2),
(441, 2),
(442, 2),
(443, 2),
(444, 2),
(445, 2),
(446, 2),
(447, 2),
(448, 2),
(449, 2),
(450, 2),
(451, 2),
(452, 2),
(453, 2),
(454, 2),
(455, 2),
(456, 2),
(457, 2),
(458, 2),
(459, 2),
(460, 2),
(461, 2),
(462, 2),
(463, 2),
(464, 2),
(465, 2),
(466, 2),
(467, 2),
(468, 2),
(469, 2),
(470, 2),
(471, 2),
(472, 2),
(473, 2),
(474, 2),
(475, 2),
(477, 2),
(478, 2),
(479, 2),
(480, 2),
(481, 2),
(482, 2),
(483, 2),
(484, 2),
(486, 2),
(487, 2),
(488, 2),
(489, 2),
(490, 2),
(491, 2),
(492, 2),
(493, 2),
(494, 2),
(495, 2),
(496, 2),
(497, 2),
(498, 2),
(499, 2),
(500, 2),
(501, 2),
(502, 2),
(503, 2),
(504, 2),
(505, 2),
(506, 2),
(507, 2),
(508, 2),
(509, 2),
(510, 2),
(511, 2),
(512, 2),
(513, 2),
(514, 2),
(515, 2),
(516, 2),
(517, 2),
(518, 2),
(519, 2),
(520, 2),
(521, 2),
(522, 2),
(523, 2),
(524, 2),
(525, 2),
(526, 2),
(527, 2),
(528, 2),
(529, 2),
(530, 2),
(531, 2),
(532, 2),
(533, 2),
(534, 2),
(535, 2),
(536, 2),
(537, 2),
(538, 2),
(539, 2),
(540, 2),
(541, 2),
(542, 2),
(543, 2),
(544, 2),
(545, 2),
(546, 2),
(547, 2),
(548, 2),
(549, 2),
(550, 2),
(551, 2),
(552, 2),
(553, 2),
(554, 2),
(555, 2),
(556, 2),
(557, 2),
(558, 2),
(559, 2),
(560, 2),
(561, 2),
(565, 2),
(566, 2),
(567, 2),
(568, 2),
(569, 2),
(570, 2),
(571, 2),
(572, 2),
(573, 2),
(574, 2),
(575, 2),
(576, 2),
(577, 2),
(578, 2),
(5, 3),
(6, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(45, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(50, 3),
(51, 3),
(52, 3),
(53, 3),
(54, 3),
(99, 3),
(106, 3),
(107, 3),
(108, 3),
(109, 3),
(110, 3),
(111, 3),
(112, 3),
(113, 3),
(114, 3),
(115, 3),
(116, 3),
(117, 3),
(118, 3),
(119, 3),
(120, 3),
(121, 3),
(122, 3),
(123, 3),
(124, 3),
(125, 3),
(126, 3),
(127, 3),
(128, 3),
(129, 3),
(130, 3),
(131, 3),
(132, 3),
(133, 3),
(134, 3),
(135, 3),
(136, 3),
(137, 3),
(138, 3),
(139, 3),
(140, 3),
(141, 3),
(142, 3),
(143, 3),
(144, 3),
(179, 3),
(180, 3),
(181, 3),
(182, 3),
(183, 3),
(184, 3),
(185, 3),
(197, 3),
(198, 3),
(199, 3),
(200, 3),
(201, 3),
(202, 3),
(203, 3),
(204, 3),
(205, 3),
(294, 3),
(295, 3),
(302, 3),
(303, 3),
(304, 3),
(305, 3),
(306, 3),
(307, 3),
(309, 3),
(310, 3),
(311, 3),
(312, 3),
(313, 3),
(314, 3),
(315, 3),
(316, 3),
(317, 3),
(318, 3),
(319, 3),
(320, 3),
(321, 3),
(322, 3),
(323, 3),
(324, 3),
(325, 3),
(398, 3),
(399, 3),
(400, 3),
(401, 3),
(402, 3),
(403, 3),
(404, 3);

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `years` int(11) NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `name`, `description`, `years`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'EYP', '1-3', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(2, 'PYP', '1-6', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(3, 'IGCSE', '6-8', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(4, 'O LEVEL', '9-10', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(5, 'A LEVEL', '11-12', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(6, 'MONTESSORI', '1-3', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(7, 'PRIMARY PROGRAMS', '1-5', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(8, 'MIDDLE  PROGRAMS', '1-8', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(9, 'MATRIC', '9-10', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51'),
(10, 'INTERMEDIATE', '11-12', 0, 'Active', NULL, NULL, NULL, NULL, '2021-02-26 07:13:51', '2021-02-26 07:13:51');

-- --------------------------------------------------------

--
-- Table structure for table `program_terms`
--

CREATE TABLE `program_terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resigns`
--

CREATE TABLE `resigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_period` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resigns`
--

INSERT INTO `resigns` (`id`, `user_id`, `reason`, `valid`, `notice_period`, `created_at`, `updated_at`) VALUES
(1, 12, 'I want to resign !', '02/24/2021', '02/24/2021 | 03/24/2021 | ', '2021-02-24 06:11:57', '2021-02-24 06:11:57'),
(2, 17, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '02/25/2021', '02/25/2021 | 03/25/2021 | ', '2021-02-25 09:34:40', '2021-02-25 09:34:40'),
(3, 77, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '02/25/2021', '02/18/2021 | 03/01/2021 | ', '2021-03-01 10:09:07', '2021-03-01 10:09:07');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'Super Admin', 'Super Admin', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(2, 'admin', 'Administrator', 'Administrator', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(3, 'admission manager', 'Admission Manager', 'Admission Manager', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(4, 'account', 'Accountant', 'Accountant', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(5, 'library', 'Librarian', 'Librarian', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(6, 'sr.coordinator', 'Sr.Coordinator', 'Sr.Coordinator', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(7, 'jr.coordinator', 'Jr.Coordinator', 'Jr.Coordinator', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(8, 'student', 'Student', 'Student', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(9, 'guardian', 'Guardian', 'Guardian', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(10, 'teacher', 'Teacher', 'Teacher', '2020-07-16 15:01:15', '2020-07-16 15:01:15'),
(11, 'Principal', 'Principal', 'Principal', '2020-07-16 16:17:30', '2020-07-16 16:17:30'),
(12, 'fee manager', 'Fee Manager', 'Fee Manager', '2020-07-16 16:17:30', '2020-07-16 16:17:30'),
(13, 'quality assurance', 'Quality Assurance', 'Quality Assurance', '2020-07-16 16:17:30', '2020-09-15 04:16:09'),
(15, 'ceo', 'CEO', 'CEO', '2020-09-16 04:53:43', '2020-09-16 04:53:43');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 6, NULL, NULL),
(3, 3, 10, NULL, NULL),
(4, 4, 11, NULL, NULL),
(5, 5, 7, NULL, NULL),
(6, 6, 15, NULL, NULL),
(7, 7, 8, NULL, NULL),
(8, 8, 8, NULL, NULL),
(9, 9, 8, NULL, NULL),
(10, 10, 8, NULL, NULL),
(11, 11, 9, NULL, NULL),
(12, 14, 13, NULL, NULL),
(13, 16, 10, NULL, NULL),
(14, 17, 8, NULL, NULL),
(15, 18, 8, NULL, NULL),
(16, 19, 8, NULL, NULL),
(17, 20, 8, NULL, NULL),
(18, 22, 10, NULL, NULL),
(19, 23, 8, NULL, NULL),
(20, 24, 10, NULL, NULL),
(21, 25, 10, NULL, NULL),
(22, 26, 10, NULL, NULL),
(23, 27, 10, NULL, NULL),
(24, 28, 10, NULL, NULL),
(25, 29, 10, NULL, NULL),
(26, 30, 10, NULL, NULL),
(27, 31, 10, NULL, NULL),
(28, 32, 10, NULL, NULL),
(29, 33, 10, NULL, NULL),
(30, 34, 10, NULL, NULL),
(31, 35, 10, NULL, NULL),
(32, 36, 10, NULL, NULL),
(33, 37, 10, NULL, NULL),
(34, 38, 10, NULL, NULL),
(35, 39, 10, NULL, NULL),
(36, 40, 10, NULL, NULL),
(37, 41, 10, NULL, NULL),
(38, 42, 10, NULL, NULL),
(39, 43, 10, NULL, NULL),
(40, 44, 10, NULL, NULL),
(41, 45, 10, NULL, NULL),
(42, 46, 10, NULL, NULL),
(43, 48, 8, NULL, NULL),
(44, 49, 8, NULL, NULL),
(45, 50, 8, NULL, NULL),
(46, 51, 8, NULL, NULL),
(47, 52, 8, NULL, NULL),
(48, 53, 8, NULL, NULL),
(49, 54, 8, NULL, NULL),
(50, 55, 8, NULL, NULL),
(51, 56, 8, NULL, NULL),
(52, 57, 8, NULL, NULL),
(53, 58, 8, NULL, NULL),
(54, 59, 8, NULL, NULL),
(55, 60, 8, NULL, NULL),
(56, 61, 8, NULL, NULL),
(57, 62, 8, NULL, NULL),
(58, 63, 8, NULL, NULL),
(59, 64, 9, NULL, NULL),
(60, 65, 5, NULL, NULL),
(61, 66, 8, NULL, NULL),
(62, 67, 3, NULL, NULL),
(63, 68, 3, NULL, NULL),
(64, 69, 12, NULL, NULL),
(65, 70, 12, NULL, NULL),
(66, 71, 8, NULL, NULL),
(67, 72, 11, NULL, NULL),
(68, 73, 9, NULL, NULL),
(69, 74, 8, NULL, NULL),
(70, 75, 11, NULL, NULL),
(71, 76, 8, NULL, NULL),
(72, 77, 9, NULL, NULL),
(73, 78, 8, NULL, NULL),
(74, 79, 9, NULL, NULL),
(75, 80, 8, NULL, NULL),
(76, 22, 8, NULL, NULL),
(77, 24, 8, NULL, NULL),
(78, 25, 10, NULL, NULL),
(79, 26, 8, NULL, NULL),
(80, 27, 10, NULL, NULL),
(81, 28, 8, NULL, NULL),
(82, 29, 10, NULL, NULL),
(83, 30, 8, NULL, NULL),
(84, 31, 10, NULL, NULL),
(85, 33, 8, NULL, NULL),
(86, 34, 10, NULL, NULL),
(87, 36, 8, NULL, NULL),
(88, 37, 8, NULL, NULL),
(89, 38, 10, NULL, NULL),
(90, 40, 8, NULL, NULL),
(91, 41, 10, NULL, NULL),
(92, 42, 8, NULL, NULL),
(93, 43, 10, NULL, NULL),
(94, 44, 8, NULL, NULL),
(95, 45, 10, NULL, NULL),
(96, 46, 8, NULL, NULL),
(97, 47, 10, NULL, NULL),
(98, 48, 8, NULL, NULL),
(99, 49, 10, NULL, NULL),
(100, 50, 8, NULL, NULL),
(101, 52, 8, NULL, NULL),
(102, 53, 10, NULL, NULL),
(103, 54, 8, NULL, NULL),
(104, 55, 10, NULL, NULL),
(105, 57, 8, NULL, NULL),
(106, 58, 10, NULL, NULL),
(107, 59, 8, NULL, NULL),
(108, 60, 10, NULL, NULL),
(109, 61, 8, NULL, NULL),
(110, 62, 10, NULL, NULL),
(111, 63, 8, NULL, NULL),
(112, 64, 10, NULL, NULL),
(113, 65, 8, NULL, NULL),
(114, 66, 10, NULL, NULL),
(115, 67, 8, NULL, NULL),
(116, 69, 8, NULL, NULL),
(117, 70, 10, NULL, NULL),
(118, 75, 8, NULL, NULL),
(119, 76, 10, NULL, NULL),
(120, 78, 8, NULL, NULL),
(121, 79, 10, NULL, NULL),
(122, 80, 8, NULL, NULL),
(123, 81, 10, NULL, NULL),
(124, 82, 8, NULL, NULL),
(125, 83, 10, NULL, NULL),
(126, 84, 3, '2021-03-01 11:51:56', '2021-03-01 11:51:56'),
(127, 85, 3, '2021-03-01 12:14:30', '2021-03-01 12:14:30'),
(128, 87, 8, NULL, NULL),
(129, 88, 10, NULL, NULL),
(130, 89, 8, NULL, NULL),
(131, 90, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `building_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `id` int(10) UNSIGNED NOT NULL,
  `candidate_form_id` int(10) UNSIGNED NOT NULL,
  `scheduled_time` datetime NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `required_doc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Interview-Pending','Selected','Interview-Taken','Rejected') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `candidate_form_id`, `scheduled_time`, `branch_id`, `required_doc`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2021-02-24 15:45:00', 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Selected', 3, 3, NULL, '2021-02-23 10:45:59', '2021-02-23 10:46:16', NULL),
(2, 2, '2021-02-26 09:00:00', 3, '1. Resume\r\n2. Educational Documents', 'Selected', 3, 3, NULL, '2021-02-25 10:01:24', '2021-02-25 10:01:48', NULL),
(3, 3, '2021-03-25 17:25:00', 3, 'dewmo', 'Selected', 3, 3, NULL, '2021-02-28 10:22:50', '2021-02-28 10:23:29', NULL),
(4, 4, '2021-03-02 12:04:00', 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Selected', 3, 3, NULL, '2021-03-01 07:04:43', '2021-03-01 07:04:58', NULL),
(5, 5, '2021-03-02 09:25:00', 3, 'Resume \r\neducational Documents', 'Selected', 3, 3, NULL, '2021-03-01 09:21:58', '2021-03-01 09:22:31', NULL),
(6, 6, '2021-03-03 14:32:00', 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'Selected', 3, 3, NULL, '2021-03-01 09:32:47', '2021-03-01 09:33:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `description`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'A', 'Section A', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 11:41:41', '2021-02-25 11:41:41'),
(2, 'B', 'Section  B', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 11:41:41', '2021-02-25 11:41:41'),
(3, 'C', 'Section  C', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 11:41:41', '2021-02-25 11:41:41'),
(4, 'D', 'Section  D', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 11:41:41', '2021-02-25 11:41:41'),
(5, 'E', 'Section   E', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 11:41:41', '2021-02-25 11:41:58'),
(6, 'F', 'Section  F', 'Active', NULL, NULL, NULL, NULL, '2021-02-25 11:41:41', '2021-02-25 11:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_years` int(11) DEFAULT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `title`, `start_year`, `end_year`, `no_of_years`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '2021-2022- Session', '2021', '2022', 1, 'Active', 10, 10, NULL, NULL, '2021-02-23 05:49:52', '2021-02-24 10:05:24'),
(2, '2021-2023- Session', '2021', '2023', 2, 'Active', 10, 10, NULL, NULL, '2021-02-24 09:38:11', '2021-02-24 10:05:02'),
(3, '2021 -2025 -Session', '2021', '2025', 3, 'Active', 10, 10, NULL, NULL, '2021-02-24 09:38:33', '2021-02-24 10:05:52');

-- --------------------------------------------------------

--
-- Table structure for table `sms_settings`
--

CREATE TABLE `sms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `identity` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `working_hours` int(10) UNSIGNED NOT NULL,
  `reg_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `join_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `father_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mother_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blood_group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mother_tongue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `temp_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualification` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_info` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary_per_hour` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `job_status` enum('Permanent','Full Time','Part Time','Visiting','Lecture base') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `branch_id`, `user_id`, `working_hours`, `reg_no`, `staff_type`, `join_date`, `first_name`, `middle_name`, `last_name`, `father_name`, `mother_name`, `date_of_birth`, `gender`, `blood_group`, `nationality`, `mother_tongue`, `address`, `country`, `city`, `temp_address`, `home_phone`, `mobile_1`, `mobile_2`, `email`, `qualification`, `experience`, `other_info`, `staff_image`, `skill`, `salary`, `salary_per_hour`, `password`, `cnic`, `status`, `job_status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(8, 3, 21, 8, '54-35732', 'Coordinator', '2021-02-27', 'AYIZA', 'AHMED', 'KHAN', 'M ASLAM', 'AMEENA  KHAN', '1999-02-16', 'female', 'AB+', 'Pakistani', 'URDU', 'NIl', 'PAKISTANI', 'LAHORE', 'Nil', '035-24441574', '0524-7325714', '0512-3461145', 'ayiza@gmail.com', 'MSCS', '5', 'NIL', '/images/1614342902.jpeg', 'gvdfyewt   dsghdyet  shgvbyte', NULL, NULL, '$2y$10$3uUXC2wFw5o.Ntgh8fNar.5AKZdMkob1we9QH4hMS4g/taXMPtWjq', '54332-2365252-5', 'Active', 'Permanent', 3, NULL, NULL, NULL, '2021-02-26 12:35:02', '2021-02-26 12:38:50'),
(9, 2, 56, 9, '36-51236', 'Candidate', '2021-02-28', 'ZARA', 'AHMED', 'ABC', 'M ASIF', 'HASIBA', '1999-02-28', 'female', 'AB+', 'PAKISTANI', 'URDU', 'DBFJHFU3YHUADJKSAE', 'PAKISTAN', 'LAHORE', 'DHBYERQUYDGH', '093-84987893', '4374-9813749', '3719-8753298', 'zara101@gmail.com', 'MS IT', '6', 'WEGYWGQY', '/images/1614538046.png', 'E63451641ETY', '53473', NULL, '$2y$10$STDxFT17s5ZFpKOBIIm5e.I0zUmmPBQXyXtnUUY45xgDLS2JB3dpi', '43547-2572582-7', 'Active', 'Permanent', 3, NULL, NULL, NULL, '2021-02-28 18:47:26', '2021-02-28 18:56:29'),
(10, 2, 72, 123123, '56-32456', 'Teacher', '2021-03-03', 'bdchschxjc', 'hvghdcsvhbc', 'xcxhjbcnhd', 'ghvcgdhvdhvgdc', 'dgfvdhsvua6we7', '2021-03-03', 'female', 'AB+', 'PAKISTANI', 'urdu', 'xvcvghvcbz   xhjbshbxc hsbd', 'Pakistan', 'Lahore', 'ertetyyubhxc c   xcbhchgdsfb  839pJKHNJKn', '534-21215615', '0323-4242655', '0213-1454213', 'shahrukhlodhi5@gmail.com', 'MSCS', '5', '123123132', '/images/1614586071.png', '12321', '50000', NULL, '$2y$10$HKuDw0IJTkkvJELUVoLKYOdYTzTc/u/eidSFHUdDxSmXCTlZA49Bi', '12321-3232132-3', 'Active', 'Permanent', 3, NULL, NULL, NULL, '2021-03-01 08:07:51', '2021-03-02 06:27:40'),
(11, 1, 77, 5, '63-54623', 'Candidate', '2021-03-02', 'UZAIR', 'AHMED', 'KHAN', 'M ASIF  KHAN', 'CXGVSDTRG63', '2021-03-03', 'male', 'AB-', 'PAKISTANI', 'URDU', 'VBHJFBRRHFJCXNZIWE4OE3', 'PAKISTAN', 'LAHORE', 'XCGVDGVEYG3EUHYJBXCFCRYE4', '032-32534251', '0323-3241324', '0241-3415364', 'uzair1999@gmail.com', 'MSCS', '5', 'NIL', '/images/1614593165.jpeg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', '50000', NULL, '$2y$10$zDjv26WtzVwO06gJ0L7O9e6hHda8UFnoQdWrs54IwLuhnvnY0l2Vu', '23421-6215561-5', 'Active', 'Permanent', 3, NULL, NULL, NULL, '2021-03-01 10:06:06', '2021-03-02 06:28:16'),
(12, 1, 91, 8, '34-25452', 'Teacher', '2021-03-03', 'Zarwah Ahmed', 'xbvchsvd', 'xzchzdvbhs', 'M  Umair', 'Saira Ahmed', '1999-03-03', 'male', 'AB+', 'Pakistani', 'Urdu', 'Nil', 'Pakistan', 'Lahore', 'Nil', '034-25231421', '0333-3333333', '0962-5333344', 'zarwah1990@gmail.com', 'MSCS', '5', 'Nil', '/images/1614678679.jpeg', 'nil', NULL, NULL, '$2y$10$j.Uz5aWN81B8RMwWK1KSSeDhACqxBILtqK4z6TaOrO/g/Rn4rb9ZS', '35646-2343562-4', 'Active', 'Permanent', 3, NULL, NULL, NULL, '2021-03-02 09:51:19', '2021-03-02 09:51:19'),
(13, 3, 94, 8, '23-23232', 'Teacher', '2021-03-04', 'test', 'test', 'test', 'test f', 'test', '2021-03-24', 'male', 'B+', 'PAKISTANI', 'URDU', 'ASDASD', 'PAKISTAN', 'LAHORE', 'TEMP ADDRESS', '222-22222222', '2222-2222222', '2222-2222222', 'test@test.com', 'MS  Computer  Science', '2', '2', '/images/1614687103.jpeg', 'NO', NULL, NULL, '$2y$10$OkCk/uEo4BWXHmyGsPrV1uNrpHegbZAPGvGMnfL9KqPjsAE0St.Qm', '22222-2222222-2', 'Active', 'Permanent', 3, NULL, NULL, NULL, '2021-03-02 12:11:43', '2021-03-02 12:11:43'),
(15, 3, 97, 8, '12-22222', 'Teacher', '2021-03-03', 'test1', 'USAMA', 'test', 'test', 'test', '2021-03-19', 'male', 'B+', 'PAKISTANI', 'URDU', 'ASDASD', 'PAKISTAN', 'LAHORE', 'TEMP ADDRESS', '323-23453545', '5456-6546455', '5645-6456546', 'test221@gmail.com', 'MS  Computer  Science', '2', 'other', '/images/1614687902.jpeg', 'Demo', NULL, NULL, '$2y$10$/tubgloETLbiLUb41HXRvOJcDhOPp8AB0tsj2cd.kMK4NVMFv1D52', '12121-2121212-1', 'Active', 'Permanent', 3, NULL, NULL, NULL, '2021-03-02 12:25:03', '2021-03-02 12:25:03');

-- --------------------------------------------------------

--
-- Table structure for table `staff_transfer_branches`
--

CREATE TABLE `staff_transfer_branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `staff_status` enum('pending','accept','reject') COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_transfer_branches`
--

INSERT INTO `staff_transfer_branches` (`id`, `user_id`, `staff_status`, `branch_id`, `created_at`, `updated_at`) VALUES
(1, 12, 'pending', 12, '2021-02-23 07:42:39', '2021-02-23 07:42:39'),
(2, 14, 'accept', 3, '2021-02-23 08:09:15', '2021-02-23 09:10:11'),
(3, 16, 'accept', 2, '2021-02-24 06:05:48', '2021-02-24 06:07:38'),
(4, 19, 'accept', 3, '2021-02-25 12:50:52', '2021-02-25 12:53:17'),
(5, 20, 'pending', 2, '2021-02-26 05:23:52', '2021-02-26 05:23:52'),
(6, 21, 'accept', 3, '2021-02-26 12:37:33', '2021-02-26 12:38:50'),
(7, 56, 'accept', 2, '2021-02-28 18:55:52', '2021-02-28 18:56:29'),
(8, 77, 'accept', 2, '2021-03-01 10:07:04', '2021-03-01 10:09:42'),
(9, 91, 'pending', 3, '2021-03-02 11:57:57', '2021-03-02 11:57:57');

-- --------------------------------------------------------

--
-- Table structure for table `std_active_s_elecive`
--

CREATE TABLE `std_active_s_elecive` (
  `id` int(10) UNSIGNED NOT NULL,
  `active_session_student_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `reg_no` varchar(191) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `board_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `academic_group_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `reg_date` varchar(191) DEFAULT NULL,
  `academic_status` varchar(191) DEFAULT NULL,
  `first_name` varchar(191) DEFAULT NULL,
  `middle_name` varchar(191) DEFAULT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `date_of_birth` varchar(191) DEFAULT NULL,
  `gender` varchar(191) DEFAULT NULL,
  `blood_group` varchar(191) DEFAULT NULL,
  `nationality` varchar(191) DEFAULT NULL,
  `mother_tongue` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `extra_info` varchar(191) DEFAULT NULL,
  `home_phone` varchar(191) DEFAULT NULL,
  `mobile_1` varchar(191) DEFAULT NULL,
  `mobile_2` varchar(191) DEFAULT NULL,
  `state` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `city` varchar(191) DEFAULT NULL,
  `address` varchar(191) DEFAULT NULL,
  `pervious_school` varchar(191) DEFAULT NULL,
  `temp_address` varchar(191) DEFAULT NULL,
  `pervious_class` varchar(191) DEFAULT NULL,
  `student_image` varchar(191) DEFAULT NULL,
  `status` enum('Active','InActive','Pending') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Active',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `reg_no`, `user_id`, `branch_id`, `board_id`, `program_id`, `academic_group_id`, `class_id`, `section_id`, `reg_date`, `academic_status`, `first_name`, `middle_name`, `last_name`, `date_of_birth`, `gender`, `blood_group`, `nationality`, `mother_tongue`, `email`, `extra_info`, `home_phone`, `mobile_1`, `mobile_2`, `state`, `country`, `city`, `address`, `pervious_school`, `temp_address`, `pervious_class`, `student_image`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`) VALUES
(23, '0-00023', NULL, 1, NULL, 1, NULL, NULL, NULL, '2021-03-02', 'New Admission', 'ALI', 'AHMED', 'KHAN', '1993-02-02', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'aliahmed@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/tmp/phpsQm6Eg', 'Active', NULL, NULL, NULL, '2021-03-02 06:52:09', '2021-03-02 06:52:09'),
(22, '26-00022', NULL, 2, NULL, 1, NULL, NULL, NULL, '2021-03-02', 'New Admission', 'MUAZNA', 'AHMED', 'KHAN', '1999-08-02', 'FEMALE', 'A+', 'PAKISTANI', 'URDU', 'muazna101@gmail.com', 'Nil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/tmp/phpGdDhM9', 'Active', NULL, NULL, NULL, '2021-03-02 06:44:57', '2021-03-02 06:44:57'),
(3, '0-0003', 40, 1, 1, 1, 1, 1, 1, '2021-02-28', 'New Admission', 'DEMO100', 'USAMA', 'WEREWR', '2021-02-03', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'admin1001@rootsivy.edu.pk', 'Extra', '03234775075', '03134775075', '04234775075', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'DEMO', NULL, 'TEMP ADDRESS', NULL, '/tmp/phpLT2iBT', 'Active', NULL, NULL, NULL, '2021-02-28 10:25:33', '2021-02-28 10:25:33'),
(4, '0-0004', 42, 1, 1, 1, 1, 1, 1, '2021-02-28', 'New Admission', 'DEMO00', 'WALEED00', 'KHAN00', '2021-02-03', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'studentnew@rootsivy.edu.pk', 'Etra', '03234775075', '03134775075', '04234775075', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'ASDASD', NULL, 'TEMP ADDRESS', NULL, '/tmp/phppi6ebu', 'Active', NULL, NULL, NULL, '2021-02-28 10:35:31', '2021-02-28 10:35:31'),
(5, '0-0005', 44, 4, 1, 1, 1, 1, 1, '2021-02-28', 'New Admission', 'DEMO090', 'USAMA', 'DEMO', '2021-02-03', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'asdfgh@rootsivy.edu.pk', 'wwww', '03234775075', '03134775075', '04234775075', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'ASDASD', NULL, 'TEMP ADDRESS', NULL, '/tmp/phpsII6r1', 'Active', NULL, NULL, NULL, '2021-02-28 10:38:39', '2021-02-28 10:38:39'),
(6, '0-0006', 46, 4, 1, 1, 1, 1, 1, '2021-02-28', 'New Admission', 'JJ', 'ASAS', 'ASASAS', '2020-12-09', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'VDV@rootsivy.edu.pk', 'extra', '03234775075', '03134775075', '04234775075', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'ASDASD', NULL, 'TEMP ADDRESS', NULL, '/tmp/phpZc1b7D', 'Active', NULL, NULL, NULL, '2021-02-28 11:05:03', '2021-02-28 11:05:03'),
(7, '0-0007', 48, 1, 1, 1, 1, 1, 1, '2021-02-28', 'New Admission', 'KKK1', 'KK1', 'K1K', '2021-02-17', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'KK1@gmail.com', 'Extra', '03234775075', '03134775075', '04234775075', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'DEMO', NULL, 'TEMP ADDRESS', NULL, '/tmp/phpclmy6W', 'Active', NULL, NULL, NULL, '2021-02-28 11:19:32', '2021-02-28 11:19:32'),
(21, '0-00021', NULL, 1, NULL, 1, NULL, NULL, NULL, '2021-03-01', 'New Admission', 'ALI', '23WEREWR', 'DEMO', '2020-12-09', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'student6666@gmail.com', 'extra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/tmp/phpAMm6rX', 'Active', NULL, NULL, NULL, '2021-03-01 11:30:37', '2021-03-01 11:30:37'),
(9, '0-0009', 52, 1, 1, 1, NULL, 1, 1, '2021-02-28', 'New Admission', 'FATIMA', 'ASLAM', 'AHMED', '1993-09-10', 'FEMALE', 'A+', 'PAKISTANI', 'URDU', 'fatima@gmail.com', 'NIL', '03032514651', '03234242655', '03224456567', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'NIL', NULL, 'NIL', NULL, '/tmp/phpsS5Vio', 'Active', NULL, NULL, NULL, '2021-02-28 17:35:32', '2021-02-28 17:35:32'),
(10, '0-00010', 54, 1, 1, 3, 1, 1, 1, '2021-02-28', 'New Admission', 'AREEJ', 'FATIMA', 'AM', '1999-12-10', 'FEMALE', 'AB+', 'PAKISTANI', 'URDU', 'fatima101@gmail.com', 'Nil', '03024242666', '03213344555', '03331234567', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'HSGDYQWGSW', NULL, 'DSDHWQUEQHSUQW', NULL, '/tmp/phpT9KIV9', 'Active', NULL, NULL, NULL, '2021-02-28 18:20:42', '2021-02-28 18:20:42'),
(11, '0-00011', 57, 1, 1, 1, 1, 1, 1, '2021-03-01', 'New Admission', 'DEMO202', 'DOMI', 'DEMO2021', '2021-03-02', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'zain001@netroots.com', 'Extra Info', '03234775075', '03134775075', '04234775075', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'DEMO', NULL, 'TEMP ADDRESS', NULL, '/tmp/phpefnaD3', 'Active', NULL, NULL, NULL, '2021-03-01 05:13:11', '2021-03-01 05:13:11'),
(12, '0-00012', 59, 1, 1, 3, 1, 10, 1, '2021-03-01', 'New Admission', 'AYESHA', 'AHMED', 'OWAIS', '1994-09-03', 'FEMALE', 'A+', 'PAKISTANI', 'URDU', 'fatima1994@gmail.com', 'NIL', '03334242651', '03334242656', '033345626221', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'NIL', NULL, 'NIL', NULL, '/tmp/phpAZCahI', 'Active', NULL, NULL, NULL, '2021-03-01 05:25:22', '2021-03-01 05:25:22'),
(13, '0-00013', 61, 1, 1, 2, NULL, 5, 1, '2021-03-01', 'New Admission', 'MOHAMMED', 'ASIF', 'ASLAM', '1999-10-19', 'FEMALE', 'A+', 'PAKISTANI', 'URDU', 'masif1994@gmail.com', 'Nil', '0342342532424', '03234343655', '032324342332', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'DFYEWT4R736TGBHSXC', NULL, '4R743Y8736Y2UDFHYSDHFJ', NULL, '/tmp/phpkB9yfT', 'Active', NULL, NULL, NULL, '2021-03-01 05:36:47', '2021-03-01 05:36:47'),
(14, '0-00014', 63, 1, 1, 1, 1, 1, 1, '2021-03-01', 'New Admission', 'MUAZNA', 'AHMED', 'KHAN', '1998-09-03', 'FEMALE', 'A+', 'PAKISTANI', 'URDU', 'muazna1999@gmail.com', 'NIL', '03334545876', '03334867388', '03332234876', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'VCBVHJXBFUHNJCV CVHBJHCFVB  VCVBFH', NULL, 'VCHBCHJVBFYHUDDU  CXHVJFB  VCJHVBF', NULL, '/tmp/phpxX5bte', 'Active', NULL, NULL, NULL, '2021-03-01 05:45:16', '2021-03-01 05:45:16'),
(15, '0-00015', 65, 1, 1, 1, 1, 1, 1, '2021-03-01', 'New Admission', 'ABRAR', 'ALI', 'KHAN', '2021-03-08', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'ibrar@gmail.com', 'Extra', '03234775075', '03134775075', '04234775075', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'DEMO', NULL, 'TEMP', NULL, '/tmp/phpn8Kh5S', 'Active', NULL, NULL, NULL, '2021-03-01 05:53:22', '2021-03-01 05:53:22'),
(19, '0-00019', NULL, 4, NULL, 1, NULL, NULL, NULL, '2021-03-01', 'New Admission', 'MOHAMMED', 'AHMED', 'ASIF', '1999-09-02', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'ahmedasif111@gmail.com', 'NIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/tmp/phpVy1iyI', 'Active', NULL, NULL, NULL, '2021-03-01 10:39:44', '2021-03-01 10:39:44'),
(20, '0-00020', NULL, 1, NULL, 1, NULL, NULL, NULL, '2021-03-01', 'New Admission', 'MOAZ', 'AHMED', 'KHAN', '1993-09-03', 'FEMALE', 'A+', 'PAKISTANI', 'URDU', 'moaz@gmail.com', 'NIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '/tmp/phpmXhNb6', 'Active', NULL, NULL, NULL, '2021-03-01 11:11:31', '2021-03-01 11:11:31'),
(17, '0-00017', 69, 1, 1, 1, 1, 1, 1, '2021-03-01', 'New Admission', 'BBB', 'BBB', 'BBB', '2021-03-01', 'MALE', 'A+', 'PAKISTANI', 'URDU', 'bb@gmail.com', 'Extra', '03234775075', '03134775075', '04234775075', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'DEMO', NULL, 'TEMP', NULL, '/tmp/phpy1vkZa', 'Active', NULL, NULL, NULL, '2021-03-01 06:19:06', '2021-03-01 06:19:06'),
(18, '0-00018', NULL, 1, 1, 1, 3, 1, 1, '2021-03-01', 'New Admission', 'SARA', 'AHMED', 'ABC', '1993-01-01', 'FEMALE', 'A+', 'PAKISTAN', 'URDU', 'saraahmed@gmail.com', 'NIL', '03234242555', '03024108858', '03334465677', 'PUNJAB', 'PAKISTAN', 'LAHORE', 'NIL', 'NIL', 'NIL', 'NIL', '/tmp/phpp3VQ0F', 'Active', NULL, NULL, NULL, '2021-03-01 09:55:44', '2021-03-01 09:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `student_enrollments`
--

CREATE TABLE `student_enrollments` (
  `branch_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_fee_settings`
--

CREATE TABLE `student_fee_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `program_id` int(10) UNSIGNED DEFAULT NULL,
  `installment_id` int(10) UNSIGNED DEFAULT NULL,
  `basic_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_fee_settings`
--

INSERT INTO `student_fee_settings` (`id`, `student_id`, `program_id`, `installment_id`, `basic_fee`, `discount`, `status`, `created_at`, `updated_at`) VALUES
(1, 81, NULL, 0, NULL, NULL, '1', '2020-09-11 01:53:52', '2020-09-11 01:53:52'),
(2, 82, NULL, 0, NULL, NULL, '1', '2020-09-17 05:12:01', '2020-09-17 05:12:01'),
(3, 83, NULL, 0, NULL, NULL, '1', '2020-09-18 00:27:58', '2020-09-18 00:27:58'),
(4, 84, NULL, 0, NULL, NULL, '1', '2020-09-18 02:46:44', '2020-09-18 02:46:44'),
(5, 86, NULL, 0, NULL, NULL, '1', '2020-09-18 08:03:11', '2020-09-18 08:03:11'),
(6, 87, NULL, 0, NULL, NULL, '1', '2020-09-18 08:09:48', '2020-09-18 08:09:48'),
(7, 88, NULL, 0, NULL, NULL, '1', '2020-09-18 08:36:51', '2020-09-18 08:36:51'),
(8, 91, NULL, 0, NULL, NULL, '1', '2020-09-23 00:58:43', '2020-09-23 00:58:43'),
(9, 92, NULL, 0, NULL, NULL, '1', '2020-09-23 01:02:25', '2020-09-23 01:02:25'),
(10, 93, NULL, 0, NULL, NULL, '1', '2020-09-23 01:08:55', '2020-09-23 01:08:55'),
(11, 94, NULL, 0, NULL, NULL, '1', '2020-09-23 01:28:00', '2020-09-23 01:28:00'),
(12, 95, NULL, 0, NULL, NULL, '1', '2020-09-23 01:31:58', '2020-09-23 01:31:58'),
(13, 96, NULL, 0, NULL, NULL, '1', '2020-09-23 01:49:18', '2020-09-23 01:49:18'),
(14, 97, NULL, 0, NULL, NULL, '1', '2020-09-25 05:31:00', '2020-09-25 05:31:00'),
(15, 98, NULL, 0, NULL, NULL, '1', '2020-09-28 07:43:44', '2020-09-28 07:43:44'),
(16, 99, NULL, 0, NULL, NULL, '1', '2020-09-28 08:04:48', '2020-09-28 08:04:48'),
(17, 100, NULL, 0, NULL, NULL, '1', '2020-09-29 05:16:24', '2020-09-29 05:16:24'),
(18, 101, NULL, 0, NULL, NULL, '1', '2020-09-29 05:34:18', '2020-09-29 05:34:18'),
(19, 102, NULL, 0, NULL, NULL, '1', '2020-09-29 06:38:58', '2020-09-29 06:38:58'),
(20, 103, NULL, 0, NULL, NULL, '1', '2020-09-30 04:04:50', '2020-09-30 04:04:50'),
(21, 104, NULL, 0, NULL, NULL, '1', '2020-10-02 10:52:33', '2020-10-02 10:52:33'),
(22, 105, NULL, 0, NULL, NULL, '1', '2020-10-11 12:22:45', '2020-10-11 12:22:45'),
(23, 106, NULL, 0, NULL, NULL, '1', '2020-10-12 11:09:26', '2020-10-12 11:09:26'),
(24, 107, NULL, 0, NULL, NULL, '1', '2020-10-13 08:04:07', '2020-10-13 08:04:07'),
(25, 108, NULL, 0, NULL, NULL, '1', '2020-10-13 08:58:58', '2020-10-13 08:58:58'),
(26, 109, NULL, NULL, NULL, NULL, '1', '2020-12-01 11:59:45', '2020-12-01 11:59:45'),
(27, 111, NULL, NULL, NULL, NULL, '1', '2020-12-04 13:02:55', '2020-12-04 13:02:55'),
(28, 112, NULL, NULL, NULL, NULL, '1', '2020-12-04 13:05:56', '2020-12-04 13:05:56'),
(29, 113, NULL, NULL, NULL, NULL, '1', '2020-12-04 13:06:48', '2020-12-04 13:06:48'),
(30, 114, NULL, NULL, NULL, NULL, '1', '2020-12-04 13:07:22', '2020-12-04 13:07:22'),
(31, 110, NULL, NULL, NULL, NULL, '1', '2020-12-09 06:43:58', '2020-12-09 06:43:58'),
(32, 111, NULL, NULL, NULL, NULL, '1', '2020-12-09 06:51:02', '2020-12-09 06:51:02');

-- --------------------------------------------------------

--
-- Table structure for table `student_guardians`
--

CREATE TABLE `student_guardians` (
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `students_id` int(11) NOT NULL,
  `guardians_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_guardians`
--

INSERT INTO `student_guardians` (`created_at`, `updated_at`, `students_id`, `guardians_id`) VALUES
('2021-02-28 10:25:33', '2021-02-28 10:25:33', 3, 1),
('2021-02-28 10:35:31', '2021-02-28 10:35:31', 4, 2),
('2021-02-28 10:38:39', '2021-02-28 10:38:39', 5, 3),
('2021-02-28 11:05:03', '2021-02-28 11:05:03', 6, 4),
('2021-02-28 11:19:32', '2021-02-28 11:19:32', 7, 5),
('2021-02-28 17:35:32', '2021-02-28 17:35:32', 9, 6),
('2021-02-28 18:20:42', '2021-02-28 18:20:42', 10, 7),
('2021-03-01 05:13:11', '2021-03-01 05:13:11', 11, 8),
('2021-03-01 05:25:22', '2021-03-01 05:25:22', 12, 9),
('2021-03-01 05:36:47', '2021-03-01 05:36:47', 13, 10),
('2021-03-01 05:45:16', '2021-03-01 05:45:16', 14, 11),
('2021-03-01 05:53:22', '2021-03-01 05:53:22', 15, 12),
('2021-03-01 06:19:06', '2021-03-01 06:19:06', 17, 13),
('2021-03-01 09:55:44', '2021-03-01 09:55:44', 18, 14),
('2021-03-01 10:39:44', '2021-03-01 10:39:44', 19, 15),
('2021-03-01 11:11:31', '2021-03-01 11:11:31', 20, 16),
('2021-03-01 11:30:37', '2021-03-01 11:30:37', 21, 17),
('2021-03-02 06:44:57', '2021-03-02 06:44:57', 22, 18),
('2021-03-02 06:52:09', '2021-03-02 06:52:09', 23, 19);

-- --------------------------------------------------------

--
-- Table structure for table `student_status`
--

CREATE TABLE `student_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `syllabi`
--

CREATE TABLE `syllabi` (
  `id` int(11) NOT NULL,
  `syllabus_head_id` int(11) DEFAULT NULL,
  `active_session_id` int(11) DEFAULT NULL,
  `description` varchar(191) DEFAULT NULL,
  `file` varchar(191) DEFAULT NULL,
  `status` varchar(191) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `syllabi`
--

INSERT INTO `syllabi` (`id`, `syllabus_head_id`, `active_session_id`, `description`, `file`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 'asdasdasd', '7036.sql', '1', 12, NULL, '2021-02-16 11:02:06', '2021-02-16 11:02:06'),
(2, 2, 3, 'asdasd', '5431.sql', '1', 12, NULL, '2021-02-16 11:05:55', '2021-02-16 11:05:55'),
(3, 3, 4, 'new', '8989.sql', '1', 11, NULL, '2021-02-17 10:11:59', '2021-02-17 10:11:59'),
(4, 6, NULL, 'EXAM  syllabus', '6855.jpg', '1', 1, NULL, '2021-02-24 08:03:54', '2021-02-24 08:03:54'),
(5, 8, NULL, 'All the best', '7129.jpg', '1', 1, NULL, '2021-02-24 09:13:52', '2021-02-24 09:13:52'),
(6, 2, NULL, 'asd', '8615.jpg', '1', 1, NULL, '2021-02-24 09:16:47', '2021-02-24 09:16:47'),
(7, 9, NULL, 'new subject introduced.', '5620.xlsx', '1', 1, NULL, '2021-02-24 09:18:44', '2021-02-24 09:18:44'),
(8, 4, NULL, 'This  is  Quiz  Syllabus', '4637.jpg', '1', 1, NULL, '2021-02-24 09:21:24', '2021-02-24 09:21:24'),
(9, 10, NULL, 'Syllabus  For Exam  Syllabus', '9567.jpg', '1', 1, NULL, '2021-02-25 08:14:17', '2021-02-25 08:14:17'),
(10, 3, 16, 'EXAM  SYLLABUS', '9713.jpeg', '1', 1, NULL, '2021-02-27 23:36:33', '2021-02-27 23:36:33'),
(11, 5, 25, 'Mid  Term  Portfolio', '9381.jpeg', '1', 1, NULL, '2021-02-28 18:27:16', '2021-02-28 18:27:16'),
(12, 11, NULL, 'For all the failed students', '9630.csv', '1', 1, NULL, '2021-03-01 05:59:30', '2021-03-01 05:59:30'),
(13, 12, 18, 'All students', '8096.csv', '1', 1, NULL, '2021-03-01 06:11:16', '2021-03-01 06:11:16');

-- --------------------------------------------------------

--
-- Table structure for table `syllabus_heads`
--

CREATE TABLE `syllabus_heads` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `syllabus_head_title` varchar(191) DEFAULT NULL,
  `slug` varchar(191) DEFAULT NULL,
  `status` varchar(191) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `syllabus_heads`
--

INSERT INTO `syllabus_heads` (`id`, `branch_id`, `session_id`, `syllabus_head_title`, `slug`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(2, NULL, NULL, 'Class', 'Class', '1', 12, NULL, '2021-02-15 21:04:28', '2021-02-24 07:58:01'),
(3, NULL, NULL, 'Exam', 'Exam', '1', 12, NULL, '2021-02-15 21:28:59', '2021-03-01 05:55:02'),
(4, NULL, NULL, 'Quiz', 'Quiz', '1', 12, NULL, '2021-02-15 21:29:17', '2021-03-01 05:55:08'),
(5, 4, NULL, 'mids', 'mids', NULL, 12, NULL, '2021-02-16 12:32:23', '2021-02-16 12:32:23'),
(6, 4, NULL, 'Final Term Syllabus', 'Final Term Syllabus', '1', 1, NULL, '2021-02-24 07:57:27', '2021-02-24 07:57:34'),
(8, 4, NULL, 'Class Test', 'Class Test', '1', 1, NULL, '2021-02-24 09:12:47', '2021-02-24 09:12:54'),
(9, 4, NULL, 'new', 'new', '1', 1, NULL, '2021-02-24 09:13:27', '2021-02-24 09:13:35'),
(10, 4, NULL, 'Mid  Term Exam Syllabus', 'Mid  Term Exam Syllabus', '1', 1, NULL, '2021-02-25 08:10:49', '2021-02-25 08:10:58'),
(12, 4, NULL, 'new syllabus', 'new syllabus', '1', 1, NULL, '2021-03-01 06:09:46', '2021-03-01 06:09:52');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_classes`
--

CREATE TABLE `teacher_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `terminations`
--

CREATE TABLE `terminations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warnings` enum('first','second','third') COLLATE utf8mb4_unicode_ci NOT NULL,
  `acknowledge` enum('no','yes') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terminations`
--

INSERT INTO `terminations` (`id`, `user_id`, `reason`, `warnings`, `acknowledge`, `created_at`, `updated_at`) VALUES
(1, 12, 'not on time', 'first', 'yes', '2021-02-23 09:44:48', '2021-02-23 09:45:17'),
(2, 16, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'first', 'yes', '2021-02-24 06:05:11', '2021-02-24 06:07:30'),
(3, 77, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', 'first', 'yes', '2021-03-01 10:06:35', '2021-03-01 10:09:52');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `academic_year_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','InActive','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `academic_year_id`, `name`, `start_month`, `start_year`, `end_month`, `end_year`, `status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'S-21-22-Y-21-22-T1', 'August', '2021', 'December', '2022', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:45:39', '2021-02-26 09:51:16'),
(2, 1, 'S-21-22-Y-21-22-T2', 'January', '2021', 'June', '2022', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:46:26', '2021-02-25 07:46:26'),
(3, 2, 'S-21-23 -Y-21-22-T1', 'August', '2021', 'December', '2022', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:48:11', '2021-02-25 07:48:11'),
(4, 2, 'S-21-23 -Y-21-22-T2', 'January', '2021', 'June', '2022', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:49:23', '2021-02-25 07:49:23'),
(5, 3, 'S-21-23 -Y-22-23-T1', 'August', '2022', 'December', '2023', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:51:21', '2021-02-25 07:51:21'),
(6, 3, 'S-21-23 -Y-22-23-T2', 'January', '2022', 'June', '2023', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:52:38', '2021-02-25 07:52:38'),
(7, 4, 'S-21-25 -Y-21-22-T1', 'August', '2021', 'December', '2022', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:54:37', '2021-02-25 07:54:37'),
(8, 4, 'S-21-25 -Y-21-22-T2', 'January', '2021', 'June', '2022', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:56:10', '2021-02-25 07:56:10'),
(9, 5, 'S-21-25 -Y-22-23-T1', 'August', '2022', 'December', '2023', 'Active', 10, NULL, NULL, NULL, '2021-02-25 07:58:35', '2021-02-25 07:58:35'),
(10, 5, 'S-21-25 -Y-22-23-T2', 'January', '2022', 'June', '2023', 'Active', 10, NULL, NULL, NULL, '2021-02-25 08:00:42', '2021-02-25 08:00:42'),
(11, 6, 'S-21-25 -Y-23-24-T1', 'August', '2023', 'December', '2024', 'Active', 10, NULL, NULL, NULL, '2021-02-25 08:04:13', '2021-02-25 08:04:13'),
(12, 6, 'S-21-25 -Y-23-24-T2', 'January', '2023', 'June', '2024', 'Active', 10, NULL, NULL, NULL, '2021-02-25 08:05:36', '2021-02-25 08:05:36'),
(13, 7, 'S-21-25 -Y-24 -25-T1', 'August', '2024', 'December', '2025', 'Active', 10, NULL, NULL, NULL, '2021-02-25 08:07:50', '2021-02-25 08:07:50'),
(14, 7, 'S-21-25 -Y-24 -25-T2', 'January', '2024', 'June', '2025', 'Active', 10, NULL, NULL, NULL, '2021-02-25 08:08:50', '2021-02-25 08:08:50');

-- --------------------------------------------------------

--
-- Table structure for table `timetable_categories`
--

CREATE TABLE `timetable_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `timetable_categories`
--

INSERT INTO `timetable_categories` (`id`, `name`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'EYP  Class  Timetable', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 1, 10, NULL, '2021-02-23 12:06:16', '2021-02-23 12:06:16'),
(2, 'EYP  Class Timetable', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 1, 10, NULL, '2021-02-25 13:08:42', '2021-02-25 13:08:42'),
(3, 'Time  Table  for  O LEVEL  -1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore', 1, 10, NULL, '2021-03-01 07:17:31', '2021-03-01 07:17:31');

-- --------------------------------------------------------

--
-- Table structure for table `time_tables`
--

CREATE TABLE `time_tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(10) UNSIGNED NOT NULL,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_tables`
--

INSERT INTO `time_tables` (`id`, `title`, `branch_id`, `start_date`, `end_date`, `start_time`, `end_time`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'EYP  Class  Timetable', 1, '', '', '10:10', '19:08', 10, NULL, '2021-02-23 12:07:21', '2021-02-23 12:07:21'),
(2, 'Class  Time  Table', 1, '', '', '10:26', '14:27', 10, NULL, '2021-02-24 05:27:13', '2021-02-24 05:27:13'),
(3, 'Physics     Class  Timetable', 1, '', '', '10:00', '23:59', 10, NULL, '2021-03-01 05:00:15', '2021-03-01 05:00:15'),
(4, 'Time table  for  EYP Classes', 1, '', '', '09:24', '22:00', 10, NULL, '2021-03-01 07:18:24', '2021-03-01 07:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `todos`
--

CREATE TABLE `todos` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `task` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `todos`
--

INSERT INTO `todos` (`id`, `branch_id`, `task`, `created_by`, `created_at`, `updated_at`) VALUES
(5, 4, 'Board of directors meeting', 6, '2020-09-25 04:11:54', '2020-09-25 04:11:54'),
(3, 7, 'Hello', 4, '2020-09-18 06:15:31', '2020-09-18 06:15:31'),
(6, 4, 'Test To DO', 14, '2020-09-28 03:13:54', '2020-09-28 03:13:54');

-- --------------------------------------------------------

--
-- Table structure for table `trainees`
--

CREATE TABLE `trainees` (
  `id` int(10) UNSIGNED NOT NULL,
  `staff_id` bigint(20) NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `avalibility` tinyint(1) NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acknowledge` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trainees`
--

INSERT INTO `trainees` (`id`, `staff_id`, `training_id`, `avalibility`, `remarks`, `acknowledge`, `created_at`, `updated_at`) VALUES
(1, 12, 1, 1, NULL, 1, NULL, '2021-02-23 13:01:43'),
(2, 1, 3, 1, 'I  have accepted  this  training', 1, NULL, '2021-02-23 13:05:02'),
(3, 1, 1, 0, NULL, 0, NULL, NULL),
(4, 11, 2, 0, NULL, 1, NULL, '2021-02-26 05:28:56'),
(5, 17, 2, 1, NULL, 1, NULL, '2021-02-25 12:52:20'),
(6, 11, 4, 0, NULL, 0, NULL, NULL),
(7, 12, 4, 0, NULL, 0, NULL, NULL),
(8, 14, 4, 0, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(10) UNSIGNED NOT NULL,
  `venue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` enum('local','international') COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `training_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_days` bigint(20) DEFAULT NULL,
  `training_charges` tinyint(1) NOT NULL,
  `training_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `venue`, `category`, `department`, `designation`, `branch_id`, `description`, `date`, `time`, `training_by`, `no_of_days`, `training_charges`, `training_type`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'johar town', 'local', 'IT', 'Lecturer', 1, 'all the faculty seniior and junior lecturers are encourgaed to join', '2021-02-27', '13:05', 'azhar', 1, 0, 'ethical training', 3, 3, '2021-02-23 12:55:54', '2021-02-25 06:48:31'),
(2, 'Enrichment Professional Development', 'local', 'IT and  Computer Science', 'IT  Head', 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '2021-02-25', '10:04', 'Saira  Ahmed', 3, 1, 'Local', 3, 0, '2021-02-23 13:00:03', '2021-02-23 13:00:03'),
(3, 'Language Tutor Training Certification, Certificate', 'international', 'Professional  Behavior   Studies', 'Behavioral Studies  Head', 1, NULL, '2021-02-24', '10:05', 'Saira  Gillani', 3, 1, 'Regional', 3, 0, '2021-02-23 13:01:48', '2021-02-23 13:01:48'),
(4, 'Enrichment Professional Development', 'local', 'Professional  Behavior  Studies', 'HR  Head', 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ', '2021-03-03', '09:40', 'Saira  Ahmed  Khan', 5, 1, 'Regional', 3, 0, '2021-03-02 06:31:28', '2021-03-02 06:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `unit_planner`
--

CREATE TABLE `unit_planner` (
  `id` int(11) NOT NULL,
  `title` varchar(191) NOT NULL,
  `curriculum_id` int(11) NOT NULL,
  `date` int(11) DEFAULT NULL,
  `file` int(11) DEFAULT NULL,
  `description` int(11) DEFAULT NULL,
  `status` varchar(191) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit_planner`
--

INSERT INTO `unit_planner` (`id`, `title`, `curriculum_id`, `date`, `file`, `description`, `status`, `created_by`, `last_updated_by`, `created_at`, `updated_at`) VALUES
(1, 'asdasd', 1, 2021, 7154, 0, NULL, 12, NULL, '2021-02-16 15:33:30', '2021-02-16 15:33:30'),
(2, 'dasdasd', 1, 2021, 8299, 0, NULL, 12, NULL, '2021-02-16 15:34:29', '2021-02-16 15:34:29'),
(3, 'ABC', 3, 2021, 5425, 0, NULL, 1, NULL, '2021-02-28 16:28:39', '2021-02-28 16:28:39'),
(4, 'Unit #  5', 4, 2021, 7012, 0, '1', 1, NULL, '2021-02-28 18:30:37', '2021-02-28 18:33:08'),
(5, 'monday maths', 5, 2021, 9170, 0, '1', 1, NULL, '2021-03-01 06:13:19', '2021-03-01 06:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `uploaded_documents`
--

CREATE TABLE `uploaded_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_event_id` int(10) UNSIGNED NOT NULL,
  `files_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_pemissions`
--

CREATE TABLE `user_pemissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `last_updated_by` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `staffs` int(10) UNSIGNED DEFAULT NULL,
  `programs` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_pemissions`
--

INSERT INTO `user_pemissions` (`id`, `created_at`, `updated_at`, `created_by`, `last_updated_by`, `branch_id`, `staffs`, `programs`) VALUES
(42, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 1),
(43, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 3),
(44, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 2),
(45, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 4),
(46, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 5),
(47, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 6),
(48, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 7),
(49, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 8),
(50, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 9),
(51, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 10),
(52, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 11),
(53, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 12),
(54, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 13),
(55, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 14),
(56, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 15),
(57, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 16),
(58, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 17),
(59, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 18),
(60, '2020-08-12 19:40:29', '2020-08-12 19:40:29', 6, NULL, 7, 3, 19),
(115, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 9),
(114, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 8),
(113, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 7),
(112, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 6),
(111, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 5),
(110, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 4),
(109, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 3),
(108, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 2),
(107, '2020-08-23 22:18:50', '2020-08-23 22:18:50', 6, NULL, 7, 1, 1),
(80, '2020-08-12 19:46:44', '2020-08-12 19:46:44', 6, NULL, 7, 8, 1),
(81, '2020-08-12 19:46:44', '2020-08-12 19:46:44', 6, NULL, 7, 8, 2),
(82, '2020-08-12 19:46:44', '2020-08-12 19:46:44', 6, NULL, 7, 8, 3),
(83, '2020-08-12 19:47:55', '2020-08-12 19:47:55', 6, NULL, 7, 10, 4),
(84, '2020-08-12 19:47:55', '2020-08-12 19:47:55', 6, NULL, 7, 10, 5),
(85, '2020-08-12 19:47:55', '2020-08-12 19:47:55', 6, NULL, 7, 10, 6),
(86, '2020-08-12 19:47:55', '2020-08-12 19:47:55', 6, NULL, 7, 10, 8),
(87, '2020-08-12 19:47:55', '2020-08-12 19:47:55', 6, NULL, 7, 10, 7),
(88, '2020-08-12 19:47:55', '2020-08-12 19:47:55', 6, NULL, 7, 10, 9),
(89, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 1),
(90, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 2),
(91, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 3),
(92, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 4),
(93, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 5),
(94, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 7),
(95, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 6),
(96, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 9),
(97, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 10),
(98, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 11),
(99, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 12),
(100, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 13),
(101, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 14),
(102, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 15),
(103, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 16),
(104, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 17),
(105, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 18),
(106, '2020-08-12 19:51:31', '2020-08-12 19:51:31', 6, NULL, 7, 11, 19);

-- --------------------------------------------------------

--
-- Table structure for table `walk_in_registration`
--

CREATE TABLE `walk_in_registration` (
  `id` int(6) UNSIGNED NOT NULL,
  `branch_id` int(11) NOT NULL,
  `student_first_name` varchar(255) NOT NULL,
  `student_middle_name` varchar(255) DEFAULT NULL,
  `student_last_name` varchar(255) NOT NULL,
  `parent_first_name` varchar(255) NOT NULL,
  `parent_middle_name` varchar(255) DEFAULT NULL,
  `parent_last_name` varchar(255) NOT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `program` enum('EYP','PYP','IGCSE','O-Levels','A-Levels') DEFAULT NULL,
  `reg_date` date DEFAULT NULL,
  `cnic` varchar(255) NOT NULL,
  `refrence` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `walk_in_registration`
--

INSERT INTO `walk_in_registration` (`id`, `branch_id`, `student_first_name`, `student_middle_name`, `student_last_name`, `parent_first_name`, `parent_middle_name`, `parent_last_name`, `contact_number`, `program`, `reg_date`, `cnic`, `refrence`, `created_at`, `updated_at`) VALUES
(21, 4, 'new', NULL, 'enww', 'new', NULL, 'new', '2147483647', 'O-Levels', '2020-12-03', '2147483647', 'Not available', '2020-12-03 09:43:08', '2020-12-03 09:43:08'),
(22, 4, 'new', NULL, 'enww', 'new', NULL, 'new', '2147483647', 'PYP', '2020-12-02', '2147483647', 'Not available', '2020-12-03 09:43:08', '2020-12-03 09:43:08'),
(23, 4, 'latest', NULL, 'enww', 'new', NULL, 'new', '2147483647', 'PYP', '2020-11-13', '2147483647', 'Not available', '2020-12-03 09:43:08', '2020-12-03 09:43:08'),
(24, 4, 'added', NULL, 'enww', 'new', NULL, 'new', '2147483647', 'O-Levels', '2020-11-25', '2147483647', 'Not available', '2020-12-03 09:43:08', '2020-12-03 09:43:08'),
(25, 4, 'latest', NULL, 'enww', 'new', NULL, 'new', '04236305505', 'A-Levels', '2020-11-13', '3520136250471', 'Not availableNot availableNot availableNot available', '2020-12-03 09:43:08', '2020-12-03 09:43:08'),
(26, 4, 'latest', NULL, 'enww', 'new', NULL, 'new', '2147483647', 'A-Levels', '2020-09-13', '2147483647', 'Not available', '2020-12-03 09:43:08', '2020-12-03 09:43:08'),
(27, 4, 'usaam', NULL, 'irshad', 'parent', NULL, 'parent', '99999999999', 'O-Levels', '2020-12-04', '9999999999999', 'From Social Media', '2020-12-04 09:49:26', '2020-12-04 09:49:26'),
(28, 4, 'Ahmed', 'Amjad', 'Khan', 'Amjad', NULL, 'Khan', '222222222', 'EYP', '2021-03-01', '222222222', 'asdasdasd', '2021-03-01 11:11:06', '2021-03-01 11:11:06'),
(29, 4, 'Test', 'Demo', 'Demio', 'Test Ddmo F', 'Fq', 'dxdx', '23233223233', 'EYP', '2021-03-01', '2121223233232', 'Lahore', '2021-03-01 11:14:27', '2021-03-01 11:14:27'),
(30, 4, 'ali', 'raza', 'khan', 'raza', 'khan', 'm', '0309756554', 'PYP', '2021-03-02', '23232323323', 'news', '2021-03-02 15:29:56', '2021-03-02 15:29:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academics_groups`
--
ALTER TABLE `academics_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academics_groups_program_id_foreign` (`program_id`);

--
-- Indexes for table `academics_group_courses`
--
ALTER TABLE `academics_group_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academics_group_courses_academics_group_id_foreign` (`academics_group_id`),
  ADD KEY `academics_group_courses_course_id_foreign` (`course_id`);

--
-- Indexes for table `active_sessions`
--
ALTER TABLE `active_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active_sessions_session_id_foreign` (`session_id`),
  ADD KEY `active_sessions_board_id_foreign` (`board_id`),
  ADD KEY `active_sessions_branch_id_foreign` (`branch_id`),
  ADD KEY `active_sessions_program_id_foreign` (`program_id`),
  ADD KEY `active_sessions_class_id_foreign` (`class_id`);

--
-- Indexes for table `active_session_sections`
--
ALTER TABLE `active_session_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active_session_sections_active_session_id_foreign` (`active_session_id`),
  ADD KEY `active_session_sections_section_id_foreign` (`section_id`);

--
-- Indexes for table `active_session_students`
--
ALTER TABLE `active_session_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active_session_students_academics_group_id_foreign` (`academics_group_id`),
  ADD KEY `active_session_students_active_session_section_id_foreign` (`active_session_section_id`),
  ADD KEY `active_session_students_student_id_foreign` (`student_id`);

--
-- Indexes for table `active_session_teachers`
--
ALTER TABLE `active_session_teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active_session_teachers_active_session_section_id_foreign` (`active_session_section_id`),
  ADD KEY `active_session_teachers_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `agendas`
--
ALTER TABLE `agendas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `approve_agendas`
--
ALTER TABLE `approve_agendas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `approve_agendas_agenda_request_id_foreign` (`agenda_request_id`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assets_asset_type_id_foreign` (`asset_type_id`),
  ADD KEY `assets_created_by_foreign` (`created_by`),
  ADD KEY `assets_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `assign_active_session_course_lab`
--
ALTER TABLE `assign_active_session_course_lab`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assign_active_session_course_lab_active_session_id_foreign` (`active_session_id`),
  ADD KEY `assign_active_session_course_lab_course_id_foreign` (`course_id`),
  ADD KEY `assign_active_session_course_lab_lab_id_foreign` (`lab_id`),
  ADD KEY `assign_active_session_course_lab_created_by_foreign` (`created_by`),
  ADD KEY `assign_active_session_course_lab_updated_by_foreign` (`updated_by`),
  ADD KEY `assign_active_session_course_lab_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `assign_class_section`
--
ALTER TABLE `assign_class_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assign_class_section_board_program_class_id_foreign` (`board_program_class_id`),
  ADD KEY `assign_class_section_section_id_foreign` (`section_id`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attendances_session_id_foreign` (`session_id`),
  ADD KEY `attendances_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `attendance_masters`
--
ALTER TABLE `attendance_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance_statuses`
--
ALTER TABLE `attendance_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `boards_created_by_foreign` (`created_by`),
  ADD KEY `boards_updated_by_foreign` (`updated_by`),
  ADD KEY `boards_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `board_branches`
--
ALTER TABLE `board_branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_branches_board_id_foreign` (`board_id`),
  ADD KEY `board_branches_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `board_programs`
--
ALTER TABLE `board_programs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_programs_board_id_foreign` (`board_id`),
  ADD KEY `board_programs_program_id_foreign` (`program_id`);

--
-- Indexes for table `board_program_class`
--
ALTER TABLE `board_program_class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_program_class_board_id_foreign` (`board_id`),
  ADD KEY `board_program_class_program_id_foreign` (`program_id`),
  ADD KEY `board_program_class_class_id_foreign` (`class_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_issues`
--
ALTER TABLE `book_issues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_masters`
--
ALTER TABLE `book_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_status`
--
ALTER TABLE `book_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch_board_program`
--
ALTER TABLE `branch_board_program`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branch_board_program_branch_id_foreign` (`branch_id`),
  ADD KEY `branch_board_program_board_id_foreign` (`board_id`),
  ADD KEY `branch_board_program_program_id_foreign` (`program_id`);

--
-- Indexes for table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buildings_created_by_foreign` (`created_by`),
  ADD KEY `buildings_updated_by_foreign` (`updated_by`),
  ADD KEY `buildings_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `candidate_form`
--
ALTER TABLE `candidate_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidate_form_job_post_id_foreign` (`job_post_id`),
  ADD KEY `candidate_form_created_by_foreign` (`created_by`),
  ADD KEY `candidate_form_updated_by_foreign` (`updated_by`),
  ADD KEY `candidate_form_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classes_created_by_foreign` (`created_by`),
  ADD KEY `classes_updated_by_foreign` (`updated_by`),
  ADD KEY `classes_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `class_time_tables`
--
ALTER TABLE `class_time_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_time_tables_time_table_id_foreign` (`time_table_id`),
  ADD KEY `class_time_tables_subject_id_foreign` (`subject_id`),
  ADD KEY `class_time_tables_teacher_id_foreign` (`teacher_id`),
  ADD KEY `class_time_tables_created_by_foreign` (`created_by`),
  ADD KEY `class_time_tables_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `compulsory_courses`
--
ALTER TABLE `compulsory_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `compulsory_courses_program_id_foreign` (`program_id`),
  ADD KEY `compulsory_courses_course_id_foreign` (`course_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curriculum`
--
ALTER TABLE `curriculum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_types`
--
ALTER TABLE `discount_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discount_types_created_by_foreign` (`created_by`),
  ADD KEY `discount_types_updated_by_foreign` (`updated_by`),
  ADD KEY `discount_types_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_branch_id_foreign` (`branch_id`),
  ADD KEY `documents_member_id_foreign` (`member_id`),
  ADD KEY `documents_created_by_foreign` (`created_by`),
  ADD KEY `documents_updated_by_foreign` (`updated_by`),
  ADD KEY `documents_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_academic_year`
--
ALTER TABLE `erp_academic_year`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_academic_year_session_id_foreign` (`session_id`);

--
-- Indexes for table `erp_account_types`
--
ALTER TABLE `erp_account_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_account_types_created_by_foreign` (`created_by`),
  ADD KEY `erp_account_types_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_account_types_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_asset_types`
--
ALTER TABLE `erp_asset_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_asset_types_created_by_foreign` (`created_by`),
  ADD KEY `erp_asset_types_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `erp_attendances`
--
ALTER TABLE `erp_attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_attendance_masters`
--
ALTER TABLE `erp_attendance_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_attendance_statuses`
--
ALTER TABLE `erp_attendance_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_bankdetail`
--
ALTER TABLE `erp_bankdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_banks`
--
ALTER TABLE `erp_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_branches`
--
ALTER TABLE `erp_branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_branches_created_by_foreign` (`created_by`),
  ADD KEY `erp_branches_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_branches_deleted_by_foreign` (`deleted_by`),
  ADD KEY `erp_branches_company_id_foreign` (`company_id`),
  ADD KEY `erp_branches_city_id_foreign` (`city_id`),
  ADD KEY `erp_branches_country_id_foreign` (`country_id`);

--
-- Indexes for table `erp_branch_users`
--
ALTER TABLE `erp_branch_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_branch_users_user_id_foreign` (`user_id`),
  ADD KEY `erp_branch_users_branch_id_foreign` (`branch_id`),
  ADD KEY `erp_branch_users_created_by_foreign` (`created_by`),
  ADD KEY `erp_branch_users_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_branch_users_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_buildings`
--
ALTER TABLE `erp_buildings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_candidate_score`
--
ALTER TABLE `erp_candidate_score`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_candidate_score_candidate_form_id_foreign` (`candidate_form_id`),
  ADD KEY `erp_candidate_score_schedule_id_foreign` (`schedule_id`),
  ADD KEY `erp_candidate_score_created_by_foreign` (`created_by`),
  ADD KEY `erp_candidate_score_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_candidate_score_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_category_policies`
--
ALTER TABLE `erp_category_policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_child_benefit`
--
ALTER TABLE `erp_child_benefit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_city`
--
ALTER TABLE `erp_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_code_of_conducts`
--
ALTER TABLE `erp_code_of_conducts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_code_of_conducts_category_policy_id_foreign` (`category_policy_id`);

--
-- Indexes for table `erp_companies`
--
ALTER TABLE `erp_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_companies_created_by_foreign` (`created_by`),
  ADD KEY `erp_companies_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_companies_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_country`
--
ALTER TABLE `erp_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_currencies`
--
ALTER TABLE `erp_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_customers`
--
ALTER TABLE `erp_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_c_invoice`
--
ALTER TABLE `erp_c_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_databank_comment`
--
ALTER TABLE `erp_databank_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_departments`
--
ALTER TABLE `erp_departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_departments_created_by_foreign` (`created_by`),
  ADD KEY `erp_departments_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_departments_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_duty_calculation`
--
ALTER TABLE `erp_duty_calculation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_duty_calculation_created_by_foreign` (`created_by`),
  ADD KEY `erp_duty_calculation_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_duty_calculation_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_entries`
--
ALTER TABLE `erp_entries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_entry_items`
--
ALTER TABLE `erp_entry_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_entry_items_entry_type_id_foreign` (`entry_type_id`),
  ADD KEY `erp_entry_items_entry_id_foreign` (`entry_id`),
  ADD KEY `erp_entry_items_ledger_id_foreign` (`ledger_id`),
  ADD KEY `erp_entry_items_created_by_foreign` (`created_by`),
  ADD KEY `erp_entry_items_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_entry_items_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_entry_types`
--
ALTER TABLE `erp_entry_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_fee_head`
--
ALTER TABLE `erp_fee_head`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_fee_head_created_by_foreign` (`created_by`),
  ADD KEY `erp_fee_head_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_fee_head_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_fee_section`
--
ALTER TABLE `erp_fee_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_financial_years`
--
ALTER TABLE `erp_financial_years`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_financial_years_created_by_foreign` (`created_by`),
  ADD KEY `erp_financial_years_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_financial_years_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_fixedassets`
--
ALTER TABLE `erp_fixedassets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_goodreceipt`
--
ALTER TABLE `erp_goodreceipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_goodsissudetail`
--
ALTER TABLE `erp_goodsissudetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_goodsissue`
--
ALTER TABLE `erp_goodsissue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_goodsreceiptdetail`
--
ALTER TABLE `erp_goodsreceiptdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_gratuity`
--
ALTER TABLE `erp_gratuity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_gratuity_user_id_foreign` (`user_id`),
  ADD KEY `erp_gratuity_created_by_foreign` (`created_by`),
  ADD KEY `erp_gratuity_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_gratuity_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_groups`
--
ALTER TABLE `erp_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_groups_account_type_id_foreign` (`account_type_id`),
  ADD KEY `erp_groups_created_by_foreign` (`created_by`),
  ADD KEY `erp_groups_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_groups_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_inventorytransfer`
--
ALTER TABLE `erp_inventorytransfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_inventorytransferdetail`
--
ALTER TABLE `erp_inventorytransferdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_invoice_details`
--
ALTER TABLE `erp_invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_invoice_details_salesinvoice_id_foreign` (`salesinvoice_id`),
  ADD KEY `erp_invoice_details_created_by_foreign` (`created_by`),
  ADD KEY `erp_invoice_details_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_invoice_details_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_invoice_services`
--
ALTER TABLE `erp_invoice_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_invoice_services_salesinvoice_id_foreign` (`salesinvoice_id`),
  ADD KEY `erp_invoice_services_created_by_foreign` (`created_by`),
  ADD KEY `erp_invoice_services_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_invoice_services_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_lcbank`
--
ALTER TABLE `erp_lcbank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_lcdoler`
--
ALTER TABLE `erp_lcdoler`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_lcduty`
--
ALTER TABLE `erp_lcduty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_ledgers`
--
ALTER TABLE `erp_ledgers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_ledgers_account_type_id_foreign` (`account_type_id`),
  ADD KEY `erp_ledgers_branch_id_foreign` (`branch_id`),
  ADD KEY `erp_ledgers_created_by_foreign` (`created_by`),
  ADD KEY `erp_ledgers_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_ledgers_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_model_has_permissions`
--
ALTER TABLE `erp_model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `erp_model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `erp_model_has_roles`
--
ALTER TABLE `erp_model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `erp_model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `erp_orderdetail`
--
ALTER TABLE `erp_orderdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_orders`
--
ALTER TABLE `erp_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_order_product_serial`
--
ALTER TABLE `erp_order_product_serial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_order_product_serial_created_by_foreign` (`created_by`),
  ADD KEY `erp_order_product_serial_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_order_product_serial_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_password_resets`
--
ALTER TABLE `erp_password_resets`
  ADD KEY `erp_password_resets_email_index` (`email`);

--
-- Indexes for table `erp_payees_amounts`
--
ALTER TABLE `erp_payees_amounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_payees_amounts_payee_id_foreign` (`payee_id`),
  ADD KEY `erp_payees_amounts_building_id_foreign` (`building_id`);

--
-- Indexes for table `erp_payee_lists`
--
ALTER TABLE `erp_payee_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_payment_methods`
--
ALTER TABLE `erp_payment_methods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_payment_methods_created_by_foreign` (`created_by`),
  ADD KEY `erp_payment_methods_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_payment_methods_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_performastock`
--
ALTER TABLE `erp_performastock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_permissions`
--
ALTER TABLE `erp_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_permission_role`
--
ALTER TABLE `erp_permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_policies`
--
ALTER TABLE `erp_policies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_policies_category_policy_id_foreign` (`category_policy_id`);

--
-- Indexes for table `erp_ponumber`
--
ALTER TABLE `erp_ponumber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_program`
--
ALTER TABLE `erp_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_regions`
--
ALTER TABLE `erp_regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_regions_created_by_foreign` (`created_by`),
  ADD KEY `erp_regions_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_regions_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_releasestock`
--
ALTER TABLE `erp_releasestock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_roles`
--
ALTER TABLE `erp_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_role_has_permissions`
--
ALTER TABLE `erp_role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `erp_role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `erp_sales`
--
ALTER TABLE `erp_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_salesdetial`
--
ALTER TABLE `erp_salesdetial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_salesinvoice`
--
ALTER TABLE `erp_salesinvoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_salesinvoice_created_by_foreign` (`created_by`),
  ADD KEY `erp_salesinvoice_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_salesinvoice_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_sale_return`
--
ALTER TABLE `erp_sale_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_sale_return_invoice_id_foreign` (`invoice_id`),
  ADD KEY `erp_sale_return_created_by_foreign` (`created_by`),
  ADD KEY `erp_sale_return_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_sale_return_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_sale_return_detail`
--
ALTER TABLE `erp_sale_return_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_sale_return_detail_sale_return_id_foreign` (`sale_return_id`),
  ADD KEY `erp_sale_return_detail_created_by_foreign` (`created_by`),
  ADD KEY `erp_sale_return_detail_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_sale_return_detail_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_settings`
--
ALTER TABLE `erp_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_settings_created_by_foreign` (`created_by`),
  ADD KEY `erp_settings_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_settings_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_stockitems`
--
ALTER TABLE `erp_stockitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_suppliers`
--
ALTER TABLE `erp_suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_tax_settings`
--
ALTER TABLE `erp_tax_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_tax_settings_created_by_foreign` (`created_by`),
  ADD KEY `erp_tax_settings_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_tax_settings_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_territory`
--
ALTER TABLE `erp_territory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_territory_created_by_foreign` (`created_by`),
  ADD KEY `erp_territory_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_territory_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `erp_users`
--
ALTER TABLE `erp_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `erp_users_email_unique` (`email`);

--
-- Indexes for table `erp_workdays`
--
ALTER TABLE `erp_workdays`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_workdays_applicable_to_foreign` (`applicable_to`),
  ADD KEY `erp_workdays_created_by_foreign` (`created_by`),
  ADD KEY `erp_workdays_updated_by_foreign` (`updated_by`),
  ADD KEY `erp_workdays_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `event_models`
--
ALTER TABLE `event_models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_time_tables`
--
ALTER TABLE `event_time_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_time_tables_time_table_id_foreign` (`time_table_id`),
  ADD KEY `event_time_tables_created_by_foreign` (`created_by`),
  ADD KEY `event_time_tables_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_time_tables`
--
ALTER TABLE `exam_time_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_time_tables_time_table_id_foreign` (`time_table_id`),
  ADD KEY `exam_time_tables_created_by_foreign` (`created_by`),
  ADD KEY `exam_time_tables_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_collections`
--
ALTER TABLE `fee_collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_heads`
--
ALTER TABLE `fee_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_masters`
--
ALTER TABLE `fee_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_master_basics`
--
ALTER TABLE `fee_master_basics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_scholarship_defination`
--
ALTER TABLE `fee_scholarship_defination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_scholarship_details`
--
ALTER TABLE `fee_scholarship_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_sections`
--
ALTER TABLE `fee_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structures`
--
ALTER TABLE `fee_structures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_terms`
--
ALTER TABLE `fee_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_types`
--
ALTER TABLE `fee_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_events`
--
ALTER TABLE `group_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_event_students`
--
ALTER TABLE `group_event_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_event_students_group_event_id_foreign` (`group_event_id`),
  ADD KEY `group_event_students_student_id_foreign` (`student_id`);

--
-- Indexes for table `guardian_details`
--
ALTER TABLE `guardian_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `idcards`
--
ALTER TABLE `idcards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_post`
--
ALTER TABLE `job_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_post_job_post_request_id_foreign` (`job_post_request_id`),
  ADD KEY `job_post_created_by_foreign` (`created_by`),
  ADD KEY `job_post_updated_by_foreign` (`updated_by`),
  ADD KEY `job_post_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `job_post_request`
--
ALTER TABLE `job_post_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_post_request_subject_id_foreign` (`subject_id`),
  ADD KEY `job_post_request_created_by_foreign` (`created_by`),
  ADD KEY `job_post_request_updated_by_foreign` (`updated_by`),
  ADD KEY `job_post_request_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `labs`
--
ALTER TABLE `labs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `labs_created_by_foreign` (`created_by`),
  ADD KEY `labs_updated_by_foreign` (`updated_by`),
  ADD KEY `labs_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `lab_time_tables`
--
ALTER TABLE `lab_time_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lab_time_tables_time_table_id_foreign` (`time_table_id`),
  ADD KEY `lab_time_tables_created_by_foreign` (`created_by`),
  ADD KEY `lab_time_tables_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `library_circulation`
--
ALTER TABLE `library_circulation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library_members`
--
ALTER TABLE `library_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_detail`
--
ALTER TABLE `login_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_detail_start_time_unique` (`start_time`),
  ADD KEY `login_detail_user_id_foreign` (`user_id`),
  ADD KEY `login_detail_created_by_foreign` (`created_by`),
  ADD KEY `login_detail_updated_by_foreign` (`updated_by`),
  ADD KEY `login_detail_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `login_details`
--
ALTER TABLE `login_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login_details_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parent_details`
--
ALTER TABLE `parent_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_details_students_id_foreign` (`students_id`);

--
-- Indexes for table `payment_settings`
--
ALTER TABLE `payment_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `programs_created_by_foreign` (`created_by`),
  ADD KEY `programs_updated_by_foreign` (`updated_by`),
  ADD KEY `programs_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `program_terms`
--
ALTER TABLE `program_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `program_terms_program_id_foreign` (`program_id`),
  ADD KEY `program_terms_term_id_foreign` (`term_id`);

--
-- Indexes for table `resigns`
--
ALTER TABLE `resigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rooms_building_id_foreign` (`building_id`),
  ADD KEY `rooms_created_by_foreign` (`created_by`),
  ADD KEY `rooms_updated_by_foreign` (`updated_by`),
  ADD KEY `rooms_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `schedule_candidate_form_id_foreign` (`candidate_form_id`),
  ADD KEY `schedule_branch_id_foreign` (`branch_id`),
  ADD KEY `schedule_created_by_foreign` (`created_by`),
  ADD KEY `schedule_updated_by_foreign` (`updated_by`),
  ADD KEY `schedule_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sections_created_by_foreign` (`created_by`),
  ADD KEY `sections_updated_by_foreign` (`updated_by`),
  ADD KEY `sections_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_created_by_foreign` (`created_by`),
  ADD KEY `sessions_updated_by_foreign` (`updated_by`),
  ADD KEY `sessions_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `sms_settings`
--
ALTER TABLE `sms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `staff_reg_no_unique` (`reg_no`),
  ADD UNIQUE KEY `staff_email_unique` (`email`),
  ADD UNIQUE KEY `staff_cnic_unique` (`cnic`),
  ADD KEY `staff_user_id_foreign` (`user_id`),
  ADD KEY `staff_branch_id_foreign` (`branch_id`),
  ADD KEY `staff_created_by_foreign` (`created_by`),
  ADD KEY `staff_updated_by_foreign` (`updated_by`),
  ADD KEY `staff_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `staff_transfer_branches`
--
ALTER TABLE `staff_transfer_branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `std_active_s_elecive`
--
ALTER TABLE `std_active_s_elecive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `std_active_s_elecive_active_session_student_id_foreign` (`active_session_student_id`),
  ADD KEY `std_active_s_elecive_course_id_foreign` (`course_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_fee_settings`
--
ALTER TABLE `student_fee_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_status`
--
ALTER TABLE `student_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `syllabi`
--
ALTER TABLE `syllabi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `syllabus_heads`
--
ALTER TABLE `syllabus_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_classes`
--
ALTER TABLE `teacher_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terminations`
--
ALTER TABLE `terminations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `terms_academic_year_id_foreign` (`academic_year_id`),
  ADD KEY `terms_created_by_foreign` (`created_by`),
  ADD KEY `terms_updated_by_foreign` (`updated_by`),
  ADD KEY `terms_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `timetable_categories`
--
ALTER TABLE `timetable_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `timetable_categories_created_by_foreign` (`created_by`),
  ADD KEY `timetable_categories_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `time_tables`
--
ALTER TABLE `time_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `time_tables_branch_id_foreign` (`branch_id`),
  ADD KEY `time_tables_created_by_foreign` (`created_by`),
  ADD KEY `time_tables_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainees`
--
ALTER TABLE `trainees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trainees_training_id_foreign` (`training_id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_planner`
--
ALTER TABLE `unit_planner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploaded_documents`
--
ALTER TABLE `uploaded_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_pemissions`
--
ALTER TABLE `user_pemissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `walk_in_registration`
--
ALTER TABLE `walk_in_registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academics_groups`
--
ALTER TABLE `academics_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `academics_group_courses`
--
ALTER TABLE `academics_group_courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `active_sessions`
--
ALTER TABLE `active_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `active_session_sections`
--
ALTER TABLE `active_session_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `active_session_students`
--
ALTER TABLE `active_session_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `active_session_teachers`
--
ALTER TABLE `active_session_teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `agendas`
--
ALTER TABLE `agendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `approve_agendas`
--
ALTER TABLE `approve_agendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assign_active_session_course_lab`
--
ALTER TABLE `assign_active_session_course_lab`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `assign_class_section`
--
ALTER TABLE `assign_class_section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendance_masters`
--
ALTER TABLE `attendance_masters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendance_statuses`
--
ALTER TABLE `attendance_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `boards`
--
ALTER TABLE `boards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `board_branches`
--
ALTER TABLE `board_branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `board_programs`
--
ALTER TABLE `board_programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `board_program_class`
--
ALTER TABLE `board_program_class`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `book_categories`
--
ALTER TABLE `book_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_issues`
--
ALTER TABLE `book_issues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_masters`
--
ALTER TABLE `book_masters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `book_status`
--
ALTER TABLE `book_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `branch_board_program`
--
ALTER TABLE `branch_board_program`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `buildings`
--
ALTER TABLE `buildings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `candidate_form`
--
ALTER TABLE `candidate_form`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `class_time_tables`
--
ALTER TABLE `class_time_tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `compulsory_courses`
--
ALTER TABLE `compulsory_courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `curriculum`
--
ALTER TABLE `curriculum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `discount_types`
--
ALTER TABLE `discount_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `erp_academic_year`
--
ALTER TABLE `erp_academic_year`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `erp_account_types`
--
ALTER TABLE `erp_account_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `erp_asset_types`
--
ALTER TABLE `erp_asset_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_attendances`
--
ALTER TABLE `erp_attendances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_attendance_masters`
--
ALTER TABLE `erp_attendance_masters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_attendance_statuses`
--
ALTER TABLE `erp_attendance_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_bankdetail`
--
ALTER TABLE `erp_bankdetail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_banks`
--
ALTER TABLE `erp_banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_branches`
--
ALTER TABLE `erp_branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `erp_branch_users`
--
ALTER TABLE `erp_branch_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `erp_buildings`
--
ALTER TABLE `erp_buildings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_candidate_score`
--
ALTER TABLE `erp_candidate_score`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `erp_category_policies`
--
ALTER TABLE `erp_category_policies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `erp_child_benefit`
--
ALTER TABLE `erp_child_benefit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_city`
--
ALTER TABLE `erp_city`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `erp_code_of_conducts`
--
ALTER TABLE `erp_code_of_conducts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `erp_companies`
--
ALTER TABLE `erp_companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `erp_country`
--
ALTER TABLE `erp_country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `erp_currencies`
--
ALTER TABLE `erp_currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `erp_customers`
--
ALTER TABLE `erp_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_c_invoice`
--
ALTER TABLE `erp_c_invoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_databank_comment`
--
ALTER TABLE `erp_databank_comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_departments`
--
ALTER TABLE `erp_departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_duty_calculation`
--
ALTER TABLE `erp_duty_calculation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_entries`
--
ALTER TABLE `erp_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_entry_items`
--
ALTER TABLE `erp_entry_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_entry_types`
--
ALTER TABLE `erp_entry_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `erp_fee_head`
--
ALTER TABLE `erp_fee_head`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `erp_fee_section`
--
ALTER TABLE `erp_fee_section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `erp_financial_years`
--
ALTER TABLE `erp_financial_years`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `erp_fixedassets`
--
ALTER TABLE `erp_fixedassets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_goodreceipt`
--
ALTER TABLE `erp_goodreceipt`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_goodsissudetail`
--
ALTER TABLE `erp_goodsissudetail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_goodsissue`
--
ALTER TABLE `erp_goodsissue`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_goodsreceiptdetail`
--
ALTER TABLE `erp_goodsreceiptdetail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_gratuity`
--
ALTER TABLE `erp_gratuity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_groups`
--
ALTER TABLE `erp_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=328;

--
-- AUTO_INCREMENT for table `erp_inventorytransfer`
--
ALTER TABLE `erp_inventorytransfer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_inventorytransferdetail`
--
ALTER TABLE `erp_inventorytransferdetail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_invoice_details`
--
ALTER TABLE `erp_invoice_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_invoice_services`
--
ALTER TABLE `erp_invoice_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_lcbank`
--
ALTER TABLE `erp_lcbank`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_lcdoler`
--
ALTER TABLE `erp_lcdoler`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_lcduty`
--
ALTER TABLE `erp_lcduty`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_ledgers`
--
ALTER TABLE `erp_ledgers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `erp_orderdetail`
--
ALTER TABLE `erp_orderdetail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_orders`
--
ALTER TABLE `erp_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_order_product_serial`
--
ALTER TABLE `erp_order_product_serial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_payees_amounts`
--
ALTER TABLE `erp_payees_amounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_payee_lists`
--
ALTER TABLE `erp_payee_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_payment_methods`
--
ALTER TABLE `erp_payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_performastock`
--
ALTER TABLE `erp_performastock`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_permissions`
--
ALTER TABLE `erp_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT for table `erp_permission_role`
--
ALTER TABLE `erp_permission_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_policies`
--
ALTER TABLE `erp_policies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `erp_ponumber`
--
ALTER TABLE `erp_ponumber`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_program`
--
ALTER TABLE `erp_program`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_regions`
--
ALTER TABLE `erp_regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_releasestock`
--
ALTER TABLE `erp_releasestock`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_roles`
--
ALTER TABLE `erp_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `erp_sales`
--
ALTER TABLE `erp_sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_salesdetial`
--
ALTER TABLE `erp_salesdetial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_salesinvoice`
--
ALTER TABLE `erp_salesinvoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_sale_return`
--
ALTER TABLE `erp_sale_return`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_sale_return_detail`
--
ALTER TABLE `erp_sale_return_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_settings`
--
ALTER TABLE `erp_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `erp_stockitems`
--
ALTER TABLE `erp_stockitems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_suppliers`
--
ALTER TABLE `erp_suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_tax_settings`
--
ALTER TABLE `erp_tax_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_territory`
--
ALTER TABLE `erp_territory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_users`
--
ALTER TABLE `erp_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `erp_workdays`
--
ALTER TABLE `erp_workdays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_models`
--
ALTER TABLE `event_models`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_time_tables`
--
ALTER TABLE `event_time_tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_time_tables`
--
ALTER TABLE `exam_time_tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `fee_collections`
--
ALTER TABLE `fee_collections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `fee_heads`
--
ALTER TABLE `fee_heads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `fee_masters`
--
ALTER TABLE `fee_masters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `fee_master_basics`
--
ALTER TABLE `fee_master_basics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `fee_scholarship_defination`
--
ALTER TABLE `fee_scholarship_defination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fee_scholarship_details`
--
ALTER TABLE `fee_scholarship_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `fee_sections`
--
ALTER TABLE `fee_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `fee_structures`
--
ALTER TABLE `fee_structures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=320;

--
-- AUTO_INCREMENT for table `fee_terms`
--
ALTER TABLE `fee_terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_types`
--
ALTER TABLE `fee_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_events`
--
ALTER TABLE `group_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_event_students`
--
ALTER TABLE `group_event_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guardian_details`
--
ALTER TABLE `guardian_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `idcards`
--
ALTER TABLE `idcards`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `job_post`
--
ALTER TABLE `job_post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `job_post_request`
--
ALTER TABLE `job_post_request`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `labs`
--
ALTER TABLE `labs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lab_time_tables`
--
ALTER TABLE `lab_time_tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `library_circulation`
--
ALTER TABLE `library_circulation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `library_members`
--
ALTER TABLE `library_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `login_detail`
--
ALTER TABLE `login_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_details`
--
ALTER TABLE `login_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `parent_details`
--
ALTER TABLE `parent_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `payment_settings`
--
ALTER TABLE `payment_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=583;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `program_terms`
--
ALTER TABLE `program_terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resigns`
--
ALTER TABLE `resigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sms_settings`
--
ALTER TABLE `sms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `staff_transfer_branches`
--
ALTER TABLE `staff_transfer_branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `std_active_s_elecive`
--
ALTER TABLE `std_active_s_elecive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `student_fee_settings`
--
ALTER TABLE `student_fee_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `student_status`
--
ALTER TABLE `student_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `syllabi`
--
ALTER TABLE `syllabi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `syllabus_heads`
--
ALTER TABLE `syllabus_heads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `teacher_classes`
--
ALTER TABLE `teacher_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `terminations`
--
ALTER TABLE `terminations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `timetable_categories`
--
ALTER TABLE `timetable_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `time_tables`
--
ALTER TABLE `time_tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `todos`
--
ALTER TABLE `todos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `trainees`
--
ALTER TABLE `trainees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `unit_planner`
--
ALTER TABLE `unit_planner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `uploaded_documents`
--
ALTER TABLE `uploaded_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_pemissions`
--
ALTER TABLE `user_pemissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `walk_in_registration`
--
ALTER TABLE `walk_in_registration`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `academics_groups`
--
ALTER TABLE `academics_groups`
  ADD CONSTRAINT `academics_groups_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `academics_group_courses`
--
ALTER TABLE `academics_group_courses`
  ADD CONSTRAINT `academics_group_courses_academics_group_id_foreign` FOREIGN KEY (`academics_group_id`) REFERENCES `academics_groups` (`id`),
  ADD CONSTRAINT `academics_group_courses_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Constraints for table `active_sessions`
--
ALTER TABLE `active_sessions`
  ADD CONSTRAINT `active_sessions_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`),
  ADD CONSTRAINT `active_sessions_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`),
  ADD CONSTRAINT `active_sessions_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  ADD CONSTRAINT `active_sessions_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  ADD CONSTRAINT `active_sessions_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`);

--
-- Constraints for table `active_session_sections`
--
ALTER TABLE `active_session_sections`
  ADD CONSTRAINT `active_session_sections_active_session_id_foreign` FOREIGN KEY (`active_session_id`) REFERENCES `active_sessions` (`id`),
  ADD CONSTRAINT `active_session_sections_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`);

--
-- Constraints for table `active_session_students`
--
ALTER TABLE `active_session_students`
  ADD CONSTRAINT `active_session_students_academics_group_id_foreign` FOREIGN KEY (`academics_group_id`) REFERENCES `academics_groups` (`id`),
  ADD CONSTRAINT `active_session_students_active_session_section_id_foreign` FOREIGN KEY (`active_session_section_id`) REFERENCES `active_session_sections` (`id`),
  ADD CONSTRAINT `active_session_students_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `active_session_teachers`
--
ALTER TABLE `active_session_teachers`
  ADD CONSTRAINT `active_session_teachers_active_session_section_id_foreign` FOREIGN KEY (`active_session_section_id`) REFERENCES `active_session_sections` (`id`),
  ADD CONSTRAINT `active_session_teachers_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`);

--
-- Constraints for table `approve_agendas`
--
ALTER TABLE `approve_agendas`
  ADD CONSTRAINT `approve_agendas_agenda_request_id_foreign` FOREIGN KEY (`agenda_request_id`) REFERENCES `agendas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_asset_type_id_foreign` FOREIGN KEY (`asset_type_id`) REFERENCES `erp_asset_types` (`id`),
  ADD CONSTRAINT `assets_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `assets_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `assign_active_session_course_lab`
--
ALTER TABLE `assign_active_session_course_lab`
  ADD CONSTRAINT `assign_active_session_course_lab_active_session_id_foreign` FOREIGN KEY (`active_session_id`) REFERENCES `active_sessions` (`id`),
  ADD CONSTRAINT `assign_active_session_course_lab_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `assign_active_session_course_lab_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `assign_active_session_course_lab_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `assign_active_session_course_lab_lab_id_foreign` FOREIGN KEY (`lab_id`) REFERENCES `labs` (`id`),
  ADD CONSTRAINT `assign_active_session_course_lab_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `assign_class_section`
--
ALTER TABLE `assign_class_section`
  ADD CONSTRAINT `assign_class_section_board_program_class_id_foreign` FOREIGN KEY (`board_program_class_id`) REFERENCES `board_program_class` (`id`),
  ADD CONSTRAINT `assign_class_section_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`);

--
-- Constraints for table `attendances`
--
ALTER TABLE `attendances`
  ADD CONSTRAINT `attendances_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`),
  ADD CONSTRAINT `attendances_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`);

--
-- Constraints for table `boards`
--
ALTER TABLE `boards`
  ADD CONSTRAINT `boards_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `boards_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `boards_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `board_branches`
--
ALTER TABLE `board_branches`
  ADD CONSTRAINT `board_branches_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`),
  ADD CONSTRAINT `board_branches_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`);

--
-- Constraints for table `board_programs`
--
ALTER TABLE `board_programs`
  ADD CONSTRAINT `board_programs_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`),
  ADD CONSTRAINT `board_programs_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `board_program_class`
--
ALTER TABLE `board_program_class`
  ADD CONSTRAINT `board_program_class_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`),
  ADD CONSTRAINT `board_program_class_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  ADD CONSTRAINT `board_program_class_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `branch_board_program`
--
ALTER TABLE `branch_board_program`
  ADD CONSTRAINT `branch_board_program_board_id_foreign` FOREIGN KEY (`board_id`) REFERENCES `boards` (`id`),
  ADD CONSTRAINT `branch_board_program_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`),
  ADD CONSTRAINT `branch_board_program_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `buildings`
--
ALTER TABLE `buildings`
  ADD CONSTRAINT `buildings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `buildings_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `buildings_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `candidate_form`
--
ALTER TABLE `candidate_form`
  ADD CONSTRAINT `candidate_form_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `candidate_form_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `candidate_form_job_post_id_foreign` FOREIGN KEY (`job_post_id`) REFERENCES `job_post` (`id`),
  ADD CONSTRAINT `candidate_form_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `classes_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `classes_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `class_time_tables`
--
ALTER TABLE `class_time_tables`
  ADD CONSTRAINT `class_time_tables_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `class_time_tables_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `class_time_tables_teacher_id_foreign` FOREIGN KEY (`teacher_id`) REFERENCES `staff` (`id`),
  ADD CONSTRAINT `class_time_tables_time_table_id_foreign` FOREIGN KEY (`time_table_id`) REFERENCES `time_tables` (`id`),
  ADD CONSTRAINT `class_time_tables_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `compulsory_courses`
--
ALTER TABLE `compulsory_courses`
  ADD CONSTRAINT `compulsory_courses_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `compulsory_courses_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `discount_types`
--
ALTER TABLE `discount_types`
  ADD CONSTRAINT `discount_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `discount_types_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `discount_types_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`),
  ADD CONSTRAINT `documents_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `documents_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `documents_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `documents_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_academic_year`
--
ALTER TABLE `erp_academic_year`
  ADD CONSTRAINT `erp_academic_year_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`);

--
-- Constraints for table `erp_account_types`
--
ALTER TABLE `erp_account_types`
  ADD CONSTRAINT `erp_account_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_account_types_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_account_types_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_asset_types`
--
ALTER TABLE `erp_asset_types`
  ADD CONSTRAINT `erp_asset_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_asset_types_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_branches`
--
ALTER TABLE `erp_branches`
  ADD CONSTRAINT `erp_branches_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `erp_city` (`id`),
  ADD CONSTRAINT `erp_branches_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `erp_companies` (`id`),
  ADD CONSTRAINT `erp_branches_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `erp_country` (`id`),
  ADD CONSTRAINT `erp_branches_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_branches_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_branches_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_branch_users`
--
ALTER TABLE `erp_branch_users`
  ADD CONSTRAINT `erp_branch_users_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `erp_branch_users_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `erp_branch_users_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `erp_branch_users_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `erp_branch_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `erp_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `erp_candidate_score`
--
ALTER TABLE `erp_candidate_score`
  ADD CONSTRAINT `erp_candidate_score_candidate_form_id_foreign` FOREIGN KEY (`candidate_form_id`) REFERENCES `candidate_form` (`id`),
  ADD CONSTRAINT `erp_candidate_score_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_candidate_score_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_candidate_score_schedule_id_foreign` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`),
  ADD CONSTRAINT `erp_candidate_score_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_code_of_conducts`
--
ALTER TABLE `erp_code_of_conducts`
  ADD CONSTRAINT `erp_code_of_conducts_category_policy_id_foreign` FOREIGN KEY (`category_policy_id`) REFERENCES `erp_category_policies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `erp_companies`
--
ALTER TABLE `erp_companies`
  ADD CONSTRAINT `erp_companies_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_companies_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_companies_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_departments`
--
ALTER TABLE `erp_departments`
  ADD CONSTRAINT `erp_departments_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_departments_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_departments_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_duty_calculation`
--
ALTER TABLE `erp_duty_calculation`
  ADD CONSTRAINT `erp_duty_calculation_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_duty_calculation_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_duty_calculation_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_entry_items`
--
ALTER TABLE `erp_entry_items`
  ADD CONSTRAINT `erp_entry_items_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_entry_items_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_entry_items_entry_id_foreign` FOREIGN KEY (`entry_id`) REFERENCES `erp_entries` (`id`),
  ADD CONSTRAINT `erp_entry_items_entry_type_id_foreign` FOREIGN KEY (`entry_type_id`) REFERENCES `erp_entry_types` (`id`),
  ADD CONSTRAINT `erp_entry_items_ledger_id_foreign` FOREIGN KEY (`ledger_id`) REFERENCES `erp_ledgers` (`id`),
  ADD CONSTRAINT `erp_entry_items_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_fee_head`
--
ALTER TABLE `erp_fee_head`
  ADD CONSTRAINT `erp_fee_head_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_fee_head_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_fee_head_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_financial_years`
--
ALTER TABLE `erp_financial_years`
  ADD CONSTRAINT `erp_financial_years_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_financial_years_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_financial_years_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_gratuity`
--
ALTER TABLE `erp_gratuity`
  ADD CONSTRAINT `erp_gratuity_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_gratuity_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_gratuity_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_gratuity_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_groups`
--
ALTER TABLE `erp_groups`
  ADD CONSTRAINT `erp_groups_account_type_id_foreign` FOREIGN KEY (`account_type_id`) REFERENCES `erp_account_types` (`id`),
  ADD CONSTRAINT `erp_groups_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_groups_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_groups_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_invoice_details`
--
ALTER TABLE `erp_invoice_details`
  ADD CONSTRAINT `erp_invoice_details_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_invoice_details_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_invoice_details_salesinvoice_id_foreign` FOREIGN KEY (`salesinvoice_id`) REFERENCES `erp_salesinvoice` (`id`),
  ADD CONSTRAINT `erp_invoice_details_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_invoice_services`
--
ALTER TABLE `erp_invoice_services`
  ADD CONSTRAINT `erp_invoice_services_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_invoice_services_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_invoice_services_salesinvoice_id_foreign` FOREIGN KEY (`salesinvoice_id`) REFERENCES `erp_salesinvoice` (`id`),
  ADD CONSTRAINT `erp_invoice_services_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_ledgers`
--
ALTER TABLE `erp_ledgers`
  ADD CONSTRAINT `erp_ledgers_account_type_id_foreign` FOREIGN KEY (`account_type_id`) REFERENCES `erp_account_types` (`id`),
  ADD CONSTRAINT `erp_ledgers_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`),
  ADD CONSTRAINT `erp_ledgers_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_ledgers_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_ledgers_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_model_has_permissions`
--
ALTER TABLE `erp_model_has_permissions`
  ADD CONSTRAINT `erp_model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `erp_permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `erp_model_has_roles`
--
ALTER TABLE `erp_model_has_roles`
  ADD CONSTRAINT `erp_model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `erp_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `erp_order_product_serial`
--
ALTER TABLE `erp_order_product_serial`
  ADD CONSTRAINT `erp_order_product_serial_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_order_product_serial_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_order_product_serial_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_payees_amounts`
--
ALTER TABLE `erp_payees_amounts`
  ADD CONSTRAINT `erp_payees_amounts_building_id_foreign` FOREIGN KEY (`building_id`) REFERENCES `erp_buildings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `erp_payees_amounts_payee_id_foreign` FOREIGN KEY (`payee_id`) REFERENCES `erp_payee_lists` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `erp_payment_methods`
--
ALTER TABLE `erp_payment_methods`
  ADD CONSTRAINT `erp_payment_methods_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_payment_methods_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_payment_methods_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_policies`
--
ALTER TABLE `erp_policies`
  ADD CONSTRAINT `erp_policies_category_policy_id_foreign` FOREIGN KEY (`category_policy_id`) REFERENCES `erp_category_policies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `erp_regions`
--
ALTER TABLE `erp_regions`
  ADD CONSTRAINT `erp_regions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_regions_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_regions_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_role_has_permissions`
--
ALTER TABLE `erp_role_has_permissions`
  ADD CONSTRAINT `erp_role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `erp_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `erp_role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `erp_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `erp_salesinvoice`
--
ALTER TABLE `erp_salesinvoice`
  ADD CONSTRAINT `erp_salesinvoice_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_salesinvoice_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_salesinvoice_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_sale_return`
--
ALTER TABLE `erp_sale_return`
  ADD CONSTRAINT `erp_sale_return_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_sale_return_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_sale_return_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `erp_salesinvoice` (`id`),
  ADD CONSTRAINT `erp_sale_return_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_sale_return_detail`
--
ALTER TABLE `erp_sale_return_detail`
  ADD CONSTRAINT `erp_sale_return_detail_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_sale_return_detail_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_sale_return_detail_sale_return_id_foreign` FOREIGN KEY (`sale_return_id`) REFERENCES `erp_sale_return` (`id`),
  ADD CONSTRAINT `erp_sale_return_detail_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_settings`
--
ALTER TABLE `erp_settings`
  ADD CONSTRAINT `erp_settings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_settings_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_settings_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_tax_settings`
--
ALTER TABLE `erp_tax_settings`
  ADD CONSTRAINT `erp_tax_settings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_tax_settings_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_tax_settings_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_territory`
--
ALTER TABLE `erp_territory`
  ADD CONSTRAINT `erp_territory_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_territory_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_territory_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `erp_workdays`
--
ALTER TABLE `erp_workdays`
  ADD CONSTRAINT `erp_workdays_applicable_to_foreign` FOREIGN KEY (`applicable_to`) REFERENCES `erp_departments` (`id`),
  ADD CONSTRAINT `erp_workdays_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_workdays_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `erp_workdays_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `event_time_tables`
--
ALTER TABLE `event_time_tables`
  ADD CONSTRAINT `event_time_tables_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `event_time_tables_time_table_id_foreign` FOREIGN KEY (`time_table_id`) REFERENCES `time_tables` (`id`),
  ADD CONSTRAINT `event_time_tables_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `exam_time_tables`
--
ALTER TABLE `exam_time_tables`
  ADD CONSTRAINT `exam_time_tables_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `exam_time_tables_time_table_id_foreign` FOREIGN KEY (`time_table_id`) REFERENCES `time_tables` (`id`),
  ADD CONSTRAINT `exam_time_tables_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `group_event_students`
--
ALTER TABLE `group_event_students`
  ADD CONSTRAINT `group_event_students_group_event_id_foreign` FOREIGN KEY (`group_event_id`) REFERENCES `group_events` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `group_event_students_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `job_post`
--
ALTER TABLE `job_post`
  ADD CONSTRAINT `job_post_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `job_post_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `job_post_job_post_request_id_foreign` FOREIGN KEY (`job_post_request_id`) REFERENCES `job_post_request` (`id`),
  ADD CONSTRAINT `job_post_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `job_post_request`
--
ALTER TABLE `job_post_request`
  ADD CONSTRAINT `job_post_request_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `job_post_request_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `job_post_request_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `job_post_request_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `labs`
--
ALTER TABLE `labs`
  ADD CONSTRAINT `labs_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `labs_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `labs_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `lab_time_tables`
--
ALTER TABLE `lab_time_tables`
  ADD CONSTRAINT `lab_time_tables_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `lab_time_tables_time_table_id_foreign` FOREIGN KEY (`time_table_id`) REFERENCES `time_tables` (`id`),
  ADD CONSTRAINT `lab_time_tables_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `login_detail`
--
ALTER TABLE `login_detail`
  ADD CONSTRAINT `login_detail_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `login_detail_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `login_detail_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `login_detail_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `login_details`
--
ALTER TABLE `login_details`
  ADD CONSTRAINT `login_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `parent_details`
--
ALTER TABLE `parent_details`
  ADD CONSTRAINT `parent_details_students_id_foreign` FOREIGN KEY (`students_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `programs`
--
ALTER TABLE `programs`
  ADD CONSTRAINT `programs_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `programs_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `programs_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `program_terms`
--
ALTER TABLE `program_terms`
  ADD CONSTRAINT `program_terms_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  ADD CONSTRAINT `program_terms_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_building_id_foreign` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`id`),
  ADD CONSTRAINT `rooms_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `rooms_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `rooms_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `schedule_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`),
  ADD CONSTRAINT `schedule_candidate_form_id_foreign` FOREIGN KEY (`candidate_form_id`) REFERENCES `candidate_form` (`id`),
  ADD CONSTRAINT `schedule_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `schedule_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `schedule_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `sections_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `sections_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `sessions_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `sessions_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`),
  ADD CONSTRAINT `staff_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `staff_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `staff_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `staff_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `std_active_s_elecive`
--
ALTER TABLE `std_active_s_elecive`
  ADD CONSTRAINT `std_active_s_elecive_active_session_student_id_foreign` FOREIGN KEY (`active_session_student_id`) REFERENCES `active_session_students` (`id`),
  ADD CONSTRAINT `std_active_s_elecive_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Constraints for table `terms`
--
ALTER TABLE `terms`
  ADD CONSTRAINT `terms_academic_year_id_foreign` FOREIGN KEY (`academic_year_id`) REFERENCES `erp_academic_year` (`id`),
  ADD CONSTRAINT `terms_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `terms_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `terms_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `timetable_categories`
--
ALTER TABLE `timetable_categories`
  ADD CONSTRAINT `timetable_categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `timetable_categories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `time_tables`
--
ALTER TABLE `time_tables`
  ADD CONSTRAINT `time_tables_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `erp_branches` (`id`),
  ADD CONSTRAINT `time_tables_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `erp_users` (`id`),
  ADD CONSTRAINT `time_tables_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `erp_users` (`id`);

--
-- Constraints for table `trainees`
--
ALTER TABLE `trainees`
  ADD CONSTRAINT `trainees_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
