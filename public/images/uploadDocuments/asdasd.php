<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Models\Admin\Company;

class CompaniesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Companies',
            'name' => 'erp_companies_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_companies_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_companies_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_companies_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_companies_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_companies_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_companies_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'School Setting',
                'name' => 'erp_school_setting',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Hired HR',
                'name' => 'erp_hired_hr',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(1, 'web');
        $role->givePermissionTo('erp_companies_manage');
        $role->givePermissionTo('erp_companies_create');
        $role->givePermissionTo('erp_companies_edit');
        $role->givePermissionTo('erp_companies_destroy');
        $role->givePermissionTo('erp_companies_detail');
        $role->givePermissionTo('erp_companies_active');
        $role->givePermissionTo('erp_companies_inactive');

        $DFIT_role = Role::create(
            ['name' => 'DFIT_head'],
        );

        $DFIT_role->givePermissionTo('erp_companies_manage');
        $DFIT_role->givePermissionTo('erp_companies_create');
        $DFIT_role->givePermissionTo('erp_companies_edit');
        $DFIT_role->givePermissionTo('erp_companies_destroy');
        $DFIT_role->givePermissionTo('erp_companies_detail');
        $DFIT_role->givePermissionTo('erp_companies_active');
        $DFIT_role->givePermissionTo('erp_companies_inactive');        

        // Company::insert([
        //     [
        //         'name' => 'Roots IVY International School (pvt) Ltd',
        //         'address' => 'Wallayat Homes Chaklala Scheme III',
        //         'status' => 1,
        //         'created_by' => 1,
        //         'updated_by' => 1,
        //         'created_at' => \Carbon\Carbon::now(),
        //         'updated_at' => \Carbon\Carbon::now()
        //     ]
        // ]);
    }
}
