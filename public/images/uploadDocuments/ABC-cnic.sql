-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2020 at 07:00 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rootsivyinternational`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `company_id`, `name`, `branch_code`, `phone`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Roots IVY International School-PWD Campus', '4', '', 'Roots IVY Scheme III', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(2, 1, 'Roots IVY International School-Westridge Campus', '10', '', 'Roots IVY Scheme III Non IB Campus', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(3, 1, 'Roots IVY International School-F-8 Campus A Level', '25', '', 'Roots IVY Scheme III Matric Campus', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(4, 1, 'Roots IVY International School-River View Campus', '23', '', 'IVY River View IB Campus', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(5, 1, 'Roots IVY International School-Educational Complex Faisalabad', '26', '', 'Roots IVY PWD Campus', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(6, 1, 'Roots IVY International School-Cantt Campus-Bahawalpur', '11', '', 'Roots IVY Westridge Campus', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(7, 1, 'Roots IVY International School-DHA Phase-V Lahore', '22', '', 'IVY World School-Lahore', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(8, 1, 'Roots IVY International School-Signature School-G6/4 Islamabad', '28', '', 'Roots IVY Junior School - Bahawalpur', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(9, 1, 'Roots IVY International School-DHA Bahawalpur', '29', '', 'Roots IVY World Signature School-G6/4 Islamabad ', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(10, 2, 'IVY CMS-DHA Phase-II University Campus', '30', '', 'Roots IVY Signature School - DHA Bhawalpur', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(11, 2, 'IVY CMS-F8 Islamabad', '19', '', 'Roots IVY F-8 Campus A Level', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49'),
(12, 2, 'IVY CMS-University Campus Faisalabad', '27', '', 'Roots IVY Educational Complex Faisalabad', '1', '2020-09-09 23:16:49', '2020-09-09 23:16:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
