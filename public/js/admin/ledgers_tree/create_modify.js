function get_group_ledger(obj, id, level){
    var DID=$(obj).attr('val');
    var g=$(obj).parents('.accordion-group').find('.accordion-body');
    $.ajax({
        url:'get_ledger_tree/'+id,
        dataType:'JSON',
        success:function (data) {
            g.addClass('in').css('margin-left', '30px');
            $("#"+DID).html(data.data).addClass('good');
            color_level(DID, level);
        }
    });
}
function color_level(id, level){
    if(level==1){
        $("#"+id).find('.accordion-group').css('border-left','4px solid #65c465');
        $("#"+id).find('.accordion-toggle').addClass('text-success');
    }
    if(level==2){
        $("#"+id).find('.accordion-group').css('border-left','4px solid #98b3fa');
        $("#"+id).find('.accordion-toggle').addClass('text-info');
    }
    if(level==3){
        $("#"+id).find('.accordion-group').css('border-left','4px solid #a83266');
        $("#"+id).find('.accordion-toggle').addClass('text-warning');
    }
    if(level==4){
        $("#"+id).find('.accordion-group').css('border-left','4px solid #e0d7d7');
        $("#"+id).find('.accordion-toggle').addClass('text-danger');
    }
    if(level==5){
        $("#"+id).find('.accordion-group').css('border-left','4px solid #141706');
        $("#"+id).find('.accordion-toggle').addClass('text-secondary');
    }
}