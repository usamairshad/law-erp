
$(function(e) {
	'use strict'
	var chart_data;
	$.get("https://localhost/rootsivy_erp/api/test-chart-data", function(data, status){
		 chart_data = data
	}).then(function(){
		//Income Expense Company Roots IVY International School
		var chartdata1 = [{
			name: 'Income',
			type: 'bar',
			barMaxWidth: 20,
			data: chart_data.data['income']
		},  {
			name: 'Expense',
			type: 'bar',
			barMaxWidth: 20,
			data: chart_data.data['expenses']
		}];
		var chart11 = document.getElementById('echart20');
		var barChart11 = echarts.init(chart11);
		var option11 = {
			valueAxis: {
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				splitArea: {
					show: true,
					areaStyle: {
						color: ['rgba(171, 167, 167,0.2)']
					}
				},
				splitLine: {
					lineStyle: {
						color: ['rgba(171, 167, 167,0.2)']
					}
				}
			},
			grid: {
				top: '6',
				right: '0',
				bottom: '17',
				left: '25',
			},
			xAxis: {
				data: chart_data.data['branches'],
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				splitLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLabel: {
					fontSize: 10,
					color: '#5f6d7a'
				}
			},
			tooltip: {
				trigger: 'axis',
				position: ['35%', '32%'],
			},
			yAxis: {
				splitLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLabel: {
					fontSize: 10,
					color: '#5f6d7a'
				}
			},
			series: chartdata1,
			color: ['#285cf7', '#7987a1' ]
		};
	
		barChart11.setOption(option11);
	
	
	});
	});
//Get income / Expense Ivy College of Management Sciences

$(function(e) {
	'use strict'
	var chart_data;
	$.get("https://localhost/rootsivy_erp/api/test-chart-data-two", function(data, status){
		 chart_data = data
	}).then(function(){
		//Income Expense Company Roots IVY International School
		var chartdata1 = [{
			name: 'Income',
			type: 'bar',
			barMaxWidth: 20,
			data: chart_data.data['income']
		},  {
			name: 'Expense',
			type: 'bar',
			barMaxWidth: 20,
			data: chart_data.data['expenses']
		}];
		var chart11 = document.getElementById('echart10');
		var barChart11 = echarts.init(chart11);
		var option11 = {
			valueAxis: {
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				splitArea: {
					show: true,
					areaStyle: {
						color: ['rgba(171, 167, 167,0.2)']
					}
				},
				splitLine: {
					lineStyle: {
						color: ['rgba(171, 167, 167,0.2)']
					}
				}
			},
			grid: {
				top: '6',
				right: '0',
				bottom: '17',
				left: '25',
			},
			xAxis: {
				data: chart_data.data['branches'],
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				splitLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLabel: {
					fontSize: 10,
					color: '#5f6d7a'
				}
			},
			tooltip: {
				trigger: 'axis',
				position: ['35%', '32%'],
			},
			yAxis: {
				splitLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLabel: {
					fontSize: 10,
					color: '#5f6d7a'
				}
			},
			series: chartdata1,
			color: ['#285cf7', '#7987a1' ]
		};
	
		barChart11.setOption(option11);
	
	
	});
	});
	
 $(function(e) {
	'use strict'
	var chart_data;
	$.get("https://localhost/rootsivy_erp/api/all_staff", function(data, status){
		 chart_data = data
	}).then(function(){
		//Income Expense Company Roots IVY International School
		var chartdata20 = [{
			name: 'Total Staff',
			type: 'bar',
			barMaxWidth: 20,
			data: chart_data.data['staff']
		}];
		var chart20 = document.getElementById('echart11');
		var barChart20 = echarts.init(chart20);
		var option20 = {
			valueAxis: {
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				splitArea: {
					show: true,
					areaStyle: {
						color: ['rgba(171, 167, 167,0.2)']
					}
				},
				splitLine: {
					lineStyle: {
						color: ['rgba(171, 167, 167,0.2)']
					}
				}
			},
			grid: {
				top: '6',
				right: '0',
				bottom: '17',
				left: '25',
			},
			xAxis: {
				data: chart_data.data['branches'],
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				splitLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLabel: {
					fontSize: 10,
					color: '#5f6d7a'
				}
			},
			tooltip: {
				trigger: 'axis',
				position: ['35%', '32%'],
			},
			yAxis: {
				splitLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLine: {
					lineStyle: {
						color: 'rgba(171, 167, 167,0.2)'
					}
				},
				axisLabel: {
					fontSize: 10,
					color: '#5f6d7a'
				}
			},
			series: chartdata20,
			color: ['#285cf7', '#7987a1' ]
		};
	
		barChart20.setOption(option20);
	
	
	});
	});
	
	 

	$(function(e) {
		'use strict'
		var chart_data;
		$.get("https://localhost/rootsivy_erp/api/all_student", function(data, status){
			 chart_data = data
		}).then(function(){
			//Income Expense Company Roots IVY International School
			var chartdata20 = [{
				name: 'Total Students',
				type: 'bar',
				barMaxWidth: 20,
				data: chart_data.data['students']
			}];
			var chart20 = document.getElementById('echart12');
			var barChart20 = echarts.init(chart20);
			var option20 = {
				valueAxis: {
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					splitArea: {
						show: true,
						areaStyle: {
							color: ['rgba(171, 167, 167,0.2)']
						}
					},
					splitLine: {
						lineStyle: {
							color: ['rgba(171, 167, 167,0.2)']
						}
					}
				},
				grid: {
					top: '6',
					right: '0',
					bottom: '17',
					left: '25',
				},
				xAxis: {
					data: chart_data.data['branches'],
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					splitLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLabel: {
						fontSize: 10,
						color: '#5f6d7a'
					}
				},
				tooltip: {
					trigger: 'axis',
					position: ['35%', '32%'],
				},
				yAxis: {
					splitLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLabel: {
						fontSize: 10,
						color: '#5f6d7a'
					}
				},
				series: chartdata20,
				color: ['#285cf7', '#7987a1' ]
			};
		
			barChart20.setOption(option20);
		
		
		});
		});

		
	$(function(e) {
		'use strict'
		var chart_data;
		$.get("https://localhost/rootsivy_erp/api/all_sos", function(data, status){
			 chart_data = data
		}).then(function(){
			//Income Expense Company Roots IVY International School
			var chartdata20 = [{
				name: 'Total Students',
				type: 'bar',
				barMaxWidth: 20,
				data: chart_data.data['students']
			}];
			var chart20 = document.getElementById('echart13');
			var barChart20 = echarts.init(chart20);
			var option20 = {
				valueAxis: {
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					splitArea: {
						show: true,
						areaStyle: {
							color: ['rgba(171, 167, 167,0.2)']
						}
					},
					splitLine: {
						lineStyle: {
							color: ['rgba(171, 167, 167,0.2)']
						}
					}
				},
				grid: {
					top: '6',
					right: '0',
					bottom: '17',
					left: '25',
				},
				xAxis: {
					data: chart_data.data['branches'],
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					splitLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLabel: {
						fontSize: 10,
						color: '#5f6d7a'
					}
				},
				tooltip: {
					trigger: 'axis',
					position: ['35%', '32%'],
				},
				yAxis: {
					splitLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLabel: {
						fontSize: 10,
						color: '#5f6d7a'
					}
				},
				series: chartdata20,
				color: ['#285cf7', '#7987a1' ]
			};
		
			barChart20.setOption(option20);
		
		
		});
		});

		
	$(function(e) {
		'use strict'
		var chart_data;
		$.get("https://localhost/rootsivy_erp/api/all_sos_staff", function(data, status){
			 chart_data = data
		}).then(function(){
			//Income Expense Company Roots IVY International School
			var chartdata20 = [{
				name: 'Total Students',
				type: 'bar',
				barMaxWidth: 20,
				data: chart_data.data['staff']
			}];
			var chart20 = document.getElementById('echart14');
			var barChart20 = echarts.init(chart20);
			var option20 = {
				valueAxis: {
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					splitArea: {
						show: true,
						areaStyle: {
							color: ['rgba(171, 167, 167,0.2)']
						}
					},
					splitLine: {
						lineStyle: {
							color: ['rgba(171, 167, 167,0.2)']
						}
					}
				},
				grid: {
					top: '6',
					right: '0',
					bottom: '17',
					left: '25',
				},
				xAxis: {
					data: chart_data.data['branches'],
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					splitLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLabel: {
						fontSize: 10,
						color: '#5f6d7a'
					}
				},
				tooltip: {
					trigger: 'axis',
					position: ['35%', '32%'],
				},
				yAxis: {
					splitLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLine: {
						lineStyle: {
							color: 'rgba(171, 167, 167,0.2)'
						}
					},
					axisLabel: {
						fontSize: 10,
						color: '#5f6d7a'
					}
				},
				series: chartdata20,
				color: ['#285cf7', '#7987a1' ]
			};
		
			barChart20.setOption(option20);
		
		
		});
		});