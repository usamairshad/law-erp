<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Bank Account Details</h3>
            <a href="<?php echo e(route('admin.bank_detail.index')); ?>" style = "float:right" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo Form::open(['method' => 'POST', 'route' => ['admin.bank_detail.store'], 'id' => 'validation-form']); ?>

        <div class="box-body" style = "margin-top:50px;">
            <div class="row">
                <div class="col-xs-12 form-group" style = "display:none;">
                    <div class="col-md-3" >
                        <label>
                            <input type="radio" name="orderType" id="orderType" value="1" checked> Company
                        </label>
                    </div>
                    
                        
                            
                        
                    
                </div>
            </div>
            
                
                    
                        
                            
                        
                    
                    
                        
                            
                        
                    
                
            
            <div  class="row">
                <div class="col-lg-12">
              
                    <div class="col-xs-3 form-group">
                        <label>Select Company</label>
                        <select class="form-control select2" name="branch_id">
                            <?php echo App\Models\Admin\Company::CompanyList(); ?>

                        </select>
                    </div>
                    <div class="col-xs-3 form-group">
                        <label>Select City</label>
                        <select class="form-control select2" name="city_id">
                            <?php echo App\Models\Admin\City::CityList(); ?>

                        </select>
                    </div>
                    <div class="col-xs-12 form-group">
        <?php echo Form::label('parent_id', 'Parent Group', ['class' => 'control-label']); ?>

        <select name="parent_id" id="parent_id" class="form-control select2" style="width: 100%;">
            <option value=""> Select a Parent Group </option>
                <?php echo $Groups; ?>

        </select>
        <span id="parent_id_handler"></span>
        <?php if($errors->has('parent_id')): ?>
                <span class="help-block">
                        <?php echo e($errors->first('parent_id')); ?>

                </span>
        <?php endif; ?>
</div>
                    <div class="col-xs-3 form-group <?php if($errors->has('bank_name')): ?> has-error <?php endif; ?>">
                        <?php echo Form::label('bank_name', 'Bank Name*', ['class' => 'control-label']); ?>


                        <select id="bank_name" name="bank_name" class="form-control select2">
                            <option value="">Select Bank Name</option>
                            <?php if($Banks->count()): ?>
                                <?php $__currentLoopData = $Banks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($Bank->id); ?>"><?php echo e($Bank->bank_name); ?></option>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </select>

                        <span id="bank_name_id"></span>
                        <?php if($errors->has('bank_name')): ?>
                            <span class="help-block">
                            <?php echo e($errors->has('bank_name')); ?>

                             </span>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-3 form-group <?php if($errors->has('branch_code')): ?> has-error <?php endif; ?>">
                        <?php echo Form::label('branch_code', 'Branch Code *', ['class' => 'control-label']); ?>

                        <?php echo Form::text('branch_code', old('branch_code'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>50]); ?>

                        <?php if($errors->has('branch_code')): ?>
                            <span class="help-block">
                                <?php echo e($errors->first('branch_code')); ?>

                                </span>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-3 form-group <?php if($errors->has('account_no')): ?> has-error <?php endif; ?>">
                        <?php echo Form::label('account_no', 'Account No: *', ['class' => 'control-label']); ?>

                        <?php echo Form::text('account_no', old('account_no'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>50]); ?>

                        <?php if($errors->has('account_no')): ?>
                            <span class="help-block">
                                <?php echo e($errors->first('account_no')); ?>

                                </span>
                        <?php endif; ?>
                    </div>

                </div>
            </div>


            <div  class="row">
                <div class="col-lg-12">
                    
                        

                        
                            
                            
                            
                            
                        

                        
                        
                            
                            
                             
                        
                    

                    <div class="col-xs-3 form-group <?php if($errors->has('phone_no')): ?> has-error <?php endif; ?>">
                        <?php echo Form::label('phone_no', 'Phone No: *', ['class' => 'control-label']); ?>

                        <?php echo Form::text('phone_no', old('phone_no'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>50]); ?>

                        <?php if($errors->has('phone_no')): ?>
                            <span class="help-block">
                            <?php echo e($errors->first('phone_no')); ?>

                        </span>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-6 form-group <?php if($errors->has('bank_address')): ?> has-error <?php endif; ?>">
                        <?php echo Form::label('bank_address', 'Address', ['class' => 'control-label']); ?>

                        <?php echo Form::text('bank_address', old('bank_name'), ['class' => 'form-control', 'placeholder' => '','maxlength'=>100]); ?>

                        <?php if($errors->has('bank_address')): ?>
                            <span class="help-block">
                            <?php echo e($errors->first('bank_address')); ?>

                        </span>
                        <?php endif; ?>
                    </div>

                </div>

            </div>

        </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?php echo Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']); ?>

            </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(url('public/js/admin/bank_detail/bank_detail_create_modify.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>