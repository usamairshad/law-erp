<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Define Tax Laws</h3>
            

    <div class="box box-primary">
         <a  href="<?php echo e(route('tax-law')); ?>" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['tax-law-store'], 'id' => 'validation-form']); ?>

        <div id="show" style="padding:5%;">
            <div id="box" class="box-body">
                <div class="form-group col-md-2 <?php if($errors->has('name')): ?> has-error <?php endif; ?>" style = "float:left;">
                    <?php echo Form::label('name', 'Law Section*', ['class' => 'control-label']); ?>

                   <input type="text" name="name[]"  required="required" placeholder="Enter Course Name" class="form-control" />
                </div>
                <div class="form-group col-md-3 <?php if($errors->has('description')): ?> has-error <?php endif; ?>" style = "float:left;">
                    <?php echo Form::label('description', 'Description', ['class' => 'control-label']); ?>

                     <input type="text" name="description[]" required="required" placeholder="Enter Description" class="form-control" />
                </div>
                <div class=" col-md-6 form-group" style = "float:left;">
        <?php echo Form::label('parent_id', 'Parent Group', ['class' => 'control-label']); ?>

        <select name="parent_id[]" id="parent_id" class="form-control select2" style="width: 100%;">
            <option value=""> Select a Parent Group </option>
                <?php echo $Groups; ?>

        </select>
        <span id="parent_id_handler"></span>
        <?php if($errors->has('parent_id')): ?>
                <span class="help-block">
                        <?php echo e($errors->first('parent_id')); ?>

                </span>
        <?php endif; ?>
</div>
                <div style ="display:none;" class="form-group col-md-1" style = "float:left;">
                    <button style="margin-top: 22px" type="button" name="add" id="add"  class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
        </div>

        <!-- /.box-body -->
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script type="text/javascript">
   
    var i = 0;
    
    $("#add").click(function(){
  
        ++i;
        $("#secondbox").append('<div id="remove" ><div class="form-group col-md-3" style = "float:left;"><input required="required" type="text" name="name[]" placeholder="Enter Course Name" class="form-control" /></div><div style = "float:left;" class="form-group col-md-6"><input type="text" required="required" name="description[]" placeholder="Enter Description" class="form-control" /></div><br> <div class="form-group col-md-2" style = "float:left;"><button " style = "float:left;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    </script>
    <script src="<?php echo e(url('public/js/admin/companies/create_modify.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>