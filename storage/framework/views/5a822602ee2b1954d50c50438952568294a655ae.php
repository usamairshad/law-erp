<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Session</h1>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Session</h3>
            <a href="<?php echo e(route('admin.session.index')); ?>" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <?php echo Form::open(['method' => 'PUT','route' => ['admin.session.update',$session->id]]); ?>


        <?php echo Form::token();; ?>

        <div class="box-body">
            <?php echo $__env->make('admin.session.fieldedit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
              <button class="btn btn-primary" type="submit">Save</button>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(url('public/js/hrm/staff/create_modify.js')); ?>" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function(){
 
        })
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>