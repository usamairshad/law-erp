<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Programs</h3>

    <div class="box box-primary">
        <div class="box-header with-border">
            <a  href="<?php echo e(route('admin.boards.create')); ?>" style = "float:right;" class="btn btn-success pull-right">Create</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($programs) > 0 ? 'datatable' : ''); ?>">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Compulsory Courses</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                <?php if(count($programs) > 0): ?>

                    <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($program['id']); ?>">
                            <td><?php echo e($program->id); ?></td>
                            <td><?php echo e($program->name); ?></td>
                            <td><?php echo e($program->description); ?></td>
                            <td><a  data-id="<?php echo e($program->id); ?>" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-xs btn-primary academics-courses" data-toggle="modal" data-target="#view-modal<?php echo e($program->id); ?>"><span>View Courses</span></a></td>
                            <td> <a href = "compulsory-course/<?php echo e($program->id); ?>"  data-target="#exampleModal1<?php echo e($program->id); ?>"><button style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-primary">Add Compulsory Course</button></a>
                                <!--a href="<?php echo e(route('admin.boards.edit',[$program->id])); ?>"><button style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-primary">Edit</button></a -->
                            
                            </td>

                   
                            <div class="modal fade" id="view-modal<?php echo e($program->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Program Compulsory Courses</h2>
                                      
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                          <div id="days_<?php echo e($program->id); ?>">
                                              
                                          </div>
                           
                                        </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        
                                    </div>
                                  </div>
                                   
                                </div>
                            </div>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable({
               "order": [[ 0, "asc" ]]
            });
            selectRefresh();
        });
     
        function selectRefresh() {
            $(".courses").select2({

            });
        }
        $(function() {
           $(".academics-courses").on('click', function () {
  
                   var id=  $(this).data('id');

                   console.log(id+'-----------------')

                    $.ajax({
                        type: 'POST',
                        url: '<?php echo e(route('admin.load-program-compulsory-courses')); ?>',
                        data: {
                            _token: '<?php echo e(csrf_token()); ?>',
                            program_id: id
                        },
                        success: function (response) {
                            $.each( response, function(k, v) {
                                if(k == 0)
                                {
                                    $("#days_"+v.program_id).empty();
                                }
                                $("#days_"+v.program_id).append('<input readonly type="text" class="form-control" value="'+v.course_id+'"><br/>');
                            });
                        }
                    });
            })
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>