<?php $request = app('Illuminate\Http\Request'); ?>
<?php $per = PermissionHelper::getUserPermissions();?>


<!-- Loader -->
<div id="global-loader">
            <img src="<?php echo e(url('public/theme')); ?>/assets/img/loaders/loader-4.svg" class="loader-img" alt="Loader">
        </div>
        <!-- /Loader -->

        <!-- main-sidebar opened -->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <aside class="main-sidebar app-sidebar sidebar-scroll">
            <div class="main-sidebar-header">
                <a class="desktop-logo logo-light" href="#" style = "text-align: center;
    font-size: 17px;
    margin: 25px;
    font-weight: 500;background-color:none !important;" class="text-center mx-auto">Roots IVY ERP Network </a>
            </div><!-- /logo -->
            <div class="main-sidebar-loggedin">
                <div class="app-sidebar__user">
                    <div class="dropdown user-pro-body text-center">
                        <div class="user-pic">
                            <img src="<?php echo e(url('public/theme')); ?>/assets/img/faces/user-icon.png" alt="user-img" class="rounded-circle mCS_img_loaded" style="width: 60px;height: 60px;font-size: 36px;">
                        </div>
                        <div class="user-info">
                            <h6 class=" mb-0 text-dark"><?php echo e(Auth::user()->name); ?></h6>
                            <span class="text-muted app-sidebar__user-name text-sm"><?php echo e(Auth::user()->name); ?></span>
                        </div>
                    </div>
                </div>
            </div><!-- /user -->
            <div class="sidebar-navs">
                <ul class="nav  nav-pills-circle">
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings" aria-describedby="tooltip365540">
                        <a class="nav-link text-center m-2">
                            <i class="fe fe-settings"></i>
                        </a>
                    </li>
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Chat">
                        <a class="nav-link text-center m-2">
                            <i class="fe fe-mail"></i>
                        </a>
                    </li>
                     <?php if(!Auth::user()->hasRole('admin')): ?>
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Profile">
                        <a class="nav-link text-center m-2" href = "<?php echo e(route('admin.staff.profile')); ?>">
                            <i class="fe fe-user"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Logout">
                          
                        <a href="#logout" onclick="$('#logout').submit();"  class="nav-link text-center m-2">
                            <i class="fe fe-power"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="main-sidebar-body">
                <ul class="side-menu ">
                <?php if(Auth::user()->hasRole('ceo') || Auth::user()->hasRole('admin') ): ?>
                    <li class="slide">
                        <a class="side-menu__item" href="<?php echo e(url('/dashboard')); ?>"><i class="side-menu__icon fe fe-airplay"></i><span class="side-menu__label"><?php echo app('translator')->getFromJson('global.app_dashboard'); ?></span></a>
                    </li>
                    <?php endif; ?>
                    <?php if(Auth::user()->hasRole('GM Finance')): ?>
                    <li class="slide">
                        <a class="side-menu__item" href="<?php echo e(url('/gmdashboard')); ?>"><i class="side-menu__icon fe fe-airplay"></i><span class="side-menu__label">Dashboard</span></a>
                    </li>
                    <?php endif; ?>
                    <?php if(Auth::user()->hasRole('HR_head')): ?>
                    <li class="slide">
                        <a class="side-menu__item" href="<?php echo e(url('/hr-dashboard')); ?>"><i class="side-menu__icon fe fe-airplay"></i><span class="side-menu__label">HR Dashboard</span></a>
                    </li>
                    <?php endif; ?>
      <!-- Accounts Module Start Here -->


      <li class="slide <?php echo e($request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance' || $request->segment(3) == 'expense_summary' ||
$request->segment(3) == 'profit_loss' || $request->segment(3) == 'balance_sheet' || $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
    <a href="#" class="side-menu__item" data-toggle="slide">
        <i class="side-menu__icon fe fe-database"></i>
            <span class="side-menu__label">Accounts</span>
    </a>
    <ul class="slide-menu">
    <li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Accounts Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
            <ul class="sub-slide-menu">
           
            <?php if(in_array('erp_groups_manage',$per)): ?>
                            <li class="<?php echo e(($request->segment(2) == 'groups' && $request->segment(3) != 'overview') ? 'active active-sub' : ''); ?>">
                                <a class="slide-item" href="<?php echo e(route('admin.groups.index')); ?>">
                                    <span class="title">Groups</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array('erp_ledgers_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'ledgers' ? 'active active-sub' : ''); ?>">
                                <a class="slide-item" href="<?php echo e(route('admin.ledgers.index')); ?>">
                                    <span class="title">Ledgers</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array('erp_ledgers_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'ledger_tree' ? 'active active-sub' : ''); ?>">
                                <a class="slide-item" href="<?php echo e(route('admin.ledger_tree')); ?>">
                                    <span class="title">Ledger Tree</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array('erp_entries_manage',$per)): ?>
                            <li class="<?php echo e(($request->segment(2) == 'entries' || $request->segment(2) == 'voucher') ? 'active active-sub' : ''); ?>">
                                <a class="slide-item" href="<?php echo e(route('admin.entries.index')); ?>">
                                    <span class="title">Journal Entry</span>
                                </a>
                            </li>
                        <?php endif; ?>
      </ul>
      </li>
      <li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Assets Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
            <ul class="sub-slide-menu">
            <li>
        
                <a class="sub-side-menu__item" href="<?php echo e(url('/admin/asset_type')); ?>">
                             <span class="title"> Create Asset Type</span>
                         </a>
            </li>
            <li class="sub-slide<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                         <a class="sub-side-menu__item" href="<?php echo e(url('/admin/asset')); ?>">
                             <span class="title">Create Asset</span>
                         </a>
                     </li>
                     <li class="sub-slide<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                         <a class="sub-side-menu__item" href="<?php echo e(url('/return-policy')); ?>">
                             <span class="title">Asset Return Policy</span>
                         </a>
                     </li>
                     <li class="sub-slide<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                         <a class="sub-side-menu__item" href="<?php echo e(url('/admin/fix_assets')); ?>">
                             <span class="title">Fixed Asset</span>
                         </a>
                     </li>
                     <li class="sub-slide<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                         <a class="sub-side-menu__item" href="<?php echo e(url('/admin/inventorytransfer')); ?>">
                             <span class="title">Asset Inverntory Transfer</span>
                         </a>
                     </li>
      </ul>
      </li>
      <li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Loan Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
            <ul class="sub-slide-menu">
            <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
                                         <a class="sub-side-menu__item" href="<?php echo e(url('/loan-list')); ?>">
                                             <span class="title"> Loan Type</span>
                                         </a>
                                     </li>
                                     <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
                                         <a class="sub-side-menu__item" href="<?php echo e(url('/loan-installment-list')); ?>">
                                             <span class="title"> Loan Plans</span>
                                         </a>
                                     </li>
                                     <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
                                         <a class="sub-side-menu__item" href="<?php echo e(url('/request_loan_employee_view')); ?>">
                                             <span class="title"> Employee Request Loan </span>
                                         </a>
                                     </li>
                                     <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
                                         <a class="sub-side-menu__item" href="<?php echo e(url('/admin/bank_loan')); ?>">
                                             <span class="title"> Bank Request Loan </span>
                                         </a>
                                     </li>
                                     </ul>
                                     </li>
      <li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Land Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
            <ul class="sub-slide-menu">
            <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
                                         <a class="sub-side-menu__item" href="<?php echo e(url('/payee-list')); ?>">
                                             <span class="title"> LandLord Details</span>
                                         </a>
                                     </li>
                                      <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
                                         <a class="sub-side-menu__item" href="<?php echo e(url('/building-list')); ?>">
                                             <span class="title"> Building List</span>
                                         </a>
                                     </li>
                                     <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
                                         <a class="sub-side-menu__item" href="<?php echo e(url('utility-bills')); ?>">
                                             <span class="title">Utilities Bills</span>
                                         </a>
                                     </li>

      </ul>
      </li
      >
      <?php if(Auth::user()->hasRole('GM Finance') || Auth::user()->hasRole('admin')  ): ?>
      <li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Payment Sheets</span><i class="sub-angle fe fe-chevron-down"></i></a>
            <ul class="sub-slide-menu">
            <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
        <a class="slide-item" href="<?php echo e(route('bank-payment')); ?>">
            <span class="title">Payment List</span>
        </a>
    </li>
    <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
        <a class="slide-item" href="<?php echo e(route('bank-payment-sheet-create')); ?>">
            <span class="title">Bank Payment Sheets</span>
        </a>
    </li>
    <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
        <a class="slide-item" href="<?php echo e(route('cash-payment-sheet-create')); ?>">
            <span class="title">Cash Payment Sheets</span>
        </a>
    </li>
    <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
        <a class="slide-item" href="<?php echo e(route('rent-payment-sheet-create')); ?>">
            <span class="title">Rental Payment Sheets</span>
        </a>
    </li>
    <li class="sub-slide<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
        <a class="slide-item" href="<?php echo e(route('payment-sheet-list')); ?>">
            <span class="title">Pending Payment List</span>
        </a>
    </li>
   
      </ul>
      </li>
      <?php endif; ?>
      <li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Salary Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
            <ul class="sub-slide-menu">
            <li>
        
                <a class="sub-side-menu__item" href="<?php echo e(url('epf_eobi_staff')); ?>">
                             <span class="title">Assign EPF and EOBI</span>
                         </a>
            </li>
            <li>
        
        <a class="sub-side-menu__item" href="<?php echo e(url('epf_applied_staff')); ?>">
                     <span class="title">EPF Applied Staff</span>
                 </a>
    </li>
    <li>
        
        <a class="sub-side-menu__item" href="<?php echo e(url('eobi_applied_staff')); ?>">
                     <span class="title">EOBI Applied Staff</span>
                 </a>
    </li>
    <li>
        
        <a class="sub-side-menu__item" href="<?php echo e(url('/admin/asset_type')); ?>">
                     <span class="title">EOBI Govt Notification</span>
                 </a>
    </li>
    <li>
        
        <a class="sub-side-menu__item" href="<?php echo e(url('salary_sheet_create')); ?>">
                     <span class="title">Salary Sheet</span>
                 </a>
    </li>


            </ul>
</li>
<li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Tax Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
            <ul class="sub-slide-menu">
            <li>
        
        <a class="sub-side-menu__item" href="<?php echo e(route('tax-law')); ?>">
                     <span class="title">Define Laws</span>
                 </a>
    </li>
            <li>
        
                <a class="sub-side-menu__item" href="<?php echo e(route('admin.taxsettings.index')); ?>">
                             <span class="title">Define Salary Tax Slabs</span>
                         </a>
            </li>
            <li>
        
                <a class="sub-side-menu__item" href="<?php echo e(route('rental-tax-settings')); ?>">
                             <span class="title">Define Rent Tax Slabs</span>
                         </a>
            </li>
            <li>
        
                <a class="sub-side-menu__item" href="<?php echo e(route('tax-law-rate')); ?>">
                             <span class="title">Define Law Rates</span>
                         </a>
            </li>
          
            </ul>
            </li>

      <?php if(Auth::user()->hasRole('GM Finance') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('ceo') ): ?>
      <li class="sub-slide">
    <a class="sub-side-menu__item" href="<?php echo e(route('vendor-list')); ?>"><span class="sub-side-menu__label">Vendor or Payee</span></a>
      </li>
                    <?php endif; ?>



                    
      </ul>
      </li>





    <!-- HR Module Started -->                
<li class="slide <?php echo e($request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance' || $request->segment(3) == 'expense_summary' ||
$request->segment(3) == 'profit_loss' || $request->segment(3) == 'balance_sheet' || $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
    <a href="#" class="side-menu__item" data-toggle="slide">
        <i class=" side-menu__icon fa fa-users"></i>
            <span class="side-menu__label">HR Management</span>
    </a>
    <ul class="slide-menu">
   
    <li class="sub-slide">
        <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Agenda Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
            <ul class="sub-slide-menu">
            <li>
                <a class="sub-side-menu__item" href="<?php echo e(url('/agenda')); ?>">
                    <span class="title">Manage Agenda</span>
                </a>
            </li>
                            
    <?php if(Auth::user()->hasRole('HR_head') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('ceo') || Auth::user()->hasRole('principal')): ?>
    <li class="<?php echo e($request->segment(2) == 'taxsettings' ? 'active active-sub' : ''); ?>">
        <a class="sub-side-menu__item" href="<?php echo e(url('agenda-list')); ?>">
            <span class="title">Agenda Request List</span>
        </a>
    </li>
    <?php endif; ?>
</ul>
</li>
    <?php if(Auth::user()->hasRole('GM Finance')  || Auth::user()->hasRole('HR_head')  || Auth::user()->hasRole('admin') || Auth::user()->hasRole('ceo') || Auth::user()->hasRole('Principal')): ?>
    <li class="sub-slide">
        <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Employee Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
    <ul class="sub-slide-menu">
    <?php if(Auth::user()->hasRole('GM Finance')  || Auth::user()->hasRole('HR_head')  || Auth::user()->hasRole('admin') || Auth::user()->hasRole('ceo') || Auth::user()->hasRole('Principal')): ?>
    
    <li class="<?php echo e(($request->segment(2) == 'groups' && $request->segment(3) != 'overview') ? 'active active-sub' : ''); ?>">
        <a class="sub-slide-item" href="<?php echo e(route('admin.staff.index')); ?>">
                <span class="title">Manage Employee</span>
        </a>
    </li>
   <?php endif; ?>
    <?php if(Auth::user()->hasRole('HR_head') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('principal') || Auth::user()->hasRole('ceo') ||  Auth::user()->hasRole('GM Finance')): ?>
         <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_id_card_request_manage',$per)): ?>
            <li class="sub-slide<?php echo e($request->segment(2) == 'candidate-hired' ? 'active active-sub' : ''); ?>">
                <a href="<?php echo e(route('admin.documents.index')); ?>" class="sub-side-menu__item">
                    <span class="title">Employee's Documents</span>
                </a>
            </li>
         <?php endif; ?> 
     <?php endif; ?>
     <li class="sub-slide<?php echo e(($request->segment(2) == 'groups' && $request->segment(3) != 'overview') ? 'active active-sub' : ''); ?>">
        <a  href="<?php echo e(route('my-resign-request-list')); ?>" class="sub-side-menu__item">
                <span class="title">Resigned Employee</span>
        </a>
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <li class="sub-slide">
      <a class="sub-side-menu__item" data-toggle="sub-slide" href="#">
        <span class="sub-side-menu__label">Training Management</span>
        <i class="sub-angle fe fe-chevron-down"></i>
      </a>
      <ul class="sub-slide-menu">
        <li >
          <a class="sub-side-menu__item" href="<?php echo e(route('admin.training.index')); ?>">
              <span class="title">Training</span>
          </a>
          </li>
            <li >
              <a class="sub-side-menu__item" href="<?php echo e(url('/admin/training_request')); ?>">
                <span class="title">Training Request</span>
              </a>
            </li>
          </ul>

    </li>

    <li class="sub-slide">
      <a class="sub-side-menu__item" data-toggle="sub-slide" href="#">
        <span class="sub-side-menu__label">BTEC Attendance Management</span>
        <i class="sub-angle fe fe-chevron-down"></i>
      </a>
      <ul class="sub-slide-menu">
          <?php if(Auth::user()->hasRole('teacher')): ?>  
          <li >
            <a class="sub-side-menu__item" href="<?php echo e(route('admin.attendance_payroll.create')); ?>">
                <span class="title">Mark Attendance Lecture Base</span>
            </a>
          </li>
          <?php endif; ?>

          <li >
              <a href="<?php echo e(url('admin\teacher_attendance_daywise')); ?>" class="sub-side-menu__item">
                  <span class="title">Daily Attendance Schedule</span>
              </a>
          </li>
          <li >
              <a href="<?php echo e(url('staff_attendance_report')); ?>" class="sub-side-menu__item">
                  <span class="title">Staff Attendance Report</span>
              </a>
          </li>
          

            <li>
              <a class="sub-side-menu__item" href="<?php echo e(route('admin.attendance_payroll.index')); ?>">
                <span class="title">Teacher Attendacne Sheet</span>
              </a>
            </li>
            
            <?php if(Auth::user()->hasRole('teacher')): ?>  
            <li>
              <a href="<?php echo e(route('admin.leave_management.index')); ?>"  class="sub-side-menu__item">
                <span class="title">Lecture Base Leave Request</span>
              </a>
            </li>
            <?php endif; ?>

            <li>
              <a href="<?php echo e(url('admin/leaverequestlist')); ?>"  class="sub-side-menu__item">
                <span class="title">Lecture Base Leave Request List</span>
              </a>
            </li> 
      </ul>

    </li>


<li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Recuritment</span><i class="sub-angle fe fe-chevron-down"></i></a>
    <ul class="sub-slide-menu">
        
        <?php if(Auth::user()->hasRole('Principal') || Auth::user()->hasRole('HR_head') ): ?>
        <li class="<?php echo e($request->segment(3) == 'job-post' ? 'active active-sub' : ''); ?>">
                    <a class="sub-side-menu__item" href="<?php echo e(route('admin.recruitment.job-request')); ?>">
                             <span class="title">Employee Require Request</span>
                    </a>
        </li>
        <?php endif; ?>


        <?php if(Auth::user()->hasRole('HR_head')): ?>
        <li class="<?php echo e($request->segment(3) == 'job-post' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(route('admin.recruitment.job-post')); ?>">
                             <span class="title">Job Post</span>
                                </a>
                    </li>
                    <li class="<?php echo e($request->segment(1) == 'candidate-form' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(route('candidate-form.index')); ?>">
                             <span class="title">Candidate Applications</span>
                                </a>
                    </li>
                    <li class="sub-slide<?php echo e($request->segment(2) == 'candidate-hired' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(route('admin.candidate-hired')); ?>">
                             <span class="title">Hired Candidates</span>
                                </a>
                    </li>
                    <li class="sub-slide<?php echo e($request->segment(1) == 'candidate-form' ? 'active active-sub' : ''); ?>">
                        <a href="<?php echo e(url('admin/candidate_waiting_list')); ?>" class="sub-side-menu__item">
                            
                          <span class="sub-side-menu__label">Waiting List Candidates</span>
                      </a>
                    </li>
         <?php endif; ?>
       
    </ul>

                    </li>
                   
    <li class="sub-slide">
      <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Leave Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
      <ul class="sub-slide-menu">
        <li class="<?php echo e($request->segment(3) == 'leaves-period' ? 'active active-sub' : ''); ?>">
          <a class="sub-side-menu__item" href="<?php echo e(url('hrm/leave_periods')); ?>">
            <span class="title">Leaves Period</span>
          </a>
        </li>
        <li class="<?php echo e($request->segment(3) == 'leaves-types' ? 'active active-sub' : ''); ?>">
          <a class="sub-side-menu__item" href="<?php echo e(url('hrm/leave_types')); ?>">
              <span class="title">Leaves Types</span>
                                </a>
                    </li>
                    <li class="<?php echo e($request->segment(1) == 'working-days' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(url('hrm/work_weeks')); ?>">
                             <span class="title">Working Days</span>
                                </a>
                    </li>
                    <li class="sub-slide<?php echo e($request->segment(2) == 'holiday' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(url('hrm/holidays')); ?>">
                             <span class="title">Holiday</span>
                                </a>
                    </li>
                    <li class="sub-slide<?php echo e($request->segment(2) == 'leave-request' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(url('hrm/leave_requests')); ?>">
                             <span class="title">Leave Request</span>
                                </a>
                    </li>
                    <li class="sub-slide<?php echo e($request->segment(2) == 'manage-leaves' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(url('hrm/leaves')); ?>">
                             <span class="title">Manage Leaves</span>
                                </a>
                    </li>
                    <li class="sub-slide<?php echo e($request->segment(2) == 'approved-leaves' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(route('admin.recruitment.job-post')); ?>">
                             <span class="title">Approved Leaves</span>
                                </a>
                    </li>
    

       
    </ul>

                    </li>
                    <li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Exit Managment</span><i class="sub-angle fe fe-chevron-down"></i></a>
    <ul class="sub-slide-menu">
    <li class="sub-slide<?php echo e($request->segment(2) == 'terminatedstaff' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(url('terminated_staff_list')); ?>">
                             <span class="title">List Of Terminated</span>
                                </a>
                    </li>
                    <li class="sub-slide<?php echo e($request->segment(2) == 'approved-leaves' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(route('staff_status_list')); ?>">
                             <span class="title">List Of SOS</span>
                                </a>
                    </li>
                    
                    <li class="sub-slide<?php echo e($request->segment(2) == 'approved-leaves' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(route('my-resign-request-list')); ?>">
                             <span class="title">Resigned List</span>
                                </a>
                    </li>
                    <li class="sub-slide<?php echo e($request->segment(2) == 'approved-leaves' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(url('admin/warning_list')); ?>">
                             <span class="title">Warning List</span>
                                </a>
                    </li>
</ul>
</li>     
<li class="sub-slide">
    <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Teacher</span><i class="sub-angle fe fe-chevron-down"></i></a>
    <ul class="sub-slide-menu">
    <li class="sub-slide<?php echo e($request->segment(2) == 'approved-leaves' ? 'active active-sub' : ''); ?>">
                        <a class="sub-side-menu__item" href="<?php echo e(url('admin/all-teachers')); ?>">
                             <span class="title">Teachers</span>
                                </a>
                    </li>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_staff_manage',$per)): ?>
                    <li >
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.teaacher.course.detail')); ?>">
                                    <span class="title">Assign Courses</span>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if(Auth::user()->hasRole('HR_head') || Auth::user()->hasRole('quality assurance') || Auth::user()->hasRole('admin')): ?>
                    <li >
                                <a class="sub-side-menu__item" href="<?php echo e(route('staff-performance.detail')); ?>">
                                    <span class="title">Performance List</span>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if(Auth::user()->hasRole('sr.coordinator') || Auth::user()->hasRole('admin') ): ?>
            <li>
                                <a class="sub-side-menu__item" href="<?php echo e(route('staff-performance.index')); ?>">
                                    <span class="title">Manage Performance</span>
                                </a>
                            </li>
            <?php endif; ?>
</ul>
</li> 
        
       
    </ul>

                    </li>
                    <?php if(Auth::user()->hasRole('HR_head') || Auth::user()->hasRole('admin')): ?>
                    <li class="slide">
                    <a href="#" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fe fe-file"></i>
                        <span class="side-menu__label">Request</span>
                     
                    </a>
                    <ul class="slide-menu">
                    <li >
                                <a class="sub-side-menu__item" href="<?php echo e(url('approve-card-request')); ?>">
                                    <span class="title">ID Card </span>
                                </a>
                            </li>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_staff_manage',$per)): ?>
                    <li >
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.resign.index')); ?>">
                                    <span class="title">Resign</span>
                                </a>
                            </li>
                            <?php endif; ?>
                           
                            <?php if(Auth::user()->hasRole('HR_head') || Auth::user()->hasRole('quality assurance') || Auth::user()->hasRole('admin')): ?>
                    <li >
                                <a class="sub-side-menu__item" href="<?php echo e(url('bank_account_request-list')); ?>">
                                    <span class="title">Bank Account</span>
                                </a>
                            </li>
                            <?php endif; ?>

                            <?php if(Auth::user()->hasRole('GM Finance')): ?>

                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_id_card_request_manage',$per)): ?>
                                   <li class="<?php echo e($request->segment(2) == 'taxsettings' ? 'active active-sub' : ''); ?>">
                                       <a class="sub-side-menu__item" href="<?php echo e(url('request-idcard_gm')); ?>">
                                           <span class="title">Approved ID Card</span>
                                       </a>
                                   </li>
                                   <?php endif; ?> 
                              <?php endif; ?>             
        
       
    </ul>

                    </li>
                    <?php endif; ?>
                    <?php if(in_array('erp_report_manage',$per)): ?>
            <li class="slide <?php echo e($request->segment(3) == 'ledger' ||
            $request->segment(3) == 'trial_balance' || $request->segment(3) == 'expense_summary' ||
              $request->segment(3) == 'profit_loss' || $request->segment(3) == 'balance_sheet' ||
               $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
               <a href="#" class="side-menu__item" data-toggle="slide">
                    <i class="side-menu__icon fe fe-package "></i>
                        <span class="side-menu__label">Reports</span>
                     
                    </a>
                    <?php if(in_array('erp_account_reports_manage', $per)): ?>
                        
             
               
                    <ul class="slide-menu">
                    <li class="sub-slide <?php echo e($request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Attendance Reports</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class="sub-slide-menu">
                                <li class="<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(url('attendance-report-monthly')); ?>">
                                        <span class="title">Monthly Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.account_reports.ledger')); ?>">
                                        <span class="title">Summarized Report</span>
                                    </a>
                                </li>
                            </ul>
                    </li>        
                        <li class="sub-slide <?php echo e($request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">A/C Financial Reports</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class="sub-slide-menu">
                                <li class="<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.account_reports.ledger')); ?>">
                                        <span class="title"> Ledger Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'trial_balance' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.account_reports.trial_balance')); ?>">
                                        <span class="title"> Trial Balance</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'expense_summary' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.account_reports.expense_summary')); ?>">
                                        <span class="title"> Expense Summary</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'profit_loss' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.account_reports.profit_loss')); ?>">
                                        <span class="title"> Profit & Loss Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'profit_loss' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.account_reports.profit_loss_cmp')); ?>">
                                        <span class="title"> Comparative P&L Report</span>
                                    </a>
                                </li> 
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.account_reports.balance_sheet')); ?>">
                                        <span class="title"> Balance Sheet Report </span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(1) == 'report_eobi' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="">
                                        <span class="title"> Eobi report</span>
                                    </a>
                                </li>

                                </ul>
                            </li>
                            <li class="sub-slide <?php echo e($request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Fee Mis Reconcilation</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class = "sub-slide-menu">
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('security-refund-register')); ?>">
                                        <span class="title">Fee - Security Refundable Register </span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('recon-with-fee-department')); ?>">
                                        <span class="title">Fee - 236i Recon Fee Department </span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('fee-recover-report')); ?>">
                                        <span class="title">Fee - Recovery Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('fee-daily-outstanding-report')); ?>">
                                        <span class="title">Fee - Daily Outstading Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.account_reports.balance_sheet')); ?>">
                                        <span class="title">Fee - Monthly Report</span>
                                    </a>
                                </li>
                                
                                </ul>


                                </li>
                                <?php endif; ?>
                                <li class="sub-slide <?php echo e($request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Academics Reports</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class = "sub-slide-menu">
                                <li >
                                    <a class="sub-slide-item" href="<?php echo e(route('nominal-report')); ?>">
                                        <span class="title">Nominal Reports </span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('student-contact')); ?>">
                                        <span class="title">Student Contact List </span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('student-address')); ?>">
                                        <span class="title">Student Address List</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('default-student-report')); ?>">
                                        <span class="title">Defaulters</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('defaulter-amt-report')); ?>">
                                        <span class="title">Defaulter Amt</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('summary-report')); ?>">
                                        <span class="title">Summary Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('concession-report')); ?>">
                                        <span class="title">Concession Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('head-wise-fee-report')); ?>">
                                        <span class="title">Head Wise Fee Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('head-wise-summary-report')); ?>">
                                        <span class="title">Head Wise Summary Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('head-wise-revenue-report')); ?>">
                                        <span class="title">Head Wise Evenue Report</span>
                                    </a>
                                </li>


                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('head-wise-initial-security-fee-report')); ?>">
                                        <span class="title">Head Wise Initial Security Fee Report</span>
                                    </a>
                                </li>
                                
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('head-wise-lms-report')); ?>">
                                        <span class="title">Head Wise LMS Report</span>
                                    </a>
                                </li>

                                 <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('head-wise-annual-report')); ?>">
                                        <span class="title">Head Wise Annual Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('head-wise-partyfund-report')); ?>">
                                        <span class="title">Head Wise Party Fund Report</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'balance_sheet' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('revenue-receivable-received-report')); ?>">
                                        <span class="title">Revenue Receivable vs Received Report</span>
                                    </a>
                                </li>

                               
                                </ul>


                                </li>
                                <?php endif; ?>
                
                </ul>



            </li>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_school_setting_manage',$per)): ?>

            <li class="slide <?php echo e($request->segment(3) == 'ledger' ||
            $request->segment(3) == 'trial_balance' || $request->segment(3) == 'expense_summary' ||
              $request->segment(3) == 'profit_loss' || $request->segment(3) == 'balance_sheet' ||
               $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
               <a href="#" class="side-menu__item" data-toggle="slide">
                    <i class="side-menu__icon fe fe-package "></i>
                        <span class="side-menu__label">Academics Management</span>
                     
                    </a>
                
                        
             
               
                    <ul class="slide-menu">
                      
                        <li class="sub-slide <?php echo e($request->segment(3) == 'ledger' || $request->segment(3) == 'trial_balance'
                         || $request->segment(3) == 'expense_summary' || $request->segment(3) == 'profit_loss'
                         || $request->segment(3) == 'balance_sheet' || $request->segment(1) == 'report_eobi' || $request->segment(3)=='balance_sheet_tree' ? 'active' : ''); ?>">
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">School Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class="sub-slide-menu">
                                <li class="<?php echo e($request->segment(3) == 'program' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.boards.index')); ?>">
                                        <span class="title">Programs</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.boardprogramindex')); ?>">
                                        <span class="title">Board Programs</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.classes.index')); ?>">
                                        <span class="title">Classes</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'ledger' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.boards-program-classes')); ?>">
                                        <span class="title">Assign Board Program Classes</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'classsTerms' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.bpc-term.index')); ?>">
                                        <span class="title">Assign Board Program Class Terms</span>
                                    </a>
                                </li>
  <li class="<?php echo e($request->segment(3) == 'sections' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.section.index')); ?>">
                                        <span class="title">Sections</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'sections' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.classes-assign-class-section')); ?>">
                                        <span class="title">Assign Class Sections</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'academics-groups' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.academics-groups.index')); ?>">
                                        <span class="title">Academics Group</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'courses' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.courses.index')); ?>">
                                        <span class="title">Courses</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'labs' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.labs.index')); ?>">
                                        <span class="title">Labs</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'course-lab' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.course-lab')); ?>">
                                        <span class="title">Assign Course Lab</span>
                                    </a>
                                </li>

                                </ul>
                                </li>
                                <li class="sub-slide <?php echo e(($request->segment(2) == 'session' || $request->segment(1) == 'active-session-section'   || $request->segment(3) == 'session' ||$request->segment(2) == 'candidate-hire') ? 'active' : ''); ?>">
                  
                          
                                <a class="sub-side-menu__item" data-toggle="sub-slide" href="#"><span class="sub-side-menu__label">Session Management</span><i class="sub-angle fe fe-chevron-down"></i></a>
                                <ul class="sub-slide-menu">
                                <li class="<?php echo e($request->segment(3) == 'session' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.session.index')); ?>">
                                        <span class="title">Session</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'session' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(route('admin.active-session.index')); ?>">
                                        <span class="title">Active Session</span>
                                    </a>
                                </li>
                                <li class="<?php echo e($request->segment(3) == 'session' ? 'active active-sub' : ''); ?>">
                                    <a class="sub-slide-item" href="<?php echo e(url('active-session-section')); ?>">
                                        <span class="title">Active Session Section</span>
                                    </a>
                                </li>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_academic_years_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'academic_years' ? 'active active-sub' : ''); ?>">
                                <a class="sub-slide-item" href="<?php echo e(route('admin.academic_year.index')); ?>">
                   
                                    <span class="title">Academic Year</span>
                                </a>
                            </li>
                            <li class="<?php echo e($request->segment(2) == 'academic_years' ? 'active active-sub' : ''); ?>">
                                <a class="sub-slide-item" href="<?php echo e(route('admin.term.index')); ?>">
                             
                                    <span class="title">Term</span>
                                </a>
                            </li>
                        <?php endif; ?>
                              


</ul>
</li>
                      
                        

                                </ul>
                               </li>
                           

<?php endif; ?>
<li class="slide <?php echo e(($request->segment(2) == 'settings' || $request->segment(2) == 'financial_years' ||
                $request->segment(2) == 'account_types' || $request->segment(2) == 'companies' ||
                $request->segment(2) == 'branches' || $request->segment(2) == 'departments' ||
                $request->segment(2) == 'employees' || $request->segment(2) == 'fee_head' ||
                $request->segment(2) == 'customers' || $request->segment(2) == 'payment_methods' ||
                $request->segment(2) == 'banks' || $request->segment(2) == 'city' ||
                $request->segment(2) == 'fee_section' || $request->segment(2) == 'country' ||
                $request->segment(2) == 'currency' ||
                $request->segment(2) == 'suppliers') ? 'active' : ''); ?>">
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_countries_manage',$per)): ?>
                    <a href="#" class="side-menu__item" data-toggle="slide">
                    <i class=" side-menu__icon fa fa-users"></i>
                        <span class="side-menu__label">Administrator</span>
                     
                    </a>
                <?php endif; ?>
                    <ul class="slide-menu">
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_companies_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'companies' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.companies.index')); ?>">
                                    <span class="title">Companies</span>
                                </a>
                            </li>
                        <?php endif; ?>
                   

                        <?php if(in_array('erp_departments_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'departments' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.departments.index')); ?>">
                                    <span class="title">Departments</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        
                        <?php if(in_array('erp_regions_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'regions' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.regions.index')); ?>">
                                    <span class="title">Regions</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array('erp_cities_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'city' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.city.index')); ?>">
                                    <span class="title">City</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array('erp_branches_manage',$per)): ?>
                            
                        <?php endif; ?>
                        <?php if(in_array('erp_fee_section_manage',$per)): ?>
                            <li  class="<?php echo e($request->segment(2) == 'fee_section' ? 'active active-sub' : ''); ?>">
                                <a  class="sub-side-menu__item" href="<?php echo e(route('admin.fee_section.index')); ?>">
                                    <span class="title">Fee Section</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array('erp_fee_head_manage',$per)): ?>
                            <!--li class="<?php echo e($request->segment(2) == 'fee_head' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.fee_head.index')); ?>">
                                    <span class="title">Fee Head</span>
                                </a>
                            </li -->
                        <?php endif; ?>
                        

                        <?php if(in_array('erp_customers_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'customers' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.customers.index')); ?>">
                                    <span class="title">Customers</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array('erp_banks_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'banks' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.banks.index')); ?>">
                                    <span class="title">Banks</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array('erp_banksdeatil_manage',$per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'bank_detail' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(route('admin.bank_detail.index')); ?>">
                                    <span class="title">Banks Detail</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <li class="<?php echo e($request->segment(2) == 'companies' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(url('index')); ?>">
                                    <span class="title">Fetch Attendance</span>
                                </a>
                            </li>
                        <li class="<?php echo e($request->segment(2) == 'companies' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(url('admin/device')); ?>">
                                    <span class="title">Devices</span>
                                </a>
                            </li>
                            <li class="<?php echo e($request->segment(2) == 'companies' ? 'active active-sub' : ''); ?>">
                                <a class="sub-side-menu__item" href="<?php echo e(url('admin/holiday')); ?>">
                                    <span class="title">Goverment Holidays </span>
                                </a>
                            </li>
</ul>
</li>
<?php if(Auth::user()->hasRole('admin')): ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_roles_manage',$per)): ?>

                <li class="slide <?php echo e(($request->segment(2) == 'roles' || $request->segment(1) == 'role' ||$request->segment(2) == 'candidate-hire') ? 'active' : ''); ?>">
                  <a href="<?php echo e(route('admin.leave_management.index')); ?>" data-toggle="slide" class="side-menu__item">
                    <i class="side-menu__icon fa fa-calendar"></i>
                        <span class="side-menu__label">Roles And Permission</span>
                        </a>
                        

                    <ul class="slide-menu">
                        <li class="<?php echo e($request->segment(2) == 'roles' ? 'active active-sub' : ''); ?>">
                            <a class="sub-side-menu__item" href="<?php echo e(route('admin.roles.index')); ?>">
                            
                                <span class="title">ERP</span>
                            </a>
                        </li>
                        <li class="<?php echo e($request->segment(1) == 'role' ? 'active active-sub' : ''); ?>">
                            <a class="sub-side-menu__item" href="<?php echo e(route('role')); ?>">
                  
                                <span class="title">Academics</span>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php endif; ?>
                </ul>
            </div>
        </aside>