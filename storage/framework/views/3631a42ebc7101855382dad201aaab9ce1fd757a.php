
<div class="form-group col-md-4 <?php if($errors->has('name')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('name', 'Name*', ['class' => 'control-label']); ?>

    <?php echo Form::text('name', old('name'), ['class' => 'form-control','minlength'=>'3','maxlength'=>'20']); ?>

    <?php if($errors->has('name')): ?>
        <span class="help-block">
            <?php echo e($errors->first('name')); ?>

        </span>
    <?php endif; ?>
</div>
<div class="form-group col-md-4 <?php if($errors->has('name')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('description', 'Description*', ['class' => 'control-label']); ?>

    <?php echo Form::text('description', old('description'), ['class' => 'form-control','maxlength'=>'20']); ?>

</div>

