<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<?php $per = PermissionHelper::getUserPermissions();?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Tax Laws</h3>
               <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('erp_laws_create',$per)): ?>
                <a href="<?php echo e(route( 'tax-law-add' )); ?>" style = "float:right; margin-bottom:20px;" class="btn btn-success pull-right">Define New Tax Law</a>
                <?php endif; ?>
            
        </div>
        <!-- /.box-header -->
      
									<table style = " width:100%" id="example" class="table table-bordered table-striped <?php echo e(count($Law) > 0 ? 'datatable' : ''); ?>">
										<thead>
											<tr>
                                            <th>S.No</th>
                                            <th>Law Section</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                
											</tr>
										</thead>
										<tbody>
											
										
									
                                            <?php if(count($Law) > 0): ?>
                    <?php $__currentLoopData = $Law; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $settings): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($settings->id); ?>">
                            <td><?php echo e($settings->law_id); ?></td>
                            <td><?php echo e($settings->law_section); ?></td>
                            <td><?php echo e($settings->description); ?></td>
                            <td style = "float:left;">
                                      

                                        <a href="<?php echo e(route('tax-law-edit', [$settings->law_id])); ?>"
                                            class="btn btn-xs btn-info">Edit</a>
                                   

                                        <?php echo Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['destroy-law', $settings->law_id],
                                        ]); ?>

                                        <?php echo Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger']); ?>

                                        <?php echo Form::close(); ?>  


                                </td> 
                       
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php endif; ?>
										</tbody>
									</table>
								
	
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>