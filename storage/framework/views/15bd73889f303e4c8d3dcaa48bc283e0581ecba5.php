<?php $request = app('Illuminate\Http\Request'); ?>
<?php $helper = app('App\Helpers\Helper'); ?>


<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Training Requests</h1>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($trainings) > 0 ? 'datatable' : ''); ?>">
                <thead>
                    <tr>
                        <th>Trainer Given By</th>
                        <th>Avalibility</th>
                        <th>Venue</th>
                        <th>Category</th>
                        <th>Department</th>
                        <th>Designation</th>
                        <th>Branch</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>No of Days</th>
                        <th>Training Charges</th>
                        <th>Training Type</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($trainings) > 0): ?>
                        <?php $__currentLoopData = $trainings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $training): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr data-entry-id="<?php echo e($training->id); ?>">
                                 <td><?php echo e($training->training_by); ?></td>
                                 <?php if($training->avalibility == 0): ?>
                                 <td>No Available</td>
                                 <?php elseif($training->avalibility == 1): ?>
                                 <td>Available</td>
                                 <?php endif; ?>
                                 <td><?php echo e($training->venue); ?></td>

                                 <td><?php echo e($training->category); ?></td>
                                 <td><?php echo e($training->department); ?></td>
                                 <td><?php echo e($training->designation); ?></td>
                                <td> <?php echo e(\App\Helpers\Helper::branchIdToName($training->branch_id)); ?></td>

                                <td><?php echo e($training->date); ?></td>
                                <td><?php echo e($training->time); ?></td>
                                <td><?php echo e($training->no_of_days); ?></td>
                                <td><?php echo e($training->training_charges); ?></td>
                                <td><?php echo e($training->training_type); ?></td>
                              
                            
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
            
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>