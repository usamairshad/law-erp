<?php $request = app('Illuminate\Http\Request'); ?>
<?php $helper = app('App\Helpers\Helper'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>






   <!----------------------------------------modal for branch transfer------------------------------------>

    <div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Transfer Branch </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row ">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <form method="POST" action="<?php echo e(route('admin.branch-transfer.store')); ?>">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="form-group">

                                        <label for="exampleInputEmail1">Branch</label>
                                        <br>
                                        <select style="width: 100%" class=" form-control" name="branch_id" >

                                            <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <option value="<?php echo e($branch->id); ?>"><?php echo e($branch->name); ?></option>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        </select>

                                        <input type="hidden" name="user_id" id="transfer_staff_id">

                                    </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <h4>Previous transfer status</h4>

                                <table class="table table-light">
                                    <thead>
                                        <tr>
                                            <td>Branch Name</td>
                                            <td>Staff Name</td>
                                            <td>Status</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td id="branch_name_transfer"></td>
                                            <td id="staff_name_transfer"></td>
                                            <td id="status_name_transfer"></td>
                                            <td><a id="action_name_transfer" href="">Delete</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Staff Details</h3>
            <?php if(Gate::check('erp_staff_create')): ?>
                <a href="<?php echo e(route('admin.staff.create')); ?>"style = "float:right" class="btn btn-success pull-right">Add New Staff</a>
            <?php endif; ?>

        </div>
        <!-- /.box-header -->

        <div class="panel-body pad table-responsive">
            <?php if($message = Session::get('success')): ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong><?php echo e($message); ?></strong>
                </div>
            <?php endif; ?>
            <table class="table table-bordered table-striped <?php echo e(count($staffs) > 0 ? 'datatable' : ''); ?>">
                <thead>

                    <tr>
                        <th>Branch Name</th>
                        <th>Name</th>
                        <th>Staff Reg no</th>
                        <th>Job Status</th>
                        <th>Staff Type</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($staffs) > 0): ?>
                        <?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php if($Employee->staff_type != 'admin'): ?>
                            <tr data-entry-id="<?php echo e($Employee->id); ?>">

                                <td><?php echo e(\App\Helpers\Helper::branchIdToName($Employee->branch_id)); ?></td>
                                <td><?php echo e($Employee->first_name . ' ' . $Employee->middle_name . ' ' . $Employee->last_name); ?></td>
                                <td><?php echo e($Employee->reg_no); ?></td>
                                <td><?php echo e($Employee->job_status); ?></td>
                                <td><?php echo e($Employee->staff_type); ?></td>
                                <td><?php echo e($Employee->email); ?></td>
                                <td>
                                    <!--  <?php if($Employee->status && Gate::check('erp_staff_inactive')): ?> <?php echo Form::open([
    'style' => 'display: inline-block;',
    'method' => 'PATCH',
    'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
    'route' => ['admin.staff.inactive', $Employee->id],
]); ?>

                                                                            <?php echo Form::submit('InActivate', [
    'class' => 'btn btn-xs btn-warning',
]); ?>

                                                                            <?php echo Form::close(); ?>


        <?php elseif(!$Employee->status && Gate::check('erp_staff_active')): ?>
                                                                            <?php echo Form::open([
    'style' => 'display: inline-block;',
    'method' => 'PATCH',
    'onsubmit' => "return confirm('Are you sure to activate this record?');",
    'route' => ['admin.staff.active', $Employee->id],
]); ?>

                                                                            <?php echo Form::submit('Activate', [
    'class' => 'btn btn-xs btn-primary',
]); ?>

                                                                    <?php echo Form::close(); ?> <?php endif; ?> -->

                             

                                    <?php if(Gate::check('erp_staff_edit')): ?>
                                        <a href="<?php echo e(route('admin.staff.show', [$Employee->id])); ?>"
                                            class="btn btn-xs btn-info">View Profile</a>
                                    <?php endif; ?>




                                    <!--if blade for active in active check--->
                                    <?php if($Employee->user->active == 1): ?>

                                        <a href="javascript:void(0)" data-user-id="<?php echo e($Employee->user_id); ?>"
                                            data-target="#warningModal" data-toggle="modal"
                                            class="btn btn-xs btn-warning warning">Warning

                                        </a>
                                        <a href="<?php echo e(url('edit-staff',$Employee->id)); ?>" class="btn btn-xs btn-success" >Edit</a>
                                        <a href="<?php echo e(url('admin/terminate/user') . '/' . $Employee->user_id); ?>"
                                            onclick="return confirm('Are you sure ');"
                                            data-user-id="<?php echo e($Employee->user_id); ?>"
                                            class="btn btn-xs btn-danger terminate">Terminate

                                        </a>



                                        <!--a style="color:white; background-color:#90989E" href="javascript:void(0)" <?php if($Employee->stafftransferbranch && $Employee->stafftransferbranch->user_id == $Employee->user_id): ?> data-name="<?php echo e($Employee->first_name); ?>" data-branch-name="<?php echo e($Employee->stafftransferbranch->branch->name ?? ''); ?>" <?php endif; ?>
                                            data-status="<?php echo e($Employee->stafftransferbranch->staff_status ?? ''); ?>"
                                            data-target="#transferModal" data-toggle="modal"
                                            data-user-id="<?php echo e($Employee->user_id); ?>"
                                            class="btn btn-xs transferBranchButton">

                                            Transfer Branch

                                        </a -->



                                     

                                    <?php else: ?>

                                        <a href="<?php echo e(url('admin/re-activate/user/') . '/' . $Employee->user_id); ?>"
                                            onclick="return confirm('Are you sure ');"
                                            data-user-id="<?php echo e($Employee->user_id); ?>"
                                            class="btn btn-xs btn-success">Re-Activate

                                        </a>

 

                                    <?php endif; ?>


                                    <!-------------------check teacher has assign courses or not---------------->

                                    <?php if($helper::checkAssignCourses($Employee->id)): ?>

                                        <!--button data-toggle="modal" data-target=".infoModal"
                                            class="btn btn-xs btn-primary"><i
                                                class=" fa fa-info-circle "></i></button -->


                                    <?php endif; ?>

                                </td>
                            </tr>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>


    <!----------------------------------------------info modal ---------------------------------->


    <div class="modal fade infoModal" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Info !</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row ">
                            <p>Hye !</p>

                            <p>This user dont have any assign courses yet ! </p>

                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>


    <!-------xxxxxxxxxxxxxxxxxx-------------info modal end-------xxxxxxxxxxxxxxxxxxxx------------------>






    <!--------------------------------------modal for Warning------------------------------------>

    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Give Warning!</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">

                            <form method="POST" action="<?php echo e(route('admin.termination.store')); ?>" style="width: 100%;" >
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control col-lg-12">
                                </div>

                                <div class="form-group">

                                    <label for="exampleInputEmail1">Reason</label>
                                    <br>

                                    <textarea  class="from-control col-lg-12" name="reason" id="reason" rows="5"></textarea>

                                    <input type="hidden" name="user_id" id="warning_user_input">

                                </div>



                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!---------------------------------------------modal for Terminate------------------------------------>

    <div class="modal fade" id="terminateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Terminate ?</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row ">

                            <form method="POST" action="<?php echo e(route('admin.termination.store')); ?>">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">

                                    <label for="exampleInputEmail1">Reason</label>
                                    <br>

                                    <textarea style="width: 100%" class="from-control" name="reason" id="" cols="30"
                                        rows="10"></textarea>

                                    <input type="hidden" name="user_id" id="terminate_user_input">

                                </div>


                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>



 

    <!----------------------------------------modal for promotion demotion------------------------------------>

    <div class="modal fade" id="promotionDemotionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Promotion/Demotion </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row ">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <h4 class="text-center">Promote/Demote Rank</h4>

                                <form class="form-inline" method="POST" action="<?php echo e(route('admin.PD.store')); ?>">
                                    <?php echo e(csrf_field()); ?>

                                    <div style="margin-right: 5%" class="form-group">
                                        <label for="">Name</label>
                                        <input readonly id="user_name_promotion" class="form-control" type="text" name=""
                                            id="">

                                    </div>
                                    <input type="hidden" id="promotion_user_id" name="user_id">

                                    <div style="margin-right: 5% !important" class="form-group col-centered">
                                        <label for="">Rank</label>
                                        <select class="js-example-basic-multiple roles" name="role_id" id="">
                                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <option value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>

                                    </div>

                                    <div class="form-group col-centered">

                                        <button type="submit" class="btn btn-default">Submit</button>

                                    </div>

                                </form>
                            </div>



                            


                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script>


        

        $(".warning").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#warning_user_input').val($user_id);

        });


        $(".terminate").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#terminate_user_input').val($user_id);

        });


        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });


        $(".transferBranchButton").on("click", function() {

            $user_id = $(this).data('user-id');
            $branch_name = $(this).data('branch-name');
            $name = $(this).data('name');
            $status = $(this).data('status');



            console.log($user_id, $branch_name, $name, $status);



            $('#transfer_staff_id').val();
            $('#branch_name_transfer').text('');
            $('#staff_name_transfer').text('');
            $('#status_name_transfer').text('');



            $('#transfer_staff_id').val($user_id);
            $('#branch_name_transfer').text($branch_name);
            $('#staff_name_transfer').text($name);
            $('#status_name_transfer').text($status);

            $('#action_name_transfer').attr("href", `<?php echo e(url('admin/delete/staff/branch')); ?>/${$user_id}`);



        });

        $('.promotionDemotion').on("click", function() {

            $name = $(this).data('user-name');
            $user_role_id = $(this).data('user-role-id');
            $user_id = $(this).data('user-id');
            $branch_id = $(this).data('branch-id');

            $('#user_name_promotion').val($name);

            $('#promotion_user_id').val($user_id);
            $('#staff_user_id').val($user_id);
            $("#active_teacher_id").val($user_id);
            console.log($user_id);
            $(".roles").select2().val($user_role_id).trigger('change');


            loadBorad($branch_id);

        });

        function loadBorad($branch_id) {
            $.ajax({
                url: `<?php echo e(url('admin/getBoard/')); ?>/${$branch_id}`,

                success: function(response) {
                    $html = '<option value="null">Select board</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.board_id}>${res.board.name}</option>`;

                    });

                    $("#boards").data("branch-id", $branch_id);
                    $("#boards").html($html);
                }
            });
        }
        ////////////////on board change get program/////////////// 
        $("#boards").on("change", function() {

            $board_id = $(this).find(":selected").val();
            $branch_id = $(this).data('branch-id');

            loadProgram($branch_id, $board_id);
        });

        function loadProgram($branch, $board) {
            $.ajax({
                url: `<?php echo e(url('admin/getProgram/')); ?>/${$branch}/${$board}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select program</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.program_id}>${res.program.name}</option>`;

                    });

                    $("#programs").data('branch-id', $branch);
                    $("#programs").data('board-id', $board);
                    $("#programs").html($html);
                }
            });
        }

        ////////////////on program change get class/////////////// 
        $("#programs").on("change", function() {

            $board_id = $(this).data("board-id");
            $branch_id = $(this).data('branch-id');
            $program_id = $(this).find(':selected').val();

            console.log($board_id, $branch_id, $program_id);

            loadClasses($branch_id, $board_id, $program_id);
        });

        function loadClasses($branch, $board, $program) {
            $.ajax({
                url: `<?php echo e(url('admin/getClasses/')); ?>/${$branch}/${$board}/${$program}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select class</option>';

                    response.forEach((res) => {

                        $html +=
                            `<option data-active-s-id=${res.id}  value=${res.get_class.id}>${res.get_class.name}</option>`;

                    });

                    $("#classes").html($html);
                }
            });
        }
        $("#classes").change(function() {
            $activeSessionID = $(this).find(":selected").data("active-s-id");
            $("#active_session_id").val($activeSessionID);
            console.log($activeSessionID);
        });


        $("#classes").change(function() {

            $activeSessionID = $(this).find(":selected").data("active-s-id");

            $("#active_session_id").val($activeSessionID);
            console.log($activeSessionID);
        });



        
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>