<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Academic Year</h3>
                <?php if(Gate::check('erp_academic_years_create')): ?>
                    <a href="<?php echo e(route('admin.academic_year.create')); ?>" style = "float:right;" class="btn btn-success pull-right">Add New Academic Year</a>
                <?php endif; ?>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($AcademicYear) > 0 ? 'datatable' : ''); ?>">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Start Month</th>
                    <th>End Month</th>
                    <th>Start_Year</th>
                    <th>End Year</th>
                     <th>Status</th>
                    <th>TimeStamp</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                <?php if(count($AcademicYear) > 0): ?>
                    <?php $__currentLoopData = $AcademicYear; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $AcademicYears): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($AcademicYears->id); ?>">
                            <td><?php echo e($AcademicYears->id); ?></td>
                            <td><?php echo e($AcademicYears->name); ?></td>
                            <td><?php echo e($AcademicYears->start_month); ?></td>
                            <td><?php echo e($AcademicYears->end_month); ?></td>
                            <td><?php echo e($AcademicYears->start_year); ?></td>
                            <td><?php echo e($AcademicYears->end_year); ?></td>
                            <td><?php echo e($AcademicYears->status); ?></td>
                            <td><?php echo e($AcademicYears->created_at); ?></td>
                            <td>
                                <?php if($AcademicYears->status == 1): ?>
                                 <?php echo Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['admin.academic_year.inactive', $AcademicYears->id])); ?>

                                    <?php echo Form::submit('InActivate', array('class' => 'btn btn-xs btn-warning')); ?>

                                    <?php echo Form::close(); ?>


                                    <?php elseif($AcademicYears->status == 0): ?>
                                    <?php echo Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['admin.academic_year.active', $AcademicYears->id])); ?>

                                    <?php echo Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')); ?>

                                    <?php echo Form::close(); ?>

                                    <?php endif; ?>
                            </td>
                            
                            
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="6"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>