<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Add New Staff</h3>
            <a href="<?php echo e(route('admin.staff.index')); ?>" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>


       <?php echo Form::open(['method' => 'POST', 'route' => ['admin.staff.store'], 'id' => 'validation-form' , 'enctype' => 'multipart/form-data']); ?> 
       <?php echo Form::token();; ?>

        <div class="box-body" style = "margin-top:50px;">
            <?php echo $__env->make('hrm.staff.field', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="box-footer">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo e(url('public/js/admin/employees/create_modify.js')); ?>" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
           
            $('.phone').mask('0000-0000000');
            $('.cnics').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');   
        })
    </script>
    
<script type="text/javascript">
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
    
});

$(".radio").change(function(){

if($(this).val()=="no")
{
  $('#banknotexist').show();
  $('#bankexist').hide();
}
else
{

  $('#banknotexist').hide();
  $('#bankexist').show();
}

});
</script>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

$(".salary").change(function() {
  // body...


  var salary=0;
  var basic_salary=0;
  var medical_allownace=0;
  var conveyance=500;
  var house_rent=0;
  salary=parseInt($("#salary").val());
  basic_salary=(80*salary)/100;

  $("#basic_salary").val(basic_salary);

  medical_allownace=(basic_salary*10)/100;

  $("#medical").val(medical_allownace);

  $("#conveyance").val(conveyance);



  house_rent=salary-basic_salary-medical_allownace-conveyance;
  
  $("#house_rent").val(house_rent);


});


</script>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>