<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Bank Details</h3>
            
            <a href="<?php echo e(route('admin.bank_detail.create')); ?>" style = "float:right;" class="btn btn-success pull-right"><?php echo app('translator')->getFromJson('global.app_add_new'); ?></a>
            
        </div>
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-bBanked table-striped <?php echo e(count($BankDetail) > 0 ? 'datatable' : ''); ?> dt-select" id="users-table">
                <thead>
                <tr>
                    <th>S: No</th>
                    <th>Bank Name</th>
                    <th>Branch Code</th>
                    <th>Account No</th>
                    <th>Bank Address</th>
                    <th>Phone No</th>
                    <th>Action</th>

                </tr>
                </thead>

                <tbody>
                <?php ($r = 1); ?>
                <?php if(count($BankDetail) > 0): ?>
                    <?php $__currentLoopData = $BankDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $BankDetails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr data-entry-id="<?php echo e($BankDetails->id); ?>">
                            <td> <?php echo e($r); ?> </td>
                            <td><?php echo e($BankDetails->banksModel->bank_name); ?></td>
                            <td><?php echo e($BankDetails->branch_code); ?></td>
                            <td><?php echo e($BankDetails->account_no); ?></td>
                            <td><?php echo e($BankDetails->bank_address); ?></td>
                            <td><?php echo e($BankDetails->phone_no); ?></td>
                            <td>
                                <a href="<?php echo e(route('admin.bank_detail.edit',[$BankDetails->id])); ?>" class="btn btn-xs btn-info"><?php echo app('translator')->getFromJson('global.app_edit'); ?></a>

                                
                                    
                                    
                                    
                                    
                                
                                
                            </td>

                        </tr>
                        <?php ($r++); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php else: ?>
                    <tr>
                        <td colspan="3"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('#users-table').DataTable();
        window.route_mass_crud_entries_destroy = '<?php echo e(route('admin.bank_detail.mass_destroy')); ?>';
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>