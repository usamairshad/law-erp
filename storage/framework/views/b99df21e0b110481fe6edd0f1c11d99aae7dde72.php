<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">branches List</h3>
            <?php if(Gate::check('erp_branches_create')): ?>
                <a href="<?php echo e(route('admin.branch-create',[$company_id])); ?>" style = "float:right;" class="btn btn-success pull-right">Add New Branch</a>
                <a href="<?php echo e(route('admin.companies.index')); ?>" style = "float:right;margin-right:20px;" class="btn btn-success pull-right">Back</a>
            <?php endif; ?>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($Branches) > 0 ? 'datatable' : ''); ?>">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Branch Code</th>
                    <th>Assign Board Program</th>
                    <th>View Board Program</th>
                    <th>Address</th>
                    
                </tr>
                </thead>

                <tbody>

                <?php if(count($Branches) > 0): ?>
                    <?php $__currentLoopData = $Branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $Branche): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($Branche->id); ?>">
                            <td><?php echo e($Branche->name); ?></td>
                            <td><?php echo e($Branche->branch_code); ?></td>
                            <td> 
                                <a class="btn btn-xs btn-primary assign-modal-anchor" data-toggle="modal" data-training-id="<?php echo e($Branche->id); ?>" data-target="#exampleModal1">
                                    <button class="btn btn-primary">Assign Board Program</button>
                                </a>                             
                            </td>
                                                        
                            <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-body">
                                      <?php echo Form::open(['route' => 'admin.branch-board-program-store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]); ?>

                                      <!-- <input type="hidden" name="branch_id" value="<?php echo e($Branche->id); ?>"> -->
                                       <input type="hidden" name="branch_id" id="training_id_input">
                                        <div class="form-group">
                                            <?php echo Form::label('board_id', 'Board*', ['class' => 'control-label']); ?>


                                            <select required="required" class="form-control" name="board_id" id="board_select">
                                                <option value="0" selected>Select Board</option>

                                                <?php $__currentLoopData = $boards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $board): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($board['id']); ?>"><?php echo e($board['name']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <?php echo Form::label('programs', 'Programs*', ['class' => 'control-label']); ?>

                                            <select style="width: 100%" class="form-control programs"
                                            name="program_id[]" id="program_select" multiple="multiple">

                                            </select>

                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    <?php echo Form::close(); ?>

                                  </div>           
                                </div>
                            </div>   
                            <td>

                                <a  data-id="<?php echo e($Branche->id); ?>" class="btn btn-xs btn-primary branch-board-program" data-toggle="modal" data-target="#exampleModal2<?php echo e($Branche->id); ?>"><span>View</span></a>
                                <a  data-id="" class="btn btn-xs btn-primary branch_detail" data-branch-detail="<?php echo e($Branche); ?>" data-city-name="<?php echo e($Branche->cities()->get()); ?>" data-toggle="modal" data-target="#branch_detail"><span>Branch Detail</span></a>

                                <div class="modal fade" id="exampleModal2<?php echo e($Branche->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-body">
                                            <h2>Assigned Board  Program</h2>
                                            <table class="table table-striped">
                                                <thead>
                                                    <th>Board</th>
                                                    <th >Program</th>

                                                </thead>
                                                <tbody class="body">
                                                </tbody>
                                            </table>
                                        </div>
                                      </div>           
                                    </div>
                                </div>
                            </td>


                            <td><?php echo e($Branche->address); ?></td>

                            <td>
                                <?php if(Gate::check('erp_branches_edit')): ?>
                                   
                                <?php endif; ?>

                                
                                    
                                        
                                        
                                        
                                        
                                    
                                    
                                
                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div class="modal fade" id="branch_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div style="padding: 10px">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Name</th>
                            <th>City</th>
                        </tr>
                        <tr>
                            <td class="name"></td>
                            <td class="city"></td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <th>Fax</th>

                        </tr>
                        <tr>
                            <td class="phone"></td>
                            <td class="fax"></td>
                        </tr>
                        <tr>
                            <th>Mobile Number</th>
                            <th>Address</th>
                        </tr>
                        <tr>
                            <td class="cell"></td>
                            <td class="address"></td>
                        </tr>
                        <tr>
                            <th>Branch Code</th>
                        </tr>
                        <tr>
                            <td class="branch_code"></td>
                        </tr>
                    </table>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script>
        $('.branch_detail').on('click',function(){


            var name =  $(this).data('branch-detail').name;
            var branch_code =  $(this).data('branch-detail').branch_code;
            var phone =  $(this).data('branch-detail').phone;
            var fax =  $(this).data('branch-detail').fax;
            var mobile_number =  $(this).data('branch-detail').cell;
            var address =  $(this).data('branch-detail').address;
            var city_name =  $(this).data('city-name')[0].name;

            $('.name').text(name);
            $('.city').text(city_name);
            $('.branch_code').text(branch_code);
            $('.phone').text(phone);
            $('.fax').text(fax);
            $('.cell').text(mobile_number);
            $('.address').text(address);
        });
        $(document).ready(function() {
          selectRefresh();
        });
        
        function selectRefresh() {
            $(".programs").select2({

            });
             $(".select2")[2].style.width = "100%";
        }

        $(document).ready(function(){
            $('.datatable').DataTable()
        });

        $(function(){
            $(".branch-board-program").on('click', function () {
               var id=  $(this).data('id');
               $.ajax({
                  url : '<?php echo e(route( 'admin.load-branch-board-programs' )); ?>',
                  data: {
                    "_token": "<?php echo e(csrf_token()); ?>",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {
                    var res='';
                    $.each (result, function (key, value) {
                    res +=
                    '<tr>'+
                        '<td>'+value.board_id+'</td>'+
                        '<td>'+value.program_id+'</td>'+
                   '</tr>';

                    });

                    $('.body').html(res);
                  },
                  error: function()
                 {
                     // alert('error...');
                 }
               });
            });
        });


        $("#board_select").on("change", function () {
            $board_id = $(this).find(":selected").val();
            console.log($board_id)
            getProgramByBoard($board_id);

        });

        $('.assign-modal-anchor').on("click", function () {

            $training_id = $(this).data("training-id");

            $("#training_id_input").val($training_id);

        });

        function getProgramByBoard($board_id) {

            $.ajax({
                 url: "<?php echo e(url('admin/board/loadPrograms')); ?>/" + $board_id,
                success: function (res) {
                    console.log(res)
                    $html = "";
                    $('#program_select').html("");

                    res.forEach(data => {

                        $html += `<option value="${data[0].id}">${data[0].name}</option>`;
                    });

                    $('#program_select').append($html);

                }
            });

        }

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>