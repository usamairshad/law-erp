<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create TimeTable Category</h3>
            <a href="<?php echo e(route('admin.timetable_category.index')); ?>" style ="float:right;" class="btn btn-success pull-right">Back</a>
        </div>


       <?php echo Form::open(['method' => 'POST', 'route' => ['admin.timetable_category.store'], 'id' => 'validation-form' , 'enctype' => 'multipart/form-data']); ?> 
       <?php echo Form::token();; ?>

        <div class="box-body" style= "margin-top:40px;">
            <?php echo $__env->make('hrm.timetable_category.field', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="box-footer" style  ="padding-top:25px;">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo e(url('public/js/admin/employees/create_modify.js')); ?>" type="text/javascript"></script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>