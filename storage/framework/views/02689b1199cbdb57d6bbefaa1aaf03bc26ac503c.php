<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Session</h3>
            <?php if(Gate::check('erp_session_create')): ?>
            <a href="<?php echo e(route('admin.active-session.index')); ?>" style = "float:right;" class="btn btn-success pull-right">Back</a>
            <?php endif; ?>

        </div>

       <?php echo Form::open(['method' => 'POST', 'route' => ['admin.active-session.store'], 'id' => 'validation-form']); ?>

       <?php echo Form::token();; ?>

        <div class="box-body" style = "margin-top:40px;">
            <?php echo $__env->make('admin.active_session.field', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="box-footer">
            <button class="btn btn-primary" style = "margin-top:27px;" type="submit">Save</button>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo e(url('public/js/admin/employees/create_modify.js')); ?>" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
        })
    </script>

    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById('btn').setAttribute("disabled",null);
            $( document ).change(function(){
                var e = document.getElementById("classes");
                var strUser = e.value;
                if (strUser != "") {
                document.getElementById('btn').removeAttribute("disabled");
                } else {
                    document.getElementById('btn').setAttribute("disabled", null);
                }
            })
            
        });
         $(".branches").select2({

            });
    </script>
    <script type="text/javascript">
        function selectRefresh() {
            $(".branches").select2({

            });
        }
        function selectRefresh() {
            $(".classes").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
    <script type="text/javascript">


    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove" style="height:120px"><hr style="border-top:1px solid #222d32"><div class="form-group col-md-3"><select id="board" class="form-control select2 branches" name="board_id[]"><?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($branch['id']); ?>"><?php echo e($branch['name']); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></div><div class="form-group col-md-3"><select class="form-control select2 branches" name="branches" id="branches"></select></div><br> <div class="form-group col-md-2"><button style="margin-top: -27px;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');
            $('.select2').select2(); 

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    // function showBranch() {
    //     var data= $('input[name="branch_name"]').val();

    //     if(data)
    //     {
    //          $('#show').show();
    //          $('#btn').hide(); 
    //     }
    // }
    </script>
    <script src="<?php echo e(url('public/js/admin/companies/create_modify.js')); ?>" type="text/javascript"></script>
     <script>
        $(function(){
            $('#board').change(function(){
               $("#programs option").remove();;
               var id = $(this).val();
               $.ajax({
                  url : '<?php echo e(route( 'admin.board-loadPrograms' )); ?>',
                  data: {
                    "_token": "<?php echo e(csrf_token()); ?>",
                    "id": id
                    },
                  type: 'post',
                  dataType: 'json',
                  success: function( result )
                  {
                        $.each( result, function(k, v) {
                            $('#branches').append($('<option value=' +  v[0].id + '>' + v[0].name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>