<?php $request = app('Illuminate\Http\Request'); ?>
<?php $helper = app('App\Helpers\Helper'); ?>


<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Class Timetable</h1>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
             <?php if(Gate::check('erp_class_time_table_create')): ?>
                <a href="<?php echo e(route('admin.class_timetable.create')); ?>" class="btn btn-success pull-right">Add New Class TimeTable</a>
            <?php endif; ?>

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($class_timetables) > 0 ? 'datatable' : ''); ?>">
                <thead>
                    <tr>
                        <th>Time Table Name</th>
                        <th>Subject</th>
                        <th>Day</th>
                        <th>Action</th>
                        <th>Slot</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($class_timetables) > 0): ?>
                        <?php $__currentLoopData = $class_timetables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $class_timetable): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr data-entry-id="<?php echo e($class_timetable->id); ?>">
                                 <td><?php echo e(\App\Helpers\Helper::timetableIdToTitle($class_timetable->time_table_id)); ?></td>
                               <td><?php echo e(\App\Helpers\Helper::subjectIdToName($class_timetable->subject_id)); ?></td>
                                <td><?php echo e($class_timetable->day); ?></td>
                                <td>
                                    <?php if(Gate::check('erp_class_time_table_destroy')): ?>
                                        <?php echo Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['admin.class_timetable.destroy', $class_timetable->id],
                                        ]); ?>

                                        <?php echo Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger']); ?>

                                        <?php echo Form::close(); ?>

                                    <?php endif; ?>
                                </td>
                                <td><?php echo e($class_timetable->timetable->start_time." - ".$class_timetable->timetable->end_time); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
            
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>