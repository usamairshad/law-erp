<?php $request = app('Illuminate\Http\Request'); ?>
<?php $helper = app('App\Helpers\Helper'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">ID Card Request</h3>


        <div class="panel-body table-responsive">

            <table class="table table-bBanked table-striped  dt-select">
                <thead>
                <tr>
                    <th>Branch Name</th>
                    <th>Employee Name</th>
                    <th>Employee Role</th>
                    <th>Status</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>

                        <td><?php echo e($record->branch_name); ?></td>
                        <td><?php echo e($record->name); ?></td>
                        <td><?php echo e($record->role); ?></td>
                        <td><?php echo e($record->status); ?></td>
                        <td><?php echo e($record->created_at); ?></td>
                        <td><a href="<?php echo e(url('approve-request',$record->id)); ?>" id="not-active" class="btn btn-primary"> 
                            <?php if($record->status == 'approved'): ?>
Approved

<?php else: ?>
Approve
                          <?php endif; ?>



                        </a>

                    </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>







<?php $__env->startSection('javascript'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>