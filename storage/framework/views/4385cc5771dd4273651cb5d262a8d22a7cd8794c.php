<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Course Labs</h3>
            <a  href="<?php echo e(route('admin.course-lab')); ?>" style="float:right;" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($data) > 0 ? 'datatable' : ''); ?>">
                <thead>
                <tr>
                    <th>Board</th>
                    <th>Program</th>
                    <th>Course</th>
                    <th>Lab</th>
                </tr>
                </thead>

                <tbody>
                <?php if(count($data) > 0): ?>

                   <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($d['id']); ?>">
                            <td><?php echo e(App\Helpers\Helper::activeSessionIdToBoard($d->active_session_id)); ?></td>
                            <td><?php echo e(App\Helpers\Helper::activeSessionIdToProgram($d->active_session_id)); ?></td>
                            <td><?php echo e(App\Helpers\Helper::subjectIdToName($d->course_id)); ?></td>
                            <td><?php echo e(App\Helpers\Helper::labIdToName($d->lab_id)); ?></td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>