<?php $request = app('Illuminate\Http\Request'); ?>
<?php $helper = app('App\Helpers\Helper'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Teacher Course Details</h3>
 
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($data) > 0 ? 'datatable' : ''); ?>">
                <thead>
                    <tr>
                        <th>Timetable Title</th>
                        <th>Branch Name</th> 
                        <th>Staff Name</th>
                        <th>Course</th>
                        <th>Rates</th>
                        <th>Lectures Per Month</th>
                        <th>Day</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($data) > 0): ?>
                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr data-entry-id="<?php echo e($Employee->id); ?>">
                                <td><?php echo e($Employee->title); ?></td>
                                <td><?php echo e(\App\Helpers\Helper::branchIdToName($Employee->branch_id)); ?></td>
                                <td><?php echo e(\App\Helpers\Helper::getStaffIDToFirstName($Employee->staff_id)); ?></td>
                                 <td><?php echo e(\App\Helpers\Helper::subjectIdToName($Employee->subject_id)); ?></td>
                                <td><?php echo e($Employee->rates); ?></td>
                                <td><?php echo e($Employee->days_in_month); ?></td>
                                <td><?php echo e($Employee->day); ?></td>
                                <td><?php echo e($Employee->start_time); ?></td>
                                <td><?php echo e($Employee->end_time); ?></td>
                                
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>




<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>