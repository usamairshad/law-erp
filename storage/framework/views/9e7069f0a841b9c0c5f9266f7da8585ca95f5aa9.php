<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Academic Groups</h3>
            <a  href="<?php echo e(route('admin.academics-groups.create')); ?>" style = "float:right;" class="btn btn-success pull-right">Create</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($ag) > 0 ? 'datatable' : ''); ?>">
                <thead>
                <tr>
                    <th>Program</th>
                    <th>Academic Group Name</th>
                    <th>Academic Group Courses</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
               <?php if(count($ag) > 0): ?>

                    <?php $__currentLoopData = $ag; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($program['id']); ?>">
                            <td><?php echo e(App\Helpers\Helper::programIdToName($program->program_id)); ?></td>
                            <td><?php echo e($program->name); ?></td>
                             <td><a  data-id="<?php echo e($program->id); ?>" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-xs btn-primary academics-courses" data-toggle="modal" data-target="#exampleModal2<?php echo e($program->id); ?>"><span>View Courses</span></a></td>

                             <td> <a href = "Academic-group-courses\<?php echo e($program['id']); ?>"><button style = "background-color: #0ba360;
    border-color: #0ba360; color:white;" class="btn btn-primary">Add Academic Group Course</button></a></td>
                             <div class="modal fade" id="exampleModal1<?php echo e($program->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-body">
                                      <?php echo Form::open(['route' => 'admin.academics-groups-courses-store','method' => 'POST', 'class' => 'form-horizontal' ,"enctype" => "multipart/form-data"]); ?>

                                      <input type="hidden" name="academics_group_id" value="<?php echo e($program->id); ?>">
                                        <div class="form-group">
                                            <?php echo Form::label('course_id', 'Courses', ['class' => 'control-label']); ?>

                                             <select style="width: 100%" name="course_id[]" class="form-control select2 courses" multiple="multiple">
                                                <option selected="selected" disabled="disabled">---Select---</option>
                                                <?php if(count($courses) > 0): ?>
                                                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($course->id); ?>"><?php echo e($course->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>    
                                             </select>
                                        </div>
                                        <div style="margin-top: 20px">
                                          
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    <?php echo Form::close(); ?>

                                  </div>           
                                </div>
                            </div>
                            <div class="modal fade" id="exampleModal2<?php echo e($program->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2 class="modal-title" id="exampleModalLabel">Academics Groups Courses</h2>
                                      
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                          <div id="days_<?php echo e($program->id); ?>">
                                              
                                          </div>
                           
                                        </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        
                                    </div>
                                  </div>
                                   
                                </div>
                            </div>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
        $(document).ready(function() {
          selectRefresh();
        });
        function selectRefresh() {
            $(".courses").select2({

            });
        }
        $(function() {
           $(".academics-courses").on('click', function () {   
                   var id=  $(this).data('id');
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo e(route('admin.load-academics-group-courses')); ?>',
                        data: {
                            _token: '<?php echo e(csrf_token()); ?>',
                            academics_group_id: id
                        },
                        success: function (response) {
                            $.each( response, function(k, v) {
                                if(k == 0)
                                {
                                    $("#days_"+v.academics_group_id).empty();
                                }
                                $("#days_"+v.academics_group_id).append('<input readonly type="text" class="form-control" value="'+v.course_id+'"><br/>');
                            });
                        }
                    });
            })
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>