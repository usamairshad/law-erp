<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Timetable Category</h3>
                <?php if(Gate::check('erp_time_table_category_create')): ?>
                <a href="<?php echo e(route('admin.timetable_category.create')); ?>" style = "float:right;" class="btn btn-success pull-right">Add New TimeTable Category</a>
                <?php endif; ?>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($timetable_category) > 0 ? 'datatable' : ''); ?>">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($timetable_category) > 0): ?>
                        <?php $__currentLoopData = $timetable_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $TimeTableCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr data-entry-id="<?php echo e($TimeTableCategory->id); ?>">
                                <td><?php echo e($TimeTableCategory->name); ?></td>
                                <td><?php echo e($TimeTableCategory->description); ?></td>
                                <td>
                                    <?php if(Gate::check('erp_staff_destroy')): ?>
                                        <?php echo Form::open([
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('" . trans('global.app_are_you_sure') . "');",
                                        'route' => ['admin.timetable_category.destroy', $TimeTableCategory->id],
                                        ]); ?>

                                        <?php echo Form::submit(trans('global.app_delete'), ['class' => 'btn btn-xs btn-danger']); ?>

                                        <?php echo Form::close(); ?>

                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>