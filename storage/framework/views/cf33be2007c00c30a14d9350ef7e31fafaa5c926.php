<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Active Session Section</h3>
            
            <a href="<?php echo e(url('active-session-section')); ?>" style = "float:right;" class="btn btn-success pull-right">Back</a>
            

        </div>

       <?php echo Form::open(['method' => 'POST', 'url' => ['active-session-section'], 'id' => 'validation-form']); ?>

       <?php echo Form::token();; ?>

        <div class="box-body" style = "margin-top:40px;">
            <?php echo $__env->make('admin.active-session-section.field', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="box-footer">
            <button class="btn btn-primary" type="submit">Save</button>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(url('public/js/admin/branches/create_modify.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>