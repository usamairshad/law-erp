<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Resign List</h1>
    </section>
    <?php echo e(csrf_field()); ?>

    <input type="hidden" name="name" value="abc">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>


        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($data) > 0 ? 'datatable' : ''); ?>"
                id="users-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Branch</th>
                        <th>Staff Role</th>
                        <th>Valid</th>
                        <th>Notice Period</th>
                        <th>Reason</th>
                        <th>Want Experience Letter</th>
                        <th>Approved By CEO</th>
                        <th>Approved By HR Head</th>
                       <th>Actions</th>
                    </tr>
                </thead>

                <tbody>

                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $resign): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                        <tr>

                            <td><?php echo e(++$key); ?></td>                          
                            <td><?php echo e(\App\Helpers\Helper::userIdToBranchName($resign->user->id)); ?></td>
                             <td><?php echo e(\App\Helpers\Helper::userIdToRole($resign->user->id)); ?></td>
                            <td><?php echo e($resign->valid); ?></td>
                            <td><?php echo e($resign->notice_period); ?></td>
                            <td><?php echo e($resign->reason); ?></td>
                            <td><?php echo e($resign->experience_letter); ?></td>
                            <td><?php echo e($resign->approved_by_ceo); ?></td>
                            <td><?php echo e($resign->approved_by_hr_head); ?></td>
                            <td>
                        
                                    <?php if($resign->approved_by_ceo == 'Pending' || $resign->approved_by_hr_head == 'Pending'): ?>
                                   <form method="POST" action="<?php echo e(url("admin/resign/$resign->id")); ?>" accept-charset="UTF-8"
                                        style="display: inline-block;" onsubmit="return confirm('Are you sure?');">

                                        <?php echo e(method_field('delete')); ?>

                                        <?php echo e(csrf_field()); ?>


                                        <input class="btn btn-xs btn-danger" type="submit" value="Delete">
                                    </form> 

                                   <?php elseif($resign->approved_by_ceo == 'Approved' || $resign->approved_by_hr_head == 'Approved'): ?>
                                    <p style="color:green"><b> Approved </b> </p>

                                    <?php elseif($resign->approved_by_ceo == 'Rejected' || $resign->approved_by_hr_head == 'Rejected'): ?>
                                    
                                     <p style="color:red"><b> Rejected </b> </p>
                                    <?php endif; ?>
                            </td>


                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>





                </tbody>




            </table>
        </div>
    </div>










<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>





<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>