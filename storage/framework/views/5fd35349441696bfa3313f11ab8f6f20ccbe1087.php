<!DOCTYPE html>
<html>
<head>
    <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div id="app">
    <div class="wrapper">

        <!-- Main Header -->
        <?php echo $__env->make('partials.topbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!-- Left side column. contains the logo and sidebar -->
        <?php echo $__env->make('partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <?php if(isset($siteTitle)): ?>
                    <h3 class="page-title">
                        <?php echo e($siteTitle); ?>

                    </h3>
                <?php endif; ?>

                <div class="row">

                    <?php echo $__env->yieldContent('breadcrumbs'); ?>

                    <div class="col-md-12">
                        <?php if(Session::has('message')): ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <p><?php echo e(Session::get('message')); ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if($errors->count() > 0): ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-warning"></i> Something went wrong!</h4>
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>

                    <div class="col-md-12">
                        <?php echo $__env->yieldContent('content'); ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
</div>
<!-- ./wrapper -->
<?php echo Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']); ?>

<button type="submit">Logout</button>
<?php echo Form::close(); ?>

<!-- REQUIRED JS SCRIPTS -->
<?php echo $__env->make('partials.javascripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatable').DataTable()
    });
    
</script>


</body>
</html>