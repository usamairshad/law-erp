<div class="form-group col-md-3 <?php if($errors->has('name')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('name', 'Tile*', ['class' => 'control-label']); ?>

    <input type="text" name="title" value="<?php echo e($session->title); ?>" placeholder="Title" class="form-control" required="required">
</div>
<div class="form-group col-md-3 <?php if($errors->has('start_year')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('start_year', 'Start Year*', ['class' => 'control-label']); ?>

    <input type="number" min="1900" max="2099" step="1" value="2021" class="form-control"/>
</div>
<div class="form-group  col-md-3 <?php if($errors->has('end_year')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('end_year', 'End Year*', ['class' => 'control-label']); ?>

    <input type="number" min="1900" max="2099" step="1" value="2022" class="form-control" />
</div>
<div class="form-group  col-md-3 <?php if($errors->has('status')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('status', 'Status*', ['class' => 'control-label']); ?>

    <?php echo Form::select('status',[ ''=>'Select  Status','Active' => 'Active','InActive' => 'InActive','Pending' => 'Pending'],old('status') , ['class' => 'form-control', 'required']); ?>

    <?php if($errors->has('status')): ?>
        <span class="help-block">
            <?php echo e($errors->first('status')); ?>

        </span>
    <?php endif; ?>
</div>