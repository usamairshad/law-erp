<?php $__env->startSection('content'); ?>
    

    <div class="row">
            <div class="col-md-4 col-md-offset-4" style = "margin-left:34%">
                <div class="panel login panel-default">
                    <div class="panel-body">
                        <div class="logo" style = "text-align:center;">
                             <img src="<?php echo e(asset('public/uploads/login-logo.png')); ?>" class="img-responsive">
                        </div>
                        <?php if(count($errors) > 0): ?>
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were problems with input:
                                <br><br>
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>
    
                        <form class="form-horizontal"
                              role="form"
                              method="POST"
                              action="<?php echo e(url('login')); ?>" style ="color: white;">
                            <input type="hidden"
                                   name="_token"
                                   value="<?php echo e(csrf_token()); ?>">
    
                            <div class="form-group">
                                <label class="col-md-12 control-label pull-left text-left">Email</label>
    
                                <div class="col-md-12">
                                    <input type="email"
                                           class="form-control"
                                           name="email"
                                           value="<?php echo e(old('email')); ?>">
                                </div>
                            </div>
    
    
    
                            <div class="form-group">
                                <label class="col-md-12 control-label  pull-left">Password</label>
    
                                <div class="col-md-12">
                                    <input type="password"
                                           class="form-control"
                                           name="password">
                                </div>
                            </div>
    
    
    
                            <div class="form-group">
                                <div class="col-md-6 text-left">
                                    <label>
                                        <input type="checkbox"
                                               name="remember"> Remember me
                                    </label>
                                </div>
                                <div class="col-md-6 text-left">
                                    <a href="<?php echo e(route('auth.password.reset')); ?>">Forgot your password?</a>
                                </div>
                                
                                
                            </div>
    
    
                            <!--<div class="form-group"-->
                            <!--   <div class="col-md-12 text-center">-->
                            <!--       <a href="<?php echo e(route('auth.password.reset')); ?>">Forgot your password?</a>-->
                            <!--    </div>-->
                            <!--</div>-->
    
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit"
                                            class="btn btn-primary"
                                            style="margin-right: 15px;">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
    
                    <div class="panel-footer">© NetRoots Technologies</div>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>