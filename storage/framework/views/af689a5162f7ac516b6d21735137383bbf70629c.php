<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Board Programs</h3>
    <div class="box box-primary">
        <!-- /.box-header -->
        <!-- form start -->
        <a  href="<?php echo e(route('admin.boardprogramindex')); ?>">
                    <button style = "float:right;" class="btn btn-success">View Board Programs</button>
                </a>
        <?php echo Form::open(['method' => 'POST', 'route' => ['admin.boards-board-program-store']]); ?>

        <div id="show">
            <div id="box" style = "padding-top: 50px;" class="box-body">
               
                
                <div class="form-group col-md-6 <?php if($errors->has('name')): ?> has-error <?php endif; ?>" style = "float:left;">
                <?php echo Form::label('name', 'Board Name*', ['class' => 'control-label']); ?>

                <?php echo Form::text('name[0]', old('name'), ['id' => 'name' ,'class' => 'form-control', 'placeholder' => 'Enter Board Name','maxlength'=>'50', 'required']); ?>

                <?php if($errors->has('name')): ?>
                    <span class="help-block">
                        <?php echo e($errors->first('name')); ?>

                    </span>
                <?php endif; ?>
                </div>
                <div class="form-group col-md-5 <?php if($errors->has('programs')): ?> has-error <?php endif; ?>" style = "float:left;">
                    <?php echo Form::label('programs', 'Programs*', ['class' => 'control-label']); ?>

                        <select class="form-control select2 programs" name="programs[0][]" required="required" multiple="multiple">
                            <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($programs[$key]['id']); ?>"><?php echo e($programs[$key]['name']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                </div>
                <div class="form-group col-md-1" style = "float:left;">
                    <button style="margin-top: 22px" type="button" name="add" id="add" class="btn btn-success add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
            
        </div>

        <!-- /.box-body -->
       
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style type="text/css">

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script type="text/javascript">
        
        function selectRefresh() {
            $(".programs").select2({

            });
        }
        $('.add').click(function() {
          $('.main').append($('.new-wrap').html());
          selectRefresh();
        });
        $(document).ready(function() {
          selectRefresh();
        });
    </script>
    <script type="text/javascript">
    
    document.getElementById("board_name").addEventListener("keyup", function() {
        var nameInput = document.getElementById('board_name').value;
        if (nameInput != "") {
            document.getElementById('btn').removeAttribute("disabled");
        } else {
            document.getElementById('btn').setAttribute("disabled", null);
        }
    });
 

    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove"><div class="form-group col-md-6" style = "float:left;"><input required="required" type="text" name="board_name[1]" placeholder="Enter Board Name" class="form-control" /></div><div class="form-group col-md-5" style = "float:left;"><select class="form-control select2 programs" name="programs['+i+'][]" multiple="multiple"><?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><option value="<?php echo e($program['id']); ?>"><?php echo e($program['name']); ?></option><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></select></div><br> <div class="form-group col-md-1" style = "float:left;"><button style=" float:left;" type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');
            $('.select2').select2(); 

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    function showBranch() {
        var data= $('input[name="board_name"]').val();

        if(data)
        {
             $('#show').show();
             $('#btn').hide(); 
        }
    }
    </script>
    <script src="<?php echo e(url('public/js/admin/companies/create_modify.js')); ?>" type="text/javascript"></script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>