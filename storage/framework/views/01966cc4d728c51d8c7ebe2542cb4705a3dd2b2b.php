<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Board Programs</h3>

            <a href="<?php echo e(route('admin.board-program')); ?>" style = "float:right;" class="btn btn-success pull-right">Create Board Programs</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($boards) > 0 ? 'datatable' : ''); ?>">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>View Programs</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($boards) > 0): ?>
                        <?php $__currentLoopData = $boards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $board): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr data-entry-id="<?php echo e($board->id); ?>">
                                <td><?php echo e($board->name); ?></td>
                                <td><a href="<?php echo e(route('admin.view-board-program', [$board->id])); ?>"><button
                                            class="btn btn-primary" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;">View Programs</button></a></td>
                                <td>
                                    <!-- a href="<?php echo e(route('admin.view-board-program', [$board->id])); ?>"><button
                                            class="btn btn-primary" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;">Edit</button></a -->
                                    <a data-toggle="modal" data-target="#exampleModal1<?php echo e($board->id); ?>"><button
                                            class="btn btn-primary" style = "background-color: #0ba360;
    border-color: #0ba360; color:white;">Add More Program</button></a>


                                </td>

                                <div class="modal fade" id="exampleModal1<?php echo e($board->id); ?>" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <?php echo Form::open(['route' => 'admin.existing-board-program', 'method' =>
                                                'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']); ?>

                                                <input type="hidden" name="board_id" value="<?php echo e($board->id); ?>">
                                                <div class="form-group">
                                                    <label for="scheduled_time" class="col-form-label">Board</label>
                                                    <input type="text"
                                                        value="<?php echo e(\App\Helpers\Helper::boardIdToName($board->id)); ?>"
                                                        readonly class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <?php echo Form::label('program_name', 'Program Name*', ['class' =>
                                                    'control-label']); ?>

                                                    <select name="program_id" class="form-control">
                                                        <?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($program['id']); ?>">
                                                                <?php echo e($program['name']); ?>

                                                            </option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo Form::label('description', 'Description', ['class' =>
                                                    'control-label']); ?>

                                                    <input type="text" name="description" required="required"
                                                        placeholder="Enter Description" class="form-control" />
                                                </div>
                                                <div style="margin-top: 20px">

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                            <?php echo Form::close(); ?>

                                        </div>
                                    </div>
                                </div>

                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                        <tr>
                            <td colspan="3"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>