<?php $role = app('Spatie\Permission\Models\Role'); ?>

<div class="form-group">
    <?php echo Form::label('name', 'Name*', ['class' => 'control-label']); ?>

    <?php echo Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']); ?>

    <p class="help-block"></p>
    <?php if($errors->has('name')): ?>
        <p class="help-block">
            <?php echo e($errors->first('name')); ?>

        </p>
    <?php endif; ?>
</div>
<div class="clearfix"></div>
<p class="pull-right">
    <a href="javascript:void(0);" onclick="FormControls.selectUnSelectGlobal('selected');" class="head_permission">Allow all</a>&nbsp;|&nbsp;
    <input id="allow_all" type="checkbox"  class="allow_all" value="allow_all" onclick="FormControls.checkAll(this);" checked="true"> ALLOW ALL
</p>

<div class="clearfix"></div>
<?php if(count($GroupPermissions)): ?>
    <?php ($counter = 1); ?>
    <?php $__currentLoopData = $GroupPermissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $GroupPermission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-4">
            <table class="table table-bordered">
                <tbody>
                <tr style="background-color: lightgrey;">
                    <th width="60%">
                        <?php echo e($GroupPermission['title']); ?>

                    </th>
                    <th>
                        <label  id="id-<?php echo e($GroupPermission['name']); ?>" class="btn btn-sm  p-id-allow_<?php echo e($GroupPermission['name']); ?> id-allow_<?php echo e($GroupPermission['name']); ?> label_all <?php if(isset($AllowedPermissions[$GroupPermission['id']])): ?> all active btn-info <?php else: ?> btn-default <?php endif; ?>" data-permission="<?php echo e($GroupPermission['name']); ?>">
                            <input id="allow_<?php echo e($GroupPermission['name']); ?>" type="checkbox" name="permission[]" class="allow_all allow <?php echo e($GroupPermission['name']); ?> allow_<?php echo e($GroupPermission['name']); ?>" value="<?php echo e($GroupPermission['name']); ?>" <?php if(isset($AllowedPermissions[$GroupPermission['id']])): ?> checked="true" <?php endif; ?> onclick="FormControls.checkMyModule(this,'allow_<?php echo e($GroupPermission['name']); ?>');"> Allow
                        </label>
                    </th>
                </tr>
                <?php if(count($SubPermissions)): ?>
                    <?php $__currentLoopData = $SubPermissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $SubPermission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($SubPermission['parent_id'] == $GroupPermission['id'] || $SubPermission['id'] == $GroupPermission['id']): ?>
                            <tr>
                                <td align="right"><?php echo e($SubPermission['title']); ?></td>
                                <td>
                                    <label id="id-<?php echo e($SubPermission['name']); ?>" class="btn btn-sm  id-allow_<?php echo e($GroupPermission['name']); ?> label_all <?php if(isset($AllowedPermissions[$SubPermission['id']])): ?> active btn-info all <?php else: ?> btn-default <?php endif; ?>" data-permission="<?php echo e($GroupPermission['name']); ?>" data-sub_permission="<?php echo e($SubPermission['name']); ?>">
                                        
                                        <input id="sub-allow_<?php echo e($SubPermission['name']); ?>" type="checkbox" name="permission[]" class="allow_all allow <?php echo e($GroupPermission['name']); ?>  sub-allow_<?php echo e($GroupPermission['name']); ?>" value="<?php echo e($SubPermission['name']); ?>" <?php if(isset($AllowedPermissions[$SubPermission['id']])): ?> checked="true" <?php endif; ?> onclick="FormControls.checkMyParent(this,'allow_<?php echo e($GroupPermission['name']); ?>' , 'sub-allow_<?php echo e($GroupPermission['name']); ?>', '<?php echo e($SubPermission['name']); ?>' );"> Allow
                                    </label>

                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <?php ($counter++); ?>
        <?php if($counter == 4): ?>
            <?php ($counter = 1); ?>
            <div class="clear clearfix"></div>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>

<div class="clear clearfix"></div>