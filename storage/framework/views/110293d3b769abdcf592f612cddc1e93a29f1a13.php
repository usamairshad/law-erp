<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Create Lab</h3>
    <div class="box box-primary">
         <a  href="<?php echo e(route('admin.labs.index')); ?>" style = "float:right;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'route' => ['admin.labs.store'], 'id' => 'validation-form']); ?>

        <div id="show">
            <div id="box" class="box-body">
                <h2>Create Lab</h2>
                <div class="form-group col-md-5 <?php if($errors->has('name')): ?> has-error <?php endif; ?>" style = "float:left;">
                    <?php echo Form::label('name', 'Lab Name*', ['class' => 'control-label']); ?>

                   <input type="text" name="name[0]"  required="required" placeholder="Enter Lab Name" class="form-control" />
                </div>
                <div class="form-group col-md-6 <?php if($errors->has('description')): ?> has-error <?php endif; ?>" style = "float:left;">
                    <?php echo Form::label('description', 'Description', ['class' => 'control-label']); ?>

                     <input type="text" name="description[0]" required="required" placeholder="Enter Description" class="form-control" />
                </div>
                <div class="form-group col-md-1" style = "float:left;">
                    <button style="margin-top: 22px" type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div> 
            <div id="secondbox">
                
            </div>  
            <button  class="btn btn-success col-md-12" > Save</button>
        </div>

        <!-- /.box-body -->
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style type="text/css">

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script type="text/javascript">
    
    var i = 0;
    $("#add").click(function(){
        ++i;
        $("#secondbox").append('<div id="remove"><div class="form-group col-md-5" style = "float:left;"><input required="required" type="text" name="name[]" placeholder="Enter Lab Name" class="form-control" /></div><div class="form-group col-md-6" style = "float:left;"><input type="text" required="required" name="description[]" placeholder="Enter Description" class="form-control" /></div><br> <div class="form-group col-md-1" style = "float:left;"><button type="button" class="btn btn-danger remove-tr"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>');

    });        

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('#remove').remove();

    });
    </script>
    <script src="<?php echo e(url('public/js/admin/companies/create_modify.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>