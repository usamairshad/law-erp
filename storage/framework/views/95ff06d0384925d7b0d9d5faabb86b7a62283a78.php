<?php $request = app('Illuminate\Http\Request'); ?>
<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Class Timetable</h1>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create Class Timetable</h3>
            <a href="<?php echo e(route('admin.class_timetable.index')); ?>" class="btn btn-success pull-right">Back</a>
        </div>

       <?php echo Form::open(['method' => 'POST', 'route' => ['admin.class_timetable.store'], 'id' => 'validation-form' , 'enctype' => 'multipart/form-data']); ?> 
       <?php echo Form::token();; ?>

        <div class="box-body">
            <?php echo $__env->make('hrm.class_timetables.field', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="box-footer">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo e(url('public/js/admin/employees/create_modify.js')); ?>" type="text/javascript"></script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>