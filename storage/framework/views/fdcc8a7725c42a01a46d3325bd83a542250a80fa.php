<?php $request = app('Illuminate\Http\Request'); ?>


<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>ERP Roles</h1>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <?php if(Gate::check('erp_roles_create')): ?>
                <a href="<?php echo e(route('admin.roles.create')); ?>" class="btn btn-success pull-right">Add New Role</a>
            <?php endif; ?>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($Roles) > 0 ? 'datatable' : ''); ?>">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Create At</th>
                    <th width="15%">Actions</th>
                </tr>
                </thead>

                <tbody>
                <?php if(count($Roles) > 0): ?>
                    <?php $__currentLoopData = $Roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($Role->id); ?>">
                            <td><?php echo e($Role->id); ?></td>
                            <td><?php echo e($Role->name); ?></td>
                            <td><?php echo e($Role->created_at); ?></td>
                            <td>
                                <?php if($Role->status && Gate::check('erp_roles_inactive')): ?>
                                    <?php echo Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to inactivate this record?');",
                                            'route' => ['admin.roles.inactive', $Role->id])); ?>

                                    <?php echo Form::submit('Inactivate', array('class' => 'btn btn-xs btn-warning')); ?>

                                    <?php echo Form::close(); ?>

                                <?php elseif(!$Role->status && Gate::check('erp_roles_active')): ?>
                                    <?php echo Form::open(array(
                                            'style' => 'display: inline-block;',
                                            'method' => 'PATCH',
                                            'onsubmit' => "return confirm('Are you sure to activate this record?');",
                                            'route' => ['admin.roles.active', $Role->id])); ?>

                                    <?php echo Form::submit('Activate', array('class' => 'btn btn-xs btn-primary')); ?>

                                    <?php echo Form::close(); ?>

                                <?php endif; ?>

                                <?php if(Gate::check('erp_roles_edit')): ?>
                                    <a href="<?php echo e(route('admin.roles.edit',[$Role->id])); ?>" class="btn btn-xs btn-info"><?php echo app('translator')->getFromJson('global.app_edit'); ?></a>
                                <?php endif; ?>

                                <?php if(Gate::check('erp_roles_destroy')): ?>
                                    <?php echo Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                        'route' => ['admin.roles.destroy', $Role->id])); ?>

                                    <?php echo Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')); ?>

                                    <?php echo Form::close(); ?>

                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="6"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>