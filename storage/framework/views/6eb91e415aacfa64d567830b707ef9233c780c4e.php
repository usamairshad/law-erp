<div class="form-group col-md-4 <?php if($errors->has('name')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('name', 'Name*', ['class' => 'control-label']); ?>

    <input type="text" name="name" class="form-control" required maxlength="30">
</div>
<div class="form-group col-md-6 <?php if($errors->has('description')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('description', 'Description*', ['class' => 'control-label']); ?>

     <input type="texarea" name="description" class="form-control" required maxlength="100">
</div>