<div class="form-group col-md-4 <?php if($errors->has('session_id')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo e(Form::label('Session*')); ?>

    <?php echo e(Form::select('session_id', $active_session, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required'))); ?>

</div>
<div class="form-group col-md-4 <?php if($errors->has('name')): ?> has-error <?php endif; ?>" style = "float:left;" >
    <?php echo Form::label('name', 'Name*', ['class' => 'control-label']); ?>

    <?php echo Form::text('name', old('name'), ['class' => 'form-control','minlength'=>'3','maxlength'=>'20']); ?>

    <?php if($errors->has('name')): ?>
        <span class="help-block">
            <?php echo e($errors->first('name')); ?>

        </span>
    <?php endif; ?>
</div>
<div class="form-group col-md-4 <?php if($errors->has('start_month')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('start_month', 'Start Month*', ['class' => 'control-label']); ?>

    <select name="start_month" class="form-control">
      <option>Select Month</option>
      <option value="January">January</option>
      <option value="February">February</option>
      <option value="March">March</option>
      <option value="April">April</option>
      <option value="May">May</option>
      <option value="June">June</option>
      <option value="July">July</option>
      <option value="August">August</option>
      <option value="September">September</option>
      <option value="October">October</option>
      <option value="November">November</option>
      <option value="December">December</option>
    </select>
</div>
<div class="form-group col-md-4 <?php if($errors->has('start_year')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('start_year', 'Start Year*', ['class' => 'control-label']); ?>

    <input type="date" name="start_year" min="1900" max="2099" class="form-control"/>
</div>
<div class="form-group col-md-4 <?php if($errors->has('end_month')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('end_month', 'End Month*', ['class' => 'control-label']); ?>

    <select name="end_month" class="form-control">
      <option>Select Month</option>
      <option value="January">January</option>
      <option value="February">February</option>
      <option value="March">March</option>
      <option value="April">April</option>
      <option value="May">May</option>
      <option value="June">June</option>
      <option value="July">July</option>
      <option value="August">August</option>
      <option value="September">September</option>
      <option value="October">October</option>
      <option value="November">November</option>
      <option value="December">December</option>
    </select>
</div>
<div class="form-group col-md-4 <?php if($errors->has('end_year')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('end_year', 'End Year*', ['class' => 'control-label']); ?>

    <input type="date" name="end_year" min="1900" max="2099" class="form-control"/>
</div>
