<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Academics Roles</h1>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
          <table class="table table-bordered table-striped <?php echo e(count($data) > 0 ? 'datatable' : ''); ?>">
            <tr>
                 <th>id</th>
                 <th>name</th>
                 <th>display_name</th>
                 <th>description</th>
                 <th>action</th>
            </tr>

            <?php $__currentLoopData = $data['roles']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                  <td><?php echo e($role->id); ?></td>
                  <td><?php echo e($role->name); ?></td>
                  <td><?php echo e($role->display_name); ?></td>
                  <td><?php echo e($role->description); ?></td>
                  <td>
                    <a href="<?php echo e(route('role.edit',[$role->id])); ?>" class="btn btn-xs btn-info"><?php echo app('translator')->getFromJson('global.app_edit'); ?></a>
                    <a href="<?php echo e(route('role.delete', ['id' => $role->id])); ?>" class="tooltip-error bootbox-confirm" data-rel="tooltip" title="Delete">
                                                    <span class="red ">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </span>
                                                    </a>
                  </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>


    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>