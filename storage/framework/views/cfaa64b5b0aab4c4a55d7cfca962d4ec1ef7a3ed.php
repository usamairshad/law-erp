<?php $request = app('Illuminate\Http\Request'); ?>
<?php $helper = app('App\Helpers\Helper'); ?>


<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Leave Request</h1>
    </section>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
                <a href="<?php echo e(route('admin.leave_management.create')); ?>" class="btn btn-success pull-right">Genrate Leave Request</a>
          
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($leaves) > 0 ? 'datatable' : ''); ?>">
                <thead>
                    <tr>
                        
                        <th>Staff Reg No</th>
                        <th>Staff Name</th>
                        <th>Course</th>
                        <th>Date</th>   
                        <th>Status</th>
                        
                    </tr>
                </thead>



                <tbody>
                    <?php if(count($leaves) > 0): ?>
                        <?php $__currentLoopData = $leaves; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $leave): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e(\App\Helpers\Helper::getStaffIDToRegNo($leave->staff_id)); ?></td>
                                <td><?php echo e(\App\Helpers\Helper::getStaffIDToFirstName($leave->staff_id)); ?></td>
                                <td><?php echo e(\App\Helpers\Helper::subjectIdToName($leave->course_id)); ?></td>
                                <td><?php echo e($leave->leave_date); ?></td>
                                <td>
                                    <button type="button" class="btn btn-info">
                                    <?php echo e($leave->status); ?> </button></td>
                             

                                
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>













<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable(
{

        columns: [
            {

               render: {

                  sort: 'status'
               }
            }
        ]
    }


                )
        });

        $(".warning").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#warning_user_input').val($user_id);

        });


        $(".terminate").on("click", function() {

            $user_id = $(this).data("user-id");
            $('#terminate_user_input').val($user_id);

        });


        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });


        $(".transferBranchButton").on("click", function() {

            $user_id = $(this).data('user-id');
            $branch_name = $(this).data('branch-name');
            $name = $(this).data('name');
            $status = $(this).data('status');



            console.log($user_id, $branch_name, $name, $status);



            $('#transfer_staff_id').val();
            $('#branch_name_transfer').text('');
            $('#staff_name_transfer').text('');
            $('#status_name_transfer').text('');



            $('#transfer_staff_id').val($user_id);
            $('#branch_name_transfer').text($branch_name);
            $('#staff_name_transfer').text($name);
            $('#status_name_transfer').text($status);

            $('#action_name_transfer').attr("href", `<?php echo e(url('admin/delete/staff/branch')); ?>/${$user_id}`);



        });

        $('.promotionDemotion').on("click", function() {

            $name = $(this).data('user-name');
            $user_role_id = $(this).data('user-role-id');
            $user_id = $(this).data('user-id');
            $branch_id = $(this).data('branch-id');

            $('#user_name_promotion').val($name);

            $('#promotion_user_id').val($user_id);
            $('#staff_user_id').val($user_id);
            $("#active_teacher_id").val($user_id);
            console.log($user_id);
            $(".roles").select2().val($user_role_id).trigger('change');


            loadBorad($branch_id);

        });

        function loadBorad($branch_id) {
            $.ajax({
                url: `<?php echo e(url('admin/getBoard/')); ?>/${$branch_id}`,

                success: function(response) {
                    $html = '<option value="null">Select board</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.board_id}>${res.board.name}</option>`;

                    });

                    $("#boards").data("branch-id", $branch_id);
                    $("#boards").html($html);
                }
            });
        }
        ////////////////on board change get program/////////////// 
        $("#boards").on("change", function() {

            $board_id = $(this).find(":selected").val();
            $branch_id = $(this).data('branch-id');

            loadProgram($branch_id, $board_id);
        });

        function loadProgram($branch, $board) {
            $.ajax({
                url: `<?php echo e(url('admin/getProgram/')); ?>/${$branch}/${$board}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select program</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.program_id}>${res.program.name}</option>`;

                    });

                    $("#programs").data('branch-id', $branch);
                    $("#programs").data('board-id', $board);
                    $("#programs").html($html);
                }
            });
        }

        ////////////////on program change get class/////////////// 
        $("#programs").on("change", function() {

            $board_id = $(this).data("board-id");
            $branch_id = $(this).data('branch-id');
            $program_id = $(this).find(':selected').val();

            console.log($board_id, $branch_id, $program_id);

            loadClasses($branch_id, $board_id, $program_id);
        });

        function loadClasses($branch, $board, $program) {
            $.ajax({
                url: `<?php echo e(url('admin/getClasses/')); ?>/${$branch}/${$board}/${$program}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select class</option>';

                    response.forEach((res) => {

                        $html +=
                            `<option data-active-s-id=${res.id}  value=${res.get_class.id}>${res.get_class.name}</option>`;

                    });

                    $("#classes").html($html);
                }
            });
        }
        $("#classes").change(function() {
            $activeSessionID= $(this).find(":selected").data("active-s-id");
            $("#active_session_id").val($activeSessionID);
            console.log($activeSessionID);
        });


        $("#classes").change(function() {

            $activeSessionID= $(this).find(":selected").data("active-s-id");

            $("#active_session_id").val($activeSessionID);
            console.log($activeSessionID);
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>