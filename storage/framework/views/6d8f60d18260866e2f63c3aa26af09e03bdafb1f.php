<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Teachers</h3>

            

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($users) > 0 ? 'datatable' : ''); ?>">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Teacher Name</th>
                        <th>Branch</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($users) > 0): ?>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e(++$key); ?></td>
                                <td><?php echo e($user->name); ?></td>
                                <td><?php echo e($user->BranchDirectByUser->name); ?></td>
                                <td>

                                    <a href="" data-user-obj="<?php echo e($user); ?>" data-toggle="modal" class="assignCourses" id="assignCourses"
                                        data-target="#exampleModal">Assign Course</a>

                                </td>

                            </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="7"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<!-------------------------------------------------modal------------------------------------>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document" Style = "max-width:800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Assign Courses To Teacher</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <div class="row">

                        <form action="<?php echo e(route('admin.all-teachers.store')); ?>" method="POST">

                            <?php echo e(csrf_field()); ?>


                            <div class="col-md-4">

                                <label for="">Board</label>
                                <br>
                                <select style="width: 100%" class="form-control" name="" id="boards">

                                    <option value=""></option>

                                </select>


                            </div>


                            <div class="col-md-4">

                                <label for="">Program</label>
                                <br>
                                <select style="width: 100%" class="form-control" name="" id="programs">

                                    <option value=""></option>

                                </select>


                            </div>

                            <div class="col-md-4">

                                <label for="">Class</label>
                                <br>
                                <select style="width: 100%" class="form-control" name="" id="classes">

                                    <option value=""></option>

                                </select>

                            </div>

                            <div class="col-md-4">

                                <label for="">Section</label>
                                <br>
                                <select style="width: 100%" class="form-control"
                                    name="active_session_section_id" id="section">

                                    <option value=""></option>

                                </select>

                            </div>


                            <div class="col-md-4">

                                <label for="">Select Courses</label>
                                <br>
                                <select style="width: 100%"  class="form-control courses"
                                    name="courses" id="courses">

                                    <option value="null"></option>

                                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <option value="<?php echo e($course->id); ?>"><?php echo e($course->name); ?></option>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>

                                <input type="hidden" id="staff_id" name="staff_id">
                            </div>
                               <div class="form-group col-md-4 <?php if($errors->has('class_timetable')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('class_timetable', 'Class Timetable', ['class' => 'control-label']); ?>

                                    <select class="form-control class_timetable" style="width: 100%; margin-top: 20px" name="class_timetable_id[]" multiple="multiple" id="class_timetable" required="required"></select>
                                </div>
                            <div class="col-md-4">

                                <label for="">Rates</label>
                                <br>
                                <input type="text" name="rates" class="form-control">
                            </div>

                            <div class="col-md-4">

                                <label>No of Lectures in Month</label>
                                <br>
                                <input type="number" name="days_in_month" min="0" max="30" class="form-control">
                            </div>

                               

                            <div class="col-sm-12 col-md-12">

                                <div class="panel-body pad table-responsive">
                                    <table class="table table-bordered table-striped datatableChild">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Board</th>
                                                <th>Program</th>
                                                <th>Class</th>
                                                <th>Section</th>
                                                <th>Courses</th>
                                
                                                <th>Rates</th>
                                                <th>No of Days</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody id="childTableBody">

                                        </tbody>
                                    </table>
                                </div>


                            </div>
                    </div>


                </div>

            </div>
            <input type="hidden" name="active_session_section_id_usman" id="active_session_section_id_usman">
            <input type="hidden" name="branch_id" id="branch_id">

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>



<?php $__env->startSection('javascript'); ?>
    <script>
        
        var active_session_section_id;
        $(document).ready(function() {
            $('.datatable').DataTable();
            $('.datatableChild').DataTable();
        });

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();

        });

        //var datatableChild = $('.datatableChild').DataTable();

        $(".assignCourses").on("click", function() {
          
            //$('.datatableChild tbody').empty();
            console.log("in");
            $("#childTableBody").html("");
            $branch_id = $(this).data("user-obj").branch_id;

            $("#branch_id").val($branch_id);

            $staff_id = $(this).data("user-obj").staff.id;
           
            $("#staff_id").val($staff_id);

            loadBorad($branch_id);



            $.ajax({
                url: `<?php echo e(url('admin/get/course-by-staff/')); ?>/${$staff_id}`,
                success: function(res) {
                    console.log(res);
                    $html = ``;

                    res.forEach((mainData) => {

                        courses = ``;

                        mainData.courses.forEach((data) => {

                            courses += data.name + " ";

                        });

                        $html += `                        
                    <tr>
                        <td>id</td>
                        <td>${mainData.activesessionsection.activesession.board.name}</td>
                        <td>${mainData.activesessionsection.activesession.program.name}</td>
                        <td>${mainData.activesessionsection.activesession.get_class.name}</td>
                        <td>${mainData.activesessionsection.section.name}</td>
                        <td>${courses}</td>
                        <td>${mainData.rates}</td>
                        <td>${mainData.days_in_month}</td>


                        <td>
                            
                          <a class="btn btn-sm btn-danger" href="delete-session-teacher/${mainData.activesessionsection.id}/${mainData.staff_id}">Delete</a>
                            
                        </td>

                
                    </tr>
                `;                        
                        $("#childTableBody").html($html);

                    });

                }

            });



        });
         function selectRefresh() {
            $(".days").select2({

            });
        }
        $(document).ready(function() {
          selectRefresh();
        });
        function loadBorad($branch_id) {
            $.ajax({
                url: `<?php echo e(url('admin/getBoard/')); ?>/${$branch_id}`,

                success: function(response) {
                    $html = '<option value="null">Select board</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.board_id}>${res.board.name}</option>`;

                    });

                    $("#boards").data("branch-id", $branch_id);
                    $("#boards").html($html);
                }
            });
        }



        ////////////////on board change get program/////////////// 
        $("#boards").on("change", function() {

            $board_id = $(this).find(":selected").val();
            //$("#hidden_board_id").val($board_id);
            $branch_id = $(this).data('branch-id');

            loadProgram($branch_id, $board_id);
        });

        function loadProgram($branch, $board) {
            $.ajax({
                url: `<?php echo e(url('admin/getProgram/')); ?>/${$branch}/${$board}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select program</option>';

                    response.forEach((res) => {

                        $html += `<option value=${res.program_id}>${res.program.name}</option>`;

                    });

                    $("#programs").data('branch-id', $branch);
                    $("#programs").data('board-id', $board);
                    $("#programs").html($html);
                }
            });
        }

        ////////////////on program change get class/////////////// 
        $("#programs").on("change", function() {

            $board_id = $(this).data("board-id");
            $branch_id = $(this).data('branch-id');
            $program_id = $(this).find(':selected').val();

            $("#hidden_program_id").val($program_id);

            console.log($board_id, $branch_id, $program_id);

            loadClasses($branch_id, $board_id, $program_id);
        });

        function loadClasses($branch, $board, $program) {
            $.ajax({
                url: `<?php echo e(url('admin/getClasses/')); ?>/${$branch}/${$board}/${$program}`,

                success: function(response) {
                    console.log(response);
                    $html = '<option value="null">select class</option>';

                    response.forEach((res) => {

                        $html +=
                            `<option data-active-s-id=${res.id}  value=${res.get_class.id}>${res.get_class.name}</option>`;

                    });

                    $("#classes").data("branch_id", $branch);
                    $("#classes").data("board_id", $board);
                    $("#classes").data("program_id", $program);


                    $("#classes").html($html);
                }
            });
        }
        ////////////----------------------load section-----------------------------///
        $("#classes").on("change", function() {

            $class_id = $(this).find(":selected").val();

            $branch_id = $(this).data('branch_id');
            $board_id = $(this).data("board_id");
            $program_id = $(this).data('program_id');




            $.ajax({
                url: `<?php echo e(url('admin/get/active-session-section/')); ?>/${$branch_id}/${$board_id}/${$program_id}/${$class_id}`,

                success: function(response) {
                    console.log(response)
                    // var usman_ki_id = response[0].id
                    // console.log(usman_ki_id)
                    // $("#active_session_section_id_usman").val(usman_ki_id);
                   
                    $html = '<option value="null">Select Section</option>';

                    response.forEach((res) => {
                        //console.log(res);
                        $html +=
                            `<option  data-active-session="" value=${res.section_id}>${res.section.name}</option>`;
                    });


                    $("#section").html($html);
                }
            });
       });

        $("#section").on("change", function() {

            $activeSessionSectionID = $(this).find(":selected").val();
            //$active_session_section_id=$(this).find(":selected").data("obj");           
            // var course= $('.courses').val();

       });
       
        $("#courses").on("change", function() {
            $course_id = $(this).find(":selected").val();
            $branch_id= $("#branch_id").val();
            $board_id= $("#boards").val();
            $program_id= $("#programs").val();
            $class_id=$("#classes").val();
            $section_id = $("#section").val();

            // console.log($bra)
            $.ajax({
                url: `<?php echo e(url('admin/get/active-session-section-courses/')); ?>/${$course_id}/${$branch_id}/${$board_id}/${$program_id}/${$class_id}/${$section_id}`,

                success: function(response) {                    
                    $.each( response, function(k, v) {
                            $('#class_timetable').append($('<option value=' +  v.id + '>' + v.title + '</option>'));
                        });
                    $("#active_session_section_id_usman").val(response[0].newActiveSessionSectionId);
                    // $('.class_timetable').select2();
                }
            });          
            
        });
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>