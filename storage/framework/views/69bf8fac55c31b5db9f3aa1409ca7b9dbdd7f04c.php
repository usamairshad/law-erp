<?php $request = app('Illuminate\Http\Request'); ?>

<style>
	/* Tabs*/
section {
    padding: 60px 0;
}

section .section-title {
    text-align: center;
    color: black;
    margin-bottom: 50px;
    text-transform: uppercase;
}
#tabs{
    color: black;
}
#tabs h6.section-title{
    color: black;
}

#tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: black;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 4px solid !important;
    font-size: 20px;
    font-weight: bold;
}
#tabs .nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: black;
    font-size: 20px;
}
section {
    padding: 0px 0;
}
	</style>
<?php $__env->startSection('content'); ?>

<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/morris.js/morris.css" rel="stylesheet">
<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>
<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/multislider/multislider.css" rel="stylesheet">

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class = "row">
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body" >
								<div class="row">
									<div class="col">
										<div class="">Companies</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($companies ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Companies </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Branches</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($branch_count ?? 0); ?> </b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Branches </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>


					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Programs</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($programs ?? 0); ?> </b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Programs </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Staff</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($staff ?? 0); ?> </b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Staff </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-lg-6 col-xl-3" onclick="myFunction()">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Students</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($students ?? 0); ?> </b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Students </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Courses</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($courses ?? 0); ?> </b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Courses </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Revenue</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($income ?? 0); ?></b><span class="text-success tx-13 ml-2">(pkr)</span></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="ti-bar-chart-alt project bg-success-transparent text-success "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-success wd-25 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted">Amount in pkr</span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Recovered</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($received ?? 0); ?></b><span class="text-success tx-13 ml-2">(pkr)</span></div>
									</div>
									<div class="col-auto align-self-center ">
									<div class="card-chart bg-teal-transparent brround ml-auto mt-0">
										<i class="typcn typcn-chart-bar-outline text-teal tx-20"></i>
									</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-success wd-25 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted">Amount in pkr</span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12" id  = "total_student" style = "display:block">
						<div class="card mg-b-20">
							<div class="card-body">
							<div id="echart12" class="ht-300"></div>
							</div>
							</div>
							</div>
          <div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Receivables</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($receivables ?? 0); ?></b><span class="text-success tx-13 ml-2">(pkr)</span></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-eye project bg-primary-transparent text-primary "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-primary wd-80 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted">Amount in pkr</span></small>
								</div>
							</div>
						</div>
					</div>
				
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Expense</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($expense ?? 0); ?></b><span class="text-success tx-13 ml-2">(pkr)</span></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="ti-pulse project bg-warning-transparent text-warning "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-danger wd-30 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted">Amount in pkr</span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Total Profit</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($total_expense ?? 0); ?></b><span class="text-success tx-13 ml-2">(pkr)</span></div>
									</div>
									<div class="col-auto align-self-center ">
									<div class="feature widget-2 text-center mt-0 mb-3">
									<i class="ti-stats-up project bg-success-transparent mx-auto text-success "></i>
								</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overview</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-danger wd-30 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted">Amount in pkr</span></small>
								</div>
							</div>
						</div>
					</div>
	

          <div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">SOS Staff</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($sos_teacher ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall SOS Staff </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3" >
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">SOS Students</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($sos_student ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall SOS Students </p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall<span class="float-right text-muted"></span></small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Fee Defaulters</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($defaulter ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Fee Defaulters</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Agenda</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($agenda ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Agenda's</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Warnings</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($warning ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Warning</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Training</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($training ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Training</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Terminated Staff</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($terminated ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Terminated Staff</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Jobs Post</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($jobs ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall jobs</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Candidates</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($candidates ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Candidates</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Resigned Employee's</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($resigned ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Resigned Employee</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-6 col-xl-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col">
										<div class="">Leaves Requests</div>
										<div class="h3 mt-2 mb-2"><b><?php echo e($leaves ?? 0); ?></b></div>
									</div>
									<div class="col-auto align-self-center ">
										<div class="feature mt-0 mb-0">
											<i class="fe fe-users project bg-pink-transparent text-pink "></i>
										</div>
									</div>
								</div>
								<div class="">
									<p class="mb-1">Overall Leaves Requests</p>
									<div class="progress progress-sm h-1 mb-1">
										<div class="progress-bar bg-secondary wd-50 " role="progressbar"></div>
									</div>
									<small class="mb-0 text-muted">Overall</small>
								</div>
							</div>
						</div>
					</div>
					
				
			

			
</div>
<div class="row row-sm">
  <div class="container-fluid">
		  <div class="row row-sm">
					<div class="col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">

<!-- Tabs -->
<section id="tabs" style ="padding:0 !important;">
	<div class="container">
	<h6 class="section-title h1" style ="font-size: 25px;margin-bottom:10px;">Income V/S Expense</h6>
		<div class="row">
		
			<div class="col-md-12 ">
				<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Roots IVY International School</a>
						<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">IVY College of manage sciences</a>
						<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">IVY United School</a>
				</div>
				</nav>
				
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
								<div id="echart20" class="ht-300"></div>
								
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								Total Staff
								<div id="echart11" class="ht-300"></div>
								
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								Total Students
								<div id="echart12" class="ht-300"></div>
								
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								Total SOS Students
								<div id="echart13" class="ht-300"></div>
								
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								Total SOS Staff
								<div id="echart14" class="ht-300"></div>
								
							</div>
						</div>
					</div>

				</div>
					<div class="tab-pane fade" id="nav-profile" style= "display:block"  role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
							<div id="echart1" class="ht-300"></div>
</div>
						</div>
					</div>
				</div>
					<div class="tab-pane fade" id="nav-contact"  role="tabpanel" aria-labelledby="nav-contact-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
							<div id="echart10" class="ht-300"></div>
</div>
						</div>
					</div></div>
					
				</div>
			
			</div>
		</div>
	</div>
</section>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="row row-sm">
  <div class="container-fluid">
		  <div class="row row-sm">
					<div class="col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">

<section id="tabs" style ="padding:0 !important;">
	<div class="container">
	<h6 class="section-title h1" style ="font-size: 25px; margin-bottom:10px;">Payables</h6>
		<div class="row">
		
			<div class="col-md-12 ">
				<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">Roots IVY International School</a>
						<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-2" role="tab" aria-controls="nav-2" aria-selected="false">IVY College of manage sciences</a>
						<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-3" role="tab" aria-controls="nav-3" aria-selected="false">IVY United School</a>
				</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-1" role="tabpanel" aria-labelledby="nav-home-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
								<div id="echart11" class="ht-300"></div>
								
							</div>
						</div>
					</div>

				</div>
					<div class="tab-pane fade" id="nav-2" style= "display:block"  role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
							<div id="echart1" class="ht-300"></div>
</div>
						</div>
					</div>
				</div>
					<div class="tab-pane fade" id="nav-3"  role="tabpanel" aria-labelledby="nav-contact-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
							<div id="echart10" class="ht-300"></div>
</div>
						</div>
					</div></div>
					
				</div>
			
			</div>
		</div>
	</div>
</section>

<section id="tabs" style ="padding:0 !important;">
	<div class="container">
	<h6 class="section-title h1" style ="font-size: 25px; margin-bottom:10px;">Receivables</h6>
		<div class="row">
		
			<div class="col-md-12 ">
				<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-4" role="tab" aria-controls="nav-4" aria-selected="true">Roots IVY International School</a>
						<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-5" role="tab" aria-controls="nav-5" aria-selected="false">IVY College of manage sciences</a>
						<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-6" role="tab" aria-controls="nav-6" aria-selected="false">IVY United School</a>
				</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-4" role="tabpanel" aria-labelledby="nav-home-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
								<div id="echart11" class="ht-300"></div>
								
							</div>
						</div>
					</div>

				</div>
					<div class="tab-pane fade" id="nav-5" style= "display:block"  role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
							<div id="echart1" class="ht-300"></div>
</div>
						</div>
					</div>
				</div>
					<div class="tab-pane fade" id="nav-6"  role="tabpanel" aria-labelledby="nav-contact-tab">
					<div class="col-lg-12 col-md-12">
						<div class="card mg-b-20">
							<div class="card-body">
								
							<div id="echart10" class="ht-300"></div>
</div>
						</div>
					</div></div>
					
				</div>
			
			</div>
		</div>
	</div>
</section>


			
				<!-- /row -->

				<!-- row -->
				<div class="row row-sm">
					<div class="col-md-6">
						<div class="card mg-b-20">
							<div class="card-body">
							
								<div class="main-content-label mg-b-5">
									Line Chart
								</div>
							
								<div class="ht-200 ht-sm-300" id="flotLine1"></div>
							</div>
						</div>
					</div><!-- col-6 -->
					<div class="col-md-6">
						<div class="card mg-b-20">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
									Line Chart
								</div>
					
								<div class="ht-200 ht-sm-300" id="flotLine2"></div>
							</div>
						</div>
					</div><!-- col-6 -->
				</div>
				<!-- /row -->

				<!-- row -->
				<div class="row row-sm">
					<div class="col-md-6">
						<div class="card mg-b-20">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
									Area Chart
								</div>
							
								<div class="ht-200 ht-sm-300" id="flotArea1"></div>
							</div>
						</div>
					</div><!-- col-6 -->
					<div class="col-md-6 ">
						<div class="card mg-b-20">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
									Area Chart
								</div>
						
								<div class="ht-200 ht-sm-300" id="flotArea2"></div>
							</div>
						</div>
					</div><!-- col-6 -->
				</div>
				<!-- /row -->

				<!-- row -->
				<div class="row row-sm">
					<div class="col-md-6">
						<div class="card mg-b-20 mg-md-b-0">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
									Pie Chart
								</div>
							
								<div class="ht-200 ht-sm-300" id="flotPie1"></div>
							</div>
						</div>
					</div><!-- col-6 -->
					<div class="col-md-6">
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
									Pie Chart
								</div>
							
								<div class="ht-200 ht-sm-300" id="flotPie2"></div>
							</div>
						</div>
					</div><!-- col-6 -->
				</div>
				<!-- row closed -->
			</div>
			<!-- Container closed -->
		</div>
   </div>

<?php $__env->stopSection(); ?>
<!-- Bar chart for Defaulter -->

<script>

</script>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>