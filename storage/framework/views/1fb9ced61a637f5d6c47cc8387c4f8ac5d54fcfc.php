<?php $request = app('Illuminate\Http\Request'); ?>
<?php $helper = app('App\Helpers\Helper'); ?>


<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Timetable</h1>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i>
            <h3 class="box-title">List</h3>
                <?php if(Gate::check('erp_time_table_category_create')): ?>
                <a href="<?php echo e(route('admin.timetable.create')); ?>" class="btn btn-success pull-right">Add New TimeTable</a>
                <?php endif; ?>

        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($timetables) > 0 ? 'datatable' : ''); ?>">
                <thead>
                    <tr>
                        <th>Branch Name</th>
                        <th>Title</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                    
                    </tr>
                </thead>

                <tbody>
                    <?php if(count($timetables) > 0): ?>
                        <?php $__currentLoopData = $timetables; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $timetable): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr data-entry-id="<?php echo e($timetable->id); ?>">
                                <td><?php echo e(\App\Helpers\Helper::branchIdToName($timetable->branch_id)); ?></td>
                                <td><?php echo e($timetable->title); ?></td>
                                <td><?php echo e($timetable->start_time); ?></td>
                                <td><?php echo e($timetable->end_time); ?></td>
                                
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script>
        $(document).ready(function() {
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>