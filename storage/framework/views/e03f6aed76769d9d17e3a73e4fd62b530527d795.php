
<div class="form-group col-md-3 <?php if($errors->has('session_id')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo e(Form::label('Session Name*')); ?>

    <?php echo e(Form::select('session_id', $sessionList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required'))); ?>

</div>
<div class="form-group col-md-3 <?php if($errors->has('board_id')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo e(Form::label('Board Name*')); ?>

    <?php echo e(Form::select('board_id', $boardList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required'))); ?>

</div>

<div class="form-group col-md-3 <?php if($errors->has('program_id')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo e(Form::label('Program Name*')); ?>

    <?php echo e(Form::select('program_id', $programList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required'))); ?>

</div>
<div class="form-group col-md-3 <?php if($errors->has('class_id')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo e(Form::label('Class Name*')); ?>

    <?php echo e(Form::select('class_id', $classList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required'))); ?>

</div>
<div class="form-group col-md-3 <?php if($errors->has('branches')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('branches', 'Branches*', ['class' => 'control-label']); ?>

        <select class="form-control select2 branches" name="branches[]" required="required" multiple="multiple">
            <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($branches[$key]['id']); ?>"><?php echo e($branches[$key]['name']); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
</div>
<div class="form-group  col-md-3 <?php if($errors->has('status')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('status', 'Status*', ['class' => 'control-label']); ?>

    <?php echo Form::select('status',[ ''=>'Select  Status','Active' => 'Active','InActive' => 'InActive','Pending' => 'Pending'],old('status') , ['class' => 'form-control', 'required']); ?>

    <?php if($errors->has('status')): ?>
        <span class="help-block">
            <?php echo e($errors->first('status')); ?>

        </span>
    <?php endif; ?>
</div>

