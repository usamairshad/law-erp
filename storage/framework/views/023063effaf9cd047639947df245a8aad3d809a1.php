<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<style>
#example_wrapper
{
    margin-top:70px  !important;
}
</style>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Companies</h3>
            <?php if(Gate::check('erp_companies_create')): ?>
                <a href="<?php echo e(route('admin.companies.create')); ?>" style = "float:right;" class="btn btn-success pull-right">Add New Company</a>
            <?php endif; ?>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($Companies) > 0 ? 'datatable' : ''); ?>">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>View Company Branches</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                <?php if(count($Companies) > 0): ?>
                    <?php $__currentLoopData = $Companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($Company->id); ?>">
                            <td><?php echo e($Company->name); ?></td>
                            <td><?php echo e($Company->address); ?></td>
                             <td>
                                 <a href="<?php echo e(route('admin.view-company-branch',[$Company->id])); ?>"><button class="btn btn-primary">View Branches</button></a>
                                 <a href="<?php echo e(url('companies-list',$Company->id)); ?>"><button class="btn btn-primary">Detail</button></a>
                             </td>
                            <td>
                                <?php if(Gate::check('erp_companies_edit')): ?>
                                    <a href="<?php echo e(route('admin.companies.edit',[$Company->id])); ?>" class="btn btn-xs btn-info"><?php echo app('translator')->getFromJson('global.app_edit'); ?></a>
                                <?php endif; ?>
                                <?php if(Gate::check('companies_destroy')): ?>
                                    
                                        
                                        
                                        
                                        
                                    
                                    
                                <?php endif; ?>
                            </td>


                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>