<?php $request = app('Illuminate\Http\Request'); ?>

        <?php if(

            in_array('pay_grades_manage', $per) ||
            in_array('pay_roll_manage', $per) ||
            in_array('employment_statuses_manage', $per) ||
            in_array('job_categories_manage', $per) ||
            in_array('job_title_manage', $per) ||
            in_array('work_shifts_manage', $per) ||
            in_array('organizations_manage', $per) ||
            in_array('organization_locations_manage', $per) ||
            in_array('skills_manage', $per) ||
            in_array('educations_manage', $per) ||
            in_array('licenses_manage', $per) ||
            in_array('languages_manage', $per) ||
            in_array('memberships_manage', $per)||


            in_array('leave_periods_manage', $per) ||
            in_array('leave_types_manage', $per) ||
            in_array('work_weeks_manage', $per) ||
            in_array('holidays_manage', $per) ||
            in_array('leave_entitlements_manage', $per) ||
            in_array('leave_requests_manage', $per) ||
            in_array('leaves_manage', $per) ||


            in_array('attendance_manage', $per) ||
            in_array('attendance_settings_manage', $per) ||
            in_array('employee_working_days_manage', $per) ||

            in_array('employee_resources_manage', $per)||
            in_array('employee_banks_manage', $per)||
            in_array('employees_suggestion_manage', $per) ||


            in_array('employee_target_manage', $per)||
            in_array('assign_target_manage', $per)||
            in_array('assign_target_details', $per) ||


            in_array('employee_tada_manage', $per)||
            in_array('employees_tada_amount_manage', $per) ||


            in_array('payroll_manage', $per) ||
            in_array('employees_manage', $per) ||
            in_array('employee_kpis_manage', $per)


        ): ?>
    <li class="treeview<?php echo e(($request->segment(1) == 'hrm') ? ' active' : ''); ?>">
        <a href="#">
            <i class="fa fa-laptop"></i>
            <span class="title">HR Management</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>

        <ul class="treeview-menu">
            
            <?php if(
                  in_array('pay_grades_manage', $per) ||
                  in_array('pay_roll_manage', $per) ||
                  in_array('employment_statuses_manage', $per) ||
                  in_array('job_categories_manage', $per) ||
                  in_array('job_title_manage', $per) ||
                  in_array('work_shifts_manage', $per) ||
                  in_array('organizations_manage', $per) ||
                  in_array('organization_locations_manage', $per) ||
                  in_array('skills_manage', $per) ||
                  in_array('educations_manage', $per) ||
                  in_array('licenses_manage', $per) ||
                  in_array('languages_manage', $per) ||
                  in_array('memberships_manage', $per)

              ): ?>
            <li class="treeview hr-settings <?php echo e((
                    $request->segment(2) == 'pay_grades' ||
                    $request->segment(2) == 'pay_roll' ||
                    $request->segment(2) == 'employee_attendance' ||
                    $request->segment(2) == 'employment_statuses' ||
                    $request->segment(2) == 'job_categories' ||
                    $request->segment(2) == 'job_title' ||
                    $request->segment(2) == 'work_shifts' ||
                    $request->segment(2) == 'organizations' ||
                    $request->segment(2) == 'organization_locations' ||
                    $request->segment(2) == 'skills' ||
                    $request->segment(2) == 'educations' ||
                    $request->segment(2) == 'licenses' ||
                    $request->segment(2) == 'languages' ||
                    $request->segment(2) == 'memberships'

                    ) ? ' active' : ''); ?>">

                <a href="#">HR Settings
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>

                <ul class="treeview-menu">
                    <?php if(
                          in_array('pay_grades_manage', $per) ||
                          in_array('pay_roll_manage', $per) ||
                          in_array('employment_statuses_manage', $per) ||
                          in_array('job_categories_manage', $per) ||
                          in_array('job_title_manage', $per) ||
                          in_array('work_shifts_manage', $per)

                      ): ?>
                    <li class="treeview<?php echo e((
                        $request->segment(2) == 'pay_grades' ||
                        $request->segment(2) == 'employment_statuses' ||
                        $request->segment(2) == 'job_categories' ||
                        $request->segment(2) == 'job_title' ||
                        $request->segment(2) == 'work_shifts'
                        ) ? ' active' : ''); ?>">

                        <a href="#">Job Settings
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <?php if(in_array('pay_grades_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'pay_grades' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.pay_grades.index')); ?>">Pay Grades</a></li>
                            <?php endif; ?>

                            <?php if(in_array('employment_statuses_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'employment_statuses' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employment_statuses.index')); ?>">Employment Statuses</a></li>
                            <?php endif; ?>
                            <?php if(in_array('job_categories_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'job_categories' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.job_categories.index')); ?>">Job Categories</a></li>
                            <?php endif; ?>
                            <?php if(in_array('job_title_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'job_title' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.job_title.index')); ?>">Job Title</a></li>
                            <?php endif; ?>
                            <?php if(in_array('work_shifts_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'work_shifts' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.work_shifts.index')); ?>">Work Shifts</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if(
                         in_array('organizations_manage', $per) ||
                         in_array('organization_locations_manage', $per)

                     ): ?>
                        <li class="treeview<?php echo e((
                            $request->segment(2) == 'organizations' ||
                            $request->segment(2) == 'organization_locations'
                            ) ? ' active' : ''); ?>">
                            <a href="#">Organizations
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('organizations_manage', $per)): ?>
                                    <li class="<?php echo e($request->segment(2) == 'organizations' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.organizations.index')); ?>">General Information</a></li>
                                <?php endif; ?>
                                <?php if(in_array('organization_locations_manage', $per)): ?>
                                    <li class="<?php echo e($request->segment(2) == 'organization_locations' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.organization_locations.index')); ?>">Locations</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                        <?php if(
                        in_array('skills_manage', $per) ||
                        in_array('educations_manage', $per) ||
                        in_array('licenses_manage', $per) ||
                        in_array('languages_manage', $per) ||
                        in_array('memberships_manage', $per)

                        ): ?>
                    <li class="treeview<?php echo e((
                        $request->segment(2) == 'skills' ||
                        $request->segment(2) == 'educations' ||
                        $request->segment(2) == 'licenses' ||
                        $request->segment(2) == 'languages' ||
                        $request->segment(2) == 'memberships'
                        ) ? ' active' : ''); ?>">
                        <a href="#">Qualifications
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <?php if(in_array('skills_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'skills' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.skills.index')); ?>">Skills</a></li>
                            <?php endif; ?>
                            <?php if(in_array('educations_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'educations' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.educations.index')); ?>">Educationss</a></li>
                            <?php endif; ?>
                            <?php if(in_array('licenses_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'licenses' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.licenses.index')); ?>">Licenses</a></li>
                            <?php endif; ?>
                            <?php if(in_array('languages_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'languages' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.languages.index')); ?>">Languages</a></li>
                            <?php endif; ?>
                            <?php if(in_array('memberships_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'memberships' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.memberships.index')); ?>">Memberships</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                </ul>
            </li>
            <?php endif; ?>


            

            
            <?php if(
               in_array('leave_periods_manage', $per) ||
               in_array('leave_types_manage', $per) ||
               in_array('work_weeks_manage', $per) ||
               in_array('holidays_manage', $per) ||
               in_array('leave_entitlements_manage', $per) ||
               in_array('leave_requests_manage', $per) ||
               in_array('leaves_manage', $per)

               ): ?>


            <li class="treeview<?php echo e((
                        $request->segment(2) == 'leave_periods' ||
                        $request->segment(2) == 'leave_types' ||
                        $request->segment(2) == 'work_weeks' ||
                        $request->segment(2) == 'holidays' ||
                        $request->segment(2) == 'leave_entitlements' ||
                        $request->segment(2) == 'leave_requests' ||
                        $request->segment(2) == 'leaves'

                        ) ? ' active' : ''); ?>">
                <a href="#">Leave Management
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <?php if(
                       in_array('leave_periods_manage', $per) ||
                       in_array('leave_types_manage', $per) ||
                       in_array('work_weeks_manage', $per) ||
                       in_array('holidays_manage', $per)
                       ): ?>
                    <li class="treeview<?php echo e((
                        $request->segment(2) == 'leave_periods' ||
                        $request->segment(2) == 'leave_types' ||
                        $request->segment(2) == 'work_weeks' ||
                        $request->segment(2) == 'holidays'
                        ) ? ' active' : ''); ?>">
                        <a href="#">Leave Settings
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <?php if(in_array('leave_periods_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'leave_periods' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.leave_periods.index')); ?>">Leave Periods</a></li>
                            <?php endif; ?>
                            <?php if(in_array('leave_types_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'leave_types' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.leave_types.index')); ?>">Leave Types</a></li>
                            <?php endif; ?>
                            <?php if(in_array('work_weeks_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'work_weeks' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.work_weeks.index')); ?>">Work Weeks</a></li>
                            <?php endif; ?>
                            <?php if(in_array('holidays_manage', $per)): ?>
                                <li class="<?php echo e($request->segment(2) == 'holidays' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.holidays.index')); ?>">Holidays</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                    
                        
                    
                    <?php if(in_array('leave_requests_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'leave_requests' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.leave_requests.index')); ?>">Leave Requests</a></li>
                    <?php endif; ?>
                    <?php if(in_array('leaves_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'leaves' && ! $request->segment(3) == 'approved' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.leaves.index')); ?>">Manage Leaves</a></li>
                    <?php endif; ?>

                        <?php if(in_array('leaves_manage', $per)): ?>
                            <li class="<?php echo e($request->segment(2) == 'leaves' && $request->segment(3) == 'approved' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.leaves.approved')); ?>">Approved Leaves</a></li>
                        <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>

            


             

            <?php if(
               in_array('attendance_manage', $per) ||
               in_array('attendance_settings_manage', $per) ||
               in_array('employee_working_days_manage', $per)

               ): ?>

            <li class="treeview<?php echo e((
                        $request->segment(2) == 'attendance' ||
                        $request->segment(2) == 'attendance_settings'
                        ) ? ' active' : ''); ?>">
                <a href="#">Manage Attendance
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">

                    <?php if(in_array('attendance_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'attendance' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.attendance.index')); ?>">Attendance List</a></li>
                        <li class="<?php echo e($request->segment(2) == 'attendance' && $request->segment(3) == 'requests' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.attendance.requests')); ?>">Manual Request</a></li>
                    <?php endif; ?>

                    <?php if(in_array('attendance_settings_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'attendance_settings' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.attendance_settings.index')); ?>">Attendance Settings</a></li>
                    <?php endif; ?>

                    <?php if(in_array('employee_working_days_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'employee_working_days' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employee_working_days')); ?>">Employee Working Days</a></li>
                    <?php endif; ?>

                </ul>
            </li>
            <?php endif; ?>
            

            
            <?php if(
            in_array('employee_resources_manage', $per)||
            in_array('employee_banks_manage', $per)||
            in_array('employees_suggestion_manage', $per)
            ): ?>


            <li class="treeview<?php echo e((
                        $request->segment(2) == 'employee_resources' ||
                        $request->segment(2) == 'employees_suggestion'

                        ) ? ' active' : ''); ?>">
                <a href="#">Manage Employees
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">

                    <?php if(in_array('employee_resources_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'employee_resources' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employee_resources.index')); ?>">Employees Resources</a></li>
                    <?php endif; ?>

                    <?php if(in_array('employee_banks_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'employee_banks' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employee_banks.index')); ?>">Employees Banks</a></li>
                    <?php endif; ?>

                    <?php if(in_array('employees_suggestion_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'employees_suggestion' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employees_suggestion.index')); ?>">Manage Employees Suggestions</a></li>
                    <?php endif; ?>

                </ul>
            </li>
            <?php endif; ?>
            

            
            <?php if(
               in_array('employee_target_manage', $per)||
               in_array('assign_target_manage', $per)||
               in_array('assign_target_details', $per)
               ): ?>

            <li class="treeview<?php echo e((
                        $request->segment(2) == 'employee_target' ||
                         $request->segment(2) == 'assign_target'
                        ) ? ' active' : ''); ?>">
                <a href="#">Target
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">

                    <?php if(in_array('employee_target_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'employee_target' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employee_target.index')); ?>"> Target Config.</a></li>
                    <?php endif; ?>
                    <?php if(in_array('assign_target_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'assign_target' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.assign_target.index')); ?>">Assign Target </a></li>
                    <?php endif; ?>

                    <?php if(in_array('assign_target_details',$per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'assign_target'  && $request->segment(3) == 'ytd' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.assign_target.ytd')); ?>">YTD Target </a></li>
                    <?php endif; ?>

                </ul>
            </li>
            <?php endif; ?>
            

            <?php if(in_array('employee_kpis_manage', $per)): ?>
                <li class="<?php echo e($request->segment(2) == 'employee_kpis_manage' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employee_kpis.index')); ?>"> Employee KPIs</a></li>
            <?php endif; ?>

            
            <?php if(
              in_array('employee_tada_manage', $per)||
              in_array('employees_tada_amount_manage', $per)
              ): ?>
            <li class="treeview<?php echo e((
                       $request->segment(2) == 'employee_tada'
                        ) ? ' active' : ''); ?>">
                <a href="#">TA/DA
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">

                    <?php if(in_array('employee_tada_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'employee_tada' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employee_tada.index')); ?>">TA/DA Config.</a></li>
                    <?php endif; ?>
                    <?php if(in_array('employees_tada_amount_manage', $per)): ?>
                        <li class="<?php echo e($request->segment(2) == 'employees_tada_amount' ? 'active active-sub' : ''); ?>"><a href="<?php echo e(route('hrm.employees_tada_amount.index')); ?>">Manage TA/DA</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <?php endif; ?>
            

            
                
                    
                        
                    
                
            



            

            
                
                    
                        
                    
                
            

            <?php if(in_array('employees_manage', $per)): ?>
                <li class="<?php echo e($request->segment(2) == 'EmpAvailable' ? 'active active-sub' : ''); ?>">
                    <a href="<?php echo e(route('admin.employees.EmpAvailable')); ?>">
                        <span class="title"> Employee Availability </span>
                    </a>
                </li>
            <?php endif; ?>

            <?php if(in_array('payroll_manage', $per)): ?>
                <li class="<?php echo e($request->segment(2) == 'payroll' ? 'active active-sub' : ''); ?>">
                    <a href="<?php echo e(route('hrm.payroll.index')); ?>">
                        <span class="title"> Payroll </span>
                    </a>
                </li>
            <?php endif; ?>

            <?php if(in_array('payroll_manage', $per)): ?>
                <li class="<?php echo e($request->segment(3) == 'salary_sheet'  || $request->segment(3) == 'salary_sheet'? 'active active-sub' : ''); ?>">
                    <a href="<?php echo e(route('hrm.payroll.salary_sheet')); ?>">
                        <span class="title"> Salary Sheet </span>
                    </a>
                </li>
            <?php endif; ?>

            

        </ul>
    </li>
<?php endif; ?>