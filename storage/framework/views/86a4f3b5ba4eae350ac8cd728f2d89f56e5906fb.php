<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Active Session Section</h3>
                
                <a href="<?php echo e(url('active-session-section/create')); ?>" style = "float:right;" class="btn btn-success pull-right">Add New Active Session Section</a>
            
        
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            <table class="table table-bordered table-striped <?php echo e(count($active_sessions) > 0 ? 'datatable' : ''); ?>">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Session</th>
                    <th>Active Session Section</th>
                    
                </tr>
                </thead>

                <tbody>
                    <?php ($r = 1); ?>
                <?php if(count($active_sessions) > 0): ?>
                    <?php $__currentLoopData = $active_sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $active_session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($active_session->id); ?>">
                            <td> <?php echo e($r); ?> </td>
                            <td><?php echo e($active_session->section->name); ?></td>
                            <td><?php echo e($active_session->activesession->title); ?></td>                 
                        </tr>
                        <?php ($r++); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>