
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<title>Roots IVY ERP Network</title>
<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/amazeui-datetimepicker/css/amazeui.datetimepicker.css" rel="stylesheet">
<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/jquery-simple-datetimepicker/jquery.simple-dtpicker.css" rel="stylesheet">
<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/pickerjs/picker.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo e(url('public')); ?>/daterangepicker.css" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
		<!--- Favicon -->
		<link rel="icon" href="<?php echo e(url('public/theme')); ?>/assets/img/brand/favicon.png" type="image/x-icon"/>

		<!--- Icons css -->
		<link href="<?php echo e(url('public/theme')); ?>/assets/css/icons.css" rel="stylesheet">
		<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>
		<!-- Owl-carousel css-->
<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet"/>

		<!--- Right-sidemenu css -->
		<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/sidebar/sidebar.css" rel="stylesheet">

		<!--- Custom Scroll bar -->
		<link href="<?php echo e(url('public/theme')); ?>/assets/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>
		<link rel="stylesheet" href="https://unpkg.com/multiple-select@1.3.1/dist/multiple-select.min.css">
		<!--- Style css -->
		<link href="<?php echo e(url('public/theme')); ?>/assets/css/style.css" rel="stylesheet">
		<link href="<?php echo e(url('public/theme')); ?>/assets/css/skin-modes.css" rel="stylesheet">
	
		<!--- Sidemenu css -->
		<link href="<?php echo e(url('public/theme')); ?>/assets/css/sidemenu.css" rel="stylesheet">

		<!--- Animations css -->
		<link href="<?php echo e(url('public/theme')); ?>/assets/css/animate.css" rel="stylesheet">
		
		<!--- Switcher css -->
		<link href="<?php echo e(url('public/theme')); ?>/assets/switcher/css/switcher.css" rel="stylesheet">
		<link href="<?php echo e(url('public/theme')); ?>/assets/switcher/demo.css" rel="stylesheet">
		
		<link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/select2/dist/css/select2.min.css">
		



	

	