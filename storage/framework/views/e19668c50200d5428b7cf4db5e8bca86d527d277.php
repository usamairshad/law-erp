<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Assign Compulsory Courses To <?php echo e($program->name); ?></h3>
            <a href="<?php echo e(route('admin.boards.index')); ?>" style = "float:right;" class="btn btn-success pull-right">Back</a>
        </div>


       <?php echo Form::open(['method' => 'POST', 'route' => ['admin.compulsory-courses-store'], 'id' => 'validation-form' , 'enctype' => 'multipart/form-data']); ?> 
       <?php echo Form::token();; ?>

       <input type="hidden" name="program_id" value="<?php echo e($program->id); ?>">
                                        
        <div class="box-body" style = "margin-top:50px;">
        <div class="form-group col-md-12">
  <?php echo Form::label('branch_id', 'Select Courses', ['class' => 'control-label']); ?>

  <select style="width: 100%;" class="js-example-basic-single" name="course_id[]" multiple="multiple">

  <?php if(count($courses) > 0): ?>
                                                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($course->id); ?>"><?php echo e($course->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>   
  </select>
</div>

        </div>

        <div class="box-footer">
            <button id="btn" class="btn btn-primary" type="submit">Save</button>
        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo e(url('public/adminlte')); ?>/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo e(url('public/js/admin/employees/create_modify.js')); ?>" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.phone').mask('0000-0000000');
            $('.cnic').mask('00000-0000000-0');
            $('.home_phone').mask('000-00000000');
            $('.reg_no').mask('00-00000');   
        })
    </script>
    
<script type="text/javascript">
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
    
});

$(".radio").change(function(){

if($(this).val()=="no")
{
  $('#banknotexist').show();
  $('#bankexist').hide();
}
else
{

  $('#banknotexist').hide();
  $('#bankexist').show();
}

});
</script>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>