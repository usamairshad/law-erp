<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Roles</h1>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Update Role</h3>
            <a href="<?php echo e(route('admin.roles.index')); ?>" class="btn btn-success pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo Form::model($Role, ['method' => 'PUT', 'id' => 'validation-form', 'route' => ['admin.roles.update', $Role->id]]); ?>

        <div class="box-body">
            
            <?php echo $__env->make('admin.roles.new_fields', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <?php echo Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger globalSaveBtn']); ?>

        </div>
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(url('public/js/admin/roles/create_modify.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>