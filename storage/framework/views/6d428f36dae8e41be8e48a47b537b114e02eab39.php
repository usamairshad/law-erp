<div class="form-group col-md-3 <?php if($errors->has('staff_type')): ?> has-error <?php endif; ?>">
    <?php echo e(Form::label('Timetable Category Name*')); ?>

    <?php echo e(Form::select('timetable_category_id', $timetable_category, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required'))); ?>

</div>
<div class="form-group col-md-3 <?php if($errors->has('staff_type')): ?> has-error <?php endif; ?>">
    <?php echo e(Form::label('Branch Name*')); ?>

    <?php echo e(Form::select('branch_id', $branchList, null, array('class'=>'form-control', 'placeholder'=>'Please select ...','required'))); ?>

</div>
<div class="form-group col-md-3 <?php if($errors->has('start_date')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('title', 'Title*', ['class' => 'control-label']); ?>

    <input type="text" name="title" class="form-control" required maxlength="30">
</div>
<div class="form-group col-md-3 <?php if($errors->has('start_time')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('start_time', 'Start Time*', ['class' => 'control-label']); ?>

    <input type="time" name="start_time" class="form-control" required maxlength="10">
</div>
<div class="form-group col-md-3 <?php if($errors->has('end_time')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('end_time', 'End Time*', ['class' => 'control-label']); ?>

    <input type="time" name="end_time" class="form-control" required maxlength="10">
</div>