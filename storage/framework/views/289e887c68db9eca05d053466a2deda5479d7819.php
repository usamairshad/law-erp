<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Groups</h3>
                                
                <a style = "float:right; margin-bottom:20px;" href="<?php echo e(route('admin.groups.create')); ?>" class="btn btn-success pull-right">Add New Group</a>
            
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="5%">ID</th>
                    
                    <th width="20%">Account Number</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                <?php if(count($Groups) > 0): ?>
                    <?php $__currentLoopData = $Groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $Group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($id); ?>">
                            <td><?php echo e(sprintf('%04d',$id)); ?></td>
                            
                            <td><?php echo e($Group['number']); ?></td>
                            <td><?php echo e($Group['name']); ?></td>
                            <td>
                                <?php if(!in_array($id, Config::get('constants.accounts_main_heads'))): ?>
                                    
                                        <a href="<?php echo e(route('admin.groups.edit',[$id])); ?>" class="btn btn-xs btn-info"><?php echo app('translator')->getFromJson('global.app_edit'); ?></a>
                                    

                                    
                                        
                                    
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <tr>
                        <td colspan="5"><?php echo app('translator')->getFromJson('global.app_no_entries_in_table'); ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>


								</div>

                                </div>
                                </div>
                              
          
        </div>
        <!-- /.box-header -->
      
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    
        
            
        
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>