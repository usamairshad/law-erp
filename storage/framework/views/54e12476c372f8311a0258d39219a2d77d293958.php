<?php $request = app('Illuminate\Http\Request'); ?>


<?php $__env->startSection('content'); ?>

    <div class="box-header with-border">
        <h3 class="page-title" style="float: left; margin: 0">User Create</h3>

    </div>

    <div class="panel panel-default">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title"></h3>

            <a href="" class="btn btn-success pull-right" data-toggle="modal" data-target=".exampleModal" onclick="resetform()">Add New User</a>

        </div>



        <div class="panel-body table-responsive">
            <?php if($message = Session::get('success')): ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong><?php echo e($message); ?></strong>
                </div>
            <?php endif; ?>
            <?php if($message = Session::get('delete')): ?>
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong><?php echo e($message); ?></strong>
                </div>
            <?php endif; ?>
            <table class="table table-bBanked table-striped  dt-select">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Branch Name</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>

                    <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($record->name); ?></td>
                        <td><?php echo e($record->email); ?></td>
                        <td><?php echo e($record['branchuser']['name'] ?? ''); ?></td>
                        <td><?php echo e($record->n); ?></td>
                        <td>
                            <form method="post" action="<?php echo e(route('create-user.destroy',$record->id)); ?>">
                                <?php echo e(method_field('delete')); ?>

                                <?php echo csrf_field(); ?>

                                
                                <a  class="btn btn-primary update btn-sm" href="<?php echo e(route('create-user.edit',$record->id)); ?>">Update</a>

                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div class="modal fade exampleModal" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="add_new" method="post" action="<?php echo e(route('create-user.store')); ?>">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="id" id="hidden_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Name:</label>
                                    <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Role Type:</label>
                                    <select class="form-control" name="role_type" id="role_type">
                                        <option disabled selected>--Select--</option>
                                
                                        <option value="erp">ERP</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Role:</label>
                                    <select class="form-control" name="role_id" id="roles">
                                    
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Branch:</label>
                                    <select class="form-control" name="branch_id" id="branch_id">
                                        <option disabled selected>--Select--</option>
                                        <?php echo App\Models\Admin\Branches::branchList(); ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Email:</label>
                                    <input type="text" placeholder="Enter Email" class="form-control" value="" name="email" id="email">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Password:</label>
                                    <input type="password" placeholder="Enter Password" class="form-control" value="" name="password" id="password" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="resetform()" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $(".update").on("click",function() {
            var name = $(this).data('record').name;
            var email = $(this).data('record').email;

            // var role_id = $(this).data('record').role_id;
            var role_type = $(this).data('record').role_type;
            var branch = $(this).data('record')['branchuser'].id;
            var hidden_id = $(this).data('record').id;

            $("#add_new select[id~='branch_id']").val(branch);
            $("#add_new  select[id~='role_type']").val(role_type);

            $('#name').val(name);
            $('#email').val(email);

            // $('#branch').val(branch);
            $('#hidden_id').val(hidden_id);

        });

        function resetform(){

            $("#add_new").trigger("reset");
        }

        $(function(){
            $('#role_type').change(function(){
               $("#roles option").remove();;
               var id = $(this).val();
               console.log(id)
               $.ajax({
                  url : '<?php echo e(route( 'admin.load-roles' )); ?>',
                  data: {
                    "_token": "<?php echo e(csrf_token()); ?>",
                    "id": id
                    },
                  type: 'get',
                  dataType: 'json',
                  success: function( result )
                  {     
                        console.log(result)
                        $('#roles').append($('<option disabled selected>--Select--</option>'));
                        $.each( result, function(k, v) {
                            $('#roles').append($('<option value=' +  v.id + '>' + v.name + '</option>'));
                        });
                  },
                  error: function()
                 {
                     alert('error...');
                 }
               });
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>