




<div class="form-group col-md-6 <?php if($errors->has('section_id')): ?> has-error <?php endif; ?>" style = "float:left;">
    <?php echo Form::label('section_id', 'Section*', ['class' => 'control-label']); ?>

        <select class="form-control  section_id" name="section_id" required="required"> 
            <option value="0" selected="disable">--select</option>
            <?php $__currentLoopData = $sessionList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($session->id); ?>"><?php echo e($session->name); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
</div>

<div class="form-group col-md-6 <?php if($errors->has('section_id')): ?> has-error <?php endif; ?>"  style = "float:left;">
    <?php echo Form::label('active_session_id', 'Active Session*', ['class' => 'control-label']); ?>

        <select class="form-control  section_id" name="active_session_id" required="required"> 
            <option value="0" selected="disable">--select</option>
            <?php $__currentLoopData = $activesessionList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activesession): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($activesession->id); ?>"><?php echo e($activesession->title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
</div>


