<?php $request = app('Illuminate\Http\Request'); ?>


<?php $__env->startSection('breadcrumbs'); ?>
    <section class="content-header" style="padding: 10px 15px !important;">
        <h1>Academics Group Course</h1>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-list"></i><h3 class="box-title">List</h3>
            <a  href="<?php echo e(route('admin.academics-groups-courses-create')); ?>" class="btn btn-success pull-right">Create</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body pad table-responsive">
            
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?> 
    <script>
        $(document).ready(function(){
            $('.datatable').DataTable()
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>