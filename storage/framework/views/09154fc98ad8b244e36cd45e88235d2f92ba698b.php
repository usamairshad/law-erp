<?php $request = app('Illuminate\Http\Request'); ?>
<?php $Currency = app('\App\Helpers\Currency'); ?>

<?php $__env->startSection('stylesheet'); ?>
    <link rel="stylesheet" href="<?php echo e(url('public/adminlte')); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
						<!--div-->
						<div class="card">
							<div class="card-body">
								<div class="main-content-label mg-b-5">
                                <h3 style = "float:left;">Update Course</h3>
    <div class="box box-primary">
        <a href="<?php echo e(route('admin.courses.index')); ?>" style = "float:right;" class="btn btn-success pull-right">Back</a>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo Form::open(['method' => 'PUT', 'enctype' => 'multipart/form-data', 'route' => ['admin.courses.update',$courses->id], 'id' => 'validation-form']); ?>

        <div id="show">
            <div id="box" class="box-body" style = "padding-top:40px;">
             
                <input type="hidden" name="course_id" value="<?php echo e($courses->id); ?>">
                <div class="form-group col-md-5 <?php if($errors->has('name')): ?> has-error <?php endif; ?>" style ="float:left;">
                    <?php echo Form::label('name', 'Course Name*', ['class' => 'control-label']); ?>

                   <input type="text" name="name" value="<?php echo e($courses->name); ?>"  required="required" placeholder="Enter Course Name" class="form-control" />
                </div>

                    <div class="form-group col-md-3 <?php if($errors->has('subject_code')): ?> has-error <?php endif; ?>">
                    <?php echo Form::label('subject_code', 'Subject Code*', ['class' => 'control-label']); ?>

                   <input type="text" name="subject_code"  required="required"  value="<?php echo e($courses->subject_code); ?>" placeholder="Enter Subject Code" class="form-control" />
                </div>
                <div class="form-group col-md-3 <?php if($errors->has('credit_hours')): ?> has-error <?php endif; ?>">
                    <?php echo Form::label('credit_hours', 'Credit hours*', ['class' => 'control-label']); ?>

                   <input type="text" name="credit_hours"  required="required" value="<?php echo e($courses->credit_hours); ?>" placeholder="Enter Credit Hours" class="form-control" />
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <select name="type" value="<?php echo e($courses->type); ?>" class="form-control" required >
                            <option>Select Type</option>
                            <?php if($type = 'cie'): ?>
                            <option value="cie"  selected="cie">Cie</option>
                            <option value="edexcle">Edexcle</option>
                            <?php elseif($type = 'edexcle'): ?>
                            <option value="male">Cie</option>
                            <option value="edexcle"selected="edexcle">Edexcle</option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-3 <?php if($errors->has('actual_fee')): ?> has-error <?php endif; ?>">
                    <?php echo Form::label('actual_fee', 'Actual Fee*', ['class' => 'control-label']); ?>

                   <input type="text" name="actual_fee"  required="required"  value="<?php echo e($courses->actual_fee); ?>" placeholder="Enter Actual Fee" class="form-control" />
                </div>
                <div class="form-group col-md-3 <?php if($errors->has('subject_fee')): ?> has-error <?php endif; ?>">
                    <?php echo Form::label('subject_fee', 'Subject Fee*', ['class' => 'control-label']); ?>

                   <input type="text" name="subject_fee"  required="required"  value="<?php echo e($courses->subject_fee); ?>"  placeholder="Enter Actual Fee" class="form-control" />
                </div>
                <div class="form-group col-md-3 <?php if($errors->has('subject_optional_code')): ?> has-error <?php endif; ?>">
                    <?php echo Form::label('subject_optional_code', 'Subject Optional Code*', ['class' => 'control-label']); ?>

                   <input type="text" name="subject_optional_code"  value="<?php echo e($courses->subject_optional_code); ?>" placeholder="Enter Subject Optional Code" class="form-control" />
                </div>
                <input type="hidden" name="section_id" value="<?php echo e($courses->id); ?>">
                <div class="form-group col-md-6 <?php if($errors->has('description')): ?> has-error <?php endif; ?>" style ="float:left;">
                    <?php echo Form::label('description', 'Description', ['class' => 'control-label']); ?>

                     <input type="text" name="description" value="<?php echo e($courses->description); ?>" required="required" placeholder="Enter Description" class="form-control" />
                </div>
            </div>  
            <button type="submit"  class="btn btn-success col-md-12" > Update</button>
        </div>

        <!-- /.box-body -->
        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style type="text/css">

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script type="text/javascript">
    
    <script src="<?php echo e(url('public/js/admin/companies/create_modify.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>