<?php
/**
 * Created by PhpStorm.
 * User: zahra
 * Date: 31/01/2017
 * Time: 03:27 PM
 */
//

return [

    'product_type_array' => array(
        1 => 'A4 AIO',
        2 => 'A4 MFP',
        3 => 'A4 SFP',
        4 => 'AIO',
        5 => 'AIO/SFP',
        6 => 'IP',
        7 => 'LBP',
        8 => 'LBP/AIO',
        9 => 'MFP',
        10 => 'Office',
        11 => 'Parts',
        12 => 'PP',
        13 => 'PP - Mono',
        14 => 'SFP',
    ),

    'product_class_array' => array(
        1 => 'Printer',
        2 => 'Photocopier',
        3 => 'Digital Press',
        4 => 'Textile Printer',
        5 => 'Industrial Printer'
    ),

    'product_color_array' => array(
        1 => 'mono',
        2 => 'color',
    ),
    'product_size_array' => array(
        1 => 'A3',
        2 => 'A4',
    ),
    'product_function_array' => array(
        1 => 'MFP',
        2 => 'PP',
        3 => 'Printer',
    ),
    'mode_of_payment' => array(
        '' => 'Select Payment Mode',
        1 => 'LC',
        2 => 'TT',
    ),
    'lc_types' => array(
        '' => 'Select Lc Type',
        1 => 'LC Sight',
       // 2 => 'Other',
    ),
    'tt_types' => array(
        '' => 'Select TT Type',
        1 => 'Contracts',
        2 => 'BME Finance',
    ),
    'delivery_term' => array(
        '' => 'Select Delivery Terms',
        1 => 'FOB',
        2 => 'CIF',
        3 => 'CFR',
        4 => 'EXW',
        5 => 'FCA',
    ),
    'import_duty' => array(
        '' => 'Select Duty Types',
        1 => 'Insurance',
        2 => 'Bank Charges',
        3 => 'Freight',
        4 => 'Profit Investment',
        5 => 'Murahbah Profit',
        6 => 'Mvmnt/In Bndg',
        7 => 'Duty'
    ),
    'costing' => array(
        '' => 'Select Type',
        1 => 'Costing',
        2 => 'Taxing'
    ),

    'invoice_depts' => array(
        '' => 'Select a department',
        'sales' => 'Sales',
        'engineering' => 'Engineering',
        'company' => 'Company'
    ),
    'account_level' => array(
        '' => 'Select a Level',
        '1' => 'Level 1',
        '2' => 'Level 2',
        '3' => 'Level 3',
        '4' => 'Level 4',
        '5' => 'Level 5',
        '6' => 'Level 6',
        '7' => 'Detail Report'
    ),
    'entry_type' => array(
        '' => 'Select entry type',
        '1' => 'Journal Voucher',
        '2' => 'Cash Receipt Voucher',
        '3' => 'Cash Payment Voucher',
        '4' => 'Bank Receipt Voucher',
        '5' => 'Bank Payment Voucher'
    ),

];