<?php
/**
 * Created by PhpStorm.
 * User: mustafa.mughal
 * Date: 12/7/2017
 * Time: 10:27 AM
 */

return [
    'currency_symbols_setting_id' => 2,
    'currency_symbols' => array('##,###.##' => '##,###.##', '##,##.##' => '##,##.##', '###,###.##' => '###,###.##'),
    'date_format_setting_id' => 3,
    'date_format' => array('d-M-Y|dd-M-yy' => 'Day-Month-Year', 'M-d-Y|M-dd-yy' => 'Month-Day-Year', 'Y-M-d|yy-M-dd' => 'Year-Month-Day'),
    'payment_type' => array(1 => 'Cash', 2 => 'Check', 3 => 'American Express', 4 => 'Discover', 5 => 'MasterCard', 6 => 'Visa', 7 => 'Other Credit Card', 8 => 'Other', 9 => 'Debit Card', 10 => 'Gift Card', 11 => 'E-Check'),
    'accounts_main_heads' => array(1, 2, 3, 4),
    'accounts_company_id' => 1,
    // Accounts Type Fields
    'accounts_assets' => 1,
    'accounts_equity_liabilities' => 2,
    'accounts_expenses' => 4,
    'accounts_incomes' => 3,

    /////////*********///////
    ////////*********///////
    /// Branches
    'accounts_debt_branch'=>126,
    'accounts_loan_branch'=>480,
    'accounts_debt_dealer'=>127,
    'accounts_debt_customer'=>128,
    'account_revenue_other_income'=>12,
//    Start Work By Sakib
    'head_office_id' => 4,
    'lahore_dha_office_id' => 7,

    'tax_236i_receiveable_group_id' => 148,
    'tax_236i_payable_group_id' => 149,
//    End Work By Sakib
    /////// end branches//////

    /////////*********///////
    ////////*********///////
    /// employess
    'accounts_EMP_salary'=>64,
    'accounts_EMP_bonus'=>66,
    'accounts_EMP_eobi'=>376,
    'accounts_EMP_pessi'=>377,
    'accounts_EMP_loan'=>479,
    'accounts_EMP_advance'=>481,
    /////// end employess//////

    /////////*********///////
    ////////*********///////
    /// banks Fields
    'account_bank_balance_local' => 515,
    'account_bank_balance_us' => 516,
    'account_bank_balance_euro' => 517,
    'account_bank_cd_importer' => 471,
    'account_bank_contract' => 474,
    'account_Lc_Transit' => 143,
//    'account_cash_inHand' => 135,
    'account_cash_inHand' => 23,
//    'account_bank_balance' => 137,
    'account_bank_balance' => 25,
//    Start Work By Sakib
    'account_bank_balance_group_id' => 25,
//    End Work By Sakib
    /////////*********///////
    ////////*********///////
    /// supplier
    'accounts_tt_advance' => 475,
    'accounts_BME_importer' => 472,
    'accounts_cd_local' => 473,
    'accounts_stock_inventory' => 152,
    'accounts_sale_revenue' => 30,
    'accounts_cost_sale' => 249,
         ////////
    /////////*********///////
    ////////*********///////
    /// supplier
    'engineering_expenses' => 652,
    'sales_expenses' => 637,
    'admin_accounts_salaries' => 577,
    'admin_accounts_service_amount' => 4,
    'admin_accounts_SalesTax' => 2,
    'admin_accounts_SalesReturn' => 119,
    'admin_accounts_discount' => 3,
    'admin_accounts_cash' => 1,
    'admin_local_stock' => 821,
    ////////
];