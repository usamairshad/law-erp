<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Admin\BranchUser;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\RoleUser;
class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Users',
            'name' => 'erp_users_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_users_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_users_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_users_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_users_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_users_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_users_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ]
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_users_manage');
        $role->givePermissionTo('erp_users_create');
        $role->givePermissionTo('erp_users_edit');
        $role->givePermissionTo('erp_users_active');
        $role->givePermissionTo('erp_users_inactive');
        $role->givePermissionTo('erp_users_destroy');
        $role->givePermissionTo('erp_users_detail');
        $role->givePermissionTo('erp_roles_manage');
        $role->givePermissionTo('erp_permissions_manage');
        $role->givePermissionTo('erp_staff_manage');
        $role->givePermissionTo('erp_staff_create');
        $role->givePermissionTo('erp_staff_edit');
        $role->givePermissionTo('erp_staff_active');
        $role->givePermissionTo('erp_staff_inactive');
        $role->givePermissionTo('erp_staff_destroy');
        $role->givePermissionTo('erp_staff_mass_destroy');
       

        // $role->givePermissionTo('erp_term_manage');
        // $role->givePermissionTo('erp_term_create');
        // $role->givePermissionTo('erp_term_edit');
        // $role->givePermissionTo('erp_term_destroy');
        // $role->givePermissionTo('erp_term_active');
        // $role->givePermissionTo('erp_term_inactive');
        

        // $role->givePermissionTo('erp_discount_type_manage');
        // $role->givePermissionTo('erp_discount_type_create');
        // $role->givePermissionTo('erp_discount_type_active');
        // $role->givePermissionTo('erp_discount_type_inactive');
        // $role->givePermissionTo('erp_discount_type_destroy');
        // $role->givePermissionTo('erp_discount_type_edit');      
          
        // $dfit_head->givePermissionTo('erp_session_manage');
        // $dfit_head->givePermissionTo('erp_session_create');
        // $dfit_head->givePermissionTo('erp_session_edit');
        // $dfit_head->givePermissionTo('erp_session_active');
        // $dfit_head->givePermissionTo('erp_session_inactive');
        // $dfit_head->givePermissionTo('erp_session_destroy');   

        // $dfit_head->givePermissionTo('erp_time_table_category_manage');
        // $dfit_head->givePermissionTo('erp_time_table_category_create');
        // $dfit_head->givePermissionTo('erp_time_table_category_edit');
        // $dfit_head->givePermissionTo('erp_time_table_category_destroy');
        // $dfit_head->givePermissionTo('erp_time_table_category_active');
        // $dfit_head->givePermissionTo('erp_time_table_category_inactive'); 


            // $dfit_head->givePermissionTo('erp_timetable_manage');
            // $dfit_head->givePermissionTo('erp_timetable_create');
            // $dfit_head->givePermissionTo('erp_timetable_edit');
            // $dfit_head->givePermissionTo('erp_timetable_destroy');
            // $dfit_head->givePermissionTo('erp_timetable_active');
            // $dfit_head->givePermissionTo('erp_timetable_inactive'); 


        // $dfit_head->givePermissionTo('erp_class_time_table_manage');
        // $dfit_head->givePermissionTo('erp_class_time_table_create');
        // $dfit_head->givePermissionTo('erp_class_time_table_edit');
        // $dfit_head->givePermissionTo('erp_class_time_table_destroy');
        // $dfit_head->givePermissionTo('erp_class_time_table_active');
        // $dfit_head->givePermissionTo('erp_class_time_table_inactive'); 
       

        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@rootsivy.edu.pk',
            'password' => bcrypt('12345678'),
            'role_id'=>2,
            'active' => 1,
            'branch_id' => 4,
        ]);
        RoleUser::create(['user_id'=>$user->id, 'role_id'=>2]);
        $user->assignRole('admin');

    }
}
