<?php

use Illuminate\Database\Seeder;
use App\Models\Academics\Program;
class ProgramSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Program::insert([
            [
                'name' => 'Early Year Program',
                'description' => 'This is Yearly Year Program',
                'years' => 2010,
                'status' => 'Active',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Primary Year Program',
                'description' => 'This is Primary Year Program',
                'years' => 2010,
                'status' => 'Active',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Secondary Year Program',
                'description' => 'This is Secondary Year Program',
                'years' => 2010,
                'status' => 'Active',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
        ]);
    }
}
