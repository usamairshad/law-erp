<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class AcadamicYearSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Academic Years',
            'name' => 'erp_academic_years_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_academic_years_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_academic_years_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_academic_years_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_academic_years_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_academic_years_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_academic_years_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_academic_years_manage');
        $role->givePermissionTo('erp_academic_years_create');
        $role->givePermissionTo('erp_academic_years_edit');
        $role->givePermissionTo('erp_academic_years_active');
        $role->givePermissionTo('erp_academic_years_inactive');
        $role->givePermissionTo('erp_academic_years_destroy');
        $role->givePermissionTo('erp_academic_years_detail');

        // $DFIT_head = Role::whereName("DFIT_head")->first(); 
        
        // $DFIT_head->givePermissionTo('erp_academic_years_manage');
        // $DFIT_head->givePermissionTo('erp_academic_years_create');
        // $DFIT_head->givePermissionTo('erp_academic_years_edit');
        // $DFIT_head->givePermissionTo('erp_academic_years_active');
        // $DFIT_head->givePermissionTo('erp_academic_years_inactive');
        // $DFIT_head->givePermissionTo('erp_academic_years_destroy');
        // $DFIT_head->givePermissionTo('erp_academic_years_detail');
    }
}
