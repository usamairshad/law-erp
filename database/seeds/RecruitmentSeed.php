<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RecruitmentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		// Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Recruitment',
            'name' => 'erp_recruitment_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Job Post Request Create',
                'name' => 'erp_job_post_request_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Job Post Request Edit',
                'name' => 'erp_job_post_request_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Job Post Create',
                'name' => 'erp_job_post_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Interview Scheduling',
                'name' => 'erp_interview_scheduling_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Interview ReScheduling',
                'name' => 'erp_interview_rescheduling_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Enter Hired Candidate',
                'name' => 'erp_hired_candidate',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ]

            
        ]);

        $role = Role::whereName("HR_head")->first();

        // Assign Permission to 'administrator' role
        $role->givePermissionTo('erp_recruitment_manage');
        $role->givePermissionTo('erp_job_post_request_create');
        $role->givePermissionTo('erp_job_post_request_edit');
        $role->givePermissionTo('erp_job_post_create');
        $role->givePermissionTo('erp_interview_scheduling_create');
        $role->givePermissionTo('erp_interview_rescheduling_create');




        $admin_role = Role::findById(2, 'web');
        $admin_role->givePermissionTo('erp_recruitment_manage');
        $admin_role->givePermissionTo('erp_job_post_request_create');
        $admin_role->givePermissionTo('erp_job_post_request_edit');
        $admin_role->givePermissionTo('erp_job_post_create');
        $admin_role->givePermissionTo('erp_interview_scheduling_create');
        $admin_role->givePermissionTo('erp_interview_rescheduling_create');
    }
}
