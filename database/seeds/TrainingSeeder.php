<?php

use App\Models\Permissions;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class TrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hr_role = Role::whereName("HR")->first();
        $hr_head_role = Role::whereName("HR_head")->first();

        $parent_permission = Permission::create([
            "name" => "erp_training_manage",
            "guard_name" => "web",
            "title" => "Training",
            "main_group" => 1,
            "parent_id" => 0,
            "status" => 1,
        ]);
      
        $hr_role->givePermissionTo($parent_permission->id);
        $hr_head_role->givePermissionTo($parent_permission->id);

        $permissions = [
            ["name" =>  "erp_training_create", "title" => "Create"],
            ["name" => "erp_training_edit", "title" => "Edit"],
            ["name" => "erp_training_destroy", "title" => "Delete"],
            ["name" => "erp_training_activate", "title" => "Activate"],
            ["name" => "erp_training_Inactivate", "title" => "Inactivate"]
        ];
        foreach ($permissions as $permission) {

            $child_permission = Permission::create([
                "name" => $permission["name"],
                "guard_name" => "web",
                "title" => $permission["title"],
                "main_group" => 0,
                "parent_id" => $parent_permission->id,
                "status" => 1,
            ]);
            $hr_role->givePermissionTo($child_permission->id);
            $hr_head_role->givePermissionTo($child_permission->id);
        }
    }
}
