<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Admin\FeeSection;

class FeeSectionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        FeeSection::insert([

            [
                'name' => 'One Time Fee',
                'description'=> '(At the time of Admission)',
                'status'=> 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'One Time Fee',
                'description'=> '(At the time of Admission)',
                'status'=> 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'One Time Fee',
                'description'=> '(At the time of Admission)',
                'status'=> 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'One Time Fee',
                'description'=> '(At the time of Admission)',
                'status'=> 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);
    }
}
