<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class EntriesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Entries',
            'name' => 'erp_entries_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_entries_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_entries_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_entries_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_entries_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_entries_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Bulk Delete',
                'name' => 'erp_entries_mass_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_entries_manage');
        $role->givePermissionTo('erp_entries_create');
        $role->givePermissionTo('erp_entries_edit');
        $role->givePermissionTo('erp_entries_active');
        $role->givePermissionTo('erp_entries_inactive');
        $role->givePermissionTo('erp_entries_destroy');
        $role->givePermissionTo('erp_entries_mass_destroy');

        $accountant = Role::whereName("account")->first();
        $accountant->givePermissionTo('erp_entries_manage');
        $accountant->givePermissionTo('erp_entries_create');
        $accountant->givePermissionTo('erp_entries_edit');
        $accountant->givePermissionTo('erp_entries_active');
        $accountant->givePermissionTo('erp_entries_inactive');
        $accountant->givePermissionTo('erp_entries_destroy');
        $accountant->givePermissionTo('erp_entries_mass_destroy');

        $gm = Role::whereName("GM Finance")->first();
        $gm->givePermissionTo('erp_entries_manage');
        $gm->givePermissionTo('erp_entries_create');
        $gm->givePermissionTo('erp_entries_edit');
        $gm->givePermissionTo('erp_entries_active');
        $gm->givePermissionTo('erp_entries_inactive');
        $gm->givePermissionTo('erp_entries_destroy');
        $gm->givePermissionTo('erp_entries_mass_destroy');
    }
}
