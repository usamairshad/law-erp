<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BanksSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'erp_banks',
            'name' => 'erp_banks_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_banks_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_banks_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_banks_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_banks_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_banks_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_banks_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_banks_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_banks_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_banks_manage');
        $role->givePermissionTo('erp_banks_create');
        $role->givePermissionTo('erp_banks_edit');
        $role->givePermissionTo('erp_banks_destroy');
        $role->givePermissionTo('erp_banks_detail');

        $accountant = Role::whereName("account")->first();
        $accountant->givePermissionTo('erp_banks_manage');
        $accountant->givePermissionTo('erp_banks_create');
        $accountant->givePermissionTo('erp_banks_edit');
        $accountant->givePermissionTo('erp_banks_destroy');
        $accountant->givePermissionTo('erp_banks_detail');

        $gm = Role::whereName("GM Finance")->first();
        $gm->givePermissionTo('erp_banks_manage');
        $gm->givePermissionTo('erp_banks_create');
        $gm->givePermissionTo('erp_banks_edit');
        $gm->givePermissionTo('erp_banks_destroy');
        $gm->givePermissionTo('erp_banks_detail');

//        if(env('APP_ENV') == 'local') {
//            factory(App\Models\Admin\BanksModel::class, 10)->create();
//        }

//        BanksModel::insert([
//            [
//                'group_id'=> 137,
//                'bank_name' => 'MCB',
//                'created_by' => 0,
//                'updated_by' => 0,
//                'deleted_by' => 0,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//            ],
//
//        ]);
    }
}
