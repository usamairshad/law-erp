<?php
use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use \App\Models\Admin\FinancialYears;

class FinancialYearsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Financial Years',
            'name' => 'erp_financial_years_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_financial_years_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_financial_years_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_financial_years_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_financial_years_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_financial_years_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_financial_years_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_financial_years_manage');
        $role->givePermissionTo('erp_financial_years_create');
        $role->givePermissionTo('erp_financial_years_edit');
        $role->givePermissionTo('erp_financial_years_active');
        $role->givePermissionTo('erp_financial_years_inactive');
        $role->givePermissionTo('erp_financial_years_destroy');
        $role->givePermissionTo('erp_financial_years_detail');

        $accountant = Role::whereName("account")->first();
        $accountant->givePermissionTo('erp_financial_years_manage');
        $accountant->givePermissionTo('erp_financial_years_create');
        $accountant->givePermissionTo('erp_financial_years_edit');
        $accountant->givePermissionTo('erp_financial_years_active');
        $accountant->givePermissionTo('erp_financial_years_inactive');
        $accountant->givePermissionTo('erp_financial_years_destroy');
        $accountant->givePermissionTo('erp_financial_years_detail');

        $gm = Role::whereName("GM Finance")->first();
        $gm->givePermissionTo('erp_financial_years_manage');
        $gm->givePermissionTo('erp_financial_years_create');
        $gm->givePermissionTo('erp_financial_years_edit');
        $gm->givePermissionTo('erp_financial_years_active');
        $gm->givePermissionTo('erp_financial_years_inactive');
        $gm->givePermissionTo('erp_financial_years_destroy');
        $gm->givePermissionTo('erp_financial_years_detail');

        \Carbon\Carbon::now();

        $now = \Carbon\Carbon::now();;
        $lastyear = \Carbon\Carbon::parse('last year');

        // Insert Default Financial Years
        FinancialYears::insert([
            [
                'id' => 1,
                'name' => $lastyear->year,
                'start_date' => $lastyear->startOfYear()->format('Y-m-d'),
                'end_date' => $lastyear->endOfYear()->format('Y-m-d'),
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 2,
                'name' => $now->year,
                'start_date' => $now->startOfYear()->format('Y-m-d'),
                'end_date' => $now->endOfYear()->format('Y-m-d'),
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ]
        ]);
    }
}
