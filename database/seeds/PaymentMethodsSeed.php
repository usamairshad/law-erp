<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\PaymentMethods;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PaymentMethodsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Payment Methods',
            'name' => 'erp_payment_methods_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_payment_methods_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_payment_methods_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_payment_methods_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_payment_methods_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_payment_methods_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_payment_methods_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ]
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_payment_methods_manage');
        $role->givePermissionTo('erp_payment_methods_create');
        $role->givePermissionTo('erp_payment_methods_edit');
        $role->givePermissionTo('erp_payment_methods_active');
        $role->givePermissionTo('erp_payment_methods_inactive');
        $role->givePermissionTo('erp_payment_methods_destroy');
        $role->givePermissionTo('erp_payment_methods_detail');

        $accountant = Role::whereName("account")->first();
        $accountant->givePermissionTo('erp_payment_methods_manage');
        $accountant->givePermissionTo('erp_payment_methods_create');
        $accountant->givePermissionTo('erp_payment_methods_edit');
        $accountant->givePermissionTo('erp_payment_methods_active');
        $accountant->givePermissionTo('erp_payment_methods_inactive');
        $accountant->givePermissionTo('erp_payment_methods_destroy');
        $accountant->givePermissionTo('erp_payment_methods_detail');

        $gm = Role::whereName("GM Finance")->first();
        $gm->givePermissionTo('erp_payment_methods_manage');
        $gm->givePermissionTo('erp_payment_methods_create');
        $gm->givePermissionTo('erp_payment_methods_edit');
        $gm->givePermissionTo('erp_payment_methods_active');
        $gm->givePermissionTo('erp_payment_methods_inactive');
        $gm->givePermissionTo('erp_payment_methods_destroy');
        $gm->givePermissionTo('erp_payment_methods_detail');


        PaymentMethods::insert([
            [
                'name' => 'Cash',
                'payment_type' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name' => 'Check',
                'payment_type' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name' => 'American Express',
                'payment_type' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name' => 'Discover',
                'payment_type' => 4,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name' => 'MasterCard',
                'payment_type' => 5,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name' => 'Visa',
                'payment_type' => 6,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name' => 'Debit Card',
                'payment_type' => 9,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name' => 'Gift Card',
                'payment_type' => 10,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'name' => 'E-Check',
                'payment_type' => 11,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
        ]);
    }
}
