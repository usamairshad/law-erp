<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Admin\Ledgers;
use Illuminate\Support\Facades\Config;
class CeoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_ceo = Role::create(
            ['id'=>18,'name' => 'Ceo']
        );

        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Dashboad',
            'name' => 'erp_ceo_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Index',
                'name' => 'erp_ceo_dashboard',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ]
        ]);
        $role_ceo->givePermissionTo('erp_ceo_dashboard');
        $role_ceo->givePermissionTo('erp_roles_manage');
        $role_ceo->givePermissionTo('erp_permissions_manage');
        $role_ceo->givePermissionTo('erp_document_manage');
        $role_ceo->givePermissionTo('erp_document_create');
        $role_ceo->givePermissionTo('erp_document_edit');
        $role_ceo->givePermissionTo('erp_document_active');
        $role_ceo->givePermissionTo('erp_document_inactive');
        $role_ceo->givePermissionTo('erp_document_destroy');
    
    }
}
