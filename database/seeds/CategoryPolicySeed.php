<?php

use App\Models\RulesAndRegulations\CategoryPolicy;
use Illuminate\Database\Seeder;
class CategoryPolicySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryPolicy::insert([
            [
                'categorypolicy_name' => 'categorypolicy name',
                'categorypolicy_description' => 'This is categorypolicy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
            [
                'categorypolicy_name' => 'categorypolicy name',
                'categorypolicy_description' => 'This is categorypolicy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
            [
                'categorypolicy_name' => 'categorypolicy name',
                'categorypolicy_description' => 'This is categorypolicy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
            ]);
    }
}
