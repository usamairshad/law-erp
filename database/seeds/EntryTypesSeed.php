<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\EntryTypes;

class EntryTypesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EntryTypes::insert([
            [
                'id' => 1,
                'name' => 'Journal Voucher',
                'code' => 'GJV',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Cash Receipt Voucher',
                'code' => 'CRV',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Cash Payment Voucher',
                'code' => 'CPV',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 4,
                'name' => 'Bank Receipt Voucher',
                'code' => 'BRV',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 5,
                'name' => 'Bank Payment Voucher',
                'code' => 'BPV',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 6,
                'name' => 'Fee Voucher',
                'code' => 'FV',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],

        ]);
    }
}
