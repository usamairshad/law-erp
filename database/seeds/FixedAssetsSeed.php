<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class FixedAssetsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'FixedAssets',
            'name' => 'erp_fixedasset_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_fixedasset_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_fixedasset_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_fixedasset_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Bulk Delete',
                'name' => 'erp_fixedasset_mass_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_fixedasset_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ]
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_fixedasset_manage');
        $role->givePermissionTo('erp_fixedasset_create');
        $role->givePermissionTo('erp_fixedasset_edit');
        $role->givePermissionTo('erp_fixedasset_destroy');
        $role->givePermissionTo('erp_fixedasset_mass_destroy');
        $role->givePermissionTo('erp_fixedasset_detail');

        $accountant = Role::whereName("account")->first();
        $accountant->givePermissionTo('erp_fixedasset_manage');
        $accountant->givePermissionTo('erp_fixedasset_create');
        $accountant->givePermissionTo('erp_fixedasset_edit');
        $accountant->givePermissionTo('erp_fixedasset_destroy');
        $accountant->givePermissionTo('erp_fixedasset_mass_destroy');
        $accountant->givePermissionTo('erp_fixedasset_detail');

        $gm = Role::whereName("GM Finance")->first();
        $gm->givePermissionTo('erp_fixedasset_manage');
        $gm->givePermissionTo('erp_fixedasset_create');
        $gm->givePermissionTo('erp_fixedasset_edit');
        $gm->givePermissionTo('erp_fixedasset_destroy');
        $gm->givePermissionTo('erp_fixedasset_mass_destroy');
        $gm->givePermissionTo('erp_fixedasset_detail');

    }
}
