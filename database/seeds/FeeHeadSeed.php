<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Models\Admin\ErpFeeHead;

class FeeHeadSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Fee Head',
            'name' => 'erp_fee_head_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_fee_head_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_fee_head_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_fee_head_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_fee_head_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_fee_head_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_fee_head_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_fee_head_manage');
        $role->givePermissionTo('erp_fee_head_create');
        $role->givePermissionTo('erp_fee_head_edit');
        $role->givePermissionTo('erp_fee_head_destroy');
        $role->givePermissionTo('erp_fee_head_detail');
        $role->givePermissionTo('erp_fee_head_active');
        $role->givePermissionTo('erp_fee_head_inactive');

        $accountant = Role::whereName("account")->first();
        $accountant->givePermissionTo('erp_fee_head_manage');
        $accountant->givePermissionTo('erp_fee_head_create');
        $accountant->givePermissionTo('erp_fee_head_edit');
        $accountant->givePermissionTo('erp_fee_head_destroy');
        $accountant->givePermissionTo('erp_fee_head_detail');
        $accountant->givePermissionTo('erp_fee_head_active');
        $accountant->givePermissionTo('erp_fee_head_inactive');

        $gm = Role::whereName("GM Finance")->first();
        $gm->givePermissionTo('erp_fee_head_manage');
        $gm->givePermissionTo('erp_fee_head_create');
        $gm->givePermissionTo('erp_fee_head_edit');
        $gm->givePermissionTo('erp_fee_head_destroy');
        $gm->givePermissionTo('erp_fee_head_detail');
        $gm->givePermissionTo('erp_fee_head_active');
        $gm->givePermissionTo('erp_fee_head_inactive');

        ErpFeeHead::insert([
            [
                'status'=>'1',
                'group_id'=>'124',
                'title' => 'Registration Fee',
                'fee_section_id'=> 1,
                'description'=> '(only at the time of admission)',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'status'=>'17',
                'group_id'=>'1',
                'title' => 'Admission Fee',
                'fee_section_id'=> 2,
                'description'=> '(only at the time of admission)',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'status'=>'1',
                'group_id'=>'12',
                'title' => 'Security Fee',
                'fee_section_id'=> 3,
                'description'=> '(only at the time of admission)',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ]);
    }
}
