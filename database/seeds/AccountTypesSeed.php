<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use \App\Models\Admin\AccountTypes;
use App\User;

class AccountTypesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $accountant = Role::create(
            ['id'=>4,'name' => 'account']
        );
        $gm = Role::create(
            ['id'=>16,'name' => 'GM Finance']
        );
        $tax_manager = Role::create(
            ['id'=>19,'name' => 'Tax Manager']
        );
        $deputy_manager = Role::create(
            ['id'=>20,'name' => 'Deputy Manager']
        );
        $assistant_accounts = Role::create(
            ['id'=>21,'name' => 'Assistant Accounts']
        );
        //  $accountant = User::create([
        //     'name' => 'Accountant',
        //     'email' => 'accountant@rootsivy.edu.pk',
        //     'password' => bcrypt('12345678'),
        //     'active' => 1
        // ]);
        // $accountant->assignRole('account');
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Account Types',
            'name' => 'erp_account_types_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_account_types_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Salary Management',
                'name' => 'erp_salary_management',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_account_types_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_account_types_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_account_types_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_account_types_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_account_types_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_account_types_manage');
        $role->givePermissionTo('erp_account_types_create');
        $role->givePermissionTo('erp_account_types_edit');
        $role->givePermissionTo('erp_account_types_active');
        $role->givePermissionTo('erp_account_types_inactive');
        $role->givePermissionTo('erp_account_types_destroy');
        $role->givePermissionTo('erp_account_types_detail');
        $role->givePermissionTo('erp_salary_management');
        $role->givePermissionTo('erp_laws_manage'); 
        $role->givePermissionTo('erp_laws_create'); 
        $role->givePermissionTo('erp_laws_edit'); 
        $role->givePermissionTo('erp_laws_destroy'); 

        $accountant = Role::whereName("account")->first(); 
        $accountant->givePermissionTo('erp_account_types_manage');
        $accountant->givePermissionTo('erp_account_types_create');
        $accountant->givePermissionTo('erp_account_types_edit');
        $accountant->givePermissionTo('erp_account_types_active');
        $accountant->givePermissionTo('erp_account_types_inactive');
        $accountant->givePermissionTo('erp_account_types_destroy');
        $accountant->givePermissionTo('erp_account_types_detail');
        $accountant->givePermissionTo('erp_salary_management');

        $gm = Role::whereName("GM Finance")->first(); 
        $gm->givePermissionTo('erp_account_types_manage');
        $gm->givePermissionTo('erp_account_types_create');
        $gm->givePermissionTo('erp_account_types_edit');
        $gm->givePermissionTo('erp_account_types_active');
        $gm->givePermissionTo('erp_account_types_inactive');
        $gm->givePermissionTo('erp_account_types_destroy');
        $gm->givePermissionTo('erp_account_types_detail');
        $gm->givePermissionTo('erp_salary_management'); 
        $gm->givePermissionTo('erp_laws_manage'); 
        $gm->givePermissionTo('erp_laws_create'); 
        $gm->givePermissionTo('erp_laws_edit'); 
        $gm->givePermissionTo('erp_laws_destroy'); 


        $tax_manager->givePermissionTo('erp_salary_management');
        $deputy_manager->givePermissionTo('erp_salary_management');
        $assistant_accounts->givePermissionTo('erp_salary_management');

        // Insert Default AccountTypes
        AccountTypes::insert([
            [
                'id' => 1,
                'name' => 'Assets',
                'code' => 'A',
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Equity',
                'code' => 'P',
                'status' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Expenses',
                'code' => 'E',
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 4,
                'name' => 'Income',
                'code' => 'I',
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
            [
                'id' => 5,
                'name' => 'Liabilities',
                'code' => 'L',
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1,
            ],
        ]);
    }
}
