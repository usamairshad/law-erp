<?php

use App\Roles;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name'=>'super-admin',
                'display_name'=>'Super Admin',
                'description'=>'Super Admin'
            ],
            [
                'name'=>'admin',
                'display_name'=>'Administrator',
                'description'=>'Administrator'
            ],
            [
                'name'=>'admission manager',
                'display_name'=>'Admission Manager',
                'description'=>'Admission Manager'
            ],
            [
                'name'=>'account',
                'display_name'=>'Accountant',
                'description'=>'Accountant'
            ],
            [
                'name'=>'library',
                'display_name'=>'Librarian',
                'description'=>'Librarian'
            ],
            [
                'name'=>'sr.coordinator',
                'display_name'=>'Sr.Coordinator',
                'description'=>'Sr.Coordinator'
            ],
            [
                'name'=>'jr.coordinator',
                'display_name'=>'Jr.Coordinator',
                'description'=>'Jr.Coordinator'
            ],
            [
                'name'=>'student',
                'display_name'=>'Student',
                'description'=>'Student'
            ],
            [
                'name'=>'guardian',
                'display_name'=>'guardian',
                'description'=>'Guardian'
            ],
            [
                'name'=>'teacher',
                'display_name'=>'Teacher',
                'description'=>'Teacher'
            ],
            [
                'name'=>'Principal',
                'display_name'=>'Principal',
                'description'=>'Principal'
            ],
            [
                'name'=>'fee manager',
                'display_name'=>'Fee Manager',
                'description'=>'Fee Manager'
            ],
            [
                'name'=>'quality assurance',
                'display_name'=>'Quality Assurance',
                'description'=>'Quality Assurance'
            ],

        ]);
    }
}
