<?php

use App\Models\HRM\EmployeeAttendance;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class PayrollSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Payroll',
            'name' => 'erp_payroll_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_payroll_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_payroll_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Activate',
                'name' => 'erp_payroll_active',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Details',
                'name' => 'erp_payroll_details',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Inactivate',
                'name' => 'erp_payroll_inactive',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_payroll_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Datatables',
                'name' => 'erp_payroll_datatables',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_payroll_delete',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ]
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_payroll_manage');
        $role->givePermissionTo('erp_payroll_create');
        $role->givePermissionTo('erp_payroll_edit');
        $role->givePermissionTo('erp_payroll_active');
        $role->givePermissionTo('erp_payroll_details');
        $role->givePermissionTo('erp_payroll_inactive');
        $role->givePermissionTo('erp_payroll_destroy');
        $role->givePermissionTo('erp_payroll_datatables');
        $role->givePermissionTo('erp_payroll_delete');

        $accountant = Role::whereName("account")->first();
        $accountant->givePermissionTo('erp_payroll_manage');
        $accountant->givePermissionTo('erp_payroll_create');
        $accountant->givePermissionTo('erp_payroll_edit');
        $accountant->givePermissionTo('erp_payroll_active');
        $accountant->givePermissionTo('erp_payroll_details');
        $accountant->givePermissionTo('erp_payroll_inactive');
        $accountant->givePermissionTo('erp_payroll_destroy');
        $accountant->givePermissionTo('erp_payroll_datatables');
        $accountant->givePermissionTo('erp_payroll_delete');

        $gm = Role::whereName("GM Finance")->first();
        // $gm->givePermissionTo('erp_payroll_manage');
        $gm->givePermissionTo('erp_payroll_create');
        $gm->givePermissionTo('erp_payroll_edit');
        $gm->givePermissionTo('erp_payroll_active');
        $gm->givePermissionTo('erp_payroll_details');
        $gm->givePermissionTo('erp_payroll_inactive');
        $gm->givePermissionTo('erp_payroll_destroy');
        $gm->givePermissionTo('erp_payroll_datatables');
        $gm->givePermissionTo('erp_payroll_delete');

       /* if(env('APP_ENV') == 'local') {
            factory(Payroll::class, 20)->create();
        } */
    }
}
