<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class GroupEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $dfit_head = Role::whereName("DFIT_head")->first();
        //$hr_head_role = Role::whereName("HR_head")->first();

        $parent_permission = Permission::create([
            "name" => "erp_group_event_manage",
            "guard_name" => "web",
            "title" => "Group Event",
            "main_group" => 1,
            "parent_id" => 0,
            "status" => 1,
        ]);

        // $dfit_head->givePermissionTo($parent_permission->id);
        //$hr_head_role->givePermissionTo($parent_permission->id);

        $permissions = [
            ["name" =>  "erp_group_event_create", "title" => "Create"],
            ["name" => "erp_group_event_edit", "title" => "Edit"],
            ["name" => "erp_group_event_destroy", "title" => "Delete"],
            ["name" => "erp_group_event_activate", "title" => "Activate"],
            ["name" => "erp_group_event_Inactivate", "title" => "Inactivate"]
        ];
        foreach ($permissions as $permission) {

            $child_permission = Permission::create([
                "name" => $permission["name"],
                "guard_name" => "web",
                "title" => $permission["title"],
                "main_group" => 0,
                "parent_id" => $parent_permission->id,
                "status" => 1,
            ]);
            // $dfit_head->givePermissionTo($child_permission->id);
            //$hr_head_role->givePermissionTo($child_permission->id);
        }
    }
}
