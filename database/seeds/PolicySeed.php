<?php

use App\Models\RulesAndRegulations\Policy;
use Illuminate\Database\Seeder;

class PolicySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Policy::insert([
            [
                'category_policy_id' => 1,
                'policy_name' => 'This is policy name',
                'policy_description' => 'This is dummy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
            [
                'category_policy_id' => 2,
                'policy_name' => 'This is policy name',
                'policy_description' => 'This is dummy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
            [
                'category_policy_id' => 3,
                'policy_name' => 'This is policy name',
                'policy_description' => 'This is dummy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
        ]);
    }
}
