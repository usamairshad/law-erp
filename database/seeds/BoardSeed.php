<?php

use Illuminate\Database\Seeder;
use App\Models\Academics\Board;
class BoardSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Board::insert([
            [
                'name' => 'INTERNATIONAL BACCALAUREATE',
                'status' => 'pending',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
          [
              'name' => 'NON INTERNATIONAL BACCALAUREATE',
              'status' => 'pending',
              'created_at' => \Carbon\Carbon::now(),
              'updated_at' => \Carbon\Carbon::now(),
          ],
          [
              'name' => 'UNIVERSITY',
              'status' => 'pending',
              'created_at' => \Carbon\Carbon::now(),
              'updated_at' => \Carbon\Carbon::now(),
          ],

          [
              'name' => 'IVY CAMPUS',
              'status' => 'pending',
              'created_at' => \Carbon\Carbon::now(),
              'updated_at' => \Carbon\Carbon::now(),
          ],
            ]);
    }
}
