<?php

use Illuminate\Database\Seeder;
use App\Models\Academics\Company;
use App\Models\Academics\Branch;

class CompanyBranchSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company1 = Company::create([
        	'name' => 'Roots IVY International School (pvt) Ltd',
        	'description' => 'If you cannot go to London, London can come to you',
        	'status' => 1
        ]);
        $company1Branches = [
            [
                'name' => 'Roots IVY International School - Head Office',
                'company_id' => '1',
                'branch_code' => '0',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - PWD Campus',
                'company_id' => '1',
                'branch_code' => '26',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - Westridge Campus',
                'company_id' => '1',
                'branch_code' => '11',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - Riverview Campus',
                'company_id' => '1',
                'branch_code' => '23',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - F-8 Campus A Level',
                'company_id' => '1',
                'branch_code' => '19',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - Educational Complex Faisalabad',
                'company_id' => '1',
                'branch_code' => '27',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - DHA Phase-5 Lahore',
                'company_id' => '1',
                'branch_code' => '22',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - Cantt Campus Bahawalpur',
                'company_id' => '1',
                'branch_code' => '28',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - Signature School - G6/4 Islamabad',
                'company_id' => '1',
                'branch_code' => '1',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'Roots IVY International School - DHA Bahawalpur',
                'company_id' => '1',
                'branch_code' => '2',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ];

        Branch::insert($company1Branches);

        $company2 = Company::create([
            'name' => 'IVY College of Management Science (Ptv) Ltd',
            'description' => 'If you cannot go to London, London can come to you',
            'status' => 1
        ]);
        $company2Branches = [
            [
                'name' => 'IVY CMS - Head Office',
                'company_id' => '2',
                'branch_code' => '0',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'IVY CMS - DHA Phase - II University Campus',
                'company_id' => '2',
                'branch_code' => '1',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'IVY CMS - F-8 Islamabad',
                'company_id' => '2',
                'branch_code' => '2',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'IVY CMS - University Campus Faisalabad',
                'company_id' => '2',
                'branch_code' => '3',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            
        ];

        Branch::insert($company2Branches);

        $company3 = Company::create([
            'name' => 'IVY United School (Pvt) Ltd',
            'description' => 'If you cannot go to London, London can come to you',
            'status' => 1
        ]);
        $company3Branches = [
            [
                'name' => 'IVY United School - Airport Housing Society Campus',
                'company_id' => '3',
                'branch_code' => '1',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'name' => 'IVY United School - Satellite Town Campus',
                'company_id' => '3',
                'branch_code' => '2',
                'address' => 'Roots IVY Scheme III',
                'status' => 1,
                'city_id' => 1,
                'country_id' => 1,
                'phone' => NULL,
                'fax' => NULL,
                'cell' => NULL,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            
            
        ];

        Branch::insert($company3Branches);

        // $company2 = Company::create([
        //     'name' => 'IVY College of Management',
        //     'description' => 'If you cannot go to London, London can come to you',
        //     'address' => 'Roots IVY Scheme III',
        //     'status' => 1,
        // ]);
        // $company1Branches = [
        //     [
        //         'name' => 'Roots IVY International School - Head Office',
        //         'company_id' => '1',
        //         'branch_code' => '0',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'name' => 'Roots IVY International School - PWD Campus',
        //         'company_id' => '1',
        //         'branch_code' => '26',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'name' => 'Roots IVY International School - Westridge Campus',
        //         'company_id' => '1',
        //         'branch_code' => '11',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'name' => 'Roots IVY International School - Riverview Campus',
        //         'company_id' => '1',
        //         'branch_code' => '23',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'name' => 'Roots IVY International School - F-8 Campus A Level',
        //         'company_id' => '1',
        //         'branch_code' => '19',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'name' => 'Roots IVY International School - Educational Complex Faisalabad',
        //         'company_id' => '1',
        //         'branch_code' => '27',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'name' => 'Roots IVY International School - DHA Phase-5 Lahore',
        //         'company_id' => '1',
        //         'branch_code' => '22',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'name' => 'Roots IVY International School - Cantt Campus Bahawalpur',
        //         'company_id' => '1',
        //         'branch_code' => '28',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'branch_name' => 'Roots IVY International School - Signature School - G6/4 Islamabad',
        //         'company_id' => '1',
        //         'branch_code' => '1',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
        //     [
        //         'branch_name' => 'Roots IVY International School - DHA Bahawalpur',
        //         'company_id' => '1',
        //         'branch_code' => '2',
        //         'address' => 'Roots IVY Scheme III',
        //         'status' => 1,
        //     ],
            
        // ];

        // Branch::insert($company1Branches);

        // $company2 = Company::create([
        // 	'name' => 'IVY College of Management Science (pvt) Ltd',
        // 	'description' => 'Study Abroad',
        // 	'status' => 1,
        // ]);
        // $branch1 = Branch::create([
        // 	'name' => 'Roots IVY International School-Head Office',
        // 	'company_id' => '1',
        // 	'branch_code' => '0',
        // 	'address' => 'Roots IVY Scheme III',
        // 	'status' => 1,
        // ]);
        // $branch2 = Branch::create([
        // 	'name' => 'Roots IVY International School-Westridge Campus',
        // 	'company_id' => '1',
        // 	'branch_code' => '11',
        // 	'address' => 'Roots IVY Scheme III Non IB Campus',
        // 	'status' => 1,
        // ]);
        // $branch3 = Branch::create([
        // 	'name' => 'Roots IVY International ',
        // 	'company_id' => '2',
        // 	'branch_code' => '2',
        // 	'address' => 'F8 Islamabad',
        // 	'status' => 1,
        // ]);
        // $branch3 = Branch::create([
        //     'name' => '',
        //     'company_id' => '2',
        //     'branch_code' => '2',
        //     'address' => 'F8 Islamabad',
        //     'status' => 1,
        // ]);
        // $branch3 = Branch::create([
        //     'name' => 'Roots IVY International School-F-8 Campus A Level',
        //     'company_id' => '2',
        //     'branch_code' => '2',
        //     'address' => 'F8 Islamabad',
        //     'status' => 1,
        // ]);
    }
}
