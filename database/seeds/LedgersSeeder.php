<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Admin\Ledgers;
use Illuminate\Support\Facades\Config;

class LedgersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Ledgers',
            'name' => 'erp_ledgers_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_ledgers_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_ledgers_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Delete',
                'name' => 'erp_ledgers_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Bulk Delete',
                'name' => 'erp_ledgers_mass_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ]
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_ledgers_manage');
        $role->givePermissionTo('erp_ledgers_create');
        $role->givePermissionTo('erp_ledgers_edit');
        $role->givePermissionTo('erp_ledgers_destroy');
        $role->givePermissionTo('erp_ledgers_mass_destroy');

        Ledgers::insert(Config::get('ledgers.data'));

        $accountant = Role::whereName("account")->first();
        $accountant->givePermissionTo('erp_ledgers_manage');
        $accountant->givePermissionTo('erp_ledgers_create');
        $accountant->givePermissionTo('erp_ledgers_edit');
        $accountant->givePermissionTo('erp_ledgers_destroy');
        $accountant->givePermissionTo('erp_ledgers_mass_destroy');

        $gm = Role::whereName("GM Finance")->first();
        $gm->givePermissionTo('erp_ledgers_manage');
        $gm->givePermissionTo('erp_ledgers_create');
        $gm->givePermissionTo('erp_ledgers_edit');
        $gm->givePermissionTo('erp_ledgers_destroy');
        $gm->givePermissionTo('erp_ledgers_mass_destroy');


//        $Ledgers = Ledgers::insert([
//            [
//                'name' => 'Cash In Hand',
//                'number' => '1-02-008-0001-0001',
//                'group_id' => '135',
//                'balance_type' => 'd',
//                'opening_balance' => '0',
//                'closing_balance' => '0',
//                'account_type_id' => 1,
//                'parent_type' => '',
//                'created_at' => \Carbon\Carbon::now('UTC +5'),
//            ],
//            [
//                'name' => 'Sale Tax Payable',
//                'number' => '2-03-001-0007',
//                'group_id' => '742',
//                'balance_type' => 'c',
//                'opening_balance' => '0',
//                'closing_balance' => '0',
//                'account_type_id' => 5,
//                'parent_type' => '',
//                'created_at' => \Carbon\Carbon::now('UTC +5'),
//            ],
//            [
//                'name' => 'Sales Discount',
//                'number' => '2-03-001-0007',
//                'group_id' => '742',
//                'balance_type' => 'c',
//                'opening_balance' => '0',
//                'closing_balance' => '0',
//                'account_type_id' => 5,
//                'parent_type' => '',
//                'created_at' => \Carbon\Carbon::now('UTC +5'),
//            ],
//        ]);
    }
}
