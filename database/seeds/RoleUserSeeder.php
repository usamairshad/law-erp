<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_Admission_Manager = Role::create(
            ['id'=>3,'name' => 'admission manager']
        );

        $role_Librarian = Role::create(
            ['id'=>5,'name' => 'library']
        );

        $role_Sr_Coordinator = Role::create(
            ['id'=>6,'name' => 'sr.coordinator']
        );

        $role_HR = Role::create(
            ['id'=>9,'name' => 'HR']
        );
        $role_Teacher = Role::create(
            ['id'=>10,'name' => 'teacher']
        );

        $role_Principal = Role::create(
            ['id'=>11,'name' => 'Principal']
        );

        $role_Fee_Manager = Role::create(
            ['id'=>12,'name' => 'fee manager']
        );

        $role_Quality_Assurance = Role::create(
            ['id'=>13,'name' => 'quality assurance']
        );

        $role_Candidate = Role::create(
            ['id'=>15,'name' => 'Candidate']
        );

        $role_Staff = Role::create(
            ['id'=>17,'name' => 'Supporting Staff']
        );


        //Permissoin for Admission Manager
        $hr_role = Role::whereName("HR")->first();
        $hr_head_role = Role::whereName("HR_head")->first();

        $parent_permission = Permission::create([
            "name" => "erp_resign_manage",
            "guard_name" => "web",
            "title" => "Resign",
            "main_group" => 1,
            "parent_id" => 0,
            "status" => 1,
        ]);

        $hr_role->givePermissionTo($parent_permission->id);
        $hr_head_role->givePermissionTo($parent_permission->id);

        $permissions = [

            ["name" => "erp_resign_destroy", "title" => "Delete"],
            ["name" => "erp_resign_activate", "title" => "Activate"],
            ["name" => "erp_resign_Inactivate", "title" => "Inactivate"]
        ];

        foreach ($permissions as $permission) {

            $child_permission = Permission::create([
                "name" => $permission["name"],
                "guard_name" => "web",
                "title" => $permission["title"],
                "main_group" => 0,
                "parent_id" => $parent_permission->id,
                "status" => 1,
            ]);
            $hr_role->givePermissionTo($child_permission->id);
            $hr_head_role->givePermissionTo($child_permission->id);
        }

        $role_Admission_Manager->givePermissionTo('erp_user_staff_profile');
        $role_Admission_Manager->givePermissionTo('erp_resign_manage');
        $role_Admission_Manager->givePermissionTo('erp_leave_apply_index');
        $role_Admission_Manager->givePermissionTo('erp_id_card_request_manage');
        $role_Admission_Manager->givePermissionTo('erp_agenda_manage');
        $role_Admission_Manager->givePermissionTo('erp_teacher_attendance_index');
        $role_Admission_Manager->givePermissionTo('erp_document_manage');
        $role_Admission_Manager->givePermissionTo('erp_document_create');
        $role_Admission_Manager->givePermissionTo('erp_document_edit');
        $role_Admission_Manager->givePermissionTo('erp_document_active');
        $role_Admission_Manager->givePermissionTo('erp_document_inactive');
        $role_Admission_Manager->givePermissionTo('erp_document_destroy');

        //Permissoin for role_Librarian
        
        $role_Librarian->givePermissionTo('erp_user_staff_profile');
        $role_Librarian->givePermissionTo('erp_resign_manage');
        $role_Librarian->givePermissionTo('erp_leave_apply_index');
        $role_Librarian->givePermissionTo('erp_id_card_request_manage');
        $role_Librarian->givePermissionTo('erp_agenda_manage');
        $role_Librarian->givePermissionTo('erp_teacher_attendance_index');
        // $role_Librarian->givePermissionTo('erp_document_manage');
        $role_Librarian->givePermissionTo('erp_document_manage');
        $role_Librarian->givePermissionTo('erp_document_create');
        $role_Librarian->givePermissionTo('erp_document_edit');
        $role_Librarian->givePermissionTo('erp_document_active');
        $role_Librarian->givePermissionTo('erp_document_inactive');
        $role_Librarian->givePermissionTo('erp_document_destroy');

        //Permissoin for Sr-Coordinator
        
        $role_Sr_Coordinator->givePermissionTo('erp_user_staff_profile');
        $role_Sr_Coordinator->givePermissionTo('erp_resign_manage');
        $role_Sr_Coordinator->givePermissionTo('erp_leave_apply_index');
        $role_Sr_Coordinator->givePermissionTo('erp_id_card_request_manage');
        $role_Sr_Coordinator->givePermissionTo('erp_agenda_manage');
        $role_Sr_Coordinator->givePermissionTo('erp_teacher_attendance_index');
        $role_Sr_Coordinator->givePermissionTo('erp_time_table');
        $role_Sr_Coordinator->givePermissionTo('erp_time_table_category_manage');
        $role_Sr_Coordinator->givePermissionTo('erp_timetable_manage');
        $role_Sr_Coordinator->givePermissionTo('erp_class_time_table_manage');
        $role_Sr_Coordinator->givePermissionTo('erp_group_report_manage');
        $role_Sr_Coordinator->givePermissionTo('erp_group_report_create');
        $role_Sr_Coordinator->givePermissionTo('erp_group_report_edit');
        $role_Sr_Coordinator->givePermissionTo('erp_group_report_destroy');
        $role_Sr_Coordinator->givePermissionTo('erp_group_report_detail');
        $role_Sr_Coordinator->givePermissionTo('erp_group_report_active');
        $role_Sr_Coordinator->givePermissionTo('erp_group_report_inactive');
        $role_Sr_Coordinator->givePermissionTo('erp_document_manage');
        $role_Sr_Coordinator->givePermissionTo('erp_document_create');
        $role_Sr_Coordinator->givePermissionTo('erp_document_edit');
        $role_Sr_Coordinator->givePermissionTo('erp_document_active');
        $role_Sr_Coordinator->givePermissionTo('erp_document_inactive');
        $role_Sr_Coordinator->givePermissionTo('erp_document_destroy');

        
        //Permissoin for teacher
        
        $role_Teacher->givePermissionTo('erp_user_staff_profile');
        $role_Teacher->givePermissionTo('erp_resign_manage');
        $role_Teacher->givePermissionTo('erp_leave_apply_index');
        $role_Teacher->givePermissionTo('erp_id_card_request_manage');
        $role_Teacher->givePermissionTo('erp_agenda_manage');
        $role_Teacher->givePermissionTo('erp_teacher_attendance_index');
        $role_Teacher->givePermissionTo('erp_time_table');
        $role_Teacher->givePermissionTo('erp_time_table_category_manage');
        $role_Teacher->givePermissionTo('erp_timetable_manage');
        $role_Teacher->givePermissionTo('erp_class_time_table_manage');
        $role_Teacher->givePermissionTo('erp_document_manage');
        $role_Teacher->givePermissionTo('erp_document_create');
        $role_Teacher->givePermissionTo('erp_document_edit');
        $role_Teacher->givePermissionTo('erp_document_active');
        $role_Teacher->givePermissionTo('erp_document_inactive');
        $role_Teacher->givePermissionTo('erp_document_destroy');


        //Permissoin for Principal
        
        $role_Principal->givePermissionTo('erp_user_staff_profile');
        $role_Principal->givePermissionTo('erp_resign_manage');
        $role_Principal->givePermissionTo('erp_leave_apply_index');
        $role_Principal->givePermissionTo('erp_id_card_request_manage');
        $role_Principal->givePermissionTo('erp_agenda_manage');
        $role_Principal->givePermissionTo('erp_teacher_attendance_index');
        $role_Principal->givePermissionTo('erp_time_table');
        $role_Principal->givePermissionTo('erp_time_table_category_manage');
        $role_Principal->givePermissionTo('erp_timetable_manage');
        $role_Principal->givePermissionTo('erp_class_time_table_manage');
        $role_Principal->givePermissionTo('erp_document_manage');
        $role_Principal->givePermissionTo('erp_document_create');
        $role_Principal->givePermissionTo('erp_document_edit');
        $role_Principal->givePermissionTo('erp_document_active');
        $role_Principal->givePermissionTo('erp_document_inactive');
        $role_Principal->givePermissionTo('erp_document_destroy');

        //Permissoin for role_Fee_Manager
        
        $role_Fee_Manager->givePermissionTo('erp_user_staff_profile');
        $role_Fee_Manager->givePermissionTo('erp_resign_manage');
        $role_Fee_Manager->givePermissionTo('erp_leave_apply_index');
        $role_Fee_Manager->givePermissionTo('erp_id_card_request_manage');
        $role_Fee_Manager->givePermissionTo('erp_agenda_manage');
        $role_Fee_Manager->givePermissionTo('erp_teacher_attendance_index');
        $role_Fee_Manager->givePermissionTo('erp_document_manage');
        $role_Fee_Manager->givePermissionTo('erp_document_create');
        $role_Fee_Manager->givePermissionTo('erp_document_edit');
        $role_Fee_Manager->givePermissionTo('erp_document_active');
        $role_Fee_Manager->givePermissionTo('erp_document_inactive');
        $role_Fee_Manager->givePermissionTo('erp_document_destroy');


        //Permissoin for role_Quality_Assurance
        
        $role_Quality_Assurance->givePermissionTo('erp_user_staff_profile');
        $role_Quality_Assurance->givePermissionTo('erp_resign_manage');
        $role_Quality_Assurance->givePermissionTo('erp_leave_apply_index');
        $role_Quality_Assurance->givePermissionTo('erp_id_card_request_manage');
        $role_Quality_Assurance->givePermissionTo('erp_agenda_manage');
        $role_Quality_Assurance->givePermissionTo('erp_teacher_attendance_index');
        $role_Quality_Assurance->givePermissionTo('erp_document_manage');
        $role_Quality_Assurance->givePermissionTo('erp_time_table');
        $role_Quality_Assurance->givePermissionTo('erp_time_table_category_manage');
        $role_Quality_Assurance->givePermissionTo('erp_timetable_manage');
        $role_Quality_Assurance->givePermissionTo('erp_class_time_table_manage');
        $role_Quality_Assurance->givePermissionTo('erp_document_create');
        $role_Quality_Assurance->givePermissionTo('erp_document_edit');
        $role_Quality_Assurance->givePermissionTo('erp_document_active');
        $role_Quality_Assurance->givePermissionTo('erp_document_inactive');
        $role_Quality_Assurance->givePermissionTo('erp_document_destroy');



        // Assign Permission to 'HR' role
        $role_HR->givePermissionTo('erp_hr_manage');
        $role_HR->givePermissionTo('erp_recruitment_manage');
        $role_HR->givePermissionTo('erp_job_post_request_create');
        $role_HR->givePermissionTo('erp_job_post_request_edit');

        
        $role_HR->givePermissionTo('erp_users_manage');
        $role_HR->givePermissionTo('erp_users_create');
        $role_HR->givePermissionTo('erp_users_edit');
        $role_HR->givePermissionTo('erp_users_active');
        $role_HR->givePermissionTo('erp_users_inactive');
        $role_HR->givePermissionTo('erp_users_destroy');
        $role_HR->givePermissionTo('erp_users_detail');


        // Assign Permission to 'HR head' role
        
        $hr_head = Role::whereName("HR_head")->first();
        $hr_head->givePermissionTo('erp_hr_manage');
        $hr_head->givePermissionTo('erp_users_manage');
        $hr_head->givePermissionTo('erp_users_create');
        $hr_head->givePermissionTo('erp_users_edit');
        $hr_head->givePermissionTo('erp_users_active');
        $hr_head->givePermissionTo('erp_users_inactive');   
        $hr_head->givePermissionTo('erp_users_destroy');
        $hr_head->givePermissionTo('erp_users_detail');     
        $hr_head->givePermissionTo('erp_hired_candidate');
        $hr_head->givePermissionTo('erp_employees_manage');
        $hr_head->givePermissionTo('erp_employees_create');
        $hr_head->givePermissionTo('erp_employees_edit');
        $hr_head->givePermissionTo('erp_employees_active');
        $hr_head->givePermissionTo('erp_employees_inactive');
        $hr_head->givePermissionTo('erp_employees_destroy');
        $hr_head->givePermissionTo('erp_employees_mass_destroy');
        $hr_head->givePermissionTo('erp_hire_hr');
        $hr_head->givePermissionTo('erp_id_card_request_manage');
        $hr_head->givePermissionTo('erp_agenda_manage');
        $hr_head->givePermissionTo('erp_employee_management_new');
        $hr_head->givePermissionTo('erp_exit_management');
        // $hr_head->givePermissionTo('erp_asset_manage');
        $hr_head->givePermissionTo('erp_rules_and_regulations_manage');
        $hr_head->givePermissionTo('erp_payroll_manage');
        $hr_head->givePermissionTo('erp_payroll_create');
        $hr_head->givePermissionTo('erp_payroll_edit');
        $hr_head->givePermissionTo('erp_payroll_active');
        $hr_head->givePermissionTo('erp_payroll_details');
        $hr_head->givePermissionTo('erp_payroll_inactive');
        $hr_head->givePermissionTo('erp_payroll_destroy');
        $hr_head->givePermissionTo('erp_payroll_datatables');
        $hr_head->givePermissionTo('erp_payroll_delete');

        $hr_head->givePermissionTo('erp_branchhr_manage');
        $hr_head->givePermissionTo('erp_branchhr_create');
        $hr_head->givePermissionTo('erp_branchhr_edit');
        $hr_head->givePermissionTo('erp_branchhr_destroy');
        $hr_head->givePermissionTo('erp_branchhr_detail');
        $hr_head->givePermissionTo('erp_branchhr_active');
        $hr_head->givePermissionTo('erp_branchhr_inactive');
        $hr_head->givePermissionTo('erp_leave_management');

        // Assign Permission to 'HR' role
        $role_HR->givePermissionTo('erp_recruitment_manage');
        $role_HR->givePermissionTo('erp_job_post_request_create');
        $role_HR->givePermissionTo('erp_job_post_request_edit');
        $role_HR->givePermissionTo('erp_users_manage');
        $role_HR->givePermissionTo('erp_users_create');
        $role_HR->givePermissionTo('erp_users_edit');
        $role_HR->givePermissionTo('erp_users_active');
        $role_HR->givePermissionTo('erp_users_inactive');
        $role_HR->givePermissionTo('erp_users_destroy');
        $role_HR->givePermissionTo('erp_users_detail');
        $role_HR->givePermissionTo('erp_document_create');
        $role_HR->givePermissionTo('erp_document_edit');
        $role_HR->givePermissionTo('erp_document_active');
        $role_HR->givePermissionTo('erp_document_inactive');
        $role_HR->givePermissionTo('erp_document_destroy');
        // $role_HR->givePermissionTo('erp_payroll_manage');
        // $role_HR->givePermissionTo('erp_payroll_create');
        // $role_HR->givePermissionTo('erp_payroll_edit');
        // $role_HR->givePermissionTo('erp_payroll_active');
        // $role_HR->givePermissionTo('erp_payroll_details');
        // $role_HR->givePermissionTo('erp_payroll_inactive');
        // $role_HR->givePermissionTo('erp_payroll_destroy');
        // $role_HR->givePermissionTo('erp_payroll_datatables');
        // $role_HR->givePermissionTo('erp_payroll_delete');

        $hr_head->givePermissionTo('erp_staff_manage');
        $hr_head->givePermissionTo('erp_staff_create');
        $hr_head->givePermissionTo('erp_staff_edit');
        $hr_head->givePermissionTo('erp_staff_active');
        $hr_head->givePermissionTo('erp_staff_inactive');
        $hr_head->givePermissionTo('erp_staff_destroy');
        $hr_head->givePermissionTo('erp_staff_mass_destroy');
        $hr_head->givePermissionTo('erp_document_manage');

        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_hr_manage');
        $role->givePermissionTo('erp_group_report_manage');
        $role->givePermissionTo('erp_group_report_create');
        $role->givePermissionTo('erp_group_report_edit');
        $role->givePermissionTo('erp_group_report_destroy');
        $role->givePermissionTo('erp_group_report_detail');
        $role->givePermissionTo('erp_group_report_active');
        $role->givePermissionTo('erp_group_report_inactive');


        
        $hr_head->givePermissionTo('erp_document_create');
        $hr_head->givePermissionTo('erp_document_edit');
        $hr_head->givePermissionTo('erp_document_active');
        $hr_head->givePermissionTo('erp_document_inactive');
        $hr_head->givePermissionTo('erp_document_destroy');
        $hr_head->givePermissionTo('erp_user_staff_profile');
        $hr_head->givePermissionTo('erp_attendance_management');

        $admin = Role::whereName("admin")->first();
        $admin->givePermissionTo('erp_asset_manage');
        $admin->givePermissionTo('erp_school_setting_manage');
        $admin->givePermissionTo('erp_land_manage');
        $admin->givePermissionTo('erp_daily_attendace_manage');

        
    }
}
