<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AccountReportsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Reports',
            'name' => 'erp_report_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Account Reports',
                'name' => 'erp_account_reports_manage',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Trial Balance',
                'name' => 'erp_account_reports_trial_balance',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Ledger Statement',
                'name' => 'erp_account_reports_ledger_statement',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Balance Sheet Report',
                'name' => 'erp_account_reports_balance_sheet',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Profit and Loss Reports',
                'name' => 'erp_account_reports_profit_loss',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Expense Summary',
                'name' => 'erp_account_reports_expense_summary',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_report_manage');
        $role->givePermissionTo('erp_account_reports_manage');
        $role->givePermissionTo('erp_account_reports_trial_balance');
        $role->givePermissionTo('erp_account_reports_ledger_statement');
        $role->givePermissionTo('erp_account_reports_balance_sheet');
        $role->givePermissionTo('erp_account_reports_profit_loss');
        $role->givePermissionTo('erp_account_reports_expense_summary');

        $accountant = Role::whereName("account")->first(); 
        $accountant->givePermissionTo('erp_report_manage');
        $accountant->givePermissionTo('erp_account_reports_manage');
        $accountant->givePermissionTo('erp_account_reports_trial_balance');
        $accountant->givePermissionTo('erp_account_reports_ledger_statement');
        $accountant->givePermissionTo('erp_account_reports_balance_sheet');
        $accountant->givePermissionTo('erp_account_reports_profit_loss');
        $accountant->givePermissionTo('erp_account_reports_expense_summary');

        $gm = Role::whereName("GM Finance")->first(); 
        $gm->givePermissionTo('erp_report_manage');
        $gm->givePermissionTo('erp_account_reports_manage');
        $gm->givePermissionTo('erp_account_reports_trial_balance');
        $gm->givePermissionTo('erp_account_reports_ledger_statement');
        $gm->givePermissionTo('erp_account_reports_balance_sheet');
        $gm->givePermissionTo('erp_account_reports_profit_loss');
        $gm->givePermissionTo('erp_account_reports_expense_summary');
    }
}
