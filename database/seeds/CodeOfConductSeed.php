<?php

use App\Models\RulesAndRegulations\CodeOfConduct;
use Illuminate\Database\Seeder;

class CodeOfConductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CodeOfConduct::insert([
            [
                'category_policy_id' => 1,
                'codeofconduct_name' => 'This is dummy description',
                'codeofconduct_description' => 'This is dummy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
            [
                'category_policy_id' => 2,
                'codeofconduct_name' => 'This is dummy description',
                'codeofconduct_description' => 'This is dummy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
            [
                'category_policy_id' => 3,
                'codeofconduct_name' => 'This is dummy description',
                'codeofconduct_description' => 'This is dummy description',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'created_by' => \Carbon\Carbon::now(),
                'updated_by' => \Carbon\Carbon::now(),
                'deleted_by' => \Carbon\Carbon::now(),
            ],
        ]);
    }
}
