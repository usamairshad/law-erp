<?php

use App\AssesmentType;
use Illuminate\Database\Seeder;

class assesmentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $names = ["quiz", "assignment", "presentation", "project", "exam"];

        foreach ($names as $name) {
            AssesmentType::create(["name" => $name]);
        }
    }
}
