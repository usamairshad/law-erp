<?php

use Illuminate\Database\Seeder;
use App\Models\Academics\BoardProgram;
class BoardProgramSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        BoardProgram::insert([
            [
                'board_id' =>1,
                'program_id' =>1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'board_id' => 2,
                'program_id' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            ]);

    }
}
