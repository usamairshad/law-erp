<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SuppliersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions has been added
        $MainPermission = Permission::create([
            'title' => 'Suppliers',
            'name' => 'erp_suppliers_manage',
            'guard_name' => 'web',
            'main_group' => 1,
            'parent_id' => 0,
        ]);
        Permission::insert([
            [
                'title' => 'Create',
                'name' => 'erp_suppliers_create',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Edit',
                'name' => 'erp_suppliers_edit',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
//            [
//                'title' => 'Activate',
//                'name' => 'erp_suppliers_active',
//                'guard_name' => 'web',
//                'main_group' => 0,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//                'parent_id' => $MainPermission->id,
//            ],
//            [
//                'title' => 'Inactivate',
//                'name' => 'erp_suppliers_inactive',
//                'guard_name' => 'web',
//                'main_group' => 0,
//                'created_at' => \Carbon\Carbon::now(),
//                'updated_at' => \Carbon\Carbon::now(),
//                'parent_id' => $MainPermission->id,
//            ],
            [
                'title' => 'Delete',
                'name' => 'erp_suppliers_destroy',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ],
            [
                'title' => 'Detail',
                'name' => 'erp_suppliers_detail',
                'guard_name' => 'web',
                'main_group' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'parent_id' => $MainPermission->id,
            ]
        ]);

        // Assign Permission to 'administrator' role
        $role = Role::findById(2, 'web');
        $role->givePermissionTo('erp_suppliers_manage');
        $role->givePermissionTo('erp_suppliers_create');
        $role->givePermissionTo('erp_suppliers_edit');

//        $role->givePermissionTo('erp_suppliers_active');
//        $role->givePermissionTo('erp_suppliers_inactive');

        $role->givePermissionTo('erp_suppliers_destroy');
        $role->givePermissionTo('erp_suppliers_detail');



    }
}
