<?php

use App\AssesmentType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeed::class);
        $this->call(PermissionSeed::class);
        $this->call(CountriesSeed::class);
        $this->call(CitiesSeed::class);
        $this->call(CompaniesSeed::class);
        //Create staff permissions
        $this->call(StaffSeed::class);
        $this->call(UserSeed::class);
        $this->call(TaxLawsSeed::class);

        $this->call(AccountTypesSeed::class);
        $this->call(PayrollSeed::class);
        //Accountant 
        $this->call(BranchesSeed::class);
        $this->call(CompanyBranchSeeders::class);



        $this->call(FinancialYearsSeed::class);

        $this->call(AccountReportsSeed::class);

        $this->call(FeeSectionSeed::class);
        $this->call(FeeHeadSeed::class);
        $this->call(CurrenciesSeed::class);

        $this->call(BanksSeed::class);
        $this->call(BanksDetailSeed::class);

        $this->call(GroupsSeed::class);
        $this->call(LedgersSeeder::class);

        // Entry Types Seeder

        // Entries Seeder
        $this->call(EntriesSeed::class);
        $this->call(EntryTypesSeed::class);
        // Entries Seeder

        $this->call(SettingsSeed::class);
        $this->call(ExitManagementSeed::class);
        $this->call(LeaveManagementSeed::class);
        $this->call(EmployeeManagementSeed::class);
        $this->call(AttendanceManagementSeed::class);

        //Recruitment and Hr role
        $this->call(HrSeed::class);
        $this->call(RecruitmentSeed::class);

        $this->call(EmployeesSeed::class);

        $this->call(IDCardRequestSeed::class);
        $this->call(AssetManagementSeed::class);
        $this->call(AgendaManagementSeed::class);
        $this->call(RuleAndRegulationsSeed::class);
        $this->call(GroupReport::class);


        //Roles and Users 
        $this->call(CeoSeed::class);

        $this->call(LandManagementSeed::class);
        // $this->call(AssetManagementSeed::class);
        $this->call(DailyAttendanceSeed::class);
        $this->call(SchoolSettingManagementSeed::class);

        $this->call(ResignSeeder::class);
        $this->call(RoleUserSeeder::class);

        //Academic Year 
        $this->call(AcadamicYearSeed::class);

        $this->call(TrainingSeeder::class);


        //Board
        $this->call(BoardSeed::class);
        //Program
        $this->call(ProgramSeed::class);
        // Board Program
        $this->call(BoardProgramSeed::class);


        // Category Policy
        $this->call(CategoryPolicySeed::class);
        // Code of Conduct
        $this->call(CodeOfConductSeed::class);

        // Policy
        $this->call(PolicySeed::class);

        //Roles and Users 
        $this->call(QAHeadSeeder::class);

        $this->call(GroupEventSeeder::class);


        $this->call(RolesSeeder::class);
        //permissions

        $this->call(PermissionsSeeder::class);

        //child_benfit_config
        $this->call(ChildBenefitConfigSeeder::class);

        //assesmentType sedder
        $this->call(assesmentTypeSeeder::class);
    }
}
