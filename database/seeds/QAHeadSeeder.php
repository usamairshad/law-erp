<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;


class QAHeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Assign Permission to 'administrator' role
        $admin = Role::findById(2, 'web');
        $admin->givePermissionTo('erp_session_manage');
        $admin->givePermissionTo('erp_session_create');
        $admin->givePermissionTo('erp_session_edit');
        $admin->givePermissionTo('erp_session_active');
        $admin->givePermissionTo('erp_session_inactive');
        $admin->givePermissionTo('erp_session_destroy');

        //Session crud permissions for QA head
        // $user_qa_head = Role::whereName("QA_head")->first(); 
        // $user_qa_head->givePermissionTo('erp_session_manage');
        // $user_qa_head->givePermissionTo('erp_session_create');
        // $user_qa_head->givePermissionTo('erp_session_edit');
        // $user_qa_head->givePermissionTo('erp_session_active');
        // $user_qa_head->givePermissionTo('erp_session_inactive');
        // $user_qa_head->givePermissionTo('erp_session_destroy');
    }
}
