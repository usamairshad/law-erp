<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\ChildBenefitConfig;

class ChildBenefitConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChildBenefitConfig::insert([

        	[
        		'child_name' => 'First Child',
        		'discount'   => 100,
        		'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
        	],
        	[
        		'child_name' => 'Second Child',
        		'discount'   => 50,
        		'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),

        	]

        ]);

    }
}
