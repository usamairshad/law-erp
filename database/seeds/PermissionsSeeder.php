<?php

use App\Models\Permissions;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'group'=>'Menu',
                'name'=>'expand-action-menu',
                'display_name'=>'Expand Nav Menu',
                'description'=>'Expand Nav Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'admin-control',
                'display_name'=>'Admin control',
                'description'=>'Admin Control Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'general',
                'display_name'=>'General',
                'description'=>'General Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'dashboard',
                'display_name'=>'Dashboard',
                'description'=>'General Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'student-staff',
                'display_name'=>'Student-Staff',
                'description'=>'Student-Staff Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'account',
                'display_name'=>'Account',
                'description'=>'Account Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'library',
                'display_name'=>'Library',
                'description'=>'Library Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'attendance',
                'display_name'=>'Attendance',
                'description'=>'Attendance Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'examination',
                'display_name'=>'Examination',
                'description'=>'Examination Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'academics specific',
                'display_name'=>'Academics Specific',
                'description'=>'Academics Specific Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'report',
                'display_name'=>'Report',
                'description'=>'Report Menu'
            ],
            [
                'group'=>'Menu',
                'name'=>'alert-center',
                'display_name'=>'Alert Center',
                'description'=>'Alert Center Menu'
            ],
            [
                'group'=>'School Settings',
                'name'=>'school-settings-index',
                'display_name'=>'School Settings',
                'description'=>'School Settings Index'
            ],
            [
                'group'=>'Role',
                'name'=>'role-index',
                'display_name'=>'Index',
                'description'=>'Role Index'
            ],
            [
                'group'=>'Role',
                'name'=>'role-add',
                'display_name'=>'Add',
                'description'=>'Role Add'
            ],
            [
                'group'=>'Role',
                'name'=>'role-view',
                'display_name'=>'View',
                'description'=>'View Role'
            ],
            [
                'group'=>'Role',
                'name'=>'role-edit',
                'display_name'=>'Edit',
                'description'=>'Edit Role'
            ],
            [
                'group'=>'Role',
                'name'=>'role-delete',
                'display_name'=>'Delete',
                'description'=>'Delete Role'
            ],
            [
                'group'=>'User',
                'name'=>'user-index',
                'display_name'=>'Index',
                'description'=>'User Index'
            ],
            [
                'group'=>'User',
                'name'=>'user-add',
                'display_name'=>'Add',
                'description'=>'User Add'
            ],
            [
                'group'=>'User',
                'name'=>'user-edit',
                'display_name'=>'Edit',
                'description'=>'Edit User'
            ],
            [
                'group'=>'User',
                'name'=>'user-delete',
                'display_name'=>'Delete',
                'description'=>'Delete User'
            ],
            [
                'group'=>'User',
                'name'=>'user-active',
                'display_name'=>'Active',
                'description'=>'Active User'
            ],
            [
                'group'=>'User',
                'name'=>'user-in-active',
                'display_name'=>'In-Active',
                'description'=>'In-Active User'
            ],
            [
                'group'=>'User',
                'name'=>'user-bulk-action',
                'display_name'=>'Bulk(Active,InActive,Delete)',
                'description'=>'User Bulk Action'
            ],
        ]);
    }
}
