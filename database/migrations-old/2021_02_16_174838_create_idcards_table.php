<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idcards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('link_type')->nullable();
            $table->integer('link_id')->nullable();
            $table->string('status')->nullable();
            $table->integer('issue_date')->nullable();
            $table->integer('expiry_date')->nullable();
            $table->integer('approve_at')->nullable();
            $table->integer('approve_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idcards');
    }
}
