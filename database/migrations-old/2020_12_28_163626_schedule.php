<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Schedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('candidate_form_id');
            $table->dateTime('scheduled_time');
            $table->unsignedInteger('branch_id');
            $table->string('required_doc')->nullable();

            $table->enum('status',['Interview-Pending','Selected','Interview-Taken','Rejected']);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('candidate_form_id')->references('id')->on('candidate_form');
             $table->foreign('branch_id')->references('id')->on('erp_branches');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
    }
}
