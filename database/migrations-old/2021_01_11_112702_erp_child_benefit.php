<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ErpChildBenefit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('erp_child_benefit', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('teacher_id');
            $table->string('percentage')->nullable();
            $table->enum('status',['Active','InActive','Pending'])->default('Active');      
            $table->timestamps();
            $table->softDeletes();
            
            // $table->foreign('student_id')->references('id')->on('');
            // $table->foreign('teacher_id')->references('id')->on('');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_child_benefit');
    }
}
