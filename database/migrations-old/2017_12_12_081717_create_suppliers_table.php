<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sup_name', 255);
            $table->string('sup_address', 255)->nullable();
            $table->string('sup_country', 255)->nullable();
            $table->string('sup_city', 255)->nullable();
            $table->string('sup_contact', 255)->nullable();
            $table->string('sup_email', 255)->nullable();
            $table->string('sup_mobile', 255)->nullable();
            //$table->string('sup_company', 30);
            $table->string('sup_website', 255)->nullable();
            $table->string('sup_type', 30)->nullable();
            $table->string('sup_bank_account', 255)->nullable();
            $table->unsignedSmallInteger('status')->default(1);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_suppliers');
    }
}
