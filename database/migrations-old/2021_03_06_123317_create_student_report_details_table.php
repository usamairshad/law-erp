<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

<<<<<<< HEAD:database/migrations/2021_03_06_123317_create_student_report_details_table.php
class CreateStudentReportDetailsTable extends Migration
=======
class CreateStudentGuardians extends Migration
>>>>>>> 32e01b72ee15f346e1fdf2fa1695151367b3e274:database/migrations/2021_03_04_104942_create_student_guardians.php
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_report_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_report_id');
            $table->string('report_parameter');
            $table->integer('marks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_report_details');
    }
}
