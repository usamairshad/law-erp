<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_entry_items', function (Blueprint $table) {

            $table->increments('id');

            // Entries Data
            $table->unsignedInteger('entry_type_id');
            $table->unsignedInteger('entry_id');
            $table->unsignedInteger('ledger_id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('parent_type', 50)->nullable();

            // Debit Credit Entries
            $table->date('voucher_date');
            $table->decimal('amount', 11,2);
            $table->enum('dc', ['d','c']);
            $table->text('narration')->nullable();

            $table->unsignedSmallInteger('status')->default(1);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key relationships
            $table->foreign('entry_type_id')->references('id')->on('erp_entry_types');
            $table->foreign('entry_id')->references('id')->on('erp_entries');
            $table->foreign('ledger_id')->references('id')->on('erp_ledgers');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_entry_items');
    }
}
