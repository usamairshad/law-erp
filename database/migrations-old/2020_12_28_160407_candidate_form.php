<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CandidateForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_form', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('job_post_id');
            $table->string('name');
            $table->string('education');
            $table->string('experience');
            $table->string('gender');
            $table->integer('expected_salary');
            $table->string('cv');
            $table->string('cover_letter');
            $table->string('phone');
            $table->string('email');
            $table->string('cnic')->nullable();
            $table->string('domicile')->nullable();
            $table->string('passport')->nullable();
            $table->string('passport_size_photo')->nullable();
            $table->string('degree')->nullable();
            $table->string('awards')->nullable();
            $table->string('recommendation_letter')->nullable();
            $table->string('experience_letter')->nullable();
            $table->string('user_id')->nullable();
            $table->date('expected_joining');

            $table->enum('status',['Pending','Accepted','Rejected','Selected','Interview-Taken','Not-Hired','Acknowledged']);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('job_post_id')->references('id')->on('job_post');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users'); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_form');
    }
}
