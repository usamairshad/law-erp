<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveSessionStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_session_students', function (Blueprint $table) {
            $table->increments('id');
            //Newly Created
            $table->unsignedInteger('active_session_id');
            $table->unsignedInteger('active_session_section_id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('academics_group_id');
            $table->timestamps();

            // Foreign Key relationships

            $table->foreign('active_session_section_id')->references('id')->on('active_session_sections');
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('academics_group_id')->references('id')->on('academics_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('active_session_students');
    }
}
