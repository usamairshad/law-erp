<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_invoice_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type',100)->nullable();
            $table->string('no_of_prints',100)->nullable();
            $table->unsignedInteger('salesinvoice_id');
            $table->text('serialNo')->nullable();
            $table->string('print_name',200)->nullable();
            $table->string('total_count',255)->nullable();
            
            $table->integer('quantity')->default(0);
            $table->integer('return_qty')->default(0);
            $table->bigInteger('unit_price');
            
            $table->bigInteger('sale_price');
            $table->bigInteger('tax_amount');
            $table->bigInteger('tax_percent');
            $table->bigInteger('discount')->nullable();
            $table->integer('discount_type')->nullable();
            $table->bigInteger('discount_amount')->nullable();
            $table->bigInteger('misc_amount')->nullable();
            $table->bigInteger('net_income')->nullable();
            $table->bigInteger('total_price');
            $table->bigInteger('line_total');


            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('salesinvoice_id')->references('id')->on('erp_salesinvoice');
            
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_invoice_details');
    }
}
