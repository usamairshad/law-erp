<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncrementPfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('increment_pfs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('role_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('reason')->nullable();
            $table->integer('basic')->nullable();
            $table->integer('pf_previous')->nullable();
            $table->integer('pf_new')->nullable();
            $table->integer('New_salary_increment')->nullable();
            $table->integer('Amount')->nullable();
            $table->string('increment_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('increment_pfs');
    }
}
