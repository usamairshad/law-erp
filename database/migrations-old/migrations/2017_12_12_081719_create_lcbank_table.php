<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLcbankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_lcbank', function (Blueprint $table) {
            $table->increments('id')->unique;
            $table->integer('transaction_type')->nullable();
            $table->string('pf_no',255);
            $table->string('lc_no', 255)->nullable();
            $table->integer('bnk_id')->nullable();
            $table->string('lc_amt', 255)->nullable();
            $table->integer('lc_status')->nullable();
            $table->dateTime('open_date')->nullable();
            $table->string('dollar_rate',255)->nullable();
            $table->string('today_doller',255)->nullable();
            $table->string('total_lc_amount',255)->nullable();
            $table->date('lc_expiry_date')->nullable();
            $table->dateTime('document_date')->nullable();
            $table->string('bl_no',255)->nullable();
            $table->string('murahabah_profit',30)->nullable();
            $table->string('total_days',20)->nullable();
            $table->integer('document_received')->nullable();
            $table->integer('container_status')->nullable();
            $table->longText('description')->nullable();
            $table->integer('lc_type')->nullable();
            $table->integer('delivery_term')->nullable();
            $table->string('margin_amount',255)->nullable();
            $table->string('murahabah_amount',255)->nullable();
            $table->string('murahabah_rate',20)->nullable();
            $table->string('margin_rate',20)->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_lcbank');
    }
}
