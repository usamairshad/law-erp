<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendancePayroll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_attendance_payroll', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_student_id')->nullable();
            $table->unsignedInteger('student_id')->nullable();
            $table->Date('class_date');
            $table->time('class_time');
            $table->text('day')->nullable(); 
            $table->string('attendance_type');
            $table->string('attendance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attendance_payroll');
    }
}
