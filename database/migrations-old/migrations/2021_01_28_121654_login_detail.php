<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoginDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('start_time')->unique();
            $table->string('end_time');
            $table->enum('status',['0','1'])->default('1');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key relationships
            $table->foreign('user_id')->references('id')->on('erp_users');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
