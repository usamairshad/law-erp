<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignAssetCurrentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_asset_currents', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('staff_id');
            $table->unsignedInteger('asset_id');
            $table->unsignedInteger('return_policy_id');
            $table->string('return')->nullable();
            $table->integer('quantity')->nullable();
            $table->timestamps();
            $table->foreign('staff_id')->references('id')->on('staff')->onDelete('cascade');
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('cascade');
            $table->foreign('return_policy_id')->references('id')->on('return_policies')->onDelete('cascade');
            $table->unsignedInteger('created_by')->nullable()->nullable();
            $table->unsignedInteger('updated_by')->nullable()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_asset_currents');
    }
}
