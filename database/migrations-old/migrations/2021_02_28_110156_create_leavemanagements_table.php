<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavemanagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leavemanagements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_teacher_id');
            $table->unsignedInteger('staff_id');
            $table->unsignedInteger('course_id')->nullable();
            $table->string('leave_type')->nullable();
            $table->Date('leave_date')->nullable();
            $table->Date('start_date')->nullable();
            $table->Date('end_date')->nullable();
            $table->unsignedInteger('no_of_leaves')->nullable();
            $table->string('status')->default('Pending');
            $table->unsignedInteger('approver_id');
            $table->string('approver_role');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leavemanagements');
    }
}
