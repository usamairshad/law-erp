<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('quiz_id')->nullable();
            $table->string('date')->nullable();
            $table->integer('full_mark_theory')->nullable();
            $table->integer('pass_mark_theory')->nullable();
            $table->integer('approved')->nullable();
            $table->integer('quiz_check')->nullable();
            $table->integer('conducted')->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('last_updated_by')->nullable();

        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_schedules');
    }
}
