<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_sales', function (Blueprint $table) {
            $table->increments('id')->unique;
            $table->integer('group_id');
            $table->string('sal_no');
            $table->dateTime('sal_date');
            $table->dateTime('valid_upto');
            $table->string('customer_id');
            $table->integer('branch_id');
            $table->integer('employee_id');
            //$table->integer('stock_item_id');
            //$table->integer('product_id');
            $table->integer('sup_id');
            $table->longText('description');
            $table->string('remarks');
            $table->integer('discount');
            $table->string('tax');
            $table->integer('final_amount');
            $table->integer('total_amount');
            $table->integer('total_balance');
            $table->string('gst');
            $table->integer('shipping_charges');
            $table->unsignedSmallInteger('status')->default(1);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_sales');
    }
}
