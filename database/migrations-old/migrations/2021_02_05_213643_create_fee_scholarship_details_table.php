<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeScholarshipDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_scholarship_details', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('fsd_id');
            $table->Integer('fee_head_id');
            $table->float('discount_percentage');
            $table->enum('status',['0','1'])->default('1');
            $table->unsignedInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_scholarship_details');
    }
}
