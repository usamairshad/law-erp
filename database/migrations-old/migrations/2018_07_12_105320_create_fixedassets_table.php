<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedassetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_fixedassets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_is')->nullable();
            $table->string('product_name')->nullable();
            $table->integer('price')->nullable();
            $table->integer('branch')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('po_number')->nullable();
            $table->integer('serial_number')->nullable();
            $table->string('remarks')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('products_desc')->nullable();
            $table->dateTime('products_date');
            $table->dateTime('warranty_expires');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_fixedassets');
    }
}
