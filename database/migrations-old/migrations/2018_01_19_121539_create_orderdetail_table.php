<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderdetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_orderdetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('perchas_id');
            $table->integer('product_id')->nullable();
            $table->string('unit_name',50)->nullable();
            $table->integer('qty')->nullable();
            $table->integer('remaining_qty')->nullable();
            $table->string('unit_price',255)->nullable();
            $table->string('amount',255)->nullable();
            $table->string('country_of_origin',255)->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_orderdetail');
    }
}
