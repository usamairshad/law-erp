<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveSessionTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_session_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_section_id');
            $table->unsignedInteger('staff_id');
            $table->unsignedInteger('rate_per_lecture')->nullable(); 
            $table->string('course_ids')->nullable();  
            $table->string('rates')->default(0);
            $table->string('days_in_month')->default(0);
            $table->unsignedInteger('class_timetable_id');
          
            $table->timestamps();

            //Foreign Key relationships
            
            //$table->foreign('course_id')->references('id')->on('courses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('active_session_teachers');
    }
}
