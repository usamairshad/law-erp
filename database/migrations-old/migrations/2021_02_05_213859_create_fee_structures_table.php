<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_structures', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('session_id')->nullable();
            $table->unsignedInteger('program_id');
            $table->unsignedInteger('term_id');
            $table->string('fee_structure_code')->nullable();
            $table->unsignedInteger('fee_head_id');
            $table->float('fee_amount');
            $table->boolean('status')->default(1);
            $table->unsignedInteger('last_updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_structures');
    }
}
