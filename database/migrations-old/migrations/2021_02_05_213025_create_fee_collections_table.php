<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_collections', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('session_id')->nullable();
            $table->unsignedInteger('students_id')->unsigned();
            $table->unsignedInteger('fee_masters_id')->unsigned();
            $table->unsignedInteger('fee_structure_code');
            $table->string('bill_no', '255')->nullable();
            $table->unsignedInteger('fee_term_id')->unsigned();
            $table->date('due_date')->nullable();
            $table->string('paid_date', '255')->nullable();
            $table->float('paid_amount')->nullable()->default(0);
            $table->float('discount')->nullable()->default(0);
            $table->float('discount_type')->nullable()->default(1);
            $table->float('fine')->nullable()->default(0);
            $table->float('tax_percentage')->default(0);
            $table->float('total_amount')->nullable()->default(0);
            $table->string('payment_mode', '255')->nullable();
            $table->string('note', '100')->nullable();
            $table->boolean('fee_status')->default(1);
            $table->integer('fc_id')->unsigned();
            $table->string('fc_bill_no', '255')->nullable();
            $table->float('balance')->nullable()->default(0);
            $table->integer('all_paid')->nullable()->default(0);
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('last_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_collections');
    }
}
