<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ErpStaffSalaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_staff_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ledger_id');
            $table->unsignedInteger('staff_id');
            $table->unsignedInteger('medical_allowance');
            $table->unsignedInteger('house_rent_allowance');
            $table->unsignedInteger('conveyance_allowance');
            $table->unsignedInteger('training_all')->nullable();
            $table->unsignedInteger('eobi')->nullable();
            $table->unsignedInteger('provident_fund')->nullable();
            $table->unsignedInteger('tax_amount')->nullable();
            $table->unsignedInteger('loan_amount')->nullable();
            $table->unsignedInteger('remaining_amount')->nullable();
            $table->unsignedInteger('pay_loan')->nullable();
            $table->unsignedInteger('salary_slip_id');
            $table->string('description');
            $table->integer('Amount');
            $table->integer('deduction')->nullable();
            $table->integer('net_salary')->nullable();
            $table->date('date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
