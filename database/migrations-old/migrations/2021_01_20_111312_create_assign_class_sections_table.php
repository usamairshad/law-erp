<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignClassSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_class_section', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('board_program_class_id');
            $table->unsignedInteger('section_id');
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key relationships

            $table->foreign('board_program_class_id')->references('id')->on('board_program_class');
            $table->foreign('section_id')->references('id')->on('sections');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_class_section');
    }
}
