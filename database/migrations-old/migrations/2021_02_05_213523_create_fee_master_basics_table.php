<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeMasterBasicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_master_basics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id')->nullable();
            $table->unsignedInteger('session_id')->nullable();
            $table->unsignedInteger('student_id')->nullable();
            $table->unsignedInteger('program_id')->nullable();
            $table->unsignedInteger('fee_master_basic_id')->nullable();
            $table->unsignedInteger('installment_id')->nullable();
            $table->unsignedInteger('fee_structure_code');
            $table->unsignedInteger('fee_type')->nullable();
            $table->string('bill_no')->nullable();
            $table->string('basic_fee')->nullable();
            $table->string('total_fee')->nullable();
            $table->integer('session_total')->nullable();
            $table->integer('term_total')->nullable();
            $table->string('discount_fee')->nullable();
            $table->string('total_after_discount')->nullable();
            $table->integer('discount_type')->default(1);
            $table->string('special_discount_comment')->nullable();
            $table->string('other_discount_fee')->nullable();
            $table->integer('scholarship')->nullable();
            $table->integer('scholarship_no_of_a')->nullable();
            $table->date('voucher_date')->nullable();
            $table->date('fee_due_date_start')->nullable();
            $table->date('fee_due_date_end')->nullable();
            $table->string('status')->nullable();
            $table->integer('ceo_approval')->nullable();
            $table->text('other_discount_type')->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('last_updated_by')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_master_basics');
    }
}
