<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformastockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_performastock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ci_id')->nullable();
            $table->integer('peroforma_no')->nullable();
            $table->integer('product_name')->nullable();
            $table->string('total_qty',255)->nullable();
            $table->string('rel_qty',255)->nullable();
            $table->string('balanace_qty',255)->nullable();
            $table->string('amount',255)->nullable();
            $table->string('total_amount',255)->nullable();
            $table->string('amount_rs',255)->nullable();
            $table->string('cost_per_unit',255)->nullable();
            $table->string('insurance',255)->nullable();
            $table->string('bank_charges',255)->nullable();
            $table->string('freight',255)->nullable();
            $table->string('profit_investment',255)->nullable();
            $table->string('murahba_profit',255)->nullable();
            $table->string('mvmnt_bndg',255)->nullable();
            $table->string('Duty',255)->nullable();
            $table->string('f_total_amount',255)->nullable();
            $table->string('costing_u_price',255)->nullable();
            $table->string('costing_do_price',255)->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_performastock');
    }
}
