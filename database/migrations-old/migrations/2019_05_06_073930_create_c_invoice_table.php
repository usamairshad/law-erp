<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_c_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ci_no')->nullable();
            $table->date('eta')->nullable();
            $table->date('atp')->nullable();
            $table->string('etd',255)->nullable();
            $table->string('lcno',255)->nullable();
            $table->string('mos',100)->nullable();
            $table->string('usd_amt',255)->nullable();
            $table->string('today_doller',255)->nullable();
            $table->date('lc_expiry_date')->nullable();
            $table->string('total_days',255)->nullable();
            $table->date('document_date')->nullable();
            $table->integer('document_received')->nullable();
            $table->string('bl_no',10)->nullable();
            $table->string('container_status',10)->nullable();
            $table->string('status',5)->nullable();
            $table->date('close_date')->nullable();
            $table->string('margin_rat',50)->nullable();
            $table->string('murahba_profit_rate',50)->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_c_invoice');
    }
}
