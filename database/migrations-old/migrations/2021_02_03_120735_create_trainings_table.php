<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('venue');
            $table->enum("category", ["local", "international"]);
            $table->string('department')->nullable();
            $table->string('designation')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->string('description')->nullable();
            $table->date("date");
            $table->string("time");
            $table->string("training_by")->nullable();
            $table->bigInteger("no_of_days")->nullable();
            $table->boolean("training_charges");
            $table->string("training_type");
            $table->integer("created_by");
            $table->integer("updated_by");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainees');
        Schema::dropIfExists('trainings');
    }
}
