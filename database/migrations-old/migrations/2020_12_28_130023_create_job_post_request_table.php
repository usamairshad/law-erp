<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('job_post_request', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('branch_id');
            
            $table->unsignedInteger('subject_id');
            $table->enum('section',['junior','senior']);
            $table->string('experience');
            $table->string('gender');
            $table->unsignedInteger('program_id');
            $table->string('education');
            $table->date('expected_joining');

            $table->enum('job_status',['Permanent','Full Time','Part Time','Visiting']);

            $table->unsignedSmallInteger('status')->default(1);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            //Foreign keys
             //$table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('subject_id')->references('id')->on('courses');
            // $table->foreign('program_id')->references('id')->on('programs');
            //$table->foreign('section_id')->references('id')->on('sections');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post_request');
    }
}
