<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_tax_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tax_class');
            $table->unsignedInteger('tax_type')->default(1);
            $table->double('fix_amount',12, 2)->nullable()->default(0);
            $table->double('tax_percent',12, 2)->nullable()->default(0);
            $table->double('start_range',12, 2)->nullable()->default(0);
            $table->double('end_range',12, 2)->nullable()->default(0);
            $table->unsignedInteger('year')->nullable();
            $table->unsignedSmallInteger('status')->default(1);
            
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();

            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_tax_settings');
    }
}
