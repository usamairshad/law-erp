<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrmEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Schema::create('erp_hrm_employees', function(Blueprint $table)
		// {
		// 	$table->increments('id');
		// 	$table->string('device_id', 191)->nullable();
		// 	$table->string('employee_code', 191)->nullable();
		// 	$table->string('name', 191);
		// 	$table->string('nationality', 20)->nullable();
		// 	$table->string('title', 191)->nullable();
		// 	$table->string('middle_name', 191)->nullable();
		// 	$table->string('current_address', 191)->nullable();
		// 	$table->string('cost_center', 191)->nullable();
		// 	$table->string('religion', 191)->nullable();
		// 	$table->string('place_of_birth', 191)->nullable();
		// 	$table->string('cnic_issue_place', 191)->nullable();
		// 	$table->date('cnic_issue_date')->nullable();
		// 	$table->date('cnic_expiry_date')->nullable();
		// 	$table->string('cnic_father', 100)->nullable();
		// 	$table->string('cnic_spouse', 100)->nullable();
		// 	$table->string('ntn_no', 100)->nullable();
		// 	$table->string('eobi_no', 100)->nullable();
		// 	$table->string('social_security_no', 100)->nullable();
		// 	$table->string('driving_license_no', 100)->nullable();
		// 	$table->date('driving_license_issue_date')->nullable();
		// 	$table->date('driving_license_expiry_date')->nullable();
		// 	$table->string('passport_no', 191)->nullable();
		// 	$table->date('passport_issue_date')->nullable();
		// 	$table->date('passport_expiry_date')->nullable();
		// 	$table->string('nk_name', 191)->nullable();
		// 	$table->string('nk_relation', 191)->nullable();
		// 	$table->string('employer', 255)->nullable();
		// 	$table->string('nk_cnic', 191)->nullable();
		// 	$table->string('nk_contact_no', 191)->nullable();
		// 	$table->string('nk_address', 191)->nullable();
		// 	$table->decimal('nk_share_percentage', 11)->nullable();
		// 	$table->date('date_of_hiring')->nullable();
		// 	$table->string('employment_mode', 191)->nullable();
		// 	$table->string('category', 191)->nullable();
		// 	$table->string('designation', 191)->nullable();
		// 	$table->string('employment_type', 191)->nullable();
		// 	$table->string('employment_status', 191)->nullable();
		// 	$table->integer('employment_grade')->nullable();
		// 	$table->string('official_contact_no', 191)->nullable();
		// 	$table->string('ext_no', 191)->nullable();
		// 	$table->string('official_email', 191)->nullable();
		// 	$table->string('employee_card_no', 191)->nullable();
		// 	$table->integer('line_manager_id')->nullable();
		// 	$table->string('org_location', 191)->nullable();
		// 	$table->date('probation_expiry')->nullable();
		// 	$table->integer('probation_extention_period')->nullable();
		// 	$table->date('confirmation_date')->nullable();
		// 	$table->string('health_ins_no', 191)->nullable();
		// 	$table->date('resignation_date')->nullable();
		// 	$table->date('last_working_day')->nullable();
		// 	$table->string('branch_name', 191)->nullable();
		// 	$table->string('account_type', 191)->nullable();
		// 	$table->string('iban_number', 191)->nullable();
		// 	$table->string('account_title', 191)->nullable();
		// 	$table->string('branch_code', 191)->nullable();
		// 	$table->string('blood_group', 191)->nullable();
		// 	$table->decimal('basic_salary', 11)->nullable();
		// 	$table->decimal('current_basic_salary', 11)->nullable();
		// 	$table->decimal('house_rent_allowance', 11)->nullable();
		// 	$table->decimal('utilities_allowance', 11)->nullable();
		// 	$table->decimal('gross_salary', 11)->nullable();
		// 	$table->decimal('conveyance_allowance', 11)->nullable();
		// 	$table->decimal('accommodation_allowance', 11)->nullable();
		// 	$table->decimal('fuel_allowance', 11)->nullable();
		// 	$table->decimal('mobile_allowance', 11)->nullable();
		// 	$table->decimal('medical_allowance', 11)->default(0);
		// 	$table->integer('user_id')->unsigned()->nullable();
		// 	$table->integer('department_id')->unsigned()->nullable();
		// 	$table->string('first_name', 191);
		// 	$table->string('last_name', 191)->nullable();
		// 	$table->string('father_name', 191)->nullable();
		// 	$table->string('husband_name', 191)->nullable();
		// 	$table->string('email', 191)->nullable();
		// 	$table->string('cnic', 191)->nullable();
		// 	$table->date('date_of_birth')->nullable();
		// 	$table->integer('shift_id')->default(1);
		// 	$table->string('gender', 20)->nullable()->default('male');
		// 	$table->string('marital_status', 20)->nullable();
		// 	$table->string('mobile', 191)->nullable();
		// 	$table->string('emergency_contact_person', 191)->nullable();
		// 	$table->string('emergency_mobile', 191)->nullable();
		// 	$table->string('city', 191)->nullable();
		// 	$table->string('address', 191)->nullable();
		// 	$table->integer('job_title')->unsigned()->nullable();
		// 	$table->integer('job_category')->unsigned()->nullable();
		// 	$table->date('date_of_joining')->nullable();
		// 	$table->integer('probation_period')->nullable()->default(0);
		// 	$table->date('contract_start_date')->nullable();
		// 	$table->date('contract_end_date')->nullable();
		// 	$table->integer('notice_period')->unsigned()->nullable();
		// 	$table->integer('report_to')->unsigned()->nullable()->default(0);
		// 	$table->integer('country_id')->unsigned()->nullable()->default(1);
		// 	$table->integer('region_id')->unsigned()->nullable()->default(0);
		// 	$table->integer('branch_id')->unsigned()->nullable()->default(0);
		// 	$table->integer('territory_id')->unsigned()->nullable()->default(0);
		// 	$table->string('edu_level', 191)->nullable();
		// 	$table->string('edu_institute', 191)->nullable();
		// 	$table->string('edu_specialization', 191)->nullable();
		// 	$table->string('edu_year', 191)->nullable();
		// 	$table->string('edu_score', 191)->nullable();
		// 	$table->date('edu_start_date')->nullable();
		// 	$table->date('edu_end_date')->nullable();
		// 	$table->string('termination_reason', 191)->nullable();
		// 	$table->date('date_of_resign')->nullable();
		// 	$table->string('account_number', 191)->nullable();
		// 	$table->string('bank_name', 191)->nullable();
		// 	$table->string('pre_company', 191)->nullable();
		// 	$table->string('pre_job_title', 191)->nullable();
		// 	$table->string('pre_job_date_from', 191)->nullable();
		// 	$table->string('pre_job_date_to', 191)->nullable();
		// 	$table->string('skill_title', 191)->nullable();
		// 	$table->string('skills_experiance', 191)->nullable();
		// 	$table->boolean('is_sc')->nullable()->default(0);
		// 	$table->integer('sc_amount')->unsigned()->nullable()->default(0);
		// 	$table->boolean('is_house_rent')->nullable()->default(0);
		// 	$table->boolean('is_utility')->nullable()->default(0);
		// 	$table->boolean('is_overtime')->nullable()->default(0);
		// 	$table->integer('job_application_id')->unsigned()->nullable();
		// 	$table->string('image', 191)->nullable();
		// 	$table->integer('total_add_allowance')->nullable();
		// 	$table->boolean('status')->default(1);
		// 	$table->integer('created_by')->unsigned()->nullable();
		// 	$table->integer('updated_by')->unsigned()->nullable();
		// 	$table->integer('deleted_by')->unsigned()->nullable();
		// 	$table->timestamps();
		// 	$table->softDeletes();

  //           $table->foreign('created_by')->references('id')->on('erp_users');
  //           $table->foreign('department_id')->references('id')->on('erp_departments');

  //           $table->foreign('job_title')->references('id')->on('erp_hrm_job_title');
  //           $table->foreign('updated_by')->references('id')->on('erp_users');
  //           $table->foreign('deleted_by')->references('id')->on('erp_users');
		// });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('erp_hrm_employees');
	}

}
