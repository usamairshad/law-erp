<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('term_id')->nullable();
            $table->unsignedInteger('exam_id')->nullable();
            $table->unsignedInteger('grading_types_id')->nullable();
            $table->unsignedInteger('subjects_id')->nullable();
            $table->unsignedInteger('years_id')->nullable();
            $table->unsignedInteger('months_id')->nullable();
            $table->unsignedInteger('semesters_id')->nullable();
            $table->unsignedInteger('exams_id')->nullable();
            $table->unsignedInteger('assessment_id')->nullable();
            $table->integer('submission')->nullable();
            $table->unsignedInteger('faculty_id')->nullable();
            $table->unsignedInteger('section_id')->nullable();
            $table->unsignedInteger('active_session_id')->nullable();
            $table->enum('conducted', ['yes', 'no']);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->float('total_marks')->nullable();
            $table->integer('approval_status')->nullable();
            $table->integer('publish_status')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('last_updated_by')->nullable();

        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_schedules');
    }
}
