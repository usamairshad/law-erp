<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveSessionSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_session_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_id');
            $table->unsignedInteger('section_id');

            $table->timestamps();

            $table->foreign('active_session_id')->references('id')->on('active_sessions');
            $table->foreign('section_id')->references('id')->on('sections');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_session_sections');
    }
}
