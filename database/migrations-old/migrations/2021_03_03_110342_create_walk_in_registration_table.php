<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalkInRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('walk_in_registration', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id');
            $table->string('student_first_name')->nullable();
            $table->string('student_middle_name')->nullable();
            $table->string('student_last_name')->nullable();
            $table->string('parent_first_name')->nullable();
            $table->string('parent_middle_name')->nullable();
            $table->string('parent_last_name')->nullable();
            $table->string('contact_number')->nullable();
            $table->integer('program_id')->nullable();
            $table->date('reg_date')->nullable();
            $table->text('cnic')->nullable();
            $table->text('reference')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('walk_in_registration');
    }
}
