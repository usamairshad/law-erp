<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuardianDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guardian_details', function (Blueprint $table) {
            $table->increments('id');
            //New Created
            $table->unsignedInteger('user_id');
            $table->string('first_name');
            $table->string('middle_name');  
            $table->string('last_name');
            $table->string('cnic');
            $table->string('guardian_relation');
            $table->string('guardian_email');
            $table->string('guardian_occupation');
            $table->string('address');
            $table->string('contact_no');
            $table->string('nationality');
            $table->enum('tax_info',['Filer','Non_Filer']);
            $table->string('passport');
            $table->boolean('is_guardian');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guardian_details');
    }
}
