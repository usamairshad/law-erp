<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesDetialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_SalesDetial', function (Blueprint $table) {
            $table->increments('id')->unique;
            $table->integer('sal_id');
            $table->string('sal_st_name');
            $table->string('sal_st_unit_price');
            $table->string('sale_quantity');
            $table->string('sale_amount');
            $table->unsignedSmallInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_SalesDetial');
    }
}
