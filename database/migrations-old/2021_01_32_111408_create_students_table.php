<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_no');

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('board_id');
            $table->unsignedInteger('program_id');
            $table->unsignedInteger('academic_group_id');
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('section_id');
            $table->string('reg_date');
            $table->string('previous_school')->nullable();
            $table->string('previous_class')->nullable();
            $table->string('academic_status');
            $table->string('first_name');
            $table->string('middle_name');  
            $table->string('last_name');
            $table->string('date_of_birth');
            $table->string('gender');
            $table->string('blood_group');
            $table->string('nationality');
            $table->string('mother_tongue');
            $table->string('email');
            $table->string('extra_info');
            $table->string('home_phone');
            $table->string('mobile_1');
            $table->string('mobile_2');
            $table->string('state');
            $table->string('country');
            $table->string('city');
            $table->string('address');
            $table->string('temp_address');
            $table->string('student_image');
            $table->enum('status',['Active','InActive','Pending'])->default('Active');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();


            // Foreign Key relationships
            $table->foreign('branch_id')->references('id')->on('erp_branches');
            $table->foreign('board_id')->references('id')->on('boards');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('academic_group_id')->references('id')->on('academics_groups'); 
            $table->foreign('class_id')->references('id')->on('classes');
            $table->foreign('section_id')->references('id')->on('sections');
            $table->foreign('user_id')->references('id')->on('erp_users');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
