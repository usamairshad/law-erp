<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_candidate_score', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('candidate_form_id');
            $table->unsignedInteger('schedule_id');
            $table->string('relevant_knowledge')->nullable();
            $table->string('skills')->nullable();
            $table->string('communication')->nullable();
            $table->string('relevant_experience')->nullable();
            $table->string('confidence')->nullable();

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('candidate_form_id')->references('id')->on('candidate_form');
            $table->foreign('schedule_id')->references('id')->on('schedule');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_candidate_score');
    }
}
