<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyllabusHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('syllabus_heads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id');
            $table->integer('session_id')->nullable();
            $table->string('syllabus_head_title')->nullable();
            $table->string('slug')->nullable();
            $table->string('status')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('last_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('syllabus_heads');
    }
}
