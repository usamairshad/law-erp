<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_buildings', function (Blueprint $table) {
            $table->increments('id')->nullable();
            $table->date('aggreement_date')->nullable();
            $table->string('land_name')->nullable();
            $table->string('contact_mail')->nullable();
            $table->integer('contact_number')->nullable();
            $table->string('cnic')->nullable();
            $table->integer('basic_rent')->nullable();
            $table->string('security')->nullable();
            $table->string('tenure')->nullable();
            $table->integer('increment')->nullable();
//            $table->unsignedInteger('payee_id')->nullable();
//            $table->foreign('payee_id')->references('id')->on('payee_lists');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_buildings');
    }
}
