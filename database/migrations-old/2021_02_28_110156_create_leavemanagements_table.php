<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavemanagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leavemanagements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('teacher_id');
            $table->unsignedInteger('class_id')->nullable();
            $table->string('section_id')->nullable();
            $table->string('subject_id')->nullable();
            $table->string('board')->nullable();
            $table->string('program')->nullable();
            $table->string('leave_type');
            $table->Date('start_date');
            $table->Date('end_date');
            $table->unsignedInteger('no_of_leaves');
            $table->string('status')->default('Pending');
            $table->unsignedInteger('approver_id');
            $table->string('approver_role');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leavemanagements');
    }
}
