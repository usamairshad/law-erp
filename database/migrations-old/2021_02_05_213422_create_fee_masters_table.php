<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('session_id')->nullable();
            $table->unsignedInteger('program_id');
            $table->unsignedInteger('students_id');
            $table->string('fee_head', '100');
            $table->unsignedInteger('fee_structure_code');
            $table->unsignedInteger('fee_head_term')->default(1);
            $table->integer('fee_amount');
            $table->float('fee_discount_percentage')->default('0');
            $table->string('fee_discount_comment', '250');
            $table->integer('scholarship')->default('0');
            $table->integer('scholarship_no_of_a')->default('0');
            $table->float('discount_type')->default('1');
            $table->boolean('status')->default(1);
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('last_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_masters');
    }
}
