<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignActiveSessionCourseLabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_active_session_course_lab', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_id')->nullable();
            $table->unsignedInteger('course_id')->nullable();
            $table->unsignedInteger('lab_id')->nullable();
            $table->string('description')->nullable();
            $table->enum('status',['Active','InActive','Pending'])->default('Active');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key relationships

            $table->foreign('active_session_id')->references('id')->on('active_sessions');
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('lab_id')->references('id')->on('labs');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_active_session_course_lab');
    }
}
