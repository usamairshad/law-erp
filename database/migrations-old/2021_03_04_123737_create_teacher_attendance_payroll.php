<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherAttendancePayroll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_attendance_payroll', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_teacher_id');
            $table->Date('class_Date');
            $table->time('class_time');
            $table->text('day');
            $table->unsignedInteger('attendance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_attendance_payroll');
    }
}
