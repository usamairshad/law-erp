<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_ledgers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('number', 255)->nullable();
            $table->string('code', 255)->nullable();
            $table->unsignedInteger('group_id')->default(0);
            $table->string('group_number', 255)->nullable();
            $table->unsignedInteger('opening_balance')->nullable()->default(0);
            $table->unsignedInteger('closing_balance')->nullable()->default(0);
            $table->enum('balance_type', ['c', 'd'])->default('c');
            $table->unsignedInteger('account_type_id')->nullable();
            $table->unsignedTinyInteger('status')->default(1);
            $table->unsignedInteger('branch_id')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('parent_type')->nullable();
            $table->unsignedTinyInteger('is_common')->nullable();


            // Foreign Key relationships
            $table->foreign('account_type_id')->references('id')->on('erp_account_types');
            $table->foreign('branch_id')->references('id')->on('erp_branches');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_ledgers');
    }
}
