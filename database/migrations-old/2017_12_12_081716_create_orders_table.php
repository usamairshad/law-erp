<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('po_number',255)->nullable();
            $table->date('po_date')->nullable();
            $table->integer('sup_id')->nullable();
            $table->string('perfomra_no',255)->nullable();
            $table->string('sale_order_no',255)->nullable();
            $table->string('shipping',255)->nullable();
            $table->string('soruce',255)->nullable();
            $table->string('destination',255)->nullable();
            $table->date('sal_date')->nullable();
            $table->date('valid_upto')->nullable();
            $table->string('total_order_amt',255)->nullable();
            $table->integer('order_type')->nullable();
            $table->string('payment_terms',255)->nullable();
            $table->integer('status')->nullable();
            $table->integer('confirm_status')->default(1);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_orders');
    }
}
