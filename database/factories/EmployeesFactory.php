<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Admin\Employees::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'mobile' => $faker->unique()->phoneNumber,
        'phone' => $faker->unique()->phoneNumber,
        'created_by' => App\User::first()->id,
        'updated_by' => App\User::first()->id,
        'created_at' => $faker->dateTime('now'),
        'updated_at' => $faker->dateTime('now'),
    ];
});
