<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Config;

$factory->define(App\Models\Admin\Ledgers::class, function (Faker $faker) {

    $name = $faker->unique()->company;
    $Group = Config::get('groups.data')[array_rand(Config::get('groups.data'))];
    $number = \App\Helpers\CoreAccounts::generateLedgerNumber(1, $Group['number'], rand(1000, 9999));

    return [
        'number' => $number,
        'name' => $name,
        'code' => str_replace(' ', '+', strtoupper($name)),
        'group_id' => $Group['id'],
        'group_number' => $Group['number'],
        'account_type_id' => $Group['account_type_id'],
//        'opening_balance' => $faker->numberBetween(0, 100000),
        'opening_balance' => 0,
        'closing_balance' => $faker->numberBetween(0, 0),
        'balance_type' => $faker->randomElement(['c','d']),
        'created_by' => App\User::first()->id,
        'updated_by' => App\User::first()->id,
        'created_at' => $faker->dateTime('now'),
        'updated_at' => $faker->dateTime('now'),
    ];
});
