<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Admin\Departments::class, function (Faker $faker) {
    return [
        'name' => $faker->colorName,
        'created_by' => App\User::first()->id,
        'updated_by' => App\User::first()->id,
        'created_at' => $faker->dateTime('now'),
        'updated_at' => $faker->dateTime('now'),
    ];
});
