<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendancePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance__payrolls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_teacher_id');
            $table->Date('class_Date');
            $table->time('class_time');
            $table->text('day');
            $table->unsignedInteger('attendance');

            $table->string('teacher_id');
            $table->string('subject_id')->nullable();
            $table->string('class_id')->nullable();
            $table->string('board')->nullable();
            $table->string('program')->nullable();  

            $table->string('subject_scheduled_time');
            $table->string('attendance_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance__payrolls');
    }
}
