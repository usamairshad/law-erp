<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayeesAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_payees_amounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('building_id')->nullable();
            $table->unsignedInteger('payee_id')->nullable();
            $table->integer('payee_amount')->nullable();
            $table->timestamps();
            $table->foreign('payee_id')->references('id')->on('erp_payee_lists')->onDelete('cascade');
            $table->foreign('building_id')->references('id')->on('erp_buildings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_payees_amounts');
    }
}
