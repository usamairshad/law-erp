<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizMarkLedgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_mark_ledgers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('quiz_schedule_id')->nullable();
            $table->unsignedInteger('students_id')->nullable();
            $table->integer('obtain_mark_theory')->nullable();
            $table->integer('absent_theory')->nullable();
            $table->integer('sorting_order')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('last_updated_by')->nullable();

        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_mark_ledgers');
    }
}
