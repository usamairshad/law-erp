<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReportParameters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('report_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reportGroup_id')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('range')->nullable();
            $table->timestamps();
            

        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_parameters');
    }
}
