<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankdetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_bankdetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->nullable();
            $table->integer('branch_id')->nullable();
            $table->string('accountType')->nullable();
            $table->integer('acc_type')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('account_no',100)->nullable();
            $table->string('bank_address', 255)->nullable();
            $table->string('branch_code', 100)->nullable();
            $table->string('phone_no', 30)->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_bankdetail');
    }
}
