<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilitybillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utilitybills', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->date('month')->nullable();
            $table->integer('meter_no')->nullable();
            $table->string('nature')->nullable();
            $table->integer('bill_amount')->nullable();
            $table->integer('with_holding_tax')->nullable();
            $table->integer('total_bill')->nullable();
            $table->integer('last_month_bill')->nullable();
            $table->integer('increase_decease_bill')->nullable();
            $table->text('reason')->nullable();
            $table->enum('status',['unpaid','paid'])->default('unpaid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilitybills');
    }
}
