<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildBenefitStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_benefit_students', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('staff_id')->nullable();
            $table->unsignedInteger('student_id')->nullable();
            $table->unsignedInteger('child_benefit_config_id')->nullable();
            $table->timestamps();


            $table->foreign('staff_id')->references('id')->on('staff')->onDelete('cascade');
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->foreign('child_benefit_config_id')->references('id')->on('child_benefit_configs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_benefit_students');
    }
}
