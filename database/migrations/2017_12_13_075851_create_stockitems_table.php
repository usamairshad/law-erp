<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_stockitems', function (Blueprint $table) {
            $table->increments('id')->unique;
            $table->integer('branch_id')->nullable();
            $table->string('st_name', 50)->nullable();
            $table->integer('st_unit_price')->nullable();
            $table->integer('opening_stock')->nullable();
            $table->integer('purchase_stock')->nullable();
            $table->integer('closing_stock')->nullable();
            $table->integer('moments_stock')->nullable();
            $table->unsignedSmallInteger('status')->default(1);
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_stockitems');
    }
}
