<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->unsignedInteger('databank_id')->nullable();
            $table->string('contact_person', 255)->nullable();
            $table->string('title_1', 255)->nullable();
            $table->string('title_2', 255)->nullable();
            $table->text('address_1')->nullable();
            $table->text('address_2')->nullable();
            $table->string('zip_code', 10)->nullable();
            $table->string('phone_1', 15)->nullable();
            $table->string('phone_2', 15)->nullable();
            $table->string('fax_1', 15)->nullable();
            $table->string('fax_2', 15)->nullable();
            $table->string('mobile_1', 15);
            $table->string('mobile_2', 15)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('website', 255)->nullable();
            $table->string('cnic', 30)->nullable();
            $table->string('gst', 20)->nullable();
            $table->string('ntn', 20)->nullable();
            $table->string('reference_person', 255)->nullable();
            $table->unsignedInteger('credit_limit')->nullable();
            $table->unsignedSmallInteger('credit_term_days')->nullable();
            $table->unsignedSmallInteger('discount')->nullable();

            // Bannk Information Starts
            $table->string('bank_name', 255)->nullable();
            $table->string('bank_ac_number', 255)->nullable();
            $table->unsignedInteger('employee_id')->nullable();
            $table->unsignedInteger('branch_id')->nullable();

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->unsignedSmallInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key relationships

//            $table->foreign('employee_id')->references('id')->on('employees');
//            $table->foreign('databank_id')->references('id')->on('mrk_databanks');
//            $table->foreign('branch_id')->references('id')->on('branches');
//            $table->foreign('created_by')->references('id')->on('users');
//            $table->foreign('updated_by')->references('id')->on('users');
//            $table->foreign('deleted_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_customers');
    }
}
