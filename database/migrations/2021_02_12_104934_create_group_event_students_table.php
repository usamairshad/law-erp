<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupEventStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_event_students', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_event_id');
            $table->unsignedInteger('student_id');
            $table->timestamps();

            $table->foreign('group_event_id')->references('id')->on('group_events')->onDelete("cascade");
            $table->foreign('student_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_event_students');
    }
}
