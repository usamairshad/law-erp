<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchBoardProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_board_program', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('board_id');
            $table->unsignedInteger('program_id');
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key relationships

            $table->foreign('branch_id')->references('id')->on('erp_branches');
            $table->foreign('board_id')->references('id')->on('boards');
            $table->foreign('program_id')->references('id')->on('programs');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_board_program');
    }
}
