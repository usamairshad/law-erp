<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_branch_users', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('branch_id')->nullable();

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            // Foreign Key relationships
            $table->foreign('user_id')->references('id')->on('erp_users')->onDelete('cascade');
            $table->foreign('branch_id')->references('id')->on('erp_branches')->onDelete('cascade');

            $table->foreign('created_by')->references('id')->on('erp_users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('erp_users')->onDelete('cascade');
            $table->foreign('deleted_by')->references('id')->on('erp_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_branch_users');
    }
}
