<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentReportDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('student_report_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_report_id')->nullable();
            
            $table->string('report_parameter')->nullable();
            $table->string('name')->nullable();
            $table->float('marks')->nullable();
            $table->timestamps();
            

        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_report_details');
    }
}
