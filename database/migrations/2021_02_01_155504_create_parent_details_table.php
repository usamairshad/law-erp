<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_details', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('last_updated_by')->nullable();
            $table->unsignedInteger('students_id');
            $table->string('father_first_name', '15')->nullable();
            $table->string('father_middle_name', '15')->nullable();
            $table->string('father_last_name', '15')->nullable();
            $table->string('father_occupation', '15')->nullable();
            $table->string('father_email', '15')->nullable();
            $table->string('father_mobile_1', '15')->nullable();
            $table->string('father_cnic', '50')->nullable();
            $table->string('mother_first_name', '50')->nullable();
            $table->string('mother_middle_name', '100')->nullable();
            $table->string('mother_last_name', '15')->nullable();
            $table->string('mother_cnic', '15')->nullable();
            $table->string('mother_occupation', '15')->nullable();
            $table->string('mother_mobile_1', '15')->nullable();
            $table->string('mother_email', '100')->nullable();
            $table->string('parent_image')->nullable();
            $table->string('guardian_is', '15')->nullable();
            $table->string('guardian_first_name', '15')->nullable();
            $table->string('guardian_middle_name', '15')->nullable();
            $table->string('guardian_last_name', '50')->nullable();
            $table->string('guardian_occupation', '50')->nullable();
            $table->string('guardian_address', '100')->nullable();
            $table->string('guardian_residence_number', '15')->nullable();
            $table->string('guardian_mobile_1', '15')->nullable();
            $table->string('guardian_cnic', '15')->nullable();
            $table->string('guardian_email', '15')->nullable();
            $table->string('guardian_relation', '100')->nullable();
            $table->boolean('filer_info');
            $table->boolean('status')->default(1);
            $table->timestamps();

            // Foreign Key relationships
          
            $table->foreign('students_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent_details');
    }
}
