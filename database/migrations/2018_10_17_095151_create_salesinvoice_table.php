<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesinvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_salesinvoice', function (Blueprint $table) {

            $table->increments('id');
            $table->string('invoice_to', 100)->nullable();
            $table->string('invoice_dept', 100)->nullable(); // sales, engineeer, company
            $table->unsignedInteger('quote_id')->nullable();
            $table->unsignedInteger('sales_person')->nullable();
            $table->unsignedInteger('customer_id')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('invoice_type')->default('taxable');
            $table->date('invoice_date')->nullable();
            $table->date('due_date')->nullable();
            $table->date('installation_date')->nullable();
            $table->string('warrenty')->nullable();
            $table->date('expires_at')->nullable();
            $table->string('rr_no')->nullable();
            $table->string('po_no',255)->nullable();
            $table->smallInteger('delivery_type')->nullable();
            $table->bigInteger('services_amount')->nullable();
            $table->bigInteger('products_amount')->nullable();
            $table->bigInteger('services_tax')->nullable();
            $table->bigInteger('products_tax')->nullable();
            $table->bigInteger('amount')->nullable();
            $table->bigInteger('tax_amount')->nullable();
            $table->bigInteger('total_amount')->nullable();
            $table->bigInteger('remaining_amount')->nullable();
            $table->bigInteger('paid_amount')->nullable();
            $table->bigInteger('discount')->nullable();
            $table->bigInteger('payable_amount')->nullable();
            $table->unsignedSmallInteger('status')->default(1);
            $table->unsignedSmallInteger('confirm_status')->default(0);
            $table->unsignedInteger('is_disc')->default(0);
            $table->unsignedInteger('disc_value')->default(0);
            $table->unsignedSmallInteger('disc_type')->default(0);
            $table->unsignedInteger('disc_amount')->default(0);
            $table->unsignedInteger('total_after_disc')->default(0);
            $table->unsignedSmallInteger('is_taxable')->default(0);
            $table->unsignedSmallInteger('tax_percent')->default(0);
            $table->unsignedSmallInteger('is_service')->default(0);
            $table->unsignedSmallInteger('service_value')->default(0);
            $table->unsignedSmallInteger('service_type')->default(0);
            $table->unsignedInteger('service_tax_amount')->default(0);

            $table->unsignedInteger('misc_amount')->nullable();
            $table->unsignedInteger('total_discount')->nullable();
            $table->unsignedInteger('total_tax')->nullable();
            $table->unsignedInteger('service_tax')->nullable();
            $table->unsignedInteger('total_after_service_tax')->nullable();
            $table->unsignedInteger('gross_total')->nullable();
            $table->unsignedInteger('net_income')->nullable();

            $table->unsignedInteger('overall_discount')->nullable();
            $table->unsignedInteger('service_amount')->nullable();

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            
//            $table->foreign('order_id')->references('id')->on('mrk_sale_pipleines');
//            $table->foreign('customer_id')->references('databank_id')->on('mrk_sale_pipleines');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_salesinvoice');
    }
}
