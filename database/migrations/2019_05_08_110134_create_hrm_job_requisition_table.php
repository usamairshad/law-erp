<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrmJobRequisitionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Schema::create('erp_hrm_job_requisition', function(Blueprint $table)
		// {
		// 	$table->increments('id');
		// 	$table->integer('job_title_id')->unsigned();
		// 	$table->integer('vacancies')->nullable()->default(1);
		// 	$table->integer('department_id');
		// 	$table->string('grade', 10)->nullable();
		// 	$table->string('required_education', 1000);
		// 	$table->string('required_experience', 1000);
		// 	$table->integer('employment_type')->nullable();
		// 	$table->integer('location_id')->nullable();
		// 	$table->integer('salary_range_from')->nullable();
		// 	$table->integer('salary_range_to')->nullable();
		// 	$table->date('position_date')->nullable();
		// 	$table->string('rationale', 1000)->nullable();
		// 	$table->integer('line_manager_id_a')->nullable();
		// 	$table->integer('line_manager_id_b')->nullable();
		// 	$table->string('requisition_reason', 1000);
		// 	$table->text('pre_requisites')->nullable();
		// 	$table->text('responsibilites')->nullable();
		// 	$table->text('job_objectives')->nullable();
		// 	$table->text('skills')->nullable();
		// 	$table->string('position_fill_type', 191)->nullable();
		// 	$table->string('vacancy_approval_status', 191)->nullable();
		// 	$table->string('reviewed_by', 191)->nullable();
		// 	$table->string('new_hire_name', 191)->nullable();
		// 	$table->integer('approved_salary')->nullable();
		// 	$table->string('country', 191)->nullable();
		// 	$table->string('city', 191)->nullable();
		// 	$table->date('new_hire_joining_date')->nullable();
		// 	$table->integer('status')->nullable()->default(0);
		// 	$table->integer('created_by')->unsigned()->nullable();
		// 	$table->integer('updated_by')->unsigned()->nullable();
		// 	$table->integer('deleted_by')->unsigned()->nullable();
		// 	$table->timestamps();
		// 	$table->softDeletes();
  //           $table->foreign('job_title_id')->references('id')->on('erp_hrm_job_title');
  //           $table->foreign('created_by')->references('id')->on('erp_users');
  //           $table->foreign('updated_by')->references('id')->on('erp_users');
  //           $table->foreign('deleted_by')->references('id')->on('erp_users');
		// });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('erp_hrm_job_requisition');
	}

}
