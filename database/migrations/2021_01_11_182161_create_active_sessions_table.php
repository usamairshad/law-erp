<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_sessions', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('title');
            $table->unsignedInteger('session_id');
            $table->unsignedInteger('board_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('program_id');
            $table->unsignedInteger('class_id');
            $table->enum('status',['Active','InActive','Pending'])->default('Active');
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key relationships

            $table->foreign('session_id')->references('id')->on('sessions');
            $table->foreign('board_id')->references('id')->on('boards');
            $table->foreign('branch_id')->references('id')->on('erp_branches');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('class_id')->references('id')->on('classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_sessions');
    }
}
