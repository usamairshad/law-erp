<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('working_hours');
            $table->string('reg_no')->nullable();
            $table->integer('training');
            $table->string('staff_type');
            $table->string('join_date');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('father_name');
            $table->string('mother_name');
            $table->string('date_of_birth');
            $table->string('ntn');
            $table->string('conveyance');
            $table->string('house_rent');
            $table->string('EOBI');
            $table->string('provident_fund');
            $table->string('provident_amount');
            $table->string('eobi_amount');
            $table->string('medical_allowances');
            $table->string('filer_type');
            $table->string('gender');
            $table->string('blood_group');
            $table->string('nationality');
            $table->string('mother_tongue');
            $table->string('address');
            $table->string('country');
            $table->string('city');
            $table->string('temp_address');
            $table->string('home_phone');
            $table->string('mobile_1');
            $table->string('mobile_2')->nullable();
            $table->string('email')->unique();
            $table->string('qualification');
            $table->string('experience');
            $table->string('other_info')->nullable();
            $table->string('staff_image')->nullable();
            $table->string('skill');
            $table->string('salary')->nullable();
            $table->string('salary_per_hour')->nullable();  
            $table->string('password');
            $table->string('cnic')->unique();
            $table->enum('status',['Active','InActive','Pending'])->default('Active');
            $table->enum('job_status',['Full Time','Part Time','Visiting','Lecture base']);
            $table->string('start_day_contract')->nullable();
            $table->string('end_day_contract')->nullable();
            $table->string('probation')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key relationships
            $table->foreign('user_id')->references('id')->on('erp_users');
            $table->foreign('branch_id')->references('id')->on('erp_branches');
            $table->foreign('created_by')->references('id')->on('erp_users');
            $table->foreign('updated_by')->references('id')->on('erp_users');
            $table->foreign('deleted_by')->references('id')->on('erp_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
