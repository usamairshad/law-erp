<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadExperienceLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_experience_letters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('name');
            $table->string('reg_no');  
            $table->string('designation');
            $table->string('branch_name');
            $table->string('class'); 
            $table->string('subject');
            $table->string('home_address');   
            $table->string('contact');
            $table->string('date_of_joining');
            $table->string('last_day_of_work');
            $table->timestamps();  

     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_experience_letters');
    }
}

