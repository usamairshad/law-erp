<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacultySemestersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faculty_semesters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('last_updated_by')->nullable();
            $table->unsignedInteger('program_id');
            $table->unsignedInteger('term_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('session_id');
            $table->unsignedInteger('class_teacher');
            $table->unsignedInteger('faculty_id');
            $table->string('section_name')->nullable();
            $table->integer('student_strength')->nullable();
            $table->text('limit_counter')->nullable();
            $table->text('slug')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faculty_semesters');
    }
}
