<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeScholarshipDefinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_scholarship_definations', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('session_id');
            $table->Integer('branch_id');
            $table->Integer('program_id');   
            $table->string('title'); 
            $table->Integer('no_of_a');
            $table->enum('term_percentage',['0','1'])->default('0');
            $table->enum('status',['0','1'])->default('1');
            $table->Integer('created_by');
            $table->Integer('last_updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_scholarship_definations');
    }
}
