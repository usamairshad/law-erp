<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraineesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainees', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('staff_id');
            $table->unsignedInteger('training_id');
            $table->boolean('avalibility');
            $table->string('remarks')->nullable();
            $table->boolean('acknowledge')->default(0);
            $table->timestamps();


            $table->foreign("training_id")->references("id")->on("trainings")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainees');
    }
}
