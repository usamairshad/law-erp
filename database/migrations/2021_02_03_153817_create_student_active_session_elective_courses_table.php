<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentActiveSessionElectiveCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('std_active_s_elecive', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_student_id');
            $table->unsignedInteger('course_id');
            
            $table->softDeletes();
            $table->timestamps();

            // Foreign Key relationships
            $table->foreign('active_session_student_id')->references('id')->on('active_session_students');
            $table->foreign('course_id')->references('id')->on('courses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('std_active_s_elecive');
    }
}
