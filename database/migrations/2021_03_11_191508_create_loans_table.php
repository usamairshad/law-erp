<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('amount')->nullable();
            $table->enum('type', array('advance', 'loan'));
            $table->enum('salary_deduct', array('yes', 'no'));
            $table->enum('paid_status', array('not paid', 'partial paid', 'paid'));
            $table->date('return_date')->nullable();
            $table->integer('remaining')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
