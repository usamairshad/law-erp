<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('active_session_id')->nullable();
            $table->unsignedInteger('active_session_teacher_id')->nullable();
            $table->unsignedInteger('active_session_section_id')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('status')->nullable();
            $table->unsignedInteger('assesment_type_id')->nullable();
            $table->integer('sorting_order')->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('last_updated_by')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
    }
}
