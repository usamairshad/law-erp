<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHrmWorkShiftsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Schema::create('erp_hrm_work_shifts', function(Blueprint $table)
		// {
		// 	$table->increments('id');
		// 	$table->string('name');
		// 	$table->string('min_hours_short')->nullable();
		// 	$table->string('code')->nullable();
		// 	$table->string('color')->nullable();
		// 	$table->float('hours_per_day', 11)->default(0.00);
		// 	$table->string('shift_start_time', 5);
		// 	$table->string('shift_end_time', 5);
		// 	$table->string('min_hours_half', 5);
		// 	$table->string('min_hours_full', 5);
		// 	$table->float('working_hours_per_day', 11)->default(0.00);
		// 	$table->string('grace_come_late', 5)->nullable();
		// 	$table->string('break_start_time', 5)->nullable();
		// 	$table->string('break_end_time', 5)->nullable();
		// 	$table->float('break_hours_per_day', 11)->default(0.00);
  //           $table->string('grace_come_early', 5)->nullable();
  //           $table->string('short_punch', 5)->nullable();
  //           $table->string('half_punch', 5)->nullable();
  //           $table->string('full_punch', 5)->nullable();
  //           $table->string('grace_leave_early', 5)->nullable();
		// 	$table->smallInteger('status')->unsigned()->default(1);
		// 	$table->integer('created_by')->unsigned()->nullable();
		// 	$table->integer('updated_by')->unsigned()->nullable();
		// 	$table->integer('deleted_by')->unsigned()->nullable();
		// 	$table->timestamps();
		// 	$table->softDeletes();
  //           $table->foreign('created_by')->references('id')->on('erp_users');
  //           $table->foreign('updated_by')->references('id')->on('erp_users');
  //           $table->foreign('deleted_by')->references('id')->on('erp_users');
		// });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('erp_hrm_work_shifts');
	}

}
